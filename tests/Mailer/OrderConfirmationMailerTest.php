<?php

namespace Tests\Mailer;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Site\Site;
use AppBundle\Services\Admin;
use Doctrine\ORM\EntityManagerInterface;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\SwiftmailerBundle\DataCollector\MessageDataCollector;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class OrderConfirmationMailerTest
 * @package Tests\Mailer
 */
class OrderConfirmationMailerTest extends WebTestCase
{
    use ContainerAwareTrait;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Admin
     */
    private $admin;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());

        $this->translator = $this->container->get('translator');
        $this->entityManager = $this->container->get('doctrine.orm.entity_manager');
        $this->admin = $this->container->get('admin');
    }

    /**
     * @covers OrderConfirmationMailer::send
     */
    public function testMailIsSentAndContentIsOk()
    {
        $orderCollection = $this->entityManager->getRepository(OrderCollection::class)->find(1);

        $this->client = static::createClient();
        $this->client->enableProfiler();

        $router = $this->client->getContainer()->get('router');
        $router->setContext($this->admin->getRequestContext());

        $url = $router->generate('app_test_orderconfirmationmailertest_sendtestmail', [
            'orderCollection' => $orderCollection->getId()
        ], RouterInterface::ABSOLUTE_URL);

        $this->client->request('GET', $url);

        /** @var MessageDataCollector $mailCollector */
        $mailCollector = $this->client->getProfile()->getCollector('swiftmailer');

        static::assertSame(1, $mailCollector->getMessageCount());

        /** @var Swift_Message[] $collectedMessages */
        $collectedMessages = $mailCollector->getMessages();

        /** @var Site $site */
        $site = $orderCollection->getSite();
        $from = $site->getEmail();

        if(isset($collectedMessages[0])) {
            $message = $collectedMessages[0];

            $transSubject = $this->translator->trans('mailer.order_confirmation.subject', [
                '%ordernr%' => '10000001'
            ], null, 'nl_NL');

            static::assertInstanceOf('Swift_Message', $message);
            static::assertEquals($transSubject, $message->getSubject());
            static::assertEquals($from, key($message->getFrom()));
        } else {
            static::fail('Mail Message not found');
        }
    }
}