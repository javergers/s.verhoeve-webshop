<?php

namespace Tests\ExactGlobe;

use AdminBundle\Services\Finance\InvoiceExportGeneratorService;
use AppBundle\DBAL\Types\CompanyInvoiceDiscountType;
use AppBundle\DBAL\Types\CompanyInvoiceInclPersonalDataType;
use AppBundle\Entity\Relation\Company;
use AppBundle\ThirdParty\Exact\Entity\InvoiceExport;
use AppBundle\ThirdParty\Exact\Xml\DebtorAdapter;
use AppBundle\ThirdParty\Exact\Xml\ExactXmlFactory;
use AppBundle\ThirdParty\Exact\Xml\InvoiceAdapter;

/**
 * Class XmlExportTest
 */
class XmlExportTest extends AbstractTest
{
    /**
     * @covers DebtorAdapter::_generateXml
     */
    public function testDeptorExport()
    {
        $entityManager = $this->container->get('doctrine')->getManager();
        $companiesRepository = $entityManager->getRepository(Company::class);
        $companies = $companiesRepository->findBy([
            'id' => 1,
        ]);

        $exactXmlFactory = $this->container->get(ExactXmlFactory::class);
        /** @var DebtorAdapter $deptorAdapter */
        $deptorAdapter = $exactXmlFactory->create(DebtorAdapter::class, new InvoiceExport());

        $xml = $deptorAdapter->_generateXml($companies);
        $md5 = 'b86bd75a6f1386171519f08f9a48e815';

        static::assertEquals($md5, md5($xml));
    }

    /**
     * @covers InvoiceAdapter::_generateXml
     */
    public function testInvoiceExport()
    {
        $order = $this->fetchOrder();
        $invoice = $this->mockInvoice();

        $invoiceExportGeneratorService = $this->container->get(InvoiceExportGeneratorService::class);
        $invoiceExportGeneratorService->transformOrderToInvoiceLines($invoice, $order);

        $exactXmlFactory = $this->container->get(ExactXmlFactory::class);
        /** @var InvoiceAdapter $invoiceAdapter */
        $invoiceAdapter = $exactXmlFactory->create(InvoiceAdapter::class, new InvoiceExport());

        $xml = $invoiceAdapter->_generateXml([$invoice]);
        $md5 = 'd4d7146b81c97a1f8bea1cfae2a0c969';

        static::assertEquals($md5, md5($xml));
    }
}