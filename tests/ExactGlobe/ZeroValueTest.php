<?php

namespace Tests\ExactGlobe;

use AdminBundle\Services\Finance\InvoiceExportGeneratorService;
use AppBundle\DBAL\Types\CompanyInvoiceDiscountType;
use AppBundle\DBAL\Types\CompanyInvoiceInclPersonalDataType;

/**
 * Class ZeroValueTest
 */
class ZeroValueTest extends AbstractTest
{
    /**
     * @covers CompanyInvoiceSettings::setInvoiceZeroValue
     * @covers CompanyInvoiceSettings::setInvoiceDiscount
     * @covers InvoiceExportGeneratorService::transformOrderToInvoiceLines
     */
    public function testZeroValueTrue()
    {
        $order = $this->fetchOrder();

        $invoice = $this->mockInvoice();
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceZeroValue(true);
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceDiscount(CompanyInvoiceDiscountType::INVOICE_DISCOUNT_APPLIED);

        $invoiceExportGeneratorService = $this->container->get(InvoiceExportGeneratorService::class);
        $invoiceExportGeneratorService->transformOrderToInvoiceLines($invoice, $order);

        static::assertCount(3, $invoice->getLines());
        static::assertEquals(0.00, $invoice->getLines()[1]->getPriceValue());
    }

    /**
     * @covers CompanyInvoiceSettings::setInvoiceZeroValue
     * @covers CompanyInvoiceSettings::setInvoiceDiscount
     * @covers InvoiceExportGeneratorService::transformOrderToInvoiceLines
     */
    public function testZeroValueFalse()
    {
        $order = $this->fetchOrder();

        $invoice = $this->mockInvoice();
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceZeroValue(false);
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceDiscount(CompanyInvoiceDiscountType::INVOICE_DISCOUNT_APPLIED);

        $invoiceExportGeneratorService = $this->container->get(InvoiceExportGeneratorService::class);
        $invoiceExportGeneratorService->transformOrderToInvoiceLines($invoice, $order);

        static::assertCount(2, $invoice->getLines());
    }

    /**
     * @covers CompanyInvoiceSettings::setInvoiceZeroValue
     * @covers CompanyInvoiceSettings::setInvoiceDiscount
     * @covers CompanyInvoiceSettings::setInvoiceInclPersonalData
     * @covers InvoiceExportGeneratorService::transformOrderToInvoiceLines
     */
    public function testZeroValueFalseWithPersonalDataAll()
    {
        $order = $this->fetchOrder();

        $invoice = $this->mockInvoice();
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceZeroValue(false);
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceDiscount(CompanyInvoiceDiscountType::INVOICE_DISCOUNT_APPLIED);
        $invoice->getCompany()->getCompanyInvoiceSettings()->setInvoiceInclPersonalData(CompanyInvoiceInclPersonalDataType::INVOICE_INCL_PERSONAL_DATA_ALL);

        $invoiceExportGeneratorService = $this->container->get(InvoiceExportGeneratorService::class);
        $invoiceExportGeneratorService->transformOrderToInvoiceLines($invoice, $order);

        static::assertCount(2, $invoice->getLines());
    }
}