<?php

namespace Tests\ExactGlobe;

use AdminBundle\Services\Finance\InvoiceExportGeneratorService;
use AdminBundle\Services\Finance\InvoiceExportService;
use AppBundle\DBAL\Types\CompanyInvoiceFrequencyType;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use DateTime;
use Exception;
use ReflectionException;
use ReflectionProperty;

/**
 * Class InvoiceFrequencyTest
 */
class InvoiceFrequencyTest extends AbstractTest
{
    private const DATE_FORMAT = 'l d F Y';

    /**
     * @covers CompanyInvoiceSettings::setInvoiceFrequency
     * @covers InvoiceExportGeneratorService::groupOrders
     * @throws Exception
     */
    public function testOrder()
    {
        $orderCollections = $this->mockOrdersCollections();

        $company = $orderCollections[0]->getCompany();
        $company->getCompanyInvoiceSettings()->setInvoiceFrequency(CompanyInvoiceFrequencyType::INVOICE_FREQUENCY_ORDER);

        $orders = $this->orderCollectionsToArrayOfOrders($orderCollections);

        $groupedOrders = $this->container->get(InvoiceExportGeneratorService::class)->groupOrders($orders);

        static::assertCount(count($orders), $groupedOrders);
    }

    /**
     * @covers CompanyInvoiceSettings::setInvoiceFrequency
     * @covers InvoiceExportGeneratorService::groupOrders
     * @throws Exception
     */
    public function testOrderCollection()
    {
        $orderCollections = $this->mockOrdersCollections();

        $company = $orderCollections[0]->getCompany();
        $company->getCompanyInvoiceSettings()->setInvoiceFrequency(CompanyInvoiceFrequencyType::INVOICE_FREQUENCY_ORDER_COLLECTION);

        $orders = $this->orderCollectionsToArrayOfOrders($orderCollections);

        $groupedOrders = $this->container->get(InvoiceExportGeneratorService::class)->groupOrders($orders);

        static::assertCount(count($orderCollections), $groupedOrders);
    }

    /**
     * @covers InvoiceExportService::calculateRuleOccurences
     */
    public function testDaily()
    {
        $deliveryDate = new \DateTime('2019-01-03');

        $occurences = $this->container->get(InvoiceExportService::class)->calculateRuleOccurences(InvoiceExportService::RRULE_DAILY, $deliveryDate);

        $this->assertSame('Sunday 30 December 2018', $occurences[0]->getStart()->format(self::DATE_FORMAT));
        $this->assertSame('Monday 31 December 2018', $occurences[1]->getStart()->format(self::DATE_FORMAT));
        $this->assertSame('Tuesday 01 January 2019', $occurences[2]->getStart()->format(self::DATE_FORMAT));
    }

    /**
     * @covers InvoiceExportService::calculateRuleOccurences
     */
    public function testWeekly()
    {
        $deliveryDate = new \DateTime('2019-01-14');

        $occurences = $this->container->get(InvoiceExportService::class)->calculateRuleOccurences(InvoiceExportService::RRULE_WEEKLY, $deliveryDate);

        $this->assertSame('Sunday 30 December 2018', $occurences[0]->getStart()->format(self::DATE_FORMAT));
        $this->assertSame('Sunday 06 January 2019', $occurences[1]->getStart()->format(self::DATE_FORMAT));
        $this->assertSame('Sunday 13 January 2019', $occurences[2]->getStart()->format(self::DATE_FORMAT));
    }

    /**
     * @covers InvoiceExportService::calculateRuleOccurences
     */
    public function testBiweekly()
    {
        $deliveryDate = new \DateTime('2019-01-28');

        $occurences = $this->container->get(InvoiceExportService::class)->calculateRuleOccurences(InvoiceExportService::RRULE_BIWEEKLY, $deliveryDate);

        $this->assertSame('Sunday 30 December 2018', $occurences[0]->getStart()->format(self::DATE_FORMAT));
        $this->assertSame('Sunday 13 January 2019', $occurences[1]->getStart()->format(self::DATE_FORMAT));
        $this->assertSame('Sunday 27 January 2019', $occurences[2]->getStart()->format(self::DATE_FORMAT));
    }

    /**
     * @covers InvoiceExportService::calculateRuleOccurences
     */
    public function testMonthly()
    {
        $deliveryDate = new \DateTime('2019-03-01');

        $occurences = $this->container->get(InvoiceExportService::class)->calculateRuleOccurences(InvoiceExportService::RRULE_MONTHLY, $deliveryDate);

        $this->assertSame('Monday 31 December 2018', $occurences[0]->getStart()->format(self::DATE_FORMAT));
        $this->assertSame('Thursday 31 January 2019', $occurences[1]->getStart()->format(self::DATE_FORMAT));
        $this->assertSame('Thursday 28 February 2019', $occurences[2]->getStart()->format(self::DATE_FORMAT));
    }

    /**
     * @covers InvoiceExportService::calculateRuleOccurences
     */
    public function testQuarterly()
    {
        $deliveryDate = new \DateTime('2019-07-01');

        $occurences = $this->container->get(InvoiceExportService::class)->calculateRuleOccurences(InvoiceExportService::RRULE_QUARTERLY, $deliveryDate);

        $this->assertSame('Monday 31 December 2018', $occurences[0]->getStart()->format(self::DATE_FORMAT));
        $this->assertSame('Sunday 31 March 2019', $occurences[1]->getStart()->format(self::DATE_FORMAT));
        $this->assertSame('Sunday 30 June 2019', $occurences[2]->getStart()->format(self::DATE_FORMAT));
    }

    /**
     * @param OrderCollection[] $orderCollections
     * @return Order[]
     */
    private function orderCollectionsToArrayOfOrders(array $orderCollections): array
    {
        $orders = [];

        foreach ($orderCollections as $orderCollection) {
            foreach ($orderCollection->getOrders() as $order) {
                $orders[] = $order;
            }
        }

        return $orders;
    }

    /**
     * @return OrderCollection[]
     * @throws Exception
     */
    private function mockOrdersCollections(): array
    {
        return [
            $this->mockOrdersCollection(),
            $this->mockOrdersCollection(),
        ];
    }

    /**
     * @return OrderCollection
     * @throws Exception
     */
    private function mockOrdersCollection(): OrderCollection
    {
        $orderCollection = new OrderCollection();
        $orderCollection->setCompany($this->mockCompany());
        $orderCollection->addOrder($this->mockOrder());
        $orderCollection->addOrder($this->mockOrder());

        $this->setId($orderCollection);

        return $orderCollection;
    }

    /**
     * @return Order
     * @throws ReflectionException
     */
    private function mockOrder(): Order
    {
        $order = new Order();
        $order->setDeliveryDate(new DateTime('2019-01-01'));

        $this->setId($order);

        return $order;
    }

    /**
     * @param object $object
     * @throws ReflectionException
     * @throws Exception
     */
    private function setId(object $object): void
    {
        // Mimic an id
        $reflector = new ReflectionProperty($object, 'id');
        $reflector->setAccessible(true);
        $reflector->setValue($object, $this->generateId());
    }

    /**
     * @return int
     * @throws Exception
     */
    private function generateId(): int
    {
        // rand suffixed with microseconds ensures unqiue id's
        return (int) random_int(1000, 9999) . explode('.', microtime(true))[1];
    }
}