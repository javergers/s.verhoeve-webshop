<?php

namespace Tests\Holidays\Traits;

use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Catalog\Product\Vat;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Order\Cart;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Site;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait MocksTrait
 * @package Tests\Holidays\Traits
 */
trait MocksTrait
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @return Customer
     */
    private function mockCustomer(): Customer
    {
        $customer = new Customer();
        $customer->setEmail('test@topgeschenken.nl');

        $mainAddress = $this->mockAddress();
        $mainAddress->setMainAddress(true);

        $customer->addAddress($mainAddress);

        return $customer;
    }

    /**
     * @return Address
     */
    private function mockAddress(): Address
    {
        /** @var Country $country */
        $country = $this->container->get('doctrine')->getRepository(Country::class)->find('NL');

        $address = new Address();
        $address->setCountry($country);

        return $address;
    }

    /**
     * @return Site
     */
    private function mockSite(): Site
    {
        return new Site();
    }

    /**
     * @return Cart
     */
    private function mockCart(): Cart
    {
        $customer = $this->mockCustomer();

        $site = $this->mockSite();

        $cart = new Cart();
        $cart->setCustomer();
        $cart->setInvoiceAddressCountry($customer->getMainAddress()->getCountry());
        $cart->setSite($site);
        $cart->setTotalPriceIncl(99.99);

        $cartOrder = $this->mockCartOrder();
        $cartOrder->setCart($cart);

        $cartOrderLine = $this->mockCartOrderLine();
        $cartOrderLine->setCartOrder($cartOrder);
        $cartOrderLine->setPrice(18.82);
        $cartOrderLine->setQuantity(1);

        $cartOrder->addLine($cartOrderLine);

        $cartOrderLine = $this->mockCartOrderLine();
        $cartOrderLine->setCartOrder($cartOrder);
        $cartOrderLine->setPrice(7.5);
        $cartOrderLine->setQuantity(1);

        $cartOrder->addLine($cartOrderLine);
        $cartOrder->calculateTotals();

        $cart->addOrder($cartOrder);
        $cart->calculateTotals();

        return $cart;
    }

    /**
     * @return CartOrder
     */
    private function mockCartOrder(): CartOrder
    {
        return new CartOrder();
    }

    /**
     * @return CartOrderLine
     */
    private function mockCartOrderLine(): CartOrderLine
    {
        $product = $this->mockProduct();

        $cartOrderLine = new CartOrderLine();
        $cartOrderLine->setProduct($product);

        return $cartOrderLine;
    }

    /**
     * @return Product
     */
    private function mockProduct(): Product
    {
        $vat = $this->container->get('doctrine')->getRepository(Vat::class)->find(2);

        $productGroup = $this->mockProductGroup();

        $product = new GenericProduct();
        $product->setVat($vat);
        $product->setProductgroup($productGroup);

        return $product;
    }

    /**
     * @return Productgroup
     */
    private function mockProductGroup(): Productgroup
    {
        $productGroup = new Productgroup();
        $productGroup->setSkuPrefix('FRT');

        return $productGroup;
    }
}