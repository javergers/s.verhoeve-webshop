<?php

namespace App\Tests\DeliveryArea;

use AppBundle\Entity\Geography\Postcode;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Supplier\DeliveryArea;
use AppBundle\Exceptions\LinkedPreferredSuppliersException;
use AppBundle\Services\DeliveryArea\DeliveryAreaService;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use AppBundle\Manager\Supplier\DeliveryAreaManager;

/**
 * Class DeliveryAreaTest
 * @package App\Tests\DeliveryArea
 */
class DeliveryAreaTest extends KernelTestCase implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var DeliveryAreaManager */
    private $deliveryAreaManager;

    /** @var DeliveryAreaService */
    private $deliveryAreaService;

    /** @var ArrayCollection */
    private $deliveryAreaCollection;

    /** @var Registry */
    private $registry;

    /** @var Company[] */
    private $suppliers;

    /** @var Postcode[] */
    private $postcodes;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());

        $this->deliveryAreaManager = $this->container->get(DeliveryAreaManager::class);
        $this->deliveryAreaService = $this->container->get('delivery_area');
        $this->registry = $this->container->get('doctrine');
    }

    /**
     * @throws ConnectionException
     * @throws \Exception
     */
    public function testCreateDeliveryArea()
    {
        $doctrine = $this->getDoctrine();

        /** @var Connection $connection */
        $connection = $doctrine->getConnection();

        $connection->beginTransaction();

        try {
            $this->createTestDeliveryArea();

            $em = $doctrine->getManager();
            $em->flush();

            $succeed = true;
        } catch (\Exception $exception) {
            $succeed = false;
            throw $exception;
        } finally {
            self::assertTrue($succeed);

            $connection->rollBack();
        }
    }

    /**
     * @throws ConnectionException
     */
    public function testCreateDeliveryAreaWithPreferredSupplier()
    {
        $doctrine = $this->getDoctrine();

        /** @var Connection $connection */
        $connection = $doctrine->getConnection();

        $connection->beginTransaction();

        try {
            $deliveryAreaCollection = $this->createMultiTestDeliveryAreas();

            $preferredSettings = $this->createTestPreferredSettings($deliveryAreaCollection);

            $this->deliveryAreaManager->markPreferredSuppliers($deliveryAreaCollection, $preferredSettings);

            $doctrine->getManager()->flush();

            $succeed = true;
            $message = 'Succeed';
        } catch (\Exception $exception) {
            $succeed = false;
            $message = $exception->getMessage();
        } finally {
            self::assertTrue($succeed, $message);

            $connection->rollBack();
        }
    }

    /**
     * @throws ConnectionException
     */
    public function testRemovePreferredSupplierOnDeliveryArea()
    {
        $doctrine = $this->getDoctrine();

        /** @var Connection $connection */
        $connection = $doctrine->getConnection();

        $connection->beginTransaction();

        try {
            $companyName = 'Tester Nederland B.V.';

            $viewCompany = $doctrine->getManager()->getRepository(Company::class)->findOneBy([
                'name' => $companyName,
            ]);

            if (null === $viewCompany) {
                throw new \RuntimeException(sprintf('Test company `%s` not found', $companyName));
            }

            $deliveryAreaCollection = $this->createMultiTestDeliveryAreas();

            $preferredSettings = $this->createTestPreferredSettings($deliveryAreaCollection);

            $this->deliveryAreaManager->markPreferredSuppliers($deliveryAreaCollection, $preferredSettings);
            $this->deliveryAreaManager->markPreferredSuppliers($deliveryAreaCollection, $preferredSettings,
                $viewCompany);

            $deliveryAreasWithPreferred = $this->deliveryAreaManager->filterOnPreferredSupplier($deliveryAreaCollection);

            /** @var DeliveryArea $deliveryArea */
            foreach ($deliveryAreasWithPreferred as $deliveryArea) {
                if ($deliveryArea->getPreferredSuppliers()->count() > 1) {
                    $this->deliveryAreaManager->remove($deliveryArea);
                }
            }

            $doctrine->getManager()->flush();

            $message = 'Test failed';
            $succeed = false;

        } catch (LinkedPreferredSuppliersException $exception) {
            $succeed = true;
            $message = $exception->getMessage();
        } catch (\Exception $exception) {
            $succeed = false;
            $message = $exception->getMessage();
        } finally {
            self::assertTrue($succeed, $message);

            $connection->rollBack();
        }
    }

    /**
     * @throws ConnectionException
     */
    public function testRemoveMultiPreferredSupplierOnDeliveryArea()
    {
        $doctrine = $this->getDoctrine();

        /** @var Connection $connection */
        $connection = $doctrine->getConnection();

        $connection->beginTransaction();

        try {
            $deliveryAreaCollection = $this->createMultiTestDeliveryAreas();

            $preferredSettings = $this->createTestPreferredSettings($deliveryAreaCollection);

            $this->deliveryAreaManager->markPreferredSuppliers($deliveryAreaCollection, $preferredSettings);

            $deliveryAreaWithPreferred = $this->deliveryAreaManager->filterOnPreferredSupplier($deliveryAreaCollection);

            if (false !== ($deliveryArea = $deliveryAreaWithPreferred->first())) {
                $this->deliveryAreaManager->remove($deliveryArea);
            }

            $succeed = true;
            $message = 'Succeed';
        } catch (\Exception $exception) {
            $succeed = false;
            $message = $exception->getMessage();
        } finally {
            self::assertTrue($succeed, $message);

            $connection->rollBack();
        }
    }

    /**
     * @return DeliveryArea|null
     *
     * @throws OptimisticLockException
     */
    private function createTestDeliveryArea(): ?DeliveryArea
    {
        $suppliers = $this->getSuppliers();
        $postcodes = $this->getPostcodes();

        if (null === $suppliers) {
            throw new \RuntimeException("There aren't any suppliers");
        }

        if (null === $postcodes) {
            throw new \RuntimeException("There aren't any postcodes");
        }

        $supplier = $suppliers[array_rand($suppliers)];
        $postcode = $postcodes[array_rand($postcodes)];

        $exists = false;
        if (null !== $this->deliveryAreaCollection) {
            $exists = $this->deliveryAreaCollection->filter(function (DeliveryArea $deliveryArea) use (
                $supplier,
                $postcode
            ) {
                return $deliveryArea->getCompany()->getId() === $supplier->getId()
                    && $deliveryArea->getPostcode()->getId() === $postcode->getId();
            })->first();
        }

        if (false !== $exists) {
            return null;
        }

        $deliveryArea = new DeliveryArea();
        $deliveryArea->setCompany($supplier);
        $deliveryArea->setPostcode($postcode);

        $faker = Factory::create();
        $deliveryArea->setDeliveryPrice($faker->randomFloat(3, 0, 10));

        $deliveryArea->setDeliveryInterval('P1DT7H');

        $this->deliveryAreaManager->create($deliveryArea);

        return $deliveryArea;
    }

    /**
     * @param int $count
     *
     * @return ArrayCollection
     *
     * @throws OptimisticLockException
     */
    private function createMultiTestDeliveryAreas(int $count = 50): ArrayCollection
    {
        $this->deliveryAreaCollection = new ArrayCollection();

        for ($i = 1; $i <= $count; $i++) {
            $newDeliveryArea = $this->createTestDeliveryArea();

            if ($newDeliveryArea instanceof DeliveryArea) {
                $this->deliveryAreaCollection->add($newDeliveryArea);
            }
        }

        return $this->deliveryAreaCollection;
    }

    /**
     * @return Registry
     */
    private function getDoctrine(): Registry
    {
        return $this->registry;
    }

    /**
     * @return Company[]|array
     */
    private function getSuppliers()
    {
        $em = $this->getDoctrine()->getManager();

        if (null === $this->suppliers) {
            $suppliers = $em->getRepository(Company::class)->findAllSuppliers();

            if (false === empty($suppliers)) {
                return $this->suppliers = $suppliers;
            }
        }

        return $this->suppliers;
    }

    /**
     * @return Postcode[]|array
     */
    private function getPostcodes()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        if (null === $this->postcodes) {

            /** @var EntityRepository $postcodeRepository */
            $postcodeRepository = $em->getRepository(Postcode::class);

            $postcodes = $postcodeRepository->findBy([
                'postcode' => [1000, 2000, 3000],
            ]);

            if (false !== $postcodes) {
                return $this->postcodes = $postcodes;
            }
        }

        return $this->postcodes;
    }

    /**
     * @param ArrayCollection|DeliveryArea[] $deliveryAreaCollection
     *
     * @return ArrayCollection
     *
     * @throws \Exception
     */
    private function createTestPreferredSettings(ArrayCollection $deliveryAreaCollection): ArrayCollection
    {
        $position = [];
        $preferredSettings = new ArrayCollection();

        foreach ($deliveryAreaCollection as $deliveryArea) {
            $postcode = $deliveryArea->getPostcode();
            $postcodeId = $postcode->getId();

            $company = $deliveryArea->getCompany();

            $isPreferred = (bool)random_int(0, 1);

            if (false === $isPreferred && false === ($deliveryAreaCollection->key() === 0)) {
                continue;
            }

            if (false === isset($position[$postcodeId])) {
                $position[$postcodeId] = 1;
            } else {
                $position[$postcodeId]++;
            }

            $preferredSettings->add([
                'position' => $position[$postcodeId] ?? 0,
                'postcode' => $postcode,
                'companyPreferredSupplier' => $company,
            ]);
        }

        return $preferredSettings;
    }
}