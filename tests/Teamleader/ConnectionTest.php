<?php

namespace Tests\Teamleader;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class ConnectionTest
 * @package Tests\Teamleader
 */
class ConnectionTest extends KernelTestCase
{
    use ContainerAwareTrait;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());
    }

    /**
     * @throws \Exception
     */
    public function testConnection(): void
    {
        if ($this->container->getParameter('teamleader_api_group') && $this->container->getParameter('teamleader_api_key')) {
            static::assertTrue($this->container->get('teamleader')->test());
        }
    }
}