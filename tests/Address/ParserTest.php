<?php

namespace App\Tests\Address;

use AppBundle\Utils\Address;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class ParserTest
 * @package App\Tests\Address
 */
class ParserTest extends KernelTestCase
{
    /**
     * @var Address
     */
    private $address;

    public function setUp(): void
    {
        self::bootKernel();

        $this->address = static::$kernel->getContainer()->get('address');
    }

    public function test1(): void
    {
        $this->address->parseAddress('A. van Leeuwenhoekweg 35');

        static::assertEquals('A. van Leeuwenhoekweg', $this->address->getStreet());
        static::assertEquals('35', $this->address->getHouseNumber());
        static::assertEquals('', $this->address->getHouseNumberAddition());
        static::assertEquals('', $this->address->getPostcode());
        static::assertEquals('', $this->address->getCity());
        static::assertEquals('', $this->address->getCountry());
    }

    public function test2(): void
    {
        $this->address->parseAddress('A. van Leeuwenhoekweg 35 unit B4');

        static::assertEquals('A. van Leeuwenhoekweg', $this->address->getStreet());
        static::assertEquals('35', $this->address->getHouseNumber());
        static::assertEquals('unit B4', $this->address->getHouseNumberAddition());
        static::assertEquals('', $this->address->getPostcode());
        static::assertEquals('', $this->address->getCity());
        static::assertEquals('', $this->address->getCountry());
    }

    public function test3(): void
    {
        $this->address->parseAddress('A. van Leeuwenhoekweg 35 unit B4, 2408AK Alphen aan den Rijn');

        static::assertEquals('A. van Leeuwenhoekweg', $this->address->getStreet());
        static::assertEquals('35', $this->address->getHouseNumber());
        static::assertEquals('unit B4', $this->address->getHouseNumberAddition());
        static::assertEquals('2408AK', $this->address->getPostcode());
        static::assertEquals('Alphen aan den Rijn', $this->address->getCity());
        static::assertEquals('', $this->address->getCountry());
    }

    public function test4(): void
    {
        $this->address->parseAddress('A. van Leeuwenhoekweg 35 unit B4, 2408AK Alphen aan den Rijn, NL');

        static::assertEquals('A. van Leeuwenhoekweg', $this->address->getStreet());
        static::assertEquals('35', $this->address->getHouseNumber());
        static::assertEquals('unit B4', $this->address->getHouseNumberAddition());
        static::assertEquals('2408AK', $this->address->getPostcode());
        static::assertEquals('Alphen aan den Rijn', $this->address->getCity());
        static::assertEquals('Nederland', $this->address->getCountry());
    }

    public function test5(): void
    {
        $this->address->parseAddress('A. van Leeuwenhoekweg 35 unit B4, 2408AK Alphen aan den Rijn, Nederland');

        static::assertEquals('A. van Leeuwenhoekweg', $this->address->getStreet());
        static::assertEquals('35', $this->address->getHouseNumber());
        static::assertEquals('unit B4', $this->address->getHouseNumberAddition());
        static::assertEquals('2408AK', $this->address->getPostcode());
        static::assertEquals('Alphen aan den Rijn', $this->address->getCity());
        static::assertEquals('Nederland', $this->address->getCountry());
    }

    public function test6(): void
    {
        $this->address->parseAddress('A. van Leeuwenhoekweg 35 unit B4' . PHP_EOL . '2408AK Alphen aan den Rijn' . PHP_EOL . 'Nederland');

        static::assertEquals('A. van Leeuwenhoekweg', $this->address->getStreet());
        static::assertEquals('35', $this->address->getHouseNumber());
        static::assertEquals('unit B4', $this->address->getHouseNumberAddition());
        static::assertEquals('2408AK', $this->address->getPostcode());
        static::assertEquals('Alphen aan den Rijn', $this->address->getCity());
        static::assertEquals('Nederland', $this->address->getCountry());
    }
}
