<?php

namespace Tests\TempAPI;

use AppBundle\Services\TempAPI\OrderStatusService;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class GbOrderStatusTest
 * @package Tests\TempAPI
 */
class GbOrderStatusTest extends WebTestCase
{
    use ContainerAwareTrait;

    private const JSON = '{"10000001-0001":[{"id":"143145","order_id":"1419787","order_tg_id":"10000001-0001","barcode":"3STNOX000135446","trace_url":"https:\/\/tracktrace.postnlpakketten.nl\/Search\/Searchbasic.aspx?B=3STNOX000135450&P=3297VH"},{"id":"143151","order_id":"1419787","order_tg_id":"10000001-0001","barcode":"3STNOX000135450","trace_url":"https:\/\/tracktrace.postnlpakketten.nl\/Search\/Searchbasic.aspx?B=3STNOX000135450&P=3297VH"}]}';

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());
    }

    /**
     * @throws ConnectionException
     * @throws \Exception
     */
    public function testProcess()
    {
        $doctrine = $this->container->get('doctrine');

        /** @var Connection $connection */
        $connection = $doctrine->getConnection();

        $orderStatusService = $this->container->get(OrderStatusService::class);

        $connection->beginTransaction();

        try {
            $result = $orderStatusService->process(self::JSON);

            self::assertTrue($result);
        } catch(\Exception $exception) {
            throw $exception;
        } finally {
            $connection->rollBack();
        }
    }
}