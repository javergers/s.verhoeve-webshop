<?php

namespace Tests\Controller;

use AppBundle\Utils\Translation;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * @covers ClassName
 *
 * Class TranslationTest
 */
class TranslationTest extends WebTestCase
{
    use ContainerAwareTrait;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());
    }

    /**
     * @covers \AppBundle\Utils\Translation::sanitizeTranslationString
     */
    public function testSanitizeTranslationString()
    {
        $translationUtil = new Translation();

        $spaces = '   ';
        $response = $translationUtil->sanitizeTranslationString($spaces);

        self::assertSame(' ', $response);

        $tab = "\t";
        $response = $translationUtil->sanitizeTranslationString($tab);

        self::assertSame(' ', $response);

        $br = "\r\n";
        $response = $translationUtil->sanitizeTranslationString($br);

        self::assertSame('<br>', $response);

        $br2 = "\n";
        $response = $translationUtil->sanitizeTranslationString($br2);

        self::assertSame('<br>', $response);

        $br3 = "\r";
        $response = $translationUtil->sanitizeTranslationString($br3);

        self::assertSame('<br>', $response);

        $htmlTag = '<a href="">link</a>';
        $response = $translationUtil->sanitizeTranslationString($htmlTag, true);
        self::assertSame(htmlspecialchars($htmlTag), $response);
    }
}
