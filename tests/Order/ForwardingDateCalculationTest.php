<?php

namespace Tests\Order;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Relation\Company;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class WEB2777Test
 * @package Tests\Order
 */
class ForwardingDateCalculationTest extends KernelTestCase
{
    use ContainerAwareTrait;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());
    }

    /**
     * Taarten voor maandag worden nu op zaterdag morgen worden verstuurd naar de bakker. Dit mag wel sneller.
     * Graag de bestellingen voor maandag vanaf vrijdag middag al door gaan sturen.
     *
     * @throws \Exception
     * @ticket WEB-2770
     */
    public function test1()
    {
        static::assertTrue(\extension_loaded('timecop'));

        $order = new Order();
        $order->setDeliveryDate(new \DateTime('2018-07-16'));
        $order->setCreatedAt(new \DateTime('2018-07-13 11:00:00'));

        timecop_freeze($order->getCreatedAt());

        $supplier = new Company();

        $forwardAt = $this->container->get('auto.forward_at_calculator')->calculateForwardDateTime($order, $supplier);

        self::assertInstanceOf(\DateTime::class, $forwardAt);
        self::assertEquals('2018-07-13', $forwardAt->format('Y-m-d'));
    }

    /**
     * Auto-order proces start te laat waardoor orders pas na de leverdatum worden doorgestuurd.
     * Voorbeeld: ordernummer 10044229-0001 wordt doorgestuurd op 09-07 21.58 uur. Terwijl de bezorgdatum 10-07 is.
     *
     * @throws \Exception
     * @ticket WEB-2770
     */
    public function test2()
    {
        static::assertTrue(\extension_loaded('timecop'));

        $order = new Order();
        $order->setDeliveryDate(new \DateTime('2018-07-10'));
        $order->setCreatedAt(new \DateTime('2018-07-08 21:58:08'));

        timecop_freeze($order->getCreatedAt());

        $supplier = new Company();

        $forwardAt = $this->container->get('auto.forward_at_calculator')->calculateForwardDateTime($order, $supplier);

        self::assertInstanceOf(\DateTime::class, $forwardAt);
        self::assertEquals('2018-07-09', $forwardAt->format('Y-m-d'));
        self::assertLessThanOrEqual('09:00:00', $forwardAt->format('H:i:s'));
    }

    public function tearDown(): void
    {
        timecop_return();

        parent::tearDown();
    }
}