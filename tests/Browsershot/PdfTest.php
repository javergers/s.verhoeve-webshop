<?php

namespace Appp\Tests\Browsershot;

use AppBundle\Utils\Browsershot\Browsershot;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * Class PdfTest
 * @package Appp\Tests\Browsershot
 */
class PdfTest extends KernelTestCase
{
    public function testGoogleDotCom(): void
    {
        $path = tempnam(sys_get_temp_dir(), 'pdf_');

        Browsershot::url('https://www.google.com')->savePdf($path);

        static::assertFileExists($path);
        static::assertNotEquals(0, filesize($path));
        static::assertEquals('application/pdf', mime_content_type($path));

        unlink($path);
    }
}
