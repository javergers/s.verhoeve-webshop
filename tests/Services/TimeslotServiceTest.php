<?php

namespace App\Tests\Services;

use AppBundle\Entity\Common\Timeslot;
use AppBundle\Services\Company\TimeslotService;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Recurr\Rule;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class TimeslotServiceTest
 *
 * @package Tests\App
 */
class TimeslotServiceTest extends WebTestCase
{
    use ContainerAwareTrait;

    /**
     * @var TimeslotService
     */
    public $timeslotService;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());

        $this->timeslotService = $this->container->get(TimeslotService::class);
    }

    /**
     * @throws \Recurr\Exception\InvalidRRule
     * @throws \Exception
     */
    public function test_timeslotResults()
    {
        $pickupTimes = $this->getPickupSlots();
        $date = new DateTime('tomorrow + 1day');
        $preperationTime = new DateTime();

        $result = $this->timeslotService->getAvailableTimeslots($pickupTimes, $preperationTime, $date);

        $expected = $this->getExpectedResult();

        $calculatedResult = [
            ['from' => $result[0]->getFrom()->format('H:i'), 'till' => $result[0]->getTill()->format('H:i')],
            ['from' => $result[1]->getFrom()->format('H:i'), 'till' => $result[1]->getTill()->format('H:i')],
        ];
        $expectedResult = [
            ['from' => $expected[0]->getFrom()->format('H:i'), 'till' => $expected[0]->getTill()->format('H:i')],
            ['from' => $expected[1]->getFrom()->format('H:i'), 'till' => $expected[1]->getTill()->format('H:i')],
        ];

        static::assertSame($calculatedResult, $expectedResult);
    }

    /**
     * @return ArrayCollection
     * @throws \Recurr\Exception\InvalidRRule
     * @throws \Exception
     */
    public function getFixedSlots(): ArrayCollection
    {
        return new ArrayCollection([
            new Timeslot(
                new DateTime('09:00:00'),
                new DateTime('12:00:00'),
                new Rule('FREQ=DAILY;BYDAY=MO,TU,WE,TH,FR,SA,SU;INTERVAL=1;DTSTART=20190101'),
                true,
                true
            ),
            new Timeslot(
                new DateTime('12:00:00'),
                new DateTime('15:00:00'),
                new Rule('FREQ=DAILY;BYDAY=MO,TU,WE,TH,FR,SA,SU;INTERVAL=1;DTSTART=20190101'),
                true,
                true
            ),
            new Timeslot(
                new DateTime('15:00:00'),
                new DateTime('18:00:00'),
                new Rule('FREQ=DAILY;BYDAY=MO,TU,WE,TH,FR,SA,SU;INTERVAL=1;DTSTART=20190101'),
                true,
                true
            ),
        ]);
    }

    /**
     * @return ArrayCollection
     * @throws \Recurr\Exception\InvalidRRule
     * @throws \Exception
     */
    public function getPickupSlots(): ArrayCollection
    {
        return new ArrayCollection([
            new Timeslot(
                new DateTime('09:00:00'),
                new DateTime('12:00:00'),
                new Rule('FREQ=DAILY;BYDAY=MO,TU,WE,TH,FR,SA,SU;INTERVAL=1;DTSTART=20190101'),
                true,
                true
            ),
            new Timeslot(
                new DateTime('13:00:00'),
                new DateTime('18:00:00'),
                new Rule('FREQ=DAILY;BYDAY=MO,TU,WE,TH,FR,SA,SU;INTERVAL=1;DTSTART=20190101'),
                true,
                true
            )
        ]);
    }

    /**
     * @return ArrayCollection
     * @throws \Recurr\Exception\InvalidRRule
     * @throws \Exception
     */
    public function getExpectedResult()
    {
        return new ArrayCollection([
            new Timeslot(
                new DateTime('09:00:00'),
                new DateTime('12:00:00'),
                new Rule('FREQ=DAILY;BYDAY=MO,TU,WE,TH,FR,SA,SU;INTERVAL=1;DTSTART=20190101'),
                true,
                true
            ),
            new Timeslot(
                new DateTime('15:00:00'),
                new DateTime('18:00:00'),
                new Rule('FREQ=DAILY;BYDAY=MO,TU,WE,TH,FR,SA,SU;INTERVAL=1;DTSTART=20190101'),
                true,
                true
            )
        ]);
    }
}
