<?php

namespace Tests\Geocoder;

use Geocoder\Exception\Exception as GeocoderException;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class GeocodeAddressTest
 * @package Tests\Geocoder
 */
class GeocodeAddressTest extends KernelTestCase
{
    use ContainerAwareTrait;

    public function setUp()
    {
        self::bootKernel();

        $this->setContainer(static::$kernel->getContainer());
    }

    /**
     * @throws GeocoderException
     */
    public function testGeocodeAddress(): void
    {
        $result = $this->container->get('geocoder')->geocode('Noorderdreef 60 2153LL Nieuw-Vennep');

        static::assertNotEmpty($result);

        $coordinates = $result->first()->getCoordinates();

        static::assertEquals(52.2661402, $coordinates->getLatitude());
        static::assertEquals(4.6416769, $coordinates->getLongitude());
    }
}
