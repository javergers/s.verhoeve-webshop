# Upgrade from 1.8 to 1.9
---

## Maandafrekening
 - Huidige orders moeten op commisionable=true worden gezet indien ze voldoen aan de opgestelde regels, query hiervoor gemaild naar Harry. Edwin zou nog een aantal handmatige mutaties doorgeven voor eind deze maand.
 - Voor alle bakker gerelateerde productgroepen moeten de commissies ingesteld worden op Leveranciersgroep 'Bakker'
 - Chrome headles moet geïnstalleerd worden (zie https://github.com/topgeschenken/playbook/issues/27)
 
  