# Upgrade from 1.3 to 1.4

Locales
-----
Vanaf deze versie wordt de voorkeurstaal opgeslagen bij de Customer Entity. Dit wordt niet met terugwerkende kracht
gedaan, dus voor alle bestaande klanten zal deze bij livegang op nl_NL gezet moeten worden.


(gast) klant converteren naar account
-----

Vanaf heden is het mogelijk via het beheer om een anonieme klant te converteren naar een account. 
Het is mogelijk meerdere klanten te selecteren en de actie als Bulk uit te voeren.

Het is wel van belang dat alle klant records een RegisteredOnSite gekoppeld hebben staan, anders zal
het proces niet succesvol uitgevoerd kunnen worden. De RegisteredOnSite is aanwezig sinds versie 1.3.
 
Toegang tot gegevens
-----

Het is nu mogelijk om op element niveau in een formulier aan te geven welke rechten nodig zijn het veld
überhaupt te mogen zien. Dit wordt volledig gebaseerd op het ROLE systeem wat Symfony bevat.

**Enkele Role Controleren**

```
'security' => [
    'roles' => 'ROLE_PAYMENT_MANAGEMENT'
]
```

**Meerdere Roles Controleren (OR)**

```
'security' => [
    'roles' => ['ROLE_PAYMENT_MANAGEMENT', 'ROLE_CATALOG_MANAGER']
]
```

**Meerdere Roles Controleren (AND)**

```
'security' => [
    'roles' => ['ROLE_PAYMENT_MANAGEMENT', 'ROLE_CATALOG_MANAGER']
    'strategy' => ACCESS_DECISION_STRATEGY::UNANIMOUS
]
```

Wijzigingen in site entity
-----
Bij de site entity is een Enum kolom toegevoegd, welke niet automatisch via het migratiebestand voorzien kan worden van een standaard waarde. Dit omdat de EnumBundle hier tussen zit.
Hiervoor dient er eenmalig een update querie gedraait de worden die voor alle sites de kolom `delivery_date_method` van de waarde `required` voorzien.

Topgeschenken Bedrijf toevoegen t.b.v. Afhalen
-----
Voor het afhalen is het nu nodig een Address Entity te koppelen aan de Site Entity. De stappen om dit te regelen zijn:

- Topgeschenken Nederland BV toevoegen als bedrijf
- Bij de Site entity dit specifieke adres koppelen

Instelbare about (homepage) tekst per site
-----
Het is nu mogelijk bij website instellingen de about tekst in te stellen, deze wordt op de homepage getoond.

Bij Topfruit.nl en Toptaarten.nl moet de about tekst pagina nog gekoppeld worden, de pagina zelf is al aangemaakt op de liveomgeving.

wkhtmltopdf
-----
Voor het genereren van PDF's is wkhtmltopdf nodig (zie playbook issue #26)