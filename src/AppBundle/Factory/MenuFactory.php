<?php

namespace AppBundle\Factory;

use AppBundle\Cache\Adapter\TagAwareAdapter;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Menu;
use AppBundle\Services\Slug;
use AppBundle\Utils\MenuStructure;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class MenuFactory
 * @package AppBundle\Factory
 */
class MenuFactory
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var TagAwareAdapter */
    private $cache;

    /** @var Slug */
    private $slugService;

    /** @var TokenStorage */
    private $tokenStorage;

    /** @var bool */
    private $cacheEnabled = true;

    /** @var TranslatorInterface */
    private $translator;

    /**
     * SyncOrderCommand constructor
     * @param EntityManagerInterface $entityManager
     * @param TagAwareAdapter        $cache
     * @param Slug                   $slugService
     * @param TokenStorage           $tokenStorage
     * @param TranslatorInterface    $translator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        TagAwareAdapter $cache,
        Slug $slugService,
        TokenStorage $tokenStorage,
        TranslatorInterface $translator
    ) {
        $this->entityManager = $entityManager;
        $this->cache = $cache;
        $this->slugService = $slugService;
        $this->tokenStorage = $tokenStorage;
        $this->translator = $translator;
    }

    /**
     * @return $this
     */
    public function disableCache(): self
    {
        $this->cacheEnabled = false;

        return $this;
    }

    /**
     * @param Menu $menu
     *
     * @return MenuStructure
     * @throws InvalidArgumentException
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function get(Menu $menu): MenuStructure
    {
        $user = null;
        $token = $this->tokenStorage->getToken();

        if ($token && $token->getUser() instanceof Customer) {
            $user = $token->getUser();
        }

        if (!$this->cacheEnabled) {
            return $this->getStructure($menu);
        }

        $cachedKey = $this->getCacheKey($menu);

        $cachedMenu = $this->cache->getItem($cachedKey);

        // @todo: cache never hits
        if (!$this->cacheEnabled || !$cachedMenu->isHit()) {
            $structure = $this->getStructure($menu);

            if ($this->cacheEnabled) {
                $cachedMenu->set($structure);
                $cachedMenu->expiresAfter(60 * 60 * 24);
                $cachedMenu->tag($this->cache->generateTagKey($menu));

                if ($user) {
                    $cachedMenu->tag($this->cache->generateTagKey($user));
                }

                $cachedMenu->tag(TagAwareAdapter::CLEAR_ON_RELEASE);

                $this->cache->save($cachedMenu);
            }
        } else {
            $structure = $cachedMenu->get();
        }

        return $structure;
    }

    /**
     * @param Menu $menu
     * @throws InvalidArgumentException
     */
    public function clearCache(Menu $menu): void
    {
        $cachedKey = $this->getCacheKey($menu);

        $cachedMenu = $this->cache->getItem($cachedKey);

        if ($cachedMenu->isHit()) {
            $this->cache->deleteItem($cachedMenu);
        }
    }

    /**
     * @param Menu $menu
     *
     * @return MenuStructure
     * @throws \Exception
     */
    public function getStructure(Menu $menu): MenuStructure
    {
        $menuStructure = new MenuStructure($this->entityManager, $this->slugService, $this->tokenStorage, $menu, $this->translator);

        return $menuStructure;
    }

    /**
     * @param Menu $menu
     *
     * @return string
     * @throws \RuntimeException
     */
    private function getCacheKey(Menu $menu): string
    {
        $user = null;
        $token = $this->tokenStorage->getToken();

        if ($token !== null && $token->getUser() instanceof Customer) {
            $user = $token->getUser();
        }

        $cachedKey = 'menu_' . $menu->getId();

        if ($user) {
            $cachedKey .= '_' . $user->getId();
        }

        return $cachedKey;
    }
}
