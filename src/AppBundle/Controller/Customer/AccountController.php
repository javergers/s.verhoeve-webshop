<?php

namespace AppBundle\Controller\Customer;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Manager\CustomerManager;
use AppBundle\Manager\Relation\CompanyManager;
use AppBundle\Services\SiteService;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Translation\Common\Model\Message;

/**
 * Class AccountController
 *
 * @package AppBundle\Controller\Customer
 *
 * @Route("/account")
 */
class AccountController extends Controller
{
    /**
     * @return array
     */
    public static function getTranslationMessages()
    {
        return [
            new Message('account.settings.success', 'messages'),
            new Message('customer.login.jwt_error', 'messages'),
        ];
    }

    /**
     * @Template()
     * @Route("/")
     *
     * @param Request $request
     *
     * @return array|RedirectResponse|Response
     */
    public function indexAction(Request $request)
    {
        /** @var $session Session */
        $session = $request->getSession();

        $authErrorKey = Security::AUTHENTICATION_ERROR;

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has($authErrorKey)) {
            $error = 'customer.login.invalid_combination';
        } elseif (null !== $session && $session->has($authErrorKey)) {
            $error = 'customer.login.invalid_combination';
            $session->remove($authErrorKey);
        } elseif (null !== $session && $session->has('jwt_error')) {
            $error = 'customer.login.jwt_error';
            $session->remove('jwt_error');
        } else {
            $error = null;
        }

        if (null !== ($site = $this->get(SiteService::class)->determineSite()) && $site->getDisableCustomerLogin() && $site->getDisableCustomerRegistration()) {
            return $this->redirectToRoute('app_home_index');
        }

        if (null !== $this->getUser() && null !== $this->getUser()->getCompany() && ($this->getUser()->getCompany()->getChamberofCommerceNumber() || $this->getUser()->getCompany()->getVatNumber())) {
            return $this->render('AppBundle:Customer/Account:company.html.twig', [
                'edit' => $this->userCanEditAccounts(),
                'orderCollections' => $this->get(CompanyManager::class)->getRecentOrderCollections($this->getUser()->getCompany())
            ]);
        }

        $targetPath = null;

        if ($loginTarget = $this->get('session')->get('login_target')) {
            $targetPath = $loginTarget['uri'];
        }

        $orders = new ArrayCollection();
        if($this->getUser()) {
            $orders = $this->get(CustomerManager::class)->getRecentOrderCollections($this->getUser());
        }

        return [
            'error' => $error,
            'targetPath' => $targetPath,
            'orderCollections' => $orders,
        ];
    }

    /**
     * @return bool
     */
    private function userCanEditAccounts()
    {
        /** @var Customer $customer */
        $customer = $this->getUser();
        $company = $customer->getCompany();

        return (!$customer->isMigrated() && $company->getCustomers()->count() <= 1);
    }
}
