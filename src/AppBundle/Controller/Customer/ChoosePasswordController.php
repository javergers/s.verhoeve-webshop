<?php

namespace AppBundle\Controller\Customer;

use FOS\UserBundle\Model\UserInterface;
use Translation\Common\Model\Message;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccountStatusException;

/**
 * @Route("/wachtwoord-instellen")
 */
class ChoosePasswordController extends Controller
{
    const SESSION_EMAIL = 'fos_user_send_resetting_email/email';

    public static function getTranslationMessages()
    {
        $messages = [
            new Message('form.new_password', 'messages'),
            new Message('form.new_password_confirmation', 'messages'),
            new Message('customer.account.change_password.success', 'messages'),
            new Message('customer.choose_password.success', 'messages'),
            new Message('customer.choose_password.error', 'validators'),
        ];

        return $messages;
    }


    /**
     * @Route("/{token}")
     * @Template()
     *
     * @param Request $request
     * @param string  $token
     *
     * @return RedirectResponse|Response
     */
    public function indexAction(Request $request, $token)
    {
        $customer = $this->get('fos_user.user_manager')->findUserByConfirmationToken($token);

        if (null === $customer) {
            throw new NotFoundHttpException(sprintf('The customer with "confirmation token" does not exist for value "%s"',
                $token));
        }

        if (null !== $customer->getPassword()) {
            $this->setFlash('frontend.error', 'customer.choose_password.error');

            return $this->render('AppBundle:Customer\ChoosePassword:index.html.twig');
        }

        $form = $this->get('fos_user.resetting.form.factory')->createForm();

        $form->setData($customer);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customer->setConfirmationToken(null);
            $customer->setPasswordRequestedAt(null);
            $customer->setEnabled(true);

            $this->get('fos_user.user_manager')->updateUser($customer);

            $this->setFlash('frontend.success', 'customer.choose_password.success');

            $response = new RedirectResponse($this->getRedirectionUrl($customer));

            $this->authenticateUser($customer, $response);

            return $response;
        }

        return $this->render('AppBundle:Customer\ChoosePassword:index.html.twig', [
            'token' => $token,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Authenticate a user with Symfony Security
     *
     * @param \FOS\UserBundle\Model\UserInterface        $customer
     * @param \Symfony\Component\HttpFoundation\Response $response
     */
    protected function authenticateUser(UserInterface $customer, Response $response)
    {
        try {
            $this->container->get('fos_user.security.login_manager')->loginUser(
                $this->container->getParameter('fos_user.firewall_name'),
                $customer,
                $response);
        } catch (AccountStatusException $ex) {
            // We simply do not authenticate users which do not pass the user
            // checker (not enabled, expired, etc.).
        }
    }

    /**
     * Generate the redirection url when the resetting is completed.
     *
     * @param \FOS\UserBundle\Model\UserInterface $customer
     *
     * @return string
     */
    protected function getRedirectionUrl(UserInterface $customer)
    {
        void($customer);

        return $this->get('router')->generate('app_customer_account_index');
    }

    /**
     * @param string $action
     * @param string $value
     */
    protected function setFlash($action, $value)
    {
        $this->container->get('session')->getFlashBag()->set($action, $value);
    }
}
