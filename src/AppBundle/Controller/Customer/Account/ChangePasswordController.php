<?php

namespace AppBundle\Controller\Customer\Account;

use AppBundle\Form\Customer\Account\ChangePasswordType;
use AppBundle\Model\ChangePassword;
use Translation\Common\Model\Message;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class OrderController
 * @package AppBundle\Controller\Customer\Account
 *
 * @Route("/account/wachtwoord-wijzigen")
 * @Security("has_role('ROLE_USER')")
 */
class ChangePasswordController extends Controller
{

    public static function getTranslationMessages()
    {
        $messages = [];

        array_push($messages, new Message("customer.account.change_password.success", "messages"));

        return $messages;
    }

    /**
     * @Route("/")
     * @Template()
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction(Request $request)
    {
        $entity = $this->getUser();

        $form = $this->createForm(ChangePasswordType::class, new ChangePassword(), [
            'action' => $this->generateUrl('app_customer_account_changepassword_index'),
            'method' => 'PUT',
        ]);

        $form->add('submit', SubmitType::class, ['label' => $this->container->get('translator')->trans('customer.account.change_password.submit')]);

        try {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                try {
                    $newPassword = $form->getData()->getNew();

                    $entity->setPlainPassword($newPassword);

                    $userManager = $this->get('fos_user.user_manager');
                    $userManager->updateUser($entity);

                    $this->get('session')->getFlashBag()->add('frontend.success',
                        'customer.account.change_password.success');

                    return $this->redirect($this->generateUrl('app_customer_account_index'));
                } catch (\Exception $e) {
                }
            }
        } catch (\Exception $e) {
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
