<?php

namespace AppBundle\Controller\Customer\Account;

use AppBundle\Entity\Relation\Address;
use AppBundle\Form\Customer\Account\AddressType;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Translation\Common\Model\Message;

/**
 * Class OrderController
 * @package AppBundle\Controller\Customer\Account
 *
 * @Route("/account/adresboek")
 * @Security("has_role('ROLE_USER')")
 */
class AddressBookController extends AbstractAccountController
{
    /**
     * @return array
     */
    public static function getTranslationMessages()
    {
        return [
            new Message('account.addressbook.new.success', 'messages'),
            new Message('account.addressbook.edit.success', 'messages'),
        ];
    }

    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        if (null !== ($user = $this->getUser()) && null !== $user->getCompany() && (null !== $user->getCompany()->getChamberofCommerceNumber() || null !== $user->getCompany()->getVatNumber())) {
            $edit = true;

            try {
                $this->checkAccessRights($user);
            } catch (\Exception $e) {
                $edit = false;
            }

            return $this->render('AppBundle:Customer/Account/AddressBook:company.html.twig', [
                'edit' => $edit,
            ]);
        }

        return [];
    }

    /**
     * @Route("/nieuw", methods={"GET"})
     * @Template()
     * @param Request $request
     * @return array
     */
    public function newAction(Request $request)
    {
        $entity = new Address();
        $form = $this->createCreateForm($entity, $request);

        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{id}/bewerken", methods={"GET"})
     * @Template("@App/Customer/Account/AddressBook/edit.html.twig")
     * @param Address $entity
     * @return array
     */
    public function editAction(Address $entity)
    {
        $this->checkAccessRights($entity);

        $editForm = $this->createEditForm($entity);

        return [
            'entity' => $entity,
            'form' => $editForm->createView(),
        ];
    }

    /**
     * @Route("/{id}/bewerken", methods={"PUT"})
     * @Template("AppBundle:Customer/Account/AddressBook:edit.html.twig")
     * @param Address $entity
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function updateAction(Address $entity, Request $request)
    {
        $this->checkAccessRights($entity);

        $em = $this->getDoctrine()->getManager();

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add('frontend.success', 'account.addressbook.edit.success');

            return $this->redirect($this->generateUrl('app_customer_account_addressbook_index'));
        }

        return [
            'entity' => $entity,
            'form' => $editForm->createView(),
        ];
    }

    /**
     * @Route("/nieuw", methods={"POST"})
     * @Template("AppBundle:Customer/Account/AddressBook:new.html.twig")
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function createAction(Request $request)
    {
        $entity = new Address();
        $form = $this->createCreateForm($entity, $request);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if ($this->getUser()->getCompany()) {
                $entity->setCompany($this->getUser()->getCompany());

                $entity->setDeliveryAddress(true);
            } else {
                $entity->setCustomer($this->getUser());

                if (\count($this->getUser()->getAddresses()) === 0) {
                    $entity->setMainAddress(true);
                } else {
                    $entity->setDeliveryAddress(true);
                }
            }

            $em->persist($entity);
            $em->flush();

            if (null !== $request->getSession() && $request->getSession()->get('incomplete_facebook_registration')) {
                $request->getSession()->remove('incomplete_facebook_registration');
            }

            $this->get('session')->getFlashBag()->add('frontend.success', 'account.addressbook.new.success');

            return $this->redirect($this->generateUrl('app_customer_account_addressbook_index'));
        }

        return [
            'entity' => $entity,
            'form' => $form->createView(),
        ];
    }


    /**
     * @Route("/{id}/standaard/{type}", requirements={"type" = "bezorging|factuur"}, methods={"GET"})
     *
     * @param Address $entity
     * @param string  $type
     * @return RedirectResponse
     */
    public function defaultAction(Address $entity, $type)
    {
        $this->checkAccessRights($entity);

        $em = $this->getDoctrine()->getManager();

        try {
            switch ($type) {
                case 'bezorging':
                    if ($this->getUser()->getCompany()) {
                        $this->getUser()->getCompany()->setDefaultDeliveryAddress($entity);
                    } else {
                        $this->getUser()->setDefaultDeliveryAddress($entity);
                    }

                    $em->flush();

                    break;
                case 'factuur':
                    $entity->getCustomer()->setDefaultInvoiceAddress($entity);

                    $em->flush();

                    break;
            }
        } catch (\Exception $e) {
        }

        return $this->redirectToRoute('app_customer_account_addressbook_index');
    }

    /**
     * Creates a form to create a Company entity.
     *
     * @param Address $entity The entity
     *
     * @param Request $request
     * @return FormInterface
     */
    private function createCreateForm(Address $entity, Request $request)
    {
        $form = $this->createForm(AddressType::class, $entity, [
            'action' => $this->generateUrl('app_customer_account_addressbook_create'),
            'method' => 'POST',
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'request' => $request,
            'is_main_address' => $entity->isMainAddress(),
            'is_invoice_address' => $entity->isInvoiceAddress(),
            'is_company' => (bool)$this->getUser()->getCompany(),
            'validation_groups' => ['Default', 'customer_registration'],
        ]);

        $form->add('submit', SubmitType::class, ['label' => $this->container->get('translator')->trans('account.addressbook.new.submit')]);

        return $form;
    }

    /**
     * Creates a form to create a Company entity.
     *
     * @param Address $entity The entity
     *
     * @return FormInterface
     */
    private function createEditForm(Address $entity)
    {
        $form = $this->createForm(AddressType::class, $entity, [
            'action' => $this->generateUrl('app_customer_account_addressbook_edit', [
                'id' => $entity->getId(),
            ]),
            'method' => 'PUT',
            'attr' => [
                'novalidate' => 'novalidate',
            ],
            'is_main_address' => $entity->isMainAddress(),
            'is_invoice_address' => $entity->isInvoiceAddress(),
            'is_company' => (bool)$this->getUser()->getCompany(),
            'validation_groups' => ['Default', 'customer_registration'],
        ]);

        $form->add('submit', SubmitType::class, ['label' => $this->container->get('translator')->trans('account.addressbook.edit.submit')]);

        return $form;
    }

    /**
     * Deletes a Address entity.
     *
     * @Route("/verwijder/{id}", methods={"GET"})
     * @param Address $entity
     * @return RedirectResponse
     */
    public function deleteAction(Address $entity)
    {
        $this->checkAccessRights($entity);

        $em = $this->getDoctrine()->getManager();

        try {
            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('frontend.success', 'account.addressbook.delete.success');

            return $this->redirect($this->generateUrl('app_customer_account_addressbook_index'));
        } catch (\Exception $e) {
        }

        $this->get('session')->getFlashBag()->add('frontend.error', 'account.addressbook.delete.failed');

        return $this->redirect($this->generateUrl('app_customer_account_addressbook_index'));
    }
}
