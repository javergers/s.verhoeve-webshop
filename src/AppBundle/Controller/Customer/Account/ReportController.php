<?php

namespace AppBundle\Controller\Customer\Account;

use AppBundle\Entity\Report\CompanyReportCustomerHistory;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Services\Report\ReportService;
use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * Class ReportController
 * @package AppBundle\Controller\Customer
 * @Route("/account/rapportages")
 */
class ReportController extends Controller
{
    /**
     * @var ReportService
     */
    protected $reportService;

    /**
     * ReportController constructor.
     * @param ReportService $reportService
     */
    public function __construct(ReportService $reportService)
    {
        $this->reportService = $reportService;
    }

    /**
     * @Route("/", methods={"GET"})
     * @Template()
     */
    public function indexAction()
    {
        /** @var Customer $customer */
        $customer = $this->getUser();

        if (null === $customer) {
            throw new AuthenticationException('Not allowed');
        }

        $reports = $this->getDoctrine()->getRepository(CompanyReportCustomerHistory::class)->findByCustomer($customer);

        return [
            'reports' => $reports,
        ];
    }

    /**
     * @Route("/{companyReportCustomerHistory}/downloaden", methods={"GET"})
     * @param CompanyReportCustomerHistory $companyReportCustomerHistory
     * @return BinaryFileResponse
     * @throws MappingException
     * @throws OptimisticLockException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \ReflectionException
     */
    public function downloadCustomerReportAction(CompanyReportCustomerHistory $companyReportCustomerHistory) {
        /** @var Customer $customer */
        $customer = $this->getUser();

        if (null === $customer || $companyReportCustomerHistory->getCompanyReportCustomer()->getCustomer() !== $customer) {
            throw new AuthenticationException('Not allowed');
        }

        $companyReport = $companyReportCustomerHistory->getCompanyReportCustomer()->getCompanyReport();

        return $this->reportService->generate($companyReport->getReport(), [
            'company' => $companyReport->getCompany()
        ]);
    }

}
