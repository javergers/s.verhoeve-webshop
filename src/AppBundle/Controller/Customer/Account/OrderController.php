<?php

namespace AppBundle\Controller\Customer\Account;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class OrderController
 * @package AppBundle\Controller\Customer\Account
 *
 * @Route("/account/bestellingen")
 * @Security("has_role('ROLE_USER')")
 */
class OrderController extends AbstractAccountController
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->getDoctrine()->getManager()->getRepository(OrderCollection::class);

        $qb = $entityRepository->createQueryBuilder("order_collection");

        if (!$this->getUser()->getCompany()) {
            $qb->andWhere($qb->expr()->orX(
                "order_collection.customer = :customer",
                "order_collection.company = :company"
            ));

            $qb->setParameter("customer", $this->getUser());
            $qb->setParameter("company", $this->getUser()->getCompany());
        } else {
            $qb->andWhere("order_collection.customer = :customer");

            $qb->setParameter("customer", $this->getUser());
        }

        $qb->setMaxResults(50);
        $qb->addOrderBy("order_collection.id", "DESC");

        $orderCollections = $qb->getQuery()->getResult();

        return [
            "orderCollections" => $orderCollections
        ];
    }

    /**
     * @Route("/{id}/bekijk", methods={"GET"})
     * @Template("AppBundle:Customer/Account/Order:view-collection.html.twig")
     *
     * @param OrderCollection $orderCollection
     * @return array
     */
    public function viewCollectionAction(OrderCollection $orderCollection)
    {
        $this->checkAccessRights($orderCollection);

        return [
            'orderCollection' => $orderCollection,
        ];
    }

    /**
     * @Route("/{orderCollection}/{order}/bekijk", methods={"GET"})
     * @Template("AppBundle:Customer/Account/Order:view-order.html.twig")
     *
     * @param OrderCollection $orderCollection
     * @param Order           $order
     * @return array
     */
    public function viewOrderAction(OrderCollection $orderCollection, Order $order)
    {
        $this->checkAccessRights($order);

        return [
            'orderOrder' => $order,
        ];
    }
}
