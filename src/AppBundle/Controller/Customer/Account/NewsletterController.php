<?php

namespace AppBundle\Controller\Customer\Account;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class OrderController
 * @package AppBundle\Controller\Customer\Account
 *
 * @Route("/account/nieuwsbrief")
 * @Security("has_role('ROLE_USER')")
 */
class NewsletterController extends Controller
{

    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        return [];
    }
}
