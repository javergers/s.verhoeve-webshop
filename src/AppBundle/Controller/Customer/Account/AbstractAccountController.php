<?php

namespace AppBundle\Controller\Customer\Account;

use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;

/**
 * Class AbstractAccountController
 * @package AppBundle\Controller\Customer\Account
 */
abstract class AbstractAccountController extends Controller
{
    /**
     * @param $entity
     */
    public function checkAccessRights($entity)
    {
        $allowed = true;
        $checkCompany = false;

        /**
         * @var Customer $customer
         */
        $customer = $this->getUser();

        if ($customer->getCompany()) {
            $checkCompany = true;
        }

        if ($entity instanceof Customer) {
            if (!$checkCompany && $customer->getId() != $entity->getId()) {
                $allowed = false;
            } else {
                if ($checkCompany && (!$entity->getCompany() || $customer->getCompany()->getId() != $entity->getCompany()->getId())) {
                    $allowed = false;
                }
            }
        } else {
            if ($entity instanceof OrderCollection || $entity instanceof Address) {
                if (!$checkCompany && $customer->getId() != $entity->getCustomer()->getId()) {
                    $allowed = false;
                } else {
                    if ($checkCompany && (!$entity->getCompany() || $customer->getCompany()->getId() != $entity->getCompany()->getId())) {
                        $allowed = false;
                    }
                }
            } else {
                if ($entity instanceof Order) {
                    if (!$checkCompany && $customer->getId() != $entity->getOrderCollection()->getCustomer()->getId()) {
                        $allowed = false;
                    } else {
                        if ($checkCompany && (!$entity->getOrderCollection()->getCompany() || $customer->getCompany()->getId() != $entity->getOrderCollection()->getCompany()->getId())) {
                            $allowed = false;
                        }
                    }
                }
            }
        }

        if (!$allowed) {
            throw new AccessDeniedException("Invalid entity access");
        }
    }
}
