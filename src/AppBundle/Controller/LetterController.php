<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Catalog\Product\Letter;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Form\Cart\LetterType;
use function mime_content_type;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/brief")
 */
class LetterController extends Controller
{
    public const METADATA_KEY = 'letter';

    /**
     * @Route("/bewerken/{cartOrderLine}/{uuid}", requirements={
     *     "cartOrderLine": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *     "uuid": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     * }, methods={"GET", "POST"})
     * @Template("@App/Letter/edit.html.twig")
     *
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     * @param Request       $request
     * @param CartOrderLine $cartOrderLine
     * @param string        $uuid
     *
     * @return array|JsonResponse
     */
    public function EditAction(Request $request, CartOrderLine $cartOrderLine, $uuid)
    {
        $product = $cartOrderLine->getProduct();

        if (!($product instanceof Letter)) {
            throw new \RuntimeException('Product is not an letter');
        }

        $form = $this->getForm($request);

        /** @var \AppBundle\Services\Letter $letter */
        $letter = $this->get('app.letter');
        $letter->setUuid($uuid);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $file */
            if ($form->has('file') && null !== ($file = $request->get($form->getName())['file'])) {
                $tmpFile = tempnam('/tmp', 'letter');

                $mimeType = mime_content_type($file);
                if ($mimeType !== 'application/pdf') {
                    return new JsonResponse(['status' => 'error', 'message' => 'Bestand is geen geldige pdf'], 403);
                }

                $prefix = 'data:application/pdf;base64,';
                $explodePrefix = \explode($prefix, $file, 2);

                if (\count($explodePrefix) !== 2 || !empty($explodePrefix[0])) {
                    return new JsonResponse(['status' => 'error', 'message' => 'Bestand is geen geldige pdf'], 403);
                }

                $file = base64_decode(\substr($file, \strlen($prefix)));

                if (!$file) {
                    return new JsonResponse(['status' => 'error', 'message' => 'Bestand is geen geldige pdf'], 403);
                }

                \file_put_contents($tmpFile, $file);

                try {
                    $letter->uploadFile($tmpFile);
                    unlink($tmpFile);

                    $metadata = $cartOrderLine->getMetadata();
                    $metadata['letter']['uuid'] = $letter->getUuid();
                    $metadata['letter']['filename'] = $letter->getFilename();
                    $metadata['letter']['path'] = $letter->getUploadPath();

                    $cartOrderLine->setMetadata($metadata);
                    $this->getDoctrine()->getManager()->flush();

                    return new JsonResponse(['status' => 'success']);
                } catch (\RuntimeException $e) {
                    return new JsonResponse(['status' => 'error', 'message' => 'Bestand kon niet worden geupload']);
                }
            } else {
                return new JsonResponse(['status' => 'error', 'message' => 'Geen brief geupload']);
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/toevoegen/{cartOrderLine}", requirements={
     *     "cartOrderLine": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     * }, methods={"GET", "POST"})
     * @Template("@App/Letter/edit.html.twig")
     *
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     * @param Request       $request
     * @param CartOrderLine $cartOrderLine
     *
     * @return array|JsonResponse
     * @throws \AppBundle\Exceptions\Sales\CartInvalidSupplierCombinationException
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function addAction(Request $request, CartOrderLine $cartOrderLine)
    {
        if (!$cartOrderLine->isLetterAvailable()) {
            throw new \RuntimeException('Letter is not available for this line');
        }

        $form = $this->getForm($request);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $file */
            if ($form->has('file') && null !== ($file = $request->get($form->getName())['file'])) {
                /** @var \AppBundle\Services\Letter $letter */
                $letter = $this->get('app.letter');
                $letter->setUuid(Uuid::uuid4());

                $tmpFile = tempnam('/tmp', 'letter');

                $prefix = 'data:application/pdf;base64,';
                $explodePrefix = \explode($prefix, $file, 2);

                if (\count($explodePrefix) !== 2 || !empty($explodePrefix[0])) {
                    return new JsonResponse(['status' => 'error', 'message' => 'Bestand is geen geldige pdf']);
                }

                $file = base64_decode(\substr($file, \strlen($prefix)));

                if (!$file) {
                    return new JsonResponse(['status' => 'error', 'message' => 'Bestand is geen geldige pdf']);
                }

                \file_put_contents($tmpFile, $file);

                try {
                    $letter->uploadFile($tmpFile);
                    unlink($tmpFile);

                    $cartOrder = $cartOrderLine->getCartOrder();

                    $metadata['letter']['uuid'] = $letter->getUuid();
                    $metadata['letter']['filename'] = $letter->getFilename();
                    $metadata['letter']['path'] = $letter->getUploadPath();

                    $letterEntity = $cartOrderLine->getProduct()->getLetter() ?? $cartOrderLine->getProduct()->getParent()->getLetter();

                    $cartOrder
                        ->addProduct($letterEntity, 1, $cartOrderLine, true)
                        ->setMetadata($metadata);


                    $this->getDoctrine()->getManager()->flush();

                    return new JsonResponse(['status' => 'success']);
                } catch (\RuntimeException $e) {
                    return new JsonResponse(['status' => 'error', 'message' => 'Bestand kon niet worden geupload']);
                }
            } else {
                return new JsonResponse(['status' => 'error', 'message' => 'Geen brief geupload']);
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\Form\FormInterface
     */
    private function getForm(Request $request) {
        return $this->createForm(LetterType::class, null, [
            'action' => $this->generateUrl($request->get('_route'), $request->get('_route_params')),
            'method' => 'POST',
            'attr' => [
                'class' => 'form',
                'novalidate' => 'novalidate',
                'id' => 'letter_form',
            ],
        ]);
    }

    /**
     * @Route("/preview/{cartOrderLine}/{uuid}", requirements={
     *     "cartOrderLine": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *     "uuid": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     * }, methods={"GET"})
     *
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     * @param CartOrderLine $cartOrderLine
     * @param string        $uuid
     *
     * @return JsonResponse
     * @throws \Exception
     * @throws \RuntimeException
     */
    public function previewAction(CartOrderLine $cartOrderLine, $uuid)
    {
        $this->checkRequirements($cartOrderLine, $uuid);

        $letter = $this->get('app.letter');
        $letter->setUuid($uuid);

        if (null === ($downloadUrl = $letter->getUrl())) {
            throw new \RuntimeException('No letter uploaded');
        }

        return new JsonResponse([
            'letterUrl' => $this->generateUrl('app_checkout_personalization', [
                'cartOrder' => $cartOrderLine->getCartOrder()->getUuid(),
                'cartOrderLine' => $cartOrderLine->getUuid(),
            ]),
            'previewUrl' => $downloadUrl,
        ]);
    }

    /**
     * @param CartOrderLine $cartOrderLine
     * @param string        $uuid
     *
     * @return bool
     * @throws \RuntimeException
     */
    private function checkRequirements(CartOrderLine $cartOrderLine, $uuid)
    {
        if ($cartOrderLine->getChildren()->isEmpty()) {
            $metadata = $cartOrderLine->getMetadata();

            return (isset($metadata[self::METADATA_KEY]) && $metadata[self::METADATA_KEY]['uuid'] === $uuid);
        }

        foreach ($cartOrderLine->getChildren() as $orderLine) {
            $metadata = $orderLine->getMetadata();

            if (isset($metadata[self::METADATA_KEY]) && $metadata[self::METADATA_KEY]['uuid'] === $uuid) {
                return true;
            }
        }

        throw new \RuntimeException("can't find required uuid");
    }
}
