<?php

namespace AppBundle\Controller\Voicedata;

use AppBundle\Services\Voicedata;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/voicedata")
 */
class VoicedataController extends Controller
{
    /**
     * @Route("/notification")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $xml = $request->getContent();

        if ($xml) {
            $voicedata = new Voicedata();
            $voicedata->setContainer($this->container);
            $voicedata->processXml($xml);
        }

        return new Response("OK");
    }
}
