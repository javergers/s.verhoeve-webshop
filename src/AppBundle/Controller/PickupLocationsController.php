<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Common\Timeslot;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Supplier\PickupLocation;
use AppBundle\Services\Company\TimeslotService;
use AppBundle\Services\SupplierManagerService;
use AppBundle\Utils\Serializer\EntitySerializer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use PDO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PickupLocationsController
 *
 * @Route("/afhaallocaties")
 *
 * @package AppBundle\Controller
 */
class PickupLocationsController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var SupplierManagerService
     */
    private $supplierManagerService;

    /**
     * @var TimeslotService
     */
    private $timeslotService;

    /**
     * PickupLocationsController constructor.
     * @param EntityManagerInterface $entityManager
     * @param SupplierManagerService $supplierManagerService
     * @param TimeslotService        $timeslotService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        SupplierManagerService $supplierManagerService,
        TimeslotService $timeslotService
    ) {
        $this->entityManager = $entityManager;
        $this->supplierManagerService = $supplierManagerService;
        $this->timeslotService = $timeslotService;
    }

    /**
     * @Route("/dichtbij/{cartOrder}", methods={"GET"})
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     *
     * @param Request   $request
     *
     * @param CartOrder $cartOrder
     * @return JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     */
    public function nearPickupLocations(Request $request, CartOrder $cartOrder): JsonResponse
    {
        $lat = $request->query->get('lat');
        $lng = $request->query->get('lng');
        $distance = $request->query->get('distance') ?: 25;
        if (null === $lat || null === $lng) {
            return new JsonResponse();
        }

        $suppliersForCartOrder = $this->supplierManagerService->getAvailableSuppliers($cartOrder, true);
        $suppliersForCartOrderIds = [];
        foreach ($suppliersForCartOrder as $key => $supplier) {
            $suppliersForCartOrderIds[] = $supplier->getId();
        }
        $suppliersForCartOrderIds = implode(',', $suppliersForCartOrderIds);

        /** @var Connection $connection */
        $connection = $this->entityManager->getConnection();

        $sql = "SELECT pilo.id AS pickupLocationId
            , ROUND((6371*acos(cos(radians(:lat1))*cos(radians(Y(addr.point)))*cos(radians(X(addr.point))-radians(:lng))+sin(radians(:lat2))*sin(radians(Y(addr.point))))), 0) AS distance
            , Y(addr.point) AS lat
            , X(addr.point) AS lng
            , comp.id AS companyId
            , comp.name AS supplierName
            , addr.street
            , addr.postcode
            , addr.city

            FROM pickup_location pilo
            INNER JOIN company_establishment AS coes ON coes.id = pilo.company_establishment_id
            INNER JOIN address AS addr ON addr.id = coes.address_id
            INNER JOIN company AS comp ON comp.id = coes.company_id
            WHERE addr.point IS NOT NULL
            AND comp.id IN (${suppliersForCartOrderIds})
            HAVING distance < :distance
            ORDER BY distance
        ";

        $statement = $connection->prepare($sql);

        $statement->bindValue(':lat1', $lat, PDO::PARAM_STR);
        $statement->bindValue(':lat2', $lat, PDO::PARAM_STR);
        $statement->bindValue(':lng', $lng, PDO::PARAM_STR);
        $statement->bindValue(':distance', $distance, PDO::PARAM_INT);

        $statement->execute();
        $suppliersInRange = $statement->fetchAll();

        return new JsonResponse($suppliersInRange);
    }

    /**
     * @Route("/vooraf-invullen/{cartOrder}", methods={"GET"})
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     *
     * @param Request   $request
     *
     * @param CartOrder $cartOrder
     * @return JsonResponse
     */
    public function prefillLocations(Request $request, CartOrder $cartOrder): JsonResponse
    {
        $suppliersForCartOrder = $this->supplierManagerService->getAvailableSuppliers($cartOrder, true);
        $pickupLocations = $this->entityManager->getRepository(PickupLocation::class)->findByCompanies($suppliersForCartOrder);
        if (count($pickupLocations) >= 5) {
            return new JsonResponse();
        }

        $serializer = new EntitySerializer([
            PickupLocation::class => [
                'id' => ['keyName' => 'pickupLocationId'],
                'companyEstablishment.name' => ['keyName' => 'supplierName'],
                'companyEstablishment.address.street' => ['keyName' => 'street'],
                'companyEstablishment.address.postcode' => ['keyName' => 'postcode'],
                'companyEstablishment.address.city' => ['keyName' => 'city'],
                'companyEstablishment.address.point.latitude' => ['keyName' => 'lat'],
                'companyEstablishment.address.point.longitude' => ['keyName' => 'lng'],
            ],
        ]);

        $json = [];
        foreach ($pickupLocations as $pickupLocation) {
            $json[] = $serializer->normalize($pickupLocation);
        }

        return new JsonResponse($json);
    }

    /**
     * @Route("/openingstijden/{cartOrder}", methods={"GET"})
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     *
     * @param Request   $request
     *
     * @param CartOrder $cartOrder
     * @return JsonResponse
     * @throws \Exception
     */
    public function openeningHoursForPickupLocation(Request $request, CartOrder $cartOrder): JsonResponse
    {
        $pickupLocationId = $request->query->get('pickupLocationId');
        if (null === $pickupLocationId) {
            return new JsonResponse();
        }
        $pickupLocations = $this->entityManager->getRepository(PickupLocation::class)->findBy(['id' => $pickupLocationId]);
        $resultJson = [];

        $startDate = new \DateTime('00:00:00');
        $endDate = (new \DateTime('23:59:59'))->modify('+14 days');
        $datePeriod = new \DatePeriod($startDate, new \DateInterval('P1D'), $endDate);

        $serializer = new EntitySerializer([
            PickupLocation::class => [
                'id',
            ],
            Timeslot::class => [
                'till' => ['type' => 'time'],
                'from' => ['type' => 'time'],
            ],
        ]);

        foreach ($pickupLocations as $pickupLocation) {
            $preparationTime = $this->timeslotService->getCalculatedPreparationTime($pickupLocation, $cartOrder);

            $locationObj = [];
            $locationObj['location'] = $serializer->normalize($pickupLocation);
            $locationObj['dates'] = [];

            /** @var \DateTime $date */
            foreach ($datePeriod as $date) {
                $dateObj = [];
                $dateObj['date'] = $date->format('c');
                $dateObj['timeslots'] = [];

                //Binary AND method
                foreach($this->timeslotService->getAvailableTimeslots($pickupLocation->getTimeslots(), $preparationTime, $date) as $timeslot) {
                    $dateObj['timeslots'][] = $serializer->normalize($timeslot);
                }

//                foreach ($pickupLocation->getTimeslots() as $timeslot) {
//                    if ($this->timeslotService->isOpenOnDay($timeslot, $preparationTime, $date)) {
//                        $dateObj['timeslots'][] = $serializer->normalize($timeslot);
//                    }
//                }

                $locationObj['dates'][] = $dateObj;
            }
            $resultJson[] = $locationObj;
        }
        return new JsonResponse($resultJson);
    }

}
