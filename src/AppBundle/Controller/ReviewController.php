<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ReviewController
 * @package AppBundle\Controller
 *
 * @Route("/beoordelingen")
 */
class ReviewController extends Controller
{
    /**
     * @Route("/", methods={"GET"})
     * @Template
     * @Cache(expires="+1 days", public=true)
     */
    public function indexAction()
    {
        $page = $this->get('app.kiyoh')->getCompany()->getPage();

        if ($page) {
            $page = $this->get('app.page_decorator')->setPage($page);
        }

        return [
            'page' => $page
        ];
    }
}
