<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Site\Page;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Ekino\NewRelicBundle\NewRelic\NewRelicInteractorInterface;
use Liip\ThemeBundle\Twig\Loader\FilesystemLoader;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\TwigBundle\Controller\ExceptionController as DefaultExceptionController;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\IpUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Twig\Environment;

/**
 * Class ExceptionController
 * @package AppBundle\Controller
 */
class ExceptionController extends Controller
{
    /**
     * @var Environment
     */
    protected $twig;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var NewRelicInteractorInterface
     */
    private $newRelicInteractor;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * @var array
     */
    private const BYPASSES = [
        '/media/cache/resolve',
        '/voicedata',
        '/temp-api',
    ];

    /**
     * @param Environment                 $twig
     * @param EntityManagerInterface      $entityManager
     * @param NewRelicInteractorInterface $newRelicInteractor
     * @param ParameterBagInterface       $parameterBag
     */
    public function __construct(
        Environment $twig,
        EntityManagerInterface $entityManager,
        NewRelicInteractorInterface $newRelicInteractor,
        ParameterBagInterface $parameterBag
    ) {
        $this->twig = $twig;
        $this->entityManager = $entityManager;
        $this->newRelicInteractor = $newRelicInteractor;
        $this->parameterBag = $parameterBag;
    }

    /**
     * @param Request                   $request
     * @param FlattenException          $exception
     * @param DebugLoggerInterface|null $logger
     *
     * @return Response
     */
    public function showAction(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null)
    {
        $errorReference = $this->generateErrorReference();

        $debug = $this->isBypassed($request);

        $exceptionController = new DefaultExceptionController($this->twig, $debug);

        $page = $this->getPage();

        if ($page === null || $debug) {
            return $exceptionController->showAction($request, $exception, $logger);
        }

        try {
            return new Response($this->twig->render($this->getTemplate($request, $exception), [
                'page' => $page,
                'errorReference' => $errorReference,
            ]));
        } catch (\Twig_Error $e) {
            return $exceptionController->showAction($request, $exception, $logger);
        }
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function isBypassed(Request $request)
    {
        foreach (self::BYPASSES as $excludedUri) {
            if (strpos($request->getRequestUri(), $excludedUri) === 0) {
                return true;
            }
        }

        if ($this->parameterBag->get('kernel.debug')) {
            return true;
        }

        if ($request->getSession() !== null && $request->getSession()->get('_debug')) {
            return true;
        }

        foreach (self::BYPASSES as $excludedUri) {
            if (strpos($request->getRequestUri(), $excludedUri) === 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return Page|null
     */
    private function getPage(): ?Page
    {
        $qb = $this->entityManager->getRepository(Page::class)->createQueryBuilder('page');
        $qb->select(['page', 'page_translation']);
        $qb->leftJoin('page.translations', 'page_translation');
        $qb->andWhere('page_translation.slug = :slug');
        $qb->setParameter('slug', 'foutmelding');

        try {
            return $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $exception) {
            return null;
        }
    }

    /**
     * @param Request          $request
     * @param FlattenException $exception
     * @return string
     */
    private function getTemplate(Request $request, FlattenException $exception)
    {
        if (strpos($request->getRequestUri(), '/admin/') !== false) {
            $template = 'AdminBundle:Exception:error.html.twig';
        } else {
            $template = 'AppBundle:Exception:error' . $exception->getStatusCode() . '.html.twig';
        }

        /** @var FilesystemLoader $loader */
        $loader = $this->twig->getLoader();

        if (!$loader->exists($template)) {
            $template = 'AppBundle:Exception:error.html.twig';
        }

        return $template;
    }

    /**
     * @return string
     */
    private function generateErrorReference()
    {
        $errorReference = Uuid::uuid4()->toString();

        $this->newRelicInteractor->addCustomParameter('error_reference', $errorReference);

        return $errorReference;
    }
}
