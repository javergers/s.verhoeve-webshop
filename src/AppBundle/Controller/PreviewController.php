<?php

namespace AppBundle\Controller;

use AppBundle\Exceptions\Preview\PreviewInvalidDataException;
use AppBundle\Exceptions\Preview\PreviewNotActiveException;
use Exception;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PreviewController
 *
 * @package AppBundle\Controller
 *
 * @Route("/preview")
 */
class PreviewController extends Controller
{
    /**
     * @Route("/", methods={"GET"})
     * @Template
     *
     * @return array
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * @Route("/inner", methods={"GET"})
     *
     * @return Response
     *
     * @throws PreviewInvalidDataException
     * @throws PreviewNotActiveException
     * @throws Exception
     */
    public function innerAction()
    {
        return $this->get('preview')->render();
    }

    /**
     * @Route("/niet-beschikbaar", methods={"GET"})
     * @Template
     *
     * @return array
     *
     * @throws PreviewNotActiveException
     */
    public function blockedAction()
    {
        if (!$this->get('preview')->isActive()) {
            throw new PreviewNotActiveException();
        }

        return [];
    }
}
