<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Site\Page;
use AppBundle\Services\SiteService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PageController
 * @package AppBundle\Controller
 */
class PageController extends Controller
{
    /**
     * @Template
     * @Cache(expires="+1 days", public=true)
     * @param Page $page
     * @return array|RedirectResponse|Response
     */
    public function indexAction(Page $page)
    {
        $site = $this->container->get(SiteService::class)->determineSite();
        // If homepage get's called by the slug, redirect to /
        if (null === $site || (null === $site->getParent() && $page === $site->getHomepage())) {
            return $this->redirectToRoute('app_home_index', [], 301);
        }

        if ($page === $site->getHomepage()) {
            return $this->forward('AppBundle:Home:index');
        }

        $decoratedPage = null;
        if ($page) {
            $decoratedPage = $this->get('app.page_decorator')->setPage($page);
        }

        if(null !== $site) {
            //set main menu and site id in session to determine menu's
            $session = $this->container->get('session');
            $session->set('site_id', $site->getId());

            if(false === $site->getMainMenu()->isEmpty()) {
                $session->set('menu_id', $site->getMainMenu()->current()->getId());
            }
        }

        return [
            'page' => $decoratedPage ?: $page,
        ];
    }
}
