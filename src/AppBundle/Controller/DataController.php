<?php

namespace AppBundle\Controller;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DataController
 * @package AppBundle\Controller
 *
 * @Route("/data")
 */
class DataController extends Controller
{
    /**
     * @Route("/postcode", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws \RuntimeException
     * @throws GuzzleException
     */
    public function postcodeAction(Request $request)
    {
        if (!$request->get('postcode')) {
            throw new \RuntimeException("Missing parameter 'postcode'");
        }

        if (!$request->get('number')) {
            throw new \RuntimeException("Missing parameter 'number'");
        }

        if (!$request->get('country')) {
            throw new \RuntimeException("Missing parameter 'country'");
        }

        if (strtolower($request->get('country')) !== 'nl') {
            throw new \RuntimeException('Invalid request');
        }

        try {
            $result = $this->get('postcode_nl')->getAddress($request->get('postcode'), $request->get('number'),
                $request->get('numberAddition'));
        } catch (Exception $e) {
            $result = [];
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/city-and-postcode", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function cityPostcodeAction(Request $request)
    {
        $response = new JsonResponse();
        $response->setData($this->get('autocomplete.postcode_city')->get($request->get('q'), $request->get('limit')));

        return $response;
    }

    /**
     * @Route("/kvk", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws \RuntimeException
     */
    public function cocAction(Request $request)
    {
        if (!$request->get('number')) {
            throw new \RuntimeException("Missing parameter 'number'");
        }

        return new JsonResponse([]);
    }
}
