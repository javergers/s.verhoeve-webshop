<?php

namespace AppBundle\Controller\TempApi;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\FilterCollection;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/temp-api")
 */
class BaseController extends Controller
{
    /** @var FilterCollection */
    private $filters;

    /**
     * @param Request $request
     * @return null|JsonResponse
     */
    protected function validateRequest(Request $request)
    {
        $authToken = $this->container->getParameter('temp_api_authorization_token');

        if ($request->headers->get('Authorization') !== $authToken) {
            return new JsonResponse('Invalid requests', 401);
        }

        return null;
    }

    /**
     * @return void
     */
    protected function disableDoctrineAccessibleFilter(): void
    {
        $this->getDoctrineFilters()->disable('accessible');
    }

    /**
     * @return void
     */
    protected function disableDoctrinePublishableFilter(): void
    {
        $this->getDoctrineFilters()->disable('publishable');
    }

    /**
     * @return FilterCollection
     */
    protected function getDoctrineFilters(): FilterCollection
    {
        if(null !== $this->filters) {
            return $this->filters;
        }

        /** @var EntityManagerInterface $em */
        $em = $this->getDoctrine()->getManager();

        /** @var FilterCollection $filters */
        return $this->filters = $em->getFilters();
    }
}

