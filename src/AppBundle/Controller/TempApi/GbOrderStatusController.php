<?php

namespace AppBundle\Controller\TempApi;

use AppBundle\Services\TempAPI\OrderStatusService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/temp-api")
 */
class GbOrderStatusController extends BaseController
{
    /**
     * @Route("/gb-order-status/update", methods={"GET", "POST"})
     * @param Request            $request
     * @return string
     */
    public function update(Request $request)
    {
        $orderStatusService = $this->container->get(OrderStatusService::class);

        // header check
        if ($this->validateRequest($request)) {
            return new JsonResponse([
                'status' => 'failed',
                'message' => 'Invalid request',
            ], 500);
        }

        try {
            $orderStatusService->process($request->getContent());

            return new JsonResponse([
                'status' => 'success',
            ]);

        } catch (\Exception $exception) {
            return new JsonResponse([
                'status' => 'failed',
                'messsage' => $exception->getMessage(),
            ], 500);
        }
    }
}


