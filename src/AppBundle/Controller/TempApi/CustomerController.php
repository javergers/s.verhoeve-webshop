<?php

namespace AppBundle\Controller\TempApi;

use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Services\Convert\CustomerService;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/temp-api")
 */
class CustomerController extends BaseController
{
    /** @var Request $request */
    private $request;

    /**
     * @Route("/customer/convert", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function postAction(Request $request)
    {

        $this->request = $request;

        $this->container->get("doctrine")->getConnection()->beginTransaction();

        try {

            if ($this->validateRequest($request)) {
                throw new \Exception('Invalid request');
            }

            $customer = $this->convert();

            // Commit entities to the database.
            $this->getDoctrine()->getManager()->commit();

            // Fix for event listener (onFlush).
            $customer->setUpdatedAt(new \DateTime('now'));
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse([
                'status' => 'success',
                'data' => $this->getData($customer),
            ]);
        } catch (\Exception $e) {

            $this->container->get("doctrine")->getConnection()->rollBack();

            $data = [
                'status' => 'failed',
                'message' => (string)$e->getMessage(),
            ];

            $code = ($e->getCode() ? $e->getCode() : 500);

            return new JsonResponse($data, $code);
        }
    }

    /**
     * @Route("/customer/verify-account", methods={"POST"})
     * @param Request $request
     * @return bool|JsonResponse
     */
    public function verifyAccountAction(Request $request)
    {

        $userManager = $this->get('fos_user.user_manager');
        $factory = $this->get('security.encoder_factory');

        try {

            if ($this->validateRequest($request)) {
                throw new \Exception('Invalid request');
            }

            $data = $this->getRawData();

            if (empty($data->username) || empty($data->password)) {
                throw new \Exception('User credentials missing');
            }

            $user = $userManager->findUserBy(['username' => $data->username]);

            if (!$user) {
                throw new \Exception('User not found');
            }

            $encoder = $factory->getEncoder($user);
            $bool = ($encoder->isPasswordValid($user->getPassword(), $data->password, $user->getSalt())) ? true : false;

            if (!$bool) {
                throw new \Exception('Invalid credentials');
            }

            return new JsonResponse($this->getData($user));
        } catch (\Exception $e) {
            return new JsonResponse(['status' => 'failed', 'message' => $e->getMessage()], 500);
        }

        return new JsonResponse(['status' => 'failed'], 500);
    }

    /**
     * @Route("/customer/{customer}", methods={"GET"})
     * @param Customer $customer
     * @return JsonResponse
     * @internal param null $parameters
     */
    public function getAction(Customer $customer)
    {

        try {
            $json = $this->get(CustomerService::class)
                ->getJsonData($customer, true);
        } catch (\Exception $e) {
            return new JsonResponse('Failed', 500);
        }

        return new JsonResponse($json, 200);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    private function getRawData()
    {

        $rawJson = file_get_contents('php://input');
        $rawData = json_decode($rawJson);

        if (empty($rawData)) {
            throw new \Exception('Invalid data posted', 402);
        }

        return $rawData;
    }

    /**
     * @return Customer
     */
    private function convert()
    {
        $rawData = $this->getRawData();

        $customer = $this->convertCustomer($rawData);

        if ($customer->getCompany() && $customer->getCompany()->getAddresses()) {
            $addresses = $customer->getCompany()->getAddresses();

            $invoiceAddress = $addresses->filter(function (Address $address) {
                return $address->isInvoiceAddress();
            })->current();

            if ($invoiceAddress) {
                $customer->setDefaultInvoiceAddress($invoiceAddress);
            }
        }

        return $customer;
    }

    /**
     * @param null|\stdClass $rawData
     * @return Customer
     * @throws \Exception
     */
    private function convertCustomer(\stdClass $rawData = null)
    {
        $convert = $this->get(CustomerService::class);
        $rawData = $convert->validateRawData($rawData);

        try {
            $customer = $convert->createOrUpdate((array)$rawData);
        } catch (\Exception $e) {
            throw new \Exception('Can\'t convert customer data', 422);
        }

        return $customer;
    }

    /**
     * @param Customer|null $customer
     * @return array
     * @throws \Exception
     * @internal param $company
     */
    private function getData(Customer $customer = null)
    {

        if (is_null($customer)) {
            throw new \Exception('No customer data found.');
        }

        $customer = $this->container->get(CustomerService::class)
            ->getJsonData($customer);

        return [
            'customer' => $customer,
        ];
    }
}

