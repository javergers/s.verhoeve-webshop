<?php

namespace AppBundle\Controller\TempApi;

use AppBundle\Entity\Catalog\Product\Personalization;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Order\OrderLineVat;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\Paymentmethod;
use AppBundle\Entity\Payment\PaymentStatus;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyRegistration;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Site;
use AppBundle\Exceptions\InvalidBuildingNumberException;
use AppBundle\Exceptions\TempApiException;
use AppBundle\Manager\Order\OrderLineManager;
use AppBundle\Services\Designer\DesignGeneratorService;
use AppBundle\Services\Finance\Tax\VatGroupService;
use AppBundle\Services\SupplierManagerService;
use AppBundle\Services\TransportTypeHelper;
use AppBundle\Utils\BuildingNumber;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use const PHP_EOL;
use function sprintf;
use stdClass;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/temp-api")
 */
class GbOrderCollectionController extends BaseController
{
    /** @var object */
    private $data;

    /** @var bool */
    private $testing = false;

    /** @var array */
    private $products = [];

    /** @var array */
    private $countries = [];

    /** @var array */
    private $errorNotifications = [];

    /** @var EntityManagerInterface $em */
    private $em;

    /**
     * @Route("/post/order")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function processAction(Request $request)
    {
        $exception = null;
        $data = [];
        $this->em = $this->getDoctrine()->getManager();

        // header check
        if (!$this->testing && $this->validateRequest($request)) {
            $data = [
                'status' => 'failed',
                'message' => 'Request not allowed.',
            ];

            return new JsonResponse($data, 401);
        }

        // Disable accessibleFilter for product import.
        $this->disableDoctrineAccessibleFilter();

        $this->getDoctrine()->getConnection()->beginTransaction();

        try {
            $orderCollection = $this->convert();

            $this->createPayment($orderCollection);
            $this->em->flush();

            $this->getDoctrine()->getConnection()->commit();

            $data = [
                'id' => $orderCollection->getNumber(),
                'status' => 'succes',
            ];

        } catch (TempApiException $tempApiException) {
            $exception = new Exception($tempApiException->getMessage(), 422, $tempApiException);
        } catch (Exception $exception) {
            $exception = new Exception("Verzoek kan niet worden verwerkt.", 422, $exception);
        }

        if ($exception) {

            // Reset database changes.
            $this->getDoctrine()->getConnection()->rollBack();

            // Try to send the error mail.
            $this->sendErrorMail(
                'helpdesk@topgeschenken.nl',
                'Temp-api order import from Geschenkbezorgen.nl failed',
                $this->getErrorMailBody($exception->getPrevious())
            );

            $code = 422;
            if ($exception->getCode()) {
                $code = $exception->getCode();
            }

            return new JsonResponse([
                'status' => 'failed',
                'message' => $exception->getMessage(),
            ], $code);
        }

        return new JsonResponse($data);
    }

    /**
     * @return mixed
     * @throws TempApiException
     */
    private function getRawData()
    {
        $json = file_get_contents('php://input');

        if ($this->testing) {
            $json = '{"id":"'.time().'","bezorgdatum":"2019-05-01","opmerking_bakker":"","kvk":"5837653","ip":"37.153.226.157","totaalprijs":"35.80","totaalprijs_incl":"38.85","adressen":[{"id":"30008","naam":"Topgeschenken nederland BV","contactpersoon":"Lars","straat":"Noorderdreef","huisnummer":"60","postcode":"2153LL","plaats":"Nieuw-Vennep","totaalprijs":"35.80","totaalprijs_incl":"38.85","producten":[{"id":"27078","product_id":"PIE10002","aantal":2,"type":"product","naam":"Choco Telegram Thanks - Standaard","prijs_per_stuk":"9.95","btw_tarief":9}]}]}';

        }

        if (empty($json)) {
            throw new TempApiException('Invalid json', 422);
        }

        return json_decode($json);
    }

    /**
     * @param stdClass $data
     *
     * @return object
     */
    private function validateData($data)
    {
        /* Validate data */
        $orderCollectionResolver = new OptionsResolver();
        $orderCollectionResolver->setDefault('ip', null);
        $orderCollectionResolver->setRequired([
            "adressen",
            "bezorgdatum",
            "kvk",
            "id",
            "opmerking_bakker",
            "totaalprijs",
            "totaalprijs_incl",
        ]);

        $orderCollectionResolver->setAllowedTypes('adressen', 'array');

        // Validate as array return object.
        return (object)$orderCollectionResolver->resolve((array)$data);
    }

    /**
     * @param stdClass $address
     *
     * @return object
     */
    private function validateAddress($address)
    {
        /* Validate data */
        $addressResolver = new OptionsResolver();

        $addressResolver->setRequired([
            "contactpersoon",
            "huisnummer",
            "id",
            "plaats",
            "postcode",
            "producten",
            "straat",
            "totaalprijs",
            "totaalprijs_incl",
        ]);

        $addressResolver->setDefault('naam', null);
        $addressResolver->setDefault('telefoonnummer', null);
        $addressResolver->setAllowedTypes('producten', 'array');

        return (object)$addressResolver->resolve((array)$address);
    }

    /**
     * @param stdClass $product
     *
     * @return object
     */
    private function validateProduct($product)
    {
        /* Validate data */
        $productResolver = new OptionsResolver();
        $productResolver->setRequired([
            "aantal",
            "type",
            "naam",
            "prijs_per_stuk",
            "btw_tarief",
        ]);

        $productResolver->setDefined([
            "id",
            "product_id",
            "parent_id",
            "afbeelding_url",
            "afbeelding_tekst",
            "brief_url",
            "kaart_thema_url",
            "kaart_logo",
            "kaart_tekst",
        ]);

        $productResolver->setAllowedTypes('product_id', 'string');

        if (in_array($product->type, ['product'])) {
            $productResolver->setRequired([
                "id",
            ]);
        }

        return (object)$productResolver->resolve((array)$product);
    }

    /**
     * @return OrderCollection
     * @throws Exception
     */
    private function convert()
    {
        // Validate data.
        $this->data = $this->validateData($this->getRawData());

        // block if an Order already exist. check by invoiceReference(s).
        $this->blockIfOrderOrderExist();

        $orderCollection = new OrderCollection();

        $orderCollection->setSite($this->getSite());

        // Set company to the OrderCollection.
        $company = $this->getCompanyByChamberOfCommerceNumber($this->data->kvk);
        $orderCollection->setCompany($company);

        // Set customer to the OrderCollection.
        $customer = $this->getFirstCustomerOfCompany($company);
        $orderCollection->setCustomer($customer);

        // Set InvoiceAddress to the OrderCollection
        $invoiceAddress = $this->getCompanyInvoiceAddress($company);
        $orderCollection->setInvoiceAddress($invoiceAddress);

        // Set OrderOrders to the OrderCollection
        $this->getOrders($orderCollection);

        $this->em->persist($orderCollection);
        $this->em->flush();

        return $orderCollection;
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @throws Exception
     */
    private function createPayment(OrderCollection $orderCollection)
    {
        /** @var Paymentmethod $paymentMethod */
        $paymentMethod = $this->getDoctrine()->getRepository(Paymentmethod::class)->findOneBy([
            'code' => 'internal',
        ]);

        /** @var PaymentStatus $paymentStatus */
        $paymentStatus = $this->getDoctrine()->getRepository(PaymentStatus::class)->find('authorized');

        if ($paymentMethod && $paymentStatus) {
            $payment = new Payment();
            $payment->setPaymentmethod($paymentMethod);
            $payment->setStatus($paymentStatus);
            $payment->setDatetime(new DateTime());
            $payment->setOrderCollection($orderCollection);
            $payment->setAmount($orderCollection->getPaymentAmount());

            $orderCollection->addPayment($payment);

            foreach ($orderCollection->getOrders() as $order) {
                $order->setStatus('new');
            }

            $this->getDoctrine()->getManager()->persist($payment);
        }
    }

    /**
     * @return array
     */
    private function getInvoiceReferences()
    {
        $invoiceReferences = [];
        foreach ($this->data->adressen as $address) {
            $invoiceReferences[] = $this->data->id . '-' . $address->id;
        }

        return $invoiceReferences;
    }

    /**
     * @throws TempApiException
     */
    private function blockIfOrderOrderExist()
    {
        $invoiceReferences = $this->getInvoiceReferences();

        foreach ($invoiceReferences as $invoiceReference) {
            /** @var Order $order */
            $order = $this->em
                ->getRepository(Order::class)
                ->findOneByInvoiceReference($invoiceReference);

            if ($order && $order->getStatus() !== 'cancelled') {
                throw new TempApiException(sprintf('Order with invoiceReference \'%s\' already exist.',
                    $invoiceReference));
            }
        }
    }

    /**
     * @throws TempApiException
     */
    private function prefetchProducts()
    {
        $productSkus = [];
        foreach ($this->data->adressen as $adres) {
            foreach ($adres->producten as $product) {
                $productSkus[] = $product->product_id;
            }
        }
        $productRepository = $this->em->getRepository(Product::class);
        $products = $productRepository->findBy([
            'sku' => array_unique($productSkus),
        ]);

        foreach ($products as $product) {
            $this->products[$product->getSku()]['product'] = $product;
            $this->products[$product->getSku()]['suppliers'] = $this->getProductSupplierIds($product);
        }
    }

    /**
     * Get OrderOrders based on addresses and product suppliers.
     *
     * @param $orderCollection
     *
     * @return array
     * @throws Exception
     * @throws TempApiException
     */
    private function getOrders(OrderCollection $orderCollection)
    {
        $orders = [];

        if (count($this->data->adressen) === 0) {
            throw new TempApiException('No `OrderOrder` to create');
        }

        $this->prefetchProducts();

        foreach ($this->data->adressen as $adres) {

            $adres = $this->validateAddress($adres);

            $lines = $this->getOrderLines($adres->producten);

            if (!$lines) {
                throw new TempApiException('No order lines to import');
            }

            // Split lines into multple OrderOrders
            $splittedLineGroups = $this->splitLinesBySupplier($lines);

            if (count($splittedLineGroups) === 0) {
                throw new TempApiException('No `OrderOrder` to create (after regroup `OrderOrderLines`)');
            }

            foreach ($splittedLineGroups as $splittedLines) {

                $order = new Order();
                $order->setOrderCollection($orderCollection);
                $orderCollection->addOrder($order);
                $this->em->persist($order);

                // Set InvoiceReference.
                $order->setInvoiceReference($this->data->id . '-' . $adres->id);
                $order->setStatus('payment_pending'); // @todo: workflow refactor
                $order->setDeliveryDate(new DateTime($this->data->bezorgdatum));
                $order->setDeliveryAddressCompanyName($adres->naam);
                $order->setDeliveryAddressAttn($adres->contactpersoon);

                try {
                    $addressNumberParts = BuildingNumber::split($adres->huisnummer);
                } catch (InvalidBuildingNumberException $exception) {
                    throw new TempApiException('Could not parse building number of delivery address.');
                }

                $order->setDeliveryAddressStreet($adres->straat);
                $order->setDeliveryAddressNumber($addressNumberParts['number']);
                $order->setDeliveryAddressNumberAddition($addressNumberParts['numberAddition']);
                $order->setDeliveryAddressPostcode(preg_replace('/\s+/', '', $adres->postcode));
                $order->setDeliveryAddressCity($adres->plaats);

                if ($adres->telefoonnummer) {
                    $order->setDeliveryAddressPhoneNumber($adres->telefoonnummer);
                }

                $order->setDeliveryAddressCountry($this->getCountry('NL'));
                $order->setSupplierRemark($this->data->opmerking_bakker);

                $order->setLines($splittedLines);

                foreach ($order->getLines() as $line) {
                    foreach ($line->getChildren() as $child) {
                        $child->setOrder($order);
                    }
                    $line->setOrder($order);
                }

                if(!$order->getDeliveryLine()) {
                    //todo SF4
                    $transportType = $this->container->get(TransportTypeHelper::class)->getTransportType($order);

                    if(null === $transportType) {
                        throw new TempApiException('Could not determine transport type');
                    }

                    $orderLine = new OrderLine();
                    $orderLine->setOrder($order);
                    $orderLine->setProduct($transportType);
                    $orderLine->setLedger($transportType->getProductgroup()->getLedger());
                    $orderLine->setTitle($transportType->getDisplayName());
                    $orderLine->setPrice($transportType->getPrice());
                    $orderLine->setQuantity(1);

                    //todo SF4
                    $this->container->get(OrderLineManager::class)->createVatLines($orderLine);

                    $this->em->persist($orderLine);
                }

                //todo SF4
                $errors = $this->container->get('validator')->validate($order);
                if($errors->count()) {
                    $errorText = '';
                    foreach($errors as $error) {
                        $errorText .= $error.PHP_EOL;
                    }

                    throw new TempApiException($errorText);
                }

                $orders[] = $order;

            }
            $this->em->flush();
        }

        return $orders;
    }

    /**
     * @param $productData
     *
     * @return OrderLine
     * @throws Exception
     */
    private function generateOrderLine($productData)
    {
        $line = new OrderLine();

        $line->setQuantity($productData->aantal);

        $product = $this->getProductBySku($productData->product_id);

        $productGroup = $product->getProductgroup(true);

        $productTitle = $product->translate()->getTitle();

        // Check if product is published.
        if ($product->getParent() && !$product->getParent()->isPublished()) {
            $message = sprintf('Let op: Bovenliggend product "%s" van "%s" is niet gepubliceerd.',
                $product->getParent()->translate()->getTitle(), $productTitle);
            $this->errorNotifications[] = $message;
        } elseif (!$product->isPublished()) {
            $message = sprintf('Let op: Product "%s" is niet gepubliceerd.', $productTitle);
            $this->errorNotifications[] = $message;
        }

        if (null === $productGroup) {
            $this->errorNotifications[] = sprintf('Product "%s" heeft geen product groep', $productTitle);
        }

        if (count($this->errorNotifications) > 0) {
            throw new TempApiException(implode(',', $this->errorNotifications), 422);
        }

        switch ($productData->type) {
            case 'extra':
            case 'product':
                if (!$product) {
                    throw new TempApiException(sprintf('Product "%s" not found.',
                        $productData->naam . ' - (' . $productData->product_id . ')'));
                }

                $line->setProduct($product);
                $line->setLedger($product->getProductgroup(true)->getLedger());

                break;
            case 'bezorgkosten':
                if (!$product) {
                    throw new TempApiException(sprintf('DeliveryCost "%s" not found.',
                        $productData->naam . ' - (' . $productData->product_id . ')'));
                }

                $line->setProduct($product);
                $line->setLedger($product->getProductgroup(true)->getLedger());

                break;
            case 'afbeelding':
                if (!$product) {
                    throw new TempApiException(sprintf('Design "%s" not found.',
                        $productData->naam . ' - (' . $productData->product_id . ')'));
                }

                if(empty($productData->afbeelding_url)) {
                    throw new TempApiException(sprintf('Missing required personalization image for product "%s".',
                        $productData->naam . ' - (' . $productData->product_id . ')'));
                }

                $line->setProduct($product);
                $line->setLedger($product->getProductgroup(true)->getLedger());

                $line->setMetadata([
                    'design_url' => $productData->afbeelding_url,
                    'is_design' => true,
                    'is_gb' => true,
                ]);

                break;
            case 'brief':
                if (!$product) {
                    throw new TempApiException(sprintf('Letter "%s" not found.',
                        $productData->naam . ' - (' . $productData->product_id . ')'));
                }

                $line->setProduct($product);
                $line->setLedger($product->getProductgroup(true)->getLedger());

                if (!empty($productData->brief_url)) {
                    $line->setMetadata([
                        'letter_url' => $productData->brief_url,
                        'is_letter' => true,
                        'is_gb' => true,
                    ]);
                }

                break;
            case 'kaart':
                if (!$product) {
                    throw new TempApiException(sprintf('Card product "%s" not found.',
                        $productData->naam . ' - (' . $productData->product_id . ')'));
                }

                $line->setProduct($product);
                $line->setLedger($product->getProductgroup(true)->getLedger());

                $metadata = [];
                if (!empty($productData->kaart_tekst)) {
                    $metadata['text'] = $productData->kaart_tekst;
                    $metadata['is_card'] = true;
                    $metadata['is_gb'] = true;
                }
                if (!empty($productData->kaart_thema_url)) {
                    $metadata['theme'] = $productData->kaart_thema_url;
                }

                if (isset($metadata['is_card'])) {

                    if (!empty($productData->kaart_logo) &&
                        isset($productData->kaart_logo->url) &&
                        isset($productData->kaart_logo->positie) &&
                        isset($productData->kaart_logo->breedte) &&
                        isset($productData->kaart_logo->hoogte)
                    ) {

                        $meta_logo = new stdClass();

                        $meta_logo->url = $productData->kaart_logo->url;
                        $meta_logo->position = $productData->kaart_logo->positie;
                        $meta_logo->width = $productData->kaart_logo->breedte;
                        $meta_logo->height = $productData->kaart_logo->hoogte;
                        $meta_logo->margin_x = $productData->kaart_logo->margin_x;
                        $meta_logo->margin_y = $productData->kaart_logo->margin_y;

                        $metadata['logo'] = $meta_logo;
                    }
                }

                $line->setMetadata($metadata);

                break;

            case "kledingopdruk":
                if (!$product || !$product instanceof Personalization) {
                    throw new TempApiException(sprintf('Personalization for clothing print "%s" not found.',
                        $productData->naam . ' - (' . $productData->product_id . ')'));
                }

                if(empty($productData->afbeelding_url) && empty($productData->afbeelding_tekst)) {
                    throw new TempApiException(sprintf('Missing required personalization property ("afbeelding_url" or "afbeelding_tekst") for product "%s".',
                        $productData->naam . ' - (' . $productData->product_id . ')'));
                }

                $line->setProduct($product);
                $line->setLedger($product->getProductgroup(true)->getLedger());

                $designGeneratorService = $this->get(DesignGeneratorService::class);

                $designGeneratorService->setPersonalization($product);

                $props = [];

                if(!empty($productData->afbeelding_url)) {
                    $props['image'] = [
                        'left' => 50,
                        'top' => 35,
                        'width' => 60, // scaled
                        'height' => 40, // scaled
                        'class' => 'free-image',
                        'fileId' => 1,
                        'src' => $productData->afbeelding_url
                    ];
                }

                if(!empty($productData->afbeelding_tekst)) {
                    $props['text'] = [
                        'left' => 50,
                        'top' => 40,
                        'width' => 100,
                        'height' => 1,
                        'class' => 'elm-text',
                        'text' => $productData->afbeelding_tekst,
                    ];

                    if(!empty($productData->afbeelding_url)) {
                        $props['text']['top'] = 70;
                        $props['text']['originY'] = 'top';
                    }
                }

                $designGeneratorService->generate($props);

                $line->setMetadata([
                    'designer' => $designGeneratorService->getUuid(),
                    'is_design'  => true,
                    'is_gb'      => true,
                ]);

                break;
            case "tekst":
                throw new TempApiException("Kan order niet verwerken, order bevat een tekst als personalisatie.");
                break;
        }

        if ($product) {
            $parentProduct = $product->getParent();

            $line->setTitle($product->getDisplayName());

            if ($product->getPrice()) {
                $line->setPrice($product->getPrice());
            } else {
                if ($parentProduct && $parentProduct->getPrice()) {
                    $line->setPrice($parentProduct->getPrice());
                } else {
                    // Product with variable price, copy from input
                    // @todo: check if price matches price range for product
                    $line->setPrice($productData->prijs_per_stuk);
                }
            }

            $vatGroupService = $this->container->get('app.vat_group.service');
            $vatGroup = $vatGroupService->determineVatGroupForProduct($line->getProduct());

            if (null !== $vatGroup) {
                $vatRate = $vatGroup->getRateByDate();

                $orderLineVat = new OrderLineVat();
                $orderLineVat->setOrderLine($line);
                $orderLineVat->setVatRate($vatRate);
                $orderLineVat->setAmount($line->getPrice() * $line->getQuantity());
                $line->addVatLine($orderLineVat);
            }

            if ($line->getVatLines()->isEmpty()) {
                throw new TempApiException("Instellingsfout op product: BTW tarief is niet ingesteld.");
            }
        }

        return $line;
    }

    /**
     * @param array $producten
     *
     * @return array
     * @throws Exception
     */
    private function getOrderLines(array $producten = [])
    {
        $lines = [];

        // Loop trough product childeren
        foreach ($producten as $product) {

            if (isset($product->parent_id)) {
                continue;
            }

            $product = (object)$this->validateProduct($product);

            $parentOrderLine = $this->generateOrderLine($product);

            foreach ($producten as $childProduct) {

                if (!isset($childProduct->product_id)) {
                    $childProduct->product_id = null;
                }

                if (!isset($childProduct->parent_id)) {
                    continue;
                }

                if (!isset($product->id)) {
                    continue;
                }

                if (isset($product->id) && $product->id !== $childProduct->parent_id) {
                    continue;
                }

                $childProduct = (object)$this->validateProduct($childProduct);

                $childOrderLine = $this->generateOrderLine($childProduct);

                // Set relation.
                $parentOrderLine->addChild($childOrderLine);

                $lines[] = $childOrderLine;
            }

            $lines[] = $parentOrderLine;
        }

        return $lines;
    }

    /**
     * @param null|string $code
     *
     * @return Country|null
     */
    private function getCountry($code = null)
    {
        if (is_null($code)) {
            return null;
        }

        if (isset($this->countries[$code]) && $this->countries[$code] instanceof Country) {
            return $this->countries[$code];
        }

        return $this->countries[$code] = $this->getDoctrine()->getManager()->getRepository(Country::class)->find('NL');
    }

    /**
     * @return Site
     * @throws TempApiException
     */
    private function getSite()
    {
        $site = $this->getDoctrine()->getRepository(Site::class)->findOneBy([
            'theme' => 'toptaarten',
        ]);

        if (is_null($site)) {
            throw new TempApiException("Site doesn't exist.");
        }

        return $site;
    }

    /**
     * Return first company in Company registration based on Registration number.
     *
     * @param $chamberOfCommerceNumber
     *
     * @return mixed
     * @throws TempApiException
     */
    private function getCompanyByChamberOfCommerceNumber($chamberOfCommerceNumber)
    {
        // Get company by chamber of commerce number.
        $companyRegistration = $this->em->getRepository(CompanyRegistration::class)
            ->findOneBy(['registrationNumber' => $chamberOfCommerceNumber]);

        if (!$companyRegistration) {
            throw new TempApiException('No company registration found.');
        }

        /** @var ArrayCollection $companies */
        $companies = $companyRegistration->getCompanies();

        if (!$companies->first()) {
            throw new TempApiException('No companies found in registration.');
        }

        return $companies->first();
    }

    /**
     * @param Company $company
     *
     * @return Customer
     * @throws Exception
     */
    private function getFirstCustomerOfCompany(Company $company)
    {
        $customer = null;

        if ($company->getCustomers()) {
            $customer = $company->getCustomers()->first();
        }

        if (!$customer) {
            throw new TempApiException(sprintf('Set a customer for "%s"', $company->getName()));
        }

        return $customer;
    }

    /**
     * @param Company $company
     *
     * @return Address
     * @throws Exception
     */
    private function getCompanyInvoiceAddress(Company $company)
    {
        $invoiceAddress = $company->getInvoiceAddress();

        if (!$invoiceAddress) {
            throw new TempApiException(sprintf('Set an invoice address for "%s"', $company->getName()));
        }

        return $invoiceAddress;
    }

    /**
     * @param $sku
     *
     * @return Product
     * @throws Exception
     */
    private function getProductBySku($sku)
    {
        if (false === isset($this->products[$sku])) {
            throw new TempApiException(sprintf('Product with sku %s doesn\'t exist.', $sku));
        }

        return $this->products[$sku]['product'];
    }

    /**
     * @param Exception $exception
     *
     * @return string
     * @throws TempApiException
     */
    private function getErrorMailBody(Exception $exception)
    {

        $json = json_encode($this->getRawData());

        $body = '';
        $body .= '<div style="font: 11px/1.4 Arial, sans-serifl">';
        $body .= "<h3>TEMP API ERROR:</h3>";
        $body .= "<hr><br/>";
        $body .= "Response: <b>" . $exception->getMessage() . "</b><br/><br/><br/>";
        $body .= "JSON INPUT: <br/>" . $json . "<br/>";
        $body .= "</div>";

        return $body;
    }

    /**
     * @param string $to
     * @param string $subject
     * @param string $body
     *
     * @return int
     */
    private function sendErrorMail($to = 'helpdesk@topgeschenken.nl', $subject = '', $body = '')
    {
        /** @var Swift_Mailer $mailer */
        $mailer = $this->container->get('mailer');

        $message = (new Swift_Message($subject))
            ->setFrom(['noreply@topgeschenken.nl' => 'Topgeschenken.nl'])
            ->setTo($to)
            ->setBody(
                $body,
                'text/html'
            );

        return $mailer->send($message);
    }

    /**
     * @param Product $product
     *
     * @return array
     * @throws TempApiException
     */
    private function getProductSupplierIds(Product $product)
    {
        $supplierManager = $this->container->get(SupplierManagerService::class);
        $productSuppliers = $supplierManager
            ->getAvailableSuppliersForProduct($product);

        // If product has no supplier throws error
        if ($productSuppliers->count() == 0) {
            throw new TempApiException(sprintf("Product with id '%s' has no supplier(s) selected",
                $product->getId()));
        }

        return $productSuppliers->map(function (Company $company) {
            return $company->getId();
        })->toArray();
    }

    /**
     * Split lines into multiple groups based on product suppliers.
     *
     * @param OrderLine[] $lines
     *
     * @return array
     * @throws Exception
     */
    private function splitLinesBySupplier($lines)
    {
        $indexItems = [];

        foreach ($lines as $line) {

            if (!$line->getProduct() || $line->getParent()) {
                continue;
            }

            $product = $line->getProduct();
            $productSupplierIds = $this->products[$product->getSku()]['suppliers'];

            $intersect = [];

            $i = -1;
            foreach ($indexItems as $indexItem) {

                $i++;

                // Check of er een overlapping is.
                $intersect = array_intersect(
                    $indexItem['matched_suppliers'],
                    $productSupplierIds
                );

                if ($intersect) {
                    $indexItems[$i]['matched_suppliers'] = $intersect;
                    $indexItems[$i]['lines'][] = $line;
                    $indexItems[$i] = $this->addChildLinesToItemIndex($line, $indexItems[$i]);

                    break;
                }
            }

            if (!$intersect) {

                $indexItem = [
                    'matched_suppliers' => $productSupplierIds,
                    'lines' => [$line],
                ];

                $indexItem = $this->addChildLinesToItemIndex($line, $indexItem);
                $indexItems[] = $indexItem;
            }
        }

        $splittedLines = [];

        foreach ($indexItems as $indexItem) {
            $splittedLines[] = $indexItem['lines'];
        }

        return $splittedLines;
    }

    /**
     * @param OrderLine $line
     * @param           $indexItem
     *
     * @return mixed
     */
    private function addChildLinesToItemIndex(OrderLine $line, $indexItem)
    {
        foreach ($line->getChildren() as $childLine) {
            $indexItem['lines'][] = $childLine;
        }

        return $indexItem;
    }
}
