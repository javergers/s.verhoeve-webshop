<?php

namespace AppBundle\Controller\TempApi\Topbloemen;

use AppBundle\Controller\TempApi\BaseController;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderActivity;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\Paymentmethod;
use AppBundle\Entity\Payment\PaymentStatus;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Site\Site;
use AppBundle\Entity\Supplier\SupplierProduct;
use AppBundle\Exceptions\TempApiException;
use AppBundle\Manager\Order\OrderLineManager;
use AppBundle\Services\TransportTypeHelper;
use AppBundle\Utils\Address;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use const PHP_EOL;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @Route("/temp-api/topbloemen")
 */
class OrderController extends BaseController
{
    /**
     * @Route("/order", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function indexAction(Request $request)
    {
        if ($this->validateRequest($request)) {
            throw new \Exception('Invalid request');
        }

        try {
            $data = json_decode($request->getContent());

            $orderCollection = $this->convert((array)$data);

            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse([
                "success" => true,
                "orderId" => $orderCollection->getOrders()[0]->getOrderNumber(),
            ]);
        } catch (\Exception $e) {
            return new JsonResponse([
                "success" => false,
                "error" => $e->getMessage(),
            ]);
        }
    }

    /**
     * @param array $data
     * @return OrderCollection
     * @throws \Exception
     */
    private function convert(array $data)
    {
        // Sanitize the data
        foreach (array_keys($data) as $type) {
            $data[$type] = (array)$data[$type];

            array_walk($data[$type], function (&$value, $key) {
                if (substr($key, 0, 3) == "txt" && $value === "") {
                    $value = null;
                }

                if (substr($key, 0, 3) == "lng" && is_numeric($value)) {
                    $value = (int)$value;
                }

                if (substr($key, 0, 3) == "dec" && is_numeric($value)) {
                    $value = (float)$value;
                }
            });
        }

        $order = $this->findOrder($data['order']['lngOrderAID']);

        if ($order) {
            throw new \Exception(sprintf("Order %d already exists", $data['order']['lngOrderAID']));
        }

        $company = $this->getDoctrine()->getRepository(Company::class)->find($this->container->getParameter("topgeschenken_company_id"));

        $site = $this->getDoctrine()->getRepository(Site::class)->findOneBy([
            "theme" => "topbloemen",
        ]);

        $orderCollection = new OrderCollection();
        $orderCollection->setIp($data['order']['txtOrderIP']);
        $orderCollection->setCompany($company);
        $orderCollection->setCustomer($company->getCustomers()[0]);
        $orderCollection->setInvoiceAddress($company->getInvoiceAddress());
        $orderCollection->setSite($site);

        $this->convertOrder($orderCollection, $data);

        $this->getDoctrine()->getManager()->persist($orderCollection);

        $orderActivity = new OrderActivity();
        $orderActivity->setOrder($orderCollection->getOrders()[0]);
        $orderActivity->setDatetime(new \DateTime());
        $orderActivity->setActivity("order_created_api");

        $this->getDoctrine()->getManager()->persist($orderActivity);
        $this->getDoctrine()->getManager()->flush();

        $payment = new Payment();
        $payment->setOrder($orderCollection);
        $payment->setStatus($this->getDoctrine()->getManager()->getRepository(PaymentStatus::class)->find("authorized"));
        $payment->setPaymentmethod($this->getDoctrine()->getManager()->getRepository(Paymentmethod::class)->findOneBy([
            "code" => "internal",
        ]));
        $payment->setDatetime(new \DateTime());

        foreach($orderCollection->getOrders() as $order) {
            $order->setStatus('new');
        }

        $this->getDoctrine()->getManager()->persist($payment);

        return $orderCollection;
    }

    /**
     * @param OrderCollection $orderCollection
     * @param array           $data
     * @throws \Exception
     */
    private function convertOrder(OrderCollection $orderCollection, array $data)
    {
        $this->disableDoctrineAccessibleFilter();

        if (!empty($data['order']['txtSendNumber'])) {
            $addressString = sprintf('%s %s', $data['order']['txtSendAddress'], $data['order']['txtSendNumber']);
        } else {
            $addressString = $data['order']['txtSendAddress'];
        }

        $address = new Address();
        $address->parseAddress($addressString, true);

        if (empty($address->getStreet())) {
            throw new BadRequestHttpException('Straatnaam is leeg, kan order niet verwerken');
        }

        $order = new Order();
        $order->setOrder($orderCollection);
        $order->setStatus('payment_pending');
        $order->setNumber(0001);
        $order->setDeliveryDate(new \DateTime($data['order']['datSendDate']));
        $order->setSupplierRemark($data['order']['memSendRemark']);
        //$order->setDeliveryRemark();
        //$order->setInternalRemark();
        $order->setInvoiceReference($data['order']['txtCustomerReference']);
        $order->setDeliveryAddressCompanyName($data['order']['txtSendCompany']);
        $order->setDeliveryAddressAttn($data['order']['txtSendPerson']);
        $order->setDeliveryAddressStreet($address->getStreet());
        $order->setDeliveryAddressNumber($address->getHouseNumber());
        $order->setDeliveryAddressNumberAddition($address->getHouseNumberAddition());
        $order->setDeliveryAddressPostcode($data['order']['txtSendPostalcode']);
        $order->setDeliveryAddressCity($data['order']['txtSendCity']);
        $order->setDeliveryAddressCountry($this->getDoctrine()->getRepository(Country::class)->find($data['order']['txtSendCountry']));
        $order->setDeliveryAddressPhoneNumber($data['order']['txtSendTelephone']);
        $order->setMetadata([
            "topbloemenOrderId" => $data['order']['lngOrderAID'],
        ]);
        $order->setOrderCollection($orderCollection);

        $this->convertProduct($order, $data);
        $this->convertVase($order, $data);
        $this->convertCard($order, $data);
        $this->convertExtra($order, $data);

        //todo SF4
        $product = $this->container->get(TransportTypeHelper::class)->getTransportType($order);

        $orderLine = new OrderLine();
        $orderLine->setProduct($product);
        $orderLine->setLedger($product->getProductgroup()->getLedger());
        $orderLine->setTitle($product->translate()->getTitle());
        $orderLine->setPrice($product->getPrice());
        $orderLine->setQuantity($data['order']['lngProductNumber']);
        $orderLine->setOrder($order);

        $order->addLine($orderLine);
        $orderCollection->addOrder($order);

        $this->container->get(OrderLineManager::class)->createVatLines($orderLine);

        //todo SF4
        $errors = $this->container->get('validator')->validate($order);
        if($errors->count()) {
            $errorText = '';
            foreach($errors as $error) {
                $errorText .= $error.PHP_EOL;
            }

            throw new TempApiException($errorText);
        }
    }

    /**
     * @param Order $order
     * @param array $data
     * @throws \Exception
     */
    private function convertProduct(Order $order, array $data)
    {
        $product = $this->getProduct($data, "product");

        if ($product) {
            $orderLine = new OrderLine();
            $orderLine->setProduct($product);
            $orderLine->setLedger($product->getProductgroup()->getLedger());
            $orderLine->setTitle($product->translate()->getTitle());
            $orderLine->setPrice($product->getPrice());
            $orderLine->setQuantity($data['order']['lngProductNumber']);

            $order->addLine($orderLine);

            $this->container->get(OrderLineManager::class)->createVatLines($orderLine);
        } else {
            throw new \Exception("No product found");
        }

    }

    /**
     * @param Order $order
     * @param array $data
     * @throws \Exception
     */
    private function convertVase(Order $order, array $data)
    {
        $product = $this->getProduct($data, "vase");

        if ($product) {
            if (!$product->getVariations()->isEmpty()) {
                throw new \Exception("Products with variations are not supported yet");
            }

            $orderLine = new OrderLine();
            $orderLine->setParent($order->getLines()[0]);
            $orderLine->setProduct($product);
            $orderLine->setLedger($product->getProductgroup()->getLedger());
            $orderLine->setTitle($product->translate()->getTitle());
            $orderLine->setPrice($product->getPrice());
            $orderLine->setQuantity($data['order']['lngVaseNumber']);

            $order->addLine($orderLine);

            $this->container->get(OrderLineManager::class)->createVatLines($orderLine);
        }
    }

    /**
     * @param Order $order
     * @param array $data
     * @throws \Exception
     */
    private function convertCard(Order $order, array $data)
    {
        $product = $this->getProduct($data, "card");

        if ($product) {
            $orderLine = new OrderLine();
            $orderLine->setParent($order->getLines()[0]);
            $orderLine->setProduct($product);
            $orderLine->setLedger($product->getProductgroup()->getLedger());
            $orderLine->setTitle($product->translate()->getTitle());
            $orderLine->setPrice($product->getPrice());
            $orderLine->setQuantity($data['order']['lngCardNumber']);
            $orderLine->setMetadata([
                "is_card" => true,
                //"text" => $data['order']['txtUtf8CardText']?($data['order']['txtUtf8CardText']:$data['order']['txtCardText1']
                "text" => $data['order']['txtCardText1'],
            ]);

            $order->addLine($orderLine);

            $this->container->get(OrderLineManager::class)->createVatLines($orderLine);
        }
    }

    /**
     * @param Order $order
     * @param array $data
     * @throws \Exception
     */
    private function convertExtra(Order $order, array $data)
    {
        $product = $this->getProduct($data, "extra");

        if ($product) {
            $orderLine = new OrderLine();
            $orderLine->setProduct($product);
            $orderLine->setLedger($product->getProductgroup()->getLedger());
            $orderLine->setTitle($product->translate()->getTitle());
            $orderLine->setPrice($product->getPrice());
            $orderLine->setQuantity($data['order']['lngExtraOrderProductnumber']);
            $orderLine->setMetadata([
                'additional_product_parent' => $order->getLines()->first()->getProduct()->getId(),
            ]);

            $order->addLine($orderLine);

            $this->container->get(OrderLineManager::class)->createVatLines($orderLine);
        }
    }

    /**
     * @param array  $data
     * @param string $type
     * @return Product|null
     * @throws \Exception
     */
    private function getProduct(array $data, $type)
    {
        $sku = null;

        switch ($type) {
            case "product":
                $sku = $data['product']['memProductEurofloristdescription'];
                break;
            case "vase":
                // er is geen kenmerk om een vaas te herkennen op dit moment, wordt nu nog niet gebruikt
                if ($data['order']['lngVaseID']) {
                    throw new \Exception("Products with vases are currently not supported");
                }
                break;
            case "card":
                // er is geen kenmerk om een kaartje te herkennen, voor nu volstaat Blanco kaartje
                if ($data['order']['lngCardID']) {
                    $sku = $this->container->get("doctrine")->getManager()->getRepository(SupplierProduct::class)->findOneBy([
                        "sku" => "3topbl",
                    ])->getProduct()->getSku();
                }
                break;
            case "extra":
                if ($data['order']['lngExtraProductID']) {
                    $sku = $data['extra']['memProductEurofloristdescription'];
                }
                break;
            default:
                throw new \Exception(sprintf("Type '%s' isn't supported", $type));
        }

        if (!$sku) {
            return null;
        }

        /** @var Product $product */
        $product = $this->container->get("doctrine")->getManager()->getRepository(Product::class)->findOneBy([
            "sku" => $sku,
        ]);

        if (!$product) {
            throw new \Exception(sprintf("No product found with sku '%s'", $sku));
        }

        return $product;
    }

    /**
     * @param $topbloemenId integer
     * @return Order|null
     * @throws NonUniqueResultException
     */
    private function findOrder($topbloemenId)
    {
        /** @var EntityManagerInterface $em */
        $em = $this->getDoctrine()->getManager();

        $rsm = new ResultSetMappingBuilder($em);
        $rsm->addRootEntityFromClassMetadata(Order::class, '`order`');

        $query = $em->createNativeQuery("
            SELECT *
            FROM `order`
            WHERE `order`.`metadata` IS NOT NULL 
              AND `order`.`metadata` RLIKE 'topbloemenOrderId\":" . $topbloemenId . "[,|}]'
              AND `order`.`status` != 'cancelled'
        ", $rsm);

        try {
            return $query->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }
}
