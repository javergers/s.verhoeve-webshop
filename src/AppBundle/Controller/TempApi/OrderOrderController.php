<?php

namespace AppBundle\Controller\TempApi;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderDeliveryStatus;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Exceptions\InvalidHashException;
use AppBundle\Manager\Order\OrderLineManager;
use AppBundle\Services\Designer;
use League\Flysystem\FileNotFoundException;
use AppBundle\Utils\DisableFilter;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @Route("/temp-api")
 */
class OrderOrderController extends BaseController
{
    /**
     * @Route("/order/setProcessStatus/{hash}", methods={"GET"})
     *
     * @param string $hash
     * @return Response
     */
    public function setProcessStatusAction(string $hash)
    {
        $em = $this->getDoctrine()->getManager();

        $type = 'processStatus';
        $orderStatusObj = $this->processHash($type, $hash);

        /** @var Order $order */
        $order = $em->getRepository(Order::class)->find($orderStatusObj->orderId);

        // This check is sufficient, because subsequent checks will fail in other unwanted situations
        if ($order->getStatus() === 'complete') { // @todo: workflow guard refactor
            throw new BadRequestHttpException('Cannot change order status: already complete.');
        }

        $orderStatus = $orderStatusObj->data['status'];

        if ($orderStatus === null) {
            throw new BadRequestHttpException('Invalid order status');
        }

        if ($order->getSupplierOrder() === null) {
            throw new BadRequestHttpException('No supplier order');
        }

        if ($order->getSupplierOrder()->getSupplier()->getId() !== $orderStatusObj->supplierId) {
            throw new BadRequestHttpException('Hash has expired');
        }

        $currentStatus = $order->getStatus();
        $order->setStatus($orderStatus); // @todo: workflow refactor

        $em->flush();

        if ($currentStatus !== $order->getStatus()) {
            $message = 'Order status successfully updated.';
        } else {
            $message = 'Order status is up-to-date.';
        }

        return new Response($message);
    }

    /**
     * @Route("/order/setDeliveryStatus/{hash}", methods={"GET"})
     *
     * @param string $hash
     * @return Response
     */
    public function setDeliveryStatusAction(string $hash = null)
    {
        $em = $this->getDoctrine()->getManager();

        $type = 'deliveryStatus';

        $orderStatusObj = $this->processHash($type, $hash);

        /** @var Order $order */
        $order = $em->getRepository(Order::class)->find($orderStatusObj->orderId);

        $orderStatus = $em->getRepository(OrderDeliveryStatus::class)->find($orderStatusObj->data['status']);
        if ($orderStatus === null) {
            throw new BadRequestHttpException('Invalid order delivery status');
        }

        if ($order->getSupplierOrder() === null) {
            throw new BadRequestHttpException('No supplier order');
        }

        if ($order->getSupplierOrder()->getSupplier()->getId() !== $orderStatusObj->supplierId) {
            throw new BadRequestHttpException('Hash has expired');
        }

        $currentStatus = $order->getDeliveryStatus();
        $order->setDeliveryStatus($orderStatus);

        // @todo Delivery status handling should be smarter than this
        if ($orderStatus->getId() !== 'failed') {
            $order->setStatus('complete');  // @todo: workflow refactor
        }

        $em->flush();

        if ($currentStatus !== $order->getDeliveryStatus()) {
            $message = 'Order delivery status successfully updated.';
        } else {
            $message = 'Order delivery status is up-to-date.';
        }

        return new Response($message);
    }

    /**
     * @param string $type
     * @param string $hash
     * @return \stdClass
     */
    private function processHash(string $type, string $hash)
    {
        $hashService = $this->get('app.hash');
        try {
            return $hashService->decryptHash($type, $hash);
        } catch (InvalidHashException $exception) {
            throw new BadRequestHttpException($exception->getMessage());
        }
    }

    /**
     * @Route("/order/update/status/{hash}", methods={"GET"})
     *
     * @deprecated
     * @param null $hash
     *
     * @return Response
     */
    public function updateOrderStatusAction($hash = null)
    {
        $filter = DisableFilter::temporaryDisableFilter('accessible');
        $em = $this->getDoctrine()->getManager();

        $this->getDoctrine()->getConnection()->beginTransaction();

        $orderStatusHashService = $this->get('topgeschenken.order_status.hash');

        try {
            $orderStatusHashService->processHash($hash);

            $em->flush();
            $this->getDoctrine()->getConnection()->commit();

            if ($orderStatusHashService->isStatusModified()) {
                $message = 'Order status successfully updated.';
            } else {
                $message = 'Order status is up-to-date.';
            }

            return new Response($message);
        } catch (\Exception $e) {
            $this->getDoctrine()->getConnection()->rollBack();

            return new Response($e->getMessage(), $e->getCode());
        } finally {
            $filter->reenableFilter();
        }
    }

    /**
     * @Route("/order/design")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getDesignAction(Request $request)
    {
        try {
            return $this->get('topgeschenken.order.assets.hash')->processHash('design', $request->get('hash'));
        } catch (\Exception $exception) {
            return new Response("Can't load the design.", 422);
        }
    }

    /**
     * @Route("/order/packingslip")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getPackingSlipAction(Request $request)
    {
        try {
            return $this->get('topgeschenken.order.assets.hash')
                ->processHash('packingslip', $request->get('hash'));

        } catch (\Exception $exception) {
            return new Response("Cant't load the packingslip.", 422);
        }
    }

    /**
     * @Route("/order/{order}/{orderLine}/{types}")
     *
     * @param Request        $request
     * @param Order          $order
     * @param OrderLine      $orderLine
     * @param                $types
     * @deprecated
     * @return JsonResponse
     * @throws FileNotFoundException
     */
    public function getData(Request $request, Order $order, ?OrderLine $orderLine, $types)
    {
        $this->disableDoctrineAccessibleFilter();
        $this->disableDoctrinePublishableFilter();

        // header check
        if ($json = $this->validateRequest($request)) {
            return $json;
        }

        if (!$order || !$types) {
            return new JsonResponse('Invalid request', 500);
        }

        $types = explode(',', $types);

        $responseData = [];

        $orderCollection = $order->getOrderCollection();

        $designer = $this->get('app.designer');

        foreach ($order->getLines() as $line) {

            if ($orderLine !== null && $line->getId() !== $orderLine->getId()) {
                continue;
            }

            /* Return designs */
            if (\is_array($types) && \in_array('design', $types, true)) {

                if (!isset($responseData['design'])) {
                    $responseData['design'] = [];
                }

                if ($line->getMetadata()) {
                    $printUrl = $this->get(OrderLineManager::class)->getDesignPrintUrl($line);

                    if (isset($line->getMetadata()['designer'])) {
                        $designer->setUuid($line->getMetadata()['designer']);

                        $responseData['design']['ref'] = $orderCollection->getNumber() . '-' . $order->getNumber() . '-' . $line->getId();
                        $responseData['design']['print'] = $printUrl;

                        if ($designer->getFileUrl(Designer::FILE_PREVIEW)) {
                            $responseData['design']['thumb'] = $designer->getFileUrl(Designer::FILE_PREVIEW);
                        }
                    } elseif (isset($line->getMetadata()['design_url'])) {
                        $responseData['design']['print'] = $line->getMetadata()['design_url'];
                        $responseData['design']['thumb'] = $line->getMetadata()['design_url'];
                    }
                }
            }

            /* Return letter */
            if (\is_array($types) && \in_array('letter', $types, true)) {

                if ($line->getMetadata() && isset($line->getMetadata()['letter_url'])) {
                    $responseData['letter'] = $line->getMetadata()['letter_url'];
                }
            }

            /* Return packing slip */
            if (\is_array($types) && \in_array('slip', $types, true)) {
                $parameters = [
                    'line' => $orderLine->getId(),
                ];

                return $this->get('order.pdf.packing_slip')
                    ->setParameters($parameters)
                    ->setItem($order)
                    ->getResponse();
            }
        }

        $response = new JsonResponse($responseData);
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }
}
