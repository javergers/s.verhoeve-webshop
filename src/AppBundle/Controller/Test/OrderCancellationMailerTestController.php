<?php

namespace AppBundle\Controller\Test;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Services\EnvironmentalService;
use AppBundle\Services\Mailer\OrderCancellationMailer;
use AppBundle\Services\Mailer\OrderConfirmationMailer;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OrderCancellationMailerTestController
 * @package AppBundle\Controller\Test
 */
class OrderCancellationMailerTestController extends Controller
{
    /**
     * @var OrderCancellationMailer
     */
    private $orderCancellationMailer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * OrderConfirmationMailerTestController constructor.
     *
     * @param OrderCancellationMailer $orderCancellationMailer
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        OrderCancellationMailer $orderCancellationMailer,
        EntityManagerInterface $entityManager
    ) {
        $this->orderCancellationMailer = $orderCancellationMailer;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/test/order-cancellation/{orderCollection}/send", condition="'%kernel.environment%' === 'test'", methods={"GET"})
     * @param OrderCollection|null $orderCollection
     * @return Response
     * @throws Exception
     */
    public function sendTestMail(?OrderCollection $orderCollection = null): Response
    {
        if($orderCollection === null) {
            $orderCollection = $this->entityManager->getRepository(OrderCollection::class)->find(1);
        }

        $isSend = $this->orderCancellationMailer->send([
            'orderCollection' => $orderCollection
        ]);

        if($isSend) {
            return new Response('Ok');
        }

        return new Response('Failed');
    }

    /**
     * @Route("/test/order-cancellation/{orderCollection}/preview", condition="'%kernel.environment%' === 'dev'")
     *     methods={"GET"})
     *
     * @param OrderCollection|null $orderCollection
     *
     * @return Response
     * @throws \Exception
     */
    public function preview(?OrderCollection $orderCollection = null): Response
    {
        if (null === $orderCollection) {
            $orderCollection = $this->entityManager->getRepository(OrderCollection::class)->find(1);
        }

        $data = null;
        $this->orderCancellationMailer->send(
            [
                'orderCollection' => $orderCollection,
                'preview' => true
            ],
            static function (\Swift_Message $message) use (&$data) {
                $data = $message->getBody();
                return false;
            }
        );

        return new Response($data);

    }
}
