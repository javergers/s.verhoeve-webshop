<?php

namespace AppBundle\Controller\Test;

use AppBundle\Entity\Order\Order;
use AppBundle\Services\Mailer\OrderDeliveryStatusMailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Exception;

/**
 * Class OrderDeliveryStatusMailerTestController
 * @package AppBundle\Test
 */
class OrderDeliveryStatusMailerTestController extends Controller
{
    /**
     * @var OrderDeliveryStatusMailer
     */
    private $orderDeliveryStatusMailer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * OrderDeliveryStatusMailerTestController constructor.
     * @param OrderDeliveryStatusMailer $orderDeliveryStatusMailer
     * @param EntityManagerInterface    $entityManager
     */
    public function __construct(
        OrderDeliveryStatusMailer $orderDeliveryStatusMailer,
        EntityManagerInterface $entityManager
    ) {
        $this->orderDeliveryStatusMailer = $orderDeliveryStatusMailer;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/test/order-delivery-status/{order}/send", condition="'%kernel.environment%' === 'test'", methods={"GET"})
     * @param Order|null $order
     * @return Response
     * @throws Exception
     */
    public function sendTestMail(?Order $order = null): Response
    {
        if($order === null) {
            $order = $this->entityManager->getRepository(Order::class)->find(1);
        }

        $isSend = $this->orderDeliveryStatusMailer->send([
            'order' => $order
        ]);

        if($isSend) {
            return new Response('OK');
        }

        return new Response('Failed');
    }

    /**
     * @Route("/test/order-delivery-status/{order}/preview", condition="'%kernel.environment%' === 'dev'") methods={"GET"})
     *
     * @param Order|null $order
     * @return Response
     * @throws Exception
     */
    public function preview(?Order $order = null): Response
    {
        if($order === null) {
            $order = $this->entityManager->getRepository(Order::class)->find(1);
        }

        $data = null;
        $this->orderDeliveryStatusMailer->send(
            [
                'order' => $order,
                'preview' => true
            ],
            function (\Swift_Message $message) use (&$data) {
                $data = $message->getBody();
                return false;
            }
        );

        return new Response($data);
    }
}