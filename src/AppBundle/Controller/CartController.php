<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Discount\Voucher;
use AppBundle\Entity\Order\Cart;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Exceptions\Sales\CartInvalidSupplierCombinationException;
use AppBundle\Interfaces\Sales\OrderLineInterface;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Manager\Catalog\Product\ProductManager;
use AppBundle\Manager\Order\CartManager;
use AppBundle\Services\CartService;
use AppBundle\Services\Discount\VoucherService;
use AppBundle\Services\SiteService;
use AppBundle\Services\Sales\Order\DeliveryService;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use OpenCloud\Common\Exceptions\ForbiddenOperationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Translation\Extractor\Model\SourceLocation;
use Translation\Extractor\TranslationSourceLocationContainer;

/**
 * Class CartController
 *
 * @Route("/winkelwagen")
 *
 * @package AppBundle
 */
class CartController extends Controller implements TranslationSourceLocationContainer
{
    /**
     * @Template()
     *
     * @param Request $request
     *
     * @return array|Response
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws ORMException
     * @throws Exception
     */
    public function indexAction(Request $request)
    {
        $cart = $this->get(CartService::class);

        if (!$cart->count()) {
            return new RedirectResponse($this->generateUrl('app_home_index'));
        }

        $company = $cart->getCart()->getCompany();
        //apply company vouchers if customer is logged in as company
        if(null !== $company) {
            /** @var Voucher[] $vouchers */
            $vouchers = $this->getDoctrine()->getRepository(Voucher::class)->findByCompany($company);

            //before reapplying vouchers delete vouchers from cart to prevent duplication
            foreach ($cart->getCart()->getVouchers() as $v) {
                $cart->getCart()->removeVoucher($v);
            }

            foreach($vouchers as $voucher) {
                try {
                    $this->container->get(VoucherService::class)->applyVoucherForOrderCollectionInterface($voucher->getCode(), $cart->getCart());
                } catch(Exception $e) {
                    $this->container->get('logger')->addWarning($e->getMessage());
                }

            }

            $this->getDoctrine()->getManager()->flush();
        }

        $session = $request->getSession();
        $continueWithAddress = $session->get('continueWithAddress')? true: false;
        $session->remove('continueWithAddress');

        //calculate totals of cart to be sure the correct value is shown
        $this->container->get(CartManager::class)->calculateTotals($cart->getCart());

        return [
            'cart' => $cart,
            'continueWithAddress' => $continueWithAddress,
        ];
    }

    /**
     * @Route("/totalen", methods={"GET"})
     * @Template("@App/Cart/components/totals.html.twig")
     */
    public function totalAction()
    {
        return [
            'cart' => $this->get(CartService::class),
        ];
    }

    /**
     * @Route("/bestelling/{cartOrder}/kaartje/{cartOrderLine}/toevoegen", methods={"GET", "POST"})
     *
     * @param CartOrder     $cartOrder
     * @param CartOrderLine $cartOrderLine This is the parent CartOrderLine on which the product card needs to be added
     *
     * @param Request       $request
     * @return Response
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     */
    public function addCardAction(CartOrder $cartOrder, CartOrderLine $cartOrderLine, Request $request)
    {
        // Check whether the cartOrderLine exists in the cartOrder entity
        if (!$cartOrder->getLines()->contains($cartOrderLine)) {
            throw new NotFoundHttpException();
        }

        if ($request->isMethod('POST')) {
            if (!$request->isXmlHttpRequest()) {
                throw new BadRequestHttpException();
            }

            if (!$request->request->has('cardId')) {
                throw new BadRequestHttpException();
            }

            if (!$request->request->has('cardText')) {
                throw new BadRequestHttpException();
            }

            try {
                $cardProduct = $this->getDoctrine()->getRepository(Product::class)->find($request->request->get('cardId'));
                $cardMetadata = [
                    'is_card' => true,
                    'text' => $request->request->get('cardText'),
                ];

                $cartOrder
                    ->addProduct($cardProduct, 1, $cartOrderLine)
                    ->setMetadata($cardMetadata);

                $this->getDoctrine()->getManager()->persist($cartOrder);
                $this->getDoctrine()->getManager()->flush();

                return new JsonResponse(true);
            } catch (Exception $e) {
                return new JsonResponse(false, 500);
            }
        }

        $productCard = $cartOrderLine->getProductCard();

        $productManager = $this->get(ProductManager::class);

        $cards = $productManager->listCards($cartOrderLine->getProduct());

        return $this->render('@App/Cart/components/card.html.twig', [
            'line' => $cartOrderLine,
            'cards' => $cards,
            'currentCard' => $productCard,
        ]);
    }

    /**
     * @Route("/bestelling/{cartOrder}/kaartje/{cartOrderLine}/bewerken", methods={"GET", "POST"})
     * @Template("@App/Cart/components/card.html.twig")
     *
     * @param CartOrder     $cartOrder
     * @param CartOrderLine $cartOrderLine This is the CartOrderLine on which contains the product card
     * @param Request       $request
     *
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     * @return Response|array
     */
    public function editCardAction(CartOrder $cartOrder, CartOrderLine $cartOrderLine, Request $request)
    {
        // Check whether the cartOrderLine exists in the cartOrder entity
        if (!$cartOrder->getLines()->contains($cartOrderLine)) {
            throw new NotFoundHttpException();
        }

        if ($request->isMethod('POST')) {
            if (!$request->isXmlHttpRequest()) {
                throw new BadRequestHttpException();
            }

            if (!$request->request->has('cardId')) {
                throw new BadRequestHttpException();
            }

            if (!$request->request->has('cardText')) {
                throw new BadRequestHttpException();
            }

            try {
                $cardProduct = $this->getDoctrine()->getRepository(Product::class)->find($request->request->get('cardId'));
                $cardMetadata = $cartOrderLine->getMetadata();

                $cardMetadata['text'] = $request->request->get('cardText');

                $cartOrderLine->setProduct($cardProduct);
                $cartOrderLine->setMetadata($cardMetadata);

                $this->getDoctrine()->getManager()->persist($cartOrderLine);
                $this->getDoctrine()->getManager()->flush();

                return new JsonResponse(true);
            } catch (Exception $e) {
                return new JsonResponse(false, 500);
            }
        }

        $productManager =  $this->get(ProductManager::class);
        $cards = $productManager->listCards($cartOrderLine->getParent()->getProduct());

        return [
            'line' => $cartOrderLine,
            'cards' => $cards,
            'currentCard' => $cartOrderLine,
        ];
    }

    /**
     * @Route("/bestelling/{id}/bewerken", methods={"GET"})
     * @param CartOrder $cartOrder
     * @return Response
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function editOrderAction(CartOrder $cartOrder)
    {
        $orderCollection = $this->get(CartService::class)->getOrders();

        if (!$orderCollection->contains($cartOrder)) {
            throw new AccessDeniedException();
        }

        return $this->render('@App/Cart/components/edit.html.twig', [
            'order' => $cartOrder,
        ]);
    }

    /**
     * @Route("/bestelling/{id}/verwijderen", methods={"DELETE"})
     * @param CartOrder $cartOrder
     * @return JsonResponse
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function removeOrderAction(CartOrder $cartOrder)
    {
        $orderCollection = $this->get(CartService::class)->getOrders();

        if (!$orderCollection->contains($cartOrder)) {
            throw new AccessDeniedException();
        }

        $this->get(CartService::class)->removeOrder($cartOrder);

        return new JsonResponse([
            'count' => $this->get(CartService::class)->count(),
            'html' => $this->renderView('@App/Cart/components/totals.html.twig', [
                'cart' => $this->get(CartService::class),
                'site' => $this->get(SiteService::class)->determineSite()
            ]),
        ]);
    }

    /**
     * @Route("/bestelling/{cartOrder}/order-regel/{cartOrderLine}/verwijderen", methods={"DELETE"})
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     * @param CartOrder $cartOrder
     * @param CartOrderLine $cartOrderLine
     *
     * @return JsonResponse
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws \AppBundle\Exceptions\Sales\CartInvalidSupplierCombinationException
     * @throws Exception
     */
    public function removeOrderLineAction(CartOrder $cartOrder, CartOrderLine $cartOrderLine): JsonResponse
    {
        if(!$cartOrder->getLines()->contains($cartOrderLine)) {
            throw new NotFoundHttpException();
        }

        $em = $this->getDoctrine()->getManager();
        $cartOrder->removeLine($cartOrderLine);

        foreach($cartOrderLine->getChildren() as $child) {
            $cartOrder->removeLine($child);
            $em->remove($child);
        }

        $em->remove($cartOrderLine);
        $this->get('delivery')->updateDeliveryProduct($cartOrder);

        if($cartOrder->getLines()->isEmpty()) {
            $em->remove($cartOrder);
        }

        $cart = $cartOrder->getCart();
        foreach($cart->getVouchers() as $voucher) {
            $this->container->get(VoucherService::class)->applyVoucherForOrderCollectionInterface($voucher->getCode(), $cart, true);
        }

        $em->flush();

        return new JsonResponse([
            'count' => $this->get(CartService::class)->count(),
            'html' => $this->renderView('@App/Cart/components/totals.html.twig', [
                'cart' => $this->get(CartService::class),
                'site' => $this->get('app.site')->determineSite()
            ]),
        ]);
    }

    /**
     * @Route("/bestelling/{cartOrder}/personalisatie/{cartOrderLine}/verwijderen", methods={"DELETE"})
     *
     * @param CartOrder     $cartOrder
     * @param CartOrderLine $cartOrderLine This is the CartOrderLine on which contains the product card
     *
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function removeOrderLinePersonalizationAction(CartOrder $cartOrder, CartOrderLine $cartOrderLine): JsonResponse
    {
        // Check whether the cartOrderLine exists in the cartOrder entity
        if (!$cartOrder->getLines()->contains($cartOrderLine)) {
            throw new NotFoundHttpException();
        }

        // If there is no personalization or it is required for this product
        if (false === $cartOrderLine->hasProductPersonalization() || true === $cartOrderLine->hasRequiredPersonalization()) {
            throw new BadRequestHttpException();
        }

        $cartOrderLine->setQuantity(0);

        return $this->json([
            'html' => $this->renderView('@App/Cart/components/totals.html.twig', [
                'cart' => $this->get(CartService::class),
                'site' => $this->get('app.site')->determineSite()
            ]),
        ]);
    }

    /**
     * @Route("/bestelling/{cartOrder}/kaartje/{cartOrderLine}/verwijderen", methods={"DELETE"})
     *
     * @param CartOrder     $cartOrder
     * @param CartOrderLine $cartOrderLine This is the CartOrderLine on which contains the product card
     *
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function removeOrderLineCardAction(CartOrder $cartOrder, CartOrderLine $cartOrderLine): JsonResponse
    {
        // Check whether the cartOrderLine exists in the cartOrder entity
        if (!$cartOrder->getLines()->contains($cartOrderLine)) {
            throw new NotFoundHttpException();
        }

        // If there is no card
        if (false === $cartOrderLine->getParent()->hasProductCard()) {
            throw new BadRequestHttpException();
        }

        return $this->processOrderLineRemoval($cartOrderLine);
    }

    /**
     * @Route("/bestelling/{cartOrder}/brief/{cartOrderLine}/verwijderen", methods={"DELETE"})
     *
     * @param CartOrder     $cartOrder
     * @param CartOrderLine $cartOrderLine This is the CartOrderLine on which contains the letter
     *
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function removeOrderLineLetterAction(CartOrder $cartOrder, CartOrderLine $cartOrderLine): JsonResponse
    {
        // Check whether the cartOrderLine exists in the cartOrder entity
        if (!$cartOrder->getLines()->contains($cartOrderLine)) {
            throw new NotFoundHttpException();
        }

        // If there is no letter
        if (false === $cartOrderLine->getParent()->hasProductLetter()) {
            throw new BadRequestHttpException();
        }

        return $this->processOrderLineRemoval($cartOrderLine);
    }

    /**
     * @Route("/aantal", methods={"GET"})
     *
     * @param Request $request
     *
     * @return Response
     * @throws ForbiddenOperationException
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function countAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new ForbiddenOperationException('This Request is not allowed');
        }

        return new Response($this->get(CartService::class)->count());
    }

    /**
     * @Route("/ongeldige-combinatie/{id}", methods={"GET"})
     * @Template("@App/Cart/invalidCombination.html.twig")
     *
     * @param ProductInterface $product
     * @param Request          $request
     *
     * @return array
     */
    public function invalidCombinationAction(ProductInterface $product, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException();
        }

        $cart = $this->get(CartService::class);

        return [
            'cart' => $cart,
            'product' => $product,
        ];
    }

    /**
     * @Route("/vervang/{id}/{quantity}", methods={"POST"})
     *
     * @param Product $product
     * @param integer $quantity
     * @param Request $request
     *
     * @return JsonResponse
     * @throws CartInvalidSupplierCombinationException
     * @throws Exception
     */
    public function replaceCartOrderAction(Product $product, int $quantity, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException();
        }

        $cart = $this->get(CartService::class);

        $cart->getCurrentOrder()->replaceContentsWithProduct($product, $quantity);

        return new JsonResponse([
            'success' => true,
        ]);
    }

    /**
     *
     * @Route("/annuleren", methods={"GET"})
     *
     * @return JsonResponse
     */
    public function cancel(): JsonResponse
    {
        try {
            $this->get(CartService::class)->clear();
        } catch(Exception $e) {
            return $this->json([
                'success' => false
            ]);
        }

        return $this->json([
            'success' => true
        ]);
    }

    /**
     * @Route("/controleer-voucher/{cart}", methods={"POST"})
     *
     * @param Cart    $cart
     * @param Request $request
     * @return Response
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     * @ParamConverter("cart", options={"mapping": { "cart" = "uuid"}})
     */
    public function applyVoucherCode(Cart $cart, Request $request)
    {
        $cartService = $this->get(CartService::class);

        if (!$cartService->count()) {
            return new RedirectResponse($this->generateUrl('app_home_index'));
        }

        if ($voucherCode = $request->get('cart_voucher')) {
            try {
                $this->container->get(VoucherService::class)->applyVoucherForOrderCollectionInterface($voucherCode, $cart);

                $this->getDoctrine()->getManager()->flush();
            } catch (Exception $e) {
                if (null !== $request->getSession()) {
                    $request->getSession()->set('invalidVoucher', true);
                }
            } finally {
               return new RedirectResponse($this->generateUrl('app_checkout_sender'));
            }
        }

        if (null !== $request->getSession()) {
            $request->getSession()->set('invalidVoucher', true);
        }

        return new RedirectResponse($this->generateUrl('app_checkout_sender'));
    }

    /**
     * Return an array of source locations.
     *
     * @return SourceLocation[]
     */
    public static function getTranslationSourceLocations()
    {
        return [
            SourceLocation::createHere('cart.modal.coupon_add.title'),
            SourceLocation::createHere('cart.modal.coupon_add.submit'),
            SourceLocation::createHere('cart.modal.coupon_add.blank'),
        ];
    }

    /**
     * @param CartOrderLine $cartOrderLine
     * @return JsonResponse
     * @throws Exception
     */
    private function processOrderLineRemoval(CartOrderLine $cartOrderLine): JsonResponse
    {
        $cartOrderLine->setQuantity(0);

        return $this->json([
            'html' => $this->renderView('@App/Cart/components/totals.html.twig', [
                'cart' => $this->get(CartService::class),
                'site' => $this->get('app.site')->determineSite()
            ]),
        ]);
    }
}
