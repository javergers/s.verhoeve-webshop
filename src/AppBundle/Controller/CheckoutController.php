<?php

namespace AppBundle\Controller;

use AppBundle\DBAL\Types\AppliesToOrderType;
use AppBundle\Decorator\AssortmentDecorator;
use AppBundle\Decorator\Factory\ProductDecoratorFactory;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Personalization;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Geography\DeliveryAddressType;
use AppBundle\Entity\Geography\DeliveryAddressTypeTranslation;
use AppBundle\Entity\Order\Cart;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\PaymentStatus;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Entity\Security\Customer\NewsletterPreferences;
use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Supplier\PickupLocation;
use AppBundle\Exceptions\Discount\VoucherExpiredException;
use AppBundle\Exceptions\Discount\VoucherUsedException;
use AppBundle\Exceptions\Sales\CartInvalidSupplierCombinationException;
use AppBundle\Form\Cart\CartOrderType;
use AppBundle\Form\Cart\CartType;
use AppBundle\Form\Checkout\CartType as CheckoutCartType;
use AppBundle\Form\Checkout\PaymentType;
use AppBundle\Manager\Catalog\Product\ProductPriceManager;
use AppBundle\Manager\Order\CartManager;
use AppBundle\Manager\Order\CartOrderManager;
use AppBundle\Manager\Order\OrderCollectionManager;
use AppBundle\Manager\Relation\CompanyManager;
use AppBundle\Manager\StockManager;
use AppBundle\Services\Address\SuggestionService;
use AppBundle\Services\CartService;
use AppBundle\Services\Company\CompanyCustomFieldService;
use AppBundle\Services\DeliveryAddress;
use AppBundle\Services\Designer;
use AppBundle\Services\Discount\VoucherService;
use AppBundle\Services\Domain;
use AppBundle\Services\GoogleAnalytics;
use AppBundle\Services\Parameter\ParameterService;
use AppBundle\Services\PaymentService;
use AppBundle\Services\SiteService;
use AppBundle\Services\Webservices\WebservicesNL;
use AppBundle\Utils\DeliveryDate;
use AppBundle\Utils\DeliveryDates;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use JMS\JobQueueBundle\Entity\Job;
use Knp\Component\Pager\Paginator;
use League\Flysystem\FileNotFoundException;
use OpenCloud\Common\Exceptions\ForbiddenOperationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Exception\RuntimeException;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Translation\Common\Model\Message;

/**
 * Class CheckoutController
 *
 * @package AppBundle\Controller
 */
class CheckoutController extends Controller
{
    public const CUSTOMER_GROUP_CUSTOMER = 1;
    public const CUSTOMER_GROUP_COMPANY = 2;

    /**
     * @var ProductPriceManager
     */
    protected $productPriceManager;

    /**
     * @return array
     */
    public static function getTranslationMessages(): array
    {
        return [
            new Message('address.attn.not_blank', 'validators'),
            new Message('address.company.not_blank', 'validators'),
            new Message('address.type.metadata.not_blank', 'validators'),
            new Message('cart.customer.email.not_allowed', 'validators'),
            new Message('checkout.customer_type.not_blank', 'validators'),
            new Message('customer.email.not_unique', 'validation'),
            new Message('customer.login.invalid_combination', 'validators'),
            new Message('customer.password.not_blank', 'validators'),
            new Message('label.day_after_tomorrow', 'messages'),
            new Message('label.delivery_method.delivery', 'messages'),
            new Message('label.delivery_method.pickup', 'messages'),
            new Message('label.today', 'messages'),
            new Message('label.tomorrow', 'messages'),
            new Message('paymentmethod.not_blank', 'validators'),
        ];
    }

    /**
     * @Route("/bestelling/extra-speciaal/{cartOrder}/{cartOrderLine}", methods={"GET", "POST"})
     * @Template()
     *
     * @param CartOrder $cartOrder
     * @param CartOrderLine $cartOrderLine
     * @param Request $request
     *
     * @return array|Response
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     */
    public function additionalProductAction(CartOrder $cartOrder, CartOrderLine $cartOrderLine, Request $request)
    {
        $product = $cartOrderLine->getProduct();
        // If there is no order or the product doesn't have additional products redirect back to the product page
        if (!$this->get(CartService::class)->getCurrentOrder() || $product->getAdditionalProductAssortments()->isEmpty()) {
            return $this->redirect($product->getUrl());
        }

        $assortments = $product->getAdditionalProductAssortments();

        $products = [];

        /** @var AssortmentDecorator $assortment */
        foreach ($assortments as $assortment) {
            foreach ($assortment->getProducts()->toArray() as $key => $value) {
                $products[] = $value;
            }
        }

        // Remove products if the are not orderable
        $products = array_filter($products, function (Product $item) {
            return $this->get(StockManager::class)->isOrderable($item);
        });

        if (!$products) {
            return $this->redirectToRoute('app_checkout_delivery', [
                'cartOrder' => $cartOrder->getUuid(),
                'cartOrderLine' => $cartOrderLine->getUuid(),
            ]);
        }

        $paginator = new Paginator();
        $pagination = $paginator->paginate($products, $request->query->get('page', 1), 12);
        $paginationUrl = null;

        if ($request->isXmlHttpRequest()) {
            $view = $this->renderView('@App/blocks/Checkout/additionalProductOverview.html.twig', [
                'products' => $pagination,
                'order' => $cartOrder,
                'orderLine' => $cartOrderLine,
            ]);

            $result = [
                'html' => $view,
            ];

            if ($paginationUrl) {
                $result['paginationUrl'] = $paginationUrl;
            }

            return new JsonResponse($result);
        }

        $router = $this->container->get('router');

        if ($cartOrder->isDeliveryInformationComplete()) {
            $forwardUrl = $router->generate('app_checkout_sender');
        } else {
            $forwardUrl = $router->generate('app_checkout_delivery', [
                'cartOrder' => $cartOrder->getUuid(),
                'cartOrderLine' => $cartOrderLine->getUuid(),
            ]);
        }

        return [
            'products' => $pagination,
            'orderLine' => $cartOrderLine,
            'forwardUrl' => $forwardUrl,
        ];
    }

    /**
     * Method to add product to the cart by XHR request
     *
     * @Route("/bestelling/product-toevoegen/{id}/{parentCartOrderLine}", defaults={"parentCartOrderLine" = null},
     *     methods={"POST"})
     *
     * @param Product $product
     * @param Request $request
     * @param CartOrderLine $parentCartOrderLine
     *
     * @return JsonResponse
     *
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws CartInvalidSupplierCombinationException
     * @throws NoResultException
     *
     * @ParamConverter("product", options={"mapping": { "id" = "uuid"}})
     * @ParamConverter("parentCartOrderLine", options={"mapping": { "parentCartOrderLine" = "uuid"}})
     *
     */
    public function addProductToCartAction(
        Product $product,
        Request $request,
        ?CartOrderLine $parentCartOrderLine = null
    ): JsonResponse {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException();
        }

        $cart = $this->get(CartService::class);
        $entityManager = $this->getDoctrine()->getManager();

        if (($currentOrder = $cart->getCurrentOrder()) === null) {
            throw new BadRequestHttpException('No order found');
        }

        $cartOrderLine = $currentOrder->addProduct($product);

        // If additional product is added, add metadata to cartOrderLine
        if (null !== $parentCartOrderLine
            && null !== $parentCartOrderLine->getProduct()
        ) {
            $metadata = $cartOrderLine->getMetadata();
            $metadata['additional_product_parent'] = $parentCartOrderLine->getProduct()->getId();

            $cartOrderLine->setMetadata($metadata);
            $cartOrderLine->setCartOrder($parentCartOrderLine->getCartOrder());
            $cartOrderLine->setParent($parentCartOrderLine);

            $entityManager->persist($cartOrderLine);
            $entityManager->flush();
        }

        return new JsonResponse(true);
    }

    /**
     * @Route("/bestelling/verwerking/{cartOrder}/{cartOrderLine}")
     * @param CartOrder $cartOrder
     * @param CartOrderLine $cartOrderLine
     *
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     * @return RedirectResponse
     */
    public function orderAction(CartOrder $cartOrder, CartOrderLine $cartOrderLine): RedirectResponse
    {
        if (!$cartOrder->getLines()->contains($cartOrderLine)) {
            throw new BadRequestHttpException();
        }

        // Check if additional products is an option
        if (!$cartOrderLine->getProduct()->getAdditionalProductAssortments()->isEmpty()) {
            return $this->redirectToRoute('app_checkout_additionalproduct', [
                'cartOrder' => $cartOrder->getUuid(),
                'cartOrderLine' => $cartOrderLine->getUuid(),
            ]);
        }

        return $this->redirectToRoute('app_checkout_delivery', [
            'cartOrder' => $cartOrder->getUuid(),
            'cartOrderLine' => $cartOrderLine->getUuid(),
        ]);
    }

    /**
     * @Route("/bestelling/personalisatie-toevoegen/{cartOrder}/{cartOrderLine}")
     *
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     * @param CartOrder $cartOrder
     * @param CartOrderLine $cartOrderLine
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function addPersonalizationAction(CartOrder $cartOrder, CartOrderLine $cartOrderLine): RedirectResponse
    {
        $redirectParameters = [
            'cartOrder' => $cartOrder->getUuid(),
            'cartOrderLine' => $cartOrderLine->getUuid(),
        ];

        // If there is a personalization present redirect to edit page
        if ($cartOrderLine->hasProductPersonalization()) {
            return $this->redirectToRoute('app_checkout_personalization', $redirectParameters);
        }
        // If the current product does not support personalization redirect to default checkout process
        if (!$cartOrderLine->getProduct()->isProductPersonalizationAvailable()) {
            return $this->redirectToRoute('app_checkout_order', $redirectParameters);
        }

        $designer = $this->get(Designer::class);
        $personalizationProduct = $cartOrderLine->getProduct()->findProductProperty('personalization')->getProductVariation();

        $personalizationCartOrderLine = $cartOrder->addProduct($personalizationProduct, null, $cartOrderLine);

        $personalizationCartOrderLine->setMetadata([
            'designer' => $designer->getUuid(),
        ]);

        $this->getDoctrine()->getManager()->persist($personalizationCartOrderLine);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('app_checkout_personalization', $redirectParameters);
    }

    /**
     * @Route("/bestelling/personaliseren/{cartOrder}/{cartOrderLine}/screenshot")
     * @Template("@App/Checkout/personalization-screenshot.html.twig")
     *
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     * @param Request $request
     * @param CartOrder $cartOrder
     * @param CartOrderLine|null $cartOrderLine
     *
     * @return array|JsonResponse|RedirectResponse
     * @throws CartInvalidSupplierCombinationException
     * @throws Exception
     */
    public function convertPersonalization(Request $request, CartOrder $cartOrder, ?CartOrderLine $cartOrderLine = null)
    {
        /** todo: SF4 */
        if ($this->container->has('profiler')) {
            $profiler = $this->container->get('profiler');
            $profiler->disable();
        }

        $screenshotModus = true;

        $params = $this->personalization($request, $cartOrder, $cartOrderLine, $screenshotModus);

        /** @var Designer $designer */
        $designer = $params['designer'];

        $config = $designer->getConfig();
        $params['screenshotModus'] = $screenshotModus;
        $params['width'] = $designer->convertCmToPx($config->dimensions->width);
        $params['height'] = $designer->convertCmToPx($config->dimensions->height);

        return $params;
    }

    /**
     * @Route("/bestelling/personaliseren/{cartOrder}/{cartOrderLine}/{screenshotModus}")
     * @Template()
     *
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     * @param Request $request
     * @param CartOrder $cartOrder
     * @param CartOrderLine $cartOrderLine
     *
     * @param bool $screenshotModus
     *
     * @return array|JsonResponse|RedirectResponse
     * @throws CartInvalidSupplierCombinationException
     * @throws Exception
     */
    public function personalization(
        Request $request,
        CartOrder $cartOrder,
        ?CartOrderLine $cartOrderLine = null,
        bool $screenshotModus = false
    ) {
        $session = $this->get('session');
        $designer = $this->get(Designer::class);

        $designer->setScreenshotModus($screenshotModus);

        /** @var EntityManagerInterface $em */
        $em = $this->getDoctrine()->getManager();

        if (null !== $cartOrderLine && null === $cartOrderLine->getUuid()) {
            $cartOrderLine = null;
        }

        if (null !== $cartOrderLine) {
            $personalizationLine = $cartOrderLine->getPersonalizationLine();

            if (null === $personalizationLine) {
                return $this->redirectToRoute('app_checkout_order', [
                    'cartOrder' => $cartOrder->getUuid(),
                    'cartOrderLine' => $cartOrderLine->getUuid(),
                ]);
            }

            /** @var Product $product */
            $product = $cartOrderLine->getProduct();

            if ($personalizationLine->getProduct()->hasPersonalization()) {
                $personalization = $personalizationLine->getProduct()->getPersonalizations()->current();
            } else {
                /** @var Personalization $personalization */
                $personalization = $personalizationLine->getProduct();
            }

            $metadata = $personalizationLine->getMetadata();

            $designerUuid = $metadata['designer'];
        } else {
            if (!$session->has('current_product')) {
                return $this->redirectToRoute('app_checkout_sender');
            }

            $currentProduct = $session->get('current_product');

            /** @var Product $product */
            $product = $em->getRepository(Product::class)->findOneBy(['uuid' => $currentProduct['product_uuid']]);

            /** @var Personalization $personalization */
            $personalization = $em->getRepository(Personalization::class)->findOneBy(['uuid' => $currentProduct['product_personalization_uuid']]);

            if (!empty($currentProduct['options']['designer'])) {
                $designerUuid = $currentProduct['options']['designer'];
            }
        }

        // Assign generated designer uuid.
        if (!empty($designerUuid)) {
            $designer->setUuid($designerUuid);
        } else {
            throw new BadRequestHttpException('Required "uuid" for designer not found.');
        }

        $preDesign = null;
        if (isset($product->getMetadata()['design'])) {
            $preDesign = $product->getMetadata()['design'];
        }

        if (!$preDesign && $product instanceof CompanyProduct) {
            $preDesign = $product->getRelatedProduct()->getMetadata()['design'];
        }

        // Check if the product (combination) has predesign.
        if (!empty($preDesign)) {
            $designer->setTemplateUuid($preDesign);

            if (null === $designer->getTemplate()) {
                $designer->setTemplateUuid(null);
            }
        }

        // Used for product title, etc.
        $designer->setProduct($product);

        // Used for canvas size.
        $designer->setPersonalization($personalization);

        if (\in_array($this->container->get('kernel')->getEnvironment(), ['dev', 'test'])) {
            $designer->setDebugMode(true);
        }

        $designer->setApiUrl('/designer');

        if ($request->getMethod() === Request::METHOD_POST) {
            $uploadedFile = $request->files->get('file');
            $data = $designer->processUpload();

            // Return uploaded image url directly
            if (null !== $uploadedFile) {
                return new JsonResponse($data);
            }

            if (null === $cartOrderLine && $session->has('current_product')) {
                $currentProduct = $session->get('current_product');

                $card = null;

                if (!empty($currentProduct['product_card_uuid'])) {
                    $card = $em->getRepository(ProductCard::class)->findOneBy(['uuid' => $currentProduct['product_card_uuid']]);
                }

                $options = $currentProduct['options'] ?? [];

                $cartOrderLine = $this->get(CartService::class)->addProduct($product, $card, $personalization,
                    $options);

                foreach ($currentProduct['upsells'] as $productId => $quantity) {
                    $upsellProduct = $em->getRepository(Product::class)->find($productId);
                    $cartOrder->addProduct($upsellProduct, $quantity, $cartOrderLine, true, [
                        'upsell' => true,
                    ]);
                }
            }

            if (null === $cartOrderLine) {
                throw new \RuntimeException('No cart order line found.');
            }

            $session->remove('current_product');

            return new JsonResponse([
                'returnUrl' => $returnUrl = $this->generateUrl('app_checkout_order', [
                    'cartOrder' => $cartOrderLine->getCartOrder()->getUuid(),
                    'cartOrderLine' => $cartOrderLine->getUuid(),
                ]),
            ]);
        }

        if (null !== $cartOrderLine) {
            $returnUrl = $this->generateUrl('app_checkout_order', [
                'cartOrder' => $cartOrder->getUuid(),
                'cartOrderLine' => $cartOrderLine->getUuid(),
            ]);

            $designer->setReturnUrl($returnUrl);
            $designer->setCancelUrl($returnUrl);
        } else {
            $designer->setReturnUrl($product->getUrl());

            $designer->setCancelUrl($product->getUrl());
        }

        return [
            'designer' => $designer,
            'product' => $product,
        ];
    }

    /**
     * @Route("/bestellingen/personalisatie-annuleren/{cartOrder}/{cartOrderLine}", methods={"GET"})
     *
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     * @param Request $request
     * @param CartOrder $cartOrder
     * @param CartOrderLine $cartOrderLine
     *
     * @return RedirectResponse
     */
    public function cancelPersonalizationAction(
        Request $request,
        CartOrder $cartOrder = null,
        CartOrderLine $cartOrderLine = null
    ): RedirectResponse {

        if (!$cartOrder || !$cartOrderLine) {
            return $this->redirect('/');
        }

        $product = $cartOrderLine->getProduct();
        $personalizationLine = $cartOrderLine->getPersonalizationLine();

        if (null === $personalizationLine || !isset($personalizationLine->getMetadata()['designer_saved'])) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cartOrderLine);
            if ($cartOrder->getLines()->count() > 1) {
                $em->remove($cartOrder);
            }
            $em->flush();
        }

        $forwardUrl = $request->get('forward_url');

        if ($forwardUrl && strpos($forwardUrl, $request->getHost()) !== false) {
            return $this->redirect($request->get('forward_url'));
        }

        // Forward user back to checkout page when design is already saved.
        if (isset($personalizationLine->getMetadata()['designer_saved'])) {
            return $this->redirectToRoute('app_checkout_order', [
                'cartOrder' => $cartOrder->getUuid(),
                'cartOrderLine' => $cartOrderLine->getUuid(),
            ]);
        }

        if ($product->isCombination()) {
            $redirectUrl = $product->getUrl();
        } else {
            $redirectUrl = $product->getParent()->getUrl();
        }

        // Return to the page of the base product
        return $this->redirect($redirectUrl);
    }

    /**
     * @Route("/bestelling/{id}/bewerken", methods={"POST"})
     *
     * @param Request $request
     * @param CartOrder $cartOrder
     *
     * @return Response
     * @throws Exception
     */
    public function orderUpdateAction(Request $request, CartOrder $cartOrder): ?Response
    {
        try {
            $appCartOrder = $this->get(CartService::class)->getOrder($cartOrder);

            if (null === $appCartOrder) {
                throw new RuntimeException('Cart order not found.');
            }

            $this->updateCartOrder($appCartOrder, $request);

            if ($request->isXmlHttpRequest()) {
                return new Response();
            }

            $this->get('session')->getFlashBag()->add('frontend.success', 'checkout.order.update.success');
        } catch (Exception $e) {
            if (!$request->isXmlHttpRequest()) {
                $this->get('session')->getFlashBag()->add('frontend.error', 'checkout.order.update.error');
            }

            throw $e;
        }

        return new Response();
    }

    /**
     *
     * @param CartOrder $cartOrder
     * @param Request $request
     *
     * @return boolean
     * @throws Exception
     */
    private function updateCartOrder(CartOrder $cartOrder, Request $request): bool
    {
        foreach ($request->request->get('cart_order')['lines'] as $key => $line) {
            if (!array_key_exists($key, $cartOrder->getLines())) {
                continue;
            }

            $cartOrder->getLines()[$key]->setQuantity($line['quantity']);
        }

        return true;
    }

    /**
     * @Route("/bestelling/{cartOrder}/{cartOrderLine}/bewerken", methods={"POST"})
     *
     * @param Request $request
     * @param CartOrder $cartOrder
     * @param CartOrderLine $cartOrderLine
     *
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     * @return Response
     * @throws Exception
     */
    public function orderLineUpdateAction(
        Request $request,
        CartOrder $cartOrder,
        CartOrderLine $cartOrderLine
    ): ?Response {
        try {
            $this->updateCartOrderLine($cartOrderLine, $request);

            if ($request->isXmlHttpRequest()) {
                // When the last product is deleted also remove the Cart Order
                $remainingProducts = $cartOrder->getLines()->filter(function (CartOrderLine $cartOrderLine) {
                    return ($cartOrderLine->getProduct() instanceof GenericProduct);
                });

                if ($remainingProducts->isEmpty()) {
                    $this->get(CartService::class)->removeOrder($cartOrder);
                }

                return new JsonResponse([
                    'count' => $this->get(CartService::class)->count(),
                ]);
            }

            $this->get('session')->getFlashBag()->add('frontend.success', 'checkout.order.update.success');

            return $this->redirect($this->generateUrl('app_checkout_order'));
        } catch (Exception $e) {
            if (!$request->isXmlHttpRequest()) {
                $this->get('session')->getFlashBag()->add('frontend.error', 'checkout.order.update.error');

                return $this->redirect($this->generateUrl('app_checkout_order'));
            }

            throw $e;
        }
    }

    /**
     *
     * @param CartOrderLine $cartOrderLine
     * @param Request $request
     *
     * @return bool
     * @throws Exception
     */
    private function updateCartOrderLine(CartOrderLine $cartOrderLine, Request $request): bool
    {
        $cartOrderLine->setQuantity($request->request->get('quantity'));

        return true;
    }

    /**
     * @Route("/bestelling/{cartOrder}/dupliceer", methods={"GET"})
     *
     * @param Request $request
     *
     * @param CartOrder $cartOrder
     *
     * @return array|RedirectResponse
     * @throws Exception
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     *
     */
    public function duplicateOrderAction(Request $request, CartOrder $cartOrder)
    {
        try {
            $duplicatedOrder = $this->get(CartService::class)->duplicateOrderForAnotherAddress($cartOrder);
            $this->get('session')->getFlashBag()->add('frontend.success', 'cart.order_duplicate.success_alert');
            return $this->redirect($this->generateUrl('app_checkout_delivery', [
                'cartOrder' => $duplicatedOrder->getUuid(),
            ]));
        } catch (FileNotFoundException $e) {
            //TODO SF4
            $this->get('logger')->error(sprintf('Failed to copy personalisation of cart order %d: %s',
                $cartOrder->getId(), $e->getMessage()));
            $this->get('session')->getFlashBag()->add('frontend.warning', 'cart.order_duplicate.designer_error_alert');
            return $this->redirect($this->generateUrl('app_checkout_sender'));
        }
    }

    /**
     * @Route("/bestelling/adresboek", methods={"POST"})
     *
     * @param Request $request
     *
     * @return array|Response
     */
    public function addressBookPostAction(Request $request)
    {
        $session = $request->getSession();
        if ($session) {
            $session->set('defaultAddress', json_encode($request->request->all()));
        }

        return new Response('true');
    }

    /**
     * @Route("/bestelling/adresboek", methods={"GET"})
     *
     * @return array|Response
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function addressBookAction()
    {
        $orderCollection = $this->get(CartService::class)->getOrders();
        $suggestions = [
            'order' => [],
            'addressBook' => [],
        ];

        foreach ($orderCollection as $order) {
            if ($order->getDeliveryAddressStreet() !== null) {
                $suggestions['order'][] = [
                    'type' => $order->getDeliveryAddressType() ? $order->getDeliveryAddressType()->getId() : 1,
                    'companyName' => $order->getDeliveryAddressCompanyName(),
                    'attn' => $order->getDeliveryAddressAttn(),
                    'street' => $order->getDeliveryAddressStreet(),
                    'number' => $order->getDeliveryAddressNumber(),
                    'numberAddition' => $order->getDeliveryAddressNumberAddition(),
                    'postcode' => $order->getDeliveryAddressPostcode(),
                    'city' => $order->getDeliveryAddressCity(),
                    'country' => $order->getDeliveryAddressCountry() ? $order->getDeliveryAddressCountry()->getId() : null,
                    'phone' => $order->getDeliveryAddressPhoneNumber(),
                    'meta' => $order->getMetadata(),
                ];
            }
        }

        /** @var Customer $user */
        $user = $this->getUser();
        if ($user !== null) {
            $addresses = array_unique(array_merge($user->getAddresses()->toArray(),
                $user->getCompany() !== null ? $user->getCompany()->getAddresses()->toArray() : []));
            /** @var Address $address */
            foreach ($addresses as $address) {
                if ($address instanceof Address && stripos($address->getStreet(), 'postbus') === false) {
                    $suggestions['addressBook'][] = [
                        'type' => $address->getCompanyName() !== null ? 2 : 1,
                        'companyName' => $address->getCompanyName(),
                        'attn' => $address->getAttn(),
                        'street' => $address->getStreet(),
                        'number' => $address->getNumber(),
                        'numberAddition' => $address->getNumberAddition(),
                        'postcode' => $address->getPostcode(),
                        'city' => $address->getCity(),
                        'country' => $address->getCountry() ? $address->getCountry()->getId() : null,
                        'phone' => $address->getPhoneNumber(),
                        'meta' => [],
                    ];
                }
            }
        }

        return $this->render('@App/Checkout/components/delivery/address-list.html.twig', [
            'suggestions' => $suggestions,
        ]);
    }

    /**
     * @Route("/bestelling/bezorging/{cartOrder}/{cartOrderLine}", defaults={"cartOrderLine" = ""},
     *     methods={"GET", "POST"})
     * @Template()
     *
     * @param Request $request
     *
     * @param CartOrder $cartOrder
     * @param CartOrderLine $cartOrderLine
     *
     * @return array|RedirectResponse
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     * @throws Exception
     */
    public function deliveryAction(Request $request, CartOrder $cartOrder, CartOrderLine $cartOrderLine = null)
    {
        $orderLines = [];
        $user = null;
        $parameterService = $this->get(ParameterService::class);

        if ($token = $this->container->get('security.token_storage')->getToken()) {
            $user = $token->getUser();
        }

        $cart = $this->get(CartService::class);
        $useDefaultAddressAsDeliveryAddress = $parameterService->setEntity($cart->getCart()->getSite())->getValue('use_default_address_as_delivery_address');

        /* @var CartOrder $currentOrder */
        $currentOrder = $cart->getOrder($cartOrder);

        if (null === $currentOrder) {
            return $this->redirect($this->generateUrl('app_checkout_sender'));
        }

        if ($cartOrderLine) {
            $orderLines[] = $cartOrderLine;
        } else {
            foreach ($cartOrder->getLines() as $line) {
                if ($line->getParent() === null && $line->getProduct() instanceof GenericProduct && !isset($line->getMetadata()['additional_product_parent'])) {
                    $orderLines[] = $line;
                }
            }
        }

        $form = $this->getCartOrderForm($request, 2, $cartOrder, $cartOrderLine);
        $deliveryAddressTypeErrors = [];

        if ($request->isMethod('POST')) {
            if ($user !== null && $user !== 'anon.') {
                $deliveryAddress = null;

                if ($user->getCompany() && $user->getCompany()->getDefaultDeliveryAddress()) {
                    $deliveryAddress = $user->getCompany()->getDefaultDeliveryAddress();
                } elseif ($user->getDefaultDeliveryAddress()) {
                    $deliveryAddress = $user->getDefaultDeliveryAddress();
                }

                // Check "session" attn in order to determine if loading the address from the user is needed
                if ($deliveryAddress && !$currentOrder->getDeliveryAddressAttn() && $useDefaultAddressAsDeliveryAddress) {
                    /* @var Address $deliveryAddress */

                    $form->get('deliveryAddressCompanyName')->setData($deliveryAddress->getCompanyName());
                    $form->get('deliveryAddressAttn')->setData($deliveryAddress->getAttn());
                    $form->get('deliveryAddressPostcode')->setData($deliveryAddress->getPostcode());
                    $form->get('deliveryAddressNumber')->setData($deliveryAddress->getNumber());
                    $form->get('deliveryAddressNumberAddition')->setData($deliveryAddress->getNumberAddition());
                    $form->get('deliveryAddressStreet')->setData($deliveryAddress->getStreet());
                    $form->get('deliveryAddressCity')->setData($deliveryAddress->getCity());
                    $form->get('deliveryAddressPhoneNumber')->setData($deliveryAddress->getPhoneNumber());
                }
            }

            if ($request->request->has('deliveryMethod')) {
                if ($request->request->get('deliveryMethod') === 'pickup') {
                    if (!$request->request->has('pickupLocation')
                        && !$request->request->has('pickupLocationTimeslotDay')
                        && !$request->request->has('pickupLocationTimeslot')) {
                        $form->addError(new FormError('label.timeslot.not_selected'));
                    }
                    if (!$this->container->get(CartOrderManager::class)->isPickupPossible($cartOrder)) {
                        $form->addError(new FormError('label.pickup.not_possible'));
                    }

                    /** @var PickupLocation $pickupLocation */
                    $pickupLocation = $this->get('doctrine')->getRepository(PickupLocation::class)->find($request->request->get('pickupLocation'));

                    $currentOrder->setDeliveryDate(new \DateTime($request->request->get('pickupLocationTimeslotDay')));

                    if ($form->has('deliveryDate')) {
                        $form->get('deliveryDate')->setData($currentOrder->getDeliveryDate());
                    }

                    $currentOrder->setPickupAddress($pickupLocation->getCompanyEstablishment()->getAddress());
                    $currentOrder->setTimeslot($request->request->get('pickupLocationTimeslot'));

                } else {
                    $currentOrder->setPickupAddress();
                }
            }

            $form->handleRequest($request);

            if ($request->request->get('deliveryMethod') === 'pickup') {
                $currentOrder->setDeliveryAddress();
                $currentOrder->setDeliveryAddressCompanyName(null);
                $currentOrder->setDeliveryAddressAttn(null);
                $currentOrder->setDeliveryAddressStreet(null);
                $currentOrder->setDeliveryAddressNumber(null);
                $currentOrder->setDeliveryAddressNumberAddition(null);
                $currentOrder->setDeliveryAddressPostcode(null);
                $currentOrder->setDeliveryAddressCity(null);
                $currentOrder->setDeliveryAddressPhoneNumber(null);
            }

            if (!$request->request->has('deliveryMethod') || $request->request->get('deliveryMethod') !== 'pickup') {
                /** @var DeliveryAddressType $deliveryAddressType */
                $deliveryAddressType = $form->get('deliveryAddressType')->getData();

                if ($deliveryAddressType && $deliveryAddressType instanceof DeliveryAddressType) {
                    /** @var DeliveryAddressTypeTranslation $translations */
                    $translations = $deliveryAddressType->translate();

                    if (!$deliveryAddressType->getHideCompanyField() && !$form->get('deliveryAddressCompanyName')->getData()) {

                        $form->get('deliveryAddressCompanyName')->addError(
                            new FormError('address.company.not_blank', null, [
                                '{{ label }}' => $translations->getCompanyLabel(),
                            ])
                        );
                    }

                    if (!$form->get('deliveryAddressAttn')->getData()) {
                        $form->get('deliveryAddressAttn')->addError(
                            new FormError('address.attn.not_blank', null, [
                                '{{ label }}' => $translations->getRecipientLabel(),
                            ])
                        );
                    }

                    $deliveryAddressTypeMetadata = [];
                    $metadata = $cartOrder->getMetadata();

                    if (!$deliveryAddressType->getFields()->isEmpty()) {
                        foreach ($deliveryAddressType->getFields() as $field) {
                            if ($field->getRequired() && empty($request->request->get('deliveryAddressTypeMetadata')[$field->getId()])) {
                                $form->addError(
                                    new FormError('address.type.metadata.not_blank', null, [
                                        '{{ label }}' => $field->translate()->getLabel(),
                                    ])
                                );

                                $deliveryAddressTypeErrors[] = 'delivery_address_type_field_' . $field->getId();
                            }

                            $deliveryAddressTypeMetadata[] = [
                                'id' => $field->getId(),
                                'label' => $field->translate()->getLabel(),
                                'value' => empty($request->request->get('deliveryAddressTypeMetadata')[$field->getId()]) ? null : $request->request->get('deliveryAddressTypeMetadata')[$field->getId()],
                            ];
                        }
                    }

                    $metadata['deliveryAddressTypeFields'] = $deliveryAddressTypeMetadata;
                    $cartOrder->setMetadata($metadata);
                }
            }

            if ($request->request->get('deliveryMethod') === 'delivery' && !empty($request->request->get('cart_order')['deliveryDate'])) {
                $postedDeliveryDate = $request->request->get('cart_order')['deliveryDate'];
                if (!$currentOrder->getDeliveryDates()->getAvailable()->exists(function (
                    $key,
                    DeliveryDate $deliveryDate
                ) use ($postedDeliveryDate) {
                    void($key);

                    return $deliveryDate->format('Y-m-d') === $postedDeliveryDate;
                })) {
                    $form->addError(new FormError('delivery_date.not_blank'));
                }
            }

            if ($form->isSubmitted() && $form->isValid()) {
                //handle service
                try {
                    $this->container->get('delivery')->setDeliveryAddressForOrder($cartOrder, $request);

                    if ($request->get('continue_with_address')) {
                        $request->getSession()->set('continueWithAddress', 1);
                    }

                    return $this->redirect($this->generateUrl('app_checkout_sender'));
                } catch (\Exception $e) {
                    $form->addError(new FormError('checkout.unknown_error.contact_customer_service'));
                    $this->container->get('logger')->addError($e->getMessage(), [
                        'exception' => $e
                    ]);
                }
            }
        } elseif (null !== $currentOrder && !$currentOrder->getDeliveryAddressCountry()) {
            $country = $this->get(SiteService::class)->determineSite()->getCountry();
            $form->get('deliveryAddressCountry')->setData($country);
        }

        if ($user !== null && $user instanceof Customer) {

            $deliveryAddress = null;
            if (null !== $user->getCompany() && null !== $user->getCompany()->getDefaultDeliveryAddress()) {
                $deliveryAddress = $user->getCompany()->getDefaultDeliveryAddress();
            } elseif (null !== $user->getDefaultDeliveryAddress()) {
                $deliveryAddress = $user->getDefaultDeliveryAddress();
            }

            if (null !== $deliveryAddress && !$form->isSubmitted() && $useDefaultAddressAsDeliveryAddress) {
                $form->get('deliveryAddressCompanyName')->setData($deliveryAddress->getCompanyName());
                $form->get('deliveryAddressAttn')->setData($deliveryAddress->getAttn());
                $form->get('deliveryAddressPostcode')->setData($deliveryAddress->getPostcode());
                $form->get('deliveryAddressNumber')->setData($deliveryAddress->getNumber());
                $form->get('deliveryAddressNumberAddition')->setData($deliveryAddress->getNumberAddition());
                $form->get('deliveryAddressStreet')->setData($deliveryAddress->getStreet());
                $form->get('deliveryAddressCity')->setData($deliveryAddress->getCity());
                $form->get('deliveryAddressPhoneNumber')->setData($deliveryAddress->getPhoneNumber());
            }
        }

        $deliveryAddressValues = [];

        if ($cartOrder->getMetadata()) {
            $deliveryAddressValues = array_reduce($cartOrder->getMetadata(), function ($result, $item) {
                if (isset($item['id'])) {
                    $result[$item['id']] = $item;
                }

                return $result;
            }, []);
        }

        $this->get(GoogleAnalytics::class)->checkout($cart, 1);

        $session = $request->getSession();
        $defaultAddress = false;
        if ($session) {
            $defaultAddress = $session->get('defaultAddress') ?? false;
        }


        return [
            'errors' => $form->getErrors(true),
            'cartOrder' => $cartOrder,
            'cart' => $cart,
            'deliveryAddressTypes' => $this->get(DeliveryAddress::class)->getTypes(),
            'deliveryAddressValues' => $deliveryAddressValues,
            'deliveryAddressTypeErrors' => $deliveryAddressTypeErrors,
            'form' => $form->createView(),
            'currentOrder' => $currentOrder,
            'orderLines' => $orderLines,
            'defaultAddress' => $defaultAddress,
        ];
    }

    /**
     * @param Request $request
     * @param int $step
     * @param CartOrder|null $cartOrder
     * @param CartOrderLine|null $cartOrderLine
     *
     * @return FormInterface
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     * @throws ORMException
     */
    private function getCartOrderForm(
        Request $request,
        $step = 1,
        CartOrder $cartOrder = null,
        CartOrderLine $cartOrderLine = null
    ): FormInterface {

        if ($cartOrder) {
            $entity = $cartOrder;
        } else {
            $entity = $this->get(CartService::class)->getCurrentOrder();
        }

        switch ($step) {
            case 1:
                $action = $this->generateUrl('app_checkout_orderupdate', [
                    'id' => $this->get(CartService::class)->getCurrentOrder()->getId(),
                ]);

                break;

            case 2:
                $parameters = [
                    'cartOrder' => $entity->getUuid(),
                ];

                if (null !== $cartOrderLine && null !== $cartOrderLine) {
                    $parameters['cartOrderLine'] = $cartOrderLine->getUuid();
                }

                $action = $this->generateUrl('app_checkout_delivery', $parameters);
                break;
            default:
                throw new \RuntimeException('No Action provided');
        }

        $form = $this->createForm(CartOrderType::class, $entity, [
            'action' => $action,
            'method' => 'POST',
            'step' => $step,
            'validation_groups' => function () use ($request, $step) {
                if ($step === 2) {
                    if ($request->request->has('deliveryMethod') && $request->request->get('deliveryMethod') === 'pickup') {
                        return ['cart_pickup'];
                    }

                    $groups = ['cart_delivery'];

                    if ($this->get(Domain::class)->getDomain()->getSite()->getDeliveryDateMethod() === 'required') {
                        $groups[] = 'cart_delivery_date';
                    }

                    return $groups;
                }

                return [];
            },
        ]);

        if ($form->has('deliveryAddressType') && \count($this->get(DeliveryAddress::class)->getTypes()) === 1) {
            $form->get('deliveryAddressType')->setData(array_values($this->get(DeliveryAddress::class)->getTypes())[0]);
        }

        if (!$this->get(Domain::class)->getDomain()->getSite()->getDeliveryDateMethod()) {
            $form->remove('deliveryDate');
        }

        $form->add('submit', SubmitType::class, ['label' => 'label.next_step', 'translation_domain' => 'messages']);

        return $form;
    }

    /**
     * @Route("/bestelling/{uuid}/alle-personalisaties", methods={"GET"})
     * @Template()
     *
     * @param CartOrderLine $cartOrderLine
     *
     * @return array
     */
    public function listPersonalizationAction(CartOrderLine $cartOrderLine): array
    {
        return [
            'orderLine' => $cartOrderLine,
        ];
    }

    /**
     * @Route("/bestelling/{uuid}/alle-extra-producten", methods={"GET"})
     * @Template()
     *
     * @param CartOrderLine $cartOrderLine
     * @param Request $request
     *
     * @return array
     */
    public function listAdditionalProductAction(CartOrderLine $cartOrderLine, Request $request): array
    {
        return [
            'orderLine' => $cartOrderLine,
            'max' => $request->query->get('max', false),
        ];
    }

    /**
     * @Route("/bestelling/uw-gegevens", methods={"GET", "POST"})
     * @Template()
     *
     * @param Request $request
     *
     * @return Response|array
     * @throws \Exception
     * @throws ConnectionException
     * @throws OptimisticLockException
     * @throws \Exception
     */
    public function senderAction(Request $request)
    {
        $invalidVoucher = false;
        $userRegistration = false;
        $cart = $this->get(CartService::class);
        $orders = $cart->getOrders();

        if (!$cart->count()) {
            return $this->redirect($this->generateUrl('app_home_index'));
        }

        if (
            $cart->getCart()->getOrders()->count() === 1
            && $cart->getCart()->getSite()->getAddToCartModalEnabled()
            && !$cart->getCart()->getOrders()->current()->isDeliveryInformationComplete()
        ) {
            $currentOrder = $cart->getCart()->getOrders()->current();

            return new RedirectResponse($this->generateUrl('app_checkout_delivery',
                ['cartOrder' => $currentOrder->getUuid()]));
        }

        if (null !== $request->getSession() && $request->getSession()->has('invalidVoucher')) {
            $request->getSession()->remove('invalidVoucher');

            $invalidVoucher = true;
        }

        foreach ($orders as $currentOrder) {
            if (!$currentOrder || $currentOrder->getLines()->isEmpty()) {
                $cart->removeOrder($currentOrder);
                continue;
            }

            if (null === $this->container->get(SiteService::class)->determineSite() && $this->container->get(SiteService::class)->determineSite()->getDeliveryDateMethod() === 'required' && !$currentOrder->getDeliveryDate()) {
                return $this->redirect($this->generateUrl('app_checkout_delivery', [
                    'cartOrder' => $currentOrder->getUuid(),
                ]));
            }
        }

        $form = $this->getCartForm($request);
        $form->handleRequest($request);
        $paymentMethod = !empty($request->get('cart')['paymentMethod']) ? $request->get('cart')['paymentMethod'] : null;

        if (!$paymentMethod) {
            /** @var Cart $entity */
            $entity = $this->get(CartService::class)->getCart();

            /** @var OrderCollection $orderCollection */
            if (($orderCollection = $entity->getOrderCollection()) && ($lastPayment = $orderCollection->getPayments()->last())) {
                $paymentMethod = $lastPayment->getPaymentmethod()->getCode();
            }
        }
        /**
         * @var Form $paymentForm ;
         */
        if ($form->isSubmitted()) {
            $customerTypeData = $form->has('customerType') ? strtolower($form->get('customerType')->getData()) : null;

            /**
             * Optional additional checks in case of company registration
             */
            if ($form->has('customerType') && strtolower($form->get('customerType')->getData()) === 'company' // Check if customer type is company
                && ($form->has('companyAccountRegistration') && (bool)$form->get('companyAccountRegistration')->getData() === true) // Check if visitor wants to register a official company
                && strtolower($form->get('customer')->get('country_choice')->getData()) !== 'other') {
                $companyRegistrationError = false;

                $companyRegistrationParameters = [
                    'country' => strtolower($form->get('customer')->get('country_choice')->getData()),
                    'email' => $form->get('customer')->get('email')->getData(),
                    'company_identification' => null,
                ];

                switch ($companyRegistrationParameters['country']) {
                    case 'nl':
                        $chamberOfCommerceNumber = null;

                        if (!$form->get('chamberOfCommerce')->getData()) {
                            $this->get('session')->getFlashBag()->add('frontend.error',
                                $this->get('translator')->trans('company.chamber_of_commerce.not_blank'));

                            $companyRegistrationError = true;
                        } else {
                            $chamberOfCommerceNumber = $form->get('chamberOfCommerce')->getData();

                            try {
                                $this->get(WebservicesNL::class)->isValidChamberOfCommerceNumber($chamberOfCommerceNumber);
                            } catch (\Exception $e) {
//                                $this->get('session')->getFlashBag()->add('frontend.error', $e->getMessage());
                                $this->get('session')->getFlashBag()->add('frontend.error',
                                    $this->get('translator')->trans('company.chamber_of_commerce.not_valid'));
                                $companyRegistrationError = true;
                            }
                        }

                        $companyRegistrationParameters['company_identification'] = $chamberOfCommerceNumber;

                        break;
                    case 'be':
                        $vatNumber = null;

                        if (!$form->get('vatNumber')->getData()) {
                            $this->get('session')->getFlashBag()->add('frontend.error',
                                $this->get('translator')->trans('company.vat_number.not_blank'));

                            $companyRegistrationError = true;
                        } else {
                            $vatNumber = $form->get('vatNumber')->getData();

                            if (!$this->get('ec_europa_eu.vies')->isValidVatNumber($vatNumber)) {
                                $this->get('session')->getFlashBag()->add('frontend.error',
                                    $this->get('translator')->trans('company.vat_number.not_valid'));
                                $companyRegistrationError = true;
                            }
                        }

                        $companyRegistrationParameters['company_identification'] = $vatNumber;

                        break;
                }
                if (!$companyRegistrationError) {
                    $this->container->get('session')->set('registration_from_checkout', true);
                    $this->container->get('session')->set('registration_from_checkout_company_identification',
                        $companyRegistrationParameters['company_identification']);
                    $this->container->get('session')->set('registration_from_checkout_country',
                        $companyRegistrationParameters['country']);
                    $this->container->get('session')->set('registration_from_checkout_email',
                        $companyRegistrationParameters['email']);
                    $this->container->get('session')->save();

                    return $this->redirectToRoute('app_customer_register_company',
                        $companyRegistrationParameters);
                }
                return $this->redirectToRoute('app_checkout_sender', []);
            }

            if ($form->has('customerType') && !$form->get('customerType')->getData() && $form->get('customerStatus')->getData() !== 'existing') {
                $form->get('customerType')->addError(
                    new FormError('checkout.customer_type.not_blank')
                );
            }

            if ($customerTypeData !== 'company' && $form->has('customer') && $form->get('customer')->has('plain_password') && $form->get('customer')->get('plain_password')->getData()) {
                $checkUserExistance = $this->get('doctrine')->getManager()->getRepository(Customer::class)->findOneBy([
                    'username' => $this->get(CartService::class)->getCart()->getCustomer()->getEmail(),
                ]);

                if ($checkUserExistance) {
                    $form->get('customer')->get('email')->addError(
                        new FormError('customer.email.not_unique')
                    );
                }

                $userRegistration = true;
            }

            // Check if the login subform is submitted and validate given input data
            if (!$this->getUser() && ($form->get('login')->get('login_form_active')->getData() || $form->get('customerStatus')->getData() === 'existing')) {

                $username = $form->get('login')->get('username')->getData();
                $password = $form->get('login')->get('plain_password')->getData();

                if (!$username || !$password) {
                    if (!$username) {
                        $form->addError(new FormError('customer.email.not_blank'));
                    }
                    if (!$password) {
                        $form->addError(new FormError('customer.password.not_blank'));
                    }
                } elseif (!$this->get('app.user_provider')->loginUser($username, $password)) {
                    $form->addError(new FormError('customer.login.invalid_combination'));
                } else {

                    return $this->redirectToRoute('app_checkout_sender');
                }

            } elseif (!$paymentMethod) {
                $form->addError(new FormError('paymentmethod.not_blank'));
            } elseif ($paymentMethod) {
                if ($form->get('site_checkout_payment')->has($paymentMethod)) {
                    $paymentForm = $form->get('site_checkout_payment')->get($paymentMethod);

                    /** @var Form $field */
                    foreach ($paymentForm->all() as $field) {
                        if (!$field->getData() && $field->getConfig()->getOption('required')) {
                            if ($field->getConfig()->getOption('invalid_message')) {
                                $field->addError(new FormError($field->getConfig()->getOption('invalid_message')));
                            } else {
                                $field->addError(new FormError($field->getName() . '.not_blank'));
                            }
                        }
                    }
                }
            }

            /** @var CartOrder $cartOrder */
            foreach ($this->get(CartService::class)->getCart()->getOrders() as $cartOrder) {
                if (!$cartOrder->isDeliveryInformationComplete()) {
                    $form->addError(new FormError('checkout.delivery_information.not_complete'));
                }
            }

            $cartErrorFlag = false;
            $cart = $this->get(CartService::class)->getCart();
            if (null !== $cart) {
                foreach ($cart->getOrders() as $order) {
                    /** @var CartOrderLine $line */
                    foreach ($order->getLines() as $line) {
                        $product = $line->getProduct();
                        $title = $product->getTitle();
                        $title = $title ?: $product->getTranslations()->get('title');
                        if ($product->getHasStock() && !$product->getNoStockBackorder() && !$this->get(StockManager::class)->getPhysicalStock($product)) {
                            $this->addFlash('frontend.error',
                                sprintf($this->get('translator')->trans('cart.product.out_of_stock'),
                                    $title));
                            $cartErrorFlag = true;
                        }
                        if (null !== ($metaData = $line->getMetadata()) && \array_key_exists('designer',
                                $metaData)) {
                            $designer = $this->container->get('app.designer');
                            $designer->setUuid($metaData['designer']);
                            if (null === $designer->getJson()) {
                                $this->addFlash('frontend.error',
                                    sprintf($this->get('translator')->trans('cart.product.design_failed'),
                                        $title));
                                $cartErrorFlag = true;
                            }
                        }
                    }
                }
            }

            $voucherService = $this->container->get(VoucherService::class);
            foreach ($cart->getVouchers() as $voucher) {
                $voucherErrors = [];
                try {
                    $voucherService->checkVoucherAvailability($voucher, false);
                } catch (VoucherUsedException $e) {
                    $voucherErrors[] = 'voucher.used';
                } catch (VoucherExpiredException $e) {
                    $voucherErrors[] = 'voucher.expired';
                }
                foreach ($voucherErrors as $voucherError) {
                    $cart->removeVoucher($voucher);
                    $cartErrorFlag = true;
                    $this->addFlash('frontend.error',
                        $this->get('translator')->trans($voucherError));
                }
            }

            if (true === $cartErrorFlag) {
                return $this->redirectToRoute('app_checkout_sender', []);
            }

            if ($form->isSubmitted() && $form->isValid()) {
                $cart = $this->get(CartService::class)->getCart();
                $invoiceAddress = $cart->getCustomer()->getInvoiceAddress();
                $invoiceAddress->setInvoiceAddress(true);

                $processNewsletter = ($form->has('customerNewsletter') && $form->get('customerNewsletter')->getData()) || ($form->has('customer') && $form->get('customer')->get('newsletter')->getData());

                //Obsolete newsletter parameter data (only used for data collection)
                $cart->getCustomer()->setNewsletter($processNewsletter);

                //Configure newsletter preferences per email instead of per customer
                if ($processNewsletter) {
                    $cartCustomer = $cart->getCustomer();

                    $newsletterPreferences = $this->getDoctrine()->getRepository(NewsletterPreferences::class)->findOneBy(['email' => $cartCustomer->getEmail()]) ?? new NewsletterPreferences();
                    $newsletterPreferences->setEmail($cartCustomer->getEmail());
                    $newsletterPreferences->setName($cartCustomer->getFullname());
                    $newsletterPreferences->setCustomer($cartCustomer ?? null);
                    $newsletterPreferences->setTypeNewsletters(true);
                    $newsletterPreferences->setSitesByDetermenSite($this->container->get('app.site')->determineSite());

                    $this->getDoctrine()->getManager()->persist($newsletterPreferences);
                }

                $cart->setInvoiceAddressAttn($cart->getCustomer()->getFullname());
                $cart->setInvoiceAddress($invoiceAddress);

                /**
                 * Check if password is posted so we can register a user account
                 */
                if ($userRegistration) {
                    $cart = $cart;
                    $customer = $cart->getCustomer();

                    $customer->setUsername($cart->getCustomer()->getEmail());
                    $customer->setEnabled(true);

                    if ($form->get('customerType')->getData() === 'company') {
                        $customerGroup = $this->getDoctrine()->getRepository(CustomerGroup::class)->find(self::CUSTOMER_GROUP_COMPANY);
                    } else {
                        $customerGroup = $this->getDoctrine()->getRepository(CustomerGroup::class)->find(self::CUSTOMER_GROUP_CUSTOMER);
                    }

                    if ($customerGroup) {
                        $customer->setCustomerGroup($customerGroup);
                    }
                }

                $invoiceAddress->setCustomer($cart->getCustomer());

                $this->get(CartService::class)->finalize();
                $orderCollection = $this->get(CartService::class)->getCart()->getOrderCollection();

                //recalculate totals before payment is created
                $this->container->get(OrderCollectionManager::class)->calculateTotals($orderCollection);

                /**
                 * When there's an user registration, send the account confirmation email
                 */
                if ($userRegistration) {
                    /** Automatically login registered customer ones he is returning to the website */
                    try {
                        $this->get('session')->set('login_user_after_returning',
                            $this->get(CartService::class)->getCart()->getCustomer()->getId());
                    } catch (\Exception $e) {
                    }

                    $this->createRegistrationMailJob($this->get(CartService::class)->getCart()->getCustomer());
                }

                //check if company fields are available
                if ($this->getUser() && $this->getUser() instanceof Customer && $this->getUser()->getCompany()) {
                    $companyCustomFieldService = $this->get(CompanyCustomFieldService::class);
                    $companyCustomFieldService->saveCustomFieldsFromForm($this->getUser()->getCompany(), $form,
                        $orderCollection);
                }

                foreach ($orderCollection->getOrders() as $order) {
                    $order->setLocale($request->getLocale());
                }

                $paymentMethodClass = $this->get(PaymentService::class)->getMethod($orderCollection, $paymentMethod);

                $payment = new Payment();
                $payment->setPaymentmethod($paymentMethodClass->getEntity());
                $payment->setOrder($orderCollection);

                if ($paymentMethodClass->getEntity()->getCode() !== 'invoice') {
                    $paymentAmount = $this->container->get(OrderCollectionManager::class)->calculateOutstanding($orderCollection);
                    $payment->setAmount($paymentAmount);
                }

                $payment->setDatetime(new \DateTime());
                $payment->setStatus($this->getDoctrine()->getManager()->find(PaymentStatus::class, 'pending'));

                $metadata = [];

                foreach ($paymentForm->all() as $field) {
                    if ($field->getData() && $field->getConfig()->getOption('metadata')) {
                        $metadata[$field->getName()] = $field->getData();
                    }
                }

                $payment->setMetadata($metadata ?: null);

                $this->getDoctrine()->getManager()->persist($payment);
                $this->getDoctrine()->getManager()->flush();

                $session = $request->getSession();
                if ($session) {
                    $session->remove('defaultAddress');
                }

                try {
                    if ($paymentMethodClass->getGateway()) {
                        $result = $paymentMethodClass->getGateway()->processPayment($payment);

                        if ($result instanceof Response) {
                            return $result;
                        }

                        if (\is_bool($result) && $result) {
                            return $this->redirectToRoute('app_checkout_success');
                        }

                        foreach ($payment->getLastErrors() as $error) {
                            $form->addError(new FormError($error));
                        }
                    } elseif ($paymentMethodClass->processPayment($payment, $request)) {
                        return $this->redirectToRoute('app_checkout_success');
                    }
                } catch (\Exception $e) {
                    $form->addError(new FormError('Betaling kon niet gestart worden'));
                }
            }
        } else {
            if ($this->getUser() && $this->getUser()->getPreferredPaymentmethod()) {
                $paymentMethod = $this->getUser()->getPreferredPaymentmethod()->getCode();

                if ($form->get('site_checkout_payment')->has('ideal') && $this->getUser()->getPreferredPaymentmethod()->getCode() === 'ideal') {
                    $form->get('site_checkout_payment')->get('ideal')->get('issuer')->setData($this->getUser()->getPreferredIdealIssuer());
                }
            }

            if ($form->has('companyAccountRegistration') && !$form->get('companyAccountRegistration')->getData() && $this->get(CartService::class)->getCart()->getOrder()) {
                $form->get('companyAccountRegistration')->setData(false);
            }

            if ($request->get('payment')) {
                $payment = $this->getDoctrine()->getManager()->getRepository(Payment::class)->findOneBy([
                    'uuid' => $request->get('payment'),
                ]);

                if ($payment) {
                    foreach ($payment->getLastErrors() as $error) {
                        $form->addError(new FormError($error));
                    }
                }
            }
        }

        $formView = $form->createView();

        if ($formView->vars['valid'] && \count($form->getErrors(true)) > 0) {
            $formView->vars['valid'] = false;
        }

        $this->get(GoogleAnalytics::class)->checkout($this->get(CartService::class), 2);

        $site = $this->container->get(SiteService::class)->determineSite();

        $termsUrl = '/';
        if (null !== $site && null !== ($termsAndConditionsPage = $site->getTermsAndConditionsPage())) {
            $termsUrl = '/' . $termsAndConditionsPage->translate()->getSlug();
        }

        $cart = $this->get(CartService::class);
        $companyManager = $this->get(CompanyManager::class);
        $company = $cart->getCart()->getCompany();

        return array_merge($this->container->get(PaymentService::class)->getAdyenCseParams(), [
            'invalidVoucher' => $invalidVoucher,
            'cart' => $cart,
            'form' => $formView,
            'paymentMethod' => $paymentMethod,
            'login_form_submitted' => $form->has('login') && (bool)$form->get('login')->get('login_form_active')->getData(),
            'termsUrl' => $termsUrl,
            'showCreditLimitWarning' => null !== $company && true === $company->getVerified() && false === $companyManager->canPurchaseByCredit($company,
                    $cart->getCart()->getTotalPrice()),
        ]);
    }

    /**
     * @param Request $request
     *
     * @return mixed
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     */
    private function getCartForm(Request $request)
    {
        /** @var Cart $entity */
        $entity = $this->get(CartService::class)->getCart();

        //calculate totals of cart to be sure the correct value is shown
        $this->container->get(CartManager::class)->calculateTotals($entity);

        /** @var OrderCollection $orderCollection */
        $orderCollection = $entity->getOrderCollection();

        $form = $this->createForm(CartType::class, $entity, [
            'action' => $this->generateUrl('app_checkout_sender'),
            'method' => 'POST',
            'validation_groups' => function () use ($request, $orderCollection) {
                $groups = ['cart_sender'];

                if (!empty($request->request->get('cart')['customerType']) && $request->request->get('cart')['customerType'] === 'company') {
                    $groups[] = 'cart_sender_company';
                }

                if (!empty($request->get('cart')['paymentMethod'])) {
                    $groups[] = 'paymentmethod_' . $request->get('cart')['paymentMethod'];
                } elseif ($orderCollection &&
                    null !== ($orderCollectionPayments = $orderCollection->getPayments()) &&
                    ($lastSelectedpayment = end($orderCollectionPayments))) {
                    $groups[] = 'paymentmethod_' . $lastSelectedpayment->getPaymentmethod()->getCode();
                }

                return $groups;
            },
        ]);

        $customerStatus = null;

        if (!empty($request->get('cart')['customerType'])) {
            $customerStatus = $request->get('cart')['customerType'];
        }

        $availableMethods = $this->get(PaymentService::class)->getMethods($this->get(CartService::class)->getCart(),
            $customerStatus);

        $form->add($this->getPaymentForm($availableMethods));

        //check if company fields are available
        if ($this->getUser() && $this->getUser() instanceof Customer && $this->getUser()->getCompany()) {
            $companyCustomFieldService = $this->get(CompanyCustomFieldService::class);
            $form = $companyCustomFieldService->generateCustomFieldsForm($this->getUser()->getCompany(), $form,
                AppliesToOrderType::APPLIES_TO_ORDER_COLLECTION);
        }

        $form->add('submit', SubmitType::class, ['label' => 'label.pay_order', 'translation_domain' => 'messages']);

        if ($form->has('customerStatus') && empty($request->request->get('cart')['customerStatus'])) {
            $form->get('customerStatus')->setData('new');
        }

        if (!$request->isMethod('POST') && $form->has('customer') && $form->get('customer')->get('default_invoice_address') && $form->get('customer')->get('firstname')->getData()) {
            if ($form->get('customer')->get('default_invoice_address')->get('companyName')->getData()) {
                $form->get('customerType')->setData('company');
            } else {
                $form->get('customerType')->setData('customer');
            }
        }

        if ($form->has('customer') && $form->get('customer')->get('default_invoice_address') && $this->get(CartService::class)->getCart()->getCustomer() && $this->get(CartService::class)->getCart()->getCustomer()->getInvoiceAddress()) {
            $form->get('customer')->get('default_invoice_address')->setData($this->get(CartService::class)->getCart()->getCustomer()->getInvoiceAddress());
        }

        if ($this->getUser() && $form->has('customerStatus')) {
            $form->remove('customerStatus');
        }

        return $form;
    }

    /**
     * @param ArrayCollection $methods
     *
     * @return FormInterface
     */
    private function getPaymentForm(ArrayCollection $methods)
    {
        $form = $this->createForm(PaymentType::class, []);
        $form->getConfig()->getType()->getInnerType()->setCart($this->get(CartService::class));

        foreach ($methods as $method) {
            $attr = [];
            $paymentProductDecorator = null;

            $paymentMethod = $method->getEntity();

            if ($paymentMethod && $paymentMethod->getProduct()) {
                $paymentProductDecorator = $this->get(ProductDecoratorFactory::class)->get($paymentMethod->getProduct());

                $attr['data-payment-costs-name'] = $paymentProductDecorator->translate()->getTitle();
                $attr['data-payment-costs-amount'] = $paymentProductDecorator->getDisplayPrice();
            }

            if ($method->getDescription()) {
                $attr['data-has-description'] = true;
            }

            $form->add($method->getCode(), $method->getFormClass(), [
                'label' => $method->getName() . ($paymentProductDecorator ? ' <span> - &euro; ' . $paymentProductDecorator->getDisplayPrice() . '</span>' : null) . ($method->getDescription() ? '<br /> <span style="font-style: italic; font-size: 11px;">' . $method->getDescription() . '</span>' : null),
                'attr' => $attr,
            ]);
        }

        return $form;
    }

    /**
     * @param Customer $entity
     */
    private function createRegistrationMailJob($entity)
    {
        $job = new Job('topbloemen:site:send-registration-mail', [
            $entity->getId(),
            $this->get(Domain::class)->getDomain()->getId(),
        ], true, 'registration_mail');

        $job->addRelatedEntity($entity);
        $job->addRelatedEntity($this->get(Domain::class)->getDomain());

        $this->container->get('job.manager')->addJob($job);
        $this->container->get('doctrine')->getManager()->flush();
    }

    /**
     * @Route("/bedankt-voor-uw-bestelling")
     * @Template()
     *
     * @return array|RedirectResponse
     *
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    public function successAction()
    {
        $orderCollectionId = $this->get('session')->get('RecentOrderCollectionId');

        if (!$orderCollectionId) {
            return $this->redirectToRoute('app_home_index');
        }

        $orderCollection = $this->getDoctrine()->getManager()->find(OrderCollection::class, $orderCollectionId);

        // When no collection has been found
        if (!$orderCollection) {
            return $this->redirectToRoute('app_home_index');
        }

        $this->get(GoogleAnalytics::class)->purchase($orderCollection);

        if ($orderCollection && $this->get(OrderCollectionManager::class)->calculateOutstanding($orderCollection) > 0) {
            return $this->redirectToRoute('app_checkout_sender');
        }

        try {
            if (null === ($determineSite = $this->container->get(SiteService::class)->determineSite())) {
                $page = null;
            } else {
                $page = $determineSite->getOrderSuccessPage();
            }
        } catch (\Exception $e) {
            $page = null;
        }

        if (!$page) {
            $page = new Page();
            $page->translate()->setTitle($this->get('translator')->trans('checkout.thank_you.page_title', [],
                'messages'));
            $page->translate()->setContent($this->get('translator')->trans('checkout.thank_you.page_content', [],
                'messages'));
        }

        // @todo: calculateoutstanding
        if ($this->get(OrderCollectionManager::class)->calculateOutstanding($orderCollection) > 0) {
            return $this->redirectToRoute('app_checkout_sender');
        }

        $template = $this->get('twig')->createTemplate($page->translate()->getContent());

        $page->setRenderedContent($template->render([
            'orderCollection' => $orderCollection,
        ]));

        return [
            'page' => $page,
            'orderCollection' => $orderCollection,
        ];
    }

    /**
     * @Route("/bestelling/bezorgdata", methods={"GET"})
     * @Template()
     *
     * @param Request $request
     *
     * @return array|Response
     * @throws ForbiddenOperationException
     * @throws \Exception
     */
    public function deliveryDateAction(Request $request)
    {
        $this->productPriceManager = $this->container->get('app.product_price_manager');

        if (!$request->isXmlHttpRequest()) {
            throw new ForbiddenOperationException('Only xhr requests are allowed');
        }

        if (!$request->query->get('country') || !$request->query->get('postcode') || !$request->query->get('number')) {
            throw new BadRequestHttpException();
        }

        $postcode = $request->query->get('postcode');
        $number = $request->query->get('number');
        $country = $this->getDoctrine()->getManager()->getRepository(Country::class)->find($request->query->get('country'));

        $currentOrder = $this->get(CartService::class)->getCurrentOrder();

        if (null === $currentOrder) {
            return $this->redirect($this->generateUrl('app_home_index'));
        }

        $currentOrder->setDeliveryAddressPostcode($postcode);
        $currentOrder->setDeliveryAddressNumber($number);

        if (null !== $country) {
            $currentOrder->setDeliveryAddressCountry($country);
        }

        /** @var DeliveryDates|DeliveryDate[] $deliveryDates */
        $deliveryDates = $currentOrder->getDeliveryDates()->getAvailable();
        $datetime = new \DateTime();

        $deliveryDatesArray = [];

        /** @var DeliveryDate $deliveryDate */
        foreach ($deliveryDates as $deliveryDate) {
            $deliveryDate->setProductPriceManager($this->productPriceManager);
            $deliveryDatesArray[$deliveryDate->format('Y-m-d')] = [
                'price' => $deliveryDate->getDisplayPrice($currentOrder),
                'transport_type' => [
                    'short_description' => $deliveryDate->getTransportType()->translate()->getShortDescription(),
                ],
            ];
        }

        return [
            'deliveryDates' => $deliveryDates,
            'deliveryDate' => $request->query->get('deliveryDate'),
            'deliveryDatesDescription' => [
                $datetime->format('Y-m-d') => 'label.today',
                $datetime->add(new \DateInterval('P1D'))->format('Y-m-d') => 'label.tomorrow',
                $datetime->add(new \DateInterval('P1D'))->format('Y-m-d') => 'label.day_after_tomorrow',
            ],
            'deliveryDatesArray' => $deliveryDatesArray,
            'cartOrder' => $currentOrder,
        ];
    }

    /**
     * @Route("/bestelling/bezorgmethoden/{cartOrder}", methods={"GET"})
     * @Template()
     * @ParamConverter("cartOrder", options={"mapping": { "cartOrder" = "uuid"}})
     *
     * @param Request $request
     *
     * @param CartOrder $cartOrder
     *
     * @return array
     * @throws ForbiddenOperationException
     * @throws \Doctrine\ORM\ORMException
     */
    public function deliveryMethodAction(Request $request, CartOrder $cartOrder): array
    {
        if (!$request->isXmlHttpRequest()) {
            throw new ForbiddenOperationException('Only xhr requests are allowed');
        }

        $country = $this->getDoctrine()->getManager()->getRepository(Country::class)->find($request->query->get('deliveryAddressCountry'));

        if (null !== $country) {
            $cartOrder->setDeliveryAddressCountry($country);
        }

        $deliveryMethods = [
            'delivery' => 'label.delivery_method.delivery',
        ];

        if ($this->container->get(CartOrderManager::class)->isPickupPossible($cartOrder)) {
            $deliveryMethods['pickup'] = 'label.delivery_method.pickup';
        }

        $deliveryMethod = ($request->query->has('deliveryMethod') ? $request->query->get('deliveryMethod') : 'delivery');

        if ($request->query->get('deliveryMethod') !== 'delivery' && $cartOrder->getPickupAddress()) {
            $deliveryMethod = 'pickup';
        }

        return [
            'deliveryMethods' => $deliveryMethods,
            'deliveryMethod' => $deliveryMethod,
        ];
    }

    /**
     * @Route("/bestelling/betaalmethodes", methods={"GET", "POST"})
     * @Template()
     *
     * @param Request $request
     *
     * @return array
     * @throws Exception
     */
    public function paymentMethodAction(Request $request): array
    {
        if (!$request->isXmlHttpRequest()) {
            throw new ForbiddenOperationException('Only xhr requests are allowed');
        }

        $paymentMethod = !empty($request->get('cart')['paymentMethod']) ? $request->get('cart')['paymentMethod'] : null;

        $availableMethods = $this->get(PaymentService::class)->getMethods($this->get(CartService::class)->getCart(),
            $request->get('cart')['customerType'], $request->get('cart')['customerCountry']);

        $form = $this->createForm(CheckoutCartType::class, $request);

        $form->add($this->getPaymentForm($availableMethods));

        if (!empty($request->request->get('cart')['site_checkout_payment'])) {
            foreach ($request->request->get('cart')['site_checkout_payment'] as $methodName => $methods) {
                foreach ($methods as $elementName => $element) {
                    if ($form->get('site_checkout_payment')->get($methodName)->has($elementName)) {
                        $form->get('site_checkout_payment')->get($methodName)->get($elementName)->setData($element);
                    }
                }
            }
        }

        $cart = $this->get(CartService::class);
        $companyManager = $this->get(CompanyManager::class);
        $company = $cart->getCart()->getCompany();

        return array_merge($this->container->get(PaymentService::class)->getAdyenCseParams(), [
            'cart' => $cart,
            'form' => $form->createView(),
            'paymentMethod' => $paymentMethod,
            'hideInvoice' => !empty($request->request->get('cart')['customerType']) && $request->request->get('cart')['customerType'] === 'customer',
            'showCreditLimitWarning' => null !== $company && true === $company->getVerified() && false === $companyManager->canPurchaseByCredit($company,
                    $cart->getCart()->getTotalPrice()),
        ]);
    }

    /**
     * @Route("/bestelling/adres-suggesties", methods={"POST"})
     * @Template("@App/Checkout/components/delivery/address-suggestions.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function checkAddressAction(Request $request): array
    {
        $parameters = [
            'street' => $request->get('street'),
            'houseNo' => $request->get('housenumber'),
            'nbcode' => $request->get('postcode'),
            'city' => $request->get('city'),
        ];

        return [
            'suggestions' => $this->container->get(SuggestionService::class)->suggestAddress($parameters),
        ];
    }
}
