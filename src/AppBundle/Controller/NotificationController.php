<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/payment-notification")
 */
class NotificationController extends Controller
{
    /**
     * @Route("/adyen", methods={"POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function adyenAction(Request $request)
    {
        try {
            $content = json_decode($request->getContent());

            $this->get('app.payment.gateway.adyen')->saveNotifications($content);

            return new Response('[accepted]');
        } catch (\Exception $e) {
            return new Response('', 500);
        }
    }
}
