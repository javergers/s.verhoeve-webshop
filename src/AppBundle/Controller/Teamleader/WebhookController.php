<?php

namespace AppBundle\Controller\Teamleader;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/teamleader/webhook")
 */
class WebhookController extends Controller
{
    /**
     * @Route("");
     * @Template()
     * @return array;
     */
    public function indexAction()
    {
        // Todo
        return [];
    }
}
