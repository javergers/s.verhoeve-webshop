<?php

namespace AppBundle\Controller;

use AppBundle\Client\DefaultClient;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Interfaces\DesignerControllerInterface;
use AppBundle\Services\Designer;
use AppBundle\Traits\DesignerTrait;
use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/designer")
 */
class DesignerController extends Controller implements DesignerControllerInterface
{
    use DesignerTrait;

    /**
     * @var Designer
     */
    private $designer;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * @var DefaultClient
     */
    private $defaultClient;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * DesignerController constructor.
     * @param Designer              $designer
     * @param ParameterBagInterface $parameterBag
     * @param DefaultClient         $defaultClient
     * @param ContainerInterface    $container
     */
    public function __construct(
        Designer $designer,
        ParameterBagInterface $parameterBag,
        DefaultClient $defaultClient,
        ContainerInterface $container = null
    )
    {
        $this->designer = $designer;
        $this->parameterBag = $parameterBag;
        $this->defaultClient = $defaultClient;

        $this->setContainer($container);
    }

    /**
     * @Route("/preview/{cartOrderLine}/{uuid}", methods={"GET"})
     *
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     * @param CartOrderLine $cartOrderLine
     * @param string        $uuid
     * @param Request       $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function previewAction(CartOrderLine $cartOrderLine, $uuid, Request $request)
    {
        $request->query->set('returnUrl', 'void');

        $this->checkRequirements($cartOrderLine, $uuid);

        $this->designer->setUuid($uuid);

        return new JsonResponse([
            'designerUrl' => $this->generateUrl('app_checkout_personalization', [
                'cartOrder' => $cartOrderLine->getCartOrder()->getUuid(),
                'cartOrderLine' => $cartOrderLine ? $cartOrderLine->getUuid() : null,
            ]),
            'previewImageUrl' => $this->designer->getFileUrl(Designer::FILE_PREVIEW, 60, Designer::SOURCE_LOCAL),
        ]);
    }

    /**
     * @Route("/{cartOrderLine}/{uuid}/afbeelding-genereren", methods={"GET"})
     *
     * @ParamConverter("cartOrderLine", options={"mapping": { "cartOrderLine" = "uuid"}})
     *
     * @param CartOrderLine $cartOrderLine
     * @param string        $uuid
     * @param Request       $request
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function generateImages(CartOrderLine $cartOrderLine, $uuid, Request $request): RedirectResponse
    {
        $this->checkRequirements($cartOrderLine, $uuid);

        $designer = $this->get('app.designer');
        $designer->setUuid($uuid);

        $convertUrl = $this->container->get('router')->generate('app_checkout_convertpersonalization', [
            'cartOrder' => $cartOrderLine->getCartOrder()->getUuid(),
            'cartOrderLine' => $cartOrderLine->getUuid()
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $designer->setConvertUrl($convertUrl);

        $designer->convertJsonIntoImage();

        $url = $designer->getFileUrl(Designer::FILE_PREVIEW, 60, Designer::SOURCE_LOCAL);

        return $this->redirect($url);
    }

    /**
     * @param CartOrderLine $cartOrderLine
     * @param string        $uuid
     *
     * @return bool
     */
    private function checkRequirements(CartOrderLine $cartOrderLine, $uuid)
    {
        if ($cartOrderLine->hasProductPersonalization() || $cartOrderLine->getChildren()->isEmpty()) {
            $metadata = $cartOrderLine->getMetadata();

            return (isset($metadata['designer']) && $metadata['designer'] === $uuid);
        }

        if ($cartOrderLine->getChildren()->filter(function (CartOrderLine $childCartOrderLine) use ($uuid) {
            $metadata = $childCartOrderLine->getMetadata();

            return (isset($metadata['designer']) && $metadata['designer'] === $uuid);
        })->isEmpty()) {
            throw new BadRequestHttpException();
        }
    }

    /**
     * todo: duplicated code - load design by service method getStreamedResponse.
     *
     * @Route("/{uuid}/{filename}", requirements={
     *     "uuid": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"
     * }, methods={"GET"})
     *
     * @param Request $request
     * @param         $uuid
     * @param         $filename
     *
     * @return StreamedResponse
     */
    public function proxyAction(Request $request, $uuid, $filename)
    {
        if(ob_get_level()) {
            ob_end_clean();
            ob_implicit_flush(true);
        }

        $streamedResponse = new StreamedResponse();
        $streamedResponse->setCallback(function () use ($request, $uuid, $filename, $streamedResponse) {
            try {

                $objectStoreUrl = $this->parameterBag->get('openstack_object_store') . '/private';
                $queryParams = http_build_query($request->query->all());

                $url = $objectStoreUrl . '/designer/' . $uuid . '/' . $filename . '?' . $queryParams;

                try {
                    $response = $this->defaultClient->get($url, [
                        'on_headers' => function (ResponseInterface $response) {
                            foreach ($response->getHeaders() as $name => $value) {
                                if (strpos(current($value), 'attachment') !== false) {
                                    continue;
                                }

                                header($name . ':' . current($value));
                            }
                        },
                    ]);

                    $body = $response->getBody();
                    $body->seek(0);

                    while (!$body->eof()) {
                        print $body->read(1024);

                        flush();
                    }

                } catch (ClientException $e) {
                    print $e->getResponse()->getBody()->getContents();
                }

            } catch (\Exception $e) {
                if (!headers_sent()) {
                    http_response_code(500);
                }
            }
        });

        return $streamedResponse;
    }
}
