<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Site\Site;
use AppBundle\Interfaces\MenuRepositoryInterface;
use Doctrine\ORM\EntityRepository;

/**
 * Class AssortmentRepository
 * @package AppBundle\Repository
 */
class AssortmentRepository extends EntityRepository implements MenuRepositoryInterface
{
    /**
     * @param Site $site
     * @param      $keyword
     * @return array
     */
    public function searchMenu(Site $site, $keyword)
    {
        $qb = $this->getEntityManager()->getRepository(Assortment::class)->createQueryBuilder('a');
        $qb
            ->select('a')
            ->leftJoin('a.translations', 'at', 'WITH', 'a.id = at.translatable AND at.locale = :locale')
            ->leftJoin('a.sites', 's')
            ->andWhere('s.id = :site')
            ->andWhere($qb->expr()->orX(
                $qb->expr()->like('at.title', ':search'),
                $qb->expr()->like('a.name', ':search')
            ))
            ->setParameters([
                'site' => $site,
                'locale' => 'nl_NL',
                'search' => '%' . $keyword . '%',
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Company $company
     * @param         $keyword
     * @return array
     */
    public function searchMenuByCompany(Company $company, $keyword)
    {
        $qb = $this->getEntityManager()->getRepository(Assortment::class)->createQueryBuilder("a");
        $qb
            ->select('a')
            ->leftJoin('a.translations', 'at', 'WITH', 'a.id = at.translatable AND at.locale = :locale')
            ->leftJoin('a.companies', 'c')
            ->andWhere('c.id = :company')
            ->andWhere($qb->expr()->orX(
                $qb->expr()->like('at.title', ':search'),
                $qb->expr()->like('a.name', ':search')
            ))
            ->setParameters([
                'company' => $company,
                'locale' => 'nl_NL',
                'search' => '%' . $keyword . '%',
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $siteId
     * @return array
     */
    public function findBySite($siteId)
    {
        $qb = $this->getEntityManager()->getRepository(Assortment::class)->createQueryBuilder('a');
        $qb->leftJoin('a.sites', 's')
            ->andWhere('s.id = :site')
            ->setParameter('site', $siteId);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $companyId
     * @return array
     */
    public function findByCompany($companyId)
    {
        $qb = $this->getEntityManager()->getRepository(Assortment::class)->createQueryBuilder('a');
        $qb->leftJoin('a.companies', 'c')
            ->andWhere('c.id = :company')
            ->setParameter('company', $companyId);

        return $qb->getQuery()->getResult();
    }
}
