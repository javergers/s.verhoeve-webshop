<?php

namespace AppBundle\Repository\Supplier;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Supplier\PickupLocation;
use AppBundle\Repository\BaseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\NonUniqueResultException;

/**
 * PickupLocationRepository
 */
class PickupLocationRepository extends BaseRepository
{
    /**
     * @param Company $company
     * @return PickupLocation|null
     * @throws NonUniqueResultException
     */
    public function findByCompany(Company $company): ?PickupLocation
    {
        $qb = $this->getEntityManager()->getRepository(PickupLocation::class)->createQueryBuilder('pilo');
        $qb->innerJoin('pilo.companyEstablishment', 'coes');

        $qb->andWhere('coes.company = :company');

        $qb->setParameters([
            'company' => $company,
        ]);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param ArrayCollection $companies
     * @return PickupLocation[]
     */
    public function findByCompanies(ArrayCollection $companies): array
    {
        $qb = $this->getEntityManager()->getRepository(PickupLocation::class)->createQueryBuilder('pilo');
        $qb->innerJoin('pilo.companyEstablishment', 'coes');

        $qb->leftJoin('coes.address', 'addr');
        $qb->addSelect('coes');
        $qb->addSelect('addr');
        $qb->andWhere('coes.company IN (:companies)');

        $qb->setParameters([
            'companies' => $companies,
        ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $postcode
     * @return PickupLocation|null
     * @throws NonUniqueResultException
     */
    public function findByPostcode(int $postcode): ?PickupLocation
    {
        $qb = $this->getEntityManager()->getRepository(PickupLocation::class)->createQueryBuilder('pilo');
        $qb->innerJoin('pilo.companyEstablishment', 'coes');
        $qb->leftJoin('coes.address', 'addr');

        $qb->andWhere('addr.postcode LIKE \'%:postcode%\'');

        $qb->setParameters([
            'postcode' => $postcode,
        ]);

        return $qb->getQuery()->getOneOrNullResult();
    }
}