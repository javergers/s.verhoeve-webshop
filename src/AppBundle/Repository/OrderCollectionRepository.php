<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use Doctrine\ORM\EntityRepository;

/**
 * Class OrderCollectionRepository
 * @package AppBundle\Repository
 */
class OrderCollectionRepository extends EntityRepository
{
    /**
     * @param OrderCollection $orderCollection
     *
     * @return mixed
     */
    public function findFraudCheckableOrders(OrderCollection $orderCollection)
    {
        $qb = $this->getEntityManager()->getRepository(Order::class)->createQueryBuilder('o');
        $qb->leftJoin('o.orderCollection', 'oc');
        $qb->andWhere('oc.id = :orderCollection');
        $qb->andWhere('o.status IN (:status)');

        $parameters = [
            'orderCollection' => $orderCollection->getId(),
            'status' => ['payment_pending', 'new'],
        ];

        $qb->setParameters($parameters);

        return $qb->getQuery()->getResult();
    }
}