<?php

namespace AppBundle\Repository\Discount;

use AppBundle\Entity\Discount\Discount;
use AppBundle\Repository\BaseRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\EntityManagerInterface;

/**
 * DiscountRepository
 *
 */
class DiscountRepository extends BaseRepository
{
    /** @var EntityManagerInterface|EntityManagerInterface $entityManager */
    private $entityManager;

    /**
     *
     * @param EntityManagerInterface $em    The EntityManagerInterface to use.
     * @param Mapping\ClassMetadata  $class The class descriptor.
     */
    public function __construct(EntityManagerInterface $em, Mapping\ClassMetadata $class)
    {
        $this->entityManager = $em;
        parent::__construct($this->entityManager, $class);
    }

    /**
     * @param string Comma seperated array $fixedDiscounts
     * @return array
     */
    public function getAllFixedRestitutionDiscounts($fixedDiscounts){
        $qb = $this->entityManager->getRepository(Discount::class)->createQueryBuilder('discount');
        $qb->andWhere('discount.id IN(:ids)');
        $qb->orderBy('discount.value');
        $qb->setParameter('ids', explode(',', $fixedDiscounts), Connection::PARAM_INT_ARRAY);
        return $qb->getQuery()->getResult();
    }

    /**
     * @param string Comma seperated array $relativeDiscounts
     * @return array
     */
    public function getAllRelativeRestitutionDiscounts($relativeDiscounts){
        $qb = $this->entityManager->getRepository(Discount::class)->createQueryBuilder('discount');
        $qb->andWhere('discount.id IN(:ids)');
        $qb->orderBy('discount.value');
        $qb->setParameter('ids', explode(',', $relativeDiscounts), Connection::PARAM_INT_ARRAY);
        return $qb->getQuery()->getResult();
    }

}