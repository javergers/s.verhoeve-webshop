<?php

namespace AppBundle\Repository\Discount;

use AppBundle\Entity\Discount\VoucherBatch;
use AppBundle\Entity\Relation\Company;
use AppBundle\Repository\BaseRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;
use PDO;

/**
 * VoucherBatchRepository
 */
class VoucherBatchRepository extends BaseRepository
{
    /**
     * @return array
     * @throws Exception
     */
    public function findAllActive()
    {
        $qb = $this->getEntityManager()->getRepository(VoucherBatch::class)->createQueryBuilder('vb');
        $qb->andWhere('vb.validUntil > :validUntil')
            ->orWhere('vb.validUntil is null')
            ->addOrderBy('vb.createdAt', 'DESC')
            ->setParameter('validUntil', new \DateTime());

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $id
     * @return int
     */
    public function getUsedVouchersCount($id): int
    {
        try {
            $connection = $this->getEntityManager()->getConnection();
            $statement = $connection->prepare('
            SELECT COUNT(voco.order_collection_id) AS used_count

            FROM voucher_order_collection AS voco
            INNER JOIN voucher AS vouc ON vouc.id = voco.voucher_id
            
            WHERE vouc.batch_id = :batch_id
        ');
            $statement->bindValue(':batch_id', (int)$id, PDO::PARAM_INT);
            $statement->execute();
            return (int)$statement->fetch()['used_count'];
        } catch (DBALException $e) {
            return 0;
        }
    }
}