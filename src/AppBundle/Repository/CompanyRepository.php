<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Geography\Postcode;
use AppBundle\Entity\Relation\Company;
use AppBundle\ThirdParty\Exact\Entity\Invoice;
use AppBundle\ThirdParty\Exact\Entity\InvoiceExport;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * Class CompanyRepository
 * @package AppBundle\Repository
 */
class CompanyRepository extends EntityRepository
{
    /**
     * @param InvoiceExport $invoiceExport
     * @return array
     */
    public function findByInvoiceExport(InvoiceExport $invoiceExport)
    {
        $qb = $this->getEntityManager()->getRepository(Invoice::class)->createQueryBuilder('invo');
        $qb->select('comp');
        $qb->innerJoin(Company::class, 'comp', 'WITH', 'invo.company = comp.id');
        $qb->where('invo.invoiceExport = :export');
        $qb->setParameter('export', $invoiceExport);
        $qb->groupBy('comp.id');
        return $qb->getQuery()->getResult();
    }

    /**
     * find recentCompanies
     *
     * @param int $limit
     *
     * @return mixed
     */
    public function findRecentCompanies($limit = 5)
    {
        $qb = $this->getEntityManager()->getRepository(Company::class)->createQueryBuilder('c');
        $qb->addOrderBy('c.createdAt', 'DESC');
        $qb->setMaxResults($limit);

        return $qb->getQuery()->execute();
    }

    /**
     * @param int $id
     *
     * @return mixed|null
     * @throws NonUniqueResultException
     */
    public function findByBloemorderCompanyId(int $id)
    {
        $rsm = new ResultSetMappingBuilder($this->getEntityManager());
        $rsm->addRootEntityFromClassMetadata(Company::class, 'company');

        $query = $this->getEntityManager()->createNativeQuery("
            SELECT *
            FROM company
            WHERE company.metadata IS NOT NULL 
              AND company.metadata RLIKE 'bloemorderCompanyId\":" . $id . "[,|}]'
        ", $rsm);

        try {
            return $query->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        }
    }

    /**
     * @param $supplierGroupNames
     *
     * @return Company[]
     */
    public function findAllBySupplierGroupNames($supplierGroupNames)
    {
        return $this->createQueryBuilder('company')
            ->leftJoin('company.supplierGroups', 'supplierGroup')
            ->andWhere('supplierGroup.name IN(:supplierGroupNames)')
            ->setParameter('supplierGroupNames', $supplierGroupNames)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return array
     */
    public function findAllSuppliers(): array
    {
        return $this->createQueryBuilder('company')
            ->where('company.isSupplier = 1')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Postcode $postcode
     * @return Company[]
     */
    public function findSuppliersForPostcode(Postcode $postcode): array
    {
        $qb = $this->createQueryBuilder('company');
        $qb->andWhere('company.isSupplier = 1');
        $qb->leftJoin('company.deliveryAreas', 'delivery_area');
        $qb->andWhere('delivery_area.postcode = :postcode');
        $qb->setParameter('postcode', $postcode);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return Company[]
     */
    public function findSuppliersWithoutGroup(): array
    {
        $qb = $this->createQueryBuilder('company');
        $qb->andWhere('company.isSupplier = 1');
        $qb->leftJoin('company.supplierGroups', 'supplier_group');
        $qb->andWhere('supplier_group.id IS NULL');

        return $qb->getQuery()->getResult();
    }
}