<?php

namespace AppBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait UuidEntity
 * @package AppBundle\Traits
 */
trait UuidEntity
{
    /**
     * @ORM\Column(type="uuid", nullable=true, unique=true)
     */
    protected $uuid;

    /**
     * @param $uuid
     * @return $this
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

}