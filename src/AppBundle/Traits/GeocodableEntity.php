<?php

namespace AppBundle\Traits;

use CrEOF\Spatial\PHP\Types\Geometry\Point;
use Doctrine\ORM\Mapping as ORM;

/**
 * Trait GeocodableEntity
 * @package AppBundle\Traits
 */
trait GeocodableEntity
{
    /**
     * @ORM\Column(type="point", nullable=true)
     */
    protected $point;

    /**
     * Get location.
     *
     * @return Point.
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Set location.
     *
     * @param Point|null $point the value to set.
     *
     * @return $this
     */
    public function setPoint(Point $point = null)
    {
        $this->point = $point;

        return $this;
    }
}