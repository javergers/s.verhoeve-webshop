<?php

namespace AppBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait NonEditableEntity
 * @package AppBundle\Traits
 */
trait NonEditableEntity
{
    public static $defaultIgnoredColumns = [
        'updatedAt',
        'deletedAt',
    ];

    /**
     * @return array
     */
    public static function getIgnoredColumns(): array
    {
        return self::$defaultIgnoredColumns;
    }

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    protected $editable = true;

    /**
     * Sets editable.
     *
     * @param boolean $editable
     *
     * @return $this
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;

        return $this;
    }

    /**
     * Returns editable.
     *
     * @return boolean
     */
    public function getEditable(): bool
    {
        return $this->editable;
    }

    /**
     * Is editable?
     *
     * @return bool
     */
    public function isEditable(): bool
    {
        return ($this->editable === true);
    }
}
