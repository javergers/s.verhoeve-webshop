<?php

namespace AppBundle\Traits;

use AppBundle\Entity\Site\Site;
use AppBundle\Exceptions\Common\CommandContextException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouterInterface;

/**
 * Trait CommandContextTrait
 * @package AppBundle\Traits
 */
trait CommandContextTrait
{
    /** @var RouterInterface */
    protected $router;

    /**
     * @var ParameterBagInterface
     */
    protected $parameterBag;

    /**
     * @param Site $site
     * @return RequestContext
     */
    public function defineContext(Site $site): RequestContext
    {
        $parentSite = $site->getParent();

        if($parentSite) {
            $site = $parentSite;
        }

        if(!$site->getPrimaryDomain()) {
            throw new CommandContextException(sprintf('Site `%s` has no primary domain', $site->translate()->getDescription()));
        }

        $context = $this->router->getContext();
        $context->setHost($site->getPrimaryDomain()->getDomain());
        $context->setHttpPort($site->getHttpPort() ?: 80);
        $context->setHttpsPort($site->getHttpPort() ?: 443);
        $context->setScheme($site->getHttpScheme());

        return $context;
    }

    /**
     * @return RequestContext
     */
    public function defineAdminContext(): RequestContext
    {
        $context = $this->router->getContext();
        $context->setHost($this->parameterBag->get('admin_host') ?: 'start.topgeschenken.nl');
        $context->setHttpPort($this->parameterBag->get('admin_port') ?: 80);
        $context->setHttpsPort($this->parameterBag->get('admin_port') ?: 443);
        $context->setScheme($this->parameterBag->get('admin_scheme') ?: 'https');

        return $context;
    }

    /**
     * @required
     * @param RouterInterface       $router
     * @param ParameterBagInterface $parameterBag
     */
    public function setDependencies(RouterInterface $router, ParameterBagInterface $parameterBag)
    {
        $this->router = $router;
        $this->parameterBag = $parameterBag;
    }
}
