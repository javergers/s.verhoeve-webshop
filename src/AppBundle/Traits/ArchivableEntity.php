<?php

namespace AppBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait ArchivableEntity
 * @package AppBundle\Traits
 */
trait ArchivableEntity
{
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $archivedAt;

    /**
     * Sets archivedAt.
     *
     * @param \Datetime|null $archivedAt
     *
     * @return $this
     */
    public function setArchivedAt(\DateTime $archivedAt = null)
    {
        $this->archivedAt = $archivedAt;

        return $this;
    }

    /**
     * Returns archivedAt.
     *
     * @return \DateTime
     */
    public function getArchivedAt()
    {
        return $this->archivedAt;
    }

    public function archive()
    {
        $this->archivedAt = new \DateTime();
    }

    /**
     * Is deleted?
     *
     * @return bool
     */
    public function isArchived()
    {
        return null !== $this->archivedAt;
    }
}
