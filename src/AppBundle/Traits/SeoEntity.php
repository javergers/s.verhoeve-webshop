<?php

namespace AppBundle\Traits;

use Doctrine\ORM\Mapping as ORM;

/**
 * Trait SeoEntity
 * @package AppBundle\Traits
 */
trait SeoEntity
{
    /**
     * @ORM\Column(type="string", length=75, nullable=true)
     *
     */
    protected $metaTitle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    protected $metaDescription;

    /**
     * @ORM\Column(type="boolean")
     *
     */
    protected $metaRobotsIndex = true;

    /**
     * @ORM\Column(type="boolean")
     *
     */
    protected $metaRobotsFollow = true;

    /**
     * @return mixed
     */
    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    /**
     * @param mixed $metaTitle
     */
    public function setMetaTitle($metaTitle)
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @return mixed
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param mixed $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return mixed
     */
    public function getMetaRobotsIndex()
    {
        return $this->metaRobotsIndex;
    }

    /**
     * @param mixed $metaRobotsIndex
     */
    public function setMetaRobotsIndex($metaRobotsIndex)
    {
        $this->metaRobotsIndex = $metaRobotsIndex;
    }

    /**
     * @return mixed
     */
    public function getMetaRobotsFollow()
    {
        return $this->metaRobotsFollow;
    }

    /**
     * @param mixed $metaRobotsFollow
     */
    public function setMetaRobotsFollow($metaRobotsFollow)
    {
        $this->metaRobotsFollow = $metaRobotsFollow;
    }
}