<?php

namespace AppBundle\Traits;

use AppBundle\Services\Designer;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Trait DesignerTrait
 * @package AppBundle\Traits
 */
trait DesignerTrait
{
    /**
     * @var Designer
     */
    protected $designerService;

    /**
     * todo: move into manager
     * @Route("/get-template/{templateUuid}")
     * @Method({"GET"})
     *
     * @param         $templateUuid
     * @return JsonResponse
     * @throws Exception
     */
    public function getTemplateJson($templateUuid)
    {
        $this->designerService->setUuid($templateUuid);
        $this->designerService->setIsPreDesign(true);

        $json = $this->designerService->getJson();

        if ($json === null) {
            return new JsonResponse('Failed', 500);
        }

        return new JsonResponse(['json' => $json]);
    }

    /**
     * todo: move into manager
     * @Route("/get-alternative-templates/{templateUuid}")
     * @Method({"GET"})
     *
     * @param         $templateUuid
     * @return JsonResponse
     * @throws Exception
     */
    public function getAlternativeTemplatesAction($templateUuid)
    {
        $templates = $this->designerService->getAlternativeTemplates($templateUuid);
        return new JsonResponse(['templates' => $templates]);
    }

    /**
     * @required
     * @param Designer $designerService
     */
    public function setDesignerService(Designer $designerService)
    {
        $this->designerService = $designerService;
    }

    /**
     * @return Designer
     */
    public function getDesignerService() {
        return $this->designerService;
    }
}