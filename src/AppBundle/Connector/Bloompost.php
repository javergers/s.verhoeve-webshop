<?php

namespace AppBundle\Connector;

use AppBundle\Client\DefaultClient;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Entity\Supplier\SupplierOrderLine;
use AppBundle\Exceptions\CardTextExceedsMaximumLengthException;
use Sabre\Xml\Deserializer;
use Sabre\Xml\Reader;
use Sabre\Xml\Service;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class Bloompost
 * @package AppBundle\Connector
 */
class Bloompost extends AbstractConnector
{
    /**
     * @var string
     */
    protected $color = '#49B185';

    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var DefaultClient
     */
    private $client;

    /**
     * Bloompost constructor.
     *
     * @param KernelInterface $kernel
     * @param DefaultClient $client
     */
    public function __construct(KernelInterface $kernel, DefaultClient $client)
    {
        $this->kernel = $kernel;
        $this->client = $client;
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @throws \Exception
     */
    public function process(SupplierOrder $supplierOrder)
    {
        $xml = $this->buildXml($supplierOrder);

        $response = $this->client->post('https://api.bloompost.nl/v1/orders/create', [
            'auth' => [
                $this->getParameter('webservice_username'),
                $this->getParameter('webservice_password'),
            ],
            'body' => $xml,
        ]);

        $result = (string)$response->getBody();

        $service = new Service();
        $service->elementMap = [
            'response' => function (Reader $reader) {
                return Deserializer\keyValue($reader, '');
            },
        ];

        $result = $service->parse($result);

        if ($result['success'] !== 'true') {
            throw new \RuntimeException(vsprintf('%s (%s)', [
                $result['error'],
                $result['errorcode'],
            ]));
        }

        $supplierOrder->setReference($result['orderid']);

        parent::process($supplierOrder);
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @return mixed|string
     * @throws \Exception
     */
    private function buildXml(SupplierOrder $supplierOrder)
    {
        $order = $supplierOrder->getOrder();

        $test = false;

        if ($this->kernel->getEnvironment() !== 'prod') {
            $test = true;
        }

        $xmlOrder = [
            'apikey' => $this->getParameter('api_key'),
            'test' => $test,
            'externalid' => $order->getOrderNumber(),
            'creationDate' => $order->getCreatedAt()->format("Y-m-d\TH:i:s"),
            'customer' => [
                'gender' => 'M',
                'firstname' => 'Klantenservice',
                // "prename"
                'lastname' => 'Topgeschenken Nederland B.V.',
                'email' => 'klantenservice@topgeschenken.nl',
                'telephone' => '0881108050',
            ],
            'shipment' => array_filter([
                'company' => $order->getDeliveryAddressCompanyName(),
                '_name' => $order->getDeliveryAddressAttn(),
                'street' => $order->getDeliveryAddressStreet(),
                'housenumber' => $order->getDeliveryAddressNumber(),
                'housenumberext' => $order->getDeliveryAddressNumberAddition(),
                'zipcode' => $order->getDeliveryAddressPostcode(),
                'city' => $order->getDeliveryAddressCity(),
                'countryCode' => $order->getDeliveryAddressCountry()->getCode(),
                'shipmentdate' => $this->calculateShippingDate($supplierOrder)->format('Y-m-d'),
            ]),
            'orderitems' => $this->buildOrderItems($supplierOrder),
        ];

        $xmlService = new Service();

        $xml = $xmlService->write('order', $xmlOrder);
        $xml = str_replace('_name>', 'name>', $xml);

        return $xml;
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @return array
     * @throws \Exception
     */
    private function buildOrderItems(SupplierOrder $supplierOrder)
    {
        $orderItems = [];

        $flowerOrderLine = $this->findOrderLine($supplierOrder, 'flower');
        $flowerSupplierProduct = $flowerOrderLine->getConcludedSupplierProduct();

        $orderItems['flower'] = [
            'productcode' => $flowerSupplierProduct->getSku(),
        ];

        $cardOrderLine = $this->findOrderLine($supplierOrder, 'card');

        if ($cardOrderLine) {
            $cardSupplierProduct = $cardOrderLine->getConcludedSupplierProduct();

            $cardText = $cardOrderLine->getOrderLine()->getMetadata()['text'];

            if (\strlen(nl2br($cardText)) > 450) {
                $exception = new CardTextExceedsMaximumLengthException();
                $exception->setMaximumLength(450);
                $exception->setText($cardText);

                throw $exception;
            }

            $orderItems['card'] = [
                'productcode' => $cardSupplierProduct->getSku(),
                'cardText' => nl2br($cardText),
            ];
        }

        $packagingOrderLine = $this->findOrderLine($supplierOrder, 'packaging');
        $packagingSupplierProduct = $packagingOrderLine->getConcludedSupplierProduct();

        $orderItems['packaging'] = [
            'productcode' => $packagingSupplierProduct->getSku(),
        ];

        $extraOrderLine = $this->findOrderLine($supplierOrder, 'extra');

        if ($extraOrderLine) {
            $extraSupplierProduct = $extraOrderLine->getConcludedSupplierProduct();

            $orderItems['extra'] = [
                'productcode' => $extraSupplierProduct->getSku(),
            ];
        }

        return $orderItems;
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @param string        $type
     * @return SupplierOrderLine
     */
    private function findOrderLine(SupplierOrder $supplierOrder, $type)
    {
        $supplierOrderLines = $supplierOrder->getLines()->filter(function (SupplierOrderLine $supplierOrderLine) use ($type) {
            switch ($type) {
                case 'flower';
                    return $this->isFlower($supplierOrderLine);
                case 'card':
                    return $this->isCard($supplierOrderLine);
                case 'packaging':
                    return $this->isPackaging($supplierOrderLine);
                case 'extra':
                    return $this->isExtra($supplierOrderLine);
                default:
                    return false;
            }
        });

        if ($type === 'flower' && $supplierOrderLines->count() === 0) {
            throw new \RuntimeException('Geen brievenbusbloemen product gevonden.');
        }

        if ($supplierOrderLines->count() > 1) {
            throw new \RuntimeException('Meerdere brievenbusbloemen producten gevonden.');
        }

        /** @var SupplierOrderLine $orderLine */
        $supplierOrderLine = $supplierOrderLines->current();

        if ($supplierOrderLine && $supplierOrderLine->getQuantity() !== 1) {
            throw new \RuntimeException(vsprintf("Besteleenheid van '%s' is niet gelijk aan 1 stuks.", [
                $supplierOrderLine->getOrderLine()->getTitle(),
            ]));
        }

        return $supplierOrderLine;
    }

    /**
     * @param SupplierOrderLine $supplierOrderLine
     * @return bool
     * @throws \Exception
     */
    private function isFlower(SupplierOrderLine $supplierOrderLine)
    {
        $product = $supplierOrderLine->getConcludedSupplierProduct()->getProduct();

        if ($product->getProductgroup() === null) {
            throw new \RuntimeException(vsprintf("Product '%s', doesn't have a product group", [
                $product,
            ]));
        }

        return $product->getProductgroup()->getSkuPrefix() === 'BPP';
    }

    /**
     * @param SupplierOrderLine $supplierOrderLine
     * @return bool
     */
    private function isCard(SupplierOrderLine $supplierOrderLine)
    {
        $orderLine = $supplierOrderLine->getOrderLine();

        return $orderLine->getMetadata() && array_key_exists('text', $orderLine->getMetadata());
    }

    /**
     * @param SupplierOrderLine $supplierOrderLine
     * @return bool
     */
    private function isPackaging(SupplierOrderLine $supplierOrderLine)
    {
        $product = $supplierOrderLine->getConcludedSupplierProduct()->getProduct();

        if ($product->getProductgroup() === null) {
            throw new \RuntimeException(vsprintf("Product '%s', doesn't have a product group", [
                $product,
            ]));
        }

        return $product->getProductgroup()->getSkuPrefix() === 'DLV';
    }

    /**
     * @param SupplierOrderLine $supplierOrderLine
     *
     * @return bool
     * @throws \Exception
     */
    private function isExtra(SupplierOrderLine $supplierOrderLine)
    {
        return !$this->isFlower($supplierOrderLine) && !$this->isCard($supplierOrderLine) && !$this->isPackaging($supplierOrderLine) && $supplierOrderLine->getConcludedSupplierProduct()->getProduct()->getPurchasable();
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @return \DateTime
     * @throws \Exception
     */
    private function calculateShippingDate(SupplierOrder $supplierOrder)
    {
        $deliveryDate = $supplierOrder->getOrder()->getDeliveryDate();

        if (!$deliveryDate) {
            $shippingDate = new \DateTime();
            $shippingDate->setTime(0, 0, 0);
        } else {
            $shippingDate = clone $deliveryDate;
            $shippingDate->setTime(0, 0, 0);
            $shippingDate->sub(new \DateInterval('P1D'));
        }

        if ($shippingDate->format('l') === 'Saturday') {
            $shippingDate->sub(new \DateInterval('P1D'));
        }

        return $shippingDate;
    }

    /**
     * @param FormBuilderInterface $form
     */
    public function addFormBuilderParameters(FormBuilderInterface $form)
    {
        $form->add('webservice_username', TextType::class, [
            'label' => 'Webservice gebruikersnaam',
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $form->add('webservice_password', TextType::class, [
            'label' => 'Webservice wachtwoord',
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $form->add('api_key', TextType::class, [
            'label' => 'API key',
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);
    }
}
