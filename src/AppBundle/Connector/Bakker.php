<?php

namespace AppBundle\Connector;

use AppBundle\Decorator\ProductDecorator;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Relation\Company as Supplier;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Entity\Supplier\SupplierOrderLine;
use AppBundle\Services\JobManager;
use AppBundle\Services\Sales\Order\OrderActivityService;
use AppBundle\Services\SMSService;
use AppBundle\Form\Type\YesNoType;
use Exception;
use GuzzleHttp\Exception\ServerException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Swift_Mailer;
use Symfony\Bridge\Twig\TwigEngine;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class Bakker
 * @package AppBundle\Connector
 */
class Bakker extends AbstractConnector
{
    use ContainerAwareTrait;

    protected $color = '#4CAF50';

    /**
     * @var bool
     */
    protected $requireForwardingPrices = true;

    /**
     * @var bool
     */
    protected $alwaysShowForwardingPrices = true;

    /**
     * @var bool
     */
    protected $commissionable = true;

    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var OrderActivityService
     */
    private $orderActivityService;

    /**
     * @var SMSService
     */
    private $smsService;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var JWTTokenManagerInterface
     */
    private $JWTTokenManager;

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * Bakker constructor.
     *
     * @param Swift_Mailer $mailer
     * @param LoggerInterface $logger
     * @param OrderActivityService $orderActivityService
     * @param SMSService $smsService
     * @param TwigEngine $twig
     * @param JWTTokenManagerInterface $JWTTokenManager
     * @param JobManager $jobManager
     * @param TranslatorInterface $translator
     */
    public function __construct(
        Swift_Mailer $mailer,
        LoggerInterface $logger,
        OrderActivityService $orderActivityService,
        SMSService $smsService,
        TwigEngine $twig,
        JWTTokenManagerInterface $JWTTokenManager,
        JobManager $jobManager,
        TranslatorInterface $translator
    ) {
        $this->mailer = $mailer;
        $this->logger = $logger;
        $this->orderActivityService = $orderActivityService;
        $this->smsService = $smsService;
        $this->twig = $twig;
        $this->JWTTokenManager = $JWTTokenManager;
        $this->jobManager = $jobManager;
        $this->translator = $translator;
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @throws \Exception
     */
    public function process(SupplierOrder $supplierOrder)
    {
        $postOrder = $this->transformOrder($supplierOrder);

        $order = $supplierOrder->getOrder();

        try {
            $apiUrl = $this->parameterBag->get('connector_bakker_api_url');

            $response = $this->getGuzzleClient($apiUrl)->post('/tgapi/import/order', [
                'json' => $postOrder,
            ]);
        } catch (ServerException $exception) {
            $response = $exception->getResponse();
        }

        if ($response->getStatusCode() !== 200) {
            $this->entityManager->flush();
            $responseMessage = $response->getBody()->getContents();

            if ($responseMessage === 'supplier.closed') {
                $responseMessage = 'De gekozen leverancier is gesloten op de leverdatum van deze order.';
            }

            throw new \RuntimeException($responseMessage);
        }

        if (strtoupper($response->getBody()->getContents()) !== 'OK') {
            throw new \RuntimeException('Onbekende fout opgetreden bij doorsturen');
        }

        if ($order->getStatus() == 'new') { // @todo: workflow guard refactor
            $order->setStatus('pending'); // @todo: workflow refactor

            try {
                if ($this->isEmailNotificationEnabled()) {
                    $job = new Job('bakery:send-new-order-mail', [$supplierOrder->getId()]);
                    $this->container->get('job.manager')->addJob($job);
                }

                if ($this->getParameter('phone')) {
                    $this->sendConfirmationSMS($supplierOrder);
                }
            } catch(Exception $e) {
                $this->container->get('logger')->addError(sprintf('Notifications for order %s not send', $order->getNumber()), [
                    'exception' => $e
                ]);
            }
        }

        parent::process($supplierOrder);
    }

    /**
     * @param SupplierOrder $supplierOrder
     */
    public function sendConfirmationSMS(SupplierOrder $supplierOrder)
    {
        try {
            $content = $this->translator->trans('confirmation.supplier.sms.content');

            $this->smsService->send([
                $this->getParameter('phone'),
            ], $content, 'Toptaarten');
        } catch (\Exception $e) {

            $this->orderActivityService->add(
                'order_notification_failed',
                $supplierOrder->getOrder(),
                null,
                [
                    'text' => sprintf('%s notificatie versturen is mislukt', 'SMS'),
                ]
            );
        }
    }

    /**
     * @return bool
     */
    protected function isEmailNotificationEnabled()
    {
        $emailNotificationEnabled = $this->getParameter('email_notification_enabled');

        // Default value is enabled.
        if($emailNotificationEnabled === null || $emailNotificationEnabled === '') {
            return true;
        }

        return (bool)$emailNotificationEnabled;
    }

    /**
     * @return string
     * @throws Twig_Error
     */
    public function getBody()
    {
        return $this->twig->render('@App/bakery-order.html.twig');
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @throws \Exception
     */
    public function cancel(SupplierOrder $supplierOrder)
    {
        $order = $supplierOrder->getOrder();
        $apiUrl = $this->parameterBag->get('connector_bakker_api_url');

        try {
            $response = $this->getGuzzleClient($apiUrl)->post('/tgapi/import/order-cancel', [
                'json' => [
                    'order_id' => $order->getId(),
                    'supplier_id' => $supplierOrder->getSupplier()->getId(),
                ],
            ]);
        } catch (ServerException $exception) {
            $response = $exception->getResponse();
        }

        if ($response->getStatusCode() !== 200) {
            $response = json_decode($response->getBody()->getContents());

            throw new \RuntimeException($response->error_message);
        }

        parent::cancel($supplierOrder);
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @return \stdClass
     * @throws \Exception
     */
    private function transformOrder(SupplierOrder $supplierOrder)
    {
        $order = $supplierOrder->getOrder();

        /** @var OrderCollection $orderCollection */
        $orderCollection = $order->getOrderCollection();

        $data = new \stdClass();
        $data->id = $orderCollection->getId();
        $data->number = $orderCollection->getNumber();
        $data->site_id = $orderCollection->getSite()->getId();

        /* @todo: dynamic */
        $data->customer_email = 'klantenservice@toptaarten.nl';
        $data->company_name = 'Toptaarten.nl';
        $data->invoice_address_company_name = 'Toptaarten.nl';
        $data->invoice_address_attn = 'Klantenservice';
        $data->invoice_address_street = 'Noorderdreef';
        $data->invoice_address_number = '60';
        $data->invoice_address_postcode = '2153LL';
        $data->invoice_address_city = 'Nieuw-Vennep';
        $data->invoice_address_country = 'Nederland';
        $data->invoice_address_phone_number = '0881108061';

        $data->ip = $orderCollection->getIp();
        $data->total_price = $orderCollection->getTotalPrice();
        $data->total_price_incl = $orderCollection->getTotalPriceIncl();

        if ($orderCollection->getCreatedAt()) {
            $data->created_at = $orderCollection->getCreatedAt()->format('Y-m-d H:i:s');
        }

        if ($orderCollection->getUpdatedAt()) {
            $data->updated_at = $orderCollection->getUpdatedAt()->format('Y-m-d H:i:s');
        }

        if ($orderCollection->getDeletedAt()) {
            $data->deleted_at = $orderCollection->getDeletedAt()->format('Y-m-d H:i:s');
        }

        $data->orders = [];

        $nonPurchasableSupplierOrderLines = [];

        foreach ($supplierOrder->getRootLines() as $supplierOrderLine) {
            if (!$supplierOrderLine->getConcludedSupplierProduct()->getProduct()->getPurchasable()) {
                $nonPurchasableSupplierOrderLines[] = $supplierOrderLine;
            }
        }

        foreach ($supplierOrder->getRootLines() as $supplierOrderLine) {
            if (!$supplierOrderLine->getConcludedSupplierProduct()->getProduct()->getPurchasable()) {
                continue;
            }

            $orderData = new \stdClass();

            $orderData->supplier_id = $supplierOrder->getSupplier()->getId();
            $orderData->id = $order->getId();
            $orderData->order_id = $orderCollection->getId();
            $orderData->number = $order->getNumber();

            $orderData->status_id = $order->getStatus();

            if ($order->getDeliveryAddressCountry()) {
                $orderData->delivery_address_country = $order->getDeliveryAddressCountry()->getCode();
            }

            if ($order->getDeliveryDate()) {
                $orderData->delivery_date = $order->getDeliveryDate()->format('Y-m-d H:i:s');
            }

            if ($order->getDeliveryStatus()) {
                $orderData->delivery_status = $order->getDeliveryStatus()->getId();
            }

            $orderData->supplier_remark = $order->getSupplierRemark();
            $orderData->delivery_remark = $order->getDeliveryRemark();
            $orderData->internal_remark = $order->getInternalRemark();
            $orderData->total_price = $order->getTotalPrice();
            $orderData->total_price_incl = $order->getTotalPriceIncl();
            $orderData->delivery_address_company_name = $order->getDeliveryAddressCompanyName();
            $orderData->delivery_address_attn = $order->getDeliveryAddressAttn();
            $orderData->delivery_address_street = $order->getDeliveryAddressStreet();
            $orderData->delivery_address_number = trim($order->getDeliveryAddressNumber() . '-' . $order->getDeliveryAddressNumberAddition(),
                '-');
            $orderData->delivery_address_postcode = $order->getDeliveryAddressPostcode();
            $orderData->delivery_address_city = $order->getDeliveryAddressCity();
            $orderData->delivery_address_phone_number = $order->getDeliveryAddressPhoneNumber();
            $orderData->metadata = $order->getMetadata();

            if ($order->getCreatedAt()) {
                $orderData->created_at = $order->getCreatedAt()->format('Y-m-d H:i:s');
            }

            if ($order->getUpdatedAt()) {
                $orderData->updated_at = $order->getUpdatedAt()->format('Y-m-d H:i:s');
            }

            if ($order->getDeletedAt()) {
                $orderData->deleted_at = $order->getDeletedAt()->format('Y-m-d H:i:s');
            }

            if ($order->getDeliveryAddressType()) {
                $orderData->delivery_address_type = $order->getDeliveryAddressType()->getId();
            }

            $orderData->lines = [];

            /** @var SupplierOrderLine[] $supplierOrderSetLines */
            $supplierOrderSetLines = array_merge([$supplierOrderLine],
                $supplierOrder->getChildLines($supplierOrderLine), $nonPurchasableSupplierOrderLines);

            foreach ($supplierOrderSetLines as $i => $supplierOrderSetLine) {
                $orderLine = $supplierOrderSetLine->getOrderLine();

                $product = $supplierOrderSetLine->getConcludedSupplierProduct()->getProduct();

                $productData = new \stdClass();
                $productData->id = $orderLine->getId();

                if ($orderLine->getParent()) {
                    $productData->parent_id = $orderLine->getParent()->getId();
                }

                if ($product) {
                    $productData->product_id = $product->getId();
                }

                if ($supplierOrderSetLine->getVatRate()) {
                    $productData->vat = $supplierOrderSetLine->getVatRate()->getTitle();
                }

                if ($orderLine->getLedger()) {
                    $productData->ledger_id = $orderLine->getLedger()->getNumber();
                }

                $supplierProduct = $supplierOrderSetLine->getConcludedSupplierProduct();

                $productData->sku = $product->getSku();
                $productData->purchasable = $product->getPurchasable();
                $productData->quantity = $supplierOrderSetLine->getQuantity();
                $productData->metadata = $supplierOrderSetLine->getOrderLine()->getMetadata();
                $productData->title = $supplierProduct->translate()->getTitle();
                $productData->description = $supplierProduct->translate()->getDescription();
                $productData->price = $supplierOrderSetLine->getPrice();

                if ($supplierOrderSetLine->getVatRate()) {
                    $productData->vat = $supplierOrderSetLine->getVatRate()->getFactor() * 100;
                }

                if ($product) {
                    $productDecorator = new ProductDecorator();
                    $productDecorator->setContainer($this->container);
                    $productDecorator->setProduct($product);

                    $productData->image = $this->getImage($productDecorator);
                }

                $orderData->lines[] = $productData;
            }

            $data->orders[] = $orderData;

            $nonPurchasableSupplierOrderLines = [];

        }

        return $data;
    }

    /**
     * @param ProductDecorator $product
     *
     * @return string|null
     */
    private function getImage(ProductDecorator $product)
    {
        $image = $product->getMainImage();
        $filesystem = $this->container->get('filesystem_public');

        if ($image && $image->getPath()) {
            return $filesystem->getPublicUrl($image->getPath());
        }

        return null;
    }

    /**
     * @param FormBuilderInterface $form
     */
    public function addFormBuilderParameters(FormBuilderInterface $form)
    {
        $form->add('email', EmailType::class, [
            'label' => 'Notificatie emailadres',
            'required' => false,
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $form->add('email_notification_enabled', YesNoType::class, [
            'label' => 'Notificaties verzenden per email',
            'required' => false,
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $form->add('phone', TextType::class, [
            'label' => 'Notificatie telefoonnummer',
            'required' => false,
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);
    }

    /**
     * @param Supplier $supplier
     *
     * @return string
     */
    public function login(Supplier $supplier)
    {
        // Todo: get current logged (Topgeschenken) user for logging
        $supplierUsers = $supplier->getUsers()->filter(function (Customer $customer) {
            return $customer->isEnabled();
        });

        if ($supplierUsers->isEmpty()) {
            throw new \RuntimeException('No users were found for the supplier');
        }

        $supplierUser = $supplierUsers->current();

        if ($supplierUser->getUsername() === null) {
            throw new \RuntimeException('User has no username set');
        }

        $token = $this->JWTTokenManager->create($supplierUser);

        $apiUrl = $this->parameterBag->get('connector_bakker_api_url');
        return sprintf($apiUrl . '/tgapi/auth/login/%s', $token);
    }
}
