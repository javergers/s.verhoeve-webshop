<?php

namespace AppBundle\Connector;

use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Services\Admin;
use AppBundle\Services\OrderAssetsHashService;
use AppBundle\Services\Supplier\HashService;
use Swift_Mailer;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class AbstractMailConnector
 * @package AppBundle\Connector
 */
abstract class AbstractMailConnector extends AbstractConnector
{
    use ContainerAwareTrait;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var bool
     */
    protected $showSku = false;

    /**
     * @var bool
     */
    protected $showPrices = true;

    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var Admin
     */
    private $admin;

    /**
     * @var TwigEngine
     */
    private $twig;

    /**
     * @var OrderAssetsHashService
     */
    private $orderAssetsHashService;

    /**
     * @var HashService
     */
    private $hashService;

    /**
     * AbstractMailConnector constructor.
     * @param Swift_Mailer           $mailer
     * @param RouterInterface        $router
     * @param Admin                  $admin
     * @param TwigEngine             $twig
     * @param OrderAssetsHashService $orderAssetsHashService
     * @param HashService            $hashService
     */
    public function __construct(
        Swift_Mailer $mailer,
        RouterInterface $router,
        Admin $admin,
        TwigEngine $twig,
        OrderAssetsHashService $orderAssetsHashService,
        HashService $hashService
    ) {
        $this->mailer = $mailer;
        $this->router = $router;
        $this->admin = $admin;
        $this->twig = $twig;
        $this->orderAssetsHashService = $orderAssetsHashService;
        $this->hashService = $hashService;
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @throws \Twig_Error
     */
    public function process(SupplierOrder $supplierOrder)
    {
        if (!$this->getParameter('email')) {
            throw new \RuntimeException(vsprintf("Er is geen e-mailadres ingesteld bij leverancier '%s'", [
                $supplierOrder->getSupplier()->getName(),
            ]));
        }

        $subject = sprintf('Nieuwe opdracht (onze referentie: %s', $supplierOrder->getOrder()->getOrderNumber());

        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom($this->getFromEmail($supplierOrder), $this->getFromName($supplierOrder))
            ->addTo($this->getParameter('email'), $supplierOrder->getSupplier()->getName())
            ->setBody($this->generateBody($supplierOrder), 'text/html');

        if($this->getParameter('cc')){
            $message->setCc(\array_map('trim', \explode(';', $this->getParameter('cc'))));
        }

        $this->mailer->send($message);

        parent::process($supplierOrder);
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @param string        $status
     * @throws \Exception
     * @throws \Twig_Error
     */
    public function notify(SupplierOrder $supplierOrder, string $status)
    {
        if (!$this->getParameter('email')) {
            throw new \RuntimeException(vsprintf("Er is geen e-mailadres ingesteld bij leverancier '%s'", [
                $supplierOrder->getSupplier()->getName(),
            ]));
        }

        $order = $supplierOrder->getOrder();

        $fromEmail = $order->getOrderCollection()->getSite()->getEmail();
        $fromName = ucfirst($order->getOrderCollection()->getSite()->getPrimaryDomain());

        if ($status === 'pending') {
            $subject = 'Onverwerkte order voor vandaag';
        } else {
            $subject = 'Order zonder bezorgstatus, bezorging voor vandaag (onze referentie: %s)';
            $subject = sprintf($subject, $order->getOrderNumber());
        }

        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom($fromEmail, $fromName)
            ->addTo($this->getParameter('email'), $supplierOrder->getSupplier()->getName())
            ->setBody($this->generateBody($supplierOrder, $status), 'text/html');

        $this->mailer->send($message);
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @return string
     *
     * @throws \Exception
     * @throws \Twig_Error
     */
    public function preview(SupplierOrder $supplierOrder): string
    {
        return $this->generateBody($supplierOrder);
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @param string        $status
     * @param string        $templateName
     * @return string
     *
     * @throws \Twig_Error
     * @throws \Exception
     */
    protected function generateBody(
        SupplierOrder $supplierOrder,
        string $status = 'process',
        string $templateName = '@App/supplier-order.html.twig'
    ) {
        $order = $supplierOrder->getOrder();

        $this->router->setContext($this->admin->getRequestContext());
        $this->orderAssetsHashService->setOrder($order);
        $assets = $this->orderAssetsHashService->buildHashes();

        $site = $order->getOrderCollection()->getSite();

        return $this->twig->render($templateName, [
            'supplierOrder' => $supplierOrder,
            'showSku' => $this->showSku,
            'showPrices' => $this->showPrices,
            'hash' => [
                'processed' => $this->getUpdateProcessStatusUrl($supplierOrder),
                'delivered' => $this->getUpdateDeliveryStatusUrl($supplierOrder),
            ],
            'status' => $status,
            'assets' => $assets,
            'fromEmail' => $site->getEmail(),
            'fromPhone' => $site->getPhoneNumber(),
            'fromDescription' => $site->translate()->getDescription(),
        ]);
    }

    /**
     * @param string $route
     * @param array  $params
     * @return string
     */
    public function getRouteUrl($route = '', array $params = []): string
    {
        /** @var UrlGeneratorInterface $generator */
        $generator = $this->router->getGenerator();

        return rtrim($generator->generate($route, $params, UrlGeneratorInterface::ABSOLUTE_URL), '/');
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @return string
     */
    public function getFromEmail(SupplierOrder $supplierOrder): string
    {
        return $supplierOrder->getOrder()->getOrderCollection()->getSite()->getEmail();
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @return string
     */
    public function getFromName(SupplierOrder $supplierOrder): string
    {
        return ucfirst($supplierOrder->getOrder()->getOrderCollection()->getSite()->getPrimaryDomain());
    }

    /**
     * @param FormBuilderInterface $form
     */
    public function addFormBuilderParameters(FormBuilderInterface $form)
    {
        $form->add('email', EmailType::class, [
            'label' => 'Notificatie emailadres',
            'required' => false,
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $form->add('phone', TextType::class, [
            'label' => 'Notificatie telefoonnummer',
            'required' => false,
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $form->add('cc', TextType::class, [
            'label' => 'CC emailadressen (; gescheiden)',
            'required' => false,
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @param string        $status
     * @return string
     */
    private function getUpdateProcessStatusUrl(SupplierOrder $supplierOrder, $status = 'processed'): string
    {
        $hashObj = new \stdClass();
        $hashObj->type = 'processStatus';
        $hashObj->data = ['status' => $status];
        $hashObj->orderId = $supplierOrder->getOrder()->getId();
        $hashObj->supplierId = $supplierOrder->getSupplier()->getId();

        $hashedObj = $this->hashService->encryptHash($hashObj);

        return $this->getRouteUrl('app_tempapi_orderorder_setprocessstatus', [
            'hash' => $hashedObj,
        ]);
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @param string        $status
     * @return string
     */
    private function getUpdateDeliveryStatusUrl(SupplierOrder $supplierOrder, $status = 'delivered'): string
    {
        $hashObj = new \stdClass();
        $hashObj->type = 'deliveryStatus';
        $hashObj->data = ['status' => $status];
        $hashObj->orderId = $supplierOrder->getOrder()->getId();
        $hashObj->supplierId = $supplierOrder->getSupplier()->getId();

        $hashedObj = $this->hashService->encryptHash($hashObj);

        return $this->getRouteUrl('app_tempapi_orderorder_setdeliverystatus', [
            'hash' => $hashedObj,
        ]);
    }
}
