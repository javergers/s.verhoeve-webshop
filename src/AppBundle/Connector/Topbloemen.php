<?php

namespace AppBundle\Connector;

use AppBundle\Connector\Client\TopbloemenClient;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Entity\Supplier\SupplierOrderLine;
use function array_key_exists;
use Sabre\Xml\Service;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class Topbloemen
 * @package AppBundle\Connector
 */
class Topbloemen extends AbstractConnector
{
    /**
     * @var string
     */
    protected $color = '#8C914F';

    /**
     * @var bool
     */
    protected $requireForwardingPrices = true;

    /**
     * @param SupplierOrder $supplierOrder
     * @throws \Exception
     */
    public function process(SupplierOrder $supplierOrder)
    {
        $orderReferences = [];

        $splittedSupplierOrder = $this->splitSupplierOrder($supplierOrder);

        foreach ($splittedSupplierOrder as $i => $splittedSupplierOrderLines) {
            $combinedOrderRemark = null;

            if ($i >= 1) {
                $combinedOrderRemark = 'LET OP: Meerdere orders voor dit adres. Deze order geen bezorgkosten.';
            }

            $xml = $this->buildXml($supplierOrder, $splittedSupplierOrderLines, $combinedOrderRemark);

            $soapClient = new TopbloemenClient($this->getParameter('webservice_wsdl'), [
                'login' => $this->getParameter('webservice_username'),
                'password' => $this->getParameter('webservice_password'),
            ]);

            $soapClient->setStrictMode(false);

            $result = $soapClient->create($xml);

            $topbloemenOrderId = $result->orderId;

            if (empty($topbloemenOrderId)) {
                throw new \RuntimeException(ucfirst($result->error->source) . ': ' . $result->error->message);
            }

            $orderReferences[] = $topbloemenOrderId;
        }

        $supplierOrder->setReference(implode(', ', $orderReferences));

        parent::process($supplierOrder);
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @return array
     */
    private function splitSupplierOrder(SupplierOrder $supplierOrder): array
    {
        $splittedSupplierOrders = [];

        foreach ($supplierOrder->getLines() as $supplierOrderLine) {
            $supplierProduct = $supplierOrderLine->getConcludedSupplierProduct();

            if ($supplierProduct->getProduct() instanceof TransportType) {
                continue;
            }

            if ($supplierProduct->getSku()[0] === 'P') {
                $splittedSupplierOrders[] = [];
            }

            $splittedSupplierOrders[\count($splittedSupplierOrders) - 1][] = $supplierOrderLine;
        }

        foreach ($splittedSupplierOrders as &$splittedSupplierOrder) {
            foreach ($supplierOrder->getLines() as $supplierOrderLine) {
                $supplierProduct = $supplierOrderLine->getConcludedSupplierProduct();

                if (!$supplierProduct->getProduct() instanceof TransportType) {
                    continue;
                }

                $splittedSupplierOrder[] = $supplierOrderLine;
            }
        }

        return $splittedSupplierOrders;
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @param array         $splittedSupplierOrderLines
     * @param string|null   $combinedOrderRemark
     * @return string
     */
    private function buildXml(
        SupplierOrder $supplierOrder,
        array $splittedSupplierOrderLines,
        ?string $combinedOrderRemark
    ): string {
        $order = $supplierOrder->getOrder();
        $orderCollection = $order->getOrderCollection();

        $xmlService = new Service();

        $data = [
            'ip' => $orderCollection->getIp(),
            'businessAccount' => [
                'emailAddress' => $this->getParameter('businessaccount_username'),
                'password' => $this->getParameter('businessaccount_password'),
            ],
            'sender' => [
                'company' => $orderCollection->getInvoiceAddressCompanyName(),
                'attn' => $orderCollection->getInvoiceAddressAttn(),
                'address' => $orderCollection->getInvoiceAddressStreetAndNumber(),
                'postcode' => $orderCollection->getInvoiceAddressPostcode(),
                'city' => $orderCollection->getInvoiceAddressCity(),
                'country' => $orderCollection->getInvoiceAddressCountry()->getCode(),
                'phone' => $orderCollection->getInvoiceAddressPhoneNumber(),
                'emailAddress' => $orderCollection->getCustomer()->getEmail(),
            ],
            'recipient' => [
                'company' => $order->getDeliveryAddressCompanyName(),
                'attn' => $order->getDeliveryAddressAttn(),
                'address' => $order->getDeliveryAddressStreetAndNumber(),
                'postcode' => $order->getDeliveryAddressPostcode(),
                'city' => $order->getDeliveryAddressCity(),
                'country' => $order->getDeliveryAddressCountry()->getCode(),
                'phone' => $order->getDeliveryAddressPhoneNumber(),
            ],
        ];

        $this->processLines($splittedSupplierOrderLines, $data);

        $remark = trim(implode(PHP_EOL, [
            $order->getSupplierRemark(),
            $order->getDeliveryRemark(),
            $combinedOrderRemark,
        ]));

        if ($remark) {
            $data['remark'] = $remark;
        }

        $data['paymentMethod'] = 'businessAccount';
        $data['reference'] = trim($order->getOrderNumber() . ' ' . $order->getInvoiceReference());

        $xml = $xmlService->write('order', $data);

        return str_replace('attn>', 'name>', $xml);
    }

    /**
     * @param SupplierOrderLine[] $splittedSupplierOrderLines
     * @param                     $data
     */
    private function processLines(array $splittedSupplierOrderLines, &$data)
    {
        $parts = [];

        foreach ($splittedSupplierOrderLines as $supplierOrderLine) {
            $supplierProduct = $supplierOrderLine->getConcludedSupplierProduct();

            if ($supplierOrderLine->getConcludedSupplierProduct()->getProduct() instanceof TransportType) {
                if (array_key_exists('delivery', $parts)) {
                    throw new \RuntimeException('Delivery already assigned.');
                }

                $parts['delivery'] = $this->convertDelivery($supplierOrderLine, 0.001);
            } else {
                if (!$supplierProduct->getSku()) {
                    $format = "SupplierProduct with ID %s doesn't have a SKU";

                    throw new \RuntimeException(sprintf($format, $supplierProduct->getId()));
                }

                switch ($supplierProduct->getSku()[0]) {
                    case 'P':
                        if (array_key_exists('product', $parts)) {
                            throw new \RuntimeException('Product already assigned.');
                        }

                        $parts['product'] = $this->convertProduct($supplierOrderLine);
                        break;
                    case 'V':
                        if (array_key_exists('vase', $parts)) {
                            throw new \RuntimeException('Vase already assigned.');
                        }

                        $parts['vase'] = $this->convertVase($supplierOrderLine);
                        break;
                    case 'C':
                        if (array_key_exists('card', $parts)) {
                            throw new \RuntimeException('Card already assigned.');
                        }

                        $parts['card'] = $this->convertCard($supplierOrderLine);
                        break;
                    case 'A':
                        if (array_key_exists('additionalProduct', $parts)) {
                            throw new \RuntimeException('Additional product already assigned.');
                        }

                        $parts['additionalProduct'] = $this->convertAdditionalProduct($supplierOrderLine);
                        break;
                    default:
                        $format = "Couldn't process product with SKU %s";

                        throw new \RuntimeException(sprintf($format, $supplierProduct->getSku()));
                }
            }
        }

        uksort($parts, function (string $key1, string $key2) {
            $keys = ['delivery', 'product', 'vase', 'card', 'additionalProduct'];

            return array_search($key1, $keys, true) <=> array_search($key2, $keys, true);
        });

        foreach ($parts as $key => $part) {
            $data[$key] = $part;
        }
    }

    /**
     * @param SupplierOrderLine $supplierOrderLine
     * @return array
     */
    private function convertProduct(SupplierOrderLine $supplierOrderLine): array
    {
        $sku = $this->retrieveSkuCode($supplierOrderLine->getConcludedSupplierProduct()->getSku());

        $skuParts = explode('-', $sku);

        if (\count($skuParts) !== 2) {
            throw new \RuntimeException(vsprintf("Supplier product '%s' SKU '%s' must match format '%%s%%d-%%d'", [
                $supplierOrderLine->getConcludedSupplierProduct()->translate()->getTitle(),
                $supplierOrderLine->getConcludedSupplierProduct()->getSku(),
            ]));
        }

        return [
            'id' => $skuParts[0],
            'variationId' => $skuParts[1],
            'amount' => $supplierOrderLine->getQuantity(),
            'price' => $supplierOrderLine->getPrice(),
        ];
    }

    /**
     * @param SupplierOrderLine $supplierOrderLine
     * @return array
     */
    private function convertVase(SupplierOrderLine $supplierOrderLine): array
    {
        $sku = $this->retrieveSkuCode($supplierOrderLine->getConcludedSupplierProduct()->getSku());

        return [
            'id' => $sku,
            'amount' => $supplierOrderLine->getQuantity(),
            'price' => $supplierOrderLine->getPrice(),
        ];
    }

    /**
     * @param SupplierOrderLine $supplierOrderLine
     * @return array
     */
    private function convertAdditionalProduct(SupplierOrderLine $supplierOrderLine): array
    {
        $sku = $this->retrieveSkuCode($supplierOrderLine->getConcludedSupplierProduct()->getSku());

        return [
            'id' => $sku,
            'amount' => $supplierOrderLine->getQuantity(),
            'price' => $supplierOrderLine->getPrice(),
        ];
    }

    /**
     * @param SupplierOrderLine $supplierOrderLine
     * @return array
     */
    private function convertCard(SupplierOrderLine $supplierOrderLine): array
    {
        $sku = $this->retrieveSkuCode($supplierOrderLine->getConcludedSupplierProduct()->getSku());
        $text = '';
        $metadata = $supplierOrderLine->getOrderLine()->getMetadata();

        if ($metadata) {
            if (array_key_exists('text', $metadata)) {
                $text = $metadata['text'];
            } elseif (array_key_exists('input_fields', $metadata)) {
                foreach ($metadata['input_fields'] as $inputField) {
                    if (empty($inputField)) {
                        continue;
                    }

                    $text .= $inputField . '&#xA;';
                }
            }
        }

        return [
            'id' => $sku,
            'amount' => $supplierOrderLine->getQuantity(),
            'price' => $supplierOrderLine->getPrice(),
            'text' => $text,
        ];
    }

    /**
     * @param SupplierOrderLine $supplierOrderLine
     * @param float|null        $price
     * @return array
     */
    private function convertDelivery(SupplierOrderLine $supplierOrderLine, float $price = null): array
    {
        $delivery = [
            'date' => $supplierOrderLine->getSupplierOrder()->getOrder()->getDeliveryDate()->format('Y-m-d'),
            'price' => $price ?? $supplierOrderLine->getPrice()
        ];

        return $delivery;
    }

    /**
     * @param FormBuilderInterface $form
     */
    public function addFormBuilderParameters(FormBuilderInterface $form)
    {
        // Are not used on the moment and disabled to

//        $form->add('webservice_wsdl', TextType::class, [
//            'label' => 'Webservice WSDL',
//            'data' => 'https://webservice.topbloemen.nl/soap/order/?wsdl',
//            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
//            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
//        ]);
//
//        $form->add('webservice_username', TextType::class, [
//            'label' => 'Webservice gebruikersnaam',
//            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
//            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
//        ]);
//
//        $form->add('webservice_password', TextType::class, [
//            'label' => 'Webservice wachtwoord',
//            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
//            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
//        ]);
//
//        $form->add('businessaccount_username', TextType::class, [
//            'label' => 'Businessaccount gebruikersnaam',
//            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
//            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
//        ]);
//
//        $form->add('businessaccount_password', TextType::class, [
//            'label' => 'Businessaccount wachtwoord',
//            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
//            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
//        ]);
//
//        $form->add('email', EmailType::class, [
//            'label' => 'Notificatie emailadres',
//            'required' => false,
//            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
//            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
//        ]);
//
//        $form->add('phone', TextType::class, [
//            'label' => 'Notificatie telefoonnummer',
//            'required' => false,
//            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
//            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
//        ]);
    }

    /**
     * @param $sku
     * @return string
     */
    private function retrieveSkuCode($sku)
    {
        return substr($sku, 1);
    }
}
