<?php

namespace AppBundle\Connector;

use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Entity\Supplier\SupplierOrderLine;
use AppBundle\Entity\Supplier\SupplierProduct;
use AppBundle\Services\Designer;
use AppBundle\Services\SupplierManagerService;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class Geschenkbezorgen
 * @package AppBundle\Connector
 */
class Geschenkbezorgen extends AbstractConnector
{
    /**
     * @var string
     */
    protected $color = '#2196F3';

    /**
     * @var string
     */
    protected $shortName = 'Intern';

    /**
     * @var SupplierManagerService
     */
    private $supplierManager;

    /**
     * @var Designer
     */
    private $designer;

    /**
     * Geschenkbezorgen constructor.
     * @param SupplierManagerService $supplierManager
     */
    public function __construct(SupplierManagerService $supplierManager, Designer $designer)
    {
        $this->supplierManager = $supplierManager;
        $this->designer = $designer;
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @throws \Exception
     */
    public function process(SupplierOrder $supplierOrder)
    {
        $data = $this->transformOrder($supplierOrder);

        $this->transmitOrder(json_encode($data, JSON_PRETTY_PRINT));

        parent::process($supplierOrder);
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @return array
     * @throws \Exception
     */
    public function transformOrder(SupplierOrder $supplierOrder)
    {
        $order = $supplierOrder->getOrder();

        $products = [];
        $additionalProductLines = [];

        foreach ($supplierOrder->getLines() as $supplierOrderLine) {
            $orderLine = $supplierOrderLine->getOrderLine();

            if (!($orderLine->getMetadata() && array_key_exists('additional_product_parent',
                    $orderLine->getMetadata()))) {
                continue;
            }

            $additionalProductLines[$orderLine->getMetadata()['additional_product_parent']] = $orderLine->getProduct();
        }

        foreach ($supplierOrder->getLines() as $supplierOrderLine) {
            $products[] = $this->transformOrderLine($supplierOrderLine, $additionalProductLines);
        }

        $products = array_filter($products);

        $data = [
            'or_referentie' => $order->getOrderNumber(),
            'or_bezorgdatum' => $order->getDeliveryDate()->format('d-m-Y'),
            'or_bedrijfsnaam' => $order->getDeliveryAddressCompanyName(),
            'or_naam' => $order->getDeliveryAddressAttn(),
            'or_straat' => $order->getDeliveryAddressStreet(),
            'or_huisnummer' => trim($order->getDeliveryAddressNumber() . '-' . $order->getDeliveryAddressNumberAddition(),
                '-'),
            'or_postcode' => $order->getDeliveryAddressPostcode(),
            'or_plaats' => $order->getDeliveryAddressCity(),
            'or_products' => $products,
            'or_opmerking' => trim($order->getSupplierRemark() . PHP_EOL . $order->getDeliveryRemark()),
        ];

        return $data;
    }

    /**
     * @param SupplierOrderLine $supplierOrderLine
     * @param                   $additionalProductLines
     * @return array|null
     * @throws \Exception
     */
    public function transformOrderLine(SupplierOrderLine $supplierOrderLine, $additionalProductLines)
    {
        $orderLine = $supplierOrderLine->getOrderLine();

        if ($supplierOrderLine->getOrderLine()->getParent()) {
            return null;
        }

        $supplierProduct = $supplierOrderLine->getConcludedSupplierProduct();

        if (!$supplierProduct) {
            return null;
        }

        if (!$supplierProduct->getProduct()->getPurchasable()) {
            return null;
        }

        if ($orderLine->getMetadata() && array_key_exists('additional_product_parent', $orderLine->getMetadata())) {
            return null;
        }

        $data = [
            'or_product_ID' => $supplierProduct->getSku(),
            'or_product_aantal' => $orderLine->getQuantity(),
        ];

        if (!empty($additionalProductLines[$orderLine->getProduct()->getId()])) {
            $supplierAdditionalProduct = $this->supplierManager->getSupplierProduct($additionalProductLines[$orderLine->getProduct()->getId()],
                $supplierOrderLine->getSupplierOrder()->getSupplier());

            if ($supplierAdditionalProduct !== null) {
                $data['or_extra_product_ID'] = $supplierAdditionalProduct->getSku();
            }
        }

        foreach ($orderLine->getChildren() as $child) {
            /** @var $child OrderLine */
            if ($child->getMetadata() && array_key_exists('text', $child->getMetadata())) {
                $cardText = $child->getMetadata()['text'];
                $cardId = null;

                if ($child->getProduct()) {
                    $supplierProducts = $child->getProduct()->getSupplierProducts()->filter(function (
                        SupplierProduct $supplierProduct
                    ) use ($supplierOrderLine) {
                        return $supplierProduct->getSupplier() === $supplierOrderLine->getSupplierOrder()->getSupplier();
                    });

                    if ($supplierProducts) {
                        $cardId = $supplierProducts->current()->getSku();
                    }

                    $data['or_product_kaart'] = $cardText;
                    $data['or_product_kaart_ID'] = $cardId;
                }
            }

            if ($child->getMetadata() && array_key_exists('designer', $child->getMetadata())) {
                $this->designer->setUuid($child->getMetadata()['designer']);

                $printUrl = $this->designer->getFileUrl(Designer::FILE_PRINT, 3600);

                if ($printUrl !== null) {
                    $data['or_product_afbeelding'] = $printUrl;
                }
            }
        }

        return $data;
    }

    /**
     * @param $json
     *
     * @throws \Exception
     */
    private function transmitOrder($json)
    {
        $url = $this->parameterBag->get('connector_gb_api_url');

        $response = $this->getGuzzleClient($url)->post($this->getParameter('api_key') . '/', [
            'form_params' => [
                'bestelling' => $json,
            ],
        ]);

        if ($response->getStatusCode() !== 200) {
            $response = json_decode($response->getBody()->getContents());

            throw new \RuntimeException($response->error_message);
        }

        // TODO store reference
    }

    /**
     * @param FormBuilderInterface $form
     */
    public function addFormBuilderParameters(FormBuilderInterface $form)
    {
        $form->add('api_key', TextType::class, [
            'label' => 'API key',
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $form->add('email', EmailType::class, [
            'label' => 'Notificatie emailadres',
            'required' => false,
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $form->add('phone', TextType::class, [
            'label' => 'Notificatie telefoonnummer',
            'required' => false,
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);
    }
}
