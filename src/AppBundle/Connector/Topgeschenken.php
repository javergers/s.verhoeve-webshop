<?php

namespace AppBundle\Connector;

use AppBundle\Entity\Supplier\SupplierOrder;

/**
 * Class Topgeschenken
 * @package AppBundle\Connector
 */
class Topgeschenken extends AbstractMailConnector
{
    /**
     * @var string
     */
    protected $color = '#b5251a';

    /**
     * @var bool
     */
    protected $showSku = true;

    /**
     * @var bool
     */
    protected $showPrices = false;

    /**
     * @var bool
     */
    protected $requireForwardingPrices = false;

    /**
     * @var bool
     */
    protected $alwaysShowForwardingPrices = false;

    /**
     * @param SupplierOrder $supplierOrder
     * @param string        $status
     * @param string        $templateName
     * @return string
     * @throws \Twig_Error
     * @throws \Exception
     */
    protected function generateBody(
        SupplierOrder $supplierOrder,
        $status = 'process',
        $templateName = '@App/supplier-order-intern.html.twig'
    ) {
        return parent::generateBody($supplierOrder, $status, $templateName);
    }
}
