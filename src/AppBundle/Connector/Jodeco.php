<?php

namespace AppBundle\Connector;

/**
 * Class Jodeco
 * @package AppBundle\Connector
 */
class Jodeco extends AbstractMailConnector
{
    /**
     * @var bool
     */
    protected $showSku = true;
}