<?php

namespace AppBundle\DependencyInjection;

use function exec;
use function ltrim;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Class AppExtension
 * @package AppBundle\DependencyInjection
 */
class AppExtension extends Extension implements PrependExtensionInterface
{
    /**
     * @param array $configs
     * @param ContainerBuilder $container
     *
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        void($configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $this->loadThirdParty($container);
    }

    /**
     * @param ContainerBuilder $container
     *
     * @throws \Exception
     */
    private function loadThirdParty(ContainerBuilder $container)
    {
        $fs = new Filesystem();

        /** @var SplFileInfo $directory */
        foreach ((new Finder())->directories()->in(__DIR__ . '/../ThirdParty')->depth(0) as $directory) {
            $path = sprintf('%s/Resources/config', $directory->getRealPath());

            if (!$fs->exists($path)) {
                continue;

            }
            $loader = new YamlFileLoader($container, new FileLocator($path));

            /** @var SplFileInfo $file */
            foreach ((new Finder())->files()->in($path)->name('*.yml') as $file) {
                if ($file->getBasename() === 'routing.yml') {
                    continue;
                }

                $loader->load($file->getBasename());
            }
        }
    }

    /**
     * Allow an extension to prepend the extension configurations.
     */
    public function prepend(ContainerBuilder $container)
    {
        $version = ltrim(exec('git describe --tags'), 'vV');
        $container->setParameter('application_version', $version);
    }
}