<?php

namespace AppBundle\Entity\Relation;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;
use RuleBundle\Annotation as Rule;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * @ORM\Entity()
 * @method CompanyCustomFieldTranslation translate(string $locale = null, boolean $fallbackToDefault = null)
 */
class CompanyCustomField
{

    use ORMBehaviors\Translatable\Translatable;
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\Regex("([A-Za-z0-9\-\_]+)")
     */
    protected $fieldKey;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="customFields", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false);
     */
    protected $company;

    /**
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Relation\CompanyCustomFieldOption", mappedBy="companyCustomField",
     *                                                                                   cascade={"persist"})
     */
    protected $options;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $required;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $multiple;

    /**
     * @ORM\Column(type="boolean", options={"default" : true})
     */
    protected $showOnInvoice;

    /**
     * @ORM\Column(type="boolean", options={"default" : true})
     */
    protected $invoiceGroup;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $position;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    protected $groupInvoices = false;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\AppliesToOrderType")
     * @ORM\Column(type="AppliesToOrderType", options={"default" : "order_collection"})
     * @Rule\Property(description="Link met order type")
     */
    protected $appliesTo;

    public function __construct()
    {
        $this->appliesTo = 'order_collection';
        $this->options = new ArrayCollection();
        $this->showOnInvoice = true;
        $this->invoiceGroup = true;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return CompanyCustomField
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get required
     *
     * @return boolean
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set required
     *
     * @param boolean $required
     *
     * @return CompanyCustomField
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * Get multiple
     *
     * @return boolean
     */
    public function getMultiple()
    {
        return $this->multiple;
    }

    /**
     * Set multiple
     *
     * @param boolean $multiple
     *
     * @return CompanyCustomField
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return CompanyCustomField
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get companySite
     *
     * @return CompanySite
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set companySite
     *
     * @param Company $company
     *
     * @return CompanyCustomField
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Add option
     *
     * @param CompanyCustomFieldOption $option
     *
     * @return CompanyCustomField
     */
    public function addOption(CompanyCustomFieldOption $option)
    {
        $option->setCompanyCustomField($this);

        $this->options[] = $option;

        return $this;
    }

    /**
     * Remove option
     *
     * @param CompanyCustomFieldOption $option
     */
    public function removeOption(CompanyCustomFieldOption $option)
    {
        $this->options->removeElement($option);
    }

    /**
     * Get options
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param $fieldKey
     * @return $this
     */
    public function setFieldKey($fieldKey)
    {
        $this->fieldKey = $fieldKey;

        return $this;
    }

    /**
     * @return string
     */
    public function getFieldKey()
    {
        return $this->fieldKey;
    }

    /**
     * @param string|null $appliesTo
     * @return $this
     */
    public function setAppliesTo(?string $appliesTo): CompanyCustomField
    {
        $this->appliesTo = $appliesTo;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAppliesTo(): ?string
    {
        return $this->appliesTo;
    }

    /**
     * @return bool
     */
    public function isGroupInvoices(): bool
    {
        return $this->groupInvoices;
    }

    /**
     * @param bool $groupInvoices
     */
    public function setGroupInvoices(bool $groupInvoices): void
    {
        $this->groupInvoices = $groupInvoices;
    }

    /**
     * @return bool
     */
    public function getShowOnInvoice(): bool
    {
        return $this->showOnInvoice;
    }

    /**
     * @param bool $showOnInvoice
     */
    public function setShowOnInvoice(bool $showOnInvoice): void
    {
        $this->showOnInvoice = $showOnInvoice;
    }

    /**
     * @param bool $invoiceGroup
     */
    public function setInvoiceGroup(bool $invoiceGroup): void
    {
        $this->invoiceGroup = $invoiceGroup;
    }

    /**
     * @return bool
     */
    public function getInvoiceGroup(): bool
    {
        return $this->invoiceGroup;
    }
}
