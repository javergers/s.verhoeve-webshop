<?php

namespace AppBundle\Entity\Relation;

use AppBundle\Entity\Catalog\Product\Productgroup;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="company_productgroup", columns={"company_id",
 *                                                                                  "product_group_id"})})
 */
class CompanyCommission
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="supplierCommissions")
     */
    protected $company;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Productgroup")
     * @ORM\JoinColumn(nullable=false))
     */
    protected $productGroup;

    /**
     * @ORM\Column(type="integer")
     */
    protected $percentage;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set percentage
     *
     * @param integer $percentage
     *
     * @return CompanyCommission
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Get percentage
     *
     * @return integer
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return CompanyCommission
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set productGroup
     *
     * @param Productgroup $productGroup
     *
     * @return CompanyCommission
     */
    public function setProductGroup(Productgroup $productGroup)
    {
        $this->productGroup = $productGroup;

        return $this;
    }

    /**
     * Get productGroup
     *
     * @return Productgroup
     */
    public function getProductGroup()
    {
        return $this->productGroup;
    }
}
