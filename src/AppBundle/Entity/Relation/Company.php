<?php

namespace AppBundle\Entity\Relation;

use AppBundle\Annotation\GeneralListener;
use AppBundle\DBAL\Types\CompanyTrustStatusType;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Product\CompanyCard;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\CompanyTransportType;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Designer\DesignerTemplate;
use AppBundle\Entity\Discount\Discount;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Payment\Paymentmethod;
use AppBundle\Entity\Report\CompanyReport;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Entity\Site\Menu;
use AppBundle\Entity\Site\Site;
use AppBundle\Entity\Supplier\DeliveryArea;
use AppBundle\Entity\Supplier\SupplierGroup;
use AppBundle\Entity\Supplier\SupplierProduct;
use AppBundle\Interfaces\Audit\AuditEntityInterface;
use AppBundle\Interfaces\ConnectorInterface;
use AppBundle\Model\CompanyModel;
use AppBundle\Traits\UuidEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use RuleBundle\Annotation as Rule;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Validator\Constraints as Assert;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use AppBundle\Services\ConnectorHelper;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompanyRepository")
 * @ORM\Table(indexes={
 *     @ORM\Index(columns={"twinfield_relation_number"})
 * })
 * @ORM\EntityListeners({"AppBundle\EventListener\CompanyListener"})
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 * @GeneralListener(parent="Company", parentGetter="getParent", get="updatedAt", set="updatedAt")
 *
 * @Rule\Entity(description="Bedrijf")
 */
class Company extends CompanyModel implements AuditEntityInterface
{
    use UuidEntity;

    public const ADDRESS_TYPE_MAIN = 'main';
    public const ADDRESS_TYPE_INVOICE = 'invoice';
    public const ADDRESS_TYPE_DELIVERY = 'delivery';

    public const CREDIT_LIMIT = 150;

    use TimestampableEntity;
    use SoftDeleteableEntity;
    use ContainerAwareTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Carrier\Carrier", mappedBy="company")
     */
    private $carrier;

    /**
     * @var string
     * @ORM\Column(type="string", length=9, nullable=true)
     * @Assert\Regex("/^(?:D\d{4}|[A-Z]{2}\d{7})$/")
     */
    protected $twinfieldRelationNumber;
    /**
     * @var CompanyRegistration $companyRegistration
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\CompanyRegistration", inversedBy="companies",
     *                                                                             cascade={"persist"})
     */
    protected $companyRegistration;

    /**
     * @var CompanyInvoiceSettings $companyInvoiceSettings
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Relation\CompanyInvoiceSettings", cascade={"persist"})
     */
    protected $companyInvoiceSettings;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Security\Customer\CustomerGroup")
     */
    protected $customerGroups;
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Supplier\SupplierGroup", inversedBy="suppliers")
     * @Rule\Property(description="Leveranciersgroep", autoComplete={"id"="suppliergroup.id",
     *                                                 "label"="suppliergroup.name"}, relation="child")
     */
    protected $supplierGroups;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Supplier\SupplierProduct", mappedBy="supplier",
     *                                                                          cascade={"persist"})
     */
    protected $supplierProducts;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * Assert\NotBlank(message = "company.company.not_blank", groups={"company_registration", "company_profile"})
     * @Rule\Property(description="Bedrijfsnaam")
     */
    protected $name;
    /**
     * @ORM\Column(type="string", length=35, nullable=true)
     */
    protected $phoneNumber;
    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     * @Rule\Property(description="Bedrijfsemail")
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $invoiceEmail;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Address", cascade={"persist"})
     */
    protected $invoiceAddress;

    /**
     * @deprecated
     * @ORM\Column(type="integer", options={"default" : 14})
     * @Rule\Property(description="Betaaltermijn")
     */
    protected $paymentTerm = 14;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $creditLimit;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Rule\Property(description="Is klant")
     */
    protected $isCustomer;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Rule\Property(description="Is leverancier")
     */
    protected $isSupplier;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $internal_remark;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $handle_child_orders;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $bill_on_parent;
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\NotBlank(message = "company.chamber_of_commerce_number.not_blank", groups={"company_profile_nl"})
     */
    protected $chamberOfCommerceNumber;
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $chamberOfCommerceEstablishmentNumber;
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\NotBlank(message = "company.chamber_of_commerce_number.not_blank", groups={"company_profile_be"})
     */
    protected $vatNumber;
    /**
     * OIN = Overheids Identificatie Nummer
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Assert\Regex(pattern="/^[0-9]{20}$/m", message="OIN nummer moet bestaan uit 20 cijfers")
     */
    protected $oinNumber;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Security\Customer\Customer", mappedBy="company",
     *                                                                            cascade={"persist"})
     * @Assert\Valid()
     * @Rule\Property(description="Contactpersoon", autoComplete={"id"="customer.id", "label"="customer.fullname"},
     *                                              relation="child")
     */
    protected $customers;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Relation\Address", mappedBy="company", cascade={"persist"})
     * @Assert\Valid()
     */
    protected $addresses;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Relation\CompanyDomain", mappedBy="company", cascade={"persist"})
     * @Assert\Valid()
     */
    protected $companyDomains;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\OrderCollection", mappedBy="company", cascade={"persist"})
     * @ORM\OrderBy({"id" = "DESC"})
     * @Rule\Property(description="Bestelling", autoComplete={"id"="order.id", "label"="order.number"},
     *                                          relation="child")
     */
    protected $orders;
    /**
     * @ORM\Column(type="boolean")
     */
    protected $verified = false;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Supplier\DeliveryArea", mappedBy="company", cascade={"persist"})
     */
    protected $deliveryAreas;
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Rule\Property(description="Connector")
     */
    protected $supplierConnector;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Relation\CompanyCommission", mappedBy="company",
     *                                                                            cascade={"persist"})
     */
    protected $supplierCommissions;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Relation\CompanyInternalRemark", mappedBy="company", cascade={"remove"})
     */
    protected $companyInternalRemarks;
    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     * @Assert\Iban(
     *     message="Dit is geen geldig International Bank Account Number (IBAN)."
     * )
     */
    protected $iban;
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $bic;
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $bankAccountHolder;
    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $metadata;
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Catalog\Assortment\Assortment", mappedBy="companies")
     */
    protected $assortments;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Site\Site", mappedBy="company")
     */
    protected $sites;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Menu", inversedBy="companies")
     */
    protected $menu;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;
    /**
     * @var CompanyEstablishment
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Relation\CompanyEstablishment", mappedBy="company")
     */
    private $establishments;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Relation\Company", mappedBy="parent")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $children;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Address", cascade={"persist"})
     */
    private $default_delivery_address;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Report\CompanyReport", mappedBy="company")
     */
    protected $reports;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Relation\CompanyRule", mappedBy="company")
     */
    protected $rules;

    /**
     * @var Discount[] $discounts
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Discount\Discount", mappedBy="companies")
     */
    protected $discounts;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\CompanyPriceDisplayModeType")
     * @ORM\Column(type="CompanyPriceDisplayModeType", options={"default"="original"})
     */
    protected $priceDisplayMode;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\CompanyProduct", mappedBy="company")
     */
    protected $products;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Catalog\Product\ProductCard")
     */
    protected $cards;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Relation\CompanyCustomField", mappedBy="company",
     *                                                                             cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $customFields;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    protected $companyProductSkuPrefix;

    /**
     * @var CompanyTransportType[]|Collection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\CompanyTransportType", mappedBy="company")
     */
    protected $transportTypes;

    /**
     * @var DesignerTemplate[]|Collection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Designer\DesignerTemplate", mappedBy="company")
     */
    protected $templates;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\CompanyTrustStatusType")
     * @ORM\Column(type="CompanyTrustStatusType", options={"default" : "normal"})
     * @Rule\Property(description="Trust status")
     */
    protected $trustStatus;

    /**
     * @var $overrideDefaultPriceView
     *
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\PriceViewType")
     * @ORM\Column(type="PriceViewType", nullable=true)
     * @Rule\Property(description="Overschrijf zichtbare prijs (incl/excl)")
     */
    protected $overrideDefaultPriceView;

    /**
     * @var Paymentmethod[]|Collection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Payment\Paymentmethod")
     */
    protected $paymentmethods;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getName();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->customers = new ArrayCollection();
        $this->supplierGroups = new ArrayCollection();
        $this->supplierProducts = new ArrayCollection();
        $this->customerGroups = new ArrayCollection();
        $this->addresses = new ArrayCollection();
        $this->companyDomains = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->deliveryAreas = new ArrayCollection();
        $this->supplierCommissions = new ArrayCollection();
        $this->establishments = new ArrayCollection();
        $this->assortments = new ArrayCollection();
        $this->reports = new ArrayCollection();
        $this->rules = new ArrayCollection();
        $this->discounts = new ArrayCollection();
        $this->companyInternalRemarks = new ArrayCollection();
        $this->customFields = new ArrayCollection();
        $this->cards = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->templates = new ArrayCollection();
        $this->transportTypes = new ArrayCollection();
        $this->paymentmethods = new ArrayCollection();
        $this->trustStatus = 'normal';
        $this->priceDisplayMode = 'original';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        if ($this->name) {
            return $this->name;
        }

        if (null !== ($companyRegistration = $this->getCompanyRegistration())) {
            return $companyRegistration->getName();
        }

        return '';
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return null|CompanyRegistration
     */
    public function getCompanyRegistration()
    {
        return $this->companyRegistration;
    }

    /**
     * @param CompanyRegistration $companyRegistration
     *
     * @return $this
     */
    public function setCompanyRegistration(CompanyRegistration $companyRegistration)
    {
        $this->companyRegistration = $companyRegistration;

        return $this;
    }

    /**
     * @return CompanyInvoiceSettings|null
     */
    public function getCompanyInvoiceSettings(): ?CompanyInvoiceSettings
    {
        if (null === $this->companyInvoiceSettings) {
            $this->companyInvoiceSettings = new CompanyInvoiceSettings();
        }

        return $this->companyInvoiceSettings;
    }

    /**
     * @param CompanyInvoiceSettings $companyInvoiceSettings
     * @return $this
     */
    public function setCompanyInvoiceSettings(CompanyInvoiceSettings $companyInvoiceSettings): self
    {
        $this->companyInvoiceSettings = $companyInvoiceSettings;

        return $this;
    }

    /**
     * Get chamberOfCommerceNumber
     *
     * @return string
     */
    public function getChamberOfCommerceNumber()
    {
        if (null !== ($companyRegistration = $this->getCompanyRegistration())) {
            return $companyRegistration->getRegistrationNumber();
        }

        return $this->chamberOfCommerceNumber;
    }

    /**
     * Set chamberOfCommerceNumber
     *
     * @param string $chamberOfCommerceNumber
     *
     * @return Company
     */
    public function setChamberOfCommerceNumber($chamberOfCommerceNumber)
    {
        $this->chamberOfCommerceNumber = $chamberOfCommerceNumber;

        return $this;
    }

    /**
     * Get vatNumber
     *
     * @return string
     */
    public function getVatNumber()
    {
        if (null !== ($companyRegistration = $this->getCompanyRegistration())) {
            return $companyRegistration->getVatNumber();
        }

        return $this->vatNumber;
    }

    /**
     * Set oinNumber
     *
     * @param string|null $oinNumber
     *
     * @return Company
     */
    public function setOinNumber(?string $oinNumber): self
    {
        $this->oinNumber = $oinNumber;

        return $this;
    }

    /**
     * Get oinNumber
     *
     * @return string
     */
    public function getOinNumber(): string
    {
        return $this->oinNumber ?? '';
    }

    /**
     * Set vatNumber
     *
     * @param string $vatNumber
     *
     * @return Company
     */
    public function setVatNumber($vatNumber)
    {
        $this->vatNumber = $vatNumber;

        return $this;
    }

    /**
     * Get trustStatus
     *
     * @return CompanyTrustStatusType
     */
    public function getTrustStatus()
    {
        return $this->trustStatus;
    }

    /**
     * Set trustStatus
     *
     * @param string $trustStatus
     *
     * @return Company
     */
    public function setTrustStatus(string $trustStatus)
    {
        $this->trustStatus = $trustStatus;

        return $this;
    }

    /**
     * Get parent
     *
     * @return Company
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set parent
     *
     * @param Company $parent
     *
     * @return Company
     */
    public function setParent(Company $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Add child
     *
     * @param Company $child
     *
     * @return Company
     */
    public function addChild(Company $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param Company $child
     */
    public function removeChild(Company $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Remove customer
     *
     * @param Customer $customer
     */
    public function removeCustomer(Customer $customer)
    {
        $this->customers->removeElement($customer);
    }

    /**
     * Get users (alias customers)
     */
    public function getUsers()
    {
        return $this->getCustomers();
    }

    /**
     * Get customers
     *
     * @return ArrayCollection|Customer[]
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * Add customers
     *
     * @param ArrayCollection $customers
     *
     * @return Company
     *
     */
    public function setCustomers(ArrayCollection $customers)
    {
        $this->customers = new ArrayCollection();

        foreach ($customers as $customer) {
            $this->addCustomer($customer);
        }

        return $this;
    }

    /**
     * Add customer
     *
     * @param Customer $customer
     *
     * @return Company
     */
    public function addCustomer(Customer $customer)
    {
        $customer->setCompany($this);

        $this->customers[] = $customer;

        return $this;
    }

    /**
     * Remove address
     *
     * @param Address $address
     */
    public function removeAddress(Address $address)
    {
        $this->addresses->removeElement($address);
    }

    /**
     * @param Address $invoiceAddress
     * @return Company
     */
    public function setInvoiceAddress(Address $invoiceAddress)
    {
        $invoiceAddress->setCompany($this);

        $this->invoiceAddress = $invoiceAddress;

        return $this;
    }

    /**
     * Get delivery addresses
     *
     * @return Address|null
     */
    public function getInvoiceAddress()
    {
        return $this->invoiceAddress;
    }

    /**
     * @param $type
     *
     * @return Collection|Address[]
     */
    private function filterAddresses($type)
    {
        if (null !== ($addresses = $this->getAddresses())) {
            return $addresses->filter(function (Address $address) use ($type) {
                switch ($type) {
                    case static::ADDRESS_TYPE_MAIN:
                        return $address->isMainAddress();
                    case static::ADDRESS_TYPE_INVOICE:
                        return $address->isInvoiceAddress();
                    case static::ADDRESS_TYPE_DELIVERY:
                        return $address->isDeliveryAddress();
                }

                return null;
            });
        }

        return null;
    }

    /**
     * Get addresses
     *
     * @return Collection|Address[]
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * Add addresses
     *
     * @param ArrayCollection $addresses
     *
     * @return Company
     * @internal param Address $address []
     *
     */
    public function setAddresses(ArrayCollection $addresses)
    {
        $this->addresses = new ArrayCollection();

        foreach ($addresses as $address) {
            $this->addAddress($address);
        }

        return $this;
    }

    /**
     * Add address
     *
     * @param Address $address
     *
     * @return Company
     */
    public function addAddress(Address $address)
    {
        $address->setCompany($this);

        $this->addresses[] = $address;

        return $this;
    }

    /**
     * Get delivery addresses
     *
     * @return Collection
     */
    public function getDeliveryAddresses()
    {
        return $this->filterAddresses(static::ADDRESS_TYPE_DELIVERY);
    }

    /**
     * Get defaultDeliveryAddress
     *
     * @return Address
     */
    public function getDefaultDeliveryAddress()
    {
        return $this->default_delivery_address;
    }

    /**
     * Set defaultDeliveryAddress
     *
     * @param Address $defaultDeliveryAddress
     *
     * @return Company
     */
    public function setDefaultDeliveryAddress(Address $defaultDeliveryAddress = null)
    {
        $this->default_delivery_address = $defaultDeliveryAddress;

        return $this;
    }

    /**
     * Add order
     *
     * @param OrderCollection $orderCollection
     *
     * @return Company
     */
    public function addOrder(OrderCollection $orderCollection)
    {
        $this->orders[] = $orderCollection;

        return $this;
    }

    /**
     * Remove order
     *
     * @param OrderCollection $orderCollection
     */
    public function removeOrder(OrderCollection $orderCollection)
    {
        $this->orders->removeElement($orderCollection);
    }

    /**
     * Get orders
     *
     * @return Collection|OrderCollection[]
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Add companyDomain
     *
     * @param CompanyDomain $companyDomain
     *
     * @return Company
     */
    public function addCompanyDomain(CompanyDomain $companyDomain)
    {
        $companyDomain->setCompany($this);

        $this->companyDomains[] = $companyDomain;

        return $this;
    }

    /**
     * Remove companyDomain
     *
     * @param CompanyDomain $companyDomain
     */
    public function removeCompanyDomain(CompanyDomain $companyDomain)
    {
        $this->companyDomains->removeElement($companyDomain);
    }

    /**
     * Get companyDomains
     *
     * @return Collection
     */
    public function getCompanyDomains()
    {
        return $this->companyDomains;
    }

    /**
     * Get verified
     *
     * @return boolean
     */
    public function getVerified()
    {
        return $this->verified;
    }

    /**
     * Set verified
     *
     * @param boolean $verified
     *
     * @return Company
     */
    public function setVerified($verified)
    {
        $this->verified = $verified;

        return $this;
    }


    /**
     * Set twinfieldRelationNumber
     *
     * @param string $twinfieldRelationNumber
     *
     * @return Company
     */
    public function setTwinfieldRelationNumber($twinfieldRelationNumber)
    {
        $this->twinfieldRelationNumber = $twinfieldRelationNumber;

        return $this;
    }

    /**
     * Get twinfieldRelationNumber
     *
     * @return string
     */
    public function getTwinfieldRelationNumber()
    {
        if (!$this->twinfieldRelationNumber) {
            return 'TG' . str_pad($this->getId(), 7, '0', STR_PAD_LEFT);
        }

        return $this->twinfieldRelationNumber;
    }

    /**
     * Get exactRelationNumber
     *
     * @return string
     */
    public function getExactRelationNumber()
    {
        $twinfieldRelationNumber = $this->getTwinfieldRelationNumber();
        $search =  ['BD', 'BE', 'BF', 'BX',  'D', 'DC', 'FB', 'FR', 'NL', 'TG'];
        $replace = ['10', '11', '12', '13', '14', '15', '16', '17', '18', '20'];
        return str_replace($search, $replace, $twinfieldRelationNumber);
    }

    /**
     * Get internalRemark
     *
     * @return string
     */
    public function getInternalRemark()
    {
        return $this->internal_remark;
    }

    /**
     * Set internalRemark
     *
     * @param string $internalRemark
     *
     * @return Company
     */
    public function setInternalRemark($internalRemark)
    {
        $this->internal_remark = $internalRemark;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHandleChildOrders()
    {
        return $this->handle_child_orders;
    }

    /**
     * @param $handleChildOrders
     * @return $this
     */
    public function setHandleChildOrders($handleChildOrders)
    {
        $this->handle_child_orders = $handleChildOrders;

        return $this;
    }

    /**
     * @return null|bool
     */
    public function getBillOnParent()
    {
        return $this->bill_on_parent;
    }

    /**
     * @param $billOnParent
     * @return $this
     */
    public function setBillOnParent($billOnParent)
    {
        $this->bill_on_parent = $billOnParent;

        return $this;
    }

    /**
     * Add customerGroup
     *
     * @param CustomerGroup $customerGroup
     *
     * @return Company
     */
    public function addCustomerGroup(CustomerGroup $customerGroup)
    {
        $this->customerGroups[] = $customerGroup;

        return $this;
    }

    /**
     * Remove customerGroup
     *
     * @param CustomerGroup $customerGroup
     */
    public function removeCustomerGroup(CustomerGroup $customerGroup)
    {
        $this->customerGroups->removeElement($customerGroup);
    }

    /**
     * Get customerGroups
     *
     * @return Collection|CustomerGroup[]
     */
    public function getCustomerGroups()
    {
        return $this->customerGroups;
    }

    /**
     * Add supplierGroup
     *
     * @param SupplierGroup $supplierGroup
     *
     * @return Company
     */
    public function addSupplierGroup(SupplierGroup $supplierGroup)
    {
        if(!$this->supplierGroups->contains($supplierGroup)) {
            $this->supplierGroups->add($supplierGroup);
        }

        return $this;
    }

    /**
     * Remove supplierGroup
     *
     * @param SupplierGroup $supplierGroup
     */
    public function removeSupplierGroup(SupplierGroup $supplierGroup)
    {
        if($this->supplierGroups->contains($supplierGroup)) {
            $this->supplierGroups->removeElement($supplierGroup);
        }
    }

    /**
     * Get supplierGroups
     *
     * @return ArrayCollection|SupplierGroup[]
     */
    public function getSupplierGroups()
    {
        return $this->supplierGroups;
    }

    /**
     * Get isCustomer
     *
     * @return boolean
     */
    public function getIsCustomer()
    {
        return $this->isCustomer;
    }

    /**
     * Set isCustomer
     *
     * @param boolean $isCustomer
     *
     * @return Company
     */
    public function setIsCustomer($isCustomer)
    {
        $this->isCustomer = $isCustomer;

        return $this;
    }

    /**
     * Get isSupplier
     *
     * @return boolean
     */
    public function getIsSupplier()
    {
        return $this->isSupplier;
    }

    /**
     * Set isSupplier
     *
     * @param boolean $isSupplier
     *
     * @return Company
     */
    public function setIsSupplier($isSupplier)
    {
        $this->isSupplier = $isSupplier;

        return $this;
    }

    /**
     * Add supplierProduct
     *
     * @param SupplierProduct $supplierProduct
     *
     * @return Company
     */
    public function addSupplierProduct(SupplierProduct $supplierProduct)
    {
        $this->supplierProducts[] = $supplierProduct;

        return $this;
    }

    /**
     * Remove supplierProduct
     *
     * @param SupplierProduct $supplierProduct
     */
    public function removeSupplierProduct(SupplierProduct $supplierProduct)
    {
        $this->supplierProducts->removeElement($supplierProduct);
    }

    /**
     * Get supplierProducts
     *
     * @return Collection
     */
    public function getSupplierProducts()
    {
        return $this->supplierProducts;
    }

    /**
     * Add deliveryArea
     *
     * @param DeliveryArea $deliveryArea
     *
     * @return Company
     */
    public function addDeliveryArea(DeliveryArea $deliveryArea)
    {

        $deliveryArea->setCompany($this);

        $this->deliveryAreas[] = $deliveryArea;

        return $this;
    }

    /**
     * Remove deliveryArea
     *
     * @param DeliveryArea $deliveryArea
     */
    public function removeDeliveryArea(DeliveryArea $deliveryArea)
    {
        $this->deliveryAreas->removeElement($deliveryArea);
    }

    /**
     * Get deliveryAreas
     *
     * @return DeliveryArea[]|Collection
     */
    public function getDeliveryAreas()
    {
        return $this->deliveryAreas;
    }

    /**
     * Get supplierConnector
     *
     * @return string
     */
    public function getSupplierConnector()
    {
        return $this->supplierConnector;
    }

    /**
     * Set supplierConnector
     *
     * @param string $supplierConnector
     *
     * @return Company
     */
    public function setSupplierConnector($supplierConnector)
    {
        $this->supplierConnector = $supplierConnector;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set metadata
     *
     * @param array $metadata
     *
     * @return Company
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Company
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * @param int
     *
     * @return null|string
     */
    public function getFormattedPhoneNumber($format = PhoneNumberFormat::INTERNATIONAL)
    {
        if (!$this->phoneNumber) {
            return null;
        }

        try {
            if (null !== ($mainAddress = $this->getMainAddress())) {
                $countryCode = $mainAddress->getCountry()->getCode();
            } else {
                $countryCode = 'NL';
            }

            $phoneNumber = PhoneNumberUtil::getInstance()->parse($this->phoneNumber, $countryCode);

            return PhoneNumberUtil::getInstance()->format($phoneNumber, $format);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Get delivery addresses
     *
     * @return Address|null
     */
    public function getMainAddress()
    {
        $establisments = $this->establishments->filter(function ($establishment) {
            /** @var CompanyEstablishment $establishment */
            return $establishment->isMain();
        });

        if ($establisments->isEmpty() === false) {
            return $establisments->first()->getAddress();
        }


        // Fallback functionality

        $addresses = $this->filterAddresses(static::ADDRESS_TYPE_MAIN);

        if ($addresses->isEmpty()) {
            return null;
        }

        $addresses = iterator_to_array($addresses);

        $address = reset($addresses);

        if (!$address) {
            return null;
        }

        return $address;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Company
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get chamberOfCommerceEstablishmentNumber
     *
     * @return string
     */
    public function getChamberOfCommerceEstablishmentNumber()
    {
        if (null !== ($companyRegistration = $this->getCompanyRegistration())) {
            return $companyRegistration->getEstablishmentNumber();
        }

        return $this->chamberOfCommerceEstablishmentNumber;
    }

    /**
     * Set invoice email
     *
     * @param string $invoiceEmail
     *
     * @return Company
     */
    public function setInvoiceEmail($invoiceEmail)
    {
        $this->invoiceEmail = $invoiceEmail;

        return $this;
    }

    /**
     * Get invoice email
     *
     * @return string
     */
    public function getInvoiceEmail()
    {
        return $this->invoiceEmail;
    }

    /**
     * Set chamberOfCommerceEstablishmentNumber
     *
     * @param string $chamberOfCommerceEstablishmentNumber
     *
     * @return Company
     */
    public function setChamberOfCommerceEstablishmentNumber($chamberOfCommerceEstablishmentNumber)
    {
        $this->chamberOfCommerceEstablishmentNumber = $chamberOfCommerceEstablishmentNumber;

        return $this;
    }

    /**
     * @return ConnectorInterface
     * @deprecated
     */
    public function getConnector()
    {
        trigger_error('Company::getConnector has been replaced by the AppBundle\Services\ConnectorHelper service.',
            E_USER_DEPRECATED);

        global $kernel;

        return $kernel->getContainer()->get(ConnectorHelper::class)->getConnector($this);
    }

    /**
     * Add supplier commission
     *
     * @param CompanyCommission $commission
     *
     * @return Company
     */
    public function addSupplierCommission(CompanyCommission $commission)
    {
        $commission->setCompany($this);

        $this->supplierCommissions[] = $commission;

        return $this;
    }

    /**
     * Remove supplier commission
     *
     * @param CompanyCommission $commission
     */
    public function removeSupplierCommission(CompanyCommission $commission)
    {
        $this->supplierCommissions->removeElement($commission);
    }

    /**
     * Get supplier commissions
     *
     * @return Collection|CompanyCommission[]
     */
    public function getSupplierCommissions()
    {
        return $this->supplierCommissions;
    }

    /**
     * Get iban
     *
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Set iban
     *
     * @param string $iban
     *
     * @return Company
     */
    public function setIban($iban)
    {
        $this->iban = strtoupper($iban);

        return $this;
    }

    /**
     * Get bic
     *
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Set bic
     *
     * @param string $bic
     *
     * @return Company
     */
    public function setBic($bic)
    {
        $this->bic = $bic;

        return $this;
    }

    /**
     * Get bankAccountHolder
     *
     * @return string
     */
    public function getBankAccountHolder()
    {
        return $this->bankAccountHolder;
    }

    /**
     * Set bankAccountHolder
     *
     * @param string $bankAccountHolder
     *
     * @return Company
     */
    public function setBankAccountHolder($bankAccountHolder)
    {
        $this->bankAccountHolder = $bankAccountHolder;

        return $this;
    }

    /**
     * Add assortment
     *
     * @param Assortment $assortment
     *
     * @return Company
     */
    public function addAssortment(Assortment $assortment)
    {
        $this->assortments[] = $assortment;

        return $this;
    }

    /**
     * Remove assortment
     *
     * @param Assortment $assortment
     */
    public function removeAssortment(Assortment $assortment)
    {
        $this->assortments->removeElement($assortment);
    }

    /**
     * Get assortments
     *
     * @return Collection|Assortment[]
     */
    public function getAssortments()
    {
        return $this->assortments;
    }

    /**
     * Add companyInternalRemark
     *
     * @param CompanyInternalRemark $companyInternalRemark
     *
     * @return Company
     */
    public function addCompanyInternalRemark(CompanyInternalRemark $companyInternalRemark)
    {
        $this->companyInternalRemarks[] = $companyInternalRemark;

        return $this;
    }

    /**
     * Remove companyInternalRemark
     *
     * @param CompanyInternalRemark $companyInternalRemark
     */
    public function removeCompanyInternalRemark(CompanyInternalRemark $companyInternalRemark)
    {
        $this->companyInternalRemarks->removeElement($companyInternalRemark);
    }

    /**
     * Get companyInternalRemarks
     *
     * @return Collection|CompanyInternalRemark[]
     */
    public function getCompanyInternalRemarks()
    {
        return $this->companyInternalRemarks;

    }

    /**
     * Add establishment
     *
     * @param CompanyEstablishment $establishment
     *
     * @return Company
     */
    public function addEstablishment(CompanyEstablishment $establishment)
    {
        $this->establishments[] = $establishment;

        return $this;
    }

    /**
     * Remove establishment
     *
     * @param CompanyEstablishment $establishment
     */
    public function removeEstablishment(CompanyEstablishment $establishment)
    {
        $this->establishments->removeElement($establishment);
    }

    /**
     * @return CompanyEstablishment[]|ArrayCollection
     */
    public function getEstablishments()
    {
        return $this->establishments;
    }

    /**
     * @param ArrayCollection $discounts
     * @return $this
     */
    public function setDiscounts(ArrayCollection $discounts)
    {
        foreach ($discounts as $discount) {
            $this->addDiscount($discount);
        }
        return $this;
    }

    /**
     * @param Discount $discount
     * @return $this
     */
    public function addDiscount(Discount $discount)
    {
        $this->discounts[] = $discount;
        return $this;
    }

    /**
     * @return Discount[]
     */
    public function getDiscounts()
    {
        return $this->discounts;
    }

    /**
     * @param Discount $discount
     */
    public function removeDiscount(Discount $discount)
    {
        $this->discounts->removeElement($discount);
    }

    /**
     * Add report
     *
     * @param CompanyReport $report
     *
     * @return Company
     */
    public function addReport(CompanyReport $report)
    {
        $this->reports[] = $report;

        return $this;
    }

    /**
     * Remove report
     *
     * @param CompanyReport $report
     */
    public function removeReport(CompanyReport $report)
    {
        $this->reports->removeElement($report);
    }

    /**
     * Get reports
     *
     * @return Collection
     */
    public function getReports()
    {
        return $this->reports;
    }

    /**
     * Add rule
     *
     * @param CompanyRule $rule
     *
     * @return Company
     */
    public function addRule(CompanyRule $rule)
    {
        $this->rules[] = $rule;

        return $this;
    }

    /**
     * Remove rule
     *
     * @param CompanyRule $rule
     */
    public function removeRule(CompanyRule $rule)
    {
        $this->rules->removeElement($rule);
    }

    /**
     * Get rules
     *
     * @return Collection
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * @deprecated set via CompanyInvoiceSettings instead
     * Set paymentTerm
     *
     * @param integer $paymentTerm
     *
     * @return Company
     */
    public function setPaymentTerm($paymentTerm)
    {
        $this->paymentTerm = $paymentTerm;

        return $this;
    }

    /**
     * @deprecated retrieve via CompanyInvoiceSettings instead
     * Get paymentTerm
     *
     * @return integer
     */
    public function getPaymentTerm()
    {
        return $this->paymentTerm;
    }

    /**
     * Set creditLimit
     *
     * @param float|null $creditLimit
     * @return Company
     */
    public function setCreditLimit(?float $creditLimit)
    {
        $this->creditLimit = $creditLimit;

        return $this;
    }

    /**
     * Get creditLimit
     *
     * @return float|null
     */
    public function getCreditLimit()
    {
        return $this->creditLimit;
    }

    /**
     * Add customField
     *
     * @param CompanyCustomField $customField
     *
     * @return Company
     */
    public function addCustomField(CompanyCustomField $customField)
    {
        $customField->setCompanySite($this);

        $this->customFields[] = $customField;

        return $this;
    }

    /**
     * Remove customField
     *
     * @param CompanyCustomField $customField
     */
    public function removeCustomField(CompanyCustomField $customField)
    {
        $this->customFields->removeElement($customField);
    }

    /**
     * Get customFields
     *
     * @return Collection|CompanyCustomField[]
     */
    public function getCustomFields(): Collection
    {
        return $this->customFields;
    }

    /**
     * @param $priceDisplayMode
     * @return $this
     */
    public function setPriceDisplayMode($priceDisplayMode)
    {
        $this->priceDisplayMode = $priceDisplayMode;

        return $this;
    }

    /**
     * @return string
     */
    public function getPriceDisplayMode()
    {
        return $this->priceDisplayMode;
    }

    /**
     * @return array
     */
    public function getAuditSettings(): array
    {
        return [
            'mapping' => [
                'name' => 'Bedrijfsnaam',
                'phoneNumber' => 'Telefoonnummer',
                'email' => 'E-mailadres',
                'chamber_of_commerce_number' => 'KVK nummer',
                'vat_number' => 'BTW nummer',
                'deletedAt' => 'Verwijderd op',
                'createdAt' => 'Aaangemaakt op',
                'updatedAt' => 'Geupdate op',
            ],
            'label' => function (Company $company) {
                return $company->getName();
            },
            'title' => function (Company $company) {
                return $company->getName();
            },
            'route' => function (Company $company) {
                return [
                    'route' => 'admin_customer_company_view',
                    'routeParameters' => [
                        'id' => $company->getId(),
                    ],
                ];
            },
        ];
    }

    /**
     * Add site
     *
     * @param Site $site
     *
     * @return Company
     */
    public function addSite(Site $site)
    {
        $this->sites[] = $site;

        return $this;
    }

    /**
     * Remove site
     *
     * @param Site $site
     */
    public function removeSite(Site $site)
    {
        $this->sites->removeElement($site);
    }

    /**
     * Get sites
     *
     * @return Collection
     */
    public function getSites()
    {
        return $this->sites;
    }

    /**
     * Set menu
     *
     * @param Menu $menu
     *
     * @return Company
     */
    public function setMenu(Menu $menu = null)
    {
        $this->menu = $menu;

        return $this;
    }

    /**
     * Get menu
     *
     * @return Menu
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * Add product
     *
     * @param CompanyProduct $product
     *
     * @return Company
     */
    public function addProduct(CompanyProduct $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param CompanyProduct $product
     */
    public function removeProduct(CompanyProduct $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return Collection
     */
    public function getProducts()
    {
        return $this->products->filter(function (Product $product) {
            return null === $product->getParent();
        });
    }

    /**
     * Add product
     *
     * @param ProductCard $card
     *
     * @return Company
     */
    public function addCard(ProductCard $card)
    {
        $this->cards[] = $card;

        return $this;
    }

    /**
     * Remove product
     *
     * @param ProductCard $card
     */
    public function removeCard(ProductCard $card)
    {
        $this->cards->removeElement($card);
    }

    /**
     * Get products
     *
     * @return Collection
     */
    public function getCards()
    {
        return $this->cards;
    }

    /**
     * @param string|null $companyProductSkuPrefix
     * @return Company
     */
    public function setCompanyProductSkuPrefix(?string $companyProductSkuPrefix): Company
    {
        $this->companyProductSkuPrefix = $companyProductSkuPrefix;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getCompanyProductSkuPrefix(): ?string
    {
        return $this->companyProductSkuPrefix;
    }

    /**
     * @return CompanyTransportType[]|Collection
     */
    public function getTransportTypes()
    {
        return $this->transportTypes;
    }

    /**
     * @param CompanyTransportType[]|Collection $transportTypes
     */
    public function setTransportTypes($transportTypes): void
    {
        $this->transportTypes = $transportTypes;
    }

    /**
     * @param CompanyTransportType $transportType
     *
     * @return Company
     */
    public function addTransportType(CompanyTransportType $transportType)
    {
        if (!$this->transportTypes->contains($transportType)) {
            if ($this !== $transportType->getCompany()) {
                $transportType->setCompany($this);
            }
            $this->transportTypes[] = $transportType;
        }

        return $this;
    }

    /**
     * @param CompanyTransportType $transportType
     * @return Company
     */
    public function removeTransportType(CompanyTransportType $transportType)
    {
        if ($this->transportTypes->contains($transportType)) {
            $this->transportTypes->removeElement($transportType);
            $transportType->setCompany(null);
        }

        return $this;
    }

    /**
     * @return DesignerTemplate[]|Collection
     */
    public function getTemplates(): Collection
    {
        return $this->templates;
    }

    /**
     * @param DesignerTemplate[]|Collection $templates
     */
    public function setTemplates($templates): void
    {
        $this->templates = $templates;
    }

    /**
     * @param DesignerTemplate $template
     * @return Company
     */
    public function addTemplate(DesignerTemplate $template): Company
    {
        if(!$this->templates->contains($template)) {
            $this->templates->add($template);
        }

        return $this;
    }

    /**
     * @param DesignerTemplate $template
     * @return Company
     */
    public function removeTemplate(DesignerTemplate $template): Company
    {
        if($this->templates->contains($template)) {
            $this->templates->removeElement($template);
        }

        return $this;
    }

    /**
     * @param string|null $overrideDefaultPriceView
     * @return Company
     */
    public function setOverrideDefaultPriceView(?string $overrideDefaultPriceView): Company
    {
        $this->overrideDefaultPriceView = $overrideDefaultPriceView;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOverrideDefaultPriceView(): ?string
    {
        return $this->overrideDefaultPriceView;
    }

    /**
     * @return Paymentmethod[]|Collection
     */
    public function getPaymentmethods()
    {
        return $this->paymentmethods;
    }

    /**
     * @param Paymentmethod $paymentmethod
     *
     * @return $this
     */
    public function addPaymentmethod(Paymentmethod $paymentmethod): self
    {
        if(!$this->paymentmethods->contains($paymentmethod)) {
            $this->paymentmethods->add($paymentmethod);
        }

        return $this;
    }

    /**
     * @param Paymentmethod $paymentmethod
     *
     * @return $this
     */
    public function removePaymentmethod(Paymentmethod $paymentmethod): self
    {
        if($this->paymentmethods->contains($paymentmethod)) {
            $this->paymentmethods->removeElement($paymentmethod);
        }

        return $this;
    }
}
