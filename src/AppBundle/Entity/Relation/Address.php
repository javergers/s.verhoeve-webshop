<?php

namespace AppBundle\Entity\Relation;

use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Interfaces\GeocodableInterface;
use AppBundle\Traits\ArchivableEntity;
use AppBundle\Traits\GeocodableEntity;
use AppBundle\Traits\UuidEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity()
 * @ORM\EntityListeners({"AppBundle\EventListener\Relation\AddressListener"})
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Address implements GeocodableInterface
{
    use TimestampableEntity;
    use ArchivableEntity;
    use SoftDeleteableEntity;
    use GeocodableEntity;
    use UuidEntity;

    public const TYPE_MAIN = 'main';
    public const TYPE_INVOICE = 'invoice';
    public const TYPE_DELIVERY = 'delivery';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="addresses")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $company;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Customer\Customer", inversedBy="addresses")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $customer;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $companyName;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $attn;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $main_address;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $invoice_address;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $delivery_address;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="address.street.not_blank", groups={"company_registration", "customer_registration",
     *                                                      "cart_sender"})
     */
    protected $street;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank(message="address.number.not_blank", groups={"company_registration", "customer_registration",
     *                                                      "cart_sender"})
     */
    protected $number;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $numberAddition;

    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\NotBlank(message="address.postcode.not_blank", groups={"company_registration", "customer_registration",
     *                                                        "cart_sender"})
     * @Assert\Length(min="4", minMessage="address.postcode.min_length", groups={"company_registration",
     *                         "customer_registration", "cart_sender"})
     */
    protected $postcode;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="address.city.not_blank", groups={"company_registration", "customer_registration",
     *                                                    "cart_sender"})
     */
    protected $city;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\Country")
     * @ORM\JoinColumn(name="country", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank(message="address.country.not_blank", groups={"company_registration", "customer_registration",
     *                                                       "cart_sender"})
     */
    protected $country;

    /**
     * @ORM\Column(type="string", length=35, nullable=true)
     */
    protected $phoneNumber;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->format();
    }

    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @param string $separator
     * @return string
     */
    public function format($separator = ', ')
    {
        $number = $this->getNumber();
        $numberAddition = $this->getNumberAddition();

        if (!empty($numberAddition)) {
            $number .= ' ' . $numberAddition;
        }

        return $this->getStreet() . ' ' . $number . $separator . $this->getPostcode() . ' ' . $this->getCity() . $separator . $this->getCountry();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set street
     *
     * @param string $street
     *
     * @return Address
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Address
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set numberAddition
     *
     * @param string $numberAddition
     *
     * @return Address
     */
    public function setNumberAddition($numberAddition)
    {
        $this->numberAddition = $numberAddition;

        return $this;
    }

    /**
     * Get numberAddition
     *
     * @return string
     */
    public function getNumberAddition()
    {
        return $this->numberAddition;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return Address
     */
    public function setPostcode($postcode)
    {
        $this->postcode = str_replace(' ', '', $postcode);

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Address
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return Address
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     *
     * @return Address
     */
    public function setCustomer(Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set country
     *
     * @param Country $country
     *
     * @return Address
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Address
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return Address
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set attn
     *
     * @param string $attn
     *
     * @return Address
     */
    public function setAttn($attn)
    {
        $this->attn = $attn;

        return $this;
    }

    /**
     * Get attn
     *
     * @return string
     */
    public function getAttn()
    {
        return $this->attn;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Address
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set invoiceAddress
     *
     * @param boolean $invoiceAddress
     *
     * @return Address
     */
    public function setInvoiceAddress($invoiceAddress)
    {
        $this->invoice_address = $invoiceAddress;

        return $this;
    }

    /**
     * Get invoiceAddress
     *
     * @return boolean
     */
    public function getInvoiceAddress()
    {
        return $this->invoice_address;
    }

    /**
     *
     * @return boolean
     */
    public function isInvoiceAddress()
    {
        if (null !== $this->getCompany() && $this->getCompany()->getInvoiceAddress() === $this) {
            return true;
        }

        return $this->getInvoiceAddress();
    }

    /**
     * Set deliveryAddress
     *
     * @param boolean $deliveryAddress
     *
     * @return Address
     */
    public function setDeliveryAddress($deliveryAddress)
    {
        $this->delivery_address = $deliveryAddress;

        return $this;
    }

    /**
     * Get deliveryAddress
     *
     * @return boolean
     */
    public function getDeliveryAddress()
    {
        return $this->delivery_address;
    }

    /**
     *
     * @return boolean
     */
    public function isDeliveryAddress()
    {
        return $this->getDeliveryAddress();
    }

    /**
     * Set mainAddress
     *
     * @param boolean $mainAddress
     *
     * @return Address
     */
    public function setMainAddress($mainAddress)
    {
        $this->main_address = $mainAddress;

        return $this;
    }

    /**
     * Get mainAddress
     *
     * @return boolean
     */
    public function getMainAddress()
    {
        return $this->main_address;
    }

    /**
     *
     * @return boolean
     */
    public function isMainAddress()
    {
        return $this->getMainAddress();
    }

    /**
     *
     * @return boolean
     */
    public function isDefaultDeliveryAddress()
    {
        if (null !== ($company = $this->getCompany()) && null !== ($defaultDeliveryAddress = $company->getDefaultDeliveryAddress())) {
            return ($this->getId() === $defaultDeliveryAddress->getId());
        }

        if (null !== ($customer = $this->getCustomer()) && null !== ($defaultDeliveryAddress = $customer->getDefaultDeliveryAddress())) {
            return ($this->getId() === $this->getCustomer()->getDefaultDeliveryAddress()->getId());
        }

        return false;
    }

    /**
     *
     * @return boolean
     */
    public function isDefaultInvoiceAddress()
    {
        if (null !== ($customer = $this->getCustomer()) && null !== ($defaultInvoiceAddress = $customer->getDefaultInvoiceAddress())) {
            return ($this->getId() === $defaultInvoiceAddress->getId());
        }

        return false;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Address
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param int $format
     * @return null|string
     */
    public function getFormattedPhoneNumber($format = PhoneNumberFormat::INTERNATIONAL)
    {
        if (!$this->phoneNumber) {
            return null;
        }

        try {
            $countryCode = 'NL';
            $phoneNumber = PhoneNumberUtil::getInstance()->parse($this->phoneNumber, $countryCode);

            return PhoneNumberUtil::getInstance()->format($phoneNumber, $format);
        } catch (\Exception $e) {
            return null;
        }
    }
}
