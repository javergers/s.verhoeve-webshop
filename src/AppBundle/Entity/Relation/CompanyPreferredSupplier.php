<?php

namespace AppBundle\Entity\Relation;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Supplier\DeliveryArea;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"company_id", "delivery_area_id", "position"})
 * })
 */
class CompanyPreferredSupplier
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company")
     */
    protected $company;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Supplier\DeliveryArea", inversedBy="preferredSuppliers",
     *                                                                       cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    protected $deliveryArea;

    /**
     * @ORM\Column(type="integer")
     */
    protected $position;

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return CompanyPreferredSupplier
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return CompanyPreferredSupplier
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set deliveryArea
     *
     * @param DeliveryArea $deliveryArea
     *
     * @return CompanyPreferredSupplier
     */
    public function setDeliveryArea(DeliveryArea $deliveryArea = null)
    {
        $this->deliveryArea = $deliveryArea;

        return $this;
    }

    /**
     * Get deliveryArea
     *
     * @return DeliveryArea
     */
    public function getDeliveryArea()
    {
        return $this->deliveryArea;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
