<?php

namespace AppBundle\Entity\Catalog\Product;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ProductTransportType
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Product|null
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product", inversedBy="productTransportTypes")
     * @ORM\JoinColumn(nullable=true);
     */
    protected $product;

    /**
     * @var Productgroup|null
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Productgroup", inversedBy="productTransportTypes")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $productGroup;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product")
     * @ORM\JoinColumn(nullable=false);
     */
    protected $transportType;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $quantity;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product
     *
     * @param Product|null $product
     *
     * @return ProductTransportType
     */
    public function setProduct(?Product $product)
    {
        $this->product = $product;
        return $this;
    }

    /**
     * Get product
     *
     * @return Product|null
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set transportType
     *
     * @param Product $transportType
     *
     * @return ProductTransportType
     */
    public function setTransportType(Product $transportType)
    {
        $this->transportType = $transportType;
        return $this;
    }

    /**
     * Get transportType
     *
     * @return Product
     */
    public function getTransportType()
    {
        return $this->transportType;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return ProductTransportType
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return Productgroup|null
     */
    public function getProductGroup(): ?Productgroup
    {
        return $this->productGroup;
    }

    /**
     * @param Productgroup|null $productGroup
     *
     * @return ProductTransportType
     */
    public function setProductGroup(?Productgroup $productGroup): self
    {
        $this->productGroup = $productGroup;

        return $this;
    }
}
