<?php

namespace AppBundle\Entity\Catalog\Product;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Fee extends Product
{
    /**
     * @var boolean
     */
    protected $purchasable = false;
}
