<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Annotation\GeneralListener;
use AppBundle\DBAL\Types\ProductTypeType;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Assortment\AssortmentProduct;
use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Entity\Supplier\SupplierGroupProduct;
use AppBundle\Entity\Supplier\SupplierProduct;
use AppBundle\Interfaces\Audit\AuditEntityInterface;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Interfaces\MenuSourceInterface;
use AppBundle\Interfaces\Preview\PreviewableEntityInterface;
use AppBundle\Interfaces\Product\PriceInterface;
use AppBundle\Model\Product\ProductModel;
use AppBundle\ThirdParty\Google\Entity\Shopping\Taxonomy;
use AppBundle\Traits\AccessibleEntity;
use AppBundle\Traits\PublishableEntity;
use AppBundle\Traits\UuidEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OrderBy;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use RuleBundle\Annotation as Rule;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 * @ORM\EntityListeners({"AppBundle\EventListener\SkuListener", "AppBundle\EventListener\Catalog\Product\ProductListener",
 *                                                              "AppBundle\EventListener\Teamleader\ProductListener"})
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="entity_type", type="string")
 * @ORM\DiscriminatorMap({
 *     "product" = "AppBundle\Entity\Catalog\Product\GenericProduct",
 *     "product_card" = "AppBundle\Entity\Catalog\Product\ProductCard",
 *     "product_personalization" = "AppBundle\Entity\Catalog\Product\Personalization",
 *     "product_letter" = "AppBundle\Entity\Catalog\Product\Letter",
 *     "product_transport_type" = "AppBundle\Entity\Catalog\Product\TransportType",
 *     "product_fee" = "AppBundle\Entity\Catalog\Product\Fee",
 *     "product_company" = "AppBundle\Entity\Catalog\Product\CompanyProduct",
 *     "product_card_company" = "AppBundle\Entity\Catalog\Product\CompanyCard"
 * })
 * @ORM\AssociationOverrides({
 *      @ORM\AssociationOverride(name="accessibleCustomerGroups",
 *          joinTable=@ORM\JoinTable(
 *              name="product_accessible_customer_group"
 *          )
 *      )
 * })
 *
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 * @method ProductTranslation translate(string $locale = null, boolean $fallbackToDefault = null)
 * @GeneralListener(parent="Product", parentGetter="getParent", get="updatedAt", set="updatedAt")
 *
 * @UniqueEntity(fields={"ean"})
 *
 * @Rule\Entity(description="Product")
 */
abstract class Product extends ProductModel implements MenuSourceInterface, PreviewableEntityInterface, AuditEntityInterface, ProductInterface, PriceInterface
{
    use ORMBehaviors\Translatable\Translatable;

    use TimestampableEntity;
    use SoftDeleteableEntity;
    use PublishableEntity;
    use AccessibleEntity;
    use UuidEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product", inversedBy="variations")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     * @Rule\Property(description="Hoofdproduct", autoComplete={ "id"="product.id", "label"="product.translate.title"})
     */
    protected $parent;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $position;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\Product", mappedBy="parent", cascade={"persist",
     *                                                                         "remove"})
     * @Rule\Property(description="Variaties", autoComplete={ "id"="variation.id",
     *                                         "label"="variation.translate.title"}, relation="child"))
     * @OrderBy({"position" = "ASC", "price" = "ASC"})
     */
    protected $variations;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\ProductImage", mappedBy="product",
     *                                                                              cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $productImages;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\ProductTransportType", mappedBy="product",
     *                                                                                      cascade={"persist"})
     */
    protected $productTransportTypes;
    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\ProductTypeType")
     * @ORM\Column(type="ProductTypeType", nullable=true)
     *
     * @Rule\Property(description="Type")
     */
    protected $type;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\ProductCountry", mappedBy="product",
     *                                                                                cascade={"persist"})
     */
    protected $productCountries;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\ProductBulkPrice", mappedBy="product",
     *                                                                                  cascade={"persist"})
     */
    protected $bulkPrices;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Assortment\AssortmentProduct", mappedBy="product",
     *                                                                                      cascade={"persist"})
     * @Rule\Property(description="Assortiment producten", autoComplete={"id"="assortmentProduct.id",
     *                                         "label"="assortmentProduct.product.translate.title"}, relation="parent")
     */
    protected $assortmentProducts;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Productgroup", inversedBy="products")
     * @ORM\JoinColumn(nullable=true)
     * @Rule\Property(description="Productgroep", autoComplete={ "id"="productgroup.id", "label"="productgroup.title"
     *                                            }, relation="child")
     */
    protected $productgroup;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\ThirdParty\Google\Entity\Shopping\Taxonomy")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $googleShoppingTaxonomy;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Assortment\Assortment")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $cards;
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Security\Customer\CustomerGroup")
     */
    protected $customergroups;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\ProductProperty", mappedBy="product",
     *                                                                                 cascade={"persist"})
     */
    protected $productProperties;
    /**
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $template;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Supplier\SupplierProduct", mappedBy="product", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $supplierProducts;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Supplier\SupplierGroupProduct", mappedBy="product",
     *                                                                               cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $supplierGroupProducts;
    /**
     * @ORM\Id
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\ProductUsp", mappedBy="product",
     *                                                                            cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $usps;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     * @Rule\Property(description="Prijs")
     */
    private $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     */
    private $minPrice;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     */
    private $maxPrice;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     */
    private $stepMinMaxPrice;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     */
    private $purchasePrice;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     * @deprecated
     */
    private $forwardPrice;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Vat")
     *
     */
    private $vat;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true})
     *
     */
    private $defaultQuantity = 1;

    /**
     * @ORM\Column(type="smallint", options={"unsigned": true})
     *
     */
    private $minQuantity = 1;

    /**
     * @ORM\Column(type="smallint", nullable=true, options={"unsigned": true})
     *
     */
    private $maxQuantity;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Regex("|^\d+(?:\;\d+)*$|", message="Alleen numerieke waarden gescheiden door een ;")
     */
    private $quantitySteps = 1;

    /**
     * @ORM\Column(type="bigint", columnDefinition="BIGINT(13) UNSIGNED ZEROFILL DEFAULT NULL", length=13,
     *                            nullable=true, options={"unsigned": true}, unique=true)
     *
     */
    private $ean;

    /**
     * @ORM\Column(type="string", length=8, nullable=true, unique=true)
     * @Rule\Property(description="SKU")
     */
    private $sku;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     * @Rule\Property(description="Op vooraad")
     */
    private $hasStock;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    private $noStockBackorder;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    private $noStockQuotation;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\Combination", mappedBy="combinationProduct",
     *                                                                     cascade={"persist"})
     */
    protected $combinations;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\Combination", mappedBy="product")
     */
    protected $combinationProducts;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Catalog\Assortment\Assortment")
     * @ORM\JoinTable(
     *      name="product_additional_product_assortment",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="assortment_id", referencedColumnName="id")}
     * )
     */
    private $additionalProductAssortments;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     *
     */
    private $manufacturer;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\Length(
     *      min = 2,
     *      max = 100,
     *      minMessage = "Internal name must be at least {{ limit }} characters long",
     *      maxMessage = "Internal name cannot be longer than {{ limit }} characters"
     * )
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\ProductgroupPropertySet", inversedBy="products")
     */
    protected $propertySet;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\ProductPackagingUnit", mappedBy="product",
     *                                                                              cascade={"persist"})
     * @Assert\Count(min = 1, minMessage="Als voorraadbeheer aan staat moet er minstens een verpakkingsunit toegevoegd
     *                   worden", groups={"Stockable"})
     */
    protected $productPackagingUnits;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     *
     */
    protected $metadata;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $configurable;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":1})
     *
     */
    protected $showOptions;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\ExpirationMeta", cascade={"persist"})
     */
    protected $expirationMeta;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\SerialNumberMeta", cascade={"persist"})
     */
    protected $serialNumberMeta;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Letter")
     *
     */
    protected $letter;

    /**
     * @var CompanyProduct
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\CompanyProduct", mappedBy="relatedProduct")
     */
    protected $companyProducts;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $showContent;

    /**
     * @var VatGroup[]|Collection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Finance\Tax\VatGroup")
     * @Rule\Property(description="BTW Groep", autoComplete={"id"="vatGroup.id", "label"="vatGroup.level.description"},
     *                                 relation="child")
     */
    protected $vatGroups;

    /**
     * @var Upsell[]|Collection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\Upsell", mappedBy="product", cascade={"persist"})
     */
    protected $upsells;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productCountries = new ArrayCollection();
        $this->productProperties = new ArrayCollection();
        $this->bulkPrices = new ArrayCollection();
        $this->variations = new ArrayCollection();
        $this->supplierProducts = new ArrayCollection();
        $this->supplierGroupProducts = new ArrayCollection();
        $this->assortmentProducts = new ArrayCollection();
        $this->usps = new ArrayCollection();
        $this->productImages = new ArrayCollection();
        $this->customergroups = new ArrayCollection();
        $this->productTransportTypes = new ArrayCollection();
        $this->additionalProductAssortments = new ArrayCollection();
        $this->productPackagingUnits = new ArrayCollection();
        $this->combinationProducts = new ArrayCollection();
        $this->combinations = new ArrayCollection();
        $this->companyProducts = new ArrayCollection();
        $this->vatGroups = new ArrayCollection();
        $this->upsells = new ArrayCollection();
    }

    /**
     * @param FormInterface $form
     * @return array
     */
    public static function determineValidationGroups(FormInterface $form): array
    {
        $data = $form->getData();

        if ($data->getHasStock()) {
            return ['Default', 'Stockable'];
        }

        return ['Default'];
    }

    /**
     * @return array
     */
    public static function getMenuSourceConfig()
    {
        return [
            'labels' => [
                'description' => 'Producten',
                'singular' => 'Product',
            ],
        ];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->id;
    }

    /**
     * @return string
     */
    public function getEntityType()
    {
        return 'Product';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get Price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get minPrice
     *
     * @return float
     */
    public function getMinPrice()
    {
        return $this->minPrice;
    }

    /**
     * Set minPrice
     *
     * @param float $minPrice
     *
     * @return Product
     */
    public function setMinPrice($minPrice)
    {
        $this->minPrice = $minPrice;

        return $this;
    }

    /**
     * Get maxPrice
     *
     * @return float
     */
    public function getMaxPrice()
    {
        return $this->maxPrice;
    }

    /**
     * Set maxPrice
     *
     * @param float $maxPrice
     *
     * @return Product
     */
    public function setMaxPrice($maxPrice)
    {
        $this->maxPrice = $maxPrice;

        return $this;
    }

    /**
     * Get purchasePrice
     *
     * @return float
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * Set purchasePrice
     *
     * @param float $purchasePrice
     *
     * @return Product
     */
    public function setPurchasePrice($purchasePrice)
    {
        $this->purchasePrice = $purchasePrice;

        return $this;
    }

    /**
     * Get defaultQuantity
     *
     * @return integer
     */
    public function getDefaultQuantity()
    {
        return $this->defaultQuantity;
    }

    /**
     * Set defaultQuantity
     *
     * @param integer $defaultQuantity
     *
     * @return Product
     */
    public function setDefaultQuantity($defaultQuantity)
    {
        $this->defaultQuantity = $defaultQuantity;

        return $this;
    }

    /**
     * Get minQuantity
     *
     * @return integer
     */
    public function getMinQuantity()
    {
        return $this->minQuantity;
    }

    /**
     * Set minQuantity
     *
     * @param integer $minQuantity
     *
     * @return Product
     */
    public function setMinQuantity($minQuantity)
    {
        $this->minQuantity = $minQuantity;

        return $this;
    }

    /**
     * Get maxQuantity
     *
     * @return integer
     */
    public function getMaxQuantity()
    {
        return $this->maxQuantity;
    }

    /**
     * Set maxQuantity
     *
     * @param integer $maxQuantity
     *
     * @return Product
     */
    public function setMaxQuantity($maxQuantity)
    {
        $this->maxQuantity = $maxQuantity;

        return $this;
    }

    /**
     * Get ean
     *
     * @return float
     */
    public function getEan()
    {
        return $this->ean;
    }

    /**
     * Set ean
     *
     * @param float $ean
     *
     * @return Product
     */
    public function setEan($ean)
    {
        $this->ean = $ean;

        return $this;
    }

    /**
     * Get hasStock
     *
     * @return boolean
     */
    public function getHasStock()
    {
        return $this->hasStock;
    }

    /**
     * Set hasStock
     *
     * @param boolean $hasStock
     *
     * @return Product
     */
    public function setHasStock($hasStock)
    {
        $this->hasStock = $hasStock;

        return $this;
    }

    /**
     * Get sku
     *
     * @return null|string
     */
    public function getSku(): ?string
    {
        return $this->sku;
    }

    /**
     * Set sku
     *
     * @param string $sku
     *
     * @return Product
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Add productCountry
     *
     * @param ProductCountry $productCountry
     *
     * @return Product
     */
    public function addProductCountry(ProductCountry $productCountry)
    {
        $productCountry->setProduct($this);

        $this->productCountries[] = $productCountry;

        return $this;
    }

    /**
     * Remove productCountry
     *
     * @param ProductCountry $productCountry
     */
    public function removeProductCountry(ProductCountry $productCountry)
    {
        $this->productCountries->removeElement($productCountry);
    }

    /**
     * Get productCountries
     *
     * @return Collection
     */
    public function getProductCountries()
    {
        return $this->productCountries;
    }

    /**
     * Get productgroup
     *
     * @param bool $traverse
     *
     * @return null|Productgroup
     * @throws \RuntimeException
     */
    public function getProductgroup($traverse = false)
    {
        void($traverse);

        if ($this->getParent()) {
            $productGroup = $this->getParent()->getProductgroup();
        } else {
            $productGroup = $this->productgroup;
        }

        return $productGroup;
    }

    /**
     * Set productgroup
     *
     * @param Productgroup $productgroup
     *
     * @return Product
     */
    public function setProductgroup(?Productgroup $productgroup)
    {
        $this->productgroup = $productgroup;

        return $this;
    }

    /**
     * Get parent
     *
     * @return null|Product
     */
    public function getParent(): ?Product
    {
        return $this->parent;
    }

    /**
     * Set parent
     *
     * @param Product $parent
     *
     * @return Product
     */
    public function setParent(Product $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Add bulkPrice
     *
     * @param ProductBulkPrice $bulkPrice
     *
     * @return Product
     */
    public function addBulkPrice(ProductBulkPrice $bulkPrice)
    {
        $bulkPrice->setProduct($this);

        $this->bulkPrices[] = $bulkPrice;

        return $this;
    }

    /**
     * Remove bulkPrice
     *
     * @param ProductBulkPrice $bulkPrice
     */
    public function removeBulkPrice(ProductBulkPrice $bulkPrice)
    {
        $this->bulkPrices->removeElement($bulkPrice);
    }

    /**
     * Get bulkPrices
     *
     * @return Collection
     */
    public function getBulkPrices()
    {
        return $this->bulkPrices;
    }

    /**
     * Add variation
     *
     * @param Product $variation
     *
     * @return Product
     */
    public function addVariation(Product $variation)
    {
        $variation->setParent($this);

        $this->variations[] = $variation;

        return $this;
    }

    /**
     * Remove variation
     *
     * @param Product $variation
     */
    public function removeVariation(Product $variation)
    {
        $this->variations->removeElement($variation);
    }

    /**
     * Get variations
     *
     * @param bool $showUnpublished
     *
     * @return Collection
     */
    public function getVariations()
    {
        return $this->variations;
    }

    /**
     * Add productProperty
     *
     * @param ProductProperty $productProperty
     *
     * @return Product
     */
    public function addProductProperty(ProductProperty $productProperty)
    {
        $this->productProperties[] = $productProperty;

        return $this;
    }

    /**
     * Remove productProperty
     *
     * @param ProductProperty $productProperty
     */
    public function removeProductProperty(ProductProperty $productProperty)
    {
        $this->productProperties->removeElement($productProperty);
    }

    /**
     * Get productProperties
     *
     * @return Collection
     */
    public function getProductProperties()
    {
        return $this->productProperties;
    }

    /**
     * @param ProductgroupProperty $productgroupProperty
     *
     * @return ProductProperty
     */
    public function getProductProperty(ProductgroupProperty $productgroupProperty)
    {
        foreach ($this->productProperties as $productProperty) {
            if ($productgroupProperty === $productProperty->getProductgroupProperty()) {
                return $productProperty;
            }
        }

        $productProperty = new ProductProperty();
        $productProperty->setProduct($this);
        $productProperty->setProductgroupProperty($productgroupProperty);

        return $productProperty;
    }

    /**
     * Get manufacturer
     *
     * @return string
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Set manufacturer
     *
     * @param string $manufacturer
     *
     * @return Product
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getInternalName()
    {
        $name = $this->name;

        if ($this->isDeleted()) {
            $name .= ' => Verwijderd!';
        } else {
            if (!$this->isPublished()) {
                $name .= ' => Niet gepubliceerd!';
            }
        }

        return $name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Add customergroup
     *
     * @param CustomerGroup $customergroup
     *
     * @return Product
     */
    public function addCustomergroup(CustomerGroup $customergroup)
    {
        if (!$this->customergroups->contains($customergroup)) {
            $this->customergroups[] = $customergroup;
        }

        return $this;
    }

    /**
     * Remove customergroup
     *
     * @param CustomerGroup $customergroup
     */
    public function removeCustomergroup(CustomerGroup $customergroup)
    {
        $this->customergroups->removeElement($customergroup);
    }

    /**
     * Get customergroups
     *
     * @return Collection
     */
    public function getCustomergroups()
    {
        return $this->customergroups;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
//        if ($this->parent && !$this->translate()->getTitle()) {
//            $name = [];
//
//            foreach ($this->getProductProperties() as $productProperty) {
//                if ($productProperty->getProductgroupProperty()->getConfigurable() && $productProperty->getProductgroupPropertyOption()) {
//                    array_push($name, $productProperty->getProductgroupPropertyOption()->getValue());
//                } else {
//                    array_push($name, $productProperty->getValue());
//                }
//            }
//
//            return implode(" ", $name);
//        }

        return $this->translate()->getTitle();
    }

    /**
     * Add productImage
     *
     * @param ProductImage $productImage
     *
     * @return Product
     */
    public function addProductImage(ProductImage $productImage)
    {
        $productImage->setProduct($this);

        $this->productImages[] = $productImage;

        return $this;
    }

    /**
     * Remove productImage
     *
     * @param ProductImage $productImage
     */
    public function removeProductImage(ProductImage $productImage)
    {
        $this->productImages->removeElement($productImage);
    }

    /**
     * Get productImages
     *
     * @return ArrayCollection
     */
    public function getProductImages()
    {
        return $this->productImages;
    }

    /**
     * Get type
     *
     * @return ProductTypeType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getMenuType(): string
    {
        return 'Product';
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Product
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set template
     *
     * @param string $template
     *
     * @return Product
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get vat
     *
     * @return Vat
     */
    public function getVat()
    {
        throw new \Exception('Hier nog een ref');

        if (null === $this->vat && $this->getParent()) {
            return $this->getParent()->getVat();
        }

        return $this->vat;
    }

    /**
     * Set vat
     *
     * @param Vat $vat
     *
     * @return Product
     */
    public function setVat(Vat $vat = null)
    {
        $this->vat = $vat;

        return $this;
    }

    /**
     * Add supplierProduct
     *
     * @param SupplierProduct $supplierProduct
     *
     * @return Product
     */
    public function addSupplierProduct(SupplierProduct $supplierProduct)
    {
        $supplierProduct->setProduct($this);

        $this->supplierProducts[] = $supplierProduct;

        return $this;
    }

    /**
     * Remove supplierProduct
     *
     * @param SupplierProduct $supplierProduct
     */
    public function removeSupplierProduct(SupplierProduct $supplierProduct)
    {
        $this->supplierProducts->removeElement($supplierProduct);
    }

    /**
     * Get supplierProducts
     *
     * @return Collection|SupplierProduct[]
     */
    public function getSupplierProducts()
    {
        return $this->supplierProducts;
    }

    /**
     * Add usp
     *
     * @param ProductUsp $usp
     *
     * @return Product
     */
    public function addUsp(ProductUsp $usp)
    {
        $usp->setProduct($this);

        $this->usps[] = $usp;

        return $this;
    }

    /**
     * Remove usp
     *
     * @param ProductUsp $usp
     */
    public function removeUsp(ProductUsp $usp)
    {
        $this->usps->removeElement($usp);
    }

    /**
     * Get usps
     *
     * @return Collection
     */
    public function getUsps()
    {
        return $this->usps;
    }

    /**
     * Get cards
     *
     * @return Assortment
     */
    public function getCards()
    {
        if (null !== $this->cards) {
            return $this->cards;
        }

        return $this->getParent() ? $this->getParent()->getCards() : $this->cards;
    }

    /**
     * Set cards
     *
     * @param Assortment $cards
     *
     * @return Product
     */
    public function setCards(Assortment $cards = null)
    {
        $this->cards = $cards;

        return $this;
    }

    /**
     * @return bool
     */
    public function isProductCardAvailable()
    {
        return (bool)$this->getCards();
    }

    /**
     * Add assortmentProduct
     *
     * @param AssortmentProduct $assortmentProduct
     *
     * @return Product
     */
    public function addAssortmentProduct(AssortmentProduct $assortmentProduct)
    {
        $this->assortmentProducts[] = $assortmentProduct;

        return $this;
    }

    /**
     * Remove assortmentProduct
     *
     * @param AssortmentProduct $assortmentProduct
     */
    public function removeAssortmentProduct(AssortmentProduct $assortmentProduct)
    {
        $this->assortmentProducts->removeElement($assortmentProduct);
    }

    /**
     * Get assortments
     *
     * @return Collection|Assortment[]
     */
    public function getAssortments()
    {
        $assortments = new ArrayCollection();

        foreach ($this->getAssortmentProducts() as $assortmentProduct) {
            try {
                if (method_exists(!$assortmentProduct->getAssortment(),
                        'isPublished') && !$assortmentProduct->getAssortment()->isPublished()) {
                    continue;
                }

                $assortments[] = $assortmentProduct->getAssortment();
            } catch (EntityNotFoundException $e) {
                continue;
            }
        }

        return $assortments;
    }

    /**
     * Get assortmentProducts
     *
     * @return Collection
     */
    public function getAssortmentProducts()
    {
        return $this->assortmentProducts;
    }

    /**
     * @return string
     */
    public function getMenuTitle()
    {
        return $this->translate()->getTitle();
    }

    /**
     * Get stepMinMaxPrice
     *
     * @return float
     */
    public function getStepMinMaxPrice()
    {
        return $this->stepMinMaxPrice;
    }

    /**
     * Set stepMinMaxPrice
     *
     * @param float $stepMinMaxPrice
     *
     * @return Product
     */
    public function setStepMinMaxPrice($stepMinMaxPrice)
    {
        $this->stepMinMaxPrice = $stepMinMaxPrice;

        return $this;
    }

    /**
     * Get googleShoppingTaxonomy
     *
     * @return Taxonomy
     */
    public function getGoogleShoppingTaxonomy()
    {
        return $this->googleShoppingTaxonomy;
    }

    /**
     * Set googleShoppingTaxonomy
     *
     * @param Taxonomy $googleShoppingTaxonomy
     *
     * @return Product
     */
    public function setGoogleShoppingTaxonomy(Taxonomy $googleShoppingTaxonomy = null)
    {
        $this->googleShoppingTaxonomy = $googleShoppingTaxonomy;

        return $this;
    }

    /**
     * Get quantitySteps
     *
     * @return string
     */
    public function getQuantitySteps()
    {
        return $this->quantitySteps;
    }

    /**
     * Set quantitySteps
     *
     * @param string $quantitySteps
     *
     * @return Product
     */
    public function setQuantitySteps($quantitySteps)
    {
        $this->quantitySteps = $quantitySteps;

        return $this;
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     */
    public function validateQuantitySteps(ExecutionContextInterface $context)
    {
        $range = [];

        if (strpos($context->getValue()->quantitySteps, ';') !== false) {
            $range = explode(';', $context->getValue()->quantitySteps);
        } elseif ($context->getValue()->quantitySteps > $context->getValue()->maxQuantity) {
            $context->buildViolation(sprintf('Gedefinieerde stappen moeten kleiner/gelijk zijn aan %s',
                $context->getValue()->maxQuantity));
        } else {
            $range = range($context->getValue()->minQuantity, $context->getValue()->maxQuantity,
                $context->getValue()->quantitySteps);
        }

        if ($range) {
            $lowestRange = min($range);
            $minQuantity = $context->getValue()->minQuantity;
            $maxQuantity = $context->getValue()->maxQuantity;

            if ($lowestRange < $minQuantity && !$maxQuantity) {
                $context->buildViolation(sprintf('Gedefinieerde stappen moeten hoger dan %s zijn.',
                    $minQuantity));
            } elseif ($lowestRange < $minQuantity || max($range) > $maxQuantity) {
                $context->buildViolation(sprintf('Gedefinieerde stappen moeten tussen %s en %s liggen.',
                    $minQuantity, $maxQuantity))->addViolation();
            }
        }
    }

    /**
     * Get noStockBackorder
     *
     * @return boolean
     */
    public function getNoStockBackorder()
    {
        return $this->noStockBackorder;
    }

    /**
     * Set noStockBackorder
     *
     * @param boolean $noStockBackorder
     *
     * @return Product
     */
    public function setNoStockBackorder($noStockBackorder)
    {
        $this->noStockBackorder = $noStockBackorder;

        return $this;
    }

    /**
     * Get noStockQuotation
     *
     * @return boolean
     */
    public function getNoStockQuotation()
    {
        return $this->noStockQuotation;
    }

    /**
     * Set noStockQuotation
     *
     * @param boolean $noStockQuotation
     *
     * @return Product
     */
    public function setNoStockQuotation($noStockQuotation)
    {
        $this->noStockQuotation = $noStockQuotation;

        return $this;
    }

    /**
     * Add supplierGroupProduct
     *
     * @param SupplierGroupProduct $supplierGroupProduct
     *
     * @return Product
     */
    public function addSupplierGroupProduct(SupplierGroupProduct $supplierGroupProduct)
    {
        $supplierGroupProduct->setProduct($this);

        $this->supplierGroupProducts[] = $supplierGroupProduct;

        return $this;
    }

    /**
     * Remove supplierGroupProduct
     *
     * @param SupplierGroupProduct $supplierGroupProduct
     */
    public function removeSupplierGroupProduct(SupplierGroupProduct $supplierGroupProduct)
    {
        $this->supplierGroupProducts->removeElement($supplierGroupProduct);
    }

    /**
     * Get supplierGroupProducts
     *
     * @return Collection|SupplierGroupProduct[]
     */
    public function getSupplierGroupProducts()
    {
        return $this->supplierGroupProducts;
    }

    /**
     * Add additionalProductAssortment
     *
     * @param Assortment $additionalProductAssortment
     *
     * @return Product
     */
    public function addAdditionalProductAssortment(Assortment $additionalProductAssortment)
    {
        $this->additionalProductAssortments[] = $additionalProductAssortment;

        return $this;
    }

    /**
     * Remove additionalProductAssortment
     *
     * @param Assortment $additionalProductAssortment
     */
    public function removeAdditionalProductAssortment(Assortment $additionalProductAssortment)
    {
        $this->additionalProductAssortments->removeElement($additionalProductAssortment);
    }

    /**
     * Get additionalProductAssortments
     *
     * @return Collection
     */
    public function getAdditionalProductAssortments()
    {
        if (!$this->additionalProductAssortments->isEmpty()) {
            return $this->additionalProductAssortments;
        }

        return $this->getParent() ? $this->getParent()->getAdditionalProductAssortments() : $this->additionalProductAssortments;
    }

    /**
     * Add productTransportType
     *
     * @param ProductTransportType $productTransportType
     *
     * @return Product
     */
    public function addProductTransportType(ProductTransportType $productTransportType)
    {
        $productTransportType->setProduct($this);

        $this->productTransportTypes[] = $productTransportType;

        return $this;
    }

    /**
     * Remove productTransportType
     *
     * @param ProductTransportType $productTransportType
     */
    public function removeProductTransportType(ProductTransportType $productTransportType)
    {
        $this->productTransportTypes->removeElement($productTransportType);
    }

    /**
     * Get productTransportTypes
     *
     * @return Collection|ProductTransportType[]
     */
    public function getProductTransportTypes()
    {
        return $this->productTransportTypes;
    }

    /**
     * @return string
     */
    public function getController()
    {
        return 'AppBundle\Controller\ProductController::indexAction';
    }

    /**
     * @return string
     */
    public function getControllerParam()
    {
        return 'product';
    }

    /**
     * Add productPackagingUnit
     *
     * @param ProductPackagingUnit $productPackagingUnit
     *
     * @return Product
     */
    public function addProductPackagingUnit(ProductPackagingUnit $productPackagingUnit)
    {
        $productPackagingUnit->setProduct($this);

        $this->productPackagingUnits[] = $productPackagingUnit;

        return $this;
    }

    /**
     * Remove productPackagingUnit
     *
     * @param ProductPackagingUnit $productPackagingUnit
     */
    public function removeProductPackagingUnit(ProductPackagingUnit $productPackagingUnit)
    {
        $this->productPackagingUnits->removeElement($productPackagingUnit);
    }

    /**
     * Get productPackagingUnits
     *
     * @return Collection|PackagingUnit[]
     */
    public function getProductPackagingUnits()
    {
        return $this->productPackagingUnits;
    }

    /**
     * Set showOptions
     *
     * @param boolean $showOptions
     *
     * @return Product
     */
    public function setShowOptions($showOptions)
    {
        $this->showOptions = $showOptions;

        return $this;
    }

    /**
     * Get showOptions
     *
     * @return boolean
     */
    public function getShowOptions()
    {
        return $this->showOptions;
    }

    /**
     * Set propertySet
     *
     * @param ProductgroupPropertySet $propertySet
     *
     * @return Product
     */
    public function setPropertySet(ProductgroupPropertySet $propertySet = null)
    {
        $this->propertySet = $propertySet;

        return $this;
    }

    /**
     * Get propertySet
     *
     * @return ProductgroupPropertySet
     */
    public function getPropertySet()
    {
        return $this->propertySet;
    }

    /**
     * Add combinationProduct
     *
     * @param Product $combinationProduct
     *
     * @return Product
     */
    public function addCombinationProduct(Product $combinationProduct)
    {
        $this->combinationProducts[] = $combinationProduct;

        return $this;
    }

    /**
     * Remove combinationProduct
     *
     * @param Product $combinationProduct
     */
    public function removeCombinationProduct(Product $combinationProduct)
    {
        $this->combinationProducts->removeElement($combinationProduct);
    }

    /**
     * Get combinationProducts
     *
     * @return Collection
     */
    public function getCombinationProducts()
    {
        return $this->combinationProducts;
    }

    /**
     * Add combination
     *
     * @param Combination $combination
     *
     * @return Product
     */
    public function addCombination(Combination $combination)
    {
        $combination->setCombinationProduct($this);

        $this->combinations[] = $combination;

        return $this;
    }

    /**
     * Remove combination
     *
     * @param Combination $combination
     */
    public function removeCombination(Combination $combination)
    {
        $this->combinations->removeElement($combination);
    }

    /**
     * Get combinations
     *
     * @return Collection|Combination[]
     */
    public function getCombinations()
    {
        return $this->combinations;
    }

    /**
     * Set metadata
     *
     * @param array $metadata
     *
     * @return Product
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set configurable
     *
     * @param boolean $configurable
     *
     * @return Product
     */
    public function setConfigurable($configurable)
    {
        $this->configurable = $configurable;

        return $this;
    }

    /**
     * Get configurable
     *
     * @return boolean
     */
    public function getConfigurable()
    {
        return $this->configurable;
    }

    /**
     * Set expirationMeta
     *
     * @param ExpirationMeta $expirationMeta
     *
     * @return Product
     */
    public function setExpirationMeta(ExpirationMeta $expirationMeta = null)
    {
        $this->expirationMeta = $expirationMeta;

        return $this;
    }

    /**
     * Get expirationMeta
     *
     * @return ExpirationMeta
     */
    public function getExpirationMeta()
    {
        return $this->expirationMeta;
    }

    /**
     * Set serialNumberMeta
     *
     * @param SerialNumberMeta $serialNumberMeta
     *
     * @return Product
     */
    public function setSerialNumberMeta(SerialNumberMeta $serialNumberMeta = null)
    {
        $this->serialNumberMeta = $serialNumberMeta;

        return $this;
    }

    /**
     * Get serialNumberMeta
     *
     * @return SerialNumberMeta
     */
    public function getSerialNumberMeta()
    {
        return $this->serialNumberMeta;
    }

    /**
     * @return Letter
     */
    public function getLetter()
    {
        return $this->letter;
    }

    /**
     * @param null|Letter $letter
     *
     * @return Product
     */
    public function setLetter($letter): Product
    {
        $this->letter = $letter;

        return $this;
    }

    /**
     * @return bool
     */
    public function isProductLetterAvailable()
    {
        if (null !== $this->getLetter()) {
            return true;
        }

        if (null !== $this->getParent() && null !== $this->getParent()->getLetter()) {
            return true;
        }

        return false;
    }

    /**
     * Remove Letter
     *
     * @param Letter $Letter
     * @return Product
     */
    public function removeLetter(Letter $Letter)
    {
        $this->letter->removeElement($Letter);

        return $this;
    }

    /**
     * @return array
     */
    public function getAuditSettings(): array
    {
        return [
            'mapping' => [
                'title' => 'Titel',
                'shortDescription' => 'Omschrijving',
                'ean' => 'EAN',
                'sku' => 'SKU',
                'price' => 'Prijs (incl. BTW)',
                'vat' => 'BTW',
                'deletedAt' => 'Verwijderd op',
                'createdAt' => 'Aaangemaakt op',
                'updatedAt' => 'Geupdate op',
            ],
            'label' => function (Product $product) {
                return $product->getDisplayName();
            },
            'title' => function (Product $product) {
                return $product->getDisplayName();
            },
            'route' => function (Product $product) {
                return [
                    'route' => 'admin_catalog_product_view',
                    'routeParameters' => [
                        'id' => $product->getId(),
                    ],
                ];
            },
        ];
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return Product
     */
    public function setPosition(int $position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getShowContent(): ?bool
    {
        return $this->showContent;
    }

    /**
     * @param bool|null $showContent
     * @return Product
     */
    public function setShowContent(?bool $showContent): Product
    {
        $this->showContent = $showContent;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getCompanyProducts(): Collection
    {
        return $this->companyProducts;
    }

    /**
     * @param CompanyProduct $companyProducts
     * @return Product
     */
    public function setCompanyProducts(CompanyProduct $companyProducts): self
    {
        $this->companyProducts = $companyProducts;

        return $this;
    }

    /**
     * @return VatGroup[]|Collection
     */
    public function getVatGroups(): Collection
    {
        return $this->vatGroups;
    }

    /**
     * @param VatGroup[]|Collection $vatGroups
     * @return Product
     */
    public function setVatGroups(Collection $vatGroups): Product
    {
        $this->vatGroups = $vatGroups;

        return $this;
    }

    /**
     * @param VatGroup $vatGroup
     * @return Product
     */
    public function addVatGroup(VatGroup $vatGroup): Product
    {
        if (!$this->vatGroups->contains($vatGroup)) {
            $this->vatGroups->add($vatGroup);
        }

        return $this;
    }

    /**
     * @param VatGroup $vatGroup
     * @return $this
     */
    public function removeVatGroup(VatGroup $vatGroup): Product
    {
        if ($this->vatGroups->contains($vatGroup)) {
            $this->vatGroups->removeElement($vatGroup);
        }

        return $this;
    }

    /**
     * @param Country $country
     * @return VatGroup|null
     */
    public function getVatGroupForCountry(Country $country)
    {
        return $this->vatGroups->filter(function (VatGroup $vatGroup) use ($country) {
            return $vatGroup->getCountry()->getId() === $country->getId();
        })->current() ?: null;
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $executionContext
     * @param                           $payload
     */
    public function validate(ExecutionContextInterface $executionContext, $payload)
    {
        if ($this->isCombination() && $this->getCombinations()->count() < 2) {
            $executionContext->buildViolation('Een combinatie moet minstens 2 producten bevatten')
                ->atPath('combinations')
                ->addViolation();
        }

        $countries = [];
        /** @var VatGroup $vatGroup */
        foreach ($this->getVatGroups() as $vatGroup) {
            if (\in_array($vatGroup->getCountry(), $countries, true)) {
                $executionContext->buildViolation('Er mogen niet meerdere BTW groepen uit hetzelfde land op een product zitten.')
                    ->atPath('vatGroups')
                    ->addViolation();

                break;
            }

            $countries[] = $vatGroup->getCountry();
        }
    }

    /**
     * @return Upsell[]|Collection
     */
    public function getUpsells()
    {
        return $this->upsells;
    }

    /**
     * @param Upsell $upsell
     */
    public function addUpsell(Upsell $upsell)
    {
        $upsell->setProduct($this);

        if(!$this->upsells->contains($upsell)) {
            $this->upsells->add($upsell);
        }
    }

    /**
     * @param Upsell $upsell
     */
    public function removeUpsell(Upsell $upsell)
    {
        if($this->upsells->contains($upsell)) {
            $this->upsells->removeElement($upsell);
        }
    }

    /**
     * @return Product|null
     */
    public function getFirstPersonalization(): ?Product
    {
        if(!$this->getPersonalizations()->isEmpty()) {
            return $this->getPersonalizations()->first();
        }

        try {
            /** @var ProductProperty $productPropertyPersonalization */
            $productPropertyPersonalization = $this->findProductProperty('personalization');
        } catch (\Exception $exception) {
            $productPropertyPersonalization = null;
        }

        return $productPropertyPersonalization ? $productPropertyPersonalization->getProductVariation() : null;
    }

    /**
     * @return bool
     */
    public function isRegularProduct(): bool
    {
        return !$this->isTransportType()
            && !$this->isPersonalization()
            && !$this->isCard()
            && !$this->isLetter();
    }

    /**
     * @return bool
     */
    public function isPersonalizationProduct(): bool
    {
        return $this->isPersonalization()
            || $this->isCard()
            || $this->isLetter();
    }
}
