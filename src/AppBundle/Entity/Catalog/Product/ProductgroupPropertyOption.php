<?php

namespace AppBundle\Entity\Catalog\Product;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 */
class ProductgroupPropertyOption
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\ProductgroupProperty", inversedBy="options")
     * @ORM\JoinColumn(nullable=false);
     *
     */
    protected $productgroupProperty;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     *
     */
    private $position;

    /**
     * @ORM\Column(type="text")
     */
    protected $value;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->value;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ProductgroupProperty
     *
     * @param ProductgroupProperty $productgroupProperty
     *
     * @return ProductgroupPropertyOption
     */
    public function setProductgroupProperty(ProductgroupProperty $productgroupProperty = null)
    {
        $this->productgroupProperty = $productgroupProperty;

        return $this;
    }

    /**
     * Get ProductgroupProperty
     *
     * @return ProductgroupProperty
     */
    public function getProductgroupProperty()
    {
        return $this->productgroupProperty;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return ProductgroupPropertyOption
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return ProductgroupPropertyOption
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }
}
