<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Entity\Relation\Company;
use AppBundle\Traits\PublishableEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * Class CompanyCard
 * @package AppBundle\Entity\Catalog\Product
 */
class CompanyCard extends ProductCard
{

    use PublishableEntity;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="cards")
     */
    protected $company;

    /**
     * @param Company $company
     * @return CompanyCard
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

}