<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Entity\Relation\Company;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * Class CompanyTransportType
 * @package AppBundle\Entity\Catalog\Product
 */
class CompanyTransportType
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var Company|null
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="transportTypes")
     */
    protected $company;

    /**
     * @var Productgroup|null
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Productgroup")
     */
    protected $productGroup;

    /**
     * @var TransportType|null
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\TransportType")
     */
    protected $transportType;

    /**
     * @var float|null
     * @ORM\Column(type="float")
     */
    protected $price;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Productgroup|null
     */
    public function getProductGroup(): ?Productgroup
    {
        return $this->productGroup;
    }

    /**
     * @param Productgroup|null $productGroup
     */
    public function setProductGroup(?Productgroup $productGroup): void
    {
        $this->productGroup = $productGroup;
    }

    /**
     * @return TransportType|null
     */
    public function getTransportType(): ?TransportType
    {
        return $this->transportType;
    }

    /**
     * @param TransportType|null $transportType
     */
    public function setTransportType(?TransportType $transportType): void
    {
        $this->transportType = $transportType;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * @param Company|null $company
     */
    public function setCompany(?Company $company): void
    {
        $this->company = $company;

        $company->addTransportType($this);
    }
}