<?php

namespace AppBundle\Entity\Catalog\Product;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Trait SkuValidationTrait
 * @package AppBundle\Entity\Catalog\Product
 */
trait SkuValidationTrait
{
    /**
     * @return Product|null
     */
    abstract public function getParent(): ?Product;

    /**
     * @return null|string
     */
    abstract public function getSku(): ?string;

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $executionContext
     * @param                           $payload
     */
    public function checkIfSkuIsApplicable(ExecutionContextInterface $executionContext, $payload)
    {
        if (null === $this->getParent() && null !== $this->getSku()) {
            $executionContext->buildViolation('Een hoofdproduct mag geen sku bevatten')
                ->atPath('sku')
                ->addViolation();
        }

        if (null !== $this->getId() && null !== $this->getParent() && null === $this->getSku()) {
            $executionContext->buildViolation('Een variatie moet een sku bevatten')
                ->atPath('sku')
                ->addViolation();
        }
    }
}