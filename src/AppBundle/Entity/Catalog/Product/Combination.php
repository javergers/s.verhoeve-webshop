<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Model\Product\CombinationModel;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="product_combination")
 * Class CombinationProduct
 * @package AppBundle\Entity\Catalog\Product\Product
 */
class Combination extends CombinationModel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product", inversedBy="combinations")
     */
    protected $combinationProduct;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product", inversedBy="combinationProducts")
     */
    protected $product;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $main;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $stock;

    /**
     * @ORM\Column(type="integer")
     */
    protected $quantity;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $metadata;

    /**
     * Set main
     *
     * @param boolean $main
     *
     * @return Combination
     */
    public function setMain($main): Combination
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main
     *
     * @return boolean
     */
    public function getMain(): ?bool
    {
        return $this->main;
    }

    /**
     * Set stock
     *
     * @param boolean $stock
     *
     * @return Combination
     */
    public function setStock($stock): Combination
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return boolean
     */
    public function getStock(): bool
    {
        return $this->stock;
    }

    /**
     * Set combinationProduct
     *
     * @param Product $combinationProduct
     *
     * @return Combination
     */
    public function setCombinationProduct(Product $combinationProduct): Combination
    {
        $this->combinationProduct = $combinationProduct;

        return $this;
    }

    /**
     * Get combinationProduct
     *
     * @return Product
     */
    public function getCombinationProduct(): Product
    {
        return $this->combinationProduct;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return Combination
     */
    public function setProduct(Product $product): Combination
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return Combination
     */
    public function setQuantity($quantity): Combination
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * Set metadata
     *
     * @param array $metadata
     *
     * @return Combination
     */
    public function setMetadata($metadata): Combination
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return array
     */
    public function getMetadata(): array
    {
        return $this->metadata;
    }
}
