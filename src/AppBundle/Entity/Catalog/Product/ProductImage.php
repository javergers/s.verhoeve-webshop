<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Interfaces\ImageInterface;
use AppBundle\Traits\ImageEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @ORM\Table("product_image");
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 */
class ProductImage implements ImageInterface
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    use ImageEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product", inversedBy="productImages")
     * @ORM\JoinColumn(nullable=false);
     *
     */
    protected $product;

    /**
     * @ORM\Column(type="text")
     *
     */
    protected $path;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     *
     */
    protected $description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    protected $main;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    protected $category;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     *
     */
    private $position;

    /**
     * @return string
     */
    public function generateName()
    {
        $name = $this->getProduct()->translate("nl_NL")->getSlug();

        return $name;
    }

    public function getPathColumn()
    {
        return 'path';
    }

    public function getStoragePath()
    {
        return "images/product/" . $this->getProduct()->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ProductImage
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return ProductImage
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return ProductImage
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return ProductImage
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return ProductImage
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set main
     *
     * @param boolean $main
     *
     * @return ProductImage
     */
    public function setMain($main)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main
     *
     * @return boolean
     */
    public function getMain()
    {
        return $this->main;
    }

    /**
     * Set category
     *
     * @param boolean $category
     *
     * @return ProductImage
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return boolean
     */
    public function getCategory()
    {
        return $this->category;
    }
}
