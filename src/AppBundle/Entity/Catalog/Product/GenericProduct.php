<?php

namespace AppBundle\Entity\Catalog\Product;

use Doctrine\ORM\Mapping as ORM;
use RuleBundle\Annotation as Rule;

/**
 * @ORM\Entity()
 * Class GenericProduct
 * @package AppBundle\Entity\Catalog\Product
 *
 * @Rule\Entity(description="GenericProduct")
 */
class GenericProduct extends Product
{
    use SkuValidationTrait;
}
