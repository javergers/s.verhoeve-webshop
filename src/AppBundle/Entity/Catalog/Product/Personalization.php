<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Traits\PublishableEntity;
use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * Class Personalization
 * @package AppBundle\Entity\Catalog\Product
 *
 * @ORM\Entity()
 */
class Personalization extends Product
{
    use PublishableEntity;
    use SkuValidationTrait;

    /**
     * @var boolean
     */
    protected $purchasable = true;

    /**
     * @var string|null
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\PersonalizationTypeType")
     * @ORM\Column(type="PersonalizationTypeType", nullable=true)
     */
    protected $personalizationType;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    protected $clothingPrint;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $printRotation;

    /**
     * Personalization constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->purchasable = true;
        $this->clothingPrint = false;
        $this->printRotation = 0;
    }

    /**
     * @return string|null
     */
    public function getPersonalizationType(): ?string
    {
        return $this->personalizationType;
    }

    /**
     * @param string|null $personalizationType
     */
    public function setPersonalizationType(?string $personalizationType): void
    {
        $this->personalizationType = $personalizationType;
    }

    /**
     * @param bool $clothingPrint
     *
     * @return Personalization
     */
    public function setClothingPrint(bool $clothingPrint): Personalization
    {
        $this->clothingPrint = $clothingPrint;

        return $this;
    }

    /**
     * @return bool
     */
    public function isClothingPrint(): bool
    {
        if ($this->getParent() && $this->getParent()->isClothingPrint()) {
            return true;
        }

        return (bool)$this->clothingPrint;
    }

    /**
     * @param int $printRotation
     */
    public function setPrintRotation(int $printRotation = 0)
    {
        $this->printRotation = $printRotation;
    }

    /**
     * @return int
     */
    public function getPrintRotation(): int
    {
        if ($this->getParent() && null !== ($printRotation = $this->getParent()->getPrintRotation())) {
            return $printRotation;
        }

        return (int)$this->printRotation;
    }
}