<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Interfaces\ImageInterface;
use AppBundle\Traits\ImageEntity;
use AppBundle\Traits\PublishableEntity;
use Doctrine\ORM\Mapping as ORM;
use RuleBundle\Entity\Rule;

/**
 * @ORM\Entity()
 * Class ProductSticker
 * @package AppBundle\Entity\Catalog\Product
 */
class ProductSticker implements ImageInterface
{
    use PublishableEntity;
    use ImageEntity;

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    protected $description;

    /**
     * @var Rule|null
     * @ORM\ManyToOne(targetEntity="RuleBundle\Entity\Rule")
     */
    protected $rule;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $path;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return Rule|null
     */
    public function getRule(): ?Rule
    {
        return $this->rule;
    }

    /**
     * @param Rule|null $rule
     */
    public function setRule(?Rule $rule): void
    {
        $this->rule = $rule;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return ProductSticker
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }


    /**
     * @return string
     */
    public function generateName(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getPathColumn(): string
    {
        return 'path';
    }

    /**
     * @return string
     */
    public function getStoragePath(): string
    {
        return 'images/productstickers/' . $this->getId();
    }
}