<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Entity\Geography\Country;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class ProductCountry
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product", inversedBy="productCountries")
     */
    protected $product;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\Country")
     * @ORM\JoinColumn(name="country", referencedColumnName="id", nullable=false)
     */
    protected $country;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $min_price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $max_price;
//
//    /**
//     * @ORM\Column(type="float", nullable=true)
//     *
//     */
//    private $minPrice;

    /**
     * Set price
     *
     * @param float $price
     *
     * @return ProductCountry
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return ProductCountry
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \AppBundle\Entity\Catalog\Product\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set country
     *
     * @param Country $country
     *
     * @return ProductCountry
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set minPrice
     *
     * @param float $minPrice
     *
     * @return ProductCountry
     */
    public function setMinPrice($minPrice)
    {
        $this->min_price = $minPrice;

        return $this;
    }

    /**
     * Get minPrice
     *
     * @return float
     */
    public function getMinPrice()
    {
        return $this->min_price;
    }

    /**
     * Set maxPrice
     *
     * @param float $maxPrice
     *
     * @return ProductCountry
     */
    public function setMaxPrice($maxPrice)
    {
        $this->max_price = $maxPrice;

        return $this;
    }

    /**
     * Get maxPrice
     *
     * @return float
     */
    public function getMaxPrice()
    {
        return $this->max_price;
    }
}
