<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Entity\Relation\Company;
use AppBundle\Traits\PublishableEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CompanyProduct
 * @package AppBundle\Entity\Catalog\Product
 * @ORM\Entity()
 */
class CompanyProduct extends Product
{

    use PublishableEntity;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product", inversedBy="companyProducts")
     */
    protected $relatedProduct;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="products")
     */
    protected $company;

    /**
     * @param Product $relatedProduct
     * @return CompanyProduct
     */
    public function setRelatedProduct(Product $relatedProduct) {
        $this->relatedProduct = $relatedProduct;

        return $this;
    }

    /**
     * @return Product
     */
    public function getRelatedProduct() {
        return $this->relatedProduct;
    }

    /**
     * @param Company $company
     * @return CompanyProduct
     */
    public function setCompany(Company $company) {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Company
     */
    public function getCompany() {
        return $this->company;
    }
}
