<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Entity\Finance\Ledger;
use AppBundle\Services\Letter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use RuleBundle\Annotation as Rule;
use Symfony\Component\Validator\Constraints as Assert;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity
 *
 * @Rule\Entity(description="Productgroep")
 */
class Productgroup
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Productgroup", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\Productgroup", mappedBy="parent")
     */
    protected $children;

    /**
     * @ORM\Column(type="text", length=50)
     *
     * @Rule\Property(description="Titel")
     */
    protected $title;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Finance\Ledger", inversedBy="productgroups")
     * @ORM\JoinColumn(nullable=false)
     *
     */
    protected $ledger;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\Product", mappedBy="productgroup")
     */
    protected $products;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $template;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $product_template;

    /**
     * @ORM\Column(type="string", nullable=true, length=3, unique=true)
     * @Assert\Length(min = 3, max = 3)
     *
     * @Rule\Property(description="SKU prefix")
     */
    protected $skuPrefix;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\ProductgroupPropertySet", mappedBy="productgroup")
     */
    protected $productgroupPropertySets;

    /**
     * @ORM\Column(type="boolean");
     */
    protected $pickupEnabled = false;

    /**
     * @ORM\Column(type="boolean");
     */
    protected $productPropertySelect = false;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Catalog\Product\ProductgroupProperty", mappedBy="productgroups")
     */
    protected $productgroupProperties;

    /**
     * @var bool|null
     * @ORM\Column(type="boolean", options={"default":0})
     */
    protected $availableForUpsell;

    /**
     * @var string|null
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\UpsellQuantityType")
     * @ORM\Column(type="UpsellQuantityType", nullable=true)
     */
    protected $upsellQuantityMode;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\ProductTransportType", mappedBy="productGroup",
     *                                                                                      cascade={"persist"})
     */
    protected $productTransportTypes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->productgroupProperties = new ArrayCollection();
        $this->productgroupPropertySets = new ArrayCollection();
        $this->productTransportTypes = new ArrayCollection();
        $this->availableForUpsell = false;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ledger
     *
     * @param Ledger $ledger
     *
     * @return Productgroup
     */
    public function setLedger(Ledger $ledger)
    {
        $this->ledger = $ledger;

        return $this;
    }

    /**
     * Get ledger
     *
     * @return Ledger
     */
    public function getLedger()
    {
        return $this->ledger;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Productgroup
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set parent
     *
     * @param Productgroup $parent
     *
     * @return Productgroup
     */
    public function setParent(Productgroup $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return Productgroup
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child
     *
     * @param Productgroup $child
     *
     * @return Productgroup
     */
    public function addChild(Productgroup $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param Productgroup $child
     */
    public function removeChild(Productgroup $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set template
     *
     * @param string $template
     *
     * @return Productgroup
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set productTemplate
     *
     * @param string $productTemplate
     *
     * @return Productgroup
     */
    public function setProductTemplate($productTemplate)
    {
        $this->product_template = $productTemplate;

        return $this;
    }

    /**
     * Get productTemplate
     *
     * @return string
     */
    public function getProductTemplate()
    {
        return $this->product_template;
    }

    /**
     * Add product
     *
     * @param Product $product
     *
     * @return Productgroup
     */
    public function addProduct(Product $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param Product $product
     */
    public function removeProduct(Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set skuPrefix
     *
     * @param string $skuPrefix
     *
     * @return Productgroup
     */
    public function setSkuPrefix($skuPrefix)
    {
        $this->skuPrefix = $skuPrefix;

        return $this;
    }

    /**
     * Get skuPrefix
     *
     * @return string
     */
    public function getSkuPrefix()
    {
        return $this->skuPrefix;
    }

    /**
     * Set pickupEnabled
     *
     * @param boolean $pickupEnabled
     *
     * @return Productgroup
     */
    public function setPickupEnabled($pickupEnabled)
    {
        $this->pickupEnabled = $pickupEnabled;

        return $this;
    }

    /**
     * Get pickupEnabled
     *
     * @return boolean
     */
    public function getPickupEnabled()
    {
        return $this->pickupEnabled;
    }


    /**
     * Set productPropertySelect
     *
     * @param boolean $productPropertySelect
     *
     * @return Productgroup
     */
    public function setProductPropertySelect($productPropertySelect)
    {
        $this->productPropertySelect = $productPropertySelect;

        return $this;
    }

    /**
     * Get productPropertySelect
     *
     * @return boolean
     */
    public function getProductPropertySelect()
    {
        return $this->productPropertySelect;
    }

    /**
     * Add productgroupPropertySet
     *
     * @param ProductgroupPropertySet $productgroupPropertySet
     *
     * @return Productgroup
     */
    public function addProductgroupPropertySet(ProductgroupPropertySet $productgroupPropertySet)
    {
        $this->productgroupPropertySets[] = $productgroupPropertySet;

        return $this;
    }

    /**
     * Remove productgroupPropertySet
     *
     * @param ProductgroupPropertySet $productgroupPropertySet
     */
    public function removeProductgroupPropertySet(ProductgroupPropertySet $productgroupPropertySet)
    {
        $this->productgroupPropertySets->removeElement($productgroupPropertySet);
    }

    /**
     * Get productgroupPropertySets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductgroupPropertySets()
    {
        return $this->productgroupPropertySets;
    }

    /**
     * Get ProductgroupProperties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductgroupProperties()
    {
        return $this->productgroupProperties;
    }

    /**
     * Add productgroupProperty
     *
     * @param ProductgroupProperty $productgroupProperty
     *
     * @return Productgroup
     */
    public function addProductgroupProperty(ProductgroupProperty $productgroupProperty)
    {
        $this->productgroupProperties[] = $productgroupProperty;

        return $this;
    }

    /**
     * Remove productgroupProperty
     *
     * @param ProductgroupProperty $productgroupProperty
     */
    public function removeProductgroupProperty(ProductgroupProperty $productgroupProperty)
    {
        $this->productgroupProperties->removeElement($productgroupProperty);
    }

    /**
     * @return string|null
     */
    public function getUpsellQuantityMode(): ?string
    {
        return $this->upsellQuantityMode;
    }

    /**
     * @param string|null $upsellQuantityMode
     */
    public function setUpsellQuantityMode(?string $upsellQuantityMode): void
    {
        $this->upsellQuantityMode = $upsellQuantityMode;
    }

    /**
     * @return bool|null
     */
    public function getAvailableForUpsell(): ?bool
    {
        return $this->availableForUpsell;
    }

    /**
     * @param bool|null $availableForUpsell
     */
    public function setAvailableForUpsell(?bool $availableForUpsell): void
    {
        $this->availableForUpsell = $availableForUpsell;
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $context
     */
    public function validateUpsell(ExecutionContextInterface $context)
    {
        if($this->availableForUpsell && null === $this->getUpsellQuantityMode()) {
            $context->buildViolation("Het veld 'Aantal bij upsell' is verplicht als je de productgroep beschikbaar stelt voor upsell");
        }
    }

    /**
     * Add productTransportType
     *
     * @param ProductTransportType $productTransportType
     *
     * @return Productgroup
     */
    public function addProductTransportType(ProductTransportType $productTransportType)
    {
        if(!$this->productTransportTypes->contains($productTransportType)) {
            $productTransportType->setProductGroup($this);
            $this->productTransportTypes->add($productTransportType);
        }

        return $this;
    }

    /**
     * Remove productTransportType
     *
     * @param ProductTransportType $productTransportType
     */
    public function removeProductTransportType(ProductTransportType $productTransportType)
    {
        if($this->productTransportTypes->contains($productTransportType)) {
            $this->productTransportTypes->removeElement($productTransportType);
        }
    }

    /**
     * Get productTransportTypes
     *
     * @return Collection|ProductTransportType[]
     */
    public function getProductTransportTypes()
    {
        return $this->productTransportTypes;
    }
}
