<?php

namespace AppBundle\Entity\Catalog\Product;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * Class ProductPropertySet
 * @package AppBundle\Entity\Catalog\Product\Product
 */
class ProductgroupPropertySet
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Productgroup", inversedBy="productgroupPropertySets")
     */
    protected $productgroup;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Catalog\Product\ProductgroupProperty", inversedBy="productgroupPropertySets")
     */
    protected $productgroupProperties;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Product\Product", mappedBy="propertySet")
     */
    protected $products;

    public function __construct()
    {
        $this->productgroupProperties = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ProductgroupPropertySet
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ProductgroupPropertySet
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add productgroupProperty
     *
     * @param ProductgroupProperty $productgroupProperty
     *
     * @return ProductgroupPropertySet
     */
    public function addProductgroupProperty(ProductgroupProperty $productgroupProperty)
    {
        $this->productgroupProperties[] = $productgroupProperty;

        return $this;
    }

    /**
     * Remove productgroupProperty
     *
     * @param ProductgroupProperty $productgroupProperty
     */
    public function removeProductgroupProperty(ProductgroupProperty $productgroupProperty)
    {
        $this->productgroupProperties->removeElement($productgroupProperty);
    }

    /**
     * Get productgroupProperties
     *
     * @return Collection
     */
    public function getProductgroupProperties()
    {
        return $this->productgroupProperties;
    }

    /**
     * Set productgroup
     *
     * @param Productgroup $productgroup
     *
     * @return ProductgroupPropertySet
     */
    public function setProductgroup(Productgroup $productgroup = null)
    {
        $this->productgroup = $productgroup;

        return $this;
    }

    /**
     * Get productgroup
     *
     * @return Productgroup
     */
    public function getProductgroup()
    {
        return $this->productgroup;
    }

    /**
     * Add product
     *
     * @param Product $product
     *
     * @return ProductgroupPropertySet
     */
    public function addProduct(Product $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param Product $product
     */
    public function removeProduct(Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return Collection
     */
    public function getProducts()
    {
        return $this->products;
    }
}
