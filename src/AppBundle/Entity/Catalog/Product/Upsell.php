<?php

namespace AppBundle\Entity\Catalog\Product;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * Class Upsell
 * @package AppBundle\Entity\Catalog\Product
 */
class Upsell
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var Product|null
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="upsells")
     */
    protected $product;

    /**
     * @var Product|null
     * @ORM\ManyToOne(targetEntity="Product")
     */
    protected $upsellProduct;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $position;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Product|null
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param Product|null $product
     */
    public function setProduct(?Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return Product|null
     */
    public function getUpsellProduct(): ?Product
    {
        return $this->upsellProduct;
    }

    /**
     * @param Product|null $upsellProduct
     */
    public function setUpsellProduct(?Product $upsellProduct): void
    {
        $this->upsellProduct = $upsellProduct;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     */
    public function setPosition(?int $position): void
    {
        $this->position = $position;
    }
}