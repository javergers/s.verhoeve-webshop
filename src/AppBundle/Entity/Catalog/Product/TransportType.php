<?php

namespace AppBundle\Entity\Catalog\Product;

use AppBundle\Entity\Carrier\Carrier;
use AppBundle\Entity\Carrier\DeliveryMethod;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 *
 */
class TransportType extends Product
{
    use TimestampableEntity;

    /**
     * @var boolean
     */
    protected $purchasable = false;

    /**
     * @var boolean
     */
    protected $publish = true;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Carrier\Carrier")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $preferredCarrier;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Carrier\DeliveryMethod")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $preferredDeliveryMethod;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getName();
    }

    /**
     * Add combination
     *
     * @param Combination $combination
     *
     * @return TransportType
     */
    public function addCombination(Combination $combination)
    {
        $this->combinations[] = $combination;

        return $this;
    }

    /**
     * Remove combination
     *
     * @param Combination $combination
     */
    public function removeCombination(Combination $combination)
    {
        $this->combinations->removeElement($combination);
    }

    /**
     * Get combinations
     *
     * @return Collection|Combination[]
     */
    public function getCombinations()
    {
        return $this->combinations;
    }

    /**
     * Set metadata
     *
     * @param array $metadata
     *
     * @return TransportType
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set configurable
     *
     * @param boolean $configurable
     *
     * @return TransportType
     */
    public function setConfigurable($configurable)
    {
        $this->configurable = $configurable;

        return $this;
    }

    /**
     * Get configurable
     *
     * @return boolean
     */
    public function getConfigurable()
    {
        return $this->configurable;
    }

    /**
     * Set preferredCarrier
     *
     * @param Carrier|null $preferredCarrier
     *
     * @return TransportType
     */
    public function setPreferredCarrier(?Carrier $preferredCarrier): TransportType
    {
        $this->preferredCarrier = $preferredCarrier;

        return $this;
    }

    /**
     * Get preferredCarrier
     *
     * @return Carrier|null
     */
    public function getPreferredCarrier(): ?Carrier
    {
        return $this->preferredCarrier;
    }

    /**
     * Set preferredDeliveryMethod
     *
     * @param DeliveryMethod|null $preferredDeliveryMethod
     *
     * @return TransportType
     */
    public function setPreferredDeliveryMethod(?DeliveryMethod $preferredDeliveryMethod): TransportType
    {
        $this->preferredDeliveryMethod = $preferredDeliveryMethod;

        return $this;
    }

    /**
     * Get preferredDeliveryMethod
     *
     * @return DeliveryMethod|null
     */
    public function getPreferredDeliveryMethod(): ?DeliveryMethod
    {
        return $this->preferredDeliveryMethod;
    }
}
