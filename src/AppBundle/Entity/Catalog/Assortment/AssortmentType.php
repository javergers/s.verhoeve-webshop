<?php

namespace AppBundle\Entity\Catalog\Assortment;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class AssortmentType
{
    const TYPE_ADDITIONAL_PRODUCTS = 'additional_products';
    const TYPE_CARDS = 'cards';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="`key`", length=30, nullable=true)
     */
    protected $key;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Assortment\Assortment", mappedBy="assortmentType")
     */
    protected $assortments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->assortments = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set key
     *
     * @param string $key
     *
     * @return AssortmentType
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return AssortmentType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get assortments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssortments()
    {
        return $this->assortments;
    }

    /**
     * Add assortment
     *
     * @param Assortment $assortment
     *
     * @return AssortmentType
     */
    public function addAssortment(Assortment $assortment)
    {
        $this->assortments[] = $assortment;

        return $this;
    }

    /**
     * Remove assortment
     *
     * @param Assortment $assortment
     */
    public function removeAssortment(Assortment $assortment)
    {
        $this->assortments->removeElement($assortment);
    }
}
