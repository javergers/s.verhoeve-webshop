<?php

namespace AppBundle\Entity\Catalog\Assortment;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Site\Banner;
use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Site\Site;
use AppBundle\Interfaces\ImageInterface;
use AppBundle\Interfaces\MenuSourceInterface;
use AppBundle\Interfaces\Preview\PreviewableEntityInterface;
use AppBundle\Model\Assortment\AssortmentModel;
use AppBundle\Traits\AccessibleEntity;
use AppBundle\Traits\PublishableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Sluggable\Util\Urlizer;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use RuleBundle\Annotation as Rule;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AssortmentRepository")
 * @ORM\AssociationOverrides({
 *      @ORM\AssociationOverride(name="accessibleCustomerGroups",
 *          joinTable=@ORM\JoinTable(
 *              name="assortment_accessible_customer_group"
 *          )
 *      )
 * })
 * @ORM\Cache(usage="READ_ONLY", region="region_entity")
 *
 *
 * @method AssortmentTranslation translate(string $locale = null, boolean $fallbackToDefault = null)
 *
 * @Rule\Entity(description="Assortiment")
 */
class Assortment extends AssortmentModel implements MenuSourceInterface, PreviewableEntityInterface, ImageInterface
{
    use ORMBehaviors\Translatable\Translatable;
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use PublishableEntity;
    use AccessibleEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $image;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Banner", inversedBy="assortments")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $banner;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Assortment\AssortmentProduct", mappedBy="assortment",
     *                                                                                      cascade={"persist",
     *                                                                                      "remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\Cache("READ_ONLY")
     * @Rule\Property(description="Assortiment producten", relation="child")
     */
    protected $assortmentProducts;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Site\Site", inversedBy="assortments")
     */
    protected $sites;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="assortments")
     */
    protected $companies;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $owner;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Assortment\AssortmentType", inversedBy="assortments")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $assortmentType;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Page")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $content;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $contentSize;

    /**
     * @ORM\Column(type="integer", nullable=true, length=1)
     */
    protected $contentPosition;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Catalog\Assortment\Assortment", mappedBy="cardAssortment")
     * @ORM\OrderBy({"name" = "ASC"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $assortments;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Assortment\Assortment", inversedBy="assortments")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $cardAssortment;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $position;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":0})
     */
    protected $showFilter;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->assortmentProducts = new ArrayCollection();
        $this->sites = new ArrayCollection();

        $this->showFilter = false;
    }

    /**
     * @return array
     */
    public static function getMenuSourceConfig()
    {
        return [
            'labels' => [
                'description' => "Assortimenten",
                'singular' => "Assortiment",
            ],
        ];
    }

    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Assortment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Add assortmentProduct
     *
     * @param AssortmentProduct $assortmentProduct
     *
     * @return Assortment
     */
    public function addAssortmentProduct(AssortmentProduct $assortmentProduct)
    {
        $assortmentProduct->setAssortment($this);

        $this->assortmentProducts[] = $assortmentProduct;

        return $this;
    }

    /**
     * Remove assortmentProduct
     *
     * @param AssortmentProduct $assortmentProduct
     */
    public function removeAssortmentProduct(AssortmentProduct $assortmentProduct)
    {
        $this->assortmentProducts->removeElement($assortmentProduct);
    }

    /**
     * Get products
     *
     * @return Collection|Product[]
     */
    public function getProducts()
    {
        $products = new ArrayCollection();

        foreach ($this->getAssortmentProducts() as $assortmentProduct) {
            try {
                /** @var Product $product */
                $product = $assortmentProduct->getProduct();

                if ($product === null || (method_exists($product,
                            'isPublished') && !$product->isPublished())) {
                    continue;
                }

                $products[] = $product;
            } catch (EntityNotFoundException $e) {
                continue;
            }
        }

        return $products;
    }

    /**
     * @return AssortmentProduct|null
     */
    public function getFirstAssortmentProduct()
    {
        return $this->getAssortmentProducts()->first() ?: null;
    }

    /**
     * Get assortmentProducts
     *
     * @return Collection
     */
    public function getAssortmentProducts()
    {
        return $this->assortmentProducts;
    }

    /**
     * Add site
     *
     * @param Site $site
     *
     * @return Assortment
     */
    public function addSite(Site $site)
    {
        $this->sites[] = $site;

        return $this;
    }

    /**
     * Remove site
     *
     * @param Site $site
     */
    public function removeSite(Site $site)
    {
        $this->sites->removeElement($site);
    }

    /**
     * Get sites
     *
     * @return Collection|Site[]
     */
    public function getSites()
    {
        return $this->sites;
    }

    /**
     * @return string
     */
    public function getMenuTitle()
    {
        return $this->translate()->getTitle();
    }

    /**
     * Get assortmentType
     *
     * @return AssortmentType
     */
    public function getAssortmentType()
    {
        return $this->assortmentType;
    }

    /**
     * Set assortmentType
     *
     * @param AssortmentType $assortmentType
     *
     * @return Assortment
     */
    public function setAssortmentType(AssortmentType $assortmentType = null)
    {
        $this->assortmentType = $assortmentType;

        return $this;
    }

    /**
     * Get banner
     *
     * @return Banner
     */
    public function getBanner()
    {
        return $this->banner;
    }

    /**
     * Set banner
     *
     * @param Banner $banner
     *
     * @return Assortment
     */
    public function setBanner(Banner $banner = null)
    {
        $this->banner = $banner;

        return $this;
    }

    public function getController()
    {
        return 'AppBundle\Controller\AssortmentController::indexAction';
    }

    /**
     * @return string
     */
    public function getControllerParam()
    {
        return 'assortment';
    }

    /**
     * Add company
     *
     * @param Company $company
     *
     * @return Assortment
     */
    public function addCompany(Company $company)
    {
        $this->companies[] = $company;

        return $this;
    }

    /**
     * Remove company
     *
     * @param Company $company
     */
    public function removeCompany(Company $company)
    {
        $this->companies->removeElement($company);
    }

    /**
     * Get companies
     *
     * @return Collection|Company[]
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * Get owner
     *
     * @return Company
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set owner
     *
     * @param Company $owner
     *
     * @return Assortment
     */
    public function setOwner(Company $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Add assortment
     *
     * @param Assortment $assortment
     *
     * @return Assortment
     */
    public function addAssortment(Assortment $assortment)
    {
        $this->assortments[] = $assortment;

        return $this;
    }

    /**
     * Remove assortment
     *
     * @param Assortment $assortment
     */
    public function removeAssortment(Assortment $assortment)
    {
        $this->assortments->removeElement($assortment);
    }

    /**
     * Get assortments
     *
     * @return Collection|Assortment[]
     */
    public function getAssortments()
    {
        return $this->assortments;
    }

    /**
     * Get cardAssortment
     *
     * @return Assortment
     */
    public function getCardAssortment()
    {
        return $this->cardAssortment;
    }

    /**
     * Set cardAssortment
     *
     * @param Assortment $cardAssortment
     *
     * @return Assortment
     */
    public function setCardAssortment(Assortment $cardAssortment = null)
    {
        $this->cardAssortment = $cardAssortment;

        return $this;
    }

    /**
     * Set contentSize
     *
     * @param string $contentSize
     *
     * @return Assortment
     */
    public function setContentSize($contentSize)
    {
        $this->contentSize = $contentSize;

        return $this;
    }

    /**
     * Get contentSize
     *
     * @return string
     */
    public function getContentSize()
    {
        return $this->contentSize;
    }

    /**
     * Set contentPosition
     *
     * @param integer $contentPosition
     *
     * @return Assortment
     */
    public function setContentPosition($contentPosition)
    {
        $this->contentPosition = $contentPosition;

        return $this;
    }

    /**
     * Get contentPosition
     *
     * @return integer
     */
    public function getContentPosition()
    {
        return $this->contentPosition;
    }

    /**
     * Set content
     *
     * @param Page $content
     *
     * @return Assortment
     */
    public function setContent(Page $content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param null|string $image
     * @return Assortment
     */
    public function setImage(?string $image): Assortment
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get content
     *
     * @return Page
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getPathColumn()
    {
        return 'image';
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->{$this->getPathColumn()};
    }

    /**
     * @param $path
     * @return $this|mixed
     */
    public function setPath($path)
    {
        $this->{$this->getPathColumn()} = $path;

        return $this;
    }

    /**
     * @return mixed
     */
    public function generateName()
    {
        return Urlizer::urlize($this->getName());
    }

    /**
     * @return string
     */
    public function getStoragePath()
    {
        return 'images/assortment/' . $this->getId();
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     */
    public function setPosition(?int $position): void
    {
        $this->position = $position;
    }

    /**
     * @return bool|null
     */
    public function isShowFilter(): ?bool
    {
        return $this->showFilter;
    }

    /**
     * @param bool|null $showFilter
     */
    public function setShowFilter(?bool $showFilter): void
    {
        $this->showFilter = $showFilter;
    }

    /**
     * @return string
     */
    public function getMenuType(): string
    {
        return 'Assortment';
    }
}
