<?php

namespace AppBundle\Entity\Catalog\Assortment;

use AppBundle\Interfaces\Seo\SlugInterface;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 * @ORM\Cache(usage="READ_ONLY", region="region_entity")
 *
 */
class AssortmentTranslation implements SlugInterface
{

    use ORMBehaviors\Translatable\Translation;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     *
     */
    protected $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=128, unique=true, nullable=true)
     *
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=120, nullable=true)
     *
     */
    protected $homepageSlogan;

    /**
     * Set title
     *
     * @param null|string $title
     *
     * @return AssortmentTranslation
     */
    public function setTitle(?string $title): AssortmentTranslation
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param null|string $slug
     *
     * @return AssortmentTranslation
     */
    public function setSlug($slug): AssortmentTranslation
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param null|string $homepageSlogan
     *
     * @return AssortmentTranslation
     */
    public function setHomepageSlogan(?string $homepageSlogan): AssortmentTranslation
    {
        $this->homepageSlogan = $homepageSlogan;

        return $this;
    }

    /**
     * Get slug
     *
     * @return null|string
     */
    public function getHomepageSlogan(): ?string
    {
        return $this->homepageSlogan;
    }
}
