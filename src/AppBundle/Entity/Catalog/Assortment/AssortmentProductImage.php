<?php

namespace AppBundle\Entity\Catalog\Assortment;

use AppBundle\Entity\Catalog\Product\ProductImage;
use AppBundle\Interfaces\ImageInterface;
use AppBundle\Traits\ImageEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @ORM\Table("assortment_product_image");
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 */
class AssortmentProductImage implements ImageInterface
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    use ImageEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Assortment\AssortmentProduct", inversedBy="assortmentProductImages")
     * @ORM\JoinColumn(nullable=false);
     *
     */
    protected $assortmentProduct;

    /**
     * @ORM\Column(type="text")
     *
     */
    protected $path;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     *
     */
    protected $description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    protected $main;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    protected $category;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     *
     */
    private $position;

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    public function generateName()
    {
        $name = $this->getAssortmentProduct()->translate("nl_NL")->getSlug();

        return $name;
    }

    public function getPathColumn()
    {
        return 'path';
    }

    public function getStoragePath()
    {
        return "images/assortment_product/" . $this->getAssortmentProduct()->getId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return ProductImage
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return AssortmentProductImage
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set main
     *
     * @param boolean $main
     *
     * @return AssortmentProductImage
     */
    public function setMain($main)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main
     *
     * @return boolean
     */
    public function getMain()
    {
        return $this->main;
    }

    /**
     * Set category
     *
     * @param boolean $category
     *
     * @return AssortmentProductImage
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return boolean
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return AssortmentProductImage
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set assortmentProduct
     *
     * @param AssortmentProduct $assortmentProduct
     *
     * @return AssortmentProductImage
     */
    public function setAssortmentProduct(AssortmentProduct $assortmentProduct)
    {
        $this->assortmentProduct = $assortmentProduct;

        return $this;
    }

    /**
     * Get assortmentProduct
     *
     * @return AssortmentProduct
     */
    public function getAssortmentProduct()
    {
        return $this->assortmentProduct;
    }
}
