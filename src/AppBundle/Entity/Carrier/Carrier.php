<?php

namespace AppBundle\Entity\Carrier;

use AppBundle\Entity\Relation\Company;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Carrier
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="carrier")
     * @Assert\NotNull(message="Bedrijf is verplicht bij een transporteur")
     */
    private $company;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Carrier\DeliveryMethod", inversedBy="carriers")
     * @ORM\JoinTable(
     *     name="carrier_delivery_method",
     *     joinColumns={
     *          @ORM\JoinColumn(name="carrier_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(name="delivery_method_id", referencedColumnName="id")
     *     }
     * )
     */
    private $deliveryMethods;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\Length(min=1, max=20, minMessage="Naam mag niet leeg zijn", maxMessage="Naam maximum lengte: {{ limit }}")
     */
    protected $shortName;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->shortName = '';
        $this->deliveryMethods = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getShortName();
    }

    /**
     * Get id
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Add deliveryMethod
     *
     * @param DeliveryMethod $deliveryMethod
     *
     * @return Carrier
     */
    public function addDeliveryMethod(DeliveryMethod $deliveryMethod): Carrier
    {
        if (!$this->deliveryMethods->contains($deliveryMethod)) {
            $this->deliveryMethods->add($deliveryMethod);
            $deliveryMethod->addCarrier($this);
        }

        return $this;
    }

    /**
     * Remove deliveryMethod
     *
     * @param DeliveryMethod $deliveryMethod
     * @return Carrier
     */
    public function removeDeliveryMethod(DeliveryMethod $deliveryMethod): Carrier
    {
        if ($this->deliveryMethods->contains($deliveryMethod)) {
            $this->deliveryMethods->removeElement($deliveryMethod);
            $deliveryMethod->removeCarrier($this);
        }

        return $this;
    }

    /**
     * @ORM\PreRemove()
     * @return Carrier
     */
    public function removeAllDeliveryMethods(): Carrier
    {
        foreach($this->deliveryMethods as $deliveryMethod){
            $this->deliveryMethods->removeElement($deliveryMethod);
            $deliveryMethod->removeCarrier($this);
        }
        return $this;
    }

    /**
     * Get deliveryMethods
     *
     * @return Collection
     */
    public function getDeliveryMethods(): Collection
    {
        return $this->deliveryMethods;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return Carrier
     */
    public function setCompany(Company $company): Carrier
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company|null
     */
    public function getCompany(): ?Company
    {
        return $this->company;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     *
     * @return Carrier
     */
    public function setShortName(string $shortName): Carrier
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return string
     */
    public function getShortName(): string
    {
        return $this->shortName;
    }
}
