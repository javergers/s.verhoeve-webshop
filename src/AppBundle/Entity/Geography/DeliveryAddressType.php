<?php

namespace AppBundle\Entity\Geography;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Site\Site;
use AppBundle\Entity\Geography\DeliveryAddressTypeField;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 *
 */
class DeliveryAddressType
{
    use ORMBehaviors\Translatable\Translatable;
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $hideCompanyField;

    /**
     * @ORM\Id
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Site\Site", inversedBy="deliveryAddressTypes")
     */
    protected $sites;

    /**
     * @ORM\Id
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Catalog\Assortment\Assortment")
     * @ORM\JoinTable(name="delivery_address_type_excluded_assortment",
     *      joinColumns={@ORM\JoinColumn(name="delivery_address_type_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="assortment_id", referencedColumnName="id")}
     * )
     */
    protected $excludedAssortments;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Geography\DeliveryAddressTypeField", mappedBy="deliveryAddressType",
     *                                                                                                        cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     */
    protected $fields;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hideCompanyField
     *
     * @param boolean $hideCompanyField
     *
     * @return DeliveryAddressType
     */
    public function setHideCompanyField($hideCompanyField)
    {
        $this->hideCompanyField = $hideCompanyField;

        return $this;
    }

    /**
     * Get hideCompanyField
     *
     * @return boolean
     */
    public function getHideCompanyField()
    {
        return $this->hideCompanyField;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fields = new ArrayCollection();
    }

    /**
     * Add field
     *
     * @param DeliveryAddressTypeField $field
     *
     * @return DeliveryAddressType
     */
    public function addField(DeliveryAddressTypeField $field)
    {
        $field->setDeliveryAddressType($this);

        $this->fields[] = $field;

        return $this;
    }

    /**
     * Remove field
     *
     * @param DeliveryAddressTypeField $field
     */
    public function removeField(DeliveryAddressTypeField $field)
    {
        $this->fields->removeElement($field);
    }

    /**
     * Get fields
     *
     * @return Collection
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Add site
     *
     * @param Site $site
     *
     * @return DeliveryAddressType
     */
    public function addSite(Site $site)
    {
        $this->sites[] = $site;

        return $this;
    }

    /**
     * Remove site
     *
     * @param Site $site
     */
    public function removeSite(Site $site)
    {
        $this->sites->removeElement($site);
    }

    /**
     * Get sites
     *
     * @return Collection
     */
    public function getSites()
    {
        return $this->sites;
    }

    /**
     * Add excludedAssortment
     *
     * @param Assortment $excludedAssortment
     *
     * @return DeliveryAddressType
     */
    public function addExcludedAssortment(Assortment $excludedAssortment)
    {
        $this->excludedAssortments[] = $excludedAssortment;

        return $this;
    }

    /**
     * Remove excludedAssortment
     *
     * @param Assortment $excludedAssortment
     */
    public function removeExcludedAssortment(Assortment $excludedAssortment)
    {
        $this->excludedAssortments->removeElement($excludedAssortment);
    }

    /**
     * Get excludedAssortments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExcludedAssortments()
    {
        return $this->excludedAssortments;
    }
}
