<?php

namespace AppBundle\Entity\Geography;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 * @ORM\Table(uniqueConstraints={
 *     @ORM\UniqueConstraint(name="date", columns={"date"})
 * })
 *
 * @method HolidayTranslation translate(string $locale = null, boolean $fallbackToDefault = null)
 */
class Holiday
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Geography\HolidayCountry", mappedBy="holiday", cascade={"persist"})
     */
    protected $countries;

    /**
     * @ORM\Column(type="date")
     */
    protected $date;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->countries = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Holiday
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Add country
     *
     * @param HolidayCountry $country
     *
     * @return Holiday
     */
    public function addCountry(HolidayCountry $country)
    {
        $this->countries[] = $country;

        return $this;
    }

    /**
     * Remove country
     *
     * @param HolidayCountry $country
     */
    public function removeCountry(HolidayCountry $country)
    {
        $this->countries->removeElement($country);
    }

    /**
     * Get countries
     *
     * @return Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }
}
