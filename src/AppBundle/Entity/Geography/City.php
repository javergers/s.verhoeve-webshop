<?php

namespace AppBundle\Entity\Geography;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model\Translatable\Translatable;

/**
 * @ORM\Entity
 * *
 * @method CityTranslation translate(string $locale = null, boolean $fallbackToDefault = null)
 */
class City
{
    use Translatable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\Province", inversedBy="cities")
     */
    private $province;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Geography\Postcode", inversedBy="cities")
     */
    private $postcodes;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->translate()->getName();
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->postcodes = new ArrayCollection();
    }

    /**
     * Add postcode
     *
     * @param Postcode $postcode
     *
     * @return City
     */
    public function addPostcode(Postcode $postcode)
    {
        $this->postcodes[] = $postcode;

        return $this;
    }

    /**
     * Remove postcode
     *
     * @param Postcode $postcode
     */
    public function removePostcode(Postcode $postcode)
    {
        $this->postcodes->removeElement($postcode);
    }

    /**
     * Get postcodes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPostcodes()
    {
        return $this->postcodes;
    }

    /**
     * Set province
     *
     * @param Province $province
     *
     * @return City
     */
    public function setProvince(Province $province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return Province
     */
    public function getProvince()
    {
        return $this->province;
    }
}
