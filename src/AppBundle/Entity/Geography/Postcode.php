<?php

namespace AppBundle\Entity\Geography;

use AppBundle\Entity\Supplier\DeliveryArea;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class Postcode
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Geography\PostcodeRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @package AppBundle\Entity
 */
class Postcode
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\Province")
     * @ORM\JoinColumn(nullable=false)
     *
     * @var Province
     */
    private $province;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Geography\City", mappedBy="postcodes")
     */
    protected $cities;

    /**
     * @ORM\Column(type="string", length=5)
     */
    protected $postcode;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Geography\PostcodeGeometry")
     */
    protected $geometry;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Supplier\DeliveryArea", mappedBy="postcode", cascade={"persist"})
     */
    protected $deliveryAreas;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return Postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cities = new ArrayCollection();
        $this->deliveryAreas = new ArrayCollection();
    }

    /**
     * Constructor
     */
    public function __toString()
    {
        return (string)$this->postcode;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        if ($this->getCities() && $this->getCities()->first()) {
            return $this->getPostcode() . ' ' . $this->getCities()->first();
        }
        return $this->getPostcode();
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->province->getCountry();
    }

    /**
     * Set province
     *
     * @param Province $province
     *
     * @return Postcode
     */
    public function setProvince(Province $province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return Province
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Add city
     *
     * @param City $city
     *
     * @return Postcode
     */
    public function addCity(City $city)
    {
        $this->cities[] = $city;

        return $this;
    }

    /**
     * Remove city
     *
     * @param City $city
     */
    public function removeCity(City $city)
    {
        $this->cities->removeElement($city);
    }

    /**
     * Get cities
     *
     * @return Collection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * Set geometry
     *
     * @param PostcodeGeometry $geometry
     *
     * @return Postcode
     */
    public function setGeometry(PostcodeGeometry $geometry = null)
    {
        $this->geometry = $geometry;

        return $this;
    }

    /**
     * Get geometry
     *
     * @return PostcodeGeometry
     */
    public function getGeometry()
    {
        return $this->geometry;
    }


    /**
     * Add deliveryArea
     *
     * @param DeliveryArea $deliveryArea
     *
     * @return Postcode
     */
    public function addDeliveryArea(DeliveryArea $deliveryArea)
    {

        $deliveryArea->setPostcode($this);

        $this->deliveryAreas[] = $deliveryArea;

        return $this;
    }

    /**
     * @param ArrayCollection $deliveryAreas
     *
     * @return $this
     */
    public function setDeliveryAreas(ArrayCollection $deliveryAreas)
    {
        $this->deliveryAreas = $deliveryAreas;

        return $this;
    }

    /**
     * Remove deliveryArea
     *
     * @param DeliveryArea $deliveryArea
     */
    public function removeDeliveryArea(DeliveryArea $deliveryArea)
    {
        $this->deliveryAreas->removeElement($deliveryArea);
    }

    /**
     * Get deliveryAreas
     *
     * @return ArrayCollection|DeliveryArea[]
     */
    public function getDeliveryAreas()
    {
        return $this->deliveryAreas;
    }
}
