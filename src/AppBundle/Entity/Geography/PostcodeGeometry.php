<?php

namespace AppBundle\Entity\Geography;

use CrEOF\Spatial\PHP\Types\Geometry\MultiPolygon;
use CrEOF\Spatial\PHP\Types\Geometry\Point;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 */
class PostcodeGeometry
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="point", nullable=true)
     */
    protected $point;

    /**
     * @ORM\Column(type="multipolygon", nullable=true)
     */
    protected $multipolygon;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set point
     *
     * @param Point $point
     *
     * @return PostcodeGeometry
     */
    public function setPoint($point)
    {
        $this->point = $point;

        return $this;
    }

    /**
     * Get point
     *
     * @return Point
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Set multipolygon
     *
     * @param MultiPolygon $multipolygon
     *
     * @return PostcodeGeometry
     */
    public function setMultipolygon($multipolygon)
    {
        $this->multipolygon = $multipolygon;

        return $this;
    }

    /**
     * Get multipolygon
     *
     * @return MultiPolygon
     */
    public function getMultipolygon()
    {
        return $this->multipolygon;
    }
}
