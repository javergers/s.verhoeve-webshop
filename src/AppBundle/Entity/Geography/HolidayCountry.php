<?php

namespace AppBundle\Entity\Geography;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class HolidayCountry
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\Holiday", inversedBy="countries")
     */
    protected $holiday;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\Country")
     */
    protected $country;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set holiday
     *
     * @param Holiday $holiday
     *
     * @return HolidayCountry
     */
    public function setHoliday(Holiday $holiday = null)
    {
        $this->holiday = $holiday;

        return $this;
    }

    /**
     * Get holiday
     *
     * @return \AppBundle\Entity\Geography\Holiday
     */
    public function getHoliday()
    {
        return $this->holiday;
    }

    /**
     * Set country
     *
     * @param Country $country
     *
     * @return HolidayCountry
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }
}
