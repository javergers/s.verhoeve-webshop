<?php

namespace AppBundle\Entity\Finance\Tax;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class VatRate
 * @package AppBundle\Entity\Finance\Tax
 * @ORM\Entity()
 */
class VatRate
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=10)
     */
    protected $code;

    /**
     * @var VatGroup
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Finance\Tax\VatGroup", inversedBy="rates")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $group;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $purchase;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $validFrom;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $validUntil;

    /**
     * @var float
     * @ORM\Column(type="float", precision=2)
     */
    protected $factor;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return VatRate
     */
    public function setCode(string $code): VatRate
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return VatGroup
     */
    public function getGroup(): ?VatGroup
    {
        return $this->group;
    }

    /**
     * @param VatGroup $group
     * @return VatRate
     */
    public function setGroup(VatGroup $group): VatRate
    {
        $this->group = $group;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPurchase(): ?bool
    {
        return $this->purchase;
    }

    /**
     * @param null|bool $purchase
     * @return VatRate
     */
    public function setPurchase(?bool $purchase): VatRate
    {
        $this->purchase = $purchase;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return VatRate
     */
    public function setTitle(string $title): VatRate
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getValidFrom(): ?\DateTime
    {
        return $this->validFrom;
    }

    /**
     * @param \DateTime $validFrom
     * @return VatRate
     */
    public function setValidFrom(\DateTime $validFrom): VatRate
    {
        $this->validFrom = $validFrom;

        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getValidUntil(): ?\DateTime
    {
        return $this->validUntil;
    }

    /**
     * @param \DateTime $validUntil
     * @return VatRate
     */
    public function setValidUntil(\DateTime $validUntil): VatRate
    {
        $this->validUntil = $validUntil;

        return $this;
    }

    /**
     * @return null|float
     */
    public function getFactor(): ?float
    {
        return $this->factor;
    }

    /**
     * @param float $factor
     * @return VatRate
     */
    public function setFactor(float $factor): VatRate
    {
        $this->factor = $factor;

        return $this;
    }
}