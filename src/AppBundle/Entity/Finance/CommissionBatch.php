<?php

namespace AppBundle\Entity\Finance;

use AppBundle\Entity\Relation\Company;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity()
 */
class CommissionBatch
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="CommissionInvoice", mappedBy="commissionBatch", cascade={"persist"})
     */
    protected $commissionInvoices;

    /**
     * @var Company[]|Collection
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Relation\Company")
     */
    protected $suppliers;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $invoiceDate;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->commissionInvoices = new ArrayCollection();
        $this->suppliers = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getCompaniesCount()
    {
        // Really querying the companies would have performance degradation
        // A company can only occurs once in a batch, so the result is the same.

        return $this->commissionInvoices->count();
    }

    /**
     * @return int
     */
    public function getCommissionInvoicesCount()
    {
        return $this->commissionInvoices->count();
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return CommissionBatch
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->id;
    }

    /**
     * Add commissionInvoice
     *
     * @param CommissionInvoice $commissionInvoice
     *
     * @return CommissionBatch
     */
    public function addCommissionInvoice(CommissionInvoice $commissionInvoice)
    {
        $this->commissionInvoices[] = $commissionInvoice;

        return $this;
    }

    /**
     * Remove commissionInvoice
     *
     * @param CommissionInvoice $commissionInvoice
     */
    public function removeCommissionInvoice(CommissionInvoice $commissionInvoice)
    {
        $this->commissionInvoices->removeElement($commissionInvoice);
    }

    /**
     * Get commissionInvoices
     *
     * @return Collection|CommissionInvoice[]
     */
    public function getCommissionInvoices()
    {
        return $this->commissionInvoices;
    }

    /**
     * @return Company[]|Collection
     */
    public function getSuppliers(): Collection
    {
        return $this->suppliers;
    }

    /**
     * @param Company|Collection $suppliers
     */
    public function setSuppliers($suppliers): void
    {
        $this->suppliers = $suppliers;
    }

    /**
     * @param Company $supplier
     */
    public function addSupplier(Company $supplier): void
    {
        if (!$this->suppliers->contains($supplier)) {
            $this->suppliers->add($supplier);
        }
    }

    /**
     * @param Company $supplier
     */
    public function removeSupplier(Company $supplier): void
    {
        if ($this->suppliers->contains($supplier)) {
            $this->suppliers->removeElement($supplier);
        }
    }

    /**
     * @return \DateTime|null
     */
    public function getInvoiceDate(): ?\DateTime
    {
        return $this->invoiceDate;
    }

    /**
     * @param \DateTime|null $invoiceDate
     */
    public function setInvoiceDate(?\DateTime $invoiceDate): void
    {
        $this->invoiceDate = $invoiceDate;
    }
}
