<?php

namespace AppBundle\Entity\Finance;

use AppBundle\Entity\Catalog\Product\Vat;
use AppBundle\Entity\Finance\Tax\VatRate;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class CommissionInvoiceLine
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CommissionInvoice", inversedBy="lines")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $commissionInvoice;

    /**
     * @ORM\Column(type="text", length=100)
     */
    protected $description;

    /**
     * @ORM\Column(type="float")
     */
    protected $price;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Finance\Tax\VatRate")
     */
    protected $vatRate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return CommissionInvoiceLine
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return CommissionInvoiceLine
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getVatPrice()
    {
        return round($this->price / $this->vatRate->getFactor(), 2);
    }

    /**
     * @return float
     */
    public function getPriceIncl()
    {
        return $this->price + $this->getVatPrice();
    }

    /**
     * Set commissionInvoice
     *
     * @param CommissionInvoice $commissionInvoice
     *
     * @return CommissionInvoiceLine
     */
    public function setCommissionInvoice(CommissionInvoice $commissionInvoice = null)
    {
        $this->commissionInvoice = $commissionInvoice;

        return $this;
    }

    /**
     * Get commissionInvoice
     *
     * @return CommissionInvoice
     */
    public function getCommissionInvoice()
    {
        return $this->commissionInvoice;
    }

    /**
     * Set vat
     *
     * @param VatRate $vatRate
     *
     * @return CommissionInvoiceLine
     */
    public function setVatRate(VatRate $vatRate = null)
    {
        $this->vatRate = $vatRate;

        return $this;
    }

    /**
     * Get vat
     *
     * @return Vat
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }
}
