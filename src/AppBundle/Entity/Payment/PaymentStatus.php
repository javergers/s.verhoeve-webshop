<?php

namespace AppBundle\Entity\Payment;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use RuleBundle\Annotation as Rule;

/**
 * @ORM\Entity
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Rule\Entity(description="Betaalstatus")
 */
class PaymentStatus
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=25)
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=12)
     */
    protected $class;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $countable;

    /**
     * Set id
     *
     * @param string $id
     *
     * @return PaymentStatus
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return PaymentStatus
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set class
     *
     * @param string $class
     *
     * @return PaymentStatus
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * Get class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set countable
     *
     * @param boolean $countable
     *
     * @return PaymentStatus
     */
    public function setCountable($countable)
    {
        $this->countable = $countable;

        return $this;
    }

    /**
     * Get countable
     *
     * @return boolean
     */
    public function getCountable()
    {
        return $this->countable;
    }
}
