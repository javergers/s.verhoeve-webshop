<?php

namespace AppBundle\Entity\Report;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CompanyReportCustomerHistoryRepository")
 * Class CompanyReportCustomerHistory
 * @package AppBundle\Entity\Report
 */
class CompanyReportCustomerHistory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Report\CompanyReportCustomer", inversedBy="histories")
     */
    protected $companyReportCustomer;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $fromDate;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $tillDate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyReportCustomer
     *
     * @param CompanyReportCustomer $companyReportCustomer
     *
     * @return CompanyReportCustomerHistory
     */
    public function setCompanyReportCustomer(CompanyReportCustomer $companyReportCustomer = null)
    {
        $this->companyReportCustomer = $companyReportCustomer;

        return $this;
    }

    /**
     * Get companyReportCustomer
     *
     * @return CompanyReportCustomer
     */
    public function getCompanyReportCustomer()
    {
        return $this->companyReportCustomer;
    }

    /**
     * Set fromDate
     *
     * @param \DateTime $fromDate
     *
     * @return CompanyReportCustomerHistory
     */
    public function setFromDate(\DateTime $fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate
     *
     * @return \DateTime
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set tillDate
     *
     * @param \DateTime $tillDate
     *
     * @return CompanyReportCustomerHistory
     */
    public function setTillDate(\DateTime $tillDate)
    {
        $this->tillDate = $tillDate;

        return $this;
    }

    /**
     * Get tillDate
     *
     * @return \DateTime
     */
    public function getTillDate()
    {
        return $this->tillDate;
    }
}
