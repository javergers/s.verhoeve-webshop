<?php

namespace AppBundle\Entity\Report;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class ReportChartData
 * @package AppBundle\Entity\Report
 * @ORM\EntityListeners({"AppBundle\EventListener\Report\ReportChartDataListener"})
 * @ORM\Entity()
 */
class ReportChartData {

    use TimestampableEntity;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Report\ReportChart", inversedBy="reportChartData")
     */
    protected $reportChart;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $fromDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $tillDate;

    /**
     * @ORM\Column(type="string")
     */
    protected $sortByPath;

    /**
     * @ORM\Column(type="string")
     */
    protected $dataPath;

    /**
     * @ORM\Column(type="string")
     */
    protected $dataLabel;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $data;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fromDate
     *
     * @param \DateTime $fromDate
     *
     * @return ReportChartData
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate
     *
     * @return \DateTime
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set tillDate
     *
     * @param \DateTime $tillDate
     *
     * @return ReportChartData
     */
    public function setTillDate($tillDate)
    {
        $this->tillDate = $tillDate;

        return $this;
    }

    /**
     * Get tillDate
     *
     * @return \DateTime
     */
    public function getTillDate()
    {
        return $this->tillDate;
    }

    /**
     * Set sortByPath
     *
     * @param string $sortByPath
     *
     * @return ReportChartData
     */
    public function setSortByPath($sortByPath)
    {
        $this->sortByPath = $sortByPath;

        return $this;
    }

    /**
     * Get sortByPath
     *
     * @return string
     */
    public function getSortByPath()
    {
        return $this->sortByPath;
    }

    /**
     * Set dataPath
     *
     * @param string $dataPath
     *
     * @return ReportChartData
     */
    public function setDataPath($dataPath)
    {
        $this->dataPath = $dataPath;

        return $this;
    }

    /**
     * Get dataPath
     *
     * @return string
     */
    public function getDataPath()
    {
        return $this->dataPath;
    }

    /**
     * Set data
     *
     * @param array $data
     *
     * @return ReportChartData
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set reportChart
     *
     * @param ReportChart $reportChart
     *
     * @return ReportChartData
     */
    public function setReportChart(ReportChart $reportChart = null)
    {
        $this->reportChart = $reportChart;

        return $this;
    }

    /**
     * Get reportChart
     *
     * @return ReportChart
     */
    public function getReportChart()
    {
        return $this->reportChart;
    }

    /**
     * Set dataLabel
     *
     * @param string $dataLabel
     *
     * @return ReportChartData
     */
    public function setDataLabel($dataLabel)
    {
        $this->dataLabel = $dataLabel;

        return $this;
    }

    /**
     * Get dataLabel
     *
     * @return string
     */
    public function getDataLabel()
    {
        return $this->dataLabel;
    }
}
