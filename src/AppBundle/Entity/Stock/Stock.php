<?php

namespace AppBundle\Entity\Stock;

use AppBundle\Entity\Catalog\Product\Product;
use Doctrine\ORM\Mapping as ORM;
use RuleBundle\Annotation as Rule;
use FS\SolrBundle\Doctrine\Annotation\Id;

/**
 * @ORM\Entity()
 * @Rule\Entity(description="Product")
 * @ORM\Table("product_stock")
 */
class Stock
{
    /** @var string */
    public const STATUS_SUCCESS = 'success';

    /** @var string  */
    public const STATUS_WARNING = 'warning';

    /** @var string  */
    public const STATUS_DANGER = 'danger';

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $product;

    /**
     * @ORM\Column(type="integer")
     * @Rule\Property(description="Actuele voorraad")
     */
    protected $physicalStock;

    /**
     * @ORM\Column(type="integer")
     */
    protected $threshold;

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return Stock
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set physicalStock
     *
     * @param integer $physicalStock
     *
     * @return Stock
     */
    public function setPhysicalStock($physicalStock)
    {
        $this->physicalStock = $physicalStock;

        return $this;
    }

    /**
     * Get physicalStock
     *
     * @return integer
     */
    public function getPhysicalStock()
    {
        return $this->physicalStock;
    }

    /**
     * Set threshold
     *
     * @param integer $threshold
     *
     * @return Stock
     */
    public function setThreshold($threshold)
    {
        $this->threshold = $threshold;

        return $this;
    }

    /**
     * Get threshold
     *
     * @return integer
     */
    public function getThreshold()
    {
        return $this->threshold;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        if($this->getPhysicalStock() === 0) {
            return self::STATUS_DANGER;
        }

        if($this->getPhysicalStock() < $this->getThreshold()) {
            return self::STATUS_WARNING;
        }

        return self::STATUS_SUCCESS;
    }
}
