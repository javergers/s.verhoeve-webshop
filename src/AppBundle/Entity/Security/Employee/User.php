<?php

namespace AppBundle\Entity\Security\Employee;

use AppBundle\Entity\Relation\CompanyInternalRemark;
use AppBundle\Model\UserModel;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use RuleBundle\Annotation as Rule;
use Scheb\TwoFactorBundle\Model\Google\TwoFactorInterface;
use Serializable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="adiuvo_user")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @UniqueEntity(fields={"email"})
 * @UniqueEntity(fields={"username"})
 * @Rule\Entity(description="Medewerker")
 */
class User extends UserModel implements UserInterface, Serializable, TwoFactorInterface
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    public const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     *
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     *
     */
    private $lastname;

    /**
     *
     * @ORM\Column(type="string", nullable=true, unique=true)
     * @Rule\Property(description="Gebruikersnaam")
     */
    protected $username;

    /**
     *
     * @ORM\Column(type="string", nullable=true, unique=true)
     * @Rule\Property(description="Emailadres")
     */
    protected $email;

    /**
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $voipExtension;

    /**
     *
     * @ORM\Column(type="boolean")
     *
     */
    protected $enabled;

    /**
     * The salt to use for hashing
     *
     *
     * @ORM\Column(type="string")
     */
    protected $salt;

    /**
     * Encrypted password. Must be persisted.
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $password;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     */
    protected $plainPassword;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lastLogin;

    /**
     * Random string sent to the user email address in order to verify it
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $confirmationToken;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $passwordRequestedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $locked;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $expired;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $expiresAt;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Security\Employee\Group", inversedBy="users", cascade={"persist"})
     * @ORM\JoinTable(name="adiuvo_user_group_user",
     *      joinColumns={@ORM\JoinColumn(name="adiuvo_user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="adiuvo_user_group_id", referencedColumnName="id")}
     * )
     * @Rule\Property(description="Group", autoComplete={"id"="group.id", "label"="group.name"}, relation="child")
     */
    protected $groups;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Security\Employee\Role", inversedBy="users", cascade={"persist"})
     * @ORM\JoinTable(name="role_roles_users",
     *     joinColumns={@ORM\JoinColumn(name="adiuvo_user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     *     )
     * @var ArrayCollection $roles
     */
    protected $roles;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $credentialsExpired;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $credentialsExpireAt;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $googleAuthenticatorSecret;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Relation\CompanyInternalRemark", mappedBy="user")
     */
    protected $companyInternalRemarks;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getEmail();
    }

    public function __construct()
    {
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $this->enabled = false;
        $this->locked = false;
        $this->expired = false;
        $this->roles = new ArrayCollection();
        $this->companyInternalRemarks = new ArrayCollection();
        $this->credentialsExpired = false;
    }

    /**
     * Serializes the user.
     *
     * The serialized data have to contain the fields used by the equals method and the username.
     *
     * @return string
     */
    public function serialize(): string
    {
        return serialize([
            $this->password,
            $this->salt,
            $this->username,
            $this->expired,
            $this->locked,
            $this->credentialsExpired,
            $this->enabled,
            $this->id,
        ]);
    }

    /**
     * Unserializes the user.
     *
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized, ['allowed_classes' => false]);
        $data = array_merge($data, array_fill(0, 2, null));

        [
            $this->password,
            $this->salt,
            $this->username,
            $this->expired,
            $this->locked,
            $this->credentialsExpired,
            $this->enabled,
            $this->id,
        ] = $data;
    }

    /**
     * Removes sensitive data from the user.
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * Returns the user unique id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @return null|string
     */
    public function getSalt(): ?string
    {
        return $this->salt;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return null|string
     */
    public function getVoipExtension():?string
    {
        return $this->voipExtension;
    }

    /**
     * Gets the encrypted password.
     *
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @return string|null
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * Gets the last login time.
     *
     * @return DateTime|null
     */
    public function getLastLogin(): ?DateTime
    {
        return $this->lastLogin;
    }

    /**
     * @return null|string
     */
    public function getConfirmationToken():?string
    {
        return $this->confirmationToken;
    }

    /**
     * Returns the user roles
     *
     * @param bool $withGroups
     * @param bool $asObject
     * @return array|object The roles
     */
    public function getRoles($withGroups = true, $asObject = false)
    {
        $roles = [];
        if ($withGroups) {
            foreach ($this->getGroups() as $group) {
                foreach ($group->getRoles() as $role) {
                    if (!$this->roles->contains($role)) {
                        $this->roles->add($role);
                    }
                }
            }
        }

        if (!$asObject) {
            foreach ($this->roles as $role) {
                $roles[] = $role->getName();
            }
        } else {
            $roles = $this->roles;
        }

        return $roles;
    }

    /**
     * Gets the groups granted to the user.
     *
     * @return Group[]|ArrayCollection
     */
    public function getGroups(): Collection
    {
        return $this->groups ?: $this->groups = new ArrayCollection();
    }

    /**
     * @return array
     */
    public function getGroupNames(): array
    {
        $names = [];
        foreach ($this->getGroups() as $group) {
            $names[] = $group->getName();
        }

        return $names;
    }

    /**
     * @param string $name
     *
     * @return boolean
     */
    public function hasGroup($name): bool
    {
        return in_array($name, $this->getGroupNames(), true);
    }

    /**
     * @param Group $group
     * @return $this
     */
    public function addGroup(Group $group): self
    {
        if (!$this->getGroups()->contains($group)) {
            $this->getGroups()->add($group);
        }

        return $this;
    }

    /**
     * @param Group $group
     * @return $this
     */
    public function removeGroup(Group $group): self
    {
        if ($this->getGroups()->contains($group)) {
            $this->getGroups()->removeElement($group);
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isAccountNonExpired(): bool
    {
        if (true === $this->expired) {
            return false;
        }

        if (null !== $this->expiresAt && $this->expiresAt->getTimestamp() < time()) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isAccountNonLocked(): bool
    {
        return !$this->locked;
    }

    /**
     * @return bool
     */
    public function isCredentialsNonExpired(): bool
    {
        if (true === $this->credentialsExpired) {
            return false;
        }

        if (null !== $this->credentialsExpireAt && $this->credentialsExpireAt->getTimestamp() < time()) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isCredentialsExpired(): bool
    {
        return !$this->isCredentialsNonExpired();
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        return !$this->isAccountNonExpired();
    }

    /**
     * @return bool
     */
    public function isLocked(): bool
    {
        return !$this->isAccountNonLocked();
    }

    /**
     * @return mixed
     */
    public function isSuperAdmin()
    {
        return $this->hasRole('ROLE_SUPER_ADMIN');
    }

    /**
     * @param UserInterface|User|null $user
     * @return bool
     */
    public function isUser(UserInterface $user = null): bool
    {
        return null !== $user && $this->getId() === $user->getId();
    }

    /**
     * @param $username
     * @return $this
     */
    public function setUsername($username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @param DateTime $date
     *
     * @return User
     */
    public function setCredentialsExpireAt(DateTime $date): User
    {
        $this->credentialsExpireAt = $date;

        return $this;
    }

    /**
     * @param boolean $boolean
     *
     * @return User
     */
    public function setCredentialsExpired($boolean): User
    {
        $this->credentialsExpired = $boolean;

        return $this;
    }

    /**
     * @param $email
     * @return $this
     */
    public function setEmail($email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param $voipExtension
     * @return $this
     */
    public function setVoipExtension($voipExtension): self
    {
        $this->voipExtension = $voipExtension;

        return $this;
    }

    /**
     * @param $boolean
     * @return $this
     */
    public function setEnabled($boolean): self
    {
        $this->enabled = (bool)$boolean;

        return $this;
    }

    /**
     * Sets this user to expired.
     *
     * @param Boolean $boolean
     *
     * @return User
     */
    public function setExpired($boolean): User
    {
        $this->expired = (bool)$boolean;

        return $this;
    }

    /**
     * @param DateTime $date
     *
     * @return User
     */
    public function setExpiresAt(DateTime $date): User
    {
        $this->expiresAt = $date;

        return $this;
    }

    /**
     * @param $password
     * @return $this
     */
    public function setPassword($password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @param $password
     *
     * @return $this
     * @throws Exception
     */
    public function setPlainPassword($password): self
    {
        $this->setUpdatedAt(new DateTime());

        $this->setPassword(password_hash($password, PASSWORD_BCRYPT, [
            'costs' => 12,
        ]));

        return $this;
    }

    /**
     * @param DateTime $time
     * @return $this
     */
    public function setLastLogin(DateTime $time): self
    {
        $this->lastLogin = $time;

        return $this;
    }

    /**
     * @param $boolean
     * @return $this
     */
    public function setLocked($boolean): self
    {
        $this->locked = $boolean;

        return $this;
    }

    /**
     * @param $confirmationToken
     * @return $this
     */
    public function setConfirmationToken($confirmationToken): self
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * @param DateTime|null $date
     * @return $this
     */
    public function setPasswordRequestedAt(DateTime $date = null): self
    {
        $this->passwordRequestedAt = $date;

        return $this;
    }

    /**
     * Gets the timestamp that the user requested a password reset.
     *
     * @return null|DateTime
     */
    public function getPasswordRequestedAt(): ?DateTime
    {
        return $this->passwordRequestedAt;
    }

    /**
     * @param $ttl
     * @return bool
     */
    public function isPasswordRequestNonExpired($ttl): bool
    {
        return null !== $this->getPasswordRequestedAt() && $this->getPasswordRequestedAt() instanceof DateTime &&
            $this->getPasswordRequestedAt()->getTimestamp() + $ttl > time();
    }

    /**
     * Get first and lastname combined
     *
     * @return string
     */
    public function getDisplayName(): string
    {
        $displayName = trim($this->firstname . ' ' . $this->lastname);

        if (!$displayName) {
            $displayName = $this->username;
        }

        return $displayName;
    }

    /**
     * Set googleAuthenticatorSecret
     *
     * @param string $googleAuthenticatorSecret
     *
     * @return User
     */
    public function setGoogleAuthenticatorSecret($googleAuthenticatorSecret): User
    {
        $this->googleAuthenticatorSecret = $googleAuthenticatorSecret;

        return $this;
    }

    /**
     * Get googleAuthenticatorSecret
     *
     * @return string
     */
    public function getGoogleAuthenticatorSecret(): string
    {
        return $this->googleAuthenticatorSecret;
    }

    /**
     * @return string
     */
    public function getGoogleAuthenticatorUsername(): string
    {
        return $this->getUsername();
    }

    /**
     * @return bool
     */
    public function isGoogleAuthenticatorEnabled(): bool
    {
        return !empty($this->googleAuthenticatorSecret);
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname): User
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname): User
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname|null
     *
     * @return string
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt): User
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get locked
     *
     * @return boolean|null
     */
    public function getLocked(): ?bool
    {
        return $this->locked;
    }

    /**
     * Get expired
     *
     * @return boolean|null
     */
    public function getExpired(): ?bool
    {
        return $this->expired;
    }

    /**
     * Get expiresAt
     *
     * @return DateTime|null
     */
    public function getExpiresAt(): ?DateTime
    {
        return $this->expiresAt;
    }

    /**
     * Get credentialsExpired
     *
     * @return boolean|null
     */
    public function getCredentialsExpired(): ?bool
    {
        return $this->credentialsExpired;
    }

    /**
     * Get credentialsExpireAt
     *
     * @return DateTime|null
     */
    public function getCredentialsExpireAt(): ?DateTime
    {
        return $this->credentialsExpireAt;
    }

    /**
     * Add role
     *
     * @param Role $role
     *
     * @return User
     */
    public function addRole(Role $role): User
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * Remove role
     *
     * @param Role $role
     */
    public function removeRole(Role $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * Add companyInternalRemark
     *
     * @param CompanyInternalRemark $companyInternalRemark
     *
     * @return User
     */
    public function addCompanyInternalRemark(CompanyInternalRemark $companyInternalRemark): User
    {
        $this->companyInternalRemarks[] = $companyInternalRemark;

        return $this;
    }

    /**
     * Remove companyInternalRemark
     *
     * @param CompanyInternalRemark $companyInternalRemark
     */
    public function removeCompanyInternalRemark(CompanyInternalRemark $companyInternalRemark)
    {
        $this->companyInternalRemarks->removeElement($companyInternalRemark);
    }

    /**
     * Get companyInternalRemarks
     *
     * @return Collection|CompanyInternalRemark[]
     */
    public function getCompanyInternalRemarks()
    {
        return $this->companyInternalRemarks;
    }
}
