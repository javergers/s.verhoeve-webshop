<?php

namespace AppBundle\Entity\Security\Employee;

use AppBundle\Entity\Security\Employee\Role;
use AppBundle\Model\GroupModel;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use RuleBundle\Annotation as Rule;

/**
 * @ORM\Entity
 * @Rule\Entity(description="Groep")
 * @ORM\Table(name="adiuvo_user_group")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Group extends GroupModel
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Rule\Property(description="Naam")
     */
    protected $name;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Security\Employee\User", mappedBy="groups", cascade={"persist"})
     * @ORM\JoinTable(name="adiuvo_user_group_user",
     *      joinColumns={@ORM\JoinColumn(name="adiuvo_user_group_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="adiuvo_user_id", referencedColumnName="id")}
     * )
     * @Rule\Property(description="Medewerker", autoComplete={"id"="user.id", "label"="user.username"}, relation="parent")
     */
    protected $users;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Security\Employee\Role", inversedBy="groups")
     */
    protected $roles;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->roles = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Group
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add user
     *
     * @param User $user
     *
     * @return Group
     */
    public function addUser(User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param User $user
     */
    public function removeUser(User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add role
     *
     * @param Role $role
     *
     * @return Group
     */
    public function addRole(Role $role)
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * Remove role
     *
     * @param Role $role
     */
    public function removeRole(Role $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * Get roles
     *
     * @return ArrayCollection
     */
    public function getRoles()
    {
        return $this->roles;
    }
}
