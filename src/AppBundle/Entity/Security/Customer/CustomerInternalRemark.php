<?php

namespace AppBundle\Entity\Security\Customer;

use AppBundle\Entity\Security\Employee\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class CustomerInternalRemark
 * @ORM\Entity()
 */
class CustomerInternalRemark
{

    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $remark;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Employee\User", inversedBy="companyInternalRemarks")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Customer\Customer", inversedBy="customerInternalRemarks")
     */
    protected $customer;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set remark
     *
     * @param string $remark
     *
     * @return CustomerInternalRemark
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get remark
     *
     * @return string
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return CustomerInternalRemark
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set company
     *
     * @param Customer $customer
     *
     * @return CustomerInternalRemark
     */
    public function setCustomer(Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get company
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }
}
