<?php

namespace AppBundle\Entity\Security\Customer;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Employee\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class AuthenticationLog
 * @package AppBundle\Entity
 *
 * @ORM\Entity()
 */
class AuthenticationLog
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $type
     *
     * @ORM\Column(type="string", length=32)
     */
    protected $type;

    /**
     * @var string $event
     *
     * @ORM\Column(type="string", length=128)
     */
    protected $event;

    /**
     * @var string $reason
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $reason;

    /**
     * @var User $employee
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Employee\User")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $employee;

    /**
     * @var Customer $customer
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Customer\Customer")
     */
    protected $customer;

    /**
     * @var string $ip
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $ip;

    /**
     * @var string $userAgent
     *
     * @ORM\Column(type="text")
     */
    protected $userAgent;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @var string $username
     *
     * @ORM\Column(type="string", length=128)
     */
    protected $username;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $event
     * @return $this
     */
    public function setEvent($event)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * @return string
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param $reason
     * @return $this
     */
    public function setReason($reason)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param User|null $employee
     * @return $this
     */
    public function setEmployee(User $employee = null)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * @return User
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @param Customer $customer
     * @return $this
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param $ip
     * @return $this
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param $userAgent
     * @return $this
     */
    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * @param $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }
}
