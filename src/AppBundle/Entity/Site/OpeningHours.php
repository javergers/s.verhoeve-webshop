<?php

namespace AppBundle\Entity\Site;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table("site_opening_hours");
 */
class OpeningHours
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Site", inversedBy="siteOpeningHours")
     * @ORM\JoinColumn(nullable=false);
     */
    private $site;

    /**
     * @ORM\Column(type="smallint", length=1)
     */
    private $day;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $fromHour;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $tillHour;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set site
     *
     * @param Site $site
     *
     * @return OpeningHours
     */
    public function setSite(Site $site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set day
     *
     * @param integer $day
     *
     * @return OpeningHours
     */
    public function setDay(int $day)
    {
        $this->day = $day;

        return $this;
    }

    /**
     * Get day
     *
     * @return int
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @return string
     */
    public function getFromHour()
    {
        return $this->fromHour;
    }

    /**
     * @param string $fromHour
     * @return OpeningHours
     */
    public function setFromHour(string $fromHour)
    {
        $this->fromHour = $fromHour;

        return $this;
    }

    /**
     * @return string
     */
    public function getTillHour()
    {
        return $this->tillHour;
    }

    /**
     * @param string $tillHour
     * @return OpeningHours
     */
    public function setTillHour(string $tillHour)
    {
        $this->tillHour = $tillHour;

        return $this;
    }
}
