<?php

namespace AppBundle\Entity\Order;

use AppBundle\Entity\Finance\Tax\VatRate;
use AppBundle\Interfaces\Order\LineInterface;
use AppBundle\Interfaces\Order\VatLineInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * Class OrderLineVat
 * @package AppBundle\Entity\Order
 */
class OrderLineVat implements VatLineInterface
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var OrderLine
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\OrderLine", inversedBy="vatLines")
     */
    protected $orderLine;

    /**
     * @var VatRate
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Finance\Tax\VatRate")
     */
    protected $vatRate;

    /**
     * @var float
     * @ORM\Column(type="float")
     */
    protected $amount;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return OrderLine
     */
    public function getOrderLine(): ?OrderLine
    {
        return $this->orderLine;
    }

    /**
     * @param OrderLine $orderLine
     * @return OrderLineVat
     */
    public function setOrderLine(OrderLine $orderLine): OrderLineVat
    {
        $this->orderLine = $orderLine;

        return $this;
    }

    /**
     * @return VatRate
     */
    public function getVatRate(): VatRate
    {
        return $this->vatRate;
    }

    /**
     * @param VatRate $vatRate
     * @return OrderLineVat
     */
    public function setVatRate(VatRate $vatRate): OrderLineVat
    {
        $this->vatRate = $vatRate;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return OrderLineVat
     */
    public function setAmount(float $amount): OrderLineVat
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return LineInterface
     */
    public function getLine(): LineInterface
    {
        return $this->orderLine;
    }
}