<?php

namespace AppBundle\Entity\Order;

use AppBundle\Entity\Order\Order;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 * Class FraudDetectionReport
 * @package AppBundle\Entity\Fraud
 */
class FraudDetectionReport
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\OrderCollection")
     */
    protected $orderCollection;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\FraudDetectionRule")
     */
    protected $fraudDetectionRule;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderCollection
     *
     * @param OrderCollection $orderCollection
     *
     * @return FraudDetectionReport
     */
    public function setOrderCollection(OrderCollection $orderCollection = null)
    {
        $this->orderCollection = $orderCollection;

        return $this;
    }

    /**
     * Get orderCollection
     *
     * @return OrderCollection
     */
    public function getOrderCollection()
    {
        return $this->orderCollection;
    }

    /**
     * Set fraudDetectionRule
     *
     * @param FraudDetectionRule $fraudDetectionRule
     *
     * @return FraudDetectionReport
     */
    public function setFraudDetectionRule(FraudDetectionRule $fraudDetectionRule = null)
    {
        $this->fraudDetectionRule = $fraudDetectionRule;

        return $this;
    }

    /**
     * Get fraudDetectionRule
     *
     * @return FraudDetectionRule
     */
    public function getFraudDetectionRule()
    {
        return $this->fraudDetectionRule;
    }
}
