<?php

namespace AppBundle\Entity\Order;

use Doctrine\ORM\Mapping as ORM;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;

/**
 * Class BillingItem
 * @package AppBundle\Entity\Order
 * @ORM\Entity(readOnly=true)
 */
class BillingItem
{
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var Order
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\Order", inversedBy="billingItems")
     */
    protected $order;

    /**
     * @var OrderCollectionLine
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\OrderCollectionLine", inversedBy="billingItems")
     */
    protected $orderCollectionLine;

    /**
     * @var string
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\BillingItemTransactionType")
     * @ORM\Column(type="BillingItemTransactionType")
     */
    protected $transactionType;

    /**
     * @var BillingItemGroup
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\BillingItemGroup", inversedBy="billingItems")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $billingItemGroup;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order
     *
     * @param Order $order
     *
     * @return BillingItem
     */
    public function setOrder(Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set orderCollectionLine
     *
     * @param OrderCollectionLine $orderCollectionLine
     *
     * @return BillingItem
     */
    public function setOrderCollectionLine(OrderCollectionLine $orderCollectionLine = null)
    {
        $this->orderCollectionLine = $orderCollectionLine;

        return $this;
    }

    /**
     * Get orderCollectionLine
     *
     * @return OrderCollectionLine
     */
    public function getOrderCollectionLine()
    {
        return $this->orderCollectionLine;
    }

    /**
     * Set billingItemGroup
     *
     * @param BillingItemGroup $billingItemGroup
     *
     * @return BillingItem
     */
    public function setBillingItemGroup(BillingItemGroup $billingItemGroup)
    {
        $this->billingItemGroup = $billingItemGroup;

        return $this;
    }

    /**
     * Get billingItemGroup
     *
     * @return BillingItemGroup
     */
    public function getBillingItemGroup()
    {
        return $this->billingItemGroup;
    }

    /**
     * Set transactionType
     *
     * @param string $transactionType
     *
     * @return BillingItem
     */
    public function setTransactionType($transactionType)
    {
        $this->transactionType = $transactionType;

        return $this;
    }

    /**
     * Get transactionType
     *
     * @return string
     */
    public function getTransactionType()
    {
        return $this->transactionType;
    }
}
