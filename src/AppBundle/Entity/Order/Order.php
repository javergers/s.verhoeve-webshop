<?php

namespace AppBundle\Entity\Order;

use AppBundle\Annotation\GeneralListener;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Discount\Voucher;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Geography\DeliveryAddressType;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\CompanyCustomOrderFieldValue;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Interfaces\Audit\AuditEntityInterface;
use AppBundle\Interfaces\Sales\OrderCollectionInterface;
use AppBundle\Interfaces\Sales\OrderInterface;
use AppBundle\Model\OrderModel;
use AppBundle\ThirdParty\Twinfield\Entity\Export;
use AppBundle\Validator\Constraints\InvalidLineCombination;
use AppBundle\Validator\Constraints\OrderValidSuppliers;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FS\SolrBundle\Doctrine\Annotation as Solr;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use RuleBundle\Annotation as Rule;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Solr\Document(index="order")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderRepository")
 * @ORM\Table(name="`order`", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="number", columns={"order_collection_id", "number"})
 * })
 * @ORM\HasLifecycleCallbacks
 *
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @GeneralListener(parent="OrderCollection", parentGetter="getOrderCollection", get="updatedAt", set="updatedAt")
 * @ORM\EntityListeners({"AppBundle\EventListener\Order\OrderListener"})
 * @Rule\Entity(description="Order")
 *
 * @InvalidLineCombination()
 * @OrderValidSuppliers()
 */
class Order extends OrderModel implements OrderInterface, AuditEntityInterface
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @Solr\Id
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Order\CartOrder", mappedBy="order", fetch="EXTRA_LAZY")
     */
    protected $cartOrder;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Order\OrderJob", mappedBy="order", fetch="EXTRA_LAZY")
     */
    protected $autoOrder;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\OrderCollection", inversedBy="orders", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @Rule\Property(description="Bestelling", autoComplete={"id"="ordercollection.id",
     *                                          "label"="ordercollection.number"}, relation="parent")
     */
    protected $orderCollection;

    /**
     * @ORM\Column(type="integer", columnDefinition="INT(4) UNSIGNED ZEROFILL", nullable=true, options={"unsigned" =
     *                             true})
     */
    protected $number;

    /**
     * @Solr\Field(type="text")
     */
    protected $orderNumber;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     *
     * @Rule\Property(description="Orderstatus", autoComplete={"id"="orderstatus.id",
     *                                           "label"="orderstatus.description"})
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\OrderDeliveryStatus")
     *
     */
    protected $deliveryStatus;

    /**
     * @Solr\Field(type="date", getter="format('Y-m-d')")
     * @ORM\Column(type="date", nullable=true)
     *
     */
    protected $deliveryDate;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\DeliveryAddressType")
     *
     */
    protected $deliveryAddressType;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $supplierRemark;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $deliveryRemark;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $internalRemark;

    /**
     * @var OrderInternalRemark $internalRemarks
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\OrderInternalRemark", mappedBy="order", cascade={"persist"})
     * @ORM\OrderBy({"createdAt" = "desc"})
     */
    protected $internalRemarks;

    /**
     * @Solr\Field(type="string")
     * @ORM\Column(type="text", nullable=true)
     *
     */
    protected $invoiceReference;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\OrderLine", mappedBy="order", cascade={"persist"})
     * @Rule\Property(description="Orderregel", autoComplete={"id"="orderline.id", "label"="orderline.number"},
     *                                          relation="child")
     */
    protected $lines;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\Collo", mappedBy="order", cascade={"persist"})
     */
    protected $collos;

    /**
     * @ORM\Column(type="float")
     *
     * @Rule\Property(description="Totaal (excl. BTW)")
     */
    private $totalPrice;

    /**
     * @ORM\Column(type="float")
     *
     * @Rule\Property(description="Totaal (incl. BTW)")
     */
    private $totalPriceIncl;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Address")
     *
     */
    protected $deliveryAddress;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Address")
     * @ORM\JoinColumn(nullable=true)
     *
     */
    protected $pickupAddress;

    /**
     * @Solr\Field(type="text")
     * @ORM\Column(type="string", length=200, nullable=true)
     *
     */
    protected $deliveryAddressCompanyName;

    /**
     * @Solr\Field(type="text")
     * @ORM\Column(type="string", length=200, nullable=true)
     *
     */
    protected $deliveryAddressAttn;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Rule\Property(description="(Bezorg) Straat")
     */
    protected $deliveryAddressStreet;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Rule\Property(description="(Bezorg) Huisnummer")
     */
    protected $deliveryAddressNumber;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     * @Rule\Property(description="(Bezorg) Huisnummer toevoeging")
     */
    protected $deliveryAddressNumberAddition;

    /**
     * @Solr\Field(type="text")
     *
     */
    protected $deliveryAddressStreetAndNumber;

    /**
     * @Solr\Field(type="text")
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Rule\Property(description="(Bezorg) Postcode")
     */
    protected $deliveryAddressPostcode;

    /**
     * @Solr\Field(type="text")
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Rule\Property(description="(Bezorg) Stad")
     */
    protected $deliveryAddressCity;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\Country")
     * @ORM\JoinColumn(name="delivery_address_country", referencedColumnName="id", nullable=false)
     *
     */
    protected $deliveryAddressCountry;

    /**
     * @Solr\Field(type="string")
     * @ORM\Column(type="string", length=35, nullable=true)
     *
     */
    protected $deliveryAddressPhoneNumber;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     *
     */
    protected $metadata;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\ThirdParty\Twinfield\Entity\Export", mappedBy="orders"))
     */
    protected $twinfieldExportOrders;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\OrderActivity", mappedBy="order", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $activities;

    /**
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    protected $forceManual = false;

    /**
     * @var int $priority
     *
     * @Assert\Length(min=1, max=100, minMessage="De waarde moet minimaal {{ limit }} zijn", maxMessage="De waarde mag
     *                       maximaal {{ limit }} zijn")
     * @Assert\Range(min=0, max=100)
     * @ORM\Column(type="integer", options={"default": 0})
     */
    protected $priority;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Supplier\SupplierOrder")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $supplierOrder;

    /**
     * @var Collection|BillingItem[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\BillingItem", mappedBy="order")
     */
    protected $billingItems;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    protected $locale;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Discount\Voucher", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $restitutionVoucher;

    /**
     * @ORM\Column(type="string", length=11, nullable=true)
     * @Assert\Regex("/^[0-9]{2}:[0-9]{2}-[0-9]{2}:[0-9]{2}$/")
     */
    protected $timeslot;

    /**
     * @var CompanyCustomOrderFieldValue[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Relation\CompanyCustomOrderFieldValue", mappedBy="order")
     */
    protected $companyCustomerOrderFieldValues;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lines = new ArrayCollection();
        $this->collos = new ArrayCollection();
        $this->twinfieldExportOrders = new ArrayCollection();
        $this->internalRemarks = new ArrayCollection();
        $this->activities = new ArrayCollection();
        $this->status = 'draft';
        $this->billingItems = new ArrayCollection();
        $this->companyCustomerOrderFieldValues = new ArrayCollection();

        $this->priority = 0;
    }

    /**
     * @deprecated use OrderManager::calculateTotals instead
     */
    public function calculateTotals()
    {
        $this->totalPrice = 0;
        $this->totalPriceIncl = 0;

        foreach ($this->getLines() as $line) {
            $this->totalPrice += $line->getTotalPrice();
            $this->totalPriceIncl += $line->getTotalPriceIncl();

            $this->totalPrice -= $line->getDiscountAmountExcl();
            $this->totalPriceIncl -= $line->getDiscountAmount();
        }

        $this->totalPrice = round($this->totalPrice, 2);
        $this->totalPriceIncl = round($this->totalPriceIncl, 2);
    }

    /**
     * Get lines
     *
     * @return Collection|OrderLine[]
     * @Rule\MappedProperty(property="lines")
     */
    public function getLines()
    {
        return $this->lines;
    }

    /**
     * Set lines
     *
     * @param OrderLine[]|ArrayCollection $lines
     *
     * @return Order
     */
    public function setLines($lines)
    {
        if ($lines instanceof ArrayCollection) {
            $this->lines = $lines;

            return $this;
        }

        $this->lines = new ArrayCollection();

        foreach ($lines as $line) {
            $this->addLine($line);
        }

        return $this;
    }

    /**
     * Add line
     *
     * @param OrderLine $line
     *
     * @return Order
     */
    public function addLine(OrderLine $line)
    {
        $line->setOrder($this);

        $this->lines[] = $line;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function calculateNumber()
    {
        $this->number = ($this->getOrderCollection()->getOrders()->indexOf($this) + 1);
    }

    /**
     * Get orderCollection
     *
     * @return OrderCollection
     */
    public function getOrderCollection()
    {
        return $this->orderCollection;
    }

    /**
     * Set orderCollection
     *
     * @param OrderCollection $orderCollection
     *
     * @return Order
     */
    public function setOrderCollection(OrderCollection $orderCollection = null)
    {
        $this->orderCollection = $orderCollection;

        return $this;
    }

    /**
     * @ORM\PostLoad()
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function load()
    {
        /**
         *  Build additional solr indexes
         */
        $this->deliveryAddressStreetAndNumber = trim($this->deliveryAddressStreet . ' ' . $this->deliveryAddressNumber);

        if (!empty($this->deliveryAddressNumberAddition)) {
            $this->deliveryAddressStreetAndNumber .= '-' . $this->deliveryAddressNumberAddition;
        }

        if ($this->getOrderCollection()) {
            $this->orderNumber = $this->getOrderCollection()->getNumber() . '-' . str_pad($this->number, 4, '0',
                    STR_PAD_LEFT);
        }
    }

    /**
     * Get supplierRemark
     *
     * @return string
     */
    public function getSupplierRemark()
    {
        return $this->supplierRemark;
    }

    /**
     * Set supplierRemark
     *
     * @param string $supplierRemark
     *
     * @return Order
     */
    public function setSupplierRemark($supplierRemark)
    {
        $this->supplierRemark = $supplierRemark;

        return $this;
    }

    /**
     * Get deliveryRemark
     *
     * @return string
     */
    public function getDeliveryRemark()
    {
        return $this->deliveryRemark;
    }

    /**
     * Set deliveryRemark
     *
     * @param string $deliveryRemark
     *
     * @return Order
     */
    public function setDeliveryRemark($deliveryRemark)
    {
        $this->deliveryRemark = $deliveryRemark;

        return $this;
    }

    /**
     * Set order
     *
     * @deprecated
     * @param OrderCollection $orderCollection
     *
     * @return Order
     */
    public function setOrder(OrderCollection $orderCollection = null)
    {
        $this->orderCollection = $orderCollection;

        return $this;
    }

    /**
     * Get order
     *
     * @deprecated
     * @return OrderCollection
     */
    public function getOrder()
    {
        return $this->orderCollection;
    }

    /**
     * Get orderCollection
     * @deprecated
     * @return OrderCollection|OrderCollectionInterface
     * @Rule\MappedProperty(property="orderCollection")
     */
    public function getCollection()
    {
        return $this->getOrderCollection();
    }

    /**
     * Get deliveryAddress
     *
     * @return Address
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * Set deliveryAddress
     *
     * @param Address $deliveryAddress
     *
     * @return Order
     */
    public function setDeliveryAddress(Address $deliveryAddress = null)
    {
        if ($deliveryAddress && $this->deliveryAddress !== $deliveryAddress) {
            $this->deliveryAddressCompanyName = $deliveryAddress->getCompanyName();
            $this->deliveryAddressAttn = $deliveryAddress->getAttn();
            $this->deliveryAddressStreet = $deliveryAddress->getStreet();
            $this->deliveryAddressNumber = $deliveryAddress->getNumber();
            $this->deliveryAddressNumberAddition = $deliveryAddress->getNumberAddition();
            $this->deliveryAddressPostcode = $deliveryAddress->getPostcode();
            $this->deliveryAddressCity = $deliveryAddress->getCity();
            $this->deliveryAddressCountry = $deliveryAddress->getCountry();
            $this->deliveryAddressPhoneNumber = $deliveryAddress->getPhoneNumber();
        }

        $this->deliveryAddress = $deliveryAddress;

        return $this;
    }

    /**
     * Remove lines
     * @throws \Exception
     */
    public function removeLines()
    {
        foreach ($this->lines as $line) {
            $this->removeLine($line);
        }
    }

    /**
     * Remove line
     *
     * @param OrderLine $line
     *
     * @throws \Exception
     */
    public function removeLine(OrderLine $line)
    {
        $this->lines->removeElement($line);

        $line->setDeletedAt(new \DateTime());
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return str_pad($this->number, 4, '0', STR_PAD_LEFT);
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Order
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customer
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Order
     */
    public function setStatus(?string $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @param null|string $status
     * @return mixed|null
     */
    public function getStatusDescription(?string $status = null)
    {
        if (null === $status) {
            $status = $this->status;
        }

        $statusDescriptions = [
            'draft' => 'Concept',
            'archived' => 'Gearchiveerd',
            'cancelled' => 'Geannuleerd',
            'complete' => 'Voltooid (volledig afgerond)',
            'new' => 'Nieuw (nog te verwerken)',
            'on_hold' => 'In de wacht',
            'payment_pending' => 'Wacht op betaling',
            'pending' => 'Verstuurd (naar leverancier)',
            'processed' => 'Verwerkt (door leverancier)',
            'sent' => 'Verzonden (naar klant)',
        ];

        if (!\array_key_exists($status, $statusDescriptions)) {
            return null;
        }

        return $statusDescriptions[$status];
    }

    /**
     * Get totalPrice
     *
     * @return float
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set totalPrice
     *
     * @param float $totalPrice
     *
     * @return Order
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPriceIncl
     *
     * @return float
     */
    public function getTotalPriceIncl()
    {
        return $this->totalPriceIncl;
    }

    /**
     * Set totalPriceIncl
     *
     * @param float $totalPriceIncl
     *
     * @return Order
     */
    public function setTotalPriceIncl($totalPriceIncl)
    {
        $this->totalPriceIncl = $totalPriceIncl;

        return $this;
    }

    /**
     * Get deliveryDate
     *
     * @return \DateTime
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * Set deliveryDate
     *
     * @param \DateTime $deliveryDate
     *
     * @return Order
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getOrderNumber(): ?string
    {
        $this->orderNumber = $this->getOrderCollection()->getNumber() . '-' . str_pad($this->number, 4, '0',
                STR_PAD_LEFT);

        return $this->orderNumber;
    }

    /**
     * Get internalRemark
     *
     * @return string
     */
    public function getInternalRemark()
    {
        return $this->internalRemark;
    }

    /**
     * Set internalRemark
     *
     * @param string $internalRemark
     *
     * @return Order
     */
    public function setInternalRemark($internalRemark)
    {
        $this->internalRemark = $internalRemark;

        return $this;
    }

    /**
     * @param $internalRemark
     * @return $this
     */
    public function addInternalRemark($internalRemark)
    {
        $this->internalRemarks[] = $internalRemark;

        return $this;
    }

    /**
     * Get internalRemarks
     *
     * @return ArrayCollection|OrderInternalRemark[]
     */
    public function getInternalRemarks()
    {
        return $this->internalRemarks;
    }

    /**
     * Get invoiceReference
     *
     * @return string
     */
    public function getInvoiceReference()
    {
        return $this->invoiceReference;
    }

    /**
     * Set invoiceReference
     *
     * @param string $invoiceReference
     *
     * @return Order
     */
    public function setInvoiceReference($invoiceReference)
    {
        $this->invoiceReference = $invoiceReference;

        return $this;
    }

    /**
     * Get deliveryAddressCompanyName
     *
     * @return string
     */
    public function getDeliveryAddressCompanyName()
    {
        return $this->deliveryAddressCompanyName;
    }

    /**
     * Set deliveryAddressCompanyName
     *
     * @param string $deliveryAddressCompanyName
     *
     * @return Order
     */
    public function setDeliveryAddressCompanyName($deliveryAddressCompanyName)
    {
        $this->deliveryAddressCompanyName = $deliveryAddressCompanyName;

        return $this;
    }

    /**
     * Get deliveryAddressAttn
     *
     * @return string
     */
    public function getDeliveryAddressAttn()
    {
        return $this->deliveryAddressAttn;
    }

    /**
     * Set deliveryAddressAttn
     *
     * @param string $deliveryAddressAttn
     *
     * @return Order
     */
    public function setDeliveryAddressAttn($deliveryAddressAttn)
    {
        $this->deliveryAddressAttn = $deliveryAddressAttn;

        return $this;
    }

    /**
     * Get deliveryAddressStreet
     *
     * @return string
     */
    public function getDeliveryAddressStreet()
    {
        return $this->deliveryAddressStreet;
    }

    /**
     * Set deliveryAddressStreet
     *
     * @param string $deliveryAddressStreet
     *
     * @return Order
     */
    public function setDeliveryAddressStreet($deliveryAddressStreet)
    {
        $this->deliveryAddressStreet = $deliveryAddressStreet;

        return $this;
    }

    /**
     * Get deliveryAddressNumber
     *
     * @return string
     */
    public function getDeliveryAddressNumber()
    {
        return $this->deliveryAddressNumber;
    }

    /**
     * Set deliveryAddressNumber
     *
     * @param string $deliveryAddressNumber
     *
     * @return Order
     */
    public function setDeliveryAddressNumber($deliveryAddressNumber)
    {
        $this->deliveryAddressNumber = $deliveryAddressNumber;

        return $this;
    }


    /**
     * Get deliveryAddressNumberAddition
     *
     * @return string
     */
    public function getDeliveryAddressNumberAddition()
    {
        return $this->deliveryAddressNumberAddition;
    }

    /**
     * Set deliveryAddressNumberAddition
     *
     * @param string $deliveryAddressNumberAddition
     *
     * @return Order
     */
    public function setDeliveryAddressNumberAddition($deliveryAddressNumberAddition)
    {
        $this->deliveryAddressNumberAddition = $deliveryAddressNumberAddition;

        return $this;
    }

    /**
     * Get the delivery address street, number and number addition as one string
     *
     * @return string
     */
    public function getDeliveryAddressStreetAndNumber()
    {
        return $this->deliveryAddressStreetAndNumber;
    }

    /**
     * Get deliveryAddressPostcode
     *
     * @return string
     */
    public function getDeliveryAddressPostcode()
    {
        return $this->deliveryAddressPostcode;
    }

    /**
     * Set deliveryAddressPostcode
     *
     * @param string $deliveryAddressPostcode
     *
     * @return Order
     */
    public function setDeliveryAddressPostcode($deliveryAddressPostcode)
    {
        $this->deliveryAddressPostcode = $deliveryAddressPostcode;

        return $this;
    }

    /**
     * Get deliveryAddressCity
     *
     * @return string
     */
    public function getDeliveryAddressCity()
    {
        return $this->deliveryAddressCity;
    }

    /**
     * Set deliveryAddressCity
     *
     * @param string $deliveryAddressCity
     *
     * @return Order
     */
    public function setDeliveryAddressCity($deliveryAddressCity)
    {
        $this->deliveryAddressCity = $deliveryAddressCity;

        return $this;
    }

    /**
     * Get deliveryAddressPhoneNumber
     *
     * @return string
     */
    public function getDeliveryAddressPhoneNumber()
    {
        return $this->deliveryAddressPhoneNumber;
    }

    /**
     * Set deliveryAddressPhoneNumber
     *
     * @param string $deliveryAddressPhoneNumber
     *
     * @return Order
     */
    public function setDeliveryAddressPhoneNumber($deliveryAddressPhoneNumber)
    {
        $this->deliveryAddressPhoneNumber = $deliveryAddressPhoneNumber;

        return $this;
    }

    /**
     * Get deliveryAddressCountry
     *
     * @return Country
     */
    public function getDeliveryAddressCountry()
    {
        return $this->deliveryAddressCountry;
    }

    /**
     * Set deliveryAddressCountry
     *
     * @param Country $deliveryAddressCountry
     *
     * @return Order
     */
    public function setDeliveryAddressCountry(Country $deliveryAddressCountry)
    {
        $this->deliveryAddressCountry = $deliveryAddressCountry;

        return $this;
    }

    /**
     * Get deliveryAddressType
     *
     * @return DeliveryAddressType
     */
    public function getDeliveryAddressType()
    {
        return $this->deliveryAddressType;
    }

    /**
     * Set deliveryAddressType
     *
     * @param DeliveryAddressType $deliveryAddressType
     *
     * @return Order
     */
    public function setDeliveryAddressType(DeliveryAddressType $deliveryAddressType = null)
    {
        $this->deliveryAddressType = $deliveryAddressType;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set metadata
     *
     * @param array $metadata
     *
     * @return Order
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Add twinfieldExportOrder
     *
     * @param Export $twinfieldExportOrder
     *
     * @return Order
     */
    public function addTwinfieldExportOrder(Export $twinfieldExportOrder)
    {
        $this->twinfieldExportOrders[] = $twinfieldExportOrder;

        return $this;
    }

    /**
     * Remove twinfieldExportOrder
     *
     * @param Export $twinfieldExportOrder
     */
    public function removeTwinfieldExportOrder(Export $twinfieldExportOrder)
    {
        $this->twinfieldExportOrders->removeElement($twinfieldExportOrder);
    }

    /**
     * Get twinfieldExportOrders
     *
     * @return Collection
     */
    public function getTwinfieldExportOrders()
    {
        return $this->twinfieldExportOrders;
    }

    /**
     * Get pickupAddress
     *
     * @return Address
     */
    public function getPickupAddress()
    {
        return $this->pickupAddress;
    }

    /**
     * Set pickupAddress
     *
     * @param Address $pickupAddress
     *
     * @return Order
     */
    public function setPickupAddress(Address $pickupAddress = null)
    {
        $this->pickupAddress = $pickupAddress;

        return $this;
    }

    /**
     * Get deliveryStatus
     *
     * @return OrderDeliveryStatus
     */
    public function getDeliveryStatus()
    {
        return $this->deliveryStatus;
    }

    /**
     * Set deliveryStatus
     *
     * @param OrderDeliveryStatus $deliveryStatus
     *
     * @return Order
     */
    public function setDeliveryStatus(OrderDeliveryStatus $deliveryStatus = null)
    {
        $this->deliveryStatus = $deliveryStatus;

        return $this;
    }

    /**
     * Get collos
     *
     * @return ArrayCollection|Collo[]
     */
    public function getCollos()
    {
        return $this->collos;
    }

    /**
     * Add collo
     *
     * @param Collo $collo
     *
     * @return Order
     */
    public function addCollo(Collo $collo)
    {
        $this->collos[] = $collo;

        return $this;
    }

    /**
     * Remove collo
     *
     * @param Collo $collo
     */
    public function removeCollo(Collo $collo)
    {
        $this->collos->removeElement($collo);
    }

    /**
     * Remove internalRemark
     *
     * @param OrderInternalRemark $internalRemark
     */
    public function removeInternalRemark(OrderInternalRemark $internalRemark)
    {
        $this->internalRemarks->removeElement($internalRemark);
    }

    /**
     * Get autoOrder
     *
     * @return OrderJob
     */
    public function getAutoOrder()
    {
        return $this->autoOrder;
    }

    /**
     * Set autoOrder
     *
     * @param OrderJob $autoOrder
     *
     * @return Order
     */
    public function setAutoOrder(?OrderJob $autoOrder): self
    {
        $this->autoOrder = $autoOrder;

        if (null !== $autoOrder) {
            $autoOrder->setOrder($this);
        }

        return $this;
    }

    /**
     * Add activity
     *
     * @param OrderActivity $activity
     *
     * @return Order
     */
    public function addActivity(OrderActivity $activity)
    {
        $this->activities[] = $activity;

        return $this;
    }

    /**
     * Remove activity
     *
     * @param OrderActivity $activity
     */
    public function removeActivity(OrderActivity $activity)
    {
        $this->activities->removeElement($activity);
    }

    /**
     * Get activities
     *
     * @return Collection|OrderActivity[]
     */
    public function getActivities()
    {
        return $this->activities;
    }

    /**
     * @return array
     */
    public function getAuditSettings(): array
    {
        return [
            'mapping' => [
                'status' => 'status',
                'deliveryStatus' => 'Bezorg status',
                'deliveryAddressCompanyName' => 'Bedrijfsnaam',
                'deliveryAddressAttn' => 'Ontvanger',
                'deliveryAddressStreet' => 'Bezorg straat',
                'deliveryAddressNumber' => 'Bezorg huisnummer',
                'deliveryAddressPostcode' => 'Bezorg postcode',
                'deliveryAddressCity' => 'Bezorg plaats',
                'deliveryAddressCountry' => 'Bezorg Land',
                'deliveryAddressPhoneNumber' => 'Telefoonnummer',
                'supplierRemark' => 'Opmerking voor leverancier',
                'deliveryRemark' => 'Opmerking voor bezorger',
                'invoiceReference' => 'Factuurreferentie',
                'deliveryDate' => 'Bezorgdatum',
                'deliveryTimeWindow' => 'Tijdsvak',
                'deletedAt' => 'Verwijderd op',
                'createdAt' => 'Aaangemaakt op',
                'updatedAt' => 'Geupdate op',
            ],
            'label' => function (Order $order) {
                /** @var Order $order */
                if ($order->getOrderCollection()->getInvoiceAddressCompanyName()) {
                    return $order->getOrderCollection()->getInvoiceAddressCompanyName();
                }

                return $order->getOrderCollection()->getInvoiceAddressAttn();
            },
            'title' => function (Order $order) {
                /** @var Order $order */
                $prefix = $order->getOrderNumber();

                if ($order->getOrderCollection()->getInvoiceAddressCompanyName()) {
                    return $prefix . ' ' . $order->getOrderCollection()->getInvoiceAddressCompanyName();
                }

                return $prefix . ' ' . $order->getOrderCollection()->getInvoiceAddressAttn();
            },
            'route' => function (Order $order) {
                return [
                    'popup' => true,
                    'route' => 'admin_sales_order_order',
                    'routeParameters' => [
                        'orderCollection' => $order->getOrderCollection()->getId(),
                    ],
                ];
            },
        ];
    }

    /**
     * @param string $activity
     * @return bool
     */
    public function hasActivity(string $activity)
    {
        foreach ($this->getActivities() as $ac) {
            if ($ac->getActivity() === $activity) {
                return true;
            }
        }

        return false;
    }

    /**
     * Set cartOrder
     *
     * @param CartOrder $cartOrder
     *
     * @return Order
     */
    public function setCartOrder(CartOrder $cartOrder = null)
    {
        $this->cartOrder = $cartOrder;

        return $this;
    }

    /**
     * Get cartOrder
     *
     * @return CartOrder
     */
    public function getCartOrder()
    {
        return $this->cartOrder;
    }

    /**
     * Set forceManual
     *
     * @param boolean $forceManual
     *
     * @return Order
     */
    public function setForceManual($forceManual)
    {
        $this->forceManual = $forceManual;

        return $this;
    }

    /**
     * Get forceManual
     *
     * @return boolean
     */
    public function getForceManual()
    {
        return $this->forceManual;
    }

    /**
     * @param int $priority
     * @return Order
     */
    public function setPriority(int $priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @return SupplierOrder|null
     */
    public function getSupplierOrder(): ?SupplierOrder
    {
        return $this->supplierOrder;
    }

    /**
     * @param null|SupplierOrder $supplierOrder
     * @return Order
     */
    public function setSupplierOrder(?SupplierOrder $supplierOrder): Order
    {
        $this->supplierOrder = $supplierOrder;

        return $this;
    }

    /**
     * Add billingItem
     *
     * @param BillingItem $billingItem
     *
     * @return Order
     */
    public function addBillingItem(BillingItem $billingItem)
    {
        if (!$this->billingItems->contains($billingItem)) {
            $this->billingItems[] = $billingItem;
        }


        return $this;
    }

    /**
     * Remove billingItem
     *
     * @param BillingItem $billingItem
     */
    public function removeBillingItem(BillingItem $billingItem)
    {
        if ($this->billingItems->contains($billingItem)) {
            $this->billingItems->removeElement($billingItem);
        }
    }

    /**
     * Get billingItems
     *
     * @return Collection
     */
    public function getBillingItems()
    {
        return $this->billingItems;
    }

    /**
     * @param string $locale
     * @return Order
     */
    public function setLocale(string $locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param Voucher|null $restitutionVoucher
     * @return Order
     */
    public function setRestitutionVoucher(?Voucher $restitutionVoucher): Order
    {
        $this->restitutionVoucher = $restitutionVoucher;

        return $this;
    }

    /**
     * @return Voucher|null
     */
    public function getRestitutionVoucher(): ?Voucher
    {
        return $this->restitutionVoucher;
    }

    /**
     * @return bool
     */
    public function hasRestitutionVoucher(): bool
    {
        return (bool)$this->restitutionVoucher;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isEligibleForRestitutionVoucher(): bool
    {
        return $this->hasRestitutionVoucher() || $this->getDeliveryDate() > (new \DateTime())->modify('-1 month');
    }

    /**
     * @param string|null $timeslot
     * @return Order
     */
    public function setTimeslot(?string $timeslot): Order
    {
        $this->timeslot = $timeslot;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTimeslot(): ?string
    {
        return $this->timeslot;
    }

    /**
     * @return CompanyCustomOrderFieldValue[]|Collection
     */
    public function getCompanyCustomerOrderFieldValues()
    {
        return $this->companyCustomerOrderFieldValues;
    }

    /**
     * @return OrderLine|bool
     */
    public function getDeliveryLine()
    {
        return $this->getLines()->filter(function (OrderLine $orderLine) {
            return ($orderLine->getProduct() instanceof TransportType);
        })->current();
    }
}
