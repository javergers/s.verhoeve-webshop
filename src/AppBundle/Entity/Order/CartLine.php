<?php

namespace AppBundle\Entity\Order;

use AppBundle\Model\Order\CartLineModel;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * Class CartLine
 * @package AppBundle\Entity\Order\Cart
 */
class CartLine extends CartLineModel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="uuid", unique=true)
     */
    protected $uuid;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product")
     */
    protected $product;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Vat")
     */
    protected $vat;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\Column(type="integer")
     */
    protected $quantity;

    /**
     * @ORM\Column(type="float")
     */
    protected $price;

    /**
     * @ORM\Column(type="float")
     */
    protected $discountAmount;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $metadata;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\Cart", inversedBy="lines")
     */
    protected $cart;
}
