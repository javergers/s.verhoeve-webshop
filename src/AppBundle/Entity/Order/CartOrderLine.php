<?php

namespace AppBundle\Entity\Order;

use AppBundle\Annotation\GeneralListener;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Vat;
use AppBundle\Interfaces\Order\LineInterface;
use AppBundle\Interfaces\Order\VatLineInterface;
use AppBundle\Interfaces\Sales\OrderLineInterface;
use AppBundle\Model\CartOrderLineModel;
use AppBundle\Traits\UuidEntity;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\OptimisticLockException;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use RuleBundle\Annotation as Rule;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @GeneralListener(parent="CartOrder", parentGetter="getCartOrder", get="updatedAt", set="updatedAt")
 *
 * @Rule\Entity(description="Conceptorderregel")
 */
class CartOrderLine extends CartOrderLineModel implements OrderLineInterface
{
    use TimestampableEntity;
    use SoftDeleteableEntity;
    use UuidEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Order\OrderLine", inversedBy="cartLine", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $orderLine;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\CartOrder", inversedBy="lines")
     * @Rule\Property(description="Conceptorder", autoComplete={"id"="cartOrder.id", "label"="cartOrder.id"},
     *                                            relation="parent")
     */
    protected $cartOrder;

    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\CartOrderLine", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\CartOrderLine", mappedBy="parent", cascade={"persist"})
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product")
     * @Rule\Property(description="Product", autoComplete={ "id"="product.id", "label"="product.translate.title"},
     *                                       relation="child")
     */
    protected $product;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Rule\Property(description="Prijs")
     */
    private $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Rule\Property(description="Kortingsbedrag")
     */
    private $discountAmount;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\DiscountType")
     * @ORM\Column(type="DiscountType", nullable=true)
     */
    private $discountType;

    /**
     * @ORM\Column(type="integer")
     * @Rule\Property(description="Aantal")
     */
    protected $quantity;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $metadata;

    /** @var array */
    private $totalPriceIncl = [];

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return CartOrderLine
     * @throws \Exception
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        parent::setQuantity((int)$quantity);

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     * @Rule\MappedProperty(property="discountAmount")
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set metadata
     *
     * @param array $metadata
     *
     * @return CartOrderLine
     */
    public function setMetadata(array $metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return CartOrderLine
     */
    public function setProduct(Product $product = null)
    {
        $this->product = $product;

        if (null === $product) {
            return $this;
        }

        $price = $product->getPrice();

        /**
         * If price of variation is set to use base product,
         * check if price is null and get price from parent product
         */
        if (null === $price && $product->getParent()) {
            $price = $product->getParent()->getPrice();
        }

        $this->setPrice($price);

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     * @Rule\MappedProperty(property="product")
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set order
     *
     * @deprecated
     * @param CartOrder $order
     *
     * @return CartOrderLine
     */
    public function setOrder(CartOrder $order = null)
    {
        $this->setCartOrder($order);

        return $this;
    }

    /**
     * Get order
     *
     * @Rule\MappedProperty(property="cartOrder")
     * @return CartOrder
     */
    public function getOrder()
    {
        return $this->getCartOrder();
    }

    /**
     * @return array
     * @throws DBALException
     * @throws OptimisticLockException
     * @throws InvalidArgumentException
     */
    public function getOrderableQuantities()
    {
        $product = $this->getProduct();

        return $product->isPersonalization() || $product->isCard() ?
            $this->getParent()->getProduct()->getOrderableQuantities() :
            $product->getOrderableQuantities();
    }

    /**
     * Set cartOrder
     *
     * @param CartOrder $cartOrder
     *
     * @return CartOrderLine
     */
    public function setCartOrder(CartOrder $cartOrder = null)
    {
        $this->cartOrder = $cartOrder;

        return $this;
    }

    /**
     * Get cartOrder
     *
     * @return CartOrder
     */
    public function getCartOrder()
    {
        return $this->cartOrder;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return CartOrderLine
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     * @Rule\MappedProperty(property="price")
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get price including vat
     * @deprecated
     * @return float
     */
    public function getPriceIncl()
    {
        $percentage = $this->getVat()->getPercentage();

        return round($this->getPrice() * (1 + ($percentage / 100)), 2);
    }

    /**
     * Get totalPrice
     * @deprecated
     * @param bool $withDiscount
     * @return float
     */
    public function getTotalPrice($withDiscount = false)
    {
        $totalPrice = $this->getPrice() * $this->getQuantity();

        return $withDiscount ? $totalPrice - $this->getDiscountAmountExcl() : $totalPrice;
    }

    /**
     * Get totalPrice including vat
     *
     * @param bool $withDiscount
     * @deprecated
     * @return float
     */
    public function getTotalPriceIncl($withDiscount = false)
    {
        if (isset($this->totalPriceIncl[$this->getId()][$withDiscount])) {
            return $this->totalPriceIncl[$this->getId()][$withDiscount];
        }

        $totalPrice = round($this->getPriceIncl() * $this->getQuantity(), 2);

        return ($this->totalPriceIncl[$this->getId()][$withDiscount] = $withDiscount ? round($totalPrice - $this->getDiscountAmount(),
            2) : $totalPrice);
    }

    /**
     * Set discountAmount
     *
     * @param float DiscountAmount
     *
     * @return CartOrderLine
     */
    public function setDiscountAmount($discountAmount)
    {
        $this->discountAmount = $discountAmount;

        return $this;
    }

    /**
     * Get discountAmount
     *
     * @return float
     * @Rule\MappedProperty(property="discountAmount")
     */
    public function getDiscountAmount()
    {
        return $this->discountAmount;
    }

    /**
     * Get discountAmount including vat
     *
     * @return float
     * @deprecated
     */
    public function getDiscountAmountIncl()
    {
        return $this->getDiscountAmount();
    }

    /**
     * @deprecated
     * @return float|int
     */
    public function getDiscountAmountExcl() {
        return $this->getDiscountAmount() / (1 + $this->getVat()->getPercentage() / 100);
    }

    /**
     * Set orderLine
     *
     * @param OrderLine $orderLine
     *
     * @return CartOrderLine
     */
    public function setOrderLine(OrderLine $orderLine = null)
    {
        $this->orderLine = $orderLine;

        return $this;
    }

    /**
     * Get orderLine
     *
     * @return OrderLine
     */
    public function getOrderLine()
    {
        return $this->orderLine;
    }

    public function __clone()
    {
        $this->id = null;
        $this->uuid = null;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * Set parent
     *
     * @param CartOrderLine $parent
     *
     * @return CartOrderLine
     */
    public function setParent(CartOrderLine $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return CartOrderLine
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child
     *
     * @param CartOrderLine $child
     *
     * @return CartOrderLine
     */
    public function addChild(CartOrderLine $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param CartOrderLine $child
     */
    public function removeChild(CartOrderLine $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Set children
     *
     * @param null|ArrayCollection|CartOrderLine[]
     *
     * @return CartOrderLine
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection|CartOrderLine[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set discountType
     *
     * @param string $discountType
     *
     * @return CartOrderLine
     */
    public function setDiscountType( $discountType)
    {
        $this->discountType = $discountType;

        return $this;
    }

    /**
     * Get discountType
     *
     * @return string
     */
    public function getDiscountType()
    {
        return $this->discountType;
    }

    /**
     * @return bool
     */
    public function isUpsell()
    {
        return isset($this->getMetadata()['upsell']);
    }

    /**
     * @return bool
     */
    public function hasUpsellLines()
    {
        return !$this->getUpsellLines()->isEmpty();
    }

    /**
     * @return Collection
     */
    public function getUpsellLines()
    {
        return $this->getChildren()->filter(function(CartOrderLine $cartOrderLine) {
            $metadata = $cartOrderLine->getMetadata();
            return null !== $metadata && isset($metadata['upsell']);
        });
    }

    /**
     * Checks whether this line has additional products for the product
     *
     * @param string $order
     * @return boolean
     */
    public function getProductAdditionalProducts($order = Criteria::DESC)
    {
        /** @var ArrayCollection $productAdditionalProducts */
        $productAdditionalProducts = $this->getCartOrder()->getLines()->filter(function (CartOrderLine $cartOrderLine) {
            /** @var $cartOrderLine CartOrderLine */
            return ($cartOrderLine->getMetadata()
                && !empty($cartOrderLine->getMetadata()['additional_product_parent'])
                && (int)$cartOrderLine->getMetadata()['additional_product_parent'] === $this->getProduct()->getId()
                && (int)$cartOrderLine->getParent()->getId() === $this->getId()
            );
        });

        $orderBy = Criteria::create();
        $orderBy->orderBy([
            'id' => $order,
        ]);

        return $productAdditionalProducts->matching($orderBy);
    }
}
