<?php

namespace AppBundle\Entity\Order;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Employee\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class OrderActivity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Employee\User")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Customer\Customer")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $customer;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\OrderCollection", inversedBy="activities")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $orderCollection;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\Order", inversedBy="activities")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $order;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $activity;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $text;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $datetime;

    /**
     * @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->datetime = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set activity
     *
     * @param string $activity
     *
     * @return OrderActivity
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return string
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return OrderActivity
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return OrderActivity
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     *
     * @return OrderActivity
     */
    public function setCustomer(Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set orderCollection
     *
     * @param OrderCollection $orderCollection
     *
     * @return OrderActivity
     */
    public function setOrderCollection(OrderCollection $orderCollection)
    {
        $this->orderCollection = $orderCollection;

        return $this;
    }

    /**
     * Get orderCollection
     *
     * @return OrderCollection
     */
    public function getOrderCollection()
    {
        return $this->orderCollection;
    }

    /**
     * Set order
     *
     * @param Order $order
     *
     * @return OrderActivity
     */
    public function setOrder(Order $order = null)
    {
        $this->order = $order;

        if (!$this->orderCollection && null !== $order) {
            $this->orderCollection = $order->getOrderCollection();
        }

        return $this;
    }

    /**
     * Get order
     *
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return OrderActivity
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }
}
