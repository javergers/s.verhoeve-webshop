<?php

namespace AppBundle\Entity\Order;

use AppBundle\Entity\Security\Employee\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class OrderOrderRemark
 * @package AppBundle\Entity
 *
 * @ORM\Entity
 */
class OrderInternalRemark
{
    use TimestampableEntity;

    /**
     * @var integer $id
     *
     * @ORM\id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User $user ;
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Employee\User")
     */
    protected $user;

    /**
     * @var Order $order ;
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\Order", inversedBy="internalRemarks")
     */
    protected $order;

    /**
     * @var string $remark
     *
     * @ORM\Column(type="text")
     */
    protected $remark;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $order
     * @return $this
     */
    public function setOrderOrder(Order $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return Order
     */
    public function getOrderOrder()
    {
        return $this->order;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param $remark
     * @return $this
     */
    public function setRemark($remark)
    {
        $this->remark = $remark;

        return $this;
    }

    /**
     * Get order
     *
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set order
     *
     * @param Order $order
     *
     * @return OrderInternalRemark
     */
    public function setOrder(Order $order = null)
    {
        $this->order = $order;

        return $this;
    }
}
