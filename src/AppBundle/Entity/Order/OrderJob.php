<?php

namespace AppBundle\Entity\Order;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Order\Order;
use Doctrine\ORM\Mapping as ORM;
use JMS\JobQueueBundle\Entity\Job;

/**
 * @ORM\Entity()
 * @ORM\Table("auto_order_job")
 * @ORM\HasLifecycleCallbacks()
 */
class OrderJob
{
    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Order\Order", inversedBy="autoOrder")
     */
    protected $order;

    /**
     * @ORM\OneToOne(targetEntity="JMS\JobQueueBundle\Entity\Job", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $job;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $updatedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $forwardAt;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company")
     */
    protected $supplier;

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateUpdatedAt()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return OrderJob
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set order
     *
     * @param Order $order
     *
     * @return OrderJob
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set job
     *
     * @param Job $job
     *
     * @return OrderJob
     */
    public function setJob(Job $job = null)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return Job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set forwardAt
     *
     * @param \DateTime $forwardAt
     *
     * @return OrderJob
     */
    public function setForwardAt($forwardAt)
    {
        $this->forwardAt = $forwardAt;

        return $this;
    }

    /**
     * Get forwardAt
     *
     * @return \DateTime
     */
    public function getForwardAt()
    {
        return $this->forwardAt;
    }

    /**
     * Set supplier
     *
     * @param Company $supplier
     *
     * @return OrderJob
     */
    public function setSupplier(Company $supplier = null)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return Company
     */
    public function getSupplier()
    {
        return $this->supplier;
    }
}
