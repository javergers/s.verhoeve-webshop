<?php

namespace AppBundle\Entity\Order;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Site;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Recurr\Rule;
use Recurr\Transformer\ArrayTransformer;
use Recurr\Transformer\ArrayTransformerConfig;
use Recurr\Transformer\Constraint\BeforeConstraint;
use Recurr\Transformer\Constraint\BetweenConstraint;
use Recurr\Transformer\TextTransformer;
use Recurr\Transformer\Translator;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Subscription
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Site")
     */
    protected $site;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Customer\Customer")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $customer;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $company;

    /**
     * @ORM\Column(type="text");
     */
    protected $rrule;

    /**
     * @ORM\Column(type="date")
     */
    protected $startDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $endDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $nextDate;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $invoiceReference;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"unsigned"=true})
     */
    protected $preferredFlorist;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $backofficeRemark;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $supplierRemark;

    /**
     * @ORM\Id
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\SubscriptionProduct", mappedBy="subscription", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\Cache("READ_ONLY")
     */
    protected $subscriptionProducts;

    /**
     * @ORM\Id
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\SubscriptionAddress", mappedBy="subscription", cascade={"persist"})
     * @ORM\OrderBy({"position" = "ASC"})
     * @ORM\Cache("READ_ONLY")
     */
    protected $subscriptionAddresses;

    /**
     * @ORM\Id
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Order\SubscriptionInterruption", mappedBy="subscription",
     *                                                                          cascade={"persist"})
     * @ORM\OrderBy({"startDate" = "ASC"})
     * @ORM\Cache("READ_ONLY")
     */
    protected $subscriptionInterruptions;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function calculateNextDate()
    {
        $exDates = [];

        foreach ($this->getSubscriptionInterruptions() as $interruption) {
            $startDate = clone $interruption->getStartDate();
            $startDate->setTime(0, 0, 0);

            $endDate = clone $interruption->getEndDate();
            $endDate = $endDate->modify('+1D');
            $endDate->setTime(23, 59, 59);

            $dateRange = new \DatePeriod($startDate, new \DateInterval('P1D'), $endDate);

            foreach ($dateRange as $date) {
                array_push($exDates, $date->format("Ymd"));
            }
        }

        $rule = new Rule($this->rrule);
        $rule->setExDates($exDates);

        $transformerConfig = new ArrayTransformerConfig();
        $transformerConfig->enableLastDayOfMonthFix();

        $transformer = new ArrayTransformer();
        $transformer->setConfig($transformerConfig);

        $today = new \DateTime();
        $today->setTime(0, 0, 0);

        if (!$this->getEndDate()) {
            $constraint = new BeforeConstraint($today, true);
        } else {
            $endDate = clone $this->getEndDate();
            $endDate->setTime(23, 59, 59);

            $constraint = new BetweenConstraint($today, $endDate, true);
        }

        $recurrences = $transformer->transform($rule, $constraint);

        if (count($recurrences) == 0) {
            $this->nextDate = null;
        } else {
            $this->nextDate = $recurrences[0]->getStart();
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Subscription
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set cardText
     *
     * @param string $cardText
     *
     * @return Subscription
     */
    public function setCardText($cardText)
    {
        $this->cardText = $cardText;

        return $this;
    }

    /**
     * Set site
     *
     * @param Site $site
     *
     * @return $this
     */
    public function setSite(Site $site = null)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     *
     * @return Subscription
     */
    public function setCustomer(Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return Subscription
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Subscription
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Subscription
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set backofficeRemark
     *
     * @param string $backofficeRemark
     *
     * @return Subscription
     */
    public function setBackofficeRemark($backofficeRemark)
    {
        $this->backofficeRemark = $backofficeRemark;

        return $this;
    }

    /**
     * Get backofficeRemark
     *
     * @return string
     */
    public function getBackofficeRemark()
    {
        return $this->backofficeRemark;
    }

    /**
     * Set supplierRemark
     *
     * @param string $supplierRemark
     *
     * @return Subscription
     */
    public function setSupplierRemark($supplierRemark)
    {
        $this->supplierRemark = $supplierRemark;

        return $this;
    }

    /**
     * Get supplierRemark
     *
     * @return string
     */
    public function getSupplierRemark()
    {
        return $this->supplierRemark;
    }

    /**
     * Set rrule
     *
     * @param string $rrule
     *
     * @return Subscription
     */
    public function setRrule($rrule)
    {
        $this->rrule = $rrule;

        return $this;
    }

    /**
     * Get rrule
     *
     * @return string
     */
    public function getRrule()
    {
        return $this->rrule;
    }

    public function getRruleAsText()
    {
        $textTransformer = new TextTransformer(new Translator('nl'));

        return $textTransformer->transform(new Rule($this->rrule));
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subscriptionProducts = new ArrayCollection();
        $this->subscriptionAddresses = new ArrayCollection();
        $this->subscriptionInterruptions = new ArrayCollection();
    }

    /**
     * Add subscriptionProduct
     *
     * @param SubscriptionProduct $subscriptionProduct
     *
     * @return Subscription
     */
    public function addSubscriptionProduct(SubscriptionProduct $subscriptionProduct)
    {
        $subscriptionProduct->setSubscription($this);

        $this->subscriptionProducts[] = $subscriptionProduct;

        return $this;
    }

    /**
     * Remove subscriptionProduct
     *
     * @param SubscriptionProduct $subscriptionProduct
     */
    public function removeSubscriptionProduct(SubscriptionProduct $subscriptionProduct)
    {
        $this->subscriptionProducts->removeElement($subscriptionProduct);
    }

    /**
     * Get subscriptionProducts
     *
     * @return Collection
     */
    public function getSubscriptionProducts()
    {
        return $this->subscriptionProducts;
    }

    public function getProducts()
    {
        return array_map(function ($subscriptionProduct) {
            return $subscriptionProduct->getProduct();
        }, $this->subscriptionProducts->toArray());
    }

    /**
     * Add subscriptionAddress
     *
     * @param SubscriptionAddress $subscriptionAddress
     *
     * @return Subscription
     */
    public function addSubscriptionAddress(SubscriptionAddress $subscriptionAddress)
    {
        $subscriptionAddress->setSubscription($this);

        $this->subscriptionAddresses[] = $subscriptionAddress;

        return $this;
    }

    /**
     * Remove subscriptionAddress
     *
     * @param SubscriptionAddress $subscriptionAddress
     */
    public function removeSubscriptionAddress(SubscriptionAddress $subscriptionAddress)
    {
        $this->subscriptionAddresses->removeElement($subscriptionAddress);
    }

    /**
     * Get subscriptionAddresses
     *
     * @return Collection
     */
    public function getSubscriptionAddresses()
    {
        return $this->subscriptionAddresses;
    }

    public function getAddresses()
    {
        return array_map(function ($subscriptionAddress) {
            return $subscriptionAddress->getAddress();
        }, $this->subscriptionAddresses->toArray());
    }

    /**
     * Add subscriptionInterruption
     *
     * @param SubscriptionInterruption $subscriptionInterruption
     *
     * @return Subscription
     */
    public function addSubscriptionInterruption(SubscriptionInterruption $subscriptionInterruption)
    {
        $subscriptionInterruption->setSubscription($this);

        $this->subscriptionInterruptions[] = $subscriptionInterruption;

        return $this;
    }

    /**
     * Remove subscriptionInterruption
     *
     * @param SubscriptionInterruption $subscriptionInterruption
     */
    public function removeSubscriptionInterruption(SubscriptionInterruption $subscriptionInterruption)
    {
        $this->subscriptionInterruptions->removeElement($subscriptionInterruption);
    }

    /**
     * Get subscriptionInterruptions
     *
     * @return Collection
     */
    public function getSubscriptionInterruptions()
    {
        return $this->subscriptionInterruptions;
    }

    /**
     * Set invoiceReference
     *
     * @param string $invoiceReference
     *
     * @return Subscription
     */
    public function setInvoiceReference($invoiceReference)
    {
        $this->invoiceReference = $invoiceReference;

        return $this;
    }

    /**
     * Get invoiceReference
     *
     * @return string
     */
    public function getInvoiceReference()
    {
        return $this->invoiceReference;
    }

    /**
     * Set preferredFlorist
     *
     * @param integer $preferredFlorist
     *
     * @return Subscription
     */
    public function setPreferredFlorist($preferredFlorist)
    {
        $this->preferredFlorist = $preferredFlorist;

        return $this;
    }

    /**
     * Get preferredFlorist
     *
     * @return integer
     */
    public function getPreferredFlorist()
    {
        return $this->preferredFlorist;
    }

    /**
     * Set nextDate
     *
     * @param \DateTime $nextDate
     *
     * @return Subscription
     */
    public function setNextDate($nextDate)
    {
        $this->nextDate = $nextDate;

        return $this;
    }

    /**
     * Get nextDate
     *
     * @return \DateTime
     */
    public function getNextDate()
    {
        return $this->nextDate;
    }
}
