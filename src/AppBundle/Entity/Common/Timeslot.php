<?php

namespace AppBundle\Entity\Common;

use Recurr\Exception\InvalidRRule;
use Recurr\Exception\InvalidWeekday;
use Recurr\Rule;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Timeslot
 * @package AppBundle\Entity\Common
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Common\TimeslotRepository", readOnly=true)
 */
class Timeslot 
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="time", name="`from`")
     * @Assert\NotBlank(message="Van tijd mag niet leeg zijn")
     */
    protected $from;

    /**
     * @ORM\Column(type="time")
     * @Assert\NotBlank(message="Tot tijd mag niet leeg zijn")
     */
    protected $till;

    /**
     * @ORM\Column(type="text", name="`rrule`")
     */
    protected $rruleString;

    /**
     * @var Rule|null
     */
    protected $rruleObject;

    /**
     * @var array|null
     */
    protected $availableOnDaysOfTheWeek;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $excludeCompanyClosingDates;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $excludeHolidays;

    /**
     * Timeslot constructor.
     * @param \DateTime $from
     * @param \DateTime $till
     * @param string    $rruleString
     * @param bool      $excludeCompanyClosingDates
     * @param bool      $excludeHolidays
     */
    public function __construct(
        \DateTime $from,
        \DateTime $till,
        string $rruleString = '',
        bool $excludeCompanyClosingDates = true,
        bool $excludeHolidays = true
    ) {
        $from->setDate(0, 0, 0);
        $this->from = $from;
        $till->setDate(0, 0, 0);
        $this->till = $till;
        $this->rruleString = $rruleString;
        $this->rruleObject = null;
        $this->excludeCompanyClosingDates = $excludeCompanyClosingDates;
        $this->excludeHolidays = $excludeHolidays;
        $this->availableOnDaysOfTheWeek = null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get from
     *
     * @return \DateTime
     */
    public function getFrom(): \DateTime
    {
        return $this->from;
    }

    /**
     * Get till
     *
     * @return \DateTime
     */
    public function getTill(): \DateTime
    {
        return $this->till;
    }

    /**
     * @return Rule|null
     * @throws InvalidRRule
     */
    public function getRrule(): ?Rule
    {
        if($this->rruleObject !== null){
            return $this->rruleObject;
        }

        return $this->rruleObject = Rule::createFromString($this->rruleString);
    }


    /**
     * @return string|null
     */
    public function getRruleString(): ?string
    {
        return $this->rruleString;
    }

    /**
     * Get excludeCompanyClosingDates
     *
     * @return boolean
     */
    public function getExcludeCompanyClosingDates(): bool
    {
        return $this->excludeCompanyClosingDates;
    }

    /**
     * Get excludeHolidays
     *
     * @return boolean
     */
    public function getExcludeHolidays(): bool
    {
        return $this->excludeHolidays;
    }

    /**
     * Get and save available days of the week so they dont have to be computed every iteration
     * Compatible with "(int)(new \DateTime)->format('N');"
     * @return array
     */
    public function getAvailableOnDaysOfTheWeek(): array
    {
        if ($this->availableOnDaysOfTheWeek !== null) {
            return $this->availableOnDaysOfTheWeek;
        }
        try {
            $rule = $this->getRrule();
            $weekdays = $rule ? $rule->getByDayTransformedToWeekdays() : null;
        } catch (InvalidRRule | InvalidWeekday $e) {
            return [];
        }
        if ($weekdays === null) {
            return [];
        }
        return $this->availableOnDaysOfTheWeek = array_map(function ($weekday) {
            return $weekday->weekday + 1;
        }, $weekdays);
    }
}
