<?php

namespace AppBundle\Entity\Common\Parameter;

use AppBundle\Model\ParameterModel;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class Parameter
 * @package AppBundle\Entity
 *
 * @ORM\Entity()
 * @UniqueEntity(fields={"key"})
 */
class Parameter
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=64, name="`key`")
     */
    protected $key;

    /**
     * @ORM\Column(type="string")
     */
    protected $formType;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $formTypeClass;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $multiple;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->key;
    }

    /**
     * Set key
     *
     * @param string $key
     *
     * @return Parameter
     */
    public function setKey($key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * Get key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Set formType
     *
     * @param string $formType
     *
     * @return Parameter
     */
    public function setFormType($formType)
    {
        $this->formType = $formType;

        return $this;
    }

    /**
     * Get formType
     *
     * @return string
     */
    public function getFormType()
    {
        return $this->formType;
    }

    /**
     * Set multiple
     *
     * @param boolean $multiple
     *
     * @return Parameter
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;

        return $this;
    }

    /**
     * Get multiple
     *
     * @return boolean
     */
    public function getMultiple()
    {
        return $this->multiple;
    }

    /**
     * Set formTypeClass
     *
     * @param string $formTypeClass
     *
     * @return Parameter
     */
    public function setFormTypeClass($formTypeClass)
    {
        $this->formTypeClass = $formTypeClass;

        return $this;
    }

    /**
     * Get formTypeClass
     *
     * @return string
     */
    public function getFormTypeClass()
    {
        return $this->formTypeClass;
    }
}
