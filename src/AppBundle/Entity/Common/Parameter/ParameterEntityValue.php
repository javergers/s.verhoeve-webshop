<?php

namespace AppBundle\Entity\Common\Parameter;

use AppBundle\Entity\Common\Parameter\ParameterEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * Class ParameterValue
 * @package AppBundle\Entity\Common\Parameter
 */
class ParameterEntityValue
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Common\Parameter\ParameterEntity", cascade={"persist", "remove"})
     */
    protected $parameterEntity;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $entity;

    /**
     * @ORM\Column(type="text")
     */
    protected $value;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entity
     *
     * @param integer $entity
     *
     * @return ParameterEntityValue
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return integer
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return ParameterEntityValue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set parameterEntity
     *
     * @param ParameterEntity $parameterEntity
     *
     * @return ParameterEntityValue
     */
    public function setParameterEntity(ParameterEntity $parameterEntity = null)
    {
        $this->parameterEntity = $parameterEntity;

        return $this;
    }

    /**
     * Get parameterEntity
     *
     * @return ParameterEntity
     */
    public function getParameterEntity()
    {
        return $this->parameterEntity;
    }
}
