<?php

namespace AppBundle\Entity\Common\Parameter;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * Class ParameterEntity
 * @package AppBundle\Entity\Common\Parameter
 * @UniqueEntity(fields={"parameter", "entity"}, ignoreNull=false)
 */
class ParameterEntity
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Common\Parameter\Parameter", cascade={"remove"})
     * @ORM\JoinColumn(referencedColumnName="key")
     */
    protected $parameter;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Common\Parameter\ParameterEntity")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    protected $parent;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $entity;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $required;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $defaultValue;

    /**
     * @ORM\Column(type="string")
     */
    protected $label;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entity
     *
     * @param string $entity
     *
     * @return ParameterEntity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity
     *
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set required
     *
     * @param boolean $required
     *
     * @return ParameterEntity
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * Get required
     *
     * @return boolean
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set defaultValue
     *
     * @param boolean $defaultValue
     *
     * @return ParameterEntity
     */
    public function setDefaultValue($defaultValue): ParameterEntity
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * Get defaultValue
     *
     * @return string|null
     */
    public function getDefaultValue(): ?string
    {
        return $this->defaultValue;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return ParameterEntity
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set parameter
     *
     * @param Parameter $parameter
     *
     * @return ParameterEntity
     */
    public function setParameter(Parameter $parameter = null)
    {
        $this->parameter = $parameter;

        return $this;
    }

    /**
     * Get parameter
     *
     * @return Parameter
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * Set parent
     *
     * @param ParameterEntity $parent
     *
     * @return ParameterEntity
     */
    public function setParent(ParameterEntity $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return ParameterEntity
     */
    public function getParent()
    {
        return $this->parent;
    }
}
