<?php

namespace AppBundle\Entity\Designer;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class DesignerCollage
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="DesignerCollageObject", mappedBy="collage", cascade={"persist"})
     */
    protected $objects;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->objects = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DesignerCollage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add object
     *
     * @param DesignerCollageObject $object
     *
     * @return DesignerCollage
     */
    public function addObject(DesignerCollageObject $object)
    {
        $object->setCollage($this);

        $this->objects[] = $object;

        return $this;
    }

    /**
     * Remove object
     *
     * @param DesignerCollageObject $object
     */
    public function removeObject(DesignerCollageObject $object)
    {
        $this->objects->removeElement($object);
    }

    /**
     * Get objects
     *
     * @return Collection
     */
    public function getObjects()
    {
        return $this->objects;
    }
}
