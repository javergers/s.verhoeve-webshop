<?php

namespace AppBundle\Entity\Supplier;

use AppBundle\Entity\Catalog\Product\Productgroup;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="supplier_productgroup", columns={"supplier_group_id",
 *                                                                                   "product_group_id"})})
 *
 */
class SupplierGroupCommission
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Supplier\SupplierGroup", inversedBy="supplierCommissions")
     */
    protected $supplierGroup;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Productgroup")
     * @ORM\JoinColumn(nullable=false))
     */
    protected $productGroup;

    /**
     * @ORM\Column(type="integer")
     */
    protected $percentage;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set percentage
     *
     * @param integer $percentage
     *
     * @return SupplierGroupCommission
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Get percentage
     *
     * @return integer
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * Set supplierGroup
     *
     * @param SupplierGroup $supplierGroup
     *
     * @return SupplierGroupCommission
     */
    public function setSupplierGroup(SupplierGroup $supplierGroup = null)
    {
        $this->supplierGroup = $supplierGroup;

        return $this;
    }

    /**
     * Get supplierGroup
     *
     * @return SupplierGroup
     */
    public function getSupplierGroup()
    {
        return $this->supplierGroup;
    }

    /**
     * Set productGroup
     *
     * @param ProductGroup $productGroup
     *
     * @return SupplierGroupCommission
     */
    public function setProductGroup(Productgroup $productGroup)
    {
        $this->productGroup = $productGroup;

        return $this;
    }

    /**
     * Get productGroup
     *
     * @return ProductGroup
     */
    public function getProductGroup()
    {
        return $this->productGroup;
    }
}
