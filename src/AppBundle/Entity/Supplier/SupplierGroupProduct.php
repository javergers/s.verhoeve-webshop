<?php

namespace AppBundle\Entity\Supplier;

use AppBundle\Entity\Catalog\Product\Product;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity()
 *
 * @method SupplierGroupProductTranslation translate(string $locale = null, boolean $fallbackToDefault = null)
 */
class SupplierGroupProduct
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Supplier\SupplierGroup")
     */
    protected $supplierGroup;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product", inversedBy="supplierGroupProducts")
     * @ORM\JoinColumn(nullable=false))
     */
    protected $product;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    protected $sku;

    /**
     * @ORM\Column(type="boolean", nullable=true);
     */
    protected $pickupEnabled;

    /**
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $forwardPrice;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sku
     *
     * @param string $sku
     *
     * @return SupplierGroupProduct
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set pickupEnabled
     *
     * @param boolean $pickupEnabled
     *
     * @return SupplierGroupProduct
     */
    public function setPickupEnabled($pickupEnabled)
    {
        $this->pickupEnabled = $pickupEnabled;

        return $this;
    }

    /**
     * Get pickupEnabled
     *
     * @return boolean
     */
    public function getPickupEnabled()
    {
        return $this->pickupEnabled;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return SupplierGroupProduct
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set supplierGroup
     *
     * @param SupplierGroup $supplierGroup
     *
     * @return SupplierGroupProduct
     */
    public function setSupplierGroup(SupplierGroup $supplierGroup = null)
    {
        $this->supplierGroup = $supplierGroup;

        return $this;
    }

    /**
     * Get supplierGroup
     *
     * @return SupplierGroup
     */
    public function getSupplierGroup()
    {
        return $this->supplierGroup;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return SupplierGroupProduct
     */
    public function setProduct(Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set forwardPrice
     *
     * @param float $forwardPrice
     *
     * @return SupplierGroupProduct
     */
    public function setForwardPrice($forwardPrice)
    {
        $this->forwardPrice = $forwardPrice;

        return $this;
    }

    /**
     * Get forwardPrice
     *
     * @return float
     */
    public function getForwardPrice()
    {
        return $this->forwardPrice;
    }
}
