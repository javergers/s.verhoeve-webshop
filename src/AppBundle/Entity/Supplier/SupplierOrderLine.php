<?php

namespace AppBundle\Entity\Supplier;

use AppBundle\Entity\Catalog\Product\Vat;
use AppBundle\Entity\Finance\Tax\VatRate;
use AppBundle\Entity\Order\OrderLine;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class SupplierOrderLine
 * @package AppBundle\Entity\Supplier
 *
 * @ORM\Entity
 */
class SupplierOrderLine
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var SupplierOrder
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Supplier\SupplierOrder", inversedBy="lines")
     */
    protected $supplierOrder;

    /**
     * @var null|OrderLine
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\OrderLine")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $orderLine;

    /**
     * @var SupplierProduct
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Supplier\SupplierProduct")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $supplierProduct;

    /**
     * @var SupplierGroupProduct
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Supplier\SupplierGroupProduct")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $supplierGroupProduct;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $quantity;

    /**
     * @var null|float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    protected $price;

    /**
     * @var null|VatRate
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Finance\Tax\VatRate")
     */
    protected $vatRate;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return SupplierOrder
     */
    public function getSupplierOrder(): SupplierOrder
    {
        return $this->supplierOrder;
    }

    /**
     * @param SupplierOrder $supplierOrder
     */
    public function setSupplierOrder(SupplierOrder $supplierOrder): void
    {
        $this->supplierOrder = $supplierOrder;
    }

    /**
     * @return OrderLine
     */
    public function getOrderLine(): OrderLine
    {
        return $this->orderLine;
    }

    /**
     * @param OrderLine $orderLine
     */
    public function setOrderLine(OrderLine $orderLine): void
    {
        $this->orderLine = $orderLine;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity(int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     */
    public function setPrice(?float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return float|null
     */
    public function getPriceIncl(): ?float
    {
        return round($this->getPrice() * (1 + $this->getVatRate()->getFactor()), 2);
    }


    /**
     * @return VatRate|null
     */
    public function getVatRate(): ?VatRate
    {
        return $this->vatRate;
    }

    /**
     * @param VatRate $vatRate
     * @return SupplierOrderLine
     */
    public function setVatRate(VatRate $vatRate): SupplierOrderLine
    {
        $this->vatRate = $vatRate;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return float
     */
    public function getTotalPrice()
    {
        return round($this->getPrice(), 2) * $this->getQuantity();
    }

    /**
     * @return float|null
     */
    public function getTotalPriceIncl(): ?float
    {
        return round($this->getTotalPrice() * (1 + $this->getVatRate()->getFactor()), 2);
    }

    /**
     * @return SupplierProduct
     * @internal
     * @deprecated Use getConcludedSupplierProduct instead
     */
    public function getSupplierProduct(): SupplierProduct
    {
        return $this->supplierProduct;
    }

    /**
     * @param SupplierProduct $supplierProduct
     */
    public function setSupplierProduct(SupplierProduct $supplierProduct): void
    {
        $this->supplierProduct = $supplierProduct;
    }

    /**
     * @return SupplierGroupProduct
     * @internal
     * @deprecated Use getConcludedSupplierProduct instead
     */
    public function getSupplierGroupProduct(): SupplierGroupProduct
    {
        return $this->supplierGroupProduct;
    }

    /**
     * @param SupplierGroupProduct $supplierGroupProduct
     */
    public function setSupplierGroupProduct(SupplierGroupProduct $supplierGroupProduct): void
    {
        $this->supplierGroupProduct = $supplierGroupProduct;
    }

    /**
     * @return SupplierProduct|SupplierGroupProduct
     */
    public function getConcludedSupplierProduct()
    {
        if ($this->supplierProduct !== null && $this->supplierGroupProduct === null) {
            return $this->supplierProduct;
        }

        if ($this->supplierProduct === null && $this->supplierGroupProduct !== null) {
            return $this->supplierGroupProduct;
        }

        throw new \RuntimeException('One of SupplierProduct or SupplierGroupProduct must be set');
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $executionContext
     * @param                           $payload
     */
    public function validate(ExecutionContextInterface $executionContext, $payload)
    {
        if (null !== $this->getVatRate() && false === $this->getVatRate()->isPurchase()) {
            $executionContext->buildViolation('Een leveranciersorder mag geen verkoop btw tarief bevatten.')
                ->atPath('vatRate')
                ->addViolation();
        }
    }
}