<?php

namespace AppBundle\Entity\Supplier;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyPreferredSupplier;
use AppBundle\Entity\Geography\Postcode;
use AppBundle\Interfaces\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\EntityListeners({"AppBundle\EventListener\Supplier\SupplierOnDeliveryAreaListener"})
 * @ORM\Table("supplier_delivery_area", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="postcode_company", columns={"postcode_id", "company_id"})
 * })
 */
class DeliveryArea implements EntityInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geography\Postcode",  inversedBy="deliveryAreas", cascade={"persist"})
     *
     */
    protected $postcode;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="deliveryAreas")
     *
     */
    protected $company;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Relation\CompanyPreferredSupplier", mappedBy="deliveryArea")
     */
    protected $preferredSuppliers;

    /**
     * @ORM\Column(type="float", nullable=true)
     *
     */
    protected $deliveryPrice;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $deliveryInterval;

    /**
     * Get id
     *
     * @return null|int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set deliveryPrice
     *
     * @param float $deliveryPrice
     *
     * @return DeliveryArea
     */
    public function setDeliveryPrice($deliveryPrice)
    {
        $this->deliveryPrice = $deliveryPrice;

        return $this;
    }

    /**
     * Get deliveryPrice
     *
     * @return float
     */
    public function getDeliveryPrice()
    {
        return $this->deliveryPrice;
    }

    /**
     * Get deliveryPrice
     * @deprecated
     * @return float
     */
    public function getDeliveryPriceIncl()
    {
        return $this->deliveryPrice * 1.09;
    }

    /**
     * Set deliveryInterval
     *
     * @param string $deliveryInterval
     *
     * @return DeliveryArea
     */
    public function setDeliveryInterval($deliveryInterval)
    {
        $this->deliveryInterval = $deliveryInterval;

        return $this;
    }

    /**
     * Get deliveryInterval
     *
     * @return string
     */
    public function getDeliveryInterval()
    {
        return $this->deliveryInterval;
    }

    /**
     * Set postcode
     *
     * @param Postcode $postcode
     *
     * @return DeliveryArea
     */
    public function setPostcode(Postcode $postcode = null)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return Postcode
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set company
     *
     * @param Company $company
     *
     * @return DeliveryArea
     */
    public function setCompany(Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->preferredSuppliers = new ArrayCollection();
    }

    /**
     * Add preferredSupplier
     *
     * @param CompanyPreferredSupplier $preferredSupplier
     *
     * @return DeliveryArea
     */
    public function addPreferredSupplier(CompanyPreferredSupplier $preferredSupplier)
    {
        $this->preferredSuppliers[] = $preferredSupplier;

        return $this;
    }

    /**
     * Remove preferredSupplier
     *
     * @param CompanyPreferredSupplier $preferredSupplier
     */
    public function removePreferredSupplier(CompanyPreferredSupplier $preferredSupplier)
    {
        $this->preferredSuppliers->removeElement($preferredSupplier);
    }

    /**
     * Get preferredSuppliers
     *
     * @return Collection|CompanyPreferredSupplier[]
     */
    public function getPreferredSuppliers()
    {
        return $this->preferredSuppliers;
    }
}
