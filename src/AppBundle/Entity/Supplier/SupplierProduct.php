<?php

namespace AppBundle\Entity\Supplier;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Relation\Company;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 *
 * @method SupplierProductTranslation translate(string $locale = null, boolean $fallbackToDefault = null)
 */
class SupplierProduct
{
    use ORMBehaviors\Translatable\Translatable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="supplierProducts")
     */
    protected $supplier;

    /**
     * @ORM\Column(type="integer", name="supplier_id_old", nullable=true)
     */
    protected $supplierId;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product", inversedBy="supplierProducts")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $product;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    protected $sku;

    /**
     * @ORM\Column(type="boolean", nullable=true);
     */
    protected $pickupEnabled;

    /**
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $forwardPrice;

    /**
     * Set supplier
     *
     * @param Company $supplier
     *
     * @return SupplierProduct
     */
    public function setSupplier(Company $supplier)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return Company
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set supplierIdOld
     *
     * @param integer $supplierId
     *
     * @return SupplierProduct
     */
    public function setSupplierId($supplierId)
    {
        $this->supplierId = $supplierId;

        return $this;
    }

    /**
     * Get supplierIdOld
     *
     * @return integer
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return SupplierProduct
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set sku
     *
     * @param string $sku
     *
     * @return SupplierProduct
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pickupEnabled
     *
     * @param boolean $pickupEnabled
     *
     * @return SupplierProduct
     */
    public function setPickupEnabled($pickupEnabled)
    {
        $this->pickupEnabled = $pickupEnabled;

        return $this;
    }

    /**
     * Get pickupEnabled
     *
     * @param bool $traverse
     * @return bool
     * @throws \Exception
     */
    public function getPickupEnabled($traverse = false)
    {
        $pickupEnabled = $this->pickupEnabled;

        if ($traverse && null === $pickupEnabled && null !== ($productGroup = $this->getProduct()->getProductgroup(true))) {
            return $productGroup->getPickupEnabled();
        }

        return $pickupEnabled;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return SupplierProduct
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set forwardPrice
     *
     * @param float $forwardPrice
     *
     * @return SupplierProduct
     */
    public function setForwardPrice($forwardPrice)
    {
        $this->forwardPrice = $forwardPrice;

        return $this;
    }

    /**
     * Get forwardPrice
     *
     * @return float
     */
    public function getForwardPrice()
    {
        return $this->forwardPrice;
    }
}
