<?php

namespace AppBundle\Entity\Supplier;

use AppBundle\Entity\Common\Timeslot;
use AppBundle\Entity\Relation\CompanyEstablishment;
use AppBundle\Entity\Common\TimeslotEntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Recurr\Rule as RRule;
use RuleBundle\Annotation as Rule;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class PickupLocation
 * @package AppBundle\Entity\Relation
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Supplier\PickupLocationRepository")
 */
class PickupLocation implements TimeslotEntityInterface
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", options={"default" : 1})
     */
    protected $preparationTime;

    /**
     * @var CompanyEstablishment
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Relation\CompanyEstablishment")
     */
    protected $companyEstablishment;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Common\Timeslot")
     */
    protected $pickupTimeslots;

    public function __construct()
    {
        $this->preparationTime = 1;
        $this->pickupTimeslots = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set preparationTime
     * @param int $preparationTime
     * @return PickupLocation
     */
    public function setPreparationTime(int $preparationTime):PickupLocation
    {
        $this->preparationTime = $preparationTime;
        return $this;
    }

    /**
     * Get preparationTime
     *
     * @return integer
     */
    public function getPreparationTime():int
    {
        return $this->preparationTime;
    }

    /**
     * Set companyEstablishment
     *
     * @param CompanyEstablishment $companyEstablishment
     *
     * @return PickupLocation
     */
    public function setCompanyEstablishment(CompanyEstablishment $companyEstablishment = null): PickupLocation
    {
        $this->companyEstablishment = $companyEstablishment;

        return $this;
    }

    /**
     * Get companyEstablishment
     *
     * @return CompanyEstablishment
     */
    public function getCompanyEstablishment(): CompanyEstablishment
    {
        return $this->companyEstablishment;
    }

    /**
     * Set pickupTimeslots
     *
     * @param Timeslot[] $pickupTimeslots
     *
     * @return PickupLocation
     */
    public function setTimeslots($pickupTimeslots): PickupLocation
    {
        $this->pickupTimeslots = new ArrayCollection();

        foreach ($pickupTimeslots as $pickupTimeslot) {
            $this->addTimeslot($pickupTimeslot);
        }

        return $this;
    }


    /**
     * Add pickupTimeslot
     *
     * @param Timeslot $pickupTimeslot
     *
     * @return PickupLocation
     */
    public function addTimeslot(Timeslot $pickupTimeslot): PickupLocation
    {
        if (!$this->pickupTimeslots->contains($pickupTimeslot)) {
            $this->pickupTimeslots->add($pickupTimeslot);
        }

        return $this;
    }

    /**
     * Remove pickupTimeslot
     *
     * @param Timeslot $pickupTimeslot
     * @return PickupLocation
     */
    public function removeTimeslot(Timeslot $pickupTimeslot): PickupLocation
    {
        if ($this->pickupTimeslots->contains($pickupTimeslot)) {
            $this->pickupTimeslots->removeElement($pickupTimeslot);
        }

        return $this;
    }

    /**
     * Remove pickupTimeslots
     * @return PickupLocation
     */
    public function removeTimeslots(): PickupLocation
    {
        foreach ($this->pickupTimeslots as $pickupTimeslot) {
            $this->removeTimeslot($pickupTimeslot);
        }

        return $this;
    }

    /**
     * Get pickupTimeslots
     *
     * @return ArrayCollection|Timeslot[]
     * Rule\MappedProperty(property="pickupTimeslots")
     * @throws \Exception
     */
    public function getTimeslots()
    {
        return $this->pickupTimeslots;
    }

    /**
     * @Assert\Callback()
     * @param ExecutionContextInterface $executionContext
     * @param                           $payload
     */
    public function validate(ExecutionContextInterface $executionContext, $payload)
    {
        if (!$this->companyEstablishment->getCompany()->getIsSupplier()) {
            $executionContext->buildViolation('Pickup location must connect to the establishment of a supplier')
                ->atPath('companyEstablishment')
                ->addViolation();
        }
    }
}
