<?php

namespace AppBundle\Entity\Discount;

use AppBundle\Entity\Relation\Company;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use RuleBundle\Entity\Rule;

/**
 * Class Discount
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Discount\DiscountRepository")
 */
class Discount
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @var int $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var Rule $rule
     *
     * @ORM\ManyToOne(targetEntity="RuleBundle\Entity\Rule")
     */
    protected $rule;

    /**
     * @var Company[] $companies
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Relation\Company", inversedBy="discounts")
     * @ORM\JoinTable(
     *     name="discount_company",
     *     joinColumns={
     *       @ORM\JoinColumn(name="discount_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *       @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     *     }
     * )
     */
    protected $companies;

    /**
     * @var Voucher[] $vouchers
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Discount\Voucher", mappedBy="discount", cascade={"remove"})
     */
    protected $vouchers;

    /**
     * @var $type
     *
     * @ORM\Column(type="string", columnDefinition="enum('amount', 'percentage')")
     */
    protected $type;

    /**
     * @var float $value
     *
     * @ORM\Column(type="decimal", precision=6, scale=3)
     */
    protected $value;

    /**
     * Discount constructor.
     */
    public function __construct()
    {
        $this->companies = new ArrayCollection();
        $this->vouchers = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getDescription();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $description
     * @return Discount
     */
    public function setDescription($description): Discount
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param ArrayCollection $vouchers
     * @return $this
     */
    public function setVouchers(ArrayCollection $vouchers)
    {
        foreach ($vouchers as $voucher) {
            $this->addVoucher($voucher);
        }

        return $this;
    }

    /**
     * @param Voucher $voucher
     * @return $this
     */
    public function addVoucher(Voucher $voucher)
    {
        $this->vouchers[] = $voucher;

        return $this;
    }

    /**
     * @param Voucher $voucher
     */
    public function removeVoucher(Voucher $voucher)
    {
        $this->vouchers->removeElement($voucher);
    }

    /**
     * @return Rule
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * @param Rule $rule
     */
    public function setRule(Rule $rule)
    {
        $this->rule = $rule;
    }

    /**
     * @param ArrayCollection $companies
     * @return $this
     */
    public function setCompanies(ArrayCollection $companies)
    {
        foreach ($companies as $company) {
            $this->addCompany($company);
        }

        return $this;
    }

    /**
     * @param Company $company
     * @return $this
     */
    public function addCompany(Company $company)
    {
        $company->addDiscount($this);
        $this->companies[] = $company;

        return $this;
    }

    /**
     * @param Company $company
     */
    public function removeCompany(Company $company)
    {
        $company->removeDiscount($this);
        $this->companies->remove($company);
    }

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $value
     * @return mixed
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Get companies
     *
     * @return Collection
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * Get vouchers
     *
     * @return Collection
     */
    public function getVouchers()
    {
        return $this->vouchers;
    }
}
