<?php

namespace AppBundle\Entity\Discount;

use Doctrine\ORM\Mapping as ORM;
use RuleBundle\Entity\Property;

/**
 * @ORM\Entity()
 * Class DiscountProperty
 * @package AppBundle\Entity\Discount
 */
class DiscountProperty
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $description;

    /**
     * @ORM\Column(type="string")
     */
    protected $path;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return DiscountProperty
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }


}
