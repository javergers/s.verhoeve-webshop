<?php

namespace AppBundle\Composer;

use AppBundle\Cache\Adapter\TagAwareAdapter;
use Composer\Script\Event;
use Symfony\Component\Process\PhpExecutableFinder;
use Symfony\Component\Process\Process;
use Symfony\Component\Yaml\Yaml;
use Twig_Environment;
use Twig_Loader_Filesystem;

/**
 * Class ScriptHandler
 * @package AppBundle\Composer
 */
class ScriptHandler
{
    protected static $options = [
        'symfony-app-dir' => 'app',
    ];

    /**
     * @param Event $event
     */
    public static function invalidateCache(Event $event): void
    {
        $options = static::getOptions($event);

        $tagArgument = TagAwareAdapter::CLEAR_ON_RELEASE;

        static::executeCommand($event, $options['symfony-app-dir'], 'cache:invalidate ' . escapeshellarg($tagArgument));
    }

    /**
     * @throws \Twig_Error
     */
    public static function generateFirebaseMessagingScript(): void
    {
        $value = Yaml::parse(file_get_contents('./config/parameters.yaml'));

        $loader = new Twig_Loader_Filesystem('./templates');

        $twig = new Twig_Environment($loader);

        file_put_contents('./web/firebase-messaging-sw.js', $twig->render('firebase-messaging-sw.js.twig', [
            'firebase_sender_id' => $value['parameters']['firebase_sender_id'],
        ]));
    }

    /**
     * @param Event $event
     * @param       $consoleDir
     * @param       $cmd
     * @param int   $timeout
     */
    protected static function executeCommand(Event $event, $consoleDir, $cmd, $timeout = 300): void
    {
        $php = escapeshellarg(static::getPhp(false));
        $phpArgs = implode(' ', array_map('escapeshellarg', static::getPhpArguments()));
        $console = escapeshellarg($consoleDir . '/console');

        if ($event->getIO()->isDecorated()) {
            $console .= ' --ansi';
        }

        $process = new Process($php . ($phpArgs ? ' ' . $phpArgs : '') . ' ' . $console . ' ' . $cmd, null, null, null,
            $timeout);

        $process->run(function ($type, $buffer) use ($event) {
//            void($type);

            $event->getIO()->write($buffer, false);
        });

        if (!$process->isSuccessful()) {
            throw new \RuntimeException(sprintf("An error occurred when executing the \"%s\" command:\n\n%s\n\n%s.",
                escapeshellarg($cmd), $process->getOutput(), $process->getErrorOutput()));
        }
    }

    /**
     * @param bool $includeArgs
     * @return string
     */
    protected static function getPhp($includeArgs = true): string
    {
        $phpFinder = new PhpExecutableFinder();
        $phpPath = $phpFinder->find($includeArgs);

        if ($phpPath === false) {
            throw new \RuntimeException('The php executable could not be found, add it to your PATH environment variable and try again');
        }

        return $phpPath;
    }

    /**
     * @return array
     */
    protected static function getPhpArguments(): array
    {
        $ini = null;
        $arguments = [];

        $phpFinder = new PhpExecutableFinder();
        if (method_exists($phpFinder, 'findArguments')) {
            $arguments = $phpFinder->findArguments();
        }

        if ($env = (string)getenv('COMPOSER_ORIGINAL_INIS')) {
            $paths = explode(PATH_SEPARATOR, $env);
            $ini = array_shift($paths);
        } else {
            $ini = php_ini_loaded_file();
        }

        if ($ini) {
            $arguments[] = '--php-ini=' . $ini;
        }

        return $arguments;
    }

    /**
     * @param Event $event
     * @return array
     */
    protected static function getOptions(Event $event): array
    {
        $options = array_merge(static::$options, $event->getComposer()->getPackage()->getExtra());

        $options['symfony-app-dir'] = 'bin';
        $options['process-timeout'] = $event->getComposer()->getConfig()->get('process-timeout');

        return $options;
    }
}
