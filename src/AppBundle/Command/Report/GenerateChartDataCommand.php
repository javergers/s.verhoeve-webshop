<?php

namespace AppBundle\Command\Report;

use AppBundle\Entity\Report\ReportChart;
use AppBundle\Entity\Report\ReportChartData;
use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\OptimisticLockException;
use RuleBundle\Service\ResolverService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class GenerateChartDataCommand
 * @package AppBundle\Command\Report
 */
class GenerateChartDataCommand extends ContainerAwareCommand
{
    /**
     * @var string
     */
    private $dateFormat;

    protected function configure()
    {
        $this
            ->setName('app:generate:chart')
            ->addArgument('id', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws MappingException
     * @throws OptimisticLockException
     * @throws \Exception
     * @throws \ReflectionException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $reportChartId  = $input->getArgument('id');
        $reportChart = $this->getContainer()->get('doctrine')->getRepository(ReportChart::class)->find($reportChartId);

        if ($reportChart === null) {
            throw new \RuntimeException(sprintf("Report chart with '%d' not found", $reportChartId));
        }
        
        /** @var ReportChartData $chartData */
        foreach ($reportChart->getReportChartData() as $chartData) {
            $requirements = [
                'fromDate' => $chartData->getFromDate(),
                'tillDate' => $chartData->getTillDate(),
            ];

            $chartDataArray = $this->getContainer()->get('app.report')->retrieveData($chartData->getReportChart()->getReport(),
                $requirements);
            $propertyAccessor = new PropertyAccessor();

            $dataArray = $this->generateDateLabels($chartData, $reportChart->getDateFilter());

            $checkedDate = false;
            foreach ($chartDataArray as $entityData) {
                $sortPath = str_replace(ResolverService::QB_ALIAS . '.', null, $chartData->getSortByPath());
                $dataPath = str_replace(ResolverService::QB_ALIAS . '.', null, $chartData->getDataPath());

                $sortValue = $propertyAccessor->getValue($entityData, $sortPath);
                $dataValue = $propertyAccessor->getValue($entityData, $dataPath);

                //check if date keys still need to exist
                if(!($sortValue instanceof \DateTime)) {
                    if(!$checkedDate) {
                        $checkedDate = true;
                        $dataArray = [];
                    }
                } else {
                    $sortValue = $sortValue->format($this->dateFormat);
                }

                if(is_numeric($dataValue)) {
                    $dataValue = number_format($dataValue, 2);
                }

                if (isset($dataArray[$sortValue])) {
                    $dataArray[$sortValue] += $dataValue;
                } else {
                    $dataArray[$sortValue] = $dataValue;
                }
            }

            $finalArray = [
                'label' => array_keys($dataArray),
                'data' => array_values($dataArray),
            ];

            $chartData->setData($finalArray);

            $this->getContainer()->get('doctrine')->getManager()->flush();
        }
    }

    /**
     * @param ReportChartData $chartData
     * @param string          $dateFilter
     * @return array
     * @throws \Exception
     */
    private function generateDateLabels(ReportChartData $chartData, $dateFilter)
    {
        $labels = [];

        switch($dateFilter) {
            case 'yearly':
                $dateInterval = 'P1Y';
                $this->dateFormat = 'Y';
                break;

            case 'weekly':
                $dateInterval = 'P7D';
                $this->dateFormat = 'W';
                break;

            case 'monthly':
                $dateInterval = 'P1M';
                $this->dateFormat = 'M';
                break;

            default:
                $dateInterval = 'P1D';
                $this->dateFormat = 'd-M';
                break;
        }

        $startDate = $chartData->getFromDate();
        $endDate = $chartData->getTillDate();

        $interval = new \DateInterval($dateInterval);
        $period = new \DatePeriod($startDate, $interval, $endDate);

        foreach($period as $value) {
            $labels[$value->format($this->dateFormat)] = 0;
        }

        return $labels;
    }
}