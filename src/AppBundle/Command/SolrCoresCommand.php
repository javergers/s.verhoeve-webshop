<?php

namespace AppBundle\Command;

use Guzzle\Http\Client as GuzzleClient;
use Solarium\Core\Client\Client as SolrClient;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SolrCoresCommand
 * @package AppBundle\Command
 */
class SolrCoresCommand extends ContainerAwareCommand
{
    /**
     * @var GuzzleClient
     */
    private $guzzleClient;

    /**
     * @var SolrClient
     */
    private $solrClient;

    protected function configure()
    {
        $this
            ->setName('topbloemen:solr-cores')
            ->addOption('missing', null, InputOption::VALUE_NONE,
                'List cores that are required by the application, but not available on the Solr server')
            ->addOption('required', null, InputOption::VALUE_NONE, 'List all cores required by the application')
            ->setDescription('List Solr cores (by default those available on Solr server)');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        if ($input->getOption('missing') && $input->getOption('required')) {
            throw new \RuntimeException('Options --missing and --required are mutually exclusive.');
        }

        if ($input->getOption('missing')) {
            $selectedCores = array_diff($this->getRequiredCores(), $this->getConfiguredCores());
        } elseif ($input->getOption('required')) {
            $selectedCores = $this->getRequiredCores();
        } else {
            $selectedCores = $this->getConfiguredCores();
        }

        foreach ($selectedCores as $selectedCore) {
            $output->writeln($selectedCore);
        }
    }

    /**
     * @return array
     */
    private function getConfiguredCores(): array
    {
        $response = $this->getGuzzleClient()->get('/solr/admin/cores', null, [
            'query' => [
                'action' => 'STATUS',
            ],
        ]);

        $cores = [];
        $body = (string)$response->getBody();

        // Check if response is json
        if ($body[0] === '{') {
            $response = json_decode($body, 1);
            foreach ($response['status'] as $core) {
                $cores[] = (string)$core['name'];
            }
            // Fallback use xml
        } else {
            $xml = new \SimpleXMLElement($body);

            foreach ($xml->xpath("/response/lst[@name='status']/lst") as $core) {
                $cores[] = (string)$core['name'];
            }
        }

        return $cores;
    }

    /**
     * @return array
     */
    private function getRequiredCores(): array
    {
        return array_keys($this->getSolrClient()->getEndpoints());
    }

    /**
     * @return SolrClient
     */
    private function getSolrClient(): SolrClient
    {
        if (!$this->solrClient) {
            $this->solrClient = $this->getContainer()->get('solr.client')->getClient();
        }

        return $this->solrClient;
    }

    /**
     * @return GuzzleClient
     */
    private function getGuzzleClient()
    {
        if (!$this->guzzleClient) {
            $this->guzzleClient = $this->getContainer()->get('eight_points_guzzle.client.solr');
        }

        return $this->guzzleClient;
    }


}
