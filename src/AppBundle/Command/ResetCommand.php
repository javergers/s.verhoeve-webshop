<?php

namespace AppBundle\Command;

use AppBundle\Services\Reset;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class ResetCommand
 * @package AppBundle\Command
 */
class ResetCommand extends ContainerAwareCommand
{
    /**
     * @var Reset
     */
    private $resetService;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * Configures the command
     */
    protected function configure(): void
    {
        $this
            ->setName('reset')
            ->setDescription('Reset project for development')
            ->addOption('list-methods', 'l', InputOption::VALUE_NONE, 'List all methods')
            ->addOption('filter', 'f', InputOption::VALUE_OPTIONAL, 'Excecute specific method', false);
    }

    /**
     * ResetCommand constructor.
     * @param ParameterBagInterface $parameterBag
     * @param Reset                 $resetService
     * @param null|string           $name
     */
    public function __construct(
        ParameterBagInterface $parameterBag,
        Reset $resetService,
        ?string $name = null)
    {
        parent::__construct($name);

        $this->resetService = $resetService;
        $this->parameterBag = $parameterBag;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return integer
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $helper = $this->getHelper('question');

        $environment = $this->parameterBag->get('kernel.environment');

        if ($environment === 'prod') {
            throw new \RuntimeException('The reset command can\'t be run in production');
        }

        $this->resetService->init($input, $output, $helper);

        if ($input->getOption('list-methods')) {
            return $this->resetService->listMethods();
        }

        if ((null !== ($method = $input->getOption('filter'))) && method_exists($this->resetService, $method)) {
            return $this->resetService->executeMethod($method);
        }

        return $this->resetService->execute();
    }
}
