<?php

namespace AppBundle\Command;

use AppBundle\Entity\Security\Customer\Customer;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * TODO Copernica code can be trashed
 * Class SyncCopernicaCommand
 * @package AppBundle\Command
 */
class SyncCopernicaCommand extends ContainerAwareCommand
{
    use  ContainerAwareTrait;

    protected function configure()
    {
        $this
            ->setName('topbloemen:customer:copernica:sync')
            ->setDescription('Sync customer to Copernica')
            ->addArgument('id', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        void($output);

        $customer = $this->getDoctrine()->getManager()->find(Customer::class, $input->getArgument('id'));

        $copernica = $this->getContainer()->get('copernica');

        if (!$copernica->customerExists($customer)) {
            $copernica->createCustomer($customer);
        } else {
            $copernica->updateCustomer($customer);
        }
    }

    /**
     * @return Registry
     */
    protected function getDoctrine()
    {
        return $this->container->get('doctrine');
    }
}