<?php

namespace AppBundle\Command\Solr;

use Solarium\Core\Client\Endpoint;
use Solarium\QueryType\Update\Query\Query as UpdateQuery;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Padam87\CronBundle\Annotation\Job as Cron;

/**
 * @Cron(minute="0", hour="23")
 */
class OptimizeCommand extends ContainerAwareCommand
{
    use ContainerAwareTrait;

    protected function configure()
    {
        $this
            ->setName('solr:optimize');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $endpoints = $this->getContainer()->get('solr.client')->getClient()->getEndpoints();

        foreach ($endpoints as $endpoint) {
            $this->optimize($endpoint);
        }

        return 0;
    }

    /**
     * @param Endpoint $endpoint
     */
    private function optimize(Endpoint $endpoint)
    {
        $client = $this->getContainer()->get('solr.client')->getClient();

        $query = new UpdateQuery();
        $query->addOptimize(true, false, 1);

        $request = $client->createRequest($query);

        $client->getAdapter()->execute($request, $endpoint);
    }
}