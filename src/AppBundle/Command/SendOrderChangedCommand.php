<?php

namespace AppBundle\Command;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Services\Mailer\OrderChangedMailer;
use AppBundle\Traits\CommandContextTrait;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SendOrderChangedCommand
 * @package AppBundle\Command
 */
class SendOrderChangedCommand extends Command
{
    use CommandContextTrait;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var OrderChangedMailer
     */
    private $orderChangedMailer;

    /**
     * SendConfirmationCommand constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param OrderChangedMailer $orderChangedMailer
     * @param null|string $name
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        OrderChangedMailer $orderChangedMailer,
        ?string $name = null
    ) {
        parent::__construct($name);
        $this->entityManager = $entityManager;
        $this->orderChangedMailer = $orderChangedMailer;
    }

    protected function configure()
    {
        $this
            ->setName('app:send-order-changed-mail')
            ->setDescription('Send order changed mail')
            ->addArgument('orderCollectionId', InputArgument::REQUIRED)
            ->addArgument('paymentId', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $orderCollection = $this->entityManager->find(
            OrderCollection::class, $input->getArgument('orderCollectionId')
        );

        if (null === $orderCollection) {
            return 1;
        }

        $payment = $this->entityManager->find(
            Payment::class, $input->getArgument('paymentId')
        );

        $this->defineContext($orderCollection->getSite());

        $mailSent = $this->orderChangedMailer->send([
            'orderCollection' => $orderCollection,
            'payment' => $payment
        ]);

        return $mailSent ? 0 : 1;
    }
}
