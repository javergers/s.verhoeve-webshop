<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Output\OutputInterface;

/**
 * Trait OutputColorTrait
 * @package AppBundle\Command
 */
trait OutputColorTrait
{
    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * @param              $mixed
     * @param array|string $color
     */
    public function debug($mixed, $color = 'white')
    {
        if (!$this->output->isVerbose()) {
            return;
        }

        if (\is_string($color)) {
            $colorString = 'fg=' . $color;
        } elseif (\is_array($color)) {
            array_walk($color, function (&$value, $key) {
                $value = $key . '=' . $value;
            });

            $colorString = implode(';', $color);
        }

        $this->output->writeln('<' . $colorString . '>' . $mixed . '</' . $colorString . '>');
    }
}
