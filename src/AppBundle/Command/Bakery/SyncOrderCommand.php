<?php

namespace AppBundle\Command\Bakery;

use AppBundle\Entity\Order\Order;
use AppBundle\Services\ConnectorHelper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SyncOrderCommand
 * @package AppBundle\Command\Bakery
 */
class SyncOrderCommand extends ContainerAwareCommand
{
    /**
     * @var ConnectorHelper
     */
    private $suppierConnectorHelper;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * SyncOrderCommand constructor
     * @param EntityManagerInterface   $entityManager
     * @param ConnectorHelper $supplierConnectorHelper
     */
    public function __construct(EntityManagerInterface $entityManager, ConnectorHelper $supplierConnectorHelper)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->suppierConnectorHelper = $supplierConnectorHelper;
    }

    protected function configure(): void
    {
        $this
            ->setName('bakery:sync-order')
            ->addArgument('id', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        void($output);

        $order = $this->getOrder((int)$input->getArgument('id'));

        if ($order->getSupplierOrder() === null) {
            throw new \RuntimeException('No supplier order');
        }

        if ($order->getSupplierOrder()->getConnector() !== 'Bakker') {
            throw new \RuntimeException('Supplier is no "bakker"');
        }

        $connector = $this->suppierConnectorHelper->getConnector($order->getSupplierOrder());
        if ($connector) {
            $connector->process($order->getSupplierOrder());
        }
    }

    /**
     * @param int $id
     * @return Order
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     */
    public function getOrder(int $id): Order
    {
        return $this->entityManager->find(Order::class, $id);
    }
}
