<?php

namespace AppBundle\Command\Auto;

use AppBundle\Entity\Order\Order;
use AppBundle\Services\Auto\OrderEvaluatorService;
use AppBundle\Traits\CommandContextTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ProcessOrderCommand
 * @package AppBundle\Command\Auto
 */
class ProcessOrderCommand extends Command
{
    use CommandContextTrait;

    /**
     * @var OrderEvaluatorService
     */
    private $orderEvaluatorService;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ProcessOrderCommand constructor.
     * @param OrderEvaluatorService  $orderEvaluatorService
     * @param EntityManagerInterface $entityManager
     * @param string|null            $name
     */
    public function __construct(OrderEvaluatorService $orderEvaluatorService, EntityManagerInterface $entityManager, ?string $name = null)
    {
        parent::__construct($name);
        $this->orderEvaluatorService = $orderEvaluatorService;
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setName('auto:process-order')
            ->addArgument('id', InputArgument::OPTIONAL)
            ->addOption('order-number', null, InputOption::VALUE_OPTIONAL)
            ->addOption('dry-run', null, InputOption::VALUE_NONE)
            ->addOption('debug', null, InputOption::VALUE_NONE)
            ->addOption('no-processing', null, InputOption::VALUE_NONE)
            ->addOption('force', null, InputOption::VALUE_NONE);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->defineAdminContext();

        if ($input->getArgument('id') && !$input->getOption('order-number')) {
            $order = $this->getOrder((int)$input->getArgument('id'));

            if (null === $order) {
                throw new \RuntimeException(sprintf('Order with id %d not found', $input->getArgument('id')));
            }
        } elseif (!$input->getArgument('id') && $input->getOption('order-number')) {
            if (!preg_match("/^\d{8}-\d{4}$/", $input->getOption('order-number'))) {
                throw new \RuntimeException(sprintf("'%s' isn't a valid order number", $input->getOption('order-number')));
            }
            $order = $this->entityManager->getRepository(Order::class)->findOneByOrderNumber($input->getOption('order-number'));

            if (null === $order) {
                throw new \RuntimeException(sprintf('Order with number %s not found', $input->getOption('order-number')));
            }
        } else {
            throw new InvalidArgumentException('Either one of input argument id or option order-number should be set, not both');
        }

        $dryRun = $input->getOption('dry-run');

        if ($dryRun && $output->getVerbosity() < OutputInterface::VERBOSITY_VERBOSE) {
            $output->setVerbosity(OutputInterface::VERBOSITY_VERBOSE);
        }

        if ($input->getOption('no-processing') && $input->getOption('force')) {
            throw new InvalidArgumentException('Either one of input options no-processing or option force should be set, not both');
        }

        $process = !$input->getOption('no-processing');
        $force = $input->getOption('force');

        $this->orderEvaluatorService
            ->setOutput($output)
            ->setDebug($input->getOption('debug'))
            ->process($order, $dryRun, $process, $force);

        return 0;
    }

    /**
     * @param int $id
     * @return Order
     */
    public function getOrder(int $id)
    {
        return $this->entityManager->find(Order::class, $id);
    }
}
