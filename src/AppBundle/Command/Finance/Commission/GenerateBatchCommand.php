<?php

namespace AppBundle\Command\Finance\Commission;

use AppBundle\Entity\Finance\CommissionBatch;
use AppBundle\Exceptions\ProductGroupPercentageNotSetException;
use AppBundle\Services\Commission\Batch\GeneratorService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateBatchCommand
 * @package AppBundle\Command\Finance\Commission
 */
class GenerateBatchCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var GeneratorService
     */
    private $generatorService;

    /**
     * GenerateBatchCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param GeneratorService       $generatorService
     */
    public function __construct(EntityManagerInterface $entityManager, GeneratorService $generatorService)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->generatorService = $generatorService;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:finance:commission:generate-batch')
            ->addArgument('id', InputArgument::OPTIONAL);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null
     * @throws ProductGroupPercentageNotSetException
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        void($output);

        /** @var CommissionBatch $commissionInvoice */
        $commissionBatch = $this->entityManager->getRepository(CommissionBatch::class)->find($input->getArgument('id'));

        if($commissionBatch) {
            $this->generatorService->generate($commissionBatch);
        } else {
            throw new \RuntimeException(sprintf('No commission batch found for ID: %d', $input->getArgument('id')));
        }

        return 0;
    }
}