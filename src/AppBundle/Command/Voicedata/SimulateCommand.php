<?php

namespace AppBundle\Command\Voicedata;

use libphonenumber\NumberParseException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SimulateCommand
 * @package AppBundle\Command\Voicedata
 */
class SimulateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('voicedata:simulate');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws NumberParseException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        void($input, $output);

        $voicedata = $this->getContainer()->get('voicedata');
        $voicedata->processXml('<?xml version="1.0" encoding="UTF-8" ?>
	<message>
	        <messageheader>
	                <debug>false</debug>
	                <msgtype>incoming</msgtype>
	                <msgversion>1.0</msgversion>
	                <msgidentifier></msgidentifier>
	                <errorcode>0</errorcode>
	                <errorcodedescription>ok</errorcodedescription>
	                <msgdatetime>2016-11-30T12:13:53</msgdatetime>
	        </messageheader>
	        <messagebody>
	                <clip>0686838733</clip>
	                <did>0881108040</did>
	                <extension>221</extension>
	                <prefix>(Geschenkbezorgen)</prefix>
	        </messagebody>
	</message>
');
    }
}