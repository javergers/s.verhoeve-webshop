<?php

namespace AppBundle\Command\Geography;

use AppBundle\Services\RegionData\Belgium;
use AppBundle\Services\RegionData\Germany;
use AppBundle\Services\RegionData\Luxembourg;
use AppBundle\Services\RegionData\Netherlands;
use Doctrine\DBAL\ConnectionException;
use GuzzleHttp\Exception\GuzzleException;
use Padam87\CronBundle\Annotation\Job;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @Job(minute="0", hour="6", day="1")
 */
class UpdateRegionDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:geography:update-region-data');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void
     * @throws GuzzleException
     * @throws ConnectionException
     * @throws \PHPExcel_Exception
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        void($output);

        $region = $this->getContainer()->get(Netherlands::class);
        $region->process();

        $region = $this->getContainer()->get(Belgium::class);
        $region->process();

        $region = $this->getContainer()->get(Luxembourg::class);
        $region->process();

        $region = $this->getContainer()->get(Germany::class);
        $region->process();
    }
}
