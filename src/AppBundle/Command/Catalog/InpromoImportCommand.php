<?php

namespace AppBundle\Command\Catalog;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class InpromoImportCommand
 * @package AppBundle\Command\Catalog
 */
class InpromoImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('catalog:impromo-import');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        void($input, $output);

        $this->getContainer()->get('supplier.inpromo')->import();
    }
}