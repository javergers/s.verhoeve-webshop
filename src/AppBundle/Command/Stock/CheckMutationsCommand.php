<?php

namespace AppBundle\Command\Stock;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Stock\Log;
use AppBundle\Manager\StockManager;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class CheckMutationsCommand
 * @package AppBundle\Command\Stock
 */
class CheckMutationsCommand extends ContainerAwareCommand
{
    use ContainerAwareTrait;

    protected function configure()
    {
        $this
            ->setName('stock:check-mutations')
            ->addArgument('since', InputArgument::REQUIRED)
            ->addOption('dryrun');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     * @throws OptimisticLockException
     * @throws InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $since = new \DateTime($input->getArgument('since'));

        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->getDoctrine()->getRepository(Order::class);

        $qb = $entityRepository->createQueryBuilder('order_order');
        $qb->andWhere("order_order.status = 'processed'");
        $qb->andWhere('order_order.createdAt >= :since');
        $qb->andWhere('order_order.deletedAt IS NULL');

        $qb->setParameter('since', $since);

        /** @var Order[] $orders */
        $orders = $qb->getQuery()->getResult();

        $stockManager = $this->container->get(StockManager::class);

        foreach ($orders as $order) {
            $orderOutputted = false;

            foreach ($order->getLines() as $line) {
                $product = $line->getProduct();

                if (!$product) {
                    continue;
                }

                if ($product->getParent() && !$product->getParent()->getHasStock()) {
                    continue;
                }

                if (!$product->getParent() && !$product->getHasStock()) {
                    continue;
                }

                $stockLog = $this->getDoctrine()->getRepository(Log::class)->findOneBy([
                    'order' => $order,
                    'product' => $line->getProduct(),
                ]);

                if (!$orderOutputted && $output->isVerbose()) {
                    $output->writeln('');
                    $output->writeln($order->getOrderNumber());

                    $orderOutputted = true;
                }

                $output->write(' ' . str_pad($line->getQuantity(), 3, ' ',
                        STR_PAD_LEFT) . ' x ' . $line->getProduct()->translate()->getTitle() . ':');

                if ($stockLog) {
                    $output->writeln(' <fg=green>reeds verwerkt</>');

                    continue;
                }

                $output->writeln(' <fg=red>mmissend</>');

                if ($input->getOption('dryrun')) {
                    continue;
                }

                $stockManager->subtractPhysicalStock($product, $line->getQuantity(), $order, 'order_processed');
            }
        }

        return 0;
    }

    /**
     * @return Registry
     */
    private function getDoctrine()
    {
        return $this->container->get('doctrine');
    }
}
