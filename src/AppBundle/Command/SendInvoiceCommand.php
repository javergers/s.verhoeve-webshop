<?php

namespace AppBundle\Command;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Services\Mailer\InvoiceMailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class SendInvoiceCommand
 * @package AppBundle\Command
 */
class SendInvoiceCommand extends Command
{
    use ContainerAwareTrait;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var InvoiceMailer
     */
    private $invoiceMailer;

    /**
     * SendInvoiceCommand constructor.
     * @param null|string            $name
     * @param EntityManagerInterface $entityManager
     * @param InvoiceMailer          $invoiceMailer
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        InvoiceMailer $invoiceMailer,
        ?string $name = null
    ) {
        parent::__construct($name);

        $this->entityManager = $entityManager;
        $this->invoiceMailer = $invoiceMailer;
    }

    /**
     * @todo: Rename command name.
     */
    protected function configure()
    {
        $this
            ->setName('topbloemen:shop:send-invoice')
            ->setDescription('Send order invoice')
            ->addArgument('id', InputArgument::REQUIRED)
            ->addArgument('to', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $orderCollection = $this->entityManager->find(OrderCollection::class, $input->getArgument('id'));

        if (null !== $orderCollection) {
            $this->invoiceMailer->send([
                'orderCollection' => $orderCollection,
                'to' => $input->getArgument('to'),
            ]);
        }
    }
}
