<?php

namespace AppBundle\Command\Robin;

use AppBundle\Entity\Security\Customer\Customer;
use Doctrine\Common\Collections\ArrayCollection;
use GuzzleHttp\Exception\ClientException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SyncCustomerCommand
 * @package AppBundle\Command\Robin
 */
class SyncCustomerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('robin:sync-customer')
            ->addArgument('id', InputArgument::OPTIONAL)
            ->addOption('all');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('all')) {
            if (!$input->getArgument('id')) {
                throw new RuntimeException('Not enough arguments (missing: "id").');
            }

            $contact = $this->getCustomer((int)$input->getArgument('id'));

            $this->getContainer()->get('robin')->syncCustomer($contact);
        } else {
            $customers = $this->getCustomers();

            $progress = new ProgressBar($output, \count($customers));
            $progress->start();

            $errors = [];

            while ($progress->getProgress() < \count($customers)) {
                try {
                    $this->getContainer()->get('robin')->syncCustomer($customers[$progress->getProgress()]);
                } catch (ClientException $e) {
//                    if ($e->getCode() == 429) {
//                        /** You can call the Teamleader API a maximum of 25 times every 5 seconds. */
//
//                        sleep(5);
//                        continue;
//                    }

                    if ($output->isVerbose() && $e->getResponse() !== null) {
                        $result = json_decode($e->getResponse()->getBody()->getContents());
                        $errors[$customers[$progress->getProgress()]->getId()] = $result->reason;
                    }
                }

                $progress->advance();
            }

            $progress->finish();

            $output->writeln('');

            if ($errors && $output->isVerbose()) {
                $output->writeln('<error>Failures:     </error>');

                foreach ($errors as $id => $value) {
                    $output->writeln(str_pad($id, 5, ' ', STR_PAD_LEFT) . ': ' . $value);
                }
            }
        }

        return null;
    }

    /**
     * @return ArrayCollection|Customer[]
     */
    public function getCustomers()
    {
        /** @var ArrayCollection $customers */
        $customers = new ArrayCollection($this->getContainer()->get('doctrine')->getManager()->getRepository(Customer::class)->findAll());

        return array_values($customers->toArray());
    }

    /**
     * @param int $id
     * @return Customer
     */
    public function getCustomer(int $id)
    {
        return $this->getContainer()->get('doctrine')->getManager()->find(Customer::class, $id);
    }
}
