<?php

namespace AppBundle\Command\Robin;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Services\Robin;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;
use GuzzleHttp\Exception\ClientException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SyncOrderCommand
 * @package AppBundle\Command\Robin
 */
class SyncOrderCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Robin
     */
    private $robin;

    /**
     * SyncOrderCommand constructor
     * @param EntityManagerInterface $entityManager
     * @param Robin         $robin
     */
    public function __construct(EntityManagerInterface $entityManager, Robin $robin)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->robin = $robin;
    }

    protected function configure(): void
    {
        $this
            ->setName('robin:sync-order')
            ->addArgument('id', InputArgument::OPTIONAL)
            ->addOption('all');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('all')) {
            if (!$input->getArgument('id')) {
                throw new RuntimeException('Not enough arguments (missing: "id").');
            }

            $order = $this->getOrder((int)$input->getArgument('id'));
            if (!$order) {
                throw new RuntimeException('Order not found.');
            }

            $this->robin->syncOrder($order);
        } else {
            $orders = $this->getOrders();
            $totalOrders = \count($orders);

            $progress = new ProgressBar($output, $totalOrders);
            $progress->start();

            $errors = [];

            while ($progress->getProgress() < $totalOrders) {
                try {
                    $this->robin->syncOrder($orders[$progress->getProgress()]);
                } catch (ClientException $e) {
                    if ($output->isVerbose() && null !== $e->getResponse()) {
                        $result = json_decode($e->getResponse()->getBody()->getContents());
                        $errors[$orders[$progress->getProgress()]->getId()] = $result->reason;
                    }
                }

                $progress->advance();
            }

            $progress->finish();

            $output->writeln('');

            if (!empty($errors) && $output->isVerbose()) {
                $output->writeln('<error>Failures:     </error>');

                foreach ($errors as $id => $value) {
                    $output->writeln(str_pad($id, 5, ' ', STR_PAD_LEFT) . ': ' . $value);
                }
            }
        }
    }

    /**
     * @return OrderCollection[]
     */
    public function getOrders(): array
    {
        /** @var ArrayCollection $orderCollection */
        $orderCollection = new ArrayCollection($this->entityManager->getRepository(OrderCollection::class)->findAll());

        return array_values($orderCollection->toArray());
    }

    /**
     * @param int $id
     * @return null|OrderCollection
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     */
    public function getOrder(int $id): ?OrderCollection
    {
        return $this->entityManager->find(OrderCollection::class, $id);
    }
}
