<?php

namespace AppBundle\Command\Payment;

use AppBundle\Entity\Payment\Payment;
use AppBundle\Services\Mailer\PaymentRequestMailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class SendPaymentRequestCommand
 * @package AppBundle\Command\Payment
 */
class SendPaymentRequestCommand extends ContainerAwareCommand
{
    use ContainerAwareTrait;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var PaymentRequestMailer */
    private $paymentRequestMailer;

    /**
     * SendPaymentRequestCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param PaymentRequestMailer   $paymentRequestMailer
     */
    public function __construct(EntityManagerInterface $entityManager, PaymentRequestMailer $paymentRequestMailer)
    {
        $this->entityManager = $entityManager;

        $this->paymentRequestMailer = $paymentRequestMailer;

        parent::__construct();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('app:payment:send-request')
            ->addArgument('id', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $payment = $this->entityManager
            ->getRepository(Payment::class)
            ->find($input->getArgument('id'));

        $this->paymentRequestMailer->send([
            'payment' => $payment
        ]);

        return 0;
    }
}
