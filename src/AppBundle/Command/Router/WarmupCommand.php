<?php

namespace AppBundle\Command\Router;

use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * Class WarmupCommand
 * @package AppBundle\Command\Router
 */
class WarmupCommand extends ContainerAwareCommand
{
    use ContainerAwareTrait;

    protected function configure(): void
    {
        $this->setName('router:warmup');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null
     * @throws InvalidArgumentException
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        void($input, $output);

        // Force an update of the dynamic routes
        $this->getContainer()->get('dynamic_routes')->build();

        $filesystem = new Filesystem();

        $finder = new Finder();
        $finder
            ->in($this->container->getParameter('kernel.cache_dir'))
            ->depth(0)
            ->files()
            ->name('/UrlGenerator|UrlMatcher/');

        foreach ($finder as $fileInfo) {
            $filesystem->remove($fileInfo->getRealPath());
        }

        $this->getContainer()->get('router')->warmUp($this->getContainer()->getParameter('kernel.cache_dir'));

        return 0;
    }
}