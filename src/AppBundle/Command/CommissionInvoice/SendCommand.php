<?php

namespace AppBundle\Command\CommissionInvoice;

use AppBundle\Entity\Finance\CommissionInvoice;
use AppBundle\Services\Commission\InvoiceService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class SendCommand
 * @package AppBundle\Command\CommissionInvoice
 */
class SendCommand extends ContainerAwareCommand
{
    use ContainerAwareTrait;

    protected function configure()
    {
        $this
            ->setName('commission-invoice:send')
            ->addArgument('id', InputArgument::OPTIONAL);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        void($output);

        $this->getContainer()->get('liip_theme.active_theme')->setThemes([
            'topgeschenken',
        ]);

        /** @var CommissionInvoice $commissionInvoice */
        $commissionInvoice = $this->getContainer()->get('doctrine')->getManager()->getRepository(CommissionInvoice::class)->find($input->getArgument('id'));

        $this->getContainer()->get(InvoiceService::class)->setCommissionInvoice($commissionInvoice)->send();

        return 0;
    }
}
