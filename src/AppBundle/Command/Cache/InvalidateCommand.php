<?php

namespace AppBundle\Command\Cache;

use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use AppBundle\Cache\Adapter\TagAwareAdapter;

/**
 * Class InvalidateCommand
 * @package AppBundle\Command\Cache
 */
class InvalidateCommand extends Command
{
    /**
     * @var TagAwareAdapter
     */
    private $cache;

    /**
     * SyncOrderCommand constructor
     * @param TagAwareAdapter $cache
     */
    public function __construct(TagAwareAdapter $cache)
    {
        parent::__construct();

        $this->cache = $cache;
    }

    /**
     * {@inheritDoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('cache:invalidate')
            ->addArgument('tags', InputArgument::IS_ARRAY | InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        try {
            $this->cache->invalidateTags($input->getArgument('tags'));

            $exitCode = 0;
        } catch (InvalidArgumentException $e) {
            $exitCode = 1;
        }

        $io = new SymfonyStyle($input, $output);

        $tagsConcat = implode(', ', $input->getArgument('tags'));

        if (0 !== $exitCode) {
            $io->error(sprintf('Some errors while invalidating the cache for tag(s) "%s"', $tagsConcat));
        } else {
            $io->success(sprintf('Cache for the tag(s) "%s" was succesfully invalidated', $tagsConcat));
        }
    }
}
