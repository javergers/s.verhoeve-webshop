<?php

namespace AppBundle\Command;

use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Geography\Holiday;
use AppBundle\Entity\Geography\HolidayCountry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Padam87\CronBundle\Annotation\Job;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yasumi\Filters\OfficialHolidaysFilter;
use Yasumi\Yasumi;

/**
 * Class GenerateHolidaysCommand
 * @package AppBundle\Command
 *
 * @Job(minute="0", hour="6", day="1")
 */
class GenerateHolidaysCommand extends ContainerAwareCommand
{
    /**
     * @var ArrayCollection|Holiday[]
     */
    private $holidays;

    protected function configure()
    {
        $this->setName('holidays:generate');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void
     * @throws \ReflectionException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        void($input, $output);

        for ($year = date('Y'); $year <= date('Y') + 2; $year++) {
            $this->holidays = $this->getExistingHolidays($year);

            $this->dutchHolidays($year);
            $this->belgiumHolidays($year);

            foreach ($this->holidays as $holiday) {
                $holiday->mergeNewTranslations();
            }
        }

        $this->getContainer()->get('doctrine')->getManager()->flush();
    }

    /**
     * @param \DateTime $date
     * @return Holiday
     */
    private function getEntityForHoliday(\DateTime $date)
    {
        $entity = $this->holidays->filter(function (Holiday $holiday) use ($date) {
            return ($holiday->getDate() == $date);
        })->current();

        if (!$entity) {
            $entity = new Holiday();
            $entity->setDate($date);

            $this->getContainer()->get('doctrine')->getManager()->persist($entity);

            $this->holidays->add($entity);
        }

        return $entity;
    }

    /**
     * @param integer $year
     * @throws \ReflectionException
     */
    private function dutchHolidays(int $year)
    {
        $nl = $this->getContainer()->get('doctrine')->getRepository(Country::class)->find('NL');

        $holidays = Yasumi::create('Netherlands', $year);

        foreach (new OfficialHolidaysFilter($holidays->getIterator()) as $holiday) {
            $entity = $this->getEntityForHoliday($holiday);
            $entity->translate('en_US')->setName($holiday->translations['en_US']);
            $entity->translate('nl_NL')->setName($holiday->translations['nl_NL']);

            if (!$entity->getCountries()->exists(function ($key, HolidayCountry $holidayCountry) use ($nl) {
                void($key);

                return $holidayCountry->getCountry() === $nl;
            })) {
                $holidayCountry = new HolidayCountry();
                $holidayCountry->setHoliday($entity);
                $holidayCountry->setCountry($nl);

                $entity->addCountry($holidayCountry);
            }
        }
    }

    /**
     * @param integer $year
     * @throws \ReflectionException
     */
    private function belgiumHolidays(int $year)
    {
        $be = $this->getContainer()->get('doctrine')->getRepository(Country::class)->find('BE');

        $holidays = Yasumi::create('Belgium', $year);

        foreach (new OfficialHolidaysFilter($holidays->getIterator()) as $holiday) {
            $entity = $this->getEntityForHoliday($holiday);
            $entity->translate('en_US')->setName($holiday->translations['en_US']);
            $entity->translate('nl_BE')->setName($holiday->translations['nl_BE']);

            if (array_key_exists('fr_FR', $holiday->translations)) {
                $entity->translate('fr_BE')->setName($holiday->translations['fr_FR']);
            }

            if (!$entity->getCountries()->exists(function ($key, HolidayCountry $holidayCountry) use ($be) {
                void($key);

                return $holidayCountry->getCountry() === $be;
            })) {
                $holidayCountry = new HolidayCountry();
                $holidayCountry->setHoliday($entity);
                $holidayCountry->setCountry($be);

                $entity->addCountry($holidayCountry);
            }
        }
    }

    /**
     * @param $year
     * @return ArrayCollection
     */
    private function getExistingHolidays($year)
    {
        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->getContainer()->get('doctrine')->getManager()->getRepository(Holiday::class);

        $q = $entityRepository->createQueryBuilder('h');
        $q->andWhere('h.date >= :start');
        $q->andWhere('h.date <= :end');
        $q->setParameter('start', new \DateTime($year . '-01-01 00:00:00'));
        $q->setParameter('end', new \DateTime($year . '-12-31 00:00:00'));

        return new ArrayCollection($q->getQuery()->getResult());
    }
}
