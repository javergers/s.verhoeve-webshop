<?php

namespace AppBundle\Command\Teamleader;

use AppBundle\Entity\Relation\Company;
use AppBundle\Services\Teamleader;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SyncCompanyCommand
 * @package AppBundle\Command\Teamleader
 */
class SyncCompanyCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Teamleader
     */
    private $teamleader;

    /**
     * SyncOrderCommand constructor
     * @param EntityManagerInterface $entityManager
     * @param Teamleader    $teamleader
     */
    public function __construct(EntityManagerInterface $entityManager, Teamleader $teamleader)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->teamleader = $teamleader;
    }

    protected function configure(): void
    {
        $this
            ->setName('teamleader:sync-company')
            ->addArgument('id', InputArgument::OPTIONAL)
            ->addOption('all');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        if (!$input->getOption('all')) {
            if (!$input->getArgument('id')) {
                throw new RuntimeException('Not enough arguments (missing: "id").');
            }

            try {
                $company = $this->getCompany((int)$input->getArgument('id'));
            } catch (EntityNotFoundException $e) {
                return 1;
            }

            $this->teamleader->syncCompany($company);
        } else {
            $companies = $this->getCompanies();
            $totalCompanies = \count($companies);

            $progress = new ProgressBar($output, $totalCompanies);
            $progress->start();

            $errors = [];

            while ($progress->getProgress() < $totalCompanies) {
                try {
                    $this->teamleader->syncCompany($companies[$progress->getProgress()]);
                } catch (ClientException $e) {
                    if ($e->getCode() === 429) {
                        /** You can call the Teamleader API a maximum of 25 times every 5 seconds. */

                        sleep(5);
                        continue;
                    }

                    if ($output->isVerbose() && null !== $e->getResponse()) {
                        $result = json_decode($e->getResponse()->getBody()->getContents());
                        $errors[$companies[$progress->getProgress()]->getId()] = $result->reason;
                    }
                } catch (\Exception $e) {
                    if ($output->isVerbose()) {
                        $errors[$companies[$progress->getProgress()]->getId()] = $e->getMessage();
                    }
                }

                $progress->advance();
            }

            $progress->finish();

            $output->writeln('');

            if (!empty($errors) && $output->isVerbose()) {
                $output->writeln('<error>Failures:     </error>');

                foreach ($errors as $id => $value) {
                    $output->writeln(str_pad($id, 5, ' ', STR_PAD_LEFT) . ': ' . $value);
                }
            }
        }

        return 0;
    }

    /**
     * @return Company[]
     */
    public function getCompanies(): array
    {
        /** @var ArrayCollection $companies */
        $companies = new ArrayCollection($this->entityManager->getRepository(Company::class)->findAll());

        return array_values($companies->toArray());
    }

    /**
     * @param int $id
     * @return Company
     * @throws EntityNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     */
    public function getCompany(int $id): Company
    {
        $company = $this->entityManager->find(Company::class, $id);

        if (!$company) {
            throw new EntityNotFoundException("Company doesn't exists");
        }

        return $company;
    }
}
