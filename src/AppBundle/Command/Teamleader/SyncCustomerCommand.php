<?php

namespace AppBundle\Command\Teamleader;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Services\Teamleader;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;
use GuzzleHttp\Exception\ClientException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SyncCustomerCommand
 * @package AppBundle\Command\Teamleader
 */
class SyncCustomerCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Teamleader
     */
    private $teamleader;

    /**
     * SyncOrderCommand constructor
     * @param EntityManagerInterface $entityManager
     * @param Teamleader    $teamleader
     */
    public function __construct(EntityManagerInterface $entityManager, Teamleader $teamleader)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->teamleader = $teamleader;
    }

    protected function configure(): void
    {
        $this
            ->setName('teamleader:sync-customer')
            ->addArgument('id', InputArgument::OPTIONAL)
            ->addOption('all');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$input->getOption('all')) {
            if (!$input->getArgument('id')) {
                throw new RuntimeException('Not enough arguments (missing: "id").');
            }

            try {
                $contact = $this->getCustomer((int)$input->getArgument('id'));
            } catch (EntityNotFoundException $e) {
                return 1;
            }

            $this->teamleader->syncCustomer($contact);
        } else {
            $customers = $this->getCustomers();
            $totalCustomers = \count($customers);

            $progress = new ProgressBar($output, $totalCustomers);
            $progress->start();

            $errors = [];

            while ($progress->getProgress() < $totalCustomers) {
                try {
                    $this->teamleader->syncCustomer($customers[$progress->getProgress()]);
                } catch (ClientException $e) {
                    if ($e->getCode() === 429) {
                        /** You can call the Teamleader API a maximum of 25 times every 5 seconds. */

                        sleep(5);
                        continue;
                    }

                    if ($output->isVerbose() && null === $e->getResponse()) {
                        $result = json_decode($e->getResponse()->getBody()->getContents());
                        $errors[$customers[$progress->getProgress()]->getId()] = $result->reason;
                    }
                } catch (\Exception $e) {
                    if ($output->isVerbose()) {
                        $errors[$customers[$progress->getProgress()]->getId()] = $e->getMessage();
                    }
                }

                $progress->advance();
            }

            $progress->finish();

            $output->writeln('');

            if (!empty($errors) && $output->isVerbose()) {
                $output->writeln('<error>Failures:     </error>');

                foreach ($errors as $id => $value) {
                    $output->writeln(str_pad($id, 5, ' ', STR_PAD_LEFT) . ': ' . $value);
                }
            }
        }

        return 0;
    }

    /**
     * @return Customer[]
     */
    public function getCustomers(): array
    {
        /** @var ArrayCollection $customers */
        $customers = new ArrayCollection($this->entityManager->getRepository(Customer::class)->findAll());
        $customers = $customers->filter(function (Customer $customer) {
            if (!$customer->getCompany()) {
                return false;
            }

            return true;
        });

        return array_values($customers->toArray());
    }

    /**
     * @param int $id
     * @return Customer
     * @throws EntityNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     */
    public function getCustomer(int $id): Customer
    {
        $customer = $this->entityManager->find(Customer::class, $id);

        if (!$customer) {
            throw new EntityNotFoundException("Customer doesn't exist");
        }

        return $customer;
    }
}
