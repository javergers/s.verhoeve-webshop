<?php

namespace AppBundle\Command;

use AppBundle\Entity\Site\Domain;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Class AbstractMailCommand
 * @package AppBundle\Command
 */
abstract class AbstractMailCommand extends ContainerAwareCommand
{
    /**
     * @var Domain
     */
    protected $domain;

    public function init()
    {
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost($this->domain->getDomain());
        $context->setScheme($this->domain->getSite()->getHttpScheme());

        if ($this->domain->getSite()->getHttpPort()) {
            if ($this->domain->getSite()->getHttpScheme() === 'https') {
                $context->setHttpsPort($this->domain->getSite()->getHttpPort());
            } else {
                $context->setHttpPort($this->domain->getSite()->getHttpPort());
            }
        }
    }
}