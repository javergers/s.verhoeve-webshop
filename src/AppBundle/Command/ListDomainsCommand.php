<?php

namespace AppBundle\Command;

use AppBundle\Entity\Site\Domain;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ListDomainsCommand
 * @package AppBundle\Command
 */
class ListDomainsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('topbloemen:site:list-domains');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        void($input, $output);

        /** @var Domain[] $domains */
        $domains = $this->getContainer()->get('doctrine')->getManager()->getRepository(Domain::class)->findAll();

        foreach ($domains as $domain) {
            $output->writeln($domain->getDomain());
        }
    }
}
