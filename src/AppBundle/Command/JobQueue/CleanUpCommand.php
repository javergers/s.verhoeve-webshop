<?php

namespace AppBundle\Command\JobQueue;

use JMS\JobQueueBundle\Command\CleanUpCommand as BaseCleanUpCommand;
use Padam87\CronBundle\Annotation\Job;
use Symfony\Component\Console\Input\InputOption;

/**
 * Class CleanUpCommand
 * @package AppBundle\Command\JobQueue
 *
 * @Job(minute="21", hour="*")
 */
class CleanUpCommand extends BaseCleanUpCommand
{
    protected function configure()
    {
        $this
            ->setName('job-queue:clean-up')
            ->setDescription('Cleans up jobs which exceed the maximum retention time.')
            ->addOption('max-retention', null, InputOption::VALUE_REQUIRED,
                'The maximum retention time (value must be parsable by DateTime).', '7 days')
            ->addOption('max-retention-succeeded', null, InputOption::VALUE_REQUIRED,
                'The maximum retention time for succeeded jobs (value must be parsable by DateTime).', '1 hour')
            ->addOption('per-call', null, InputOption::VALUE_REQUIRED,
                'The maximum number of jobs to clean-up per call.', 1000);
    }
}
