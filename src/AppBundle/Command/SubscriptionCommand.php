<?php

namespace AppBundle\Command;

use AppBundle\Entity\Order\Subscription;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class SubscriptionCommand
 * @package AppBundle\Command
 */
class SubscriptionCommand extends ContainerAwareCommand
{
    use ContainerAwareTrait;

    protected function configure()
    {
        $this
            ->setName('topbloemen:shop:subscriptions')
            ->setDescription('Process subscriptions');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        void($input, $output);

        $today = new \DateTime();

        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->getDoctrine()->getManager()->getRepository(Subscription::class);

        $qb = $entityRepository->createQueryBuilder('r');
        $qb->andWhere('r.nextDate <= :today');
        $qb->setParameter('today', $today->format('Y-m-d'));

        $query = $qb->getQuery();

        /** @var Subscription[] $results */
        $results = $query->getResult();

        foreach ($results as $result) {
            print $result->getId() . PHP_EOL;
        }
    }

    /**
     * @return Registry
     */
    private function getDoctrine()
    {
        return $this->container->get('doctrine');
    }
}