<?php

namespace AppBundle\Command\Sync;

use AppBundle\Services\Sync\FloristDeliveryAreaService;
use Exception;
use Padam87\CronBundle\Annotation\Job;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FloristDeliveryAreasCommand
 * @package AppBundle\Command\Sync
 *
 * @Job(minute="0", hour="2")
 */
class FloristDeliveryAreasCommand extends Command
{
    /**
     * @var FloristDeliveryAreaService
     */
    private $floristDeliveryAreaService;

    /**
     * FloristDeliveryAreasCommand constructor.
     * @param FloristDeliveryAreaService $floristDeliveryAreaService
     * @param null|string                $name
     */
    public function __construct(FloristDeliveryAreaService $floristDeliveryAreaService, ?string $name = null)
    {
        parent::__construct($name);

        $this->floristDeliveryAreaService = $floristDeliveryAreaService;
    }

    protected function configure()
    {
        $this
            ->setName('sync:florists-delivery-areas')
            ->setDescription('Sync florist delivery areas')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->floristDeliveryAreaService
            ->setInput($input)
            ->setOutput($output)
            ->import()
        ;
    }
}