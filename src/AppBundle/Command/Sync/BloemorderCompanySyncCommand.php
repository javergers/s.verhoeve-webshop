<?php

namespace AppBundle\Command\Sync;

use AppBundle\Entity\Relation\Company;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class BloemorderCompanySyncCommand
 * @package AppBundle\Command\Sync
 */
class BloemorderCompanySyncCommand extends ContainerAwareCommand
{
    use ContainerAwareTrait;

    protected function configure()
    {
        $this
            ->setName('sync:bloemorder:companies')
            ->addArgument('id', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $id = $input->getArgument('id');

        /** @var Company $company */
        $company = $this->getContainer()->get('doctrine')
            ->getRepository(Company::class)
            ->find($id);

        if (!$company) {
            $io->error('Company not found.');
        } elseif (!isset($company->getMetadata()['bloemorderCompanyId'])) {
            $io->error('Company `' . $id . '` isn\'t imported from bloemorder.');
        } else {
            $io->writeln($this->getContainer()->get('sync.bloemorder')->companySync($company));
        }


    }
}