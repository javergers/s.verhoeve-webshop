<?php

namespace AppBundle\Command\Sync;

use AppBundle\Entity\Security\Customer\Customer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class BloemorderCustomerSyncCommand
 * @package AppBundle\Command\Sync
 */
class BloemorderCustomerSyncCommand extends ContainerAwareCommand
{
    use ContainerAwareTrait;

    protected function configure()
    {
        $this
            ->setName('sync:bloemorder:customers')
            ->addArgument('id', InputArgument::REQUIRED);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $id = $input->getArgument('id');

        /** @var Customer $customer */
        $customer = $this->getContainer()->get('doctrine')
            ->getRepository(Customer::class)
            ->find($id);

        if (!$customer) {
            $io->error('Customer not found.');
        } elseif (!$customer->getCompany() || !isset($customer->getCompany()->getMetadata()['bloemorderCompanyId'])) {
            $io->error('Customer `' . $id . '` isn\'t imported from bloemorder.');
        } else {
            $io->writeln($this->getContainer()->get('sync.bloemorder')->customerSync($customer));
        }
    }
}