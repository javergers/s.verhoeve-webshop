<?php

namespace AppBundle\Command\Sync;

use AppBundle\Services\Sync\FloristTimeslots;
use Padam87\CronBundle\Annotation\Job;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FloristTimeslotsCommand
 * @package AppBundle\Command\Sync
 *
 * @Job(minute="21", hour="5")
 */
class FloristTimeslotsCommand extends Command
{
    /**
     * @var FloristTimeslots
     */
    private $floristTimeslots;

    /**
     * @var LoggerInterface
     */
    private $logger;

    protected function configure()
    {
        $this->setName('sync:florist:timeslots');
    }

    /**
     * FloristTimeslotsCommand constructor.
     * @param FloristTimeslots $floristTimeslots
     * @param LoggerInterface  $logger
     * @param null             $name
     */
    public function __construct(
        FloristTimeslots $floristTimeslots,
        LoggerInterface $logger,
        $name = null)
    {
        parent::__construct($name);

        $this->floristTimeslots = $floristTimeslots;
        $this->logger = $logger;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->floristTimeslots->setOutput($output)->import();

        return 0;
    }
}
