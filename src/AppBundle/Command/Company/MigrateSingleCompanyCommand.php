<?php

namespace AppBundle\Command\Company;

use AppBundle\Entity\Relation\Company;
use AppBundle\Services\Company\CompanyRegistrationService;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class MigrateSingleCompanyCommand
 * @package AppBundle\Command\Company
 */
class MigrateSingleCompanyCommand extends ContainerAwareCommand
{
    use ContainerAwareTrait;

    protected function configure()
    {
        $this
            ->setName('company:migrate:single ')
            ->setDescription('Migrate single company')
            ->addArgument('companyId', InputArgument::REQUIRED, 'The ID of the company');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('companyId');

        /** @var Company $company */
        $company = $this->getDoctrine()->getRepository(Company::class)->find($id);

        if ($company === null) {
            throw new \RuntimeException(sprintf("Company width id '%d' not found", $id));
        }

        try {
            $this->getContainer()->get(CompanyRegistrationService::class)->migrateCompany($company);
            $this->getDoctrine()->getManager()->flush();
        } catch(\Exception $exception) {
            $output->writeln($exception->getMessage());
        }
    }

    /**
     * @return Registry
     */
    private function getDoctrine()
    {
        return $this->container->get('doctrine');
    }
}
