<?php

namespace AppBundle\Command\Company;

use AppBundle\Services\Company\CompanyRegistrationService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class MigrateCompanyCommand
 * @package AppBundle\Command\Company
 */
class MigrateCompanyCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('company:migrate:batch')
            ->setDescription('Migrate companies')
            ->addArgument('limit', InputArgument::OPTIONAL, 'Optional limit for the amount of companies to migrate');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        void($output);

        $limit = null;

        if ($input->getArgument('limit')) {
            $limit = $input->getArgument('limit');
        }

        $messages = $this->getContainer()->get(CompanyRegistrationService::class)->migrateCompanyRegistrations($limit);

        $output->write($messages);
    }
}
