<?php

namespace AppBundle\Command;

use AppBundle\ThirdParty\Kiyoh\Entity\KiyohCompany;
use AppBundle\Services\Webservices\Kiyoh;
use Doctrine\ORM\EntityManagerInterface;
use Padam87\CronBundle\Annotation\Job;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class KiyohSyncIncrementalCommand
 * @package AppBundle\Command
 *
 * @Job(minute="*\/5", hour="7-23")
 * @Job(minute="55", hour="6", commandLine="topbloemen:base:kiyoh:sync --full")
 */
class KiyohSynchronizeCommand extends ContainerAwareCommand
{
    use OutputColorTrait;

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * @var Kiyoh $kiyohWebservice
     */
    private $kiyohWebservice;

    protected function configure()
    {
        $this
            ->setName('topbloemen:base:kiyoh:sync')
            ->setDescription('Command to update Kiyoh reviews incrementally')
            ->addArgument('id', InputArgument::OPTIONAL, 'Kiyoh company identifier')
            ->addOption('full', null, InputOption::VALUE_NONE, 'Synchronize all reviews')
            ->addOption('ignore-sync-flag', null, InputOption::VALUE_NONE, 'Ignore synchronize flag from database');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->entityManager = $this->getContainer()->get('doctrine');
        $this->kiyohWebservice = $this->getContainer()->get('kiyoh');

        if ($input->getArgument('id')) {
            $whereAttributes = [
                'identifier' => $input->getArgument('id'),
            ];

            if (!$input->getOption('ignore-sync-flag')) {
                $whereAttributes['syncEnabled'] = true;
            }

            $companies = $this->entityManager->getRepository(KiyohCompany::class)->findBy($whereAttributes);
        } else {
            $whereAttributes = [];

            if (!$input->getOption('ignore-sync-flag')) {
                $whereAttributes['syncEnabled'] = true;
            }

            $companies = $this->entityManager->getRepository(KiyohCompany::class)->findBy($whereAttributes);
        }

        if (!$companies) {
            $this->debug('No Kiyoh companies are set to synchronize reviews.', 'yellow');

            return;
        }

        foreach ($companies as $company) {
            /** @var KiyohCompany $company */
            $this->debug('KiyohCompany', 'green');
            $this->debug('Id: ' . $company->getIdentifier(), 'white');
            $this->debug('Name:' . $company->getName(), 'white');

            $totalSyncedReviews = $this->kiyohWebservice->synchronize($company,
                ((!$input->getOption('full')) ? Kiyoh::SYNC_TYPE_INCREMENTAL : Kiyoh::SYNC_TYPE_FULL));

            $this->debug('  -> ' . $totalSyncedReviews . ' review(s) gesynchroniseerd.', 'white');
        }
    }
}
