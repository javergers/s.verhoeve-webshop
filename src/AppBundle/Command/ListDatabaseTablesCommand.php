<?php

namespace AppBundle\Command;

use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ListDatabaseTablesCommand
 * @package AppBundle\Command
 */
class ListDatabaseTablesCommand extends ContainerAwareCommand
{
    private const SOFT_DELETABLE_ACTIVE = '1';
    private const SOFT_DELETABLE_INACTIVE = '0';

    private static $deletedAtColumnName = 'deleted_at';

    protected function configure()
    {
        $this->setName('topbloemen:base:list-database-tables')
            ->setDescription('Get all database table names based on entities managed by Doctrine')
            ->addOption('soft-deletable', null, InputOption::VALUE_REQUIRED,
                'Filter results based on soft deletable behaviour', null);
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $tables = [];

        /** @var AbstractSchemaManager $schemaManager */
        $schemaManager = $this->getContainer()->get('doctrine')->getConnection()->getSchemaManager();

        foreach ($schemaManager->listTables() as $table) {
            if (null !== $input->getOption('soft-deletable') && $input->getOption('soft-deletable')) {
                if (self::SOFT_DELETABLE_ACTIVE && !array_key_exists(self::$deletedAtColumnName,
                        $table->getColumns())) {
                    continue;
                }
                if (self::SOFT_DELETABLE_INACTIVE && array_key_exists(self::$deletedAtColumnName,
                        $table->getColumns())) {
                    continue;
                }
            }

            $tables[] = $table->getName();
        }

        sort($tables);

        $output->writeln($tables);
    }
}
