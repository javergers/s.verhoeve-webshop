<?php

namespace AppBundle\Utils\Commission\Invoice;

use AppBundle\Entity\Catalog\Product\Vat;
use AppBundle\Entity\Finance\Tax\VatRate;

/**
 * Class VatSpecification
 * @package AppBundle\Utils\Commission\Invoice
 */
class VatSpecification
{
    /**
     * @var VatRate
     */
    private $vatRate;

    /**
     * @var float
     */
    private $price;

    /**
     * @param VatRate $vatRate
     */
    public function __construct(VatRate $vatRate)
    {
        $this->vatRate = $vatRate;
    }

    /**
     * @param $price float
     */
    public function add($price)
    {
        $this->price += $price;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return round($this->price, 2);
    }

    /**
     * @return float
     */
    public function getVatPrice()
    {
        return $this->price * $this->vatRate->getFactor();
    }

    /**
     * @return VatRate
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }

    /**
     * @return int
     */
    public function getLedgerNumber()
    {
        switch ($this->getVat()->getPercentage()) {
            case 0:
                return 3632;
            case 6:
                return 3612;
            case 21:
                return 3622;
            default:
                throw new \RuntimeException(sprintf("Unexpected vat percentage '%d'", $this->getVat()->getPercentage()));
        }
    }
}