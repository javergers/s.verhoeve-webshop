<?php

namespace AppBundle\Utils\Adyen;

use Translation\Common\Model\Message;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * Class FraudCheckResult
 * @package AppBundle\Utils\Adyen
 */
class FraudCheckResult
{
    public const CHECKS = [
        'CardChunkUsage',
        'PaymentDetailUsage',
        'HolderNameUsage',
        'ShopperEmailUsage',
        'PaymentDetailRefCheck',
        'IssuerRefCheck',
        'IssuingCountryReferral',
        'ShopperEmailRefCheck',
        'PmOwnerRefCheck',
        'PaymentDetailNonFraudRefCheck',
        'HolderNameContainsNumber',
        'HolderNameIsOneWord',
        'EmailDomainValidation',
        'CVCAuthResultCheck',
    ];

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $score;

    /**
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->id = $params['checkId'];
        $this->name = $params['name'];
        $this->score = $params['accountScore'];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return '@todo translation for ' . $this->name . ' :' . $this->score;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @return Message[]
     */
    public static function getTranslationMessages()
    {
        $messages = [];

        foreach (self::CHECKS as $check) {
            $messages[] = new Message('adyen.fraud_check.' . self::generateTranslationMessage($check));
        }

        return $messages;
    }

    /**
     * @param $check
     * @return string
     */
    private static function generateTranslationMessage($check)
    {
        $converter = new CamelCaseToSnakeCaseNameConverter();

        $message = $converter->normalize($check);
        $message = str_replace('c_v_c', 'cvc', $message);

        return $message;
    }
}
