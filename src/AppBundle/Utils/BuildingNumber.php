<?php

namespace AppBundle\Utils;

use AppBundle\Exceptions\InvalidBuildingNumberException;

/**
 * Class Housenumber
 * @package AppBundle\Utils
 */
class BuildingNumber
{
    /**
     * @param string $number
     *
     * @return array
     * @throws InvalidBuildingNumberException
     */
    public static function split(string $number)
    {
        $matches = [];

        /** @noinspection NotOptimalRegularExpressionsInspection */
        $pattern = '/([1-9](?:[0-9]+)?)[\s-\/]*(.*)?/';

        if (!\preg_match($pattern, $number, $matches, PREG_UNMATCHED_AS_NULL)) {
            throw new InvalidBuildingNumberException('Could not parse building number of delivery address.');
        }

        return [
            'number'         => $matches[1] ?? '',
            'numberAddition' => $matches[2] ?? null,
        ];
    }
}
