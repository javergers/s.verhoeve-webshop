<?php

namespace AppBundle\Utils;

use RuntimeException;

/**
 * Class Network
 * @package AppBundle\Utils
 */
class Network
{
    /**
     * @param string      $url
     * @param bool        $throwException
     * @param string|null $errormessage
     * @return bool
     */
    public static function isExternalResourceAvailable(string $url, bool $throwException = false, ?string $errormessage = null): bool
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($httpCode < 200 || $httpCode >= 400) {
            if($throwException === false){
                return false;
            }
            if($errormessage === null){
                $errormessage = sprintf('The url "%s" is currently not available', $url);
            }
            throw new RuntimeException($errormessage);
        }
        return true;
    }
}