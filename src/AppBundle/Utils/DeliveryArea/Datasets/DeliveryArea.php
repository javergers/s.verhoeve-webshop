<?php

namespace AppBundle\Utils\DeliveryArea\Datasets;

use Doctrine\DBAL\DBALException;

/**
 * Class DeliveryArea
 * @package AppBundle\Utils\DeliveryArea\Datasets
 */
class DeliveryArea extends AbstractDataset
{
    /**
     * @var string
     */
    protected $label = 'Bezorggebieden';

    /**
     * @var string
     */
    protected $event = 'getRegions';

    /**
     * @var bool
     */
    public $dateSelect;

    /**
     * @var array
     */
    protected $suppliers;

    /**
     * @var array
     */
    protected $supplierGroups = [
        'Bakkers',
        'Bloemisten',
    ];

    /**
     * @var int
     */
    protected $order = 3;

    /**
     * @var array
     */
    protected $supplierConnectors = [
        'Bakker',
        'BakkerMail',
    ];

    /**
     * @param \stdClass|null $filter
     * @return array
     * @throws DBALException
     */
    public function getRegions(\stdClass $filter = null)
    {
        // Return only data when supplier filter isset through filter of pointer click
        if ($filter === null || empty($filter->suppliers)) {
            return [];
        }

        $rawDeliveryAreas = $this->getDeliveryAreas($filter);

        $data = [];
        $data['regions'] = [];
        $data['suppliers'] = [];

        /** @var object $rawDeliveryArea */
        foreach ($rawDeliveryAreas as $rawDeliveryArea) {

            $country = $rawDeliveryArea->country_code;
            $postcode = $rawDeliveryArea->postcode;
            $postcode_id = $rawDeliveryArea->postcode_id;
            $supplierId = $rawDeliveryArea->supplier_id;
            $supplierName = $rawDeliveryArea->supplier_name;
            $position = (int)$rawDeliveryArea->position;

            $isPreferred = null !== $position;

            // Default non premium;
            $color = ($isPreferred ? $this->getLegendColor(1) : $this->getLegendColor(2));

            if (isset($data['regions'][$postcode_id])) {
                if ($isPreferred) {
                    $data['regions'][$postcode_id]['color'] = $color;
                    $data['regions'][$postcode_id]['premium'] = $isPreferred;
                    $data['regions'][$postcode_id]['overlap'] = true;
                }
            } else {
                 $data['regions'][$postcode_id] = [
                    'country' => $country,
                    'postcode' => $postcode,
                    'color' => $color,
                    'premium' => $isPreferred,
                    'overlap' => false,
                ];
            }

            if ($supplierId && $supplierName) {
                $data['suppliers'][(int)$supplierId] = $supplierName;
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getLegend()
    {
        return [
            [
                'color' => '#26AE00',
                'label' => 'Voorkeur',
            ],
            [
                'color' => '#F44336',
                'label' => 'Geen voorkeur',
            ],
        ];
    }
}