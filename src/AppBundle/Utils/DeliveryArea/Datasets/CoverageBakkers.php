<?php

namespace AppBundle\Utils\DeliveryArea\Datasets;

use AppBundle\Connector\Bakker;
use AppBundle\Connector\BakkerMail;
use Doctrine\DBAL\DBALException;

/**
 * Class CoverageBakkers
 * @package AppBundle\Utils\DeliveryArea\Datasets
 */
class CoverageBakkers extends AbstractDataset
{
    /**
     * @var string
     */
    protected $label = 'Dekkingskaart - Bakkers';

    /**
     * @var string
     */
    protected $event = 'getRegions';

    /**
     * @var bool
     */
    public $dateSelect;

    /**
     * @var array
     */
    protected $suppliers;

    /**
     * @var array
     */
    protected $supplierGroups = [
        'Bakkers',
    ];

    /**
     * @var int
     */
    protected $order = 2;

    /**
     * @var array
     */
    protected $supplierConnectors = [
        'Bakker',
        'BakkerMail',
    ];

    /**
     * @param \stdClass|null $filter
     *
     * @return array
     * @throws DBALException
     */
    public function getRegions(\stdClass $filter = null)
    {
        $rawDeliveryAreas = $this->getDeliveryAreas($filter);

        $data = [];
        $data['regions'] = [];
        $data['suppliers'] = [];

        /** @var \stdClass $rawDeliveryArea */
        foreach ($rawDeliveryAreas as $rawDeliveryArea) {

            $country = $rawDeliveryArea->country_code;
            $postcode = $rawDeliveryArea->postcode;
            $postcode_id = $rawDeliveryArea->postcode_id;
            $supplierId = $rawDeliveryArea->supplier_id;
            $supplierName = $rawDeliveryArea->supplier_name;
            $supplierConnector = $rawDeliveryArea->supplier_connector;

            $isPremium = ($supplierConnector === 'Bakker');

            // Default non premium;
            $color = ($isPremium ? $this->getLegendColor(1) : $this->getLegendColor(2));

            if (isset($data['regions'][$postcode_id])) {
                if ($isPremium) {
                    $data['regions'][$postcode_id]['color'] = $color;
                    $data['regions'][$postcode_id]['premium'] = $isPremium;
                    $data['regions'][$postcode_id]['overlap'] = true;
                }
            } else {
                $data['regions'][$postcode_id] = [
                    'country' => $country,
                    'postcode' => $postcode,
                    'color' => $color,
                    'premium' => $isPremium,
                    'overlap' => false,
                ];
            }

            if ($supplierId && $supplierName) {
                $data['suppliers'][(int)$supplierId] = $supplierName;
            }
        }

        return $data;
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function getLegend()
    {

        $rf = new \ReflectionClass(Bakker::class);
        $bakkerColor = $rf->getDefaultProperties()['color'];

        $rf = new \ReflectionClass(BakkerMail::class);
        $bakkerMailColor = $rf->getDefaultProperties()['color'];

        return [
            [
                'color' => $bakkerColor,
                'label' => 'Bakker',
            ],
            [
                'color' => $bakkerMailColor,
                'label' => 'BakkerMail',
            ],
        ];
    }
}
