<?php

namespace AppBundle\Utils\DeliveryArea\Datasets;

/**
 * Class DeliveryTime
 * @package AppBundle\Utils\DeliveryArea\Datasets
 */
class DeliveryTime extends AbstractDataset
{
    /**
     * @var string
     */
    protected $label = 'Bezorgtijden';

    /**
     * @var string
     */
    protected $event = 'getRegions';

    /**
     * @var bool
     */
    public $dateSelect;

    /**
     * @var array
     */
    protected $suppliers;

    /**
     * @var array
     */
    protected $supplierGroups = [
        'Bakkers',
    ];

    /**
     * @var int
     */
    protected $order = 5;

    /**
     * @var array
     */
    protected $supplierConnectors = [
        'Bakker',
        'BakkerMail',
    ];

    /**
     * @param \stdClass|null $filter
     *
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getRegions(\stdClass $filter = null)
    {
        $rawDeliveryAreas = $this->getDeliveryAreas($filter);

        $data = [];
        $data['regions'] = [];
        $data['suppliers'] = [];

        /** @var \stdClass $rawDeliveryArea */
        foreach ($rawDeliveryAreas as $rawDeliveryArea) {

            $country = $rawDeliveryArea->country_code;
            $postcode = $rawDeliveryArea->postcode;
            $postcode_id = $rawDeliveryArea->postcode_id;
            $supplierId = $rawDeliveryArea->supplier_id;
            $supplierName = $rawDeliveryArea->supplier_name;
            $interval = $rawDeliveryArea->delivery_interval;
            $price = (float)$rawDeliveryArea->delivery_price;

            $legend = $this->getLegend();

            $order = 0;
            $color = '#000000';
            foreach ($legend as $item) {
                if ($item['value'] !== $interval) {
                    continue;
                }

                $order = $item['order'];
                $color = $item['color'];
            }

            if (isset($data['regions'][$postcode_id])) {
                if ($data['regions'][$postcode_id]['order'] < $order) {
                    $data['regions'][$postcode_id]['color'] = $color;
                    $data['regions'][$postcode_id]['order'] = $order;
                }
            } else {
                $data['regions'][$postcode_id] = [
                    'country' => $country,
                    'price' => $price,
                    'postcode' => $postcode,
                    'color' => $color,
                    'premium' => false,
                    'overlap' => false,
                    'order' => $order,
                ];
            }

            if ($supplierId && $supplierName) {
                $data['suppliers'][(int)$supplierId] = $supplierName;
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getLegend()
    {
        $colors = [
            '#000000',
            '#4CFF00',
            '#FFFF00',
            '#FF6A00',
            '#ffd700',
            '#bc8f8f',
            '#473c8b',
            '#b9d3ee',
            '#8fbc8f',
            '#26AE00',
        ];

        $deliveryIntervals = $this->deliveryAreaService->getDeliveryIntervals();

        $legend = [];
        foreach ($deliveryIntervals as $key => $deliveryInterval) {
            $legend[] = [
                'color' => $colors[$key],
                'label' => $deliveryInterval['description'],
                'value' => $deliveryInterval['interval'],
                'order' => $key,
                'option_name' => 'interval',
            ];
        }

        return $legend;
    }
}
