<?php

namespace AppBundle\Utils\Security;

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Exception\EnvironmentIsBrokenException;
use Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException;

/**
 * Class Encryptor
 * @package AppBundle\Utils\Security
 */
class Encryptor
{
    /**
     * @var string $secretKey
     */
    private $secretKey;

    /**
     * Encryptor constructor.
     *
     * @param string $secretKey
     */
    public function __construct(string $secretKey)
    {
        $this->secretKey = $secretKey;
    }

    /**
     * @param             $plainText
     * @param string|null $key
     *
     * @return string
     * @throws EnvironmentIsBrokenException
     */
    public function encrypt($plainText, string $key = null)
    {
        if ($key === null) {
            $key = $this->secretKey;
        }

        return Crypto::encryptWithPassword($plainText, $key, false);
    }

    /**
     * @param             $cipherText
     * @param string|null $key
     *
     * @return string
     * @throws EnvironmentIsBrokenException
     * @throws WrongKeyOrModifiedCiphertextException
     */
    public function decrypt($cipherText, $key = null)
    {
        if ($key === null) {
            $key = $this->secretKey;
        }

        return Crypto::decryptWithPassword($cipherText, $key, false);
    }
}
