<?php

namespace AppBundle\Utils;

/**
 * Class AmountConverter
 * @package AppBundle\Utils
 */
class AmountConverter
{
    /**
     * Safely converts a float price
     *
     * @param float $amount
     * @return int
     */
    public static function convert(float $amount)
    {
        // don't use round(), 17.90 will become 17.9, whe need 17.90)
        $result = number_format($amount, 2, ".", ",");

        /**
         * @workaround strange float to int outcome for example 17.90)
         * var_dump((int) (17.90 * 100)); == int(1789), expected is int(1790)
         */
        return (int)(string)($result * 100);
    }
}