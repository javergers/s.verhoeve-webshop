<?php

namespace AppBundle\Utils\Serializer;

use Ramsey\Uuid\Uuid;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class EntityNormalizer
 * @package AppBundle\Utils\Serializer
 */
class EntityNormalizer extends ObjectNormalizer
{
    private $attributes = [];
    private $attributeTypes = [];

    /**
     * @param $type
     * @param $value
     * @return mixed
     */
    private function reformatOutput($type, $value)
    {
        switch ($type) {
            case 'time':
                if ($value instanceof \DateTime) {
                    return $value->format('H:i');
                }
                break;

            case 'date':
                if ($value instanceof \DateTime) {
                    return $value->format('d-m-Y');
                }
                break;

            case 'uuid':
                if ($value instanceof Uuid) {
                    return $value->toString();
                }
                break;
        }
        return $value;
    }

    /**
     * @param $type
     * @param $value
     * @return mixed
     * @throws \Exception
     */
    private function reformatInput($type, $value)
    {
        switch ($type) {
            case 'time':
            case 'date':
                return new \DateTime($value);
                break;
        }
        return $value;
    }

    /**
     * @param array $attributes
     */
    public function setAttributes(array $attributes)
    {
        foreach ($attributes as $class => $data) {
            $this->extractAttributeTypes($class, $data);
        }
        $this->attributes = $attributes;
    }

    /**
     * @param $class
     * @param $data
     */
    private function extractAttributeTypes($class, $data)
    {
        foreach ($data as $key => $value) {
            $attributeType = [
                'class' => $class,
                'attribute' => is_int($key)? $value: $key,
                'type' => null,
                'keyName' => is_int($key)? $value: $key,
            ];
            if (!is_int($key) && is_array($value)) {
                if (array_key_exists('type', $value)) {
                    $attributeType['type'] = $value['type'];
                }
                if (array_key_exists('keyName', $value)) {
                    $attributeType['keyName'] = $value['keyName'];
                }
            }
            $this->attributeTypes[$attributeType['keyName']] = $attributeType;
        }
    }

    /**
     * @param array $data
     * @return array
     */
    private function extractClass(array $data)
    {
        $properties = [];
        foreach ($data as $key => $value) {
            if (is_int($key)) {
                $properties[] = $value;
            } elseif (is_string($key)) {
                $properties[] = $key;
            }
        }
        return $properties;
    }

    /**
     * @param $object
     * @return string
     */
    private function getClassName($object): string
    {
        return str_replace('Proxies\\__CG__\\', '', get_class($object));
    }

    /**
     * @param       $object
     * @param null  $format
     * @param array $context
     * @return array|string[]
     */
    protected function extractAttributes($object, $format = null, array $context = [])
    {
        foreach ($this->attributes as $class => $data) {
            if ($this->getClassName($object) === $class) {
                return $this->extractClass($data);
            }
        }
        return [];
    }

    /**
     * @param       $object
     * @param       $attribute
     * @param null  $format
     * @param array $context
     * @return mixed|string
     */
    protected function getAttributeValue($object, $attribute, $format = null, array $context = [])
    {
        $value = parent::getAttributeValue($object, $attribute, $format, $context);

        foreach ($this->attributeTypes as $attributeType) {
            if ($attributeType['attribute'] === $attribute && $this->getClassName($object) === $attributeType['class']) {
                return $this->reformatOutput($attributeType['type'], $value);
            }
        }

        return $value;
    }

    /**
     * @param       $object
     * @param null  $format
     * @param array $context
     * @return array|bool|float|int|string
     */
    public function normalize($object, $format = null, array $context = [])
    {
        $data = parent::normalize($object, $format, $context);

        foreach ($this->attributeTypes as $attributeType) {
            foreach ($data as $key => $value) {
                if ($attributeType['keyName'] !== null && $attributeType['attribute'] === $key && $this->getClassName($object) === $attributeType['class']) {
                    unset($data[$key]);
                    $data[$attributeType['keyName']] = $value;
                }
            }
        }

        return $data;
    }

    /**
     * @param       $data
     * @param       $object
     * @return object
     * @throws \Exception
     */
    public function updateObject($data, $object)
    {
        $class = $this->getClassName($object);
        $propertyAccessor = new PropertyAccessor();
        if(isset($this->attributes[$class])){
            foreach($this->attributes[$class] as $attributeName){
                if(isset($attributeName['keyName'])){
                    $attributeName = $attributeName['keyName'];
                }

                if(is_object($attributeName) || is_array($attributeName) || !isset($this->attributeTypes[$attributeName])){
                    continue;
                }

                $attribute = $this->attributeTypes[$attributeName];
                if($attribute['type'] !== null){
                    $this->reformatInput($attribute['type'], $object);
                    continue;
                }

                if($propertyAccessor->isReadable($object, $attribute['attribute'])){
                    $dataElement = $propertyAccessor->getValue($object, $attribute['attribute']);

                    if(is_object($dataElement)){
                        $this->updateObject($data[$attribute['keyName']], $dataElement);

                    } else if($propertyAccessor->isWritable($object, $attribute['attribute'])){
                        $propertyAccessor->setValue($object, $attribute['attribute'], $data[$attribute['keyName']]);
                    }
                }
            }
        }
        return $object;
    }
}
