<?php

namespace AppBundle\Utils;

use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;

/**
 * Class DisableFilter
 * @package AppBundle\Utils
 */
class DisableFilter
{
    private $filterName;
    private $oldState;

    /**
     * DisableFilter constructor.
     *
     * @param string $filterName
     * @param bool $oldState
     */
    public function __construct(string $filterName, bool $oldState)
    {
        $this->filterName = $filterName;
        $this->oldState = $oldState;
    }

    /**
     * @param $filterName
     *
     * @return DisableFilter
     */
    public static function temporaryDisableFilter($filterName): DisableFilter
    {
        $filters = self::getEntityManager()->getFilters();
        if($filters->has($filterName) === true) {
            $isFilterEnabled = $filters->isEnabled($filterName);
            if ($isFilterEnabled === true) {
                $filters->disable($filterName);
            }
        } else {
            $isFilterEnabled = false;
        }
        return new self($filterName, $isFilterEnabled);
    }

    public function reenableFilter(): void
    {
        $filters = self::getEntityManager()->getFilters();
        if($this->oldState === true && ($filters->has($this->filterName) === true) && $filters->isEnabled($this->filterName) === false) {
            $filters->enable($this->filterName);
        }
    }

    /**
     * @return EntityManagerInterface
     */
    private static function getEntityManager(): EntityManagerInterface
    {
        global $kernel;
        if($kernel->getContainer() === null){
            throw new RuntimeException('Container not found');
        }
        return $kernel->getContainer()->get('doctrine')->getManager();
    }
}