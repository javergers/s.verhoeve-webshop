<?php

namespace AppBundle\Utils;

use AppBundle\Entity\Site\Menu;
use AppBundle\Entity\Site\MenuItem;
use AppBundle\Services\Slug;
use AppBundle\Utils\MenuItem as MenuItemUtil;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class MenuStructure
 * @package AppBundle\Utils
 */
class MenuStructure implements \Iterator, \Serializable, \Countable
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var Slug
     */
    protected $slugService;

    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * @var Menu
     */
    protected $menu;

    /**
     * @var null|MenuItem
     */
    protected $menuItem;

    /** @var TranslatorInterface */
    protected $translator;

    /**
     * @var array
     */
    protected $items = [];

    /**
     * @var int
     */
    protected $position = 0;

    /**
     * MenuStructure constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param Slug                   $slugService
     * @param TokenStorage           $tokenStorage
     * @param Menu                   $menu
     * @param TranslatorInterface    $translator
     * @param MenuItem|null          $menuItem
     * @param boolean                $load
     * @throws \Exception
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        Slug $slugService,
        TokenStorage $tokenStorage,
        Menu $menu,
        TranslatorInterface $translator,
        MenuItem $menuItem = null,
        $load = true
    ) {
        $this->position = 0;

        $this->entityManager = $entityManager;
        $this->slugService = $slugService;
        $this->tokenStorage = $tokenStorage;
        $this->menu = $menu;
        $this->translator = $translator;
        $this->menuItem = $menuItem;

        if ($load) {
            $this->loadItems();
        }
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            'menuItem' => $this->menuItem,
            'items' => $this->items,
            'position' => $this->position,
        ]);
    }

    /**
     * @param string $data
     */
    public function unserialize($data)
    {
        $data = unserialize($data, []);

        foreach ($data as $key => $value) {
            $this->{$key} = $value;
        }
    }

    /**
     * Load items in array
     * @throws \Exception
     */
    private function loadItems()
    {
        $items = $this->entityManager->getRepository(MenuItem::class)->findBy([
            'menu' => $this->menu,
            'parent' => $this->menuItem,
        ], [
            'position' => 'ASC',
        ]);

        foreach ($items as $item) {
            $menuItemUtil = new MenuItemUtil($this->entityManager, $this->slugService, $this->tokenStorage,
                $this->menu, $this->translator, $item);

            $this->items[] = $menuItemUtil;
        }
    }

    /**
     * @return Menu
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * @return null|MenuItem
     */
    public function getItem()
    {
        return $this->menuItem;
    }

    /**
     * @param array $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @return mixed Can return any type.
     */
    public function current()
    {
        return $this->items[$this->position];
    }

    /**
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        ++$this->position;
    }

    /**
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * @return boolean The return value will be casted to boolean and then evaluated.
     */
    public function valid()
    {
        return isset($this->items[$this->position]);
    }

    /**
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * @return int The custom count as an integer.
     */
    public function count()
    {
        return \count($this->items);
    }
}
