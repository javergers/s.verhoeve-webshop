<?php

namespace AppBundle\Annotation\Type;

/**
 * Class YesNoType
 * @package AppBundle\Annotation\Type
 */
class YesNoType
{
    private const YES = true;
    private const NO = false;

    /**
     * @var array
     */
    public static $choices = [
        self::YES => 'Ja',
        self::NO => 'Nee',
    ];
}