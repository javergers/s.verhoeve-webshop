<?php

namespace AppBundle\Method;

use AppBundle\Entity\Payment\Payment;
use AppBundle\Form\Method\IdealType;
use AppBundle\Interfaces\MethodInterface;
use AppBundle\Interfaces\RedirectableMethodInterface;
use Translation\Common\Model\Message;

/**
 * Class Ideal
 * @package AppBundle\Method
 */
class Ideal extends AbstractMethod implements MethodInterface, RedirectableMethodInterface
{
    /**
     * @return string
     */
    public function getCode()
    {
        return 'ideal';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'iDEAL';
    }

    /**
     * @return string
     */
    public function getFormClass()
    {
        return IdealType::class;
    }

    /**
     * @param $host
     * @param $params
     * @return string
     */
    public function generateUrl($host, $params)
    {
        return $host . '/hpp/redirectIdeal.shtml?' . http_build_query($params);
    }

    /**
     * @param Payment $payment
     * @return array
     */
    public function generateParams(Payment $payment)
    {
        return [
            'brandCode' => 'ideal',
            'skipSelection' => true,
            'idealIssuerId' => $payment->getMetadata()['issuer'],
        ];
    }

    /**
     * @return array
     */
    public static function getTranslationMessages()
    {
        $messages = [
            new Message('payment.ideal.refused', 'validators'),
        ];

        return $messages;
    }
}
