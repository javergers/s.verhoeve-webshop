<?php

namespace AppBundle\Method;

use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\PaymentStatus;
use AppBundle\Exceptions\PaymentException;
use AppBundle\Form\Method\FreeOfChargeType;
use AppBundle\Interfaces\MethodInterface;
use AppBundle\Interfaces\ProcessableMethodInterface;

/**
 * Class FreeOfCharge
 * @package AppBundle\Method
 */
class FreeOfCharge extends AbstractMethod implements MethodInterface, ProcessableMethodInterface
{
    /**
     * @return string
     */
    public function getCode(): string
    {
        return 'freeofcharge';
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'Reeds voldaan';
    }

    /**
     * @return string
     */
    public function getFormClass(): string
    {
        return FreeOfChargeType::class;
    }

    /**
     * @param Payment $payment
     * @return bool
     * @throws \Exception
     */
    public function processPayment(Payment $payment): bool
    {
        if ($payment->getAmount() != 0) {
            throw new PaymentException(sprintf('The payment amount is invalid for payment method "freeofcharge"'));
        }

        $payment->setStatus($this->getEntityManager()->find(PaymentStatus::class, 'authorized'));

        $this->getEntityManager()->flush($payment);

        return true;
    }
}
