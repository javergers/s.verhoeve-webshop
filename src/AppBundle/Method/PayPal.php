<?php

namespace AppBundle\Method;

use AppBundle\Entity\Payment\Payment;
use AppBundle\Form\Method\PayPalType;
use AppBundle\Interfaces\MethodInterface;
use AppBundle\Interfaces\RedirectableMethodInterface;

/**
 * Class PayPal
 * @package AppBundle\Method
 */
class PayPal extends AbstractMethod implements MethodInterface, RedirectableMethodInterface
{
    /**
     * @return string
     */
    public function getCode()
    {
        return 'paypal';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'PayPal';
    }

    /**
     * @return string
     */
    public function getFormClass()
    {
        return PayPalType::class;
    }

    /**
     * @param $host
     * @param $params
     * @return string
     */
    public function generateUrl($host, $params)
    {
        return $host . '/hpp/details.shtml?' . http_build_query($params);
    }

    /**
     * @param Payment $payment
     * @return array
     */
    public function generateParams(Payment $payment)
    {
        return [
            'brandCode' => 'paypal',
        ];
    }
}
