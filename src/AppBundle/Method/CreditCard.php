<?php

namespace AppBundle\Method;

use AppBundle\Form\Method\CreditCardType;
use AppBundle\Interfaces\MethodInterface;
use AppBundle\Interfaces\ProcessableMethodInterface;

/**
 * @see https://docs.adyen.com/support/integration#testcardnumbers
 * @see https://docs.adyen.com/developers/api-manual
 */
class CreditCard extends AbstractMethod implements MethodInterface, ProcessableMethodInterface
{
    /**
     * @return array
     */
    public static function getTranslationMessages()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return 'creditcard';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'Creditcard';
    }

    /**
     * @return string
     */
    public function getFormClass()
    {
        return CreditCardType::class;
    }
}
