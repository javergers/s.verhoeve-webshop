<?php

namespace AppBundle\Manager\Report;

use AppBundle\Entity\Report\CompanyReportCustomer;
use AppBundle\Entity\Report\CompanyReportCustomerHistory;
use AppBundle\Services\JobManager;
use AppBundle\Services\Mailer\CompanyReportCustomerMailer;
use Doctrine\ORM\EntityManagerInterface;
use JMS\JobQueueBundle\Entity\Job;

/**
 * Class CompanyReportCustomerManager
 * @package AppBundle\Services\Report
 */
class CompanyReportCustomerManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var JobManager
     */
    protected $jobmanager;

    /**
     * @var CompanyReportCustomerMailer $mailer
     */
    protected $mailer;

    /**
     * @var string
     */
    protected $companyReportCustomerSendReportCommand = 'app:company-report-customer:send-report';

    /**
     * CompanyReportCustomerManager constructor.
     * @param EntityManagerInterface      $entityManager
     * @param JobManager                  $jobManager
     * @param CompanyReportCustomerMailer $mailer
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        JobManager $jobManager,
        CompanyReportCustomerMailer $mailer
    )
    {
        $this->entityManager = $entityManager;
        $this->jobmanager = $jobManager;
        $this->mailer = $mailer;
    }

    /**
     * @param CompanyReportCustomer $companyReportCustomer
     * @throws \Exception
     */
    public function send(CompanyReportCustomer $companyReportCustomer)
    {
        $tillDate = $this->getScheduledDateTime($companyReportCustomer);

        $fromDate = clone $tillDate;
        $fromDate->sub(new \DateInterval($companyReportCustomer->getFrequency()));

        $companyReportCustomerHistory = new CompanyReportCustomerHistory();
        $companyReportCustomerHistory->setCompanyReportCustomer($companyReportCustomer);
        $companyReportCustomerHistory->setFromDate($fromDate);
        $companyReportCustomerHistory->setTillDate($tillDate);

        $companyReportCustomer->addHistory($companyReportCustomerHistory);

        //schedule next report
        $this->schedule($companyReportCustomer);

        $this->mailer->send([
            'companyReportCustomerHistory' => $companyReportCustomerHistory,
        ]);
    }

    /**
     * @param CompanyReportCustomer $companyReportCustomer
     * @throws \Exception
     */
    public function schedule(CompanyReportCustomer $companyReportCustomer)
    {
        if (null === $this->jobmanager->getStartableJob($this->companyReportCustomerSendReportCommand,
                $companyReportCustomer)) {

            //get scheduled date
            $scheduleAt = $this->getScheduledDateTime($companyReportCustomer);

            $job = new Job($this->companyReportCustomerSendReportCommand, [$companyReportCustomer->getId()]);
            $job->addRelatedEntity($companyReportCustomer);
            $job->setExecuteAfter($scheduleAt);

            $this->jobmanager->addJob($job, null);

            $this->entityManager->flush();
        }
    }

    /**
     * @param CompanyReportCustomer $companyReportCustomer
     * @return \DateTime
     * @throws \Exception
     */
    protected function getScheduledDateTime(CompanyReportCustomer $companyReportCustomer)
    {
        if ($companyReportCustomer->getHistories()->isEmpty()) {
            $scheduleAt = $companyReportCustomer->getStartDate();
        } else {
            /** @var CompanyReportCustomerHistory $latestHistory */
            $latestHistory = $companyReportCustomer->getHistories()->last();
            $tillDate = clone $latestHistory->getTillDate();
            $tillDate->add(new \DateInterval($companyReportCustomer->getFrequency()));

            $scheduleAt = $tillDate;
        }

        return $scheduleAt;
    }
}
