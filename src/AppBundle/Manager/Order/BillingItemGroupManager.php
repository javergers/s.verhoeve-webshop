<?php

namespace AppBundle\Manager\Order;

use AppBundle\Entity\Order\BillingItem;
use AppBundle\Entity\Order\BillingItemGroup;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Order\OrderCollectionLine;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class BillingItemGroupManager
 * @package AppBundle\Manager\Order
 */
class BillingItemGroupManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var BillingItemManager
     */
    private $billingItemManager;

    /**
     * BillingItemGroupManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param BillingItemManager $billingItemManager
     */
    public function __construct(EntityManagerInterface $entityManager, BillingItemManager $billingItemManager)
    {
        $this->entityManager = $entityManager;
        $this->billingItemManager = $billingItemManager;
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @return bool
     */
    public function checkByOrderCollection(OrderCollection $orderCollection): bool
    {
        $billingItemGroup = $this->create($orderCollection);

        foreach ($orderCollection->getOrders() as $order) {
            $this->billingItemManager->createByOrder($order, $billingItemGroup);
        }

        foreach ($orderCollection->getLines() as $line) {
            $this->createByOrderCollectionLine($line, $billingItemGroup);
        }

        return !$billingItemGroup->getBillingItems()->isEmpty();
    }

    /**
     * @param OrderCollection $orderCollection
     * @return BillingItemGroup|null
     */
    public function createByOrderCollection(OrderCollection $orderCollection): ?BillingItemGroup
    {
        $billingItemGroup = $this->create($orderCollection);

        foreach ($orderCollection->getOrders() as $order) {
            $this->billingItemManager->createByOrder($order, $billingItemGroup);
        }

        foreach ($orderCollection->getLines() as $line) {
            $this->createByOrderCollectionLine($line, $billingItemGroup);
        }

        if ($billingItemGroup->getBillingItems()->isEmpty()) {
            return null;
        }

        $orderCollection->addBillingItemGroup($billingItemGroup);

        $this->entityManager->persist($billingItemGroup);

        return $billingItemGroup;
    }

    /**
     * @param OrderCollection $orderCollection
     * @return BillingItemGroup
     */
    public function create(OrderCollection $orderCollection): BillingItemGroup
    {
        $billingItemGroup = new BillingItemGroup();
        $billingItemGroup->setOrderCollection($orderCollection);
        $billingItemGroup->setSequence($this->getSequence($orderCollection));

        return $billingItemGroup;
    }

    /**
     * @param OrderCollectionLine   $line
     * @param BillingItemGroup|null $billingItemGroup
     * @return BillingItem
     */
    private function createByOrderCollectionLine(
        OrderCollectionLine $line,
        ?BillingItemGroup $billingItemGroup = null
    ): BillingItem {
        $orderBillingItemGroups = $line->getOrderCollection()->getBillingItemGroups();

        $billingItem = new BillingItem();
        $billingItem->setOrderCollectionLine($line);

        $type = 'debit';
        if($line->getStatus() === 'cancelled') {
            $type = 'credit';

            if ($line->getBillingItems()->isEmpty()) {
                return null;
            }
        }

        foreach ($orderBillingItemGroups as $group) {
            if ($group->getBillingItems()->exists(static function ($key, BillingItem $b) use ($line, $type) {
                return $b->getOrderCollectionLine() === $line && $b->getTransactionType() === $type;
            })) {
                return null;
            }
        }

        $billingItem->setTransactionType($type);

        if ($billingItemGroup) {
            $billingItemGroup->addBillingItem($billingItem);
        }

        return $billingItem;
    }

    /**
     * @param OrderCollection $orderCollection
     * @return int
     */
    private function getSequence(OrderCollection $orderCollection): int
    {
        return $orderCollection->getBillingItemGroups()->count() + 1;
    }
}