<?php

namespace AppBundle\Manager\Order;

use AppBundle\Entity\Discount\Discount;
use AppBundle\Entity\Finance\Tax\VatRate;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Services\Finance\Tax\VatGroupService;
use AppBundle\Services\PriceService;
use AppBundle\Utils\NumberUtil;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CartOrderLineManager
 * @package AppBundle\Manager\Order
 */
class CartOrderLineManager
{
    /**
     * @var PriceService
     */
    protected $priceService;

    /**
     * @var VatGroupService
     */
    protected $vatGroupService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CartOrderLineManager constructor.
     *
     * @param PriceService $priceService
     * @param VatGroupService $vatGroupService
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(PriceService $priceService, VatGroupService $vatGroupService, EntityManagerInterface $entityManager)
    {
        $this->priceService = $priceService;
        $this->vatGroupService = $vatGroupService;
        $this->entityManager = $entityManager;
    }

    /**
     * @param CartOrderLine $cartOrderLine
     * @return float
     * @throws \Exception
     */
    public function getTotalPrice(CartOrderLine $cartOrderLine): float
    {
        $vatRate = $this->getVatRate($cartOrderLine);

        $price = $this->priceService->getRawDisplayPrice($cartOrderLine->getPrice() * $cartOrderLine->getQuantity(),
            $vatRate, false, 3);

        return $price - $this->getDiscountAmountExcl($cartOrderLine);
    }

    /**
     * @param CartOrderLine $cartOrderLine
     * @return float
     * @throws \Exception
     */
    public function getTotalPriceIncl(CartOrderLine $cartOrderLine): float
    {
        $vatRate = $this->getVatRate($cartOrderLine);

        $totalIncl = 0;
        for($i = 0; $i < $cartOrderLine->getQuantity(); $i++) {
            $totalIncl += NumberUtil::truncateNumber($this->priceService->calcPrice($cartOrderLine->getPrice(), $vatRate));
        }

        return (float) $totalIncl - $cartOrderLine->getDiscountAmount();
    }

    /**
     * @param CartOrderLine $cartOrderLine
     * @return float
     * @throws \Exception
     */
    public function getDiscountAmountExcl(CartOrderLine $cartOrderLine): float
    {
        $vatRate = $this->getVatRate($cartOrderLine);
        return $cartOrderLine->getDiscountAmount() / (1 + $vatRate->getFactor());
    }

    /**
     * @param CartOrderLine $cartOrderLine
     * @return VatRate|null
     * @throws \Exception
     */
    private function getVatRate(CartOrderLine $cartOrderLine): ?VatRate
    {
        $vatGroup = $this->vatGroupService->determineVatGroupForLineInterface($cartOrderLine);
        $vatRate = $vatGroup->getRateByDate($cartOrderLine->getOrder()->getDeliveryDate());

        return $vatRate;
    }

    /**
     * @param CartOrderLine $cartOrderLine
     * @return string|null
     */
    public function getDiscountDescription(CartOrderLine $cartOrderLine)
    {
        $metadata = $cartOrderLine->getMetadata();
        if(isset($metadata['discount'])) {
            /** @var Discount $discount */
            $discount = $this->entityManager->getRepository(Discount::class)->find($metadata['discount']);

            return $discount->getDescription() ?? $discount->getRule()->getDescription();
        }

        return null;
    }
}