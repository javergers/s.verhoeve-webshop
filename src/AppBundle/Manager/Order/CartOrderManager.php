<?php

namespace AppBundle\Manager\Order;

use AppBundle\Entity\Catalog\Product\Combination;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\CompanyTransportType;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Supplier\SupplierGroupProduct;
use AppBundle\Entity\Supplier\SupplierProduct;
use AppBundle\Manager\Catalog\Product\ProductManager;
use AppBundle\Services\SiteService;
use function count;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

/**
 * Class CartOrderManager
 * @package AppBundle\Manager\Order
 */
class CartOrderManager
{
    /**
     * @var CartOrderLineManager
     */
    protected $cartOrderLineManager;

    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var SiteService
     */
    private $siteService;

    /**
     * CartOrderManager constructor.
     *
     * @param CartOrderLineManager $cartOrderLineManager
     * @param ProductManager $productManager
     * @param EntityManagerInterface $entityManager
     * @param SiteService $siteService
     */
    public function __construct(CartOrderLineManager $cartOrderLineManager, ProductManager $productManager, EntityManagerInterface $entityManager, SiteService $siteService)
    {
        $this->cartOrderLineManager = $cartOrderLineManager;
        $this->productManager = $productManager;
        $this->entityManager = $entityManager;
        $this->siteService = $siteService;
    }

    /**
     * @param CartOrder $cartOrder
     * @return float
     */
    public function getTotalDiscountAmount(CartOrder $cartOrder): float
    {
        $total = 0;

        foreach ($cartOrder->getLines() as $line) {
            $total += $line->getDiscountAmount();
        }

        return $total;
    }

    /**
     * @param CartOrder $cartOrder
     * @return float
     * @throws \Exception
     */
    public function getTotalDiscountAmountExcl(CartOrder $cartOrder): float
    {
        $total = 0;

        foreach ($cartOrder->getLines() as $line) {
            $total += $this->cartOrderLineManager->getDiscountAmountExcl($line);
        }

        return $total;
    }

    /**
     * @param CartOrder     $cartOrder
     * @param TransportType $deliveryProduct
     * @param Company       $company
     * @return float|int|null
     */
    public function getCompanyDeliveryPrice(CartOrder $cartOrder, TransportType $deliveryProduct, Company $company)
    {
        $deliveryPrice = null;

        //TODO remove when pricelist is available
        $productGroups = [];
        foreach($cartOrder->getLines() as $line) {
            //decorate the product in case of companyProducts
            $product = $this->productManager->decorateProduct($line->getProduct());

            $productGroup = $product->getProductgroup();
            if(null !== $productGroup) {
                $productGroups[$productGroup->getId()] = $productGroup->getId();
            }
        }

        $companyTransportTypes = $company->getTransportTypes()->filter(function(CompanyTransportType $companyTransportType) use($deliveryProduct, $productGroups) {
            return $companyTransportType->getTransportType() === $deliveryProduct && in_array($companyTransportType->getProductGroup()->getId(),
                    $productGroups, true);
        });

        if(!$companyTransportTypes->isEmpty()) {
            /** @var CompanyTransportType $companyTransportType */
            foreach($companyTransportTypes as $companyTransportType) {
                if(null === $deliveryPrice || $companyTransportType->getPrice() > $deliveryPrice) {
                    $deliveryPrice = $companyTransportType->getPrice();
                }
            }
        }
        
        return $deliveryPrice;
    }

    /**
     * @param CartOrder $cartOrder
     *
     * @return bool
     */
    public function isPickupPossible(CartOrder $cartOrder): bool
    {
        $sitePickupEnabled = $this->siteService->determineSite()->getOrderPickupEnabled();

        if (false === $sitePickupEnabled) {
            return false;
        }

        $purchasableProducts = [];

        /** @var CartOrderLine $line */
        foreach ($cartOrder->getLines() as $line) {
            $product = $line->getProduct() instanceof CompanyProduct ? $line->getProduct()->getRelatedProduct() : $line->getProduct();

            if ($product->isCombination()) {
                /** @var Combination $combination */
                foreach ($product->getCombinations() as $combination) {
                    if ($combination->getProduct()->getPurchasable()) {
                        $purchasableProducts[] = $combination->getProduct();
                    }
                }
            } else {
                if ($product->getPurchasable()) {
                    $purchasableProducts[] = $product;
                }
            }
        }

        $productPickupCount = 0;
        /** @var CartOrderLine $purchasableProduct */
        foreach ($purchasableProducts as $purchasableProduct) {
            /** @var QueryBuilder $qb */
            $qb = $this->entityManager->createQueryBuilder();
            $supplierProducts = $qb->select('sp')
                ->from(SupplierProduct::class, 'sp')
                ->leftJoin(Product::class, 'p', 'WITH', 'p.id = sp.product')
                ->leftJoin(Product::class, 'pp', 'WITH', 'pp.id = p.parent')
                ->leftJoin(Productgroup::class, 'pg_child', 'WITH', 'pg_child.id = p.productgroup')
                ->leftJoin(Productgroup::class, 'pg_parent', 'WITH',
                    'pg_parent.id = pp.productgroup')
                ->where('sp.product = :product')
                ->andWhere('((pg_child.pickupEnabled = 1 OR pg_parent.pickupEnabled = 1) AND sp.pickupEnabled IS NULL) OR sp.pickupEnabled = 1')
                ->setParameter('product', $purchasableProduct->getId())
                ->orderBy('sp.position', 'ASC')
                ->setMaxResults(1)
                ->getQuery()
                ->getResult();

            /** @var QueryBuilder $qb */
            $qb = $this->entityManager->createQueryBuilder();
            $supplierGroupProducts = $qb->select('sp')
                ->from(SupplierGroupProduct::class, 'sp')
                ->leftJoin(Product::class, 'p', 'WITH', 'p.id = sp.product')
                ->leftJoin(Product::class, 'pp', 'WITH', 'pp.id = p.parent')
                ->leftJoin(Productgroup::class, 'pg_child', 'WITH', 'pg_child.id = p.productgroup')
                ->leftJoin(Productgroup::class, 'pg_parent', 'WITH',
                    'pg_parent.id = pp.productgroup')
                ->where('sp.product = :product')
                ->andWhere('((pg_child.pickupEnabled = 1 OR pg_parent.pickupEnabled = 1) AND sp.pickupEnabled IS NULL) OR sp.pickupEnabled = 1')
                ->setParameter('product', $purchasableProduct->getId())
                ->orderBy('sp.position', 'ASC')
                ->setMaxResults(1)
                ->getQuery()
                ->getResult();

            if(!empty($supplierProducts) || !empty($supplierGroupProducts)) {
                $productPickupCount++;
            }
        }

        return $productPickupCount > 0 && $productPickupCount === count($purchasableProducts);
    }
}