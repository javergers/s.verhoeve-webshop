<?php

namespace AppBundle\Manager\Order;

use AppBundle\Entity\Finance\Tax\VatRate;
use AppBundle\Interfaces\Order\LineInterface;
use AppBundle\Services\PriceService;
use function round;

/**
 * Class LineManager
 * @package AppBundle\Manager\Order
 */
class LineManager
{
    /**
     * @var PriceService
     */
    protected $priceService;

    /**
     * LineManager constructor.
     *
     * @param PriceService $priceService
     */
    public function __construct(PriceService $priceService)
    {
        $this->priceService = $priceService;
    }

    /**
     * @param LineInterface $orderLine
     * @param VatRate $vatRate
     * Calculation back and forward to ensure correct rounding of excl price
     *
     * @return float
     */
    protected function calculateVatLineAmount(LineInterface $orderLine, VatRate $vatRate)
    {
        $lineAmountIncl = round($this->priceService->calcPrice($orderLine->getPrice(), $vatRate), 2) * $orderLine->getQuantity();

        return round($lineAmountIncl / (1 + $vatRate->getFactor()), 3);
    }

    /**
     * @param LineInterface $orderLine
     *
     * @return float|null
     */
    public function getDiscountAmountExcl(LineInterface $orderLine)
    {
        if (!$orderLine->getVatLines()->isEmpty()) {
            $discountEx = 0;
            foreach ($orderLine->getVatLines() as $vatLine) {
                /** @noinspection TypeUnsafeComparisonInspection */
                $vatLinePartFactor = $orderLine->getTotalPrice() == 0 ? 0 : $vatLine->getAmount() / $orderLine->getTotalPrice();
                $discountEx += ($orderLine->getDiscountAmount() * $vatLinePartFactor) / (1 + ($vatLine->getVatRate() !== null ? $vatLine->getVatRate()->getFactor(): 0));
            }

            return $discountEx;
        }

        // TODO SHOULD NOT HAPPEN
        return $orderLine->getDiscountAmount();
    }

    /**
     * @param LineInterface $orderLine
     *
     * @return float
     */
    public function getAmountIncl(LineInterface $orderLine): float
    {
        $totalPrice = 0;

        foreach ($orderLine->getVatLines() as $vatLine) {
            $totalPrice += $this->priceService->calcPrice($vatLine->getAmount(), $vatLine->getVatRate());
        }

        return $totalPrice;
    }
}