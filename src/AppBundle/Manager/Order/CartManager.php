<?php

namespace AppBundle\Manager\Order;

use AppBundle\Entity\Order\Cart;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Manager\Catalog\Product\ProductPriceManager;
use AppBundle\Services\Finance\Tax\VatGroupService;
use AppBundle\Services\PriceService;
use AppBundle\Services\SiteService;

/**
 * Class CartManager
 * @package AppBundle\Manager\Order
 */
class CartManager
{
    /**
     * @var ProductPriceManager
     */
    protected $productPriceManager;

    /**
     * @var SiteService
     */
    protected $siteService;

    /**
     * @var VatGroupService
     */
    protected $vatGroupService;

    /**
     * @var PriceService
     */
    protected $priceService;

    /**
     * @var CartOrderManager
     */
    protected $cartOrderManager;

    /**
     * @var CartOrderLineManager
     */
    protected $cartOrderLineManager;

    /**
     * CartManager constructor.
     * @param ProductPriceManager  $productPriceManager
     * @param SiteService          $siteService
     * @param VatGroupService      $vatGroupService
     * @param PriceService         $priceService
     * @param CartOrderLineManager $cartOrderLineManager
     */
    public function __construct(
        ProductPriceManager $productPriceManager,
        SiteService $siteService,
        VatGroupService $vatGroupService,
        PriceService $priceService,
        CartOrderManager $cartOrderManager,
        CartOrderLineManager $cartOrderLineManager
    ) {
        $this->productPriceManager = $productPriceManager;
        $this->siteService = $siteService;
        $this->vatGroupService = $vatGroupService;
        $this->priceService = $priceService;
        $this->cartOrderManager = $cartOrderManager;
        $this->cartOrderLineManager = $cartOrderLineManager;
    }

    /**
     * @param Cart $cart
     * @return Cart
     * @throws \Exception
     */
    public function calculateTotals(Cart $cart): Cart
    {
        $cartTotal = 0;
        $cartTotalIncl = 0;

        /** @var CartOrder $cartOrder */
        foreach ($cart->getOrders() as $cartOrder) {
            $cartOrderTotal = 0;
            $cartOrderTotalIncl = 0;

            foreach ($cartOrder->getLines() as $cartOrderLine) {
                $cartOrderTotal += $this->cartOrderLineManager->getTotalPrice($cartOrderLine);
                $cartOrderTotalIncl += $this->cartOrderLineManager->getTotalPriceIncl($cartOrderLine);
            }

            $cartOrder->setTotalPrice($cartOrderTotal);
            $cartOrder->setTotalPriceIncl($cartOrderTotalIncl);

            $cartTotal += $cartOrderTotal;
            $cartTotalIncl += $cartOrderTotalIncl;
        }

        $cart->setTotalPrice($cartTotal);
        $cart->setTotalPriceIncl($cartTotalIncl);

        return $cart;
    }

    /**
     * @param Cart $cart
     * @param bool $withDiscount
     * @return array
     * @throws \Exception
     */
    public function getVatTotals(Cart $cart, $withDiscount = true): array
    {
        $vats = [];
        /** @var CartOrder $order */
        foreach ($cart->getOrders() as $order) {
            /** @var CartOrderLine $cartOrderLine */
            foreach ($order->getLines() as $cartOrderLine) {
                $vatGroup = $this->vatGroupService->determineVatGroupForLineInterface($cartOrderLine);
                $vatRate = $vatGroup->getRateByDate($order->getDeliveryDate());

                if (null === $vatRate) {
                    continue;
                }

                $site = $this->siteService->determineSite();
                $exclusive = $site->getDefaultDisplayPrice() === 'exclusive';

                if ($exclusive) {
                    $totalPrice = $this->cartOrderLineManager->getTotalPrice($cartOrderLine);
                    $vatPrice = $totalPrice * $vatRate->getFactor();
                } else {
                    $totalPrice = $this->cartOrderLineManager->getTotalPriceIncl($cartOrderLine);
                    $vatPrice = $totalPrice - ($totalPrice / (1 + $vatRate->getFactor()));
                }

                if ($vatPrice === 0) {
                    continue;
                }

                $vatId = $vatRate->getId();

                $vats[$vatId] = [
                    'percentage' => $vatRate->getFactor() * 100,
                    'vatPrice' => isset($vats[$vatId]['vatPrice']) ? $vats[$vatId]['vatPrice'] += $vatPrice : $vatPrice,
                    'totalPrice' => isset($vats[$vatId]['totalPrice']) ? $vats[$vatId]['totalPrice'] += $totalPrice : $totalPrice,
                ];
            }
        }

        return $vats;
    }

    /**
     * @param CartOrder $order
     * @param bool      $withDiscount
     * @return array
     * @throws \Exception
     */
    public function getVatTotalsForOrder(CartOrder $order, $withDiscount = true): array
    {
        $vats = [];

        /** @var CartOrderLine $cartOrderLine */
        foreach ($order->getLines() as $cartOrderLine) {
            $vatGroup = $this->vatGroupService->determineVatGroupForLineInterface($cartOrderLine);
            $vatRate = $vatGroup->getRateByDate($order->getDeliveryDate());

            if (null === $vatRate) {
                continue;
            }

            $site = $this->siteService->determineSite();
            $exclusive = $site->getDefaultDisplayPrice() === 'exclusive';

            if ($exclusive) {
                $totalPrice = $this->cartOrderLineManager->getTotalPrice($cartOrderLine);
                $vatPrice = $totalPrice * $vatRate->getFactor();
            } else {
                $totalPrice = $this->cartOrderLineManager->getTotalPriceIncl($cartOrderLine);
                $vatPrice = $totalPrice - ($totalPrice / (1 + $vatRate->getFactor()));
            }

            if ($vatPrice === 0) {
                continue;
            }

            $vatId = $vatRate->getId();

            $vats[$vatId] = [
                'percentage' => $vatRate->getFactor() * 100,
                'vatPrice' => isset($vats[$vatId]['vatPrice']) ? $vats[$vatId]['vatPrice'] += $vatPrice : $vatPrice,
                'totalPrice' => isset($vats[$vatId]['totalPrice']) ? $vats[$vatId]['totalPrice'] += $totalPrice : $totalPrice,
            ];
        }

        return $vats;
    }

    /**
     * @param CartOrder $order
     * @param bool      $withDiscount
     * @return float
     * @throws \Exception
     */
    public function getVatAmountForOrder(CartOrder $order, $withDiscount = true): float
    {
        $amount = 0;

        /** @var CartOrderLine $cartOrderLine */
        foreach ($order->getLines() as $cartOrderLine) {
            $vatGroup = $this->vatGroupService->determineVatGroupForLineInterface($cartOrderLine);
            $vatRate = $vatGroup->getRateByDate($order->getDeliveryDate());

            if (null === $vatRate) {
                continue;
            }

            $site = $this->siteService->determineSite();
            $exclusive = $site->getDefaultDisplayPrice() === 'exclusive';

            if ($exclusive) {
                $totalPrice = $this->cartOrderLineManager->getTotalPrice($cartOrderLine);
                $vatPrice = $totalPrice * $vatRate->getFactor();
            } else {
                $totalPrice = $this->cartOrderLineManager->getTotalPriceIncl($cartOrderLine);
                $vatPrice = $totalPrice - ($totalPrice / (1 + $vatRate->getFactor()));
            }

            if ($vatPrice === 0) {
                continue;
            }

            $amount += $vatPrice;
        }

        return $amount;
    }

    /**
     * @param Cart $cart
     * @param bool $withDiscount
     * @return float
     */
    public function getSubTotal(Cart $cart, $withDiscount = true): float
    {
        $total = 0;
        /** @var CartOrder $order */
        foreach ($cart->getOrders() as $order) {
            $total += $order->getTotalPrice();
        }

        return $total;
    }

    /**
     * @param Cart $cart
     * @param bool $withDiscount
     * @return float
     */
    public function getTotal(Cart $cart, $withDiscount = true): float
    {
        $total = 0;
        /** @var CartOrder $order */
        foreach ($cart->getOrders() as $order) {
            $total += $order->getTotalPriceIncl();
        }

        return $total;
    }
}