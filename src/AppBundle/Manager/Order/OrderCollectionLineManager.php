<?php

namespace AppBundle\Manager\Order;

use AppBundle\Entity\Finance\Tax\VatRate;
use AppBundle\Entity\Order\OrderCollectionLine;
use AppBundle\Entity\Order\OrderCollectionLineVat;

/**
 * Class OrderCollectionLineManager
 * @package AppBundle\Manager\Order
 */
class OrderCollectionLineManager extends LineManager
{
    /**
     * @param OrderCollectionLine $orderCollectionLine
     *
     * @param VatRate $vatRate
     *
     * @param float|null $amount the amount to be set on the vatLine (excluding vat * quantity)
     */
    public function createVatLines(OrderCollectionLine $orderCollectionLine, VatRate $vatRate, float $amount = null)
    {
        $recalculated = false;
        foreach ($orderCollectionLine->getVatLines() as $vatLine) {
            if ($vatLine->getOrderCollectionLine() === $orderCollectionLine && $vatLine->getVatRate()->getId() === $vatRate->getId()) {
                $vatLine->setAmount($this->calculateVatLineAmount($orderCollectionLine, $vatRate));

                $recalculated = true;
            }
        }

        if (!$recalculated) {
            $orderCollectionLineVat = new OrderCollectionLineVat();
            $orderCollectionLineVat->setOrderCollectionLine($orderCollectionLine);
            $orderCollectionLineVat->setVatRate($vatRate);

            if(null === $amount) {
                $orderCollectionLineVat->setAmount($this->calculateVatLineAmount($orderCollectionLine, $vatRate));
            } else {
                $orderCollectionLineVat->setAmount($amount);
            }

            $orderCollectionLine->addVatLine($orderCollectionLineVat);
        }
    }
}