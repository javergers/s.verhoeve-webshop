<?php

namespace AppBundle\Manager\Order;

use AppBundle\Entity\Discount\Discount;
use AppBundle\Entity\Finance\Tax\VatRate;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Order\OrderLineVat;
use AppBundle\Interfaces\Sales\OrderLineInterface;
use AppBundle\Services\Designer;
use AppBundle\Services\Finance\Tax\VatGroupService;
use AppBundle\Services\PriceService;
use AppBundle\Services\ProxyService;
use Doctrine\ORM\EntityManagerInterface;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class OrderLineManager
 * @package AppBundle\Manager\Order
 */
class OrderLineManager extends LineManager
{
    /**
     * @var VatGroupService
     */
    protected $vatGroupService;

    /**
     * @var OrderLineVatManager
     */
    protected $orderLineVatManager;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Designer
     */
    private $designer;

    /**
     * @var ProxyService
     */
    private $proxyService;

    /**
     * @var CacheManager
     */
    private $cacheManager;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * OrderLineManager constructor.
     *
     * @param VatGroupService $vatGroupService
     * @param OrderLineVatManager $orderLineVatManager
     * @param EntityManagerInterface $entityManager
     * @param Designer $designer
     * @param ProxyService $proxyService
     * @param CacheManager $cacheManager
     * @param RouterInterface $router
     */
    public function __construct(
        VatGroupService $vatGroupService,
        OrderLineVatManager $orderLineVatManager,
        EntityManagerInterface $entityManager,
        Designer $designer,
        ProxyService $proxyService,
        CacheManager $cacheManager,
        RouterInterface $router,
        PriceService $priceService
    ) {
        $this->vatGroupService = $vatGroupService;
        $this->orderLineVatManager = $orderLineVatManager;
        $this->entityManager = $entityManager;
        $this->designer = $designer;
        $this->proxyService = $proxyService;
        $this->cacheManager = $cacheManager;
        $this->router = $router;

        parent::__construct($priceService);
    }

    /**
     * @param OrderLine $orderLine
     *
     * @throws \Exception
     */
    public function createVatLines(OrderLine $orderLine): void
    {
        $vatGroup = $this->vatGroupService->determineVatGroupForLineInterface($orderLine);

        if (null !== $vatGroup) {
            $vatRate = $vatGroup->getRateByDate($orderLine->getOrder()->getDeliveryDate());

            $recalculated = false;
            foreach ($orderLine->getVatLines() as $vatLine) {
                if ($vatLine->getOrderLine() === $orderLine && $vatLine->getVatRate()->getId() === $vatRate->getId()) {
                    $vatLine->setAmount($this->calculateVatLineAmount($orderLine, $vatRate));

                    $recalculated = true;
                }
            }

            if (!$recalculated) {
                $orderLineVat = new OrderLineVat();
                $orderLineVat->setOrderLine($orderLine);
                $orderLineVat->setVatRate($vatRate);
                $orderLineVat->setAmount($this->calculateVatLineAmount($orderLine, $vatRate));
                $orderLine->addVatLine($orderLineVat);
            }
        }
    }

    /**
     * @param OrderLine $orderLine
     * @deprecated
     * @return float
     */
    public function getTotalPriceIncl(OrderLine $orderLine): float
    {
        return $this->getAmountIncl($orderLine);
    }

    /**
     * @param OrderLine $orderLine
     *
     * @return string|null
     */
    public function getDiscountDescription(OrderLine $orderLine)
    {
        $metadata = $orderLine->getMetadata();
        if (isset($metadata['discount'])) {
            /** @var Discount $discount */
            $discount = $this->entityManager->getRepository(Discount::class)->find($metadata['discount']);

            return $discount->getDescription() ?? $discount->getRule()->getDescription();
        }

        return null;
    }

    /**
     * Checks whether this line has personalization for the product
     *
     * @param OrderLineInterface $orderLine     *
     * @return boolean
     */
    public function hasProductPersonalization(OrderLineInterface $orderLine)
    {
        return (bool)$this->getPersonalizationLine($orderLine);
    }

    /**
     * @param OrderLineInterface $orderLine
     * @return OrderLineInterface
     */
    public function getPersonalizationLine(OrderLineInterface $orderLine): ?OrderLineInterface
    {
        $parentOrderLine = $orderLine->getParent() ?? $orderLine;

        if ($parentOrderLine->getMetadata() && !empty($parentOrderLine->getMetadata()['designer'])) {
            return $orderLine;
        }

        $personalizationLine = $parentOrderLine->getChildren()->filter(function (OrderLineInterface $childOrderLine) {
            return ($childOrderLine->getMetadata() && !empty($childOrderLine->getMetadata()['designer']));
        });

        if (!$personalizationLine->isEmpty()) {
            return $personalizationLine->first() ?: null;
        }

        return null;
    }

    /**
     * @param OrderLineInterface $orderLine
     * @return string|null
     */
    public function getDesignPrintUrl(OrderLineInterface $orderLine): ?string
    {
        $printUrl = null;

        if (isset($orderLine->getMetadata()['designer'])) {
            $this->convertDesign($orderLine);

            $personalizationLine = $this->getPersonalizationLine($orderLine);

            if (null === $personalizationLine || null === $personalizationLine->getProduct()) {
                throw new \RuntimeException('OrderLine with Personalization not found.');
            }

            if ($personalizationLine->getProduct()->isPersonalization()) {
                $personalization = $personalizationLine->getProduct();
            } elseif ($personalizationLine->getProduct()->isCombination()) {
                $personalization = $personalizationLine->getProduct()->getPersonalizations()->first();
            } else {
                $personalization = null;
            }

            $printUrl = $this->designer->getFileUrl(Designer::FILE_PRINT);

            if($personalization->isClothingPrint() || (int)$personalization->getPrintRotation() !== 0) {
                $isManipulated = $this->designer->applyImageManipulation([
                    'flip' => $personalization->isClothingPrint(),
                    'rotation' => $personalization->getPrintRotation()
                ]);

                if($isManipulated) {
                    $printUrl = $this->designer->getFileUrl(Designer::FILE_MANIPULATED_PRINT);
                }
            }
        }

        if (isset($orderLine->getMetadata()['design_url_uuid'], $orderLine->getMetadata()['design_url_filename'])) {
            $this->designer->setUuid($orderLine->getMetadata()['design_url_uuid']);
            $this->designer->setIsUrlDesign(true);

            $printUrl = $this->designer->getFileUrl($orderLine->getMetadata()['design_url_filename']);
        }

        if (isset($orderLine->getMetadata()['design_url'])) {
            $printUrl = $orderLine->getMetadata()['design_url'];
        }

        return $printUrl;
    }

    /**
     * @param OrderLineInterface $orderLine
     *
     * @return StreamedResponse
     */
    public function downloadPrintDesign(OrderLineInterface $orderLine): ?StreamedResponse
    {
        $printUrl = $this->getDesignPrintUrl($orderLine);

        if ($printUrl !== null) {
            $ext = pathinfo(strstr($printUrl, '?', true), PATHINFO_EXTENSION);

            $orderNumber = $orderLine->getOrder()->getOrderNumber();

            $response = $this->proxyService->process($printUrl);

            $response->headers->set('Content-Disposition',
                'attachment; filename="Personalisatie-' . $orderNumber . '.' . $ext . '";');

            return $response;
        }

        return null;
    }

    /**
     * @param OrderLine $orderLine
     * @return string|null
     */
    public function getPersonalizationPreviewImage(OrderLine $orderLine): ?string
    {
        $metadata = $orderLine->getMetadata();

        if (!isset($metadata['designer'])) {
            return null;
        }

        $this->designer->setUuid($metadata['designer']);

        return $this->designer->getFileUrl(Designer::FILE_PREVIEW, 60, Designer::SOURCE_LOCAL);
    }

    /**
     * @param OrderLineInterface $orderLine
     *
     * @return bool
     */
    private function convertDesign(OrderLineInterface $orderLine): bool
    {
        if (!isset($orderLine->getMetadata()['designer'])) {
            throw new \RuntimeException("Designer 'UUID' not defined");
        }

        $uuid = $orderLine->getMetadata()['designer'];
        $this->designer->setUuid($uuid);

        if (!$this->designer->fileExist(Designer::FILE_PRINT)) {
            $convertUrl = $this->router->generate('admin_designer_designer_screenshot', [
                'uuid' => $uuid,
                'product' => $orderLine->getMainProduct()
            ], UrlGeneratorInterface::ABSOLUTE_URL);

            $this->designer->setConvertUrl($convertUrl);

            $this->designer->convertJsonIntoImage();

            return true;
        }

        return false;
    }
}
