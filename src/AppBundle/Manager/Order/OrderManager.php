<?php

namespace AppBundle\Manager\Order;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Services\ConnectorHelper;
use AppBundle\Services\Sales\Order\OrderActivityService;
use Doctrine\ORM\EntityManagerInterface;
use function in_array;
use LogicException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Workflow\Registry;
use Symfony\Component\Workflow\Workflow;

/**
 * Class OrderManager
 * @package AppBundle\Manager\Order
 */
class OrderManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var ConnectorHelper
     */
    protected $connectorHelper;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var OrderActivityService
     */
    protected $orderActivityService;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var OrderLineManager
     */
    protected $orderLineManager;

    /**
     * OrderManager constructor.
     * @param EntityManagerInterface        $entityManager
     * @param ConnectorHelper      $connectorHelper
     * @param TokenStorageInterface         $tokenStorage
     * @param OrderActivityService $orderActivityService
     * @param Registry             $registry
     * @param OrderLineManager     $orderLineManager
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ConnectorHelper $connectorHelper,
        TokenStorageInterface $tokenStorage,
        OrderActivityService $orderActivityService,
        Registry $registry,
        OrderLineManager $orderLineManager
    ) {
        $this->entityManager = $entityManager;
        $this->connectorHelper = $connectorHelper;
        $this->tokenStorage = $tokenStorage;
        $this->orderActivityService = $orderActivityService;
        $this->orderLineManager = $orderLineManager;
        $this->registry = $registry;
    }

    /**
     * @param Order $order
     * @param       $reason
     * @param       $type
     * @return Order
     */
    public function cancel(Order $order, $reason, $type)
    {
        $token = $this->tokenStorage->getToken();

        $user = null;

        if (null !== $token) {
            $user = $token->getUser();
        }

        $workflow = $this->registry->get($order, 'order_status');

        $supplierOrder = $order->getSupplierOrder();

        if ($supplierOrder) {
            $supplier = $supplierOrder->getSupplier();

            if(!$workflow->can($order, 'recall')) {
                throw new LogicException('Order cannot be recalled');
            }

            //todo fix when workflows are reevaluated
            $supplierOrder->setStatus('cancelled');
            $connector = $this->connectorHelper->getConnector($supplierOrder);

            if (null !== $connector) {
                $connector->cancel($supplierOrder);
            }

            if ($type === 'consumer') {
                $text = 'Wegens annulering van order';
            } else {
                $text = $reason;
            }

            $text .= PHP_EOL . sprintf('Oorspronkelijle leverancier: %s (%s)', $supplier->getName(), $supplier->getId());
            $this->orderActivityService->add('order_canceled_at_supplier', $order, $user, [ 'text' => $text ]);

        }

        $order->setSupplierOrder(null);

        if (($type === 'consumer') && $workflow->can($order, 'cancel')) {
            $workflow->apply($order, 'cancel');

            $orderCollection = $order->getOrderCollection();
            $nonCancelledOrders = $orderCollection->getOrders()->exists(function ($id, Order $order) {
                void($id);
                return $order->getStatus() !== 'cancelled';
            });

            //remove vouchers from orderCollection if all orders are cancelled
            if (!$nonCancelledOrders) {
                $orderCollection->removeAllVouchers();
            }

            $this->orderActivityService->add('order_cancelled', $order, $user, ['text' => $reason]);
        }

        return $order;
    }

    /**
     * @param Order $order
     */
    public function submit(Order $order): void
    {
        $this->applyWorkflowTransition($order, 'submit');
    }

    /**
     * @param Order $order
     */
    public function acceptPayment(Order $order): void
    {
        $this->applyWorkflowTransition($order, 'accept_payment');
    }

    /**
     * @param Order $order
     */
    public function complete(Order $order): void
    {

    }

    /**
     * @param Order $order
     */
    public function recall(Order $order): void
    {

    }

    /**
     * @param Order $order
     * @return float
     */
    public function getTotalDiscountAmountExcl(Order $order): float
    {
        $total = 0;
        foreach ($order->getLines() as $line) {
            $total+= $this->orderLineManager->getDiscountAmountExcl($line);
        }

        return $total;
    }

    /**
     * @param Order $order
     * @return float
     */
    public function getTotalPriceIncl(Order $order): float
    {
        $total = 0;
        foreach ($order->getLines() as $line) {
            $total+= $this->orderLineManager->getTotalPriceIncl($line);
        }

        return $total;
    }

    /**
     * @param Order $order
     * @return Order
     */
    public function calculateTotals(Order $order): Order
    {
        $orderTotal = 0;
        $orderTotalIncl = 0;

        foreach($order->getLines() as $line) {
            foreach($line->getVatLines() as $vatLine) {
                $orderTotal += $vatLine->getAmount();

                $orderTotalIncl += $vatLine->getAmount() * (1+ $vatLine->getVatRate()->getFactor());
            }

            $orderTotalIncl -= $line->getDiscountAmount();
            $orderTotal -= $this->orderLineManager->getDiscountAmountExcl($line);
        }

        $order->setTotalPrice($orderTotal);
        $order->setTotalPriceIncl(round($orderTotalIncl, 2));

        return $order;
    }

    /**
     * @param Order $order
     * @return Order
     */
    public function clone(Order $order) {
        $newOrder = new Order();
        $newOrder->setOrderCollection($order->getOrderCollection());
        $newOrder->setDeliveryAddress($order->getDeliveryAddress());

        $newOrder->setPriority($order->getPriority());

        if (null === $order->getDeliveryAddress()) {
            $newOrder->setDeliveryAddressCompanyName($order->getDeliveryAddressCompanyName());
            $newOrder->setDeliveryAddressAttn($order->getDeliveryAddressAttn());
            $newOrder->setDeliveryAddressStreet($order->getDeliveryAddressStreet());
            $newOrder->setDeliveryAddressNumber($order->getDeliveryAddressNumber());
            $newOrder->setDeliveryAddressNumberAddition($order->getDeliveryAddressNumberAddition());
            $newOrder->setDeliveryAddressPostcode($order->getDeliveryAddressPostcode());
            $newOrder->setDeliveryAddressCity($order->getDeliveryAddressCity());
            $newOrder->setDeliveryAddressCountry($order->getDeliveryAddressCountry());
            $newOrder->setDeliveryAddressPhoneNumber($order->getDeliveryAddressPhoneNumber());
        }

        $newOrder->setDeliveryDate($order->getDeliveryDate());

        $newOrder->setInternalRemark($order->getInternalRemark());
        $newOrder->setInvoiceReference($order->getInvoiceReference());

        foreach ($order->getInternalRemarks() as $internalRemark) {
            $newOrder->addInternalRemark($internalRemark);
        }

        foreach ($order->getLines() as $line) {
            if ($line->getParent() !== null) {
                continue;
            }

            $newLine = $this->cloneLine($line, $newOrder);

            if (false === $line->getChildren()->isEmpty()) {
                foreach ($line->getChildren() as $child) {
                    $newChild = $this->cloneLine($child, $newOrder);
                    $newChild->setParent($newLine);

                    $newLine->addChild($newChild);
                }
            }

            $newOrder->addLine($newLine);
        }

        $newOrder->getOrderCollection()->addOrder($newOrder);

        return $newOrder;
    }

    /**
     * @param OrderLine $line
     *
     * @param Order $order
     *
     * @return OrderLine
     * @throws \Exception
     */
    public function cloneLine(OrderLine $line, Order $order)
    {
        $newLine = (new OrderLine())
            ->setOrder($order)
            ->setProduct($line->getProduct())
            ->setPrice($line->getPrice())
            ->setQuantity($line->getQuantity())
            ->setDescription($line->getDescription())
            ->setQuantity($line->getQuantity())
            ->setDiscountAmount($line->getDiscountAmount())
            ->setTitle($line->getProduct()->getTitle())
            ->setMetadata($line->getMetadata())
            ->setLedger($line->getLedger());

        $this->orderLineManager->createVatLines($newLine);

        return $newLine;
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function isCancelable(Order $order): bool
    {
        return $this->registry->get($order)->can($order, 'cancel');
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function isProcessable(Order $order): bool
    {
        return $order->getStatus() === 'new';
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function isCountable(Order $order): bool
    {
        return $order->getStatus() !== 'cancelled';
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function isCommissionable(Order $order): bool
    {
        return in_array($order->getStatus(), ['processed', 'sent', 'complete', 'archived'], true);
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function isInvoiceable(Order $order): bool
    {
        return in_array($order->getStatus(), ['pending', 'processed', 'sent', 'complete'], true);
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function isFraudCheckable(Order $order): bool
    {
        return in_array($order->getStatus(), ['payment_pending', 'new'], true);
    }

    public function isEditable(Order $order): bool
    {
        return $order->getOrderCollection()->getFraudScore() !== null
            && in_array($order->getStatus(), ['payment_pending', 'new', 'draft'], true);
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function canChangeToDraft(Order $order): bool
    {
        // todo: use workflow
        return $order->getOrderCollection()->getFraudScore() !== null
            && false === in_array($order->getStatus(), ['draft', 'cancelled'], true);
    }

    /**
     * @param Order $order
     * @return Workflow
     */
    public function getWorkflow(Order $order): Workflow
    {
        return $this->registry->get($order, 'order_status');
    }

    /**
     * @param Order $order
     * @param string $transitionName
     */
    private function applyWorkflowTransition(Order $order, string $transitionName)
    {
        $this->registry->get($order)->apply($order, $transitionName);

        $this->entityManager->flush();
    }
}
