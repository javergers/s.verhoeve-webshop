<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Site;
use AppBundle\Event\CustomerEvent;
use AppBundle\Events\CustomerEvents;
use AppBundle\Exceptions\CustomerDisabledException;
use AppBundle\Exceptions\CustomerNotFoundException;
use AppBundle\Exceptions\CustomerPasswordAlreadyRequestedException;
use AppBundle\Services\JobManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\TokenGenerator;
use JMS\JobQueueBundle\Entity\Job;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class CustomerManager
 *
 * @package AppBundle\Services
 */
class CustomerManager
{
    /** @var UserManager */
    private $fosUserManager;

    /** @var TokenGenerator */
    private $fosTokenGenerator;

    /** @var int */
    private $fosResettingTokenTtl;

    /** @var JobManager */
    private $jobManager;

    /** @var Router */
    private $router;

    /** @var EventDispatcherInterface */
    private $dispatcher;

    /** @var RequestStack */
    private $requestStack;

    /** @var JWTManager */
    private $jwtManager;

    /** @var bool */
    private $autoFlush = true;

    /** @var Request */
    private $currentRequest;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var TranslatorInterface */
    private $translator;

    /**
     * CustomerManager constructor.
     *
     * @param UserManager              $fosUserManager
     * @param TokenGenerator           $fosTokenGenerator
     * @param ParameterBagInterface    $parameterBag
     * @param JobManager               $jobManager
     * @param Router                   $router
     * @param EventDispatcherInterface $dispatcher
     * @param RequestStack             $requestStack
     * @param JWTManager               $jwtManager
     * @param EntityManagerInterface   $entityManager
     */
    public function __construct(
        UserManager $fosUserManager,
        TokenGenerator $fosTokenGenerator,
        ParameterBagInterface $parameterBag,
        JobManager $jobManager,
        Router $router,
        EventDispatcherInterface $dispatcher,
        RequestStack $requestStack,
        JWTManager $jwtManager,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator
    ) {
        $this->fosUserManager = $fosUserManager;
        $this->fosTokenGenerator = $fosTokenGenerator;
        $this->fosResettingTokenTtl = $parameterBag->get('fos_user.resetting.token_ttl');
        $this->jobManager = $jobManager;
        $this->router = $router;
        $this->dispatcher = $dispatcher;
        $this->requestStack = $requestStack;
        $this->jwtManager = $jwtManager;
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * @param Customer $customer
     * @return UserInterface $customer
     */
    public function create(Customer $customer): UserInterface
    {
        $this->fosUserManager->updateUser($customer, false);

        $this->dispatcher->dispatch(CustomerEvents::CREATED, new CustomerEvent($customer, $this->getRequest()));

        return $customer;
    }

    /**
     * @param UserInterface $customer
     */
    public function update(UserInterface $customer): void
    {
        $this->fosUserManager->updateUser($customer, $this->autoFlush);
    }

    /**
     * @param string|UserInterface $customer Customer object or email/username as string
     *
     * @return bool
     */
    public function remove($customer): bool
    {
        if (!($customer instanceof UserInterface)) {
            $customer = $this->findCustomerByUsernameOrEmail($customer);
        }

        try {
            $this->fosUserManager->deleteUser($customer);

            $this->dispatcher->dispatch(CustomerEvents::REMOVED,
                new CustomerEvent($customer, $this->getRequest()));

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param string|UserInterface $customer Customer object or email/username as string
     *
     * @return UserInterface
     */
    public function convertToAccount($customer): UserInterface
    {
        if (!($customer instanceof UserInterface)) {
            $customer = $this->findCustomerByUsernameOrEmail($customer);
        }

        $customer->setUsername($customer->getEmail());
        $customer->setEnabled(true);

        $this->update($customer);

        $this->dispatcher->dispatch(CustomerEvents::CONVERTED_TO_CUSTOMER_ACCOUNT,
            new CustomerEvent($customer, $this->getRequest()));

        return $customer;
    }

    /**
     * Activates the given customer.
     *
     * @param string|UserInterface $customer Customer object or email/username as string
     *
     * @return UserInterface
     */
    public function activate($customer): UserInterface
    {
        if (!($customer instanceof UserInterface)) {
            $customer = $this->findCustomerByUsernameOrEmail($customer);
        }

        $customer->setEnabled(true);

        $this->update($customer);

        $this->dispatcher->dispatch(CustomerEvents::ACTIVATED,
            new CustomerEvent($customer, $this->getRequest()));

        return $customer;
    }

    /**
     * Deactivates the given customer.
     *
     * @param string|UserInterface $customer Customer object or email/username as string
     *
     * @return UserInterface
     */
    public function deactivate($customer): UserInterface
    {
        if (!($customer instanceof UserInterface)) {
            $customer = $this->findCustomerByUsernameOrEmail($customer);
        }

        $customer->setEnabled(false);

        $this->update($customer);

        $this->dispatcher->dispatch(CustomerEvents::DEACTIVATED,
            new CustomerEvent($customer, $this->getRequest()));

        return $customer;
    }

    /**
     * Changes the password for the given customer.
     *
     * @param string|UserInterface $customer Customer object or email/username as string
     * @param string               $password
     *
     * @return UserInterface
     */
    public function changePassword($customer, string $password): UserInterface
    {
        if (!($customer instanceof UserInterface)) {
            $customer = $this->findCustomerByUsernameOrEmail($customer);
        }

        $customer->setPlainPassword($password);

        $this->update($customer);

        $this->dispatcher->dispatch(CustomerEvents::PASSWORD_CHANGED,
            new CustomerEvent($customer, $this->getRequest()));

        return $customer;
    }

    /**
     * Request customer password reset and optionally send reset password mail
     *
     * @param string|UserInterface|Customer $customer
     * @param bool                          $sendResetEmail
     *
     * @throws CustomerNotFoundException
     * @throws CustomerDisabledException
     * @throws CustomerPasswordAlreadyRequestedException
     * @throws \Exception
     *
     * @return Customer|UserInterface|string
     */
    public function requestResetPassword($customer, bool $sendResetEmail = true)
    {
        if (!($customer instanceof UserInterface)) {
            $customer = $this->findCustomerByUsernameOrEmail($customer);
        }

        if ($customer === null) {
            throw new CustomerNotFoundException('No customer received');
        }

        if (!$customer->isEnabled()) {
            throw new CustomerDisabledException('Customer is disabled');
        }

        if ($customer->isPasswordRequestNonExpired($this->fosResettingTokenTtl)) {
            throw new CustomerPasswordAlreadyRequestedException('Password request already requested');
        }

        if (null === $customer->getConfirmationToken()) {
            $customer->setConfirmationToken($this->fosTokenGenerator->generateToken());
        }

        // Create job for sending the mail with reset information
        if ($sendResetEmail && null !== ($registeredSite = $customer->getRegisteredOnSite())) {
            $domain = $registeredSite->getPrimaryDomain();

            $job = new Job('topbloemen:site:send-password-reset-mail', [
                $customer->getId(),
                $domain->getId(),
            ], true, 'reset_password');

            $this->jobManager->addJob($job);
        }

        $customer->setPasswordRequestedAt(new \DateTime());

        $this->update($customer);

        return $customer;
    }

    /**
     * Reset customer password
     *
     * @param string                             $confirmationToken
     * @param string                             $password
     * @param null|string|UserInterface|Customer $customer
     *
     * @return Customer|UserInterface|null|string
     *
     * @throws CustomerNotFoundException
     */
    public function resetPassword(string $confirmationToken, string $password, $customer = null)
    {
        if (null === $customer) {
            $customer = $this->findByConfirmationToken($confirmationToken);
        } else {
            if (!($customer instanceof UserInterface)) {
                $customer = $this->findCustomerByUsernameOrEmail($customer);
            }

            if ($customer->getConfirmationToken() !== $confirmationToken) {
                $customer = null;
            }
        }

        if (null === $customer) {
            throw new CustomerNotFoundException(
                sprintf('The customer with "confirmation token" does not exist for value "%s"', $confirmationToken)
            );
        }

        $customer->setPlainPassword($password);
        $customer->setConfirmationToken(null);
        $customer->setPasswordRequestedAt(null);

        $this->update($customer);

        return $customer;
    }

    /**
     * Adds role to the given customer.
     *
     * @param string|UserInterface $customer Customer object or email/username as string
     * @param string               $role
     *
     * @return bool|UserInterface if role was added, false if user already had the role
     */
    public function addRole($customer, string $role)
    {
        if (!($customer instanceof UserInterface)) {
            $customer = $this->findCustomerByUsernameOrEmail($customer);
        }

        if ($customer->hasRole($role)) {
            return false;
        }

        $customer->addRole($role);

        $this->update($customer);

        $this->dispatcher->dispatch(CustomerEvents::ROLE_ADDED,
            new CustomerEvent($customer, $this->getRequest(), [
                'role' => $role,
            ]));

        return $customer;
    }

    /**
     * Removes role from the given customer.
     *
     * @param string|UserInterface $customer Customer object or email/username as string
     * @param string               $role
     *
     * @return bool|UserInterface if role was removed, false if user didn't have the role
     */
    public function removeRole($customer, string $role)
    {
        if (!($customer instanceof UserInterface)) {
            $customer = $this->findCustomerByUsernameOrEmail($customer);
        }

        if (!$customer->hasRole($role)) {
            return false;
        }

        $customer->removeRole($role);

        $this->update($customer);

        $this->dispatcher->dispatch(CustomerEvents::ROLE_REMOVED,
            new CustomerEvent($customer, $this->getRequest(), [
                'role' => $role,
            ]));

        return $customer;
    }

    /**
     * Logins the given user on the given site
     *
     * @param Customer  $customer
     * @param Site|null $site
     *
     * @return string
     */
    public function login(Customer $customer, Site $site = null): string
    {
        if (null === $site) {
            throw new \RuntimeException('No site received');
        }

        $session = null !== $this->getRequest() ? $this->getRequest()->getSession() : null;
        if (null === $session) {
            throw new \RuntimeException('No session received');
        }

        $scheme = 'https';
        $port = null;

        if ($site->getHttpScheme()) {
            $scheme = $site->getHttpScheme();
        }

        if (null !== $site->getPrimaryDomain() && $site->getPrimaryDomain()->getDomain()) {
            $host = $site->getPrimaryDomain()->getDomain();
        } else {
            throw new \RuntimeException('No host found');
        }

        $port = 443;
        $sitePort = $site->getHttpPort();

        if ((null !== $sitePort && $sitePort !== 80) || $sitePort !== 443) {
            $port = $site->getHttpPort();
        }

        $context = $this->router->getContext();
        $context->setHost($host);
        $context->setScheme($scheme);

        if ($scheme === 'https') {
            $context->setHttpsPort($port);
        } else {
            $context->setHttpPort($port);
        }

        $session->set('employee_login_jwt', true);

        $url = $this->router->generate('app_home_index', [
            'bearer' => $this->jwtManager->create($customer),
            'update-last-login' => false,
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        return $url;
    }

    /**
     * Disable autoflushing of customer entity
     */
    public function disableAutoFlush(): void
    {
        $this->autoFlush = false;
    }

    /**
     * Disable autoflushing of customer entity
     */
    public function enableAutoFlush(): void
    {
        $this->autoFlush = true;
    }

    /**
     * @param string $username
     *
     * @return UserInterface
     */
    public function findByUsername(string $username): ?UserInterface
    {
        return $this->fosUserManager->findUserBy(['username' => $username]);
    }

    /**
     * @param string $confirmationToken
     *
     * @return UserInterface
     */
    public function findByConfirmationToken(string $confirmationToken): ?UserInterface
    {
        return $this->fosUserManager->findUserBy(['confirmationToken' => $confirmationToken]);
    }

    /**
     * Finds a customer by his username or email and throws an exception if we can't find it.
     *
     * @param string $username
     *
     * @throws \InvalidArgumentException When user does not exist
     *
     * @return UserInterface
     */
    private function findCustomerByUsernameOrEmail(string $username): UserInterface
    {
        $customer = $this->fosUserManager->findUserByUsernameOrEmail($username);

        if (null === $customer) {
            throw new \InvalidArgumentException(sprintf('Customer identified by "%s" customer does not exist.',
                $username));
        }

        return $customer;
    }

    /**
     * @return Request
     */
    private function getRequest(): ?Request
    {
        if (null !== $this->currentRequest) {
            return $this->currentRequest;
        }

        return ($this->currentRequest = $this->requestStack->getCurrentRequest());
    }

    /**
     * @param Customer $customer
     * @param int      $limit
     * @return ArrayCollection|array
     */
    public function getRecentOrderCollections(Customer $customer, $limit = 5)
    {
        $qb = $this->entityManager->getRepository(OrderCollection::class)->createQueryBuilder('oc');
        $qb->andWhere('oc.customer = :customer')
            ->setMaxResults($limit)
            ->setParameters([
                'customer' => $customer,
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Customer $customer
     * @return string
     */
    public function getSalutation(Customer $customer): string
    {
        switch ($customer->getGender()) {
            case 'male':
                $salution = $this->translator->trans('salutation.male');
                break;
            case 'female':
                $salution = $this->translator->trans('salutation.female');
                break;
            default:
                $salution = $this->translator->trans('salutation.unknown');
        }

        return ucfirst(str_replace('%lastname%', ucfirst($customer->getLastname()), $salution));
    }

    /**
     * @param Customer $customer
     * @return string
     */
    public function getPersonalSalutation(Customer $customer): string
    {
        $salution = $this->translator->trans('salutation.personal');

        return ucfirst(str_replace('%firstname%', ucfirst($customer->getFirstname()), $salution));
    }
}
