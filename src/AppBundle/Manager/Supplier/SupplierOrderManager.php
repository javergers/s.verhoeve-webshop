<?php

namespace AppBundle\Manager\Supplier;

use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Services\ConnectorHelper;

/**
 * Class SupplierOrderManager
 * @package AppBundle\Manager\Supplier
 */
class SupplierOrderManager
{
    /**
     * @var ConnectorHelper
     */
    protected $connectorHelper;

    /**
     * SupplierOrderManager constructor.
     * @param ConnectorHelper $connectorHelper
     */
    public function __construct(ConnectorHelper $connectorHelper)
    {
        $this->connectorHelper = $connectorHelper;
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @return bool
     */
    public function hasForwardPriceToShow(SupplierOrder $supplierOrder): bool
    {
        $connector = $this->connectorHelper->getConnector($supplierOrder);

        if ($connector && ($connector->requireForwardingPrices() || $connector->alwaysShowForwardingPrices())) {
            return true;
        }

        foreach ($supplierOrder->getLines() as $line) {
            if ($line->getPrice()) {
                return true;
            }
        }

        return false;
    }
}