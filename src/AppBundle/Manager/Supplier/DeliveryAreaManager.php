<?php

namespace AppBundle\Manager\Supplier;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyPreferredSupplier;
use AppBundle\Entity\Supplier\DeliveryArea;
use AppBundle\Exceptions\LinkedPreferredSuppliersException;
use AppBundle\Interfaces\EntityInterface;
use AppBundle\Manager\AbstractCrudManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\OptimisticLockException;

/**
 * Class DeliveryAreaManager
 * @package AppBundle\Services\Supplier
 */
class DeliveryAreaManager extends AbstractCrudManager
{
    /**
     * @param Collection   $deliveryAreaCollection
     * @param Collection   $preferredSettings
     *
     * @param Company|null $viewCompany
     * @return DeliveryAreaManager
     */
    public function markPreferredSuppliers(
        Collection $deliveryAreaCollection,
        Collection $preferredSettings,
        ?Company $viewCompany = null
    ) {
        $em = $this->entityManager;

        foreach ($deliveryAreaCollection as $deliveryArea) {
            // Get preferred supplier for current company view.
            $preferredSupplier = $em->getRepository(CompanyPreferredSupplier::class)->findOneBy([
                'deliveryArea' => $deliveryArea->getId(),
                'company' => null !== $viewCompany ? $viewCompany->getId() : null,
            ]);

            // Filter settings for current PreferredSupplier.
            $currentSetting = $preferredSettings->filter(function (array $setting) use ($deliveryArea) {

                /** @var CompanyPreferredSupplier $companyPreferredSupplier */
                $companyPreferredSupplier = $setting['companyPreferredSupplier'] ?? null;

                return null !== $companyPreferredSupplier->getId()
                    && ($companyPreferredSupplier->getId() === $deliveryArea->getCompany()->getId());
            })->first();

            if (false === $currentSetting) {
                if (null !== $preferredSupplier) {
                    $em->remove($preferredSupplier);
                }

                continue;
            }

            if(null !== $preferredSupplier) {
                $preferredSupplier->setPosition($currentSetting['position'] ?? 0);

                continue;
            }

            // Create foreach company view
            $preferredSupplier = new CompanyPreferredSupplier();
            $preferredSupplier->setCompany($viewCompany);
            $preferredSupplier->setDeliveryArea($deliveryArea);
            $preferredSupplier->setPosition($currentSetting['position'] ?? 0);

            $deliveryArea->addPreferredSupplier($preferredSupplier);

            $em->persist($preferredSupplier);
        }

        return $this;
    }

    /**
     * @param EntityInterface|DeliveryArea $deliveryArea
     *
     * @return void
     *
     * @throws OptimisticLockException
     * @throws LinkedPreferredSuppliersException
     */
    public function remove(EntityInterface $deliveryArea): void
    {
        $this->removePreferredSuppliers($deliveryArea);

        parent::remove($deliveryArea);
    }

    /**
     * @param ArrayCollection $deliveryAreaCollection
     *
     * @return ArrayCollection
     */
    public function filterOnPreferredSupplier(ArrayCollection $deliveryAreaCollection)
    {
        return $deliveryAreaCollection->filter(function (DeliveryArea $deliveryArea) {
            return $deliveryArea->getPreferredSuppliers()->count() > 0;
        });
    }


    /**
     * @param DeliveryArea $deliveryArea
     *
     * @throws LinkedPreferredSuppliersException
     */
    public function removePreferredSuppliers(DeliveryArea $deliveryArea)
    {
        $deletablePreferredSuppliers = new ArrayCollection();

        foreach ($deliveryArea->getPreferredSuppliers() as $preferredSupplier) {
            if ($preferredSupplier->getCompany() !== null) {
                throw new LinkedPreferredSuppliersException('Er zijn voorkeursleveranciers ingesteld op dit gebied die niet automatisch verwijderd kunnen worden.');
            }

            $deletablePreferredSuppliers->add($preferredSupplier);
        }

        foreach ($deletablePreferredSuppliers as $preferredSupplier) {
            $this->entityManager->remove($preferredSupplier);
        }
    }
}