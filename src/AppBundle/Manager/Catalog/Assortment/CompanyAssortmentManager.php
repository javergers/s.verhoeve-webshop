<?php

namespace AppBundle\Manager\Catalog\Assortment;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Assortment\AssortmentProduct;
use AppBundle\Entity\Relation\Company;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CompanyAssortmentManager
 * @package AppBundle\Manager\Catalog\Assortment
 */
class CompanyAssortmentManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * CompanyAssortmentManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Company    $company
     * @param Assortment $assortment
     * @param bool       $setNewOwner
     * @return Assortment
     */
    public function duplicate(Company $company, Assortment $assortment, $setNewOwner = false): Assortment
    {
        $this->entityManager->beginTransaction();

        if ($setNewOwner) {
            $newAssortment = new Assortment();
            $newAssortment->setOwner($company);
            $newAssortment->setName($assortment->getName());
            $newAssortment->addCompany($company);
            $newAssortment->setPublish($assortment->getPublish());
            $newAssortment->setAssortmentType($assortment->getAssortmentType());
            $newAssortment->setImage($assortment->getImage());
            $newAssortment->clearSites();

            /** @var Assortment $assortment */
            foreach ($assortment->getTranslations()->getIterator() as $translation) {
                $newTranslation = clone $translation;

                if (null !== $newTranslation->getSlug()) {
                    $newTranslation->setSlug($company->getName() . '-' . $newTranslation->getSlug());
                }

                $newAssortment->addTranslation($newTranslation);
            }

            $this->entityManager->persist($newAssortment);

            $productIds = [];
            foreach ($assortment->getAssortmentProducts() as $assortmentProduct) {
                if (\in_array($assortmentProduct->getProduct()->getId(), $productIds, true)) {
                    continue;
                }
                $productIds[] = $assortmentProduct->getProduct()->getId();

                $newAssortmentProduct = new AssortmentProduct();
                $newAssortmentProduct->setProduct($assortmentProduct->getProduct());
                $newAssortmentProduct->setPosition($assortmentProduct->getPosition());
                $newAssortmentProduct->setAssortment($newAssortment);

                $this->entityManager->persist($newAssortmentProduct);
            }

            $assortment = $newAssortment;
        } else {
            $assortment->addCompany($company);
        }


        $this->entityManager->flush();
        $this->entityManager->commit();

        return $assortment;
    }
}
