<?php

namespace AppBundle\Manager\Catalog\Product;

use AppBundle\Decorator\Catalog\Product\CompanyProductDecorator;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Assortment\AssortmentProduct;
use AppBundle\Entity\Catalog\Product\Combination;
use AppBundle\Entity\Catalog\Product\CompanyCard;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Catalog\Product\ProductSticker;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Interfaces\Catalog\Product\ProductDecoratorInterface;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Services\Parameter\ParameterService;
use AppBundle\Services\SupplierManagerService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Exception;
use RuleBundle\Service\ResolverService;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ProductManager
 * @package AppBundle\Manager
 */
class ProductManager
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var ResolverService
     */
    private $resolverService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ParameterService
     */
    private $parameterService;

    /**
     * @var SupplierManagerService
     */
    private $supplierManager;

    /**
     * ProductManager constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param ResolverService $resolverService
     * @param EntityManagerInterface $entityManager
     * @param ParameterService $parameterService
     * @param SupplierManagerService $supplierManager
     */
    public function __construct(TokenStorageInterface $tokenStorage, ResolverService $resolverService, EntityManagerInterface $entityManager, ParameterService $parameterService, SupplierManagerService $supplierManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->parameterService = $parameterService;
        $this->resolverService = $resolverService;
        $this->entityManager = $entityManager;
        $this->supplierManager = $supplierManager;
    }

    /**
     * @param Product $product
     * @return Product|CompanyProductDecorator
     */
    public function decorateProduct(Product $product): ProductInterface
    {
        if ($product instanceof CompanyProduct) {
            return new CompanyProductDecorator($product);
        }

        return $product;
    }

    /**
     * @param ProductInterface $product
     * @return ProductInterface
     */
    public function getEntity(ProductInterface $product)
    {
        //if productInterface is decorator, return entity
        if ($product instanceof ProductDecoratorInterface) {
            return $product->getEntity();
        }

        return $product;
    }

    /**
     * @param ProductInterface $product
     * @return ArrayCollection
     */
    public function listCards(ProductInterface $product)
    {
        if ($product instanceof Product) {
            $product = $this->decorateProduct($product);
        }

        /** @var Assortment $cards */
        $cards = $product->getCards();
        $cardsCollection = new ArrayCollection();

        /** @var Customer|null $customer */
        $customer = $this->tokenStorage->getToken() ? $this->tokenStorage->getToken()->getUser() : null;

        if (null !== $cards) {
            $company = null;
            $hideDefaultCards = false;
            if ($customer instanceof Customer && null !== $customer->getCompany()) {
                $company = $customer->getCompany();

                $hideDefaultCards = $this->parameterService->setEntity(Company::class)->getValue('catalog_cards_explicit_assignment',
                    $company->getId());
            }

            /** @var AssortmentProduct $assortmentProduct */
            foreach ($cards->getAssortmentProducts() as $assortmentProduct) {
                $card = $assortmentProduct->getProduct();

                if (null !== $card) {
                    if(!$this->supplierManager->canCombineProductWith($product, $card)) {
                        continue;
                    }

                    if(!$hideDefaultCards || ($company !== null && $company->getCards()->contains($card))) {
                        $cardsCollection->add($card);
                    }

                    if (null !== $company) {
                        /** @var CompanyCard $companyCard */
                        foreach ($company->getCards() as $companyCard) {
                            if (null !== $companyCard->getParent() && $companyCard->getParent()->getId() === $card->getId()) {
                                $cardsCollection->add($companyCard);
                                break;
                            }
                        }
                    }
                }
            }
        }

        return $cardsCollection;
    }

    /**
     * @param ProductInterface $product
     * @return ProductCard|null
     */
    public function getCheapestCard(ProductInterface $product): ?ProductCard
    {
        $listedCards = $this->listCards($product);
        $cheapestCard = null;

        /**
         * @var ProductCard $card
         * @var ProductCard $cheapestCard
         */
        foreach ($listedCards as $card) {
            if (null === $cheapestCard || $card->getPrice() < $cheapestCard->getPrice()) {
                $cheapestCard = $card;
            }
        }

        return $cheapestCard;
    }

    /**
     * @param Product $product
     * @return bool
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function isUsedInAVariation(Product $product): bool
    {
        $productIds[] = $product->getId();

        if ($product->getParent() === null) {
            /** @var Product $variation * */
            foreach ($product->getVariations() as $variation) {
                $productIds[] = $variation->getId();
            }
        } else {
            $productIds[] = $product->getParent()->getId();
        }

        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder
            ->select('count(c.id)')
            ->from(Combination::class, 'c')
            ->where('c.product in (:productIds)')
            ->setParameter(':productIds', $productIds);

        return $queryBuilder->getQuery()->getSingleScalarResult() > 1;
    }

    /**
     * @param Product $product
     * @return ProductSticker|null
     * @throws Exception
     */
    public function getSticker(ProductInterface $product)
    {
        /** @var ProductSticker[] $stickers */
        $stickers = $this->entityManager->getRepository(ProductSticker::class)->findAll();
        foreach ($stickers as $sticker) {
            if ($this->resolverService->satisfies($product, $sticker->getRule())) {
                return $sticker;
            }
        }

        return null;
    }

    /**
     * @param Product $product
     * @return array
     */
    public function getUpsellProducts(ProductInterface $product)
    {
        $upsells = $product->getUpsells()->toArray();

        foreach($product->getVariations() as $variation) {
            foreach($variation->getUpsells() as $upsell) {
                $upsells[] = $upsell;
            }
        }

        return $upsells;
    }
}
