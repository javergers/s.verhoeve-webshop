<?php

namespace AppBundle\Manager\Catalog\Product;

use AppBundle\Entity\Catalog\Product\Combination;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Personalization;
use AppBundle\Entity\Catalog\Product\Product;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductCombinationManager
 * @package AppBundle\Manager
 */
class ProductCombinationManager
{
    /** @var CacheManager */
    private $cacheManager;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ProductManager */
    private $productManager;

    /**  @var ProductPriceManager */
    private $productPriceManager;

    /**
     * ProductCombinationManager constructor.
     * @param CacheManager           $cacheManager
     * @param EntityManagerInterface $entityManager
     * @param ProductManager         $productManager
     * @param ProductPriceManager    $productPriceManager
     */
    public function __construct(
        CacheManager $cacheManager,
        EntityManagerInterface $entityManager,
        ProductManager $productManager,
        ProductPriceManager $productPriceManager
    )
    {
        $this->cacheManager = $cacheManager;
        $this->entityManager = $entityManager;
        $this->productManager = $productManager;
        $this->productPriceManager = $productPriceManager;
    }

    /**
     * @param Combination $combination
     * @param Product     $product
     * @return bool
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function isUsedInAnotherCombination(Combination $combination, Product $product): bool
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $this->entityManager->createQueryBuilder();

        $queryBuilder
            ->select('count(c.id)')
            ->from(Combination::class, 'c')
            ->where('c.product = :product')
            ->andWhere('c.id != :combination')
            ->setParameters([
                ':product' => $product->getId(),
                ':combination' => $combination->getId()
            ]);

        return $queryBuilder->getQuery()->getSingleScalarResult() > 0;
    }

    /**
     * @param $products
     * @return array
     * @throws Exception
     */
    public function productSelectionJson($products): array
    {
        $array = [];

        /** @var Product $product */
        foreach ($products as $product) {
            $array[] = [
                'id' => $product->getId(),
                'title' => $product->getName(),
                'image' => $product->getMainImage() ? $this->cacheManager->getBrowserPath(
                    $product->getMainImage()->getPath(),
                    'product_thumbnail'
                ) : null,
                'price' => $product->getCheapestVariation()->getPrice(),
            ];
        }

        return $array;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function combinationSelectionQuery(Request $request): JsonResponse
    {
        $products = new ArrayCollection();
        $repo = $this->entityManager->getRepository(Product::class);
        $combination = $repo->find( $request->get('combination_id'));
        $searchTerm = $request->get('query');
        $existingProducts = [];
        $supplierIds = [];
        $supplierGroupIds = [];

        foreach ($combination->getCombinations() as $existing) {
            $existingProducts[] = $existing->getProduct()->getId();
        }

        /** @var GenericProduct $firstVariation */
        $firstVariation = $combination->getCombinations()->first()->getProduct()->getVariations()->first();

        if ($firstVariation !== false && !$firstVariation->getSupplierProducts()->isEmpty()) {
            foreach ($firstVariation->getSupplierProducts() as $supplierProduct) {
                $supplierIds[] = $supplierProduct->getSupplier()->getId();
            }
        }

        if ($firstVariation !== false && !$firstVariation->getSupplierGroupProducts()->isEmpty()) {
            foreach ($firstVariation->getSupplierGroupProducts() as $supplierGroupProducts) {
                $supplierGroupIds[] = $supplierGroupProducts->getSupplierGroup()->getId();
            }
        }

        $supplierProductsResult = empty($supplierIds) ? [] : $repo->getSupplierProducts(
            $supplierIds, $searchTerm, $existingProducts
        );

        $supplierGroupProductsResult = empty($supplierGroupIds) ? [] : $repo->getSupplierGroupProducts(
            $supplierGroupIds, $searchTerm, $existingProducts
        );

        $combinableProducts = array_merge($supplierProductsResult, $supplierGroupProductsResult);

        foreach ($combinableProducts as $result) {
            if (!$products->contains($result)) {
                $products->add($result);
            }
        }

        return new JsonResponse($this->productSelectionJson($products));
    }

    /**
     * @param Combination $combination
     * @param Product     $product
     * @return array
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function removeProductConditions(Combination $combination, Product $product): array
    {
        $result = [];

        if ($combination->getCombinationProduct()->getCombinations()->count() < 2) {
            $result['error'] = 'De combinatie moet uit minstens één product bestaan';
        }

        if ($this->productManager->isUsedInAVariation($product)) {
            $result['error'] = 'Dit product bestaat al als variatie.';
        }

        if ($this->isUsedInAnotherCombination($combination, $product)) {
            $result['error'] = 'Dit product is al in een combinatie opgenomen.';
        }

        return $result;
    }
}
