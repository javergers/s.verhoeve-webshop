<?php

namespace AppBundle\Manager;

use AppBundle\Interfaces\EntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;

/**
 * Class AbstractManager
 * @package AppBundle\Manager
 */
class AbstractCrudManager
{
    /** @var bool */
    private $autoFlush = false;

    /** @var EntityManagerInterface */
    protected $entityManager;

    /**
     * AbstractCrudManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return AbstractCrudManager
     */
    public function enableAutoFlush(): AbstractCrudManager
    {
        $this->autoFlush = true;

        return $this;
    }

    /**
     * @return $this
     */
    public function disableAutoFlush(): AbstractCrudManager
    {
        $this->autoFlush = false;

        return $this;
    }

    /**
     * @param EntityInterface|null $entity
     *
     * @return void
     *
     * @throws OptimisticLockException
     */
    public function create(EntityInterface $entity): void
    {
        $this->entityManager->persist($entity);

        if ($this->autoFlush) {
            $this->entityManager->flush();
        }
    }

    /**
     * @param EntityInterface $entity
     *
     * @return void
     *
     * @throws OptimisticLockException
     */
    public function update(EntityInterface $entity): void
    {
        if ($this->autoFlush) {
            $this->entityManager->flush();
        }
    }

    /**
     * @param EntityInterface $entity
     *
     * @return void
     *
     * @throws OptimisticLockException
     */
    public function remove(EntityInterface $entity): void
    {
        $this->entityManager->remove($entity);

        if ($this->autoFlush) {
            $this->entityManager->flush();
        }
    }
}