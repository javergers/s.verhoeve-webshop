<?php

namespace AppBundle\Manager;

use AppBundle\DBAL\Types\MenuLocationType;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Site\Menu;
use AppBundle\Entity\Site\MenuItem;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class MenuManager
 * @package AppBundle\Manager
 */
class MenuManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * MenuManager constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Company $company
     * @return Menu
     */
    public function createForCompany(Company $company): Menu
    {
        if(null !== $company->getMenu()) {
            return $company->getMenu();
        }

        $menu = new Menu();
        $menu->setName($company->getName());
        $menu->setLocation(MenuLocationType::MENU_LOCATION_MAIN);

        $company->setMenu($menu);

        $this->entityManager->persist($menu);

        return $menu;
    }

    /**
     * @param Menu       $menu
     * @param Assortment $assortment
     * @return Menu
     */
    public function addAssortment(Menu $menu, Assortment $assortment): Menu
    {
        $menuItem = new MenuItem();
        $menuItem->setEntityName(Assortment::class);
        $menuItem->setEntityId($assortment->getId());
        $menuItem->setMenu($menu);
        $menuItem->setPosition($menu->getItems()->count() + 1);
        $menuItem->setBlank(false);
        $menuItem->setPublish(true);

        $this->entityManager->persist($menuItem);

        $menu->addItem($menuItem);

        return $menu;
    }

}