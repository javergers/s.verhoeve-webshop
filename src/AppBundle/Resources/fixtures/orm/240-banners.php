<?php


use AppBundle\Entity\Site\Banner;
use AppBundle\Entity\Site\BannerTranslation;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

$return = [
    Banner::class => [],
];

global $kernel;

$slugs = [
    'topfruit-home'
];

foreach ($slugs as $slug) {
    $path = 'src/AppBundle/Resources/fixtures/public/img/banner-' . $slug . '.jpg';

    if (!$kernel->getContainer()->get('filesystem')->exists($path)) {
        throw new FileNotFoundException(null, 0, null, 'public/img/' . $slug . '.jpg');
    }

    $url = 'images/banner/' . $slug . '/' . substr(md5($slug), 0, 13) . '.jpg';

    if (!$kernel->getContainer()->get('filesystem_public')->has($url)) {
        $kernel->getContainer()->get('filesystem_public')->putStream($url, fopen($path, 'r'));
    }

    $identifier = 'banner_' . str_replace('-', '_', $slug);

    $return[Banner::class][$identifier] = [
        'name' => $slug,
        'textBlockPosition' => 'tc',
        'image' => $url,
        'publish' => 1
    ];

    $return[BannerTranslation::class][$identifier .'_translation'] = [
        'translatable' => '@'. $identifier,
        'textBlockContent' => 'Lorem ipsum banner tekst',
        'locale' => 'NL_NL'
    ];
}

return $return;
