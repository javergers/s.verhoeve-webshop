<?php

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductImage;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

$return = [
    ProductImage::class => [],
];

global $kernel;

$slugs = [
    'fruitmand-basis',
    'fruitmand-jongen',
    'feestelijke-slagroomtaart',
    'feestelijke-slagroomtaart-met-opdruk',
    'boeket-sanne-standaard',
    'boeket-sanne-groot',
    'boeket-sanne-xl',
    'boeket-joyce-standaard',
    'boeket-joyce-groot',
    'boeket-joyce-xl',
    'boeket-jaimy-standaard',
    'boeket-jaimy-groot',
    'boeket-jaimy-xl',
    'boeket-hanna-standaard',
    'boeket-hanna-groot',
    'boeket-hanna-xl',
    'glazen-vaas',
    'bloemencadeaubon',
    'polo',
    'boeket-hanna',
    'boeket-joyce',
    'leffe-pakket',
    'bolcadeaukaart',
    'guilhem-rouge',
    'prestige-rood',
    'additional-tijdschriften-vrouw',
];

foreach ($slugs as $slug) {

    $doctrine = $kernel->getContainer()->get('doctrine');

    $filesystem = $kernel->getContainer()->get('filesystem_public');

    /** @var Product $product */
    $product = $doctrine->getManager()->getRepository(Product::class)->getBySlug($slug);

    if (!$product) {
        throw new \Exception(sprintf("Product with slug '%s' not found", $slug));
    }

    $path = 'src/AppBundle/Resources/fixtures/public/img/' . $slug . '.png';

    if (!$kernel->getContainer()->get('filesystem')->exists($path)) {
        throw new FileNotFoundException(null, 0, null, 'public/img/' . $slug . '.png');
    }

    $url = 'images/product/' . $product->getId() . '/' . substr(md5($slug), 0, 13) . '.png';

    if (!$filesystem->has($url)) {
        $filesystem->putStream($url, fopen($path, 'rb'));
    }

    $productKey = str_replace('-', '_', $slug);

    $return[ProductImage::class]['productimage_' . $productKey] = [
        'product' => '@product_' . $productKey,
        'path' => $url,
        'main' => true,
        'position' => 1,
    ];
}

return $return;
