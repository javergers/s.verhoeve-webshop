<?php

use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Designer\DesignerCollage;
use AppBundle\Entity\Designer\DesignerFont;
use AppBundle\Entity\Designer\DesignerTemplate;
use AppBundle\Entity\Common\Parameter\ParameterEntity;
use AppBundle\Entity\Common\Parameter\ParameterEntityValue;
use AppBundle\Entity\Discount\Discount;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Site\Site;
use AppBundle\Entity\Supplier\SupplierGroupProduct;
use AppBundle\Entity\Supplier\SupplierProduct;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\YesNoType;
use AdminBundle\Form\Parameter\ParameterEntityFileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use AppBundle\Entity\Finance\Tax\VatGroup;

function addParameters()
{
    /** @var \AppBundle\Services\Parameter\ParameterService $parameterService */
    $parameterService = $GLOBALS['kernel']->getContainer()->get('app.parameter');

    $parameterService->createViaArray([
        'key' => 'cookie_consent',
        'entity' => Site::class,
        'label' => 'Cookie Consent',
        'value' => null,
        'form_type' => ColumnType::class
    ]);

    $parameterService->createViaArray([
        'key' => 'cookie_consent_page_about_cookies',
        'entity' => Site::class,
        'label' => 'Pagina over cookies',
        'value' => null,
        'parent' => 'cookie_consent',
        'form_type' => EntityType::class,
        'form_type_class' => Page::class,
        'required' => false,
    ]);

    $parameterService->createViaArray([
        'key' => 'cookie_consent_categories_file',
        'entity' => Site::class,
        'label' => 'Bestand categorieen',
        'parent' => 'cookie_consent',
        'value' => null,
        'form_type' => ParameterEntityFileType::class,
        'required' => false,
    ]);

    $parameterService->createViaArray([
        'key' => 'cookie_consent_translation_file',
        'entity' => Site::class,
        'label' => 'Bestand vertalingen',
        'parent' => 'cookie_consent',
        'value' => null,
        'form_type' => ParameterEntityFileType::class,
        'required' => false,
    ]);

    $parameterService->createViaArray([
        'key' => 'company_credit_limit',
        'entity' => null,
        'label' => 'Krediet limiet bedrijven',
        'value' => 150,
    ]);

    $parameterService->createViaArray([
        'key' => 'fraud_warning_score',
        'entity' => null,
        'label' => 'Fraudescore waarschuwingsscore',
        'value' => 30
    ]);

    $parameterService->createViaArray([
        'key' => 'fraud_blocked_score',
        'entity' => null,
        'label' => 'Fraudescore blokkadescore',
        'value' => 50
    ]);

    $parameterService->createViaArray([
        'key' => 'combination_min_quantity',
        'entity' => null,
        'label' => 'Standaard minimale besteleenheid',
        'value' => 1
    ]);

    $parameterService->createViaArray([
        'key' => 'combination_max_quantity',
        'entity' => null,
        'label' => 'Standaard maximale besteleenheid',
        'value' => 100
    ]);

    $parameterService->createViaArray([
        'key' => 'wics_ftp_host',
        'entity' => null,
        'label' => 'WICS ftp host',
        'value' => null
    ]);

    $parameterService->createViaArray([
        'key' => 'wics_ftp_username',
        'entity' => null,
        'label' => 'WICS ftp gebruikersnaam',
        'value' => null
    ]);

    $parameterService->createViaArray([
        'key' => 'wics_ftp_password',
        'entity' => null,
        'label' => 'WICS ftp wachtwoord',
        'value' => null
    ]);

    $parameterService->createViaArray([
        'key' => 'wms_order_type_fallback',
        'entity' => null,
        'label' => 'WMS order type fallback',
        'value' => 'STDRF',
    ]);

    $parameterService->createViaArray([
        'key' => 'voicedata_api_account_id',
        'entity' => null,
        'label' => 'Voicedata API Account ID',
        'value' => null
    ]);

    $parameterService->createViaArray([
        'key' => 'voicedata_api_password',
        'entity' => null,
        'label' => 'Voicedata API Password',
        'value' => null
    ]);

    $parameterService->createViaArray([
        'key' => 'wics_incomplete_order_from',
        'entity' => null,
        'label' => 'WICS | Incomplete order van',
        'value' => 'magazijn@topgeschenken.nl'
    ]);

    $parameterService->createViaArray([
        'key' => 'wics_incomplete_order_to',
        'entity' => null,
        'label' => 'WICS | Incomplete order naar',
        'value' => 'klantenservice@topgeschenken.nl'
    ]);

    $parameterService->createViaArray([
        'key' => 'google_maps_place_url',
        'entity' => null,
        'label' => 'Google maps place url',
        'value' => 'https://www.google.com/maps/place/%s'
    ]);

    $parameterService->createViaArray([
        'key' => 'default_company_cards',
        'form_type' => EntityType::class,
        'form_type_class' => ProductCard::class,
        'multiple' => true,
        'entity' => null,
        'label' => 'Standaard bedrijfskaartjes',
        'value' => null
    ]);

    $parameterService->createViaArray([
        'key' => 'default_company_site',
        'entity' => null,
        'label' => 'Standaard site voor bedrijven',
        'value' => null,
        'form_type' => EntityType::class,
        'form_type_class' => Site::class,
        'required' => true
    ]);

    $parameterService->createViaArray([
        'key' => 'designer_ext_background',
        'entity' => DesignerTemplate::class,
        'label' => 'Ext. achtergrond',
        'value' => null,
        'form_type' => ColumnType::class,
        'form_type_class' => '',
        'required' => true,
    ]);

    $parameterService->createViaArray([
        'key' => 'designer_ext_background_image_enabled',
        'entity' => DesignerTemplate::class,
        'label' => 'Afbeelding actief',
        'value' => null,
        'parent' => 'designer_ext_background',
        'form_type' => YesNoType::class,
        'form_type_class' => '',
        'required' => true,
    ]);

    $parameterService->createViaArray([
        'key' => 'designer_ext_background_pattern_enabled',
        'entity' => DesignerTemplate::class,
        'label' => 'Patronen actief',
        'value' => null,
        'parent' => 'designer_ext_background',
        'form_type' => YesNoType::class,
        //'form_type_class' => '',
        'required' => true,
    ]);

    $parameterService->createViaArray([
        'key' => 'designer_ext_collage',
        'entity' => DesignerTemplate::class,
        'label' => 'Ext. Collage',
        'value' => null,
        'form_type' => ColumnType::class,
        //'form_type_class' => '',
        'required' => true,
    ]);

    $parameterService->createViaArray([
        'key' => 'designer_ext_collage_enabled',
        'entity' => DesignerTemplate::class,
        'label' => 'Actief',
        'value' => null,
        'parent' => 'designer_ext_collage',
        'form_type' => YesNoType::class,
        'form_type_class' => '',
        'required' => true,
    ]);

    $parameterService->createViaArray([
        'key' => 'designer_ext_collage_items',
        'entity' => DesignerTemplate::class,
        'label' => 'Collages',
        'value' => null,
        'parent' => 'designer_ext_collage',
        'form_type' => EntityType::class,
        'form_type_class' => DesignerCollage::class,
        'required' => false,
    ]);

    $parameterService->createViaArray([
        'key' => 'designer_ext_emoji',
        'entity' => DesignerTemplate::class,
        'label' => 'Ext. Emoji',
        'value' => null,
        'form_type' => ColumnType::class,
        'form_type_class' => '',
        'required' => true,
    ]);

    $parameterService->createViaArray([
        'key' => 'designer_ext_emoji_enabled',
        'entity' => DesignerTemplate::class,
        'label' => 'Actief',
        'value' => null,
        'parent' => 'designer_ext_emoji',
        'form_type' => YesNoType::class,
        'form_type_class' => '',
        'required' => true,
    ]);

    $parameterService->createViaArray([
        'key' => 'designer_ext_image',
        'entity' => DesignerTemplate::class,
        'label' => 'Ext. Image',
        'value' => null,
        'form_type' => ColumnType::class,
        'form_type_class' => '',
        'required' => true,
    ]);

    $parameterService->createViaArray([
        'key' => 'designer_ext_image_enabled',
        'entity' => DesignerTemplate::class,
        'label' => 'Actief',
        'value' => null,
        'parent' => 'designer_ext_image',
        'form_type' => YesNoType::class,
        'form_type_class' => '',
        'required' => true,
    ]);

    $parameterService->createViaArray([
        'key' => 'designer_ext_text',
        'entity' => DesignerTemplate::class,
        'label' => 'Ext. Text',
        'value' => null,
        'form_type' => ColumnType::class,
        'form_type_class' => '',
        'required' => true,
    ]);

    $parameterService->createViaArray([
        'key' => 'designer_ext_text_enabled',
        'entity' => DesignerTemplate::class,
        'label' => 'Actief',
        'value' => null,
        'parent' => 'designer_ext_text',
        'form_type' => YesNoType::class,
        'form_type_class' => '',
        'required' => true,
    ]);

    $parameterService->createViaArray([
        'key' => 'designer_ext_text_fonts',
        'entity' => DesignerTemplate::class,
        'label' => 'Lettertypen',
        'value' => null,
        'parent' => 'designer_ext_text',
        'form_type' => EntityType::class,
        'form_type_class' => DesignerFont::class,
        'required' => true,
    ]);

    $parameterService->createViaArray([
        'key' => 'show_product_sku',
        'entity' => Company::class,
        'label' => 'Artikelnrs. tonen in shop',
        'value' => '',
        'form_type' => YesNoType::class,
        'required' => false,
    ]);

    $parameterService->createViaArray([
        'key' => 'commission_fee_vatgroup',
        'entity' => null,
        'label' => 'Commissie fee BTW group',
        'value' => null,
        'form_type' => EntityType::class,
        'form_type_class' => VatGroup::class,
        'required' => true,
    ]);

    $parameterService->createViaArray([
        'key' => 'show_product_stock',
        'entity' => Company::class,
        'label' => 'Voorraad tonen bij product',
        'value' => '',
        'form_type' => YesNoType::class,
        'required' => false,
    ]);

    $parameterService->createViaArray([
        'key' => 'copernica_access_token',
        'entity' => null,
        'label' => 'Copernica access token',
        'value' => null,
    ]);

    $parameterService->createViaArray([
        'key' => 'supplier_product_preparation_time',
        'entity' => SupplierProduct::class,
        'label' => 'Productie tijd',
        'value' => '1',
        'form_type' => IntegerType::class,
        'required' => false,
    ]);

    $parameterService->createViaArray([
        'key' => 'supplier_group_product_preparation_time',
        'entity' => SupplierGroupProduct::class,
        'label' => 'Productie tijd',
        'value' => '1',
        'form_type' => IntegerType::class,
        'required' => false,
    ]);

    $parameterService->createViaArray([
        'key' => 'order_evaluator_matching_percentage',
        'entity' => null,
        'label' => 'Order evalutie vergelijkingspercentage',
        'value' => 75
    ]);

    $parameterService->createViaArray([
        'key' => 'catalog_cards_explicit_assignment',
        'form_type' => YesNoType::class,
        'entity' => Company::class,
        'label' => 'Standaardkaartjes verbergen',
        'value' => null,
    ]);

    $parameterService->createViaArray([
        'key' => 'order_evaluator_matching_percentage',
        'entity' => null,
        'label' => 'Order evalutie vergelijkingspercentage',
        'value' => 75
    ]);

    $parameterService->createViaArray([
        'key' => 'catalog_cards_explicit_assignment',
        'form_type' => YesNoType::class,
        'entity' => Company::class,
        'label' => 'Standaardkaartjes verbergen',
        'value' => null,
    ]);

    $parameterService->createViaArray([
        'key' => 'default_sender_notification_email',
        'entity' => null,
        'label' => 'Standaard afzender notificatie emailadres',
        'value' => 'no-reply@topgeschenken.nl',
    ]);

    $parameterService->createViaArray([
        'key' => 'order_restitution_voucher_values',
        'label' => 'Vergoeding bedragen voor restitution vouchers',
        'form_type_class' => Discount::class,
        'form_type' => EntityType::class,
        'multiple' => 1,
        'entity' => null,
        'required' => false,
    ]);

    $parameterService->createViaArray([
        'key' => 'order_restitution_voucher_relative_values',
        'label' => 'Vergoeding percentages voor restitution vouchers',
        'form_type_class' => Discount::class,
        'form_type' => EntityType::class,
        'multiple' => 1,
        'entity' => null,
        'required' => false,
    ]);

    $parameterService->createViaArray([
        'key' => 'order_restitution_voucher_max_relative_pct',
        'value' => 60,
        'label' => 'Maximale percentage van het orderbedrag (incl) dat een restitution voucher waard mag zijn',
        'entity' => null,
    ]);
}
addParameters();

$return = [
    ParameterEntityValue::class => [],
];

$converter = new CamelCaseToSnakeCaseNameConverter();

$parameterEntities = $GLOBALS['kernel']->getContainer()->get("doctrine")->getManager()->getRepository(ParameterEntity::class)->findByParameter([
    'designer_ext_background_image_enabled',
    'designer_ext_background_pattern_enabled',
    'designer_ext_collage_enabled',
    'designer_ext_emoji_enabled',
    'designer_ext_image_enabled',
    'designer_ext_text_enabled',
    'designer_ext_text_fonts',
]);

$templates = $GLOBALS['kernel']->getContainer()->get("doctrine")->getManager()->getRepository(DesignerTemplate::class)->findAll();

/** @var DesignerTemplate $template*/
foreach ($templates as $template) {

    $templateName = str_replace(' ', '_', $template->getName());

    /** @var parameterEntity $parameterEntity */
    foreach ($parameterEntities as $parameterEntity) {

        $return[ParameterEntityValue::class]['paremeter_entity_values_' . $parameterEntity->getParameter()->getKey() . '_' . $converter->normalize($templateName)] = [
            'entity' => $template->getId(),
            'parameterEntity' => $parameterEntity->getId(),
            'value' => 1,
        ];
    }
}

return $return;
