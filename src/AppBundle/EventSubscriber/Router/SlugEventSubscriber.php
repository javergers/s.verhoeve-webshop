<?php

namespace AppBundle\EventSubscriber\Router;

use AppBundle\Entity\Catalog\Assortment\AssortmentProduct;
use AppBundle\Interfaces\Seo\SlugInterface;
use AppBundle\Traits\GetUnitOfWorksTrait;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class SlugEventSubscriber
 * @package AppBundle\EventSubscriber\Router
 */
class SlugEventSubscriber implements EventSubscriber
{
    use GetUnitOfWorksTrait;

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            Events::postUpdate,
            Events::postPersist,
            Events::postRemove,
        ];
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function checkChange(LifecycleEventArgs $eventArgs)
    {
        $entityClass = $eventArgs->getObject();
        $slugUpdated = false;

        if ($entityClass instanceof SlugInterface) {
            $uow = $eventArgs->getEntityManager()->getUnitOfWork();
            $changeSet = $uow->getEntityChangeSet($entityClass);

            if ($changeSet && isset($changeSet['slug'])) {
                $slugUpdated = true;
            }

        } elseif ($entityClass instanceof AssortmentProduct) {
            $slugUpdated = true;
        }

        // Return when no slug update has been found
        if (!$slugUpdated) {
            return;
        }

        $job = new Job('router:warmup', [], true, 'routing');
        $this->container->get('job.manager')->addJob($job);
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        $this->checkChange($eventArgs);
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        $this->checkChange($eventArgs);
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postRemove(LifecycleEventArgs $eventArgs)
    {
        $this->checkChange($eventArgs);
    }
}
