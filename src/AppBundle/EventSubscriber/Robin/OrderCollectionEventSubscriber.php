<?php

namespace AppBundle\EventSubscriber\Robin;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Traits\GetUnitOfWorksTrait;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class OrderCollectionEventSubscriber
 * @package AppBundle\EventSubscriber\Robin
 */
class OrderCollectionEventSubscriber implements EventSubscriber
{
    use GetUnitOfWorksTrait;

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            Events::postUpdate,
            Events::postPersist,
            Events::postRemove,
        ];
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        /** @var  $uow */
        $uow = $eventArgs->getEntityManager()->getUnitOfWork();
        $changeSet = $uow->getEntityChangeSet($eventArgs->getEntity());

        //return if only fraudScore and updatedAt fields are updated
        if (array_key_exists('fraudScore', $changeSet) && \count($changeSet) <= 2) {
            return;
        }

        $this->addJobOnChange($eventArgs, OrderCollection::class, 'robin:sync-order', '', 'robin');
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        $this->addJobOnChange($eventArgs, OrderCollection::class, 'robin:sync-order', '', 'robin');
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postRemove(LifecycleEventArgs $eventArgs)
    {
        $this->addJobOnChange($eventArgs, OrderCollection::class, 'robin:sync-order', '', 'robin');
    }
}
