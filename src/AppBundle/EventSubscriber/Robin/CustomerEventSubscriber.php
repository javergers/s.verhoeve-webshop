<?php

namespace AppBundle\EventSubscriber\Robin;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Traits\GetUnitOfWorksTrait;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class CustomerEventSubscriber
 * @package AppBundle\EventSubscriber\Robin
 */
class CustomerEventSubscriber implements EventSubscriber
{
    use GetUnitOfWorksTrait;

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            Events::postUpdate,
            Events::postPersist,
            Events::postRemove,
        ];
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        $this->addJobOnChange($eventArgs, Customer::class, 'robin:sync-customer', '', 'robin');
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        $this->addJobOnChange($eventArgs, Customer::class, 'robin:sync-customer', '', 'robin');
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postRemove(LifecycleEventArgs $eventArgs)
    {
        $this->addJobOnChange($eventArgs, Customer::class, 'robin:sync-customer', '', 'robin');
    }
}
