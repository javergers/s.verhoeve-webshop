<?php

namespace AppBundle\EventSubscriber\Supplier;

use AppBundle\Entity\Order\Order;
use AppBundle\Traits\GetUnitOfWorksTrait;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

/**
 * Class BakeryOrderEventSubscriber
 * @package AppBundle\EventSubscriber\Supplier
 */
class BakeryOrderEventSubscriber implements EventSubscriber
{
    use GetUnitOfWorksTrait;

    public const BAKER = 'bakker';

    /**
     * @return array
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::preUpdate,
            Events::prePersist,
        ];
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function preUpdate(LifecycleEventArgs $eventArgs): void
    {
        $this->handle($eventArgs);
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function prePersist(LifecycleEventArgs $eventArgs): void
    {
        $this->handle($eventArgs);
    }

    /**
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */
    private function handle(LifecycleEventArgs $args): void
    {
        $object = $args->getObject();

        if (!$object instanceof Order) {
            return;
        }

        $changeSet = $args->getEntityManager()->getUnitOfWork()->getEntityChangeSet($object);
        unset($changeSet['updatedAt'], $changeSet['totalPrice']);

        if(count($changeSet) >= 1) {
        $supplierOrder = $object->getSupplierOrder();

        if ($supplierOrder === null || strtolower($supplierOrder->getConnector()) !== self::BAKER) {
            return;
        }

            $this->addJobOnChange($args, Order::class, 'bakery:sync-order');
        }

    }
}
