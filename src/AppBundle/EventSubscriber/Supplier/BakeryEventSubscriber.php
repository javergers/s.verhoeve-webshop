<?php

namespace AppBundle\EventSubscriber\Supplier;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Supplier\SupplierGroup;
use AppBundle\Traits\GetUnitOfWorksTrait;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

/**
 * Class BakeryEventSubscriber
 * @package AppBundle\EventSubscriber\Supplier
 */
class BakeryEventSubscriber implements EventSubscriber
{
    use GetUnitOfWorksTrait;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * BakeryEventSubscriber constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            Events::postUpdate,
            Events::postPersist,
            Events::postRemove,
        ];
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function checkChange(LifecycleEventArgs $eventArgs)
    {
        $this->addJobOnChange($eventArgs, Company::class, 'supplier:bakery:sync', 'validateObject');
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        $this->checkChange($eventArgs);
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        $this->checkChange($eventArgs);
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postRemove(LifecycleEventArgs $eventArgs)
    {
        $this->checkChange($eventArgs);
    }

    /**
     * @param Company $company
     * @param array   $changeSet
     * @return bool
     * @throws \Exception
     */
    protected function validateObject(Company $company, array $changeSet): bool
    {
        void($changeSet);

        $dateTime = new \DateTime();

        $bakeryGroup = $this->entityManager->getRepository(SupplierGroup::class)->findOneBy([
            'name' => 'Bakkers',
        ]);

        if (!$company->getIsSupplier()) {
            return false;
        }

        $isBakery = $company->getSupplierGroups()->exists(function ($key, $supplierGroup) use ($bakeryGroup) {
            void($key);

            return $supplierGroup === $bakeryGroup;
        });

        if (!$isBakery) {
            return false;
        }

        /** @var Company $child */
        foreach ($company->getChildren() as $child) {
            $child->setUpdatedAt($dateTime);
        }

        return true;
    }
}