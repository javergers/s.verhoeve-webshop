<?php

namespace AppBundle\EventSubscriber\Bloemorder;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Traits\GetUnitOfWorksTrait;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class BloemorderCustomerEventSubscriber
 * @package AppBundle\EventSubscriber\Bloemorder
 */
class BloemorderCustomerEventSubscriber implements EventSubscriber
{
    use GetUnitOfWorksTrait;

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            Events::postUpdate,
            Events::postPersist,
            Events::postRemove,
        ];
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        $this->addJobOnChange($eventArgs, Customer::class, 'sync:bloemorder:customers', 'validateObject', 'bloemorder');
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        $this->addJobOnChange($eventArgs, Customer::class, 'sync:bloemorder:customers', 'validateObject', 'bloemorder');
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postRemove(LifecycleEventArgs $eventArgs)
    {
        $this->addJobOnChange($eventArgs, Customer::class, 'sync:bloemorder:customers', 'validateObject', 'bloemorder');
    }

    /**
     * @param Customer $customer
     * @param array    $changeSet
     *
     * @return bool
     */
    protected function validateObject(Customer $customer, array $changeSet): bool
    {
        if(null === $customer->getCompany() || !isset($customer->getCompany()->getMetadata()['bloemorderCompanyId'])) {
            return false;
        }

        $totalChanges = \count($changeSet);
        return !(($totalChanges === 2 && isset($changeSet['updateAt'], $changeSet['lastLogin'])) || ($totalChanges === 1 && (isset($changeSet['updateAt']) || isset($changeSet['lastLogin']))));
    }
}
