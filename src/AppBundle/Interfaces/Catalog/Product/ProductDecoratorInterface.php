<?php

namespace  AppBundle\Interfaces\Catalog\Product;

/**
 * Interface ProductDecoratorInterface
 * @package AppBundle\Interfaces
 */
interface ProductDecoratorInterface
{
    /**
     * @return ProductInterface
     */
    public function getEntity();
}