<?php

namespace AppBundle\Interfaces;

interface MenuSourceInterface
{
    /**
     * @return array
     */
    public static function getMenuSourceConfig();

    /**
     * @return string
     */
    public function getMenuTitle();

    /**
     * @return string
     */
    public function getMenuType(): string;
}