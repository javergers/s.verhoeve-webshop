<?php

namespace AppBundle\Interfaces\Audit;

/**
 * Interface AuditEntityInterface
 * @package AppBundle\Interfaces\PageView
 */
interface AuditEntityInterface
{
    /**
     * @return array
     */
    public function getAuditSettings(): array;
}
