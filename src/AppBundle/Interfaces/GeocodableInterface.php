<?php

namespace AppBundle\Interfaces;

use CrEOF\Spatial\PHP\Types\Geometry\Point;

interface GeocodableInterface
{
    /**
     * @return Point|null
     */
    public function getPoint();

    /**
     * @param Point $point
     * @return $this
     */
    public function setPoint(Point $point);
}