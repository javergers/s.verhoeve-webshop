<?php

namespace AppBundle\Interfaces;

use AppBundle\Entity\Payment\Payment;

interface RedirectableMethodInterface
{
    public function generateUrl($host, $params);

    public function generateParams(Payment $payment);
}