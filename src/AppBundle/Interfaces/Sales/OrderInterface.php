<?php

namespace AppBundle\Interfaces\Sales;

use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Relation\Address;
use RuleBundle\Annotation as Rule;

/**
 * Interface OrderInterface
 * @Rule\EntityInterface(description="(Concept-)order")
 */
interface OrderInterface
{
    /**
     * @return ArrayCollection|OrderLineInterface[]
     * @Rule\Method(relatedEntity="AppBundle\Interfaces\Sales\OrderLineInterface", description="(Concept-)orderregels")
     */
    public function getLines();

    /**
     * @return int
     */
    public function getId();

    /**
     * @return OrderCollectionInterface
     * @Rule\Method(relatedEntity="AppBundle\Interfaces\Sales\OrderCollectionInterface", relation="parent", autoComplete={"id"="orderCollection.id", "label"="orderCollection.number"}, description="Bestelling of winkelwagen")
     */
    public function getCollection();

    /**
     * @return Country
     * @Rule\Method(description="Bezorgland")
     */
    public function getDeliveryAddressCountry();

    /**
     * @return string
     * @Rule\Method(description="Bezorgpostcode")
     */
    public function getDeliveryAddressPostcode();

    /**
     * @return \DateTime
     * @Rule\Method(description="Bezorgdatum")
     */
    public function getDeliveryDate();

   /**
     * Get pickupAddress
     *
     * @return null|Address
     */
    public function getPickupAddress();

    /**
     * @return float
     * @Rule\Method(description="Totaal prijs (Excl. BTW)")
     */
    public function getTotalPrice();

    /**
     * @return float
     * @Rule\Method(description="Totaal prijs (Incl. BTW)")
     */
    public function getTotalPriceIncl();
}
