<?php

namespace AppBundle\Interfaces\Sales;

use AppBundle\Entity\Discount\Voucher;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Site;
use Doctrine\Common\Collections\ArrayCollection;
use RuleBundle\Annotation as Rule;

/**
 * Interface OrderCollectionInterface
 * @package AppBundle\Interfaces\Sales
 * @Rule\EntityInterface(description="Bestelling of winkelwagen")
 */
interface OrderCollectionInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return Site
     * @Rule\Method(relatedEntity="AppBundle\Entity\Site\Site", relation="child", description="Site", autoComplete={"id"="site.id", "label"="site.translate.description"})
     */
    public function getSite();

    /**
     * @return Company
     * @Rule\Method(relatedEntity="AppBundle\Entity\Relation\Company", relation="child", description="Bedrijf", autoComplete={"id"="company.id", "label"="company.name"})
     */
    public function getCompany();

    /**
     * @return OrderInterface
     * @Rule\Method(relatedEntity="AppBundle\Interfaces\Sales\OrderInterface", relation="child", description="(Concept-)orders", autoComplete={"id"="order.id", "label"="order.number"})
     */
    public function getOrders();

    /**
     * @return float
     * @Rule\Method(description="Totaal prijs (Excl. BTW")
     */
    public function getTotalPrice();

    /**
     * @return float
     * @Rule\Method(description="Totaal prijs (Incl. BTW)")
     */
    public function getTotalPriceIncl();

    /**
     * @return Customer
     * @Rule\Method(relatedEntity="AppBundle\Entity\Relation\Customer", relation="child", description="Contactpersoon", autoComplete={"id"="customer.id", "label"="customer.username"})
     */
    public function getCustomer();

    /**
     * @param Voucher $voucher
     * @return mixed
     */
    public function addVoucher(Voucher $voucher);

    /**
     * @param Voucher $voucher
     * @return mixed
     */
    public function removeVoucher(Voucher $voucher);

    /**
     * @return ArrayCollection|Voucher[]
     */
    public function getVouchers();

    /**
     * @return Country|null
     */
    public function getInvoiceAddressCountry();
}