<?php

namespace AppBundle\Interfaces\Preview;

interface PreviewableEntityInterface
{
    /**
     * @return string|array
     */
    public function getController();

    /**
     * @return string
     */
    public function getControllerParam();
}