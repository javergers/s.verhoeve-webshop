<?php

namespace AppBundle\Interfaces;

/**
 * Interface EntityInterfaces
 * @package AppBundle\Interfaces
 */
interface EntityInterface
{
    /**
     * @return null|int
     */
    public function getId(): ?int;
}