<?php

namespace AppBundle\Interfaces\Product;

/**
 * Class PriceInterface
 * @package AppBundle\Interfaces\Product
 */
interface PriceInterface
{
    /**
     * @todo: fix return type null|float (not compatible with ProductModel)
     * @return float|null
     */
    public function getPrice();

    /**
     * @todo: fix return type null|float (not compatible with ProductModel)
     * @return float|null
     */
    public function getPriceIncl();
}