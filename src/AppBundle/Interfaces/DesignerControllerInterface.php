<?php

namespace AppBundle\Interfaces;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Interface DesignerControllerInterface
 * @package AppBundle\Interfaces
 */
interface DesignerControllerInterface
{
    /**
     * @param $templateUuid
     * @return JsonResponse
     */
    public function getTemplateJson($templateUuid);

    /**
     * @param $templateUuid
     * @return JsonResponse
     */
    public function getAlternativeTemplatesAction($templateUuid);
}