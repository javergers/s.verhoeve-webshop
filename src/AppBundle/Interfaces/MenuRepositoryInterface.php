<?php

namespace AppBundle\Interfaces;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Site\Site;

/**
 * Interface MenuRepositoryInterface
 * @package AppBundle\Interfaces
 */
interface MenuRepositoryInterface
{
    /**
     * @param Site $site
     * @param      $keyword
     * @return mixed
     */
    public function searchMenu(Site $site, $keyword);
}
