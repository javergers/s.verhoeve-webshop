<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class ProductgroupPropertyTargetType
 * @package AppBundle\DBAL\Types
 */
final class ProductgroupPropertyTargetType extends AbstractEnumType
{
    public const PRODUCT = 'product';
    public const PRODUCT_VARIATION = 'product_variation';

    protected static $choices = [
        self::PRODUCT => 'Product',
        self::PRODUCT_VARIATION => 'Productvariatie',
    ];
}