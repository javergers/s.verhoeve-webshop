<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class CompanyInvoiceTypeType
 * @package AppBundle\DBAL\Types
 */
final class CompanyInvoiceTypeType extends AbstractEnumType
{
    public const INVOICE_TYPE_PDF_MAIL = 'pdf_mail';
    public const INVOICE_TYPE_UBL_MAIL = 'ubl_mail';
    public const INVOICE_TYPE_UBL_GOV_NL = 'ubl_gov_nl';

    protected $name = 'company_invoice_type_type';

    protected static $choices = [
        self::INVOICE_TYPE_PDF_MAIL => 'PDF via mail',
        self::INVOICE_TYPE_UBL_MAIL => 'UBL via mail',
        self::INVOICE_TYPE_UBL_GOV_NL => 'UBL via e-facturatie',
    ];
}