<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class VoucherUsageType
 * @package AppBundle\DBAL\Types
 */
final class VoucherUsageType extends AbstractEnumType
{
    public const ONCE = 'once';
    public const LIMITED = 'limited';
    public const UNLIMITED = 'unlimited';

    protected static $choices = [
        self::ONCE => 'Eenmalig',
        self::LIMITED => 'Gelimiteerd',
        self::UNLIMITED => 'Ongelimiteerd',
    ];
}