<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class CompanyInvoiceFrequencyType
 */
final class CompanyInvoiceFrequencyType extends AbstractEnumType
{
    public const INVOICE_FREQUENCY_COLLECTIVE = 'collective';
    public const INVOICE_FREQUENCY_ORDER = 'order';
    public const INVOICE_FREQUENCY_ORDER_COLLECTION = 'order_collection';
    public const INVOICE_FREQUENCY_WEEKLY = 'weekly';
    public const INVOICE_FREQUENCY_BIWEEKLY = 'biweekly';
    public const INVOICE_FREQUENCY_MONTHLY = 'monthly';
    public const INVOICE_FREQUENCY_QUARTERLY = 'quarterly';

    protected $name = 'company_invoice_frequency_type';

    protected static $choices = [
        self::INVOICE_FREQUENCY_COLLECTIVE => 'Verzamelfactuur',
        self::INVOICE_FREQUENCY_ORDER => 'Per levering',
        self::INVOICE_FREQUENCY_ORDER_COLLECTION => 'Per gehele bestelling',
        self::INVOICE_FREQUENCY_WEEKLY => 'Per week',
        self::INVOICE_FREQUENCY_BIWEEKLY => 'Per 14 dagen',
        self::INVOICE_FREQUENCY_MONTHLY => 'Per maand',
        self::INVOICE_FREQUENCY_QUARTERLY => 'Per kwartaal',
    ];
}