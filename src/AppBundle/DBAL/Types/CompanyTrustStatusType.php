<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class CompanyTrustStatusType
 * @package AppBundle\DBAL\Types
 */
final class CompanyTrustStatusType extends AbstractEnumType
{
    public const TRUST_STATUS_NORMAL = 'normal';
    public const TRUST_STATUS_WHITELIST = 'whitelist';
    public const TRUST_STATUS_BLACKLIST = 'blacklist';

    protected static $choices = [
        self::TRUST_STATUS_NORMAL => 'Normaal',
        self::TRUST_STATUS_WHITELIST => 'Whitelist',
        self::TRUST_STATUS_BLACKLIST => 'Blacklist',
    ];
}