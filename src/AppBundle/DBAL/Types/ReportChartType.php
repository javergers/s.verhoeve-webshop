<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class ReportChartType
 * @package AppBundle\DBAL\Types
 */
final class ReportChartType extends AbstractEnumType
{
    public const BAR = 'bar';
    public const LINE = 'line';
    public const PIE = 'pie';

    protected static $choices = [
        self::BAR => 'Bar',
        self::LINE => 'Line',
        self::PIE => 'Pie',
    ];
}