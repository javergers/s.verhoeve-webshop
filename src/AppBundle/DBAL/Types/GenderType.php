<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class GenderType
 * @package AppBundle\DBAL\Types
 */
final class GenderType extends AbstractEnumType
{
    public const MALE = 'male';
    public const FEMALE = 'female';

    protected static $choices = [
        self::MALE => 'label.male',
        self::FEMALE => 'label.female',
    ];
}