<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class ProductTypeType
 * @package AppBundle\DBAL\Types
 */
final class ProductTypeType extends AbstractEnumType
{
    public const PRODUCT_TYPE_VARIATION = 'variable';
    public const PRODUCT_TYPE_COMBINATION = 'combination';

    protected static $choices = [
        self::PRODUCT_TYPE_VARIATION => 'Product',
        self::PRODUCT_TYPE_COMBINATION => 'Gecombineerd product',
    ];
}
