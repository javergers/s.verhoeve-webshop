<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;
use Translation\Common\Model\Message;

/**
 * Class SalutationType
 * @package AppBundle\DBAL\Types
 */
final class SalutationType extends AbstractEnumType
{
    public const MALE = 'male';
    public const FEMALE = 'female';

    protected static $choices = [
        self::FEMALE => 'label.salutation.female',
        self::MALE => 'label.salutation.male',
    ];

    /**
     * @return array
     */
    public static function getTranslationMessages()
    {
        $messages = [];

        foreach (static::$choices as $choice) {
            $messages[] = new Message($choice);
        }

        return $messages;
    }
}
