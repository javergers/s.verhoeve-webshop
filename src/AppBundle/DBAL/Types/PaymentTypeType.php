<?php

namespace AppBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

/**
 * Class PaymentTypeType
 * @package AppBundle\DBAL\Types
 */
final class PaymentTypeType extends AbstractEnumType
{
    public const REFUND = 'refund';
    public const CANCEL_OR_REFUND = 'refundOrCancel';
    public const CANCEL = 'cancel';

    protected static $choices = [
        self::REFUND => 'Terugbetaald',
        self::CANCEL_OR_REFUND => 'Geannuleerd or terugbetaald',
        self::CANCEL => 'Geannuleerd',
    ];
}