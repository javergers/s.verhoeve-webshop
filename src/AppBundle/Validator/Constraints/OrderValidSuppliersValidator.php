<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Services\SupplierManagerService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class OrderValidSuppliersValidator
 */
class OrderValidSuppliersValidator extends ConstraintValidator
{
    /**
     * @var SupplierManagerService
     */
    private $supplierManagerService;

    /**
     * OrderValidSuppliersValidator constructor.
     *
     * @param SupplierManagerService $supplierManagerService
     */
    public function __construct(SupplierManagerService $supplierManagerService)
    {
        $this->supplierManagerService = $supplierManagerService;
    }

    /**
     * Checks if the products within the order can be matched to the same supplier.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($value, Constraint $constraint)
    {
        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        if(!$this->supplierManagerService->hasAvailableSuppliersForOrder($value)) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}