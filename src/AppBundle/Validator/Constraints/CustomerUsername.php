<?php

namespace AppBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Translation\Common\Model\Message;
use Symfony\Component\Validator\Constraint;

/**
 * Class CustomerUsername
 *
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 */
class CustomerUsername extends Constraint
{

    public $message = 'username.not_unique';
    /**
     * @var EntityManagerInterface $entityManager
     */
    protected $entityManager;

    /**
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        //parent::__construct($entityManager);
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    /**
     * @return array
     */
    public static function getTranslationMessages()
    {
        $messages = [];

        $messages[] = new Message('username.not_unique', 'validators');

        return $messages;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'customer_username';
    }
}
