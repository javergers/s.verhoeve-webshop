<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class InvalidLineCombination
 * @Annotation
 */
class InvalidLineCombination extends Constraint
{
    public $invalidPersonalization = 'error.orderline.invalidPersonalization';
    public $invalidCombination = 'error.orderline.invalidCombination';

    /**
     * @return array|string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}