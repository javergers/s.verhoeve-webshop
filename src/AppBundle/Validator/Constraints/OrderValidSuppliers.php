<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class OrderValidSuppliers
 * @Annotation
 */
class OrderValidSuppliers extends Constraint
{
    /**
     * @var string $message
     */
    public $message = 'error.order.invalid_suppliers';

    /**
     * @return array|string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}