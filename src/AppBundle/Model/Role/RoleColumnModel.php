<?php

namespace AppBundle\Model\Role;

use AppBundle\Entity\Security\Employee\Role;
use AppBundle\Model\BaseModel;

/**
 * Class RoleColumnModel
 * @package AppBundle\Model\Role
 */
class RoleColumnModel extends BaseModel
{

    /**
     * @return Role|bool
     */
    public function getReadRole()
    {
        foreach ($this->getRoles() as $role) {
            if (strpos($role->getName(), 'READ')) {
                return $role;
            }
        }

        return false;
    }

    /**
     * @return Role|bool
     */
    public function getWriteRole()
    {
        foreach ($this->getRoles() as $role) {
            if (strpos($role->getName(), 'WRITE')) {
                return $role;
            }
        }

        return false;
    }

}