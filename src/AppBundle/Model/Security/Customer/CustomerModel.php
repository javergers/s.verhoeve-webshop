<?php

namespace AppBundle\Model\Security\Customer;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Model\BaseModel;
use Doctrine\ORM\QueryBuilder;

/**
 * Class CustomerModel
 * @package AppBundle\Model\Security\Customer
 */
class CustomerModel extends BaseModel {

    /**
     * @param int $limit
     * @return array
     */
    public function getRecentOrders($limit = 5)
    {
        /**
         * @var $qb QueryBuilder
         */
        $qb = $this->container->get('doctrine')->getManager()->getRepository(Order::class)->createQueryBuilder('o');
        $qb->leftJoin('o.orderCollection', 'oc');
        $qb->andWhere('oc.customer = :customer');
        $qb->orderBy('o.createdAt', 'DESC');
        $qb->setMaxResults($limit);
        $qb->setParameter('customer', $this);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return float|int
     */
    public function getTotalOrdersRevenue()
    {
        $total = 0;
        /**
         * @var Order $order
         */
        foreach ($this->getOrders()->getIterator() as $order) {
            $total += $order->getTotalPriceIncl();
        }

        return $total;
    }

    /**
     * @return int
     */
    public function getTotalOrders()
    {
        return $this->getOrders()->count();
    }

}