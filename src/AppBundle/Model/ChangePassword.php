<?php

namespace AppBundle\Model;

use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ChangePassword
 * @package AppBundle\Model
 *
 * @todo    : improve empty password post
 */
class ChangePassword
{
    /**
     * @SecurityAssert\UserPassword(
     *     message = "customer.account.change_password.current.invalid"
     * )
     */
    protected $current;

    /**
     * @Assert\NotBlank(
     *     message = "customer.account.change_password.new.not_blank"
     * )
     */
    protected $new;

    public function setCurrent($current)
    {
        $this->current = $current;

        return $this;
    }

    public function getCurrent()
    {
        return $this->current;
    }

    public function setNew($new)
    {
        $this->new = $new;

        return $this;
    }

    public function getNew()
    {
        return $this->new;
    }
}
