<?php

namespace AppBundle\Model\Product;

use AppBundle\DBAL\Types\ProductTypeType;
use AppBundle\Entity\Catalog\Product\Combination;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\Letter;
use AppBundle\Entity\Catalog\Product\Personalization;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Catalog\Product\ProductgroupProperty;
use AppBundle\Entity\Catalog\Product\ProductgroupPropertyOption;
use AppBundle\Entity\Catalog\Product\ProductgroupPropertySet;
use AppBundle\Entity\Catalog\Product\ProductImage;
use AppBundle\Entity\Catalog\Product\ProductProperty;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Site\Site;
use AppBundle\Manager\StockManager;
use AppBundle\Model\BaseModel;
use AppBundle\Services\SiteService;
use AppBundle\Utils\Designer\DesignDimensions;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\PersistentCollection;
use Exception;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use AppBundle\Services\SupplierManagerService;

/**
 * Class ProductModel
 * @package AppBundle\Model\Product
 */
class ProductModel extends BaseModel
{
    /**
     * @var boolean
     */
    protected $purchasable = true;

    /**
     * Get purchasable
     *
     * @return boolean
     */
    public function getPurchasable()
    {
        return $this->purchasable;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->translate()->getTitle();
    }

    /**
     * @return PersistentCollection|ProductImage[]|null
     */
    public function getImages()
    {
        // Determine the collection of images based on the presents of a parent product
        if ($this->getParent()) {
            $images = $this->getParent()->getProductImages();
        } else {
            $images = $this->getProductImages();
        }

        return $images;
    }

    /**
     * @param $fallback
     *
     * @return Product|ProductImage|ProductModel|null
     */
    public function getMainImage($fallback = false)
    {
        /** @var Collection|ProductImage $images */
        $images = $this->getProductImages();

        // Filter the image collection for a main image
        $mainImage = $images->filter(function (ProductImage $entry) {
            return ($entry->getMain() || $entry->getCategory());
        });

        // Return the main image
        if (!$mainImage->isEmpty()) {
            return $mainImage->current();
        }

        // If there's no main image and the fallback is set to true, return the first available image
        if ($fallback && !$mainImage->current() && !$images->isEmpty()) {
            return $images->current();
        }

        //if there is no main image and the product is a variation, select the parent's main image
        if ($mainImage->isEmpty() && $this->getParent()) {
            $images = $this->getParent()->getProductImages();
            $parentImage = $images->filter(function (ProductImage $entry) {
                return $entry->getMain();
            });

            if (!$parentImage->isEmpty()) {
                return $parentImage->current();
            }
        }

        // Try to find an image from the first variantion that got an image
        if (!$this->getVariations()->isEmpty()) {
            foreach ($this->getVariations() as $variation) {
                $images = $variation->getProductImages();
                $variantImage = $images->filter(function (ProductImage $entry) {
                    return $entry->getMain();
                });

                if (!$variantImage->isEmpty()) {
                    return $variantImage->current();
                }
            }
        }

        // If combination (variation) has not own images display main product image.
        if ($this->isCombination()
            && null !== $this->getMainCombination()
            && null !== $this->getMainCombination()->getProduct()
            && null !== $this->getMainCombination()->getProduct()->getMainImage()
        ) {
            return $this->getMainCombination()->getProduct()->getMainImage();
        }

        if (\method_exists($this, 'getPath') && !empty($this->getPath())) {
            return $this;
        }

        // If there's no main image and the fallback is set to false, return null
        return null;
    }

    /**
     * @return null
     */
    public function getUntaggedImages()
    {
        /** @var Collection|ProductImage $images */
        $images = $this->getProductImages();

        // Filter the image collection for all untagged images
        $untaggedImages = $images->filter(function (ProductImage $entry) {
            // If there's no variation, return the untagged images
            return (!$entry->getMain() && !$entry->getCategory());
        });

        if ($untaggedImages->isEmpty()) {
            return null;
        }

        return $untaggedImages;
    }

    /**
     * @param bool $fallback
     * @return ProductImage|null
     */
    public function getCategoryImage($fallback = false)
    {
        /** @var Collection|ProductImage $images */
        $images = $this->getProductImages();

        // Filter the image collection for a category image
        $categoryImage = $images->filter(function (ProductImage $entry) {
            return $entry->getCategory();
        });

        // Return the category image
        if (!$categoryImage->isEmpty()) {
            return $categoryImage->current();
        }

        // If the fallback is set to true, return the main image of the product
        if ($fallback) {
            return $this->getMainImage(true);
        }

        // If there's no category image and the fallback is set to false return null
        return null;
    }

    /**
     * @param null      $language
     * @param Site|null $site
     * @param null      $referenceType
     * @return null|string
     */
    public function getUrl($language = null, Site $site = null, $referenceType = null)
    {
        $product = $this->getParent() ?? $this;

        return $this->container->get('app.slug')->getUrl($product, $language, $site, $referenceType);
    }

    /**
     * @param null      $language
     * @param Site|null $site
     * @return null|string
     */
    public function getAbsoluteUrl($language = null, Site $site = null)
    {
        return $this->container->get('app.slug')->getUrl($this, $language, $site,
            UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * @deprecated Use PriceService to calculate price incl VAT and/or format prices
     *
     * @param bool $excludeVariations
     * @return string
     */
    public function getDisplayPrice($excludeVariations = false)
    {
        /** depens on site and user settings */
        $site = $this->container->get(SiteService::class)->determineSite();

        $siteDefaultDisplayPrice = $site ? $site->getDefaultDisplayPrice() : 'inclusive';
        if ($siteDefaultDisplayPrice === 'exclusive') {
            $price = $this->calculatePrice($excludeVariations);
        } else {
            $price = $this->getPriceIncl($excludeVariations);
        }

        //check if number is already formatted
        if (!is_numeric($price)) {
            return $price;
        }

        return number_format($price, 2, ',', '.');
    }

    /**
     * @param bool $excludeVariations
     * @return float
     */
    public function calculatePrice($excludeVariations = false): float
    {
        if (!$excludeVariations && !$this->getVariations()->isEmpty()) {
            $prices = [];

            foreach ($this->getVariations() as $variation) {
                $prices[] = $variation->getPrice();
            }

            if (!$prices) {
                return 0.000;
            }

            return min($prices);
        }

        if ($this->getPrice()) {
            return $this->getPrice();
        }

        if ($this instanceof CompanyProduct && $this->getRelatedProduct()) {
            return $this->getRelatedProduct()->calculatePrice();
        }

        return 0.000;
    }

    /**
     * @deprecated Use PriceService to calculate price incl VAT and/or format prices
     *
     * @param bool $excludeVariations
     * @return string
     */
    public function getPriceIncl($excludeVariations = false)
    {
        if (null !== $this->getParent() && null === $this->getVat()) {
            $percentage = $this->getParent()->getVat()->getPercentage();
        } else {
            $percentage = $this->getVat() ? $this->getVat()->getPercentage() : null;
        }

        return number_format($this->calculatePrice($excludeVariations) * (1 + ($percentage / 100)), 2, ',', '.');
    }

    /**
     * @deprecated Use PriceService to calculate price incl VAT and/or format prices
     *
     * @return string
     */
    public function getMinPriceIncl()
    {
        $percentage = $this->getVat()->getPercentage();

        return number_format($this->getMinPrice() * (1 + ($percentage / 100)), 2, ',', '.');
    }

    /**
     * @deprecated Use PriceService to calculate price incl VAT and/or format prices
     *
     * @return string
     */
    public function getDisplayMinPrice()
    {
        /** depens on site and user settings */
        return number_format($this->getMinPriceIncl(), 2, ',', '.');
    }

    /**
     * @deprecated Use PriceService to calculate price incl VAT and/or format prices
     *
     * @return string
     */
    public function getMaxPriceIncl()
    {
        $percentage = $this->getVat()->getPercentage();

        return number_format($this->getMaxPrice() * (1 + ($percentage / 100)), 2);
    }

    /**
     * @deprecated Use PriceService to calculate price incl VAT and/or format prices
     *
     * @return string
     */
    public function getDisplayMaxPrice()
    {
        /** depens on site and user settings */
        return number_format($this->getMaxPriceIncl(), 2, ',', '.');
    }

    /**
     * @deprecated Use PriceService to calculate price incl VAT and/or format prices
     *
     * @return string
     */
    public function getStepMinMaxPriceIncl()
    {
        if (!$this->getStepMinMaxPrice()) {
            return null;
        }

        $percentage = $this->getVat()->getPercentage();

        return number_format($this->getStepMinMaxPrice() * (1 + ($percentage / 100)), 2);
    }

    /**
     * @deprecated Use PriceService to calculate price incl VAT and/or format prices
     *
     * @return null|string
     */
    public function getDisplayStepMinMaxPrice()
    {
        if (!$this->getStepMinMaxPrice()) {
            return null;
        }

        /** depens on site and user settings */
        return number_format($this->getStepMinMaxPriceIncl(), 2, ',', '.');
    }

    /**
     * @param $price
     * @return array
     */
    public function checkMinMaxPrice($price)
    {
        $errors = [];

        /** depens on site and user settings */
        $minPrice = $this->getMinPriceIncl();
        $maxPrice = $this->getMaxPriceIncl();
        $step = $this->getStepMinMaxPriceIncl();

        if ($price < $minPrice || $price > $maxPrice) {
            $errors[] = 'min_max';
        }

        $priceInt = number_format($price, 2, '', '');

        if ($step) {
            $stepInt = number_format($step, 2, '', '');

            if ($priceInt % $stepInt !== 0) {
                $errors[] = 'step';
            }
        }

        return $errors;
    }

    /**
     * @return null
     */
    public function generateUrl()
    {
        if (null === $this->getMainImage()) {
            return null;
        }

        /** @var CacheManager */
        $imagineCacheManager = $this->container->get('liip_imagine.cache.manager');
        $image = $imagineCacheManager->getBrowserPath($this->getMainImage()->getPath(), 'product_assortment');

        if (!$image) {
            return null;
        }

        return $image;
    }

    /**
     * @return bool
     */
    public function hasVariations()
    {
        $minimumProducts = 1;

        //check if bigger then one, if it's one it means it's not an actual variation
        return $this->getVariations()->filter(function (Product $variation) {
                return $variation->isPublished();
            })->count() > $minimumProducts;
    }

    /**
     * @param string $key
     *
     * @return ProductgroupProperty|null
     */
    public function getProductgroupProperty($key = ''): ?ProductgroupProperty
    {
        /** @var ProductgroupPropertySet $propertySet */
        $propertySet = $this->getPropertySet();

        if ($propertySet) {
            foreach ($propertySet->getProductgroupProperties() as $groupProperty) {
                /** @var ProductgroupProperty $groupProperty */
                if (strpos($groupProperty->getKey(), $key) === 0) {
                    return $groupProperty;
                }
            }
        }

        return null;
    }

    /**
     * @return bool
     */
    public function hasSupplier(): bool
    {
        /** @var Product $product */
        $product = $this;

        $supplierManager = $this->container->get(SupplierManagerService::class);

        return false === $supplierManager->getAvailableSuppliersForProduct($product)->isEmpty();
    }

    /**
     * @return bool
     *
     * @throws DBALException
     * @throws InvalidArgumentException
     * @throws OptimisticLockException
     * @deprecated use Stockmanager instead
     */
    public function isOrderable(): bool
    {
        /** @var Product $product */
        $product = $this;

        $stockManager = $this->container->get(StockManager::class);

        return $stockManager->isOrderable($product);
    }

    /**
     * @return bool
     */
    public function isProductAdditionalProductsAvailable()
    {
        return !$this->getAdditionalProductAssortments()->isEmpty();
    }

    /**
     * Check if product variation has a personalisation
     *
     * @return bool
     * @throws Exception
     */
    public function isProductPersonalizationAvailable()
    {
        if(isset($this->getMetadata()['design'])) {
            return true;
        }

        foreach($this->getVariations() as $variation) {
            if(isset($variation->getMetadata()['design'])) {
                return true;
            }
        }

        if ($this->isCombination() && $this->hasPersonalization()) {
            return true;
        }

        if (!$this->getProductgroupProperty('personalization')) {
            return false;
        }

        if ($this->getVariations()->isEmpty()) {
            try {
                return (($personalization = $this->findProductProperty('personalization')) && $personalization->getProductVariation());
            } catch (\Exception $e) {
                return false;
            }
        } else {
            foreach ($this->getVariations() as $variation) {
                try {
                    if (!(bool)$variation->findProductProperty('personalization')) {
                        continue;
                    }
                } catch (\Exception $e) {
                    continue;
                }

                if ($variation->findProductProperty('personalization')->getProductVariation()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isProductPersonalizationRequired(): bool
    {
        $isRequired = false;

        if (false === $this->isCombination()) {
            $productProperty = $this->findProductProperty('personalization');
            $personalization = $productProperty->getProductVariation();

            $isRequired = $personalization->getPrice() === 0.0;
        } elseif (!empty($this->getMetadata()['design'])) {
            $isRequired = true;
        }

        return $isRequired;
    }

    /**
     * @param $key
     * @return bool
     */
    public function hasProductProperty($key)
    {
        try {
            $this->findProductProperty($key);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $key
     * @return ProductProperty
     * @throws \RuntimeException
     */
    public function findProductProperty($key)
    {
        /** @var Product $this */
        $productProperty = $this->getProductProperties()->filter(function (ProductProperty $entry) use ($key) {
            return strpos($entry->getProductgroupProperty()->getKey(), $key) === 0;
        });

        if ($productProperty->isEmpty()) {
            throw new \RuntimeException(sprintf('Could not find product property with key "%s"', $key));
        }

        return $productProperty->current();
    }

    /**
     * @param string $key
     * @return ProductgroupPropertyOption|null
     */
    public function getProductPropertyOption(string $key = ''): ?ProductgroupPropertyOption
    {
        try {
            return $this->findProductProperty($key)->getProductgroupPropertyOption();
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getFilterableProductProperties()
    {
        /**
         * @var Product $variation
         */
        $productPropertiesArray = [];
        foreach ($this->getVariations() as $variation) {
            foreach ($variation->getProductProperties()->toArray() as $key => $value) {
                $productPropertiesArray[$value->getId()] = $value;
            }
        }

        $productProperties = new ArrayCollection(
            array_merge($this->getProductProperties()->toArray(), $productPropertiesArray)
        );

        return $productProperties;
    }

    /**
     * @return array
     * @throws DBALException
     * @throws OptimisticLockException
     * @throws InvalidArgumentException
     */
    public function getOrderableQuantities()
    {
        $minQuantity = 1;
        $maxQuantity = 99;
        $quantitySteps = 1;

        if ($this->getMinQuantity()) {
            $minQuantity = $this->getMinQuantity();
        }

        if ($this->getMaxQuantity()) {
            $maxQuantity = $this->getMaxQuantity();
        }

        $mainProduct = ($this->getParent() ?: $this);

        if ($mainProduct->getHasStock() && !$mainProduct->getNoStockBackorder()) {
            if ($this->getType() === ProductTypeType::PRODUCT_TYPE_VARIATION && $this->hasVariations()) {
                return [];
            }

            $virtualStock = $this->container->get(StockManager::class)->getVirtualStock($this);

            if ($virtualStock < $maxQuantity) {
                $maxQuantity = $virtualStock;
            }
        }

        if ($maxQuantity !== null && $minQuantity > $maxQuantity) {
            return [];
        }

        if ($this->getQuantitySteps()) {
            $quantitySteps = $this->getQuantitySteps();
        }

        if (strpos($this->getQuantitySteps(), ';') !== false) {
            $range = explode(';', $this->getQuantitySteps());

            $range = array_filter($range, function ($value) use ($minQuantity, $maxQuantity) {
                return $value >= $minQuantity && $value <= $maxQuantity;
            });
        } else {
            if (($minQuantity + $quantitySteps) > $maxQuantity) {
                $maxQuantity = $minQuantity;
            }

            $range = range($minQuantity, $maxQuantity, $quantitySteps);
        }

        return array_combine($range, $range);
    }

    /**
     * @return integer
     * @throws OptimisticLockException
     */
    public function getPhysicalStock()
    {
        $stockManager = $this->container->get(StockManager::class);

        return $stockManager->getPhysicalStock($this);
    }

    /**
     * @return integer
     *
     * @throws DBALException
     * @throws InvalidArgumentException
     * @throws OptimisticLockException
     */
    public function getVirtualStock()
    {
        $stockManager = $this->container->get(StockManager::class);

        return $stockManager->getVirtualStock($this);
    }

    /**
     * @return integer
     * @throws OptimisticLockException
     */
    public function getStockThreshold()
    {
        $stockManager = $this->container->get(StockManager::class);

        return $stockManager->getThreshold($this);
    }

    /**
     * @return string
     * @throws OptimisticLockException
     */
    public function getStockStatus()
    {
        $stockManager = $this->container->get(StockManager::class);

        return $stockManager->getStockStatus($this);
    }

    /**
     * @return bool
     */
    public function isCombination()
    {
        if (null === $this->getCombinations()) {
            return false;
        }

        return !$this->getCombinations()->isEmpty();
    }

    /**
     * @return Combination|null
     */
    public function getMainCombination(): ?Combination
    {
        /** @var Combination[]|Collection $combinations */
        $combinations = $this->getCombinations();

        $mainCombinations = $combinations->filter(function (Combination $combination) {
            return $combination->getMain();
        });

        return $mainCombinations->current() ?: null;
    }

    /**
     * @return bool
     */
    public function isTransportType()
    {
        if ($this instanceof TransportType) {
            return true;
        }

        return false;
    }

    /**
     * Check if a product is a personalization type.
     *
     * @return bool
     */
    public function isPersonalization()
    {
        return ($this instanceof Personalization);
    }

    /**
     * Check if a product is a personalization type.
     *
     * @return bool
     */
    public function isLetter()
    {
        return ($this instanceof Letter);
    }

    /**
     * Check if a product is a card type.
     *
     * @return bool
     */
    public function isCard()
    {
        return ($this instanceof ProductCard);
    }

    /**
     * Has combination a personalization product
     *
     * @return bool
     */
    public function hasPersonalization()
    {
        if (false === $this->isCombination()) {
            return false;
        }

        if (null !== ($personalizations = $this->getPersonalizations()) && !$personalizations->isEmpty()) {
            return true;
        }

        return false;
    }

    /**
     * Get personalizations of product combinations
     *
     * @return ArrayCollection
     */
    public function getPersonalizations(): ArrayCollection
    {
        if($this instanceof CompanyProduct) {
            $product = $this->getRelatedProduct();
        } else {
            $product = $this;
        }

        /** @var Product $this */
        $combinationProducts = $product->getCombinations();

        $personalization = new ArrayCollection();

        if (!$product->isCombination()) {
            return $personalization;
        }

        foreach ($combinationProducts as $combinationProduct) {

            /** @var Combination $combinationProduct */
            if (null === $combinationProduct->getProduct()) {
                continue;
            }

            if ($combinationProduct->getProduct()->isPersonalization()) {
                $personalization->add($combinationProduct->getProduct());
            }
        }

        return $personalization;
    }

    /**
     * @return DesignDimensions
     * @throws Exception
     */
    public function getDesignDimensions()
    {
        $width = (float)$this->findProductProperty('design.width')->getValue();
        $height = (float)$this->findProductProperty('design.height')->getValue();

        return new DesignDimensions($width, $height);
    }

    /**
     * @return Product|ProductModel
     */
    public function getCheapestVariation()
    {
        $sortedVariations = $this->getVariationsSortedByPrice();

        if (null !== $sortedVariations) {
            return $sortedVariations->current();
        }

        return $this;
    }

    /**
     * @return null|ArrayCollection
     */
    public function getVariationsSortedByPrice(): ?ArrayCollection
    {
        /** @var Collection|Product[] $variations */
        $variations = $this->getVariations();
        if (!$variations->isEmpty()) {
            $variations->filter(function (Product $product) {
                $hasPrice = null !== $product->getPrice();

                if(true === $hasPrice){
                    return true;
                }

                if($product instanceof CompanyProduct){
                    $product = $product->getRelatedProduct();
                }

                return null !== $product->getPrice();
            });

            $iterator = $variations->getIterator();

            $iterator->uasort(function (Product $a, Product $b) {
                $priceA = $a->getPrice();
                if(null === $priceA && $a instanceof CompanyProduct){
                    $priceA = $a->getRelatedProduct()->getPrice();
                }

                $priceB = $b->getPrice();
                if(null === $priceB && $b instanceof CompanyProduct){
                    $priceB = $b->getRelatedProduct()->getPrice();
                }

                return $priceA <=> $priceB;
            });

            return new ArrayCollection(iterator_to_array($iterator));
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isConfigurable()
    {
        if (null !== $this->getConfigurable()) {
            return $this->isCombination() && $this->getConfigurable();
        }

        if (null !== $this->getParent()) {
            return $this->getParent()->isConfigurable();
        }

        return false;
    }

    /**
     * @return array
     */
    public function getConfigurations()
    {
        /** @var Combination[] $combinations */
        $combinations = $this->getCombinations();

        $configurations = [];
        $inputs = [];
        foreach ($combinations as $combination) {
            $combinationProduct = $combination->getProduct();

            //get all variations from combination
            /** @var Product[] $combinationVariations */
            $combinationVariations = $combination->getCombinationProduct()->getVariations();
            $productId = $combinationProduct->getId();

            //get product in combination and check if its the main product
            $inputs[$productId]['product'] = $combinationProduct;
            $inputs[$productId]['isMain'] = $combination->getMain();

            //loop through all variations
            foreach ($combinationVariations as $variation) {
                /** @var Combination $combi */
                //get combination of variations
                foreach ($variation->getCombinations() as $combi) {
                    $product = $combi->getProduct();

                    $productId = $product->getId();
                    $combinationProductId = $combi->getCombinationProduct()->getId();

                    //set all possible configurations
                    $configurations[$combinationProductId][] = $productId;
                    $configurations[$combinationProductId] = array_unique($configurations[$combinationProductId]);

                    if (null !== ($productParent = $product->getParent())) {
                        //create readable inputs for front end
                        $inputs[$productParent->getId()]['options'][$productId] = $product;
                    }
                }
            }
        }

        return [
            'inputs' => $inputs,
            'configurations' => json_encode($configurations, true),
        ];

    }

    /**
     * @return bool
     */
    public function isRequiredPersonalization()
    {
        return (float)$this->getPrice() === 0.0 && $this->isPersonalization();
    }

    /**
     * @return bool
     */
    public function hasRequiredPersonalization()
    {
        return $this->isCombination() && $this->hasPersonalization();
    }

    /**
     * @return Product|null
     */
    public function getDefaultVariation(): ?Product
    {
        /** @var Product $product */
        $product = $this;

        if (false !== $product->getVariations()->isEmpty()) {
            return null;
        }

        $filteredVariations = $product->getVariations()->filter(function (Product $variation) {
            if (isset($variation->getMetadata()['default_variation'])) {
                return (bool)$variation->getMetadata()['default_variation'];
            }

            return false;
        });

        if (false === $filteredVariations->current()) {
            return $product->getVariations()->current();
        }

        return $filteredVariations->current();
    }

    /**
     * @return bool
     */
    public function isCompanyProduct()
    {
        return $this instanceof CompanyProduct;
    }
}
