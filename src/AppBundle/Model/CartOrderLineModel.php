<?php

namespace AppBundle\Model;

use AppBundle\Entity\Catalog\Product\Combination;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Vat;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Services\Designer;
use Doctrine\Common\Collections\ArrayCollection;
use Exception;
use League\Flysystem\FileNotFoundException;

/**
 * Class CartOrderLineModel
 * @package AppBundle\Model
 */
class CartOrderLineModel extends BaseModel
{
    /**
     * @return string
     */
    public function getDisplayPrice()
    {
        $defaultDisplayPrice = $this->getCartOrder()->getCart()->getSite()->getDefaultDisplayPrice();

        /** @var Company $company */
        $company = $this->getCartOrder()->getCart()->getCompany();

        $withDiscount = false;
        if (null !== $company && $company->getPriceDisplayMode() !== 'original' && $this->getDiscountType() === 'assortment') {
            $withDiscount = true;
        }

        if ($defaultDisplayPrice === 'exclusive') {
            $price = $this->getPrice();

            if ($withDiscount) {
                $price -= ($this->getDiscountAmountExcl() / $this->getQuantity());
            }
        } else {
            $price = $this->getPriceIncl();

            if ($withDiscount) {
                $price -= ($this->getDiscountAmountIncl() / $this->getQuantity());
            }
        }

        return number_format($price, 2, ',', '.');
    }

    /**
     *
     * @return string
     */
    public function getTotalDisplayPrice()
    {
        $defaultDisplayPrice = $this->getCartOrder()->getCart()->getSite()->getDefaultDisplayPrice();

        if ($defaultDisplayPrice === 'exclusive') {
            return number_format($this->getPrice() * $this->getQuantity(), 2, ',', '.');
        }

        return number_format($this->getPriceIncl() * $this->getQuantity(), 2, ',', '.');
    }

    /**
     * @return float
     */
    public function getDisplayDiscountAmount()
    {
        $percentage = $this->getVat()->getPercentage();
        $vat = $this->getDiscountAmount() / (100 + $percentage) * $percentage;

        return number_format($this->getDiscountAmount() - $vat, 2, ',', '.');
    }

    /**
     * @return float
     */
    public function getDisplayDiscountAmountIncl()
    {
        return number_format($this->getDiscountAmount(), 2, ',', '.');
    }

    /**
     * @return float
     */
    public function getDiscountAmountExcl()
    {
        if ($this->getDiscountAmount()) {
            return ($this->getDiscountAmount() / ($this->getVat()->getPercentage() + 100) * 100);
        }

        return $this->getDiscountAmount();
    }

    /**
     * @param $quantity integer
     *
     * @return int
     * @throws Exception
     */
    public function setQuantity($quantity)
    {
        if (null == $this->container) {
            return $quantity;
        }

        $em = $this->container->get('doctrine')->getManager();

        if ($quantity === 0) {
            $this->getCartOrder()->removeLine($this);
            $em->remove($this);

            if ($this->getChildren()) {
                foreach ($this->getChildren() as $child) {
                    $child->setQuantity($quantity);
                }
            }
        } else {
            /** @var Product $product */
            $product = $this->getProduct();

            $orderableQuantities = $this->getOrderableQuantities();

            if ($orderableQuantities && !\in_array($quantity, $orderableQuantities, true)) {
                throw new \RuntimeException(sprintf("Quantity %d is not valid for product '%s', valid quantities are %s",
                    $quantity, $product->translate()->getTitle(), implode(', ', $orderableQuantities)));
            }

            foreach ($this->getChildren() as $child) {
                $metadata = $child->getMetadata();

                // Cards can have different quantities when the parent product has a minimum order quantity greater then 1
                if (isset($metadata['is_card']) && $metadata['is_card'] === true && $product->getMinQuantity() > 1) {
                    continue;
                }

                if(isset($metadata['upsell'])) {
                    continue;
                }

                $child->setQuantity($quantity);
            }
        }

        $em->flush();
    }

    /**
     * Checks whether this line has cards for the product
     *
     * @return boolean
     */
    public function hasProductCard()
    {
        $hasProductPersonalization = $this->getChildren()->filter(function (CartOrderLine $cartOrderLine) {
            /** @var $cartOrderLine CartOrderLine */
            return ($cartOrderLine->getMetadata() && !empty($cartOrderLine->getMetadata()['is_card']));
        });

        return !$hasProductPersonalization->isEmpty();
    }

    /**
     * Checks whether this line has cards for the product
     *
     * @return boolean
     */
    public function getProductCard()
    {
        $productCard = $this->getChildren()->filter(function (CartOrderLine $cartOrderLine) {
            /** @var $cartOrderLine CartOrderLine */
            return ($cartOrderLine->getMetadata() && !empty($cartOrderLine->getMetadata()['is_card']));
        });

        if ($productCard) {
            $productCard = $productCard->first();
        }

        return $productCard;
    }

    /**
     * Checks whether this line has personalization for the product
     *
     * @deprecated Moved to OrderLineManager
     *
     * @return boolean
     */
    public function hasProductPersonalization()
    {
        return (bool)$this->getPersonalizationLine();
    }

    /**
     * Get personalization child line
     *
     * @deprecated Moved to OrderLineManager
     *
     * @return CartOrderLine|null
     */
    public function getPersonalizationLine()
    {
        if ($this->getMetadata() && !empty($this->getMetadata()['designer'])) {
            return $this;
        }

        /** @var ArrayCollection $productPersonalizationLine */
        $productPersonalizationLine = $this->getChildren()->filter(function (CartOrderLine $cartOrderLine) {
            /** @var $cartOrderLine CartOrderLine */
            return ($cartOrderLine->getMetadata() && !empty($cartOrderLine->getMetadata()['designer']));
        });

        if (!$productPersonalizationLine->isEmpty()) {
            return $productPersonalizationLine->first();
        }

        return null;
    }

    /**
     * Get the preview image for the designer
     *
     * @return bool|null|string
     * @deprecated
     * @throws FileNotFoundException
     */
    public function getPersonalizationPreviewImage()
    {
        $metadata = $this->getMetadata();

        if (!isset($metadata['designer'])) {
            return false;
        }

        $designer = $this->container->get('app.designer');
        $designer->setUuid($metadata['designer']);

        return $designer->getFileUrl(Designer::FILE_PREVIEW, 60, Designer::SOURCE_LOCAL);
    }

    /**
     * @return CartOrderLine
     */
    public function getProductLetterLine()
    {
        /** @var CartOrderLine $cartOrderLine */
        foreach ($this->getCartOrder()->getLines() as $cartOrderLine) {
            $metaData = $cartOrderLine->getMetadata();

            if (null !== $metaData && !empty($metaData['letter'])) {
                return $cartOrderLine;
            }
        }

        return null;
    }

    /**
     * @return bool
     */
    public function getProductLetterDownload()
    {
        $metaData = $this->getMetadata();
        if (null !== $metaData && !empty($metaData['letter'])) {
            $letter = $this->container->get('app.letter');
            $letter->setUuid($metaData['letter']['uuid']);

            return $letter->getUrl(60);
        }

        return null;
    }

    /**
     * @return bool
     */
    public function hasProductLetter()
    {
        /** @var CartOrderLine $cartOrderLine */
        foreach ($this->getCartOrder()->getLines() as $cartOrderLine) {
            $metaData = $cartOrderLine->getMetadata();

            if (null !== $metaData && !empty($metaData['letter'])) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isLetterAvailable()
    {
        return $this->getProduct()->isProductLetterAvailable();
    }

    /**
     * Checks whether this line has additional products for the product
     *
     * @return boolean
     */
    public function hasProductAdditionalProducts()
    {
        $hasProductAdditionalProducts = $this->getCartOrder()->getLines()->filter(function (CartOrderLine $cartOrderLine
        ) {
            /** @var $cartOrderLine CartOrderLine */
            return ($cartOrderLine->getMetadata() && !empty($cartOrderLine->getMetadata()['additional_product_parent']) && (int)$cartOrderLine->getMetadata()['additional_product_parent'] === $this->getProduct()->getId());
        });

        return !$hasProductAdditionalProducts->isEmpty();
    }

    /**
     * @param Product $product
     * @return float|int
     */
    public function setConfigurationPrice(Product $product)
    {
        $price = 0;
        /** @var Combination $combination */
        foreach ($product->getCombinations() as $combination) {
            $quantity = 1;
            if ($this->getMetadata() && isset($this->getMetadata()['configuration'])) {
                $configurations = $this->getMetadata()['configuration'];

                foreach ($configurations as $configuration) {
                    if ($combination->getProduct()->getId() === $configuration->productId) {
                        $quantity = $configuration->quantity;
                    }
                }
            }

            $price += ($combination->getProduct()->calculatePrice() * $quantity);
        }

        $this->setPrice($price);

        return $price;
    }

    /**
     * @return bool
     */
    public function hasRequiredPersonalization()
    {
        /** @var Product $product */
        $product = $this->getProduct();

        return $product->isRequiredPersonalization()
            || ($this->getParent() && $this->getParent()->getProduct()->hasRequiredPersonalization());

    }

    /**
     * @return int
     */
    public function getLineCount()
    {
        $lines = 1;

        if ($this->getDiscountAmount()) {
            $lines++;
        }

        foreach ($this->getChildren() as $childLine) {
            if (null !== $childLine->getProduct()
                && ($childLine->getProduct()->isCard()
                    || isset($childLine->getMetadata()['additional_product_parent'])
                    || $childLine->getProduct()->isPersonalization()
                    || $childLine->getProduct()->isLetter())
            ) {
                continue;
            }

            $lines++;
            if ($childLine->getDiscountAmount()) {
                $lines++;
            }
        }


        return $lines;
    }

    /**
     * Get vat
     *
     * @param Product|null $product
     * @return Vat
     */
    public function getVat(?Product $product = null)
    {
        /** @var Customer $customer */
        $customer = $this->getCartOrder()->getCart()->getCustomer();

        //todo VAT WEB-2869
        if (null !== $customer && null !== $customer->getCompany()) {
            $company = $customer->getCompany();

            $invoiceAddress = $company->getInvoiceAddress();
            $deliveryAddress = $company->getDefaultDeliveryAddress();
            if (null !== $invoiceAddress && null !== $deliveryAddress && $invoiceAddress->getCountry()->getId() === 'BE' && $deliveryAddress->getCountry()->getId() === 'BE') {
                return $this->container->get('doctrine.orm.default_entity_manager')->getRepository(Vat::class)->find(4);
            }
        }

        if (null === $product) {
            $product = $this->getProduct();
        }

        if (null === $product->getVat() && null !== $product->getParent()) {
            $product = $product->getParent();
        }

        if (null === $product->getVat() && $product instanceof CompanyProduct) {
            return $this->getVat($product->getRelatedProduct());
        }

        return $product->getVat();
    }
}
