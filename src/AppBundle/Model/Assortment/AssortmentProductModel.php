<?php

namespace AppBundle\Model\Assortment;

use AppBundle\Entity\Catalog\Assortment\AssortmentProductImage;
use AppBundle\Entity\Catalog\Product\ProductImage;
use AppBundle\Model\BaseModel;
use Doctrine\Common\Collections\Collection;

/**
 * Class AssortmentProductModel
 * @package AppBundle\Model\Assortment
 */
class AssortmentProductModel extends BaseModel
{
    /**
     * @param $fallback
     *
     * @return AssortmentProductImage|AssortmentProductModel
     */
    public function getMainImage($fallback = false)
    {
        if(null === $this->getProduct()) {
            return null;
        }

        /** @var Collection|AssortmentProductImage $images */
        $images = $this->getAssortmentProductImages();

        if ($images->isEmpty()) {
            return $this->getProduct()->getMainImage();
        }

        // Filter the image collection for a main image
        $mainImage = $images->filter(function (AssortmentProductImage $entry) {
            return ($entry->getMain() || $entry->getCategory());
        });

        // Return the main image
        if (!$mainImage->isEmpty()) {
            return $mainImage->current();
        }

        // If there's no main image and the fallback is set to true, return the first available image
        if ($fallback && !$mainImage->current() && !$images->isEmpty()) {
            return $images->current();
        }

        //if there is no main image and the product is a variation, select the parent's main image
        $parent = $this->getParent();
        if (null !== $parent && $mainImage->isEmpty()) {
            $images = $parent->getProductImages();
            $parentImage = $images->filter(function (ProductImage $entry) {
                return $entry->getMain();
            });

            if (!$parentImage->isEmpty()) {
                return $parentImage->current();
            }
        }

        // Try to find an image from the first variantion that got an image
        $variations = $this->getVariations();
        if (!$variations->isEmpty()) {
            foreach ($variations as $variation) {
                $images = $variation->getProductImages();
                $variantImage = $images->filter(function (ProductImage $entry) {
                    return $entry->getMain();
                });

                if (!$variantImage->isEmpty()) {
                    return $variantImage->current();
                }
            }
        }

        if (\method_exists($this, 'getPath') && !empty($this->getPath())) {
            return $this;
        }

        // If there's no main image and the fallback is set to false, return null
        return null;
    }

    /**
     * @return null
     */
    public function getUntaggedImages()
    {
        /** @var Collection|AssortmentProductImage $images */
        $images = $this->getAssortmentProductImages();

        if ($images->isEmpty()) {
            return $this->getProduct()->getUntaggedImages();
        }

        // Filter the image collection for all untagged images
        $untaggedImages = $images->filter(function (AssortmentProductImage $entry) {
            return (!$entry->getMain() && !$entry->getCategory());
        });

        if ($untaggedImages->isEmpty()) {
            return null;
        }

        return $untaggedImages;
    }

}
