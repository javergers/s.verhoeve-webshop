<?php

namespace AppBundle\Model;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Assortment\AssortmentType;
use AppBundle\Entity\Catalog\Product\CompanyCard;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyEstablishment;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Order\Order;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * Class CompanyModel
 *
 * @package AppBundle\Model
 */
class CompanyModel extends BaseModel
{

    /**
     * @param int $limit
     * @return array
     */
    public function getRecentOrders($limit = 5)
    {
        /**
         * @var $qb QueryBuilder
         */
        $qb = $this->container->get('doctrine')->getManager()->getRepository(Order::class)->createQueryBuilder('o');
        $qb->leftJoin('o.orderCollection', 'oc');
        $qb->andWhere('oc.company = :company');
        $qb->orderBy('o.createdAt', 'DESC');
        $qb->setMaxResults($limit);
        $qb->setParameter('company', $this);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param int $limit
     * @return array
     */
    public function getRecentCustomers($limit = 5)
    {
        /**
         * @var $qb QueryBuilder
         */
        $qb = $this->container->get('doctrine')->getManager()->getRepository(Customer::class)->createQueryBuilder('customer');
        $qb->andWhere('customer.company = :company');
        $qb->orderBy('customer.createdAt', 'DESC');
        $qb->setMaxResults($limit);
        $qb->setParameter('company', $this);
        return $qb->getQuery()->getResult();
    }

    /**
     * @return float|int
     */
    public function getTotalOrdersRevenue()
    {
        /** @var Company $company */
        $company = $this;
        $total = 0;
        /**
         * @var Order $order
         */
        foreach ($company->getOrders()->getIterator() as $order) {
            $total += $order->getTotalPriceIncl();
        }

        return $total;
    }

    /**
     * @return int
     */
    public function getTotalOrders()
    {
        /** @var Company $company */
        $company = $this;
        return $company->getOrders()->count();
    }

    /**
     * @return Collection
     */
    public function getNonCardAssortments()
    {
        /** @var Company $company */
        $company = $this;
        return $company->getAssortments()->filter(function ($assortment) {
            /** @var Assortment $assortment */
            return (!$assortment->getAssortmentType() || $assortment->getAssortmentType()->getKey() !== AssortmentType::TYPE_CARDS);
        });
    }

    /**
     * @return Collection
     */
    public function getCardAssortments()
    {
        /** @var Company $company */
        $company = $this;
        return $company->getAssortments()->filter(function ($assortment) {
            /** @var Assortment $assortment */
            return ($assortment->getAssortmentType() && $assortment->getAssortmentType()->getKey() === AssortmentType::TYPE_CARDS);
        });
    }

    /**
     * @return ArrayCollection
     */
    public function getAssortmentCards()
    {
        /** @var Company $company */
        $company = $this;
        $cards = new ArrayCollection();

        foreach ($company->getAssortments() as $assortment) {
            if ($assortment->getAssortmentType() && $assortment->getAssortmentType()->getKey() === AssortmentType::TYPE_CARDS) {
                foreach ($assortment->getAssortmentProducts() as $assortmentProduct) {
                    if (!$cards->contains($assortmentProduct->getProduct())) {
                        $cards->add($assortmentProduct->getProduct());
                    }
                }
            }
        }

        return $cards;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getSiteDefaultCards()
    {
        $companyCardIds = $this->container->get('app.parameter')->setEntity(null)->getValue('default_company_cards');

        /** @var EntityRepository $repo */
        $repo = $this->container->get('doctrine')->getManager()->getRepository(ProductCard::class);
        /** @var QueryBuilder $qb */
        $qb = $repo->createQueryBuilder('card');
        $qb->andWhere('card.id IN(:ids)');
        $qb->setParameter('ids', explode(',', $companyCardIds), Connection::PARAM_INT_ARRAY);

        $cards = [];
        /** @var CompanyCard $card */
        foreach ($qb->getQuery()->getResult() as $card) {
            $cards[] = [
                'id' => $card->getId(),
                'title' => $card->translate()->getTitle(),
            ];
        }

        return $cards;
    }

}