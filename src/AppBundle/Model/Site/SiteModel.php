<?php

namespace AppBundle\Model\Site;

use AppBundle\Entity\Site\Menu;
use AppBundle\Model\BaseModel;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class SiteModel
 * @package AppBundle\Model\Site
 */
class SiteModel extends BaseModel
{
    /**
     * @return ArrayCollection|Menu[]
     */
    public function getMainMenu()
    {
        return $this->getMenus()->filter(function (Menu $menu) {
            return $menu->getLocation() === 'main';
        });
    }
}