<?php

namespace AppBundle\Model;

class CustomerModel extends BaseModel
{

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->getFirstname() . ' ' . $this->getLastname();
    }

}