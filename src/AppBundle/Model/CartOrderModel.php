<?php

namespace AppBundle\Model;

use AppBundle\Decorator\Catalog\Product\CompanyProductDecorator;
use AppBundle\Entity\Catalog\Product\Combination;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Geography\Holiday;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Entity\Supplier\SupplierGroupProduct;
use AppBundle\Entity\Supplier\SupplierProduct;
use AppBundle\Exceptions\Sales\CartInvalidSupplierCombinationException;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Manager\Catalog\Product\ProductManager;
use AppBundle\Services\CartService;
use AppBundle\Services\Discount\DiscountService;
use AppBundle\Services\SiteService;
use AppBundle\Utils\DeliveryDate;
use AppBundle\Utils\DeliveryDates;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use AppBundle\Services\SupplierManagerService;

/**
 * Class CartOrderModel
 * @package AppBundle\Model
 */
class CartOrderModel extends BaseModel
{
    private $displayPriceIncl = [];

    /**
     * @param Product            $product
     * @param int                $quantity
     * @param CartOrderLine|null $parentCartOrderLine
     * @param bool               $skipSupplierCheck
     * @param array              $metadata
     * @return CartOrderLine
     * @throws CartInvalidSupplierCombinationException
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws \Exception
     */
    public function addProduct(
        Product $product,
        $quantity = 1,
        CartOrderLine $parentCartOrderLine = null,
        $skipSupplierCheck = false,
        array $metadata = []
    ) {
        /** @var EntityManagerInterface $em */
        $em = $this->container->get('doctrine')->getManager();
        $cartOrderLine = null;

        if ($parentCartOrderLine && $parentCartOrderLine->getProduct()->getId() === $product->getId()) {
            throw new \RuntimeException("product and parentProduct couldn't be the same");
        }

        if ($product->isPublished() !== true) {
            throw new \RuntimeException('product is not published');
        }

        if (!$skipSupplierCheck && !$this->isSupplierAvailable($product)) {
            throw new CartInvalidSupplierCombinationException(
                vsprintf('There is no supplier available for: %s in combination with other suppliers for the current cart',
                    [
                        $product->translate()->getTitle(),
                    ])
            );
        }

        /** @var CartOrderLine $line */
        foreach ($this->getLines() as $line) {
            if ($line->getProduct()->getId() === $product->getId()) {
                $line->setQuantity($line->getQuantity() + $quantity);
                $cartOrderLine = $line;
                break;
            }
        }

        if (null === $cartOrderLine) {
            /** @var $parentCartOrderLine CartOrderLine */
            $cartOrderLine = new CartOrderLine();
            $cartOrderLine->setContainer($this->container);
            $cartOrderLine->setCartOrder($this);
            $cartOrderLine->setMetadata($metadata);

            if ($parentCartOrderLine) {
                $cartOrderLine->setParent($parentCartOrderLine);
            }

            $cartOrderLine->setProduct($product);

            //use decorated product to retrieve the correct price
            $productManager = $this->container->get(ProductManager::class);
            $decoratedProduct = $productManager->decorateProduct($product);

            if ($product->isConfigurable() && $product->isCombination()) {
                $cartOrderLine->setConfigurationPrice($product);
            }

            $cartOrderLine->setPrice($decoratedProduct->getPrice());

            if (!$quantity && $parentCartOrderLine) {
                $quantity = $parentCartOrderLine->getQuantity();
            }

            $cartOrderLine->setQuantity($quantity);

            $discount = $this->container->get('app.product_price_manager')->getDiscount($product);
            $discountService = $this->container->get(DiscountService::class);

            if(null !== $discount) {
                $discountService->applyDiscountForOrderLineInterface($discount, $cartOrderLine);

                if($cartOrderLine->getDiscountAmount()) {
                    $cartOrderLine->setDiscountType('assortment');
                }
            }

            $this->addLine($cartOrderLine);

            $em->persist($cartOrderLine);
        }

        if ($this->container->get(CartService::class)->autoFlush) {
            $em->flush($cartOrderLine);
        }

        return $cartOrderLine;
    }

    /**
     * @param Product $product
     *
     * @return bool
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    private function isSupplierAvailable(ProductInterface $product)
    {
        $available = false;
        $cartSuppliers = $this->container->get(SupplierManagerService::class)->getAvailableSuppliers($this);
        $productSuppliers = $this->container->get(SupplierManagerService::class)->getAvailableSuppliersForProduct($product);
        if ($this->container->get(CartService::class)->count() === 0) {
            return !$productSuppliers->isEmpty();
        }
        foreach ($productSuppliers as $supplier) {
            if ($cartSuppliers->contains($supplier)) {
                $available = true;
                break;
            }
        }

        return $available;
    }

    /**
     * @param Productgroup $productgroup
     *
     * @return ArrayCollection|CartOrderLine[]
     */
    public function getLinesContainingProductgroup(Productgroup $productgroup)
    {
        $filtered = $this->getLines()->filter(function (CartOrderLine $cartOrderLine) use ($productgroup) {
            $mainProduct = $cartOrderLine->getProduct();

            if ($mainProduct->getParent()) {
                $mainProduct = $mainProduct->getParent();
            }

            if (null === $mainProduct->getProductgroup()) {
                return false;
            }

            return $mainProduct->getProductgroup()->getId() === $productgroup->getId();
        });

        return $filtered;
    }

    /**
     * @throws \Exception
     */
    public function getDeliveryDates()
    {
        $begin = new \DateTime();
        $begin->setTime(0, 0, 0);

        $end = clone $begin;
        $end = $end->modify('+365 day');

        $interval = new \DateInterval('P1D');

        /** @var $dates \DateTime[] */
        $dates = new \DatePeriod($begin, $interval, $end);

        $deliveryDates = new DeliveryDates([], $this->container);
        $order = $this->container->get(CartService::class)->getCurrentOrder();
        $transportType = $this->container->get('app.transport_type_helper')->getTransportType($order);

        if (null === $transportType) {
            throw new \RuntimeException('No transport type received');
        }

        foreach ($dates as $date) {
            $deliveryDate = new DeliveryDate($date->format('r'));
            $deliveryDate->setTransportType($transportType);

            $deliveryDates->add($deliveryDate);
        }

        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->container->get('doctrine')->getRepository(Holiday::class);

        $qb = $entityRepository->createQueryBuilder('h');
        $qb->leftJoin('h.countries', 'hc');
        $qb->andWhere('hc.country = :country');

        $qb->setParameters([
            'country' => $this->getDeliveryAddressCountry(),
        ]);

        $holidays = new ArrayCollection($qb->getQuery()->getResult());

        foreach ($deliveryDates as &$deliveryDate) {
            $deliveryDate->setIterator($deliveryDates);

            $matchedHolidays = $holidays->filter(function (Holiday $holiday) use ($deliveryDate) {
                return $holiday->getDate()->format('d-m-Y') === $deliveryDate->format('d-m-Y');
            });

            if (!$matchedHolidays->isEmpty()) {
                $deliveryDate->setHoliday($matchedHolidays->current());
            }
        }

        return $deliveryDates;
    }

    /**
     * @param Product $product
     *
     * @return ArrayCollection|CartOrderLine[]
     */
    public function getLinesContainingProduct(Product $product)
    {
        return $this->getLines()->filter(function (CartOrderLine $cartOrderLine) use ($product) {
            return $cartOrderLine->getProduct()->getId() === $product->getId();
        });
    }

    /**
     * Method to get the cartOrderLine containing the delivery product
     *
     * @return CartOrderLine|bool
     */
    public function getLineContainingDeliveryProduct()
    {
        return $this->getLines()->filter(function (CartOrderLine $cartOrderLine) {
            return ($cartOrderLine->getProduct() instanceof TransportType);
        })->current();
    }

    /**
     * @return string
     */
    public function getTotalDisplayPrice()
    {
        /** depens on site and user settings */
        $site = $this->container->get(SiteService::class)->determineSite();
        $siteDefaultDisplayPrice = $site ? $site->getDefaultDisplayPrice() : 'inclusive';

        if ($siteDefaultDisplayPrice === 'exclusive') {
            $price = $this->getTotalPrice();
        } else {
            $price = $this->getTotalPriceIncl();
        }

        return $price;
    }

    /**
     * @param bool $withDiscount
     * @return float
     */
    public function getDisplayPrice($withDiscount = true)
    {
        $displayPrice = 0;
        foreach ($this->getLines() as $line) {
            $totalPriceIncl = $line->getTotalPrice();

            if ($withDiscount) {
                $totalPriceIncl -= $line->getDiscountAmount();
            }

            $displayPrice += $totalPriceIncl;
        }

        return $displayPrice;
    }

    /**
     * @param bool $withDiscount
     * @return float
     */
    public function getDisplayPriceIncl($withDiscount = true)
    {
        if (isset($this->displayPriceIncl[$this->getId()][$withDiscount])) {
            return $this->displayPriceIncl[$this->getId()][$withDiscount];
        }

        $displayPrice = 0;
        foreach ($this->getLines() as $line) {
            $totalPriceIncl = $line->getTotalPriceIncl();

            if ($withDiscount) {
                $totalPriceIncl -= $line->getDiscountAmountIncl();
            }

            $displayPrice += $totalPriceIncl;
        }

        return ($this->displayPriceIncl[$this->getId()][$withDiscount] = $displayPrice);
    }

    /**
     * @return int
     */
    public function getDisplayDiscountAmountIncl()
    {
        $displayPrice = 0;

        foreach ($this->getLines() as $line) {
            $displayPrice += $line->getDiscountAmountIncl();
        }

        return $displayPrice;
    }

    /**
     * @return int
     */
    public function getAssortmentDiscountAmountIncl()
    {
        $total = 0;

        /** @var CartOrderLine $line */
        foreach ($this->getLines() as $line) {
            $total += $line->getDiscountType() === 'assortment' ? $line->getDiscountAmount() : 0;
        }

        return $total;
    }

    /**
     * @return ArrayCollection
     */
    public function getAvailableCountries()
    {
        $countries = new ArrayCollection($this->container->get('doctrine')->getRepository(Country::class)->findAll());

        $availableCountries = $countries->filter(function (Country $country) {
            return ($country->getCode() === 'NL' || $country->getCode() === 'BE');
        });

        return $availableCountries;
    }

    /**
     * @param Product $product
     * @param int     $quantity
     *
     * @return CartOrderLine
     * @throws CartInvalidSupplierCombinationException
     * @throws \Exception
     */
    public function replaceContentsWithProduct(ProductInterface $product, int $quantity)
    {
        $this->removeAllLines();

        return $this->addProduct($product, $quantity);
    }

    /**
     * Method that removes all CartOrderLines
     */
    public function removeAllLines()
    {
        $em = $this->container->get('doctrine')->getManager();

        /** @var CartOrderLine $line */
        foreach ($this->getLines() as $line) {
            $this->removeLine($line);
        }

        $this->setDeliveryDate(null);

        $em->persist($this);
        $em->flush();
    }

    /**
     * @return bool
     */
    public function isDeliveryInformationComplete()
    {
        if ($this->getPickupAddress() !== null) {
            return true;
        }

        $site = $this->container->get(SiteService::class)->determineSite();
        if (null !== $site && !$site->getAddToCartModalEnabled() && (null === $this->getDeliveryAddressCountry()
                || (null === $this->getDeliveryDate() && $site->getDeliveryDateMethod() === 'required'))) {
            return false;
        }

        return !(null === $this->getDeliveryAddressAttn()
            || null === $this->getDeliveryAddressStreet()
            || null === $this->getDeliveryAddressPostcode()
            || null === $this->getDeliveryAddressCity()
            || null === $this->getDeliveryAddressNumber());
    }

    /**
     * Get delivery address street, number and number addition as a single string
     *
     * @return string
     */
    public function getDeliveryAddressStreetAndNumber(): string
    {
        $deliveryAddressStreetAndNumber = trim($this->getDeliveryAddressStreet() . " " . $this->getDeliveryAddressNumber());

        if ($this->getDeliveryAddressNumberAddition()) {
            $deliveryAddressStreetAndNumber .= '-' . $this->getDeliveryAddressNumberAddition();
        }

        return $deliveryAddressStreetAndNumber;
    }
}
