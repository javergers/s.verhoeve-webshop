<?php

namespace AppBundle\Model;

use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Manager\Catalog\Product\ProductManager;
use AppBundle\Services\SiteService;

/**
 * Class CartModel
 * @package AppBundle\Model
 */
class CartModel extends BaseModel
{

    /**
     * @deprecated
     * @param bool $withDiscount
     * @return float
     */
    public function getCartTotal($withDiscount = true)
    {
        $total = 0;
        /** @var CartOrder $order */
        foreach ($this->getOrders() as $order) {
            $total += $order->getDisplayPriceIncl($withDiscount);
        }

        return $total;
    }

    /**
     * @deprecated
     * @param bool $withDiscount
     * @return float
     */
    public function getCartSubTotal($withDiscount = true)
    {
        $subtotal = 0;
        foreach ($this->getOrders() as $order) {
            $subtotal += $order->getTotalPrice($withDiscount);
        }

        return $subtotal;
    }

    /**
     * @return float
     * @deprecated
     */
    public function getDiscountAmountIncl()
    {
        $total = 0;

        foreach ($this->getOrders() as $order) {
            $total += $order->getDisplayDiscountAmountIncl();
        }

        return $total;
    }

    /**
     * @return int
     */
    public function getAssortmentDiscountAmountIncl() {
        $total = 0;

        /** @var CartOrder $order */
        foreach($this->getOrders() as $order) {
            $total += $order->getAssortmentDiscountAmountIncl();
        }

        return $total;
    }

    /**
     * @param bool $withDiscount
     * @return array
     */
    public function getVatTotals($withDiscount = true)
    {
        $productManager = $this->container->get(ProductManager::class);

        $vats = [];

        $site = $this->container->get(SiteService::class)->determineSite();

        /** @var CartOrder $order */
        foreach ($this->getOrders() as $order) {
            /** @var CartOrderLine $cartOrderLine */
            foreach ($order->getLines() as $cartOrderLine) {
                $vat = $cartOrderLine->getVat();

                if (null === $vat) {
                    continue;
                }

                $exclusive = $site->getDefaultDisplayPrice() === 'exclusive';

                $totalPrice =  $exclusive ? $cartOrderLine->getTotalPrice($withDiscount) : $cartOrderLine->getTotalPriceIncl($withDiscount);

                if($exclusive) {
                    $vatPrice = ($totalPrice * ($vat->getPercentage() / 100));
                } else {
                    $vatPrice = ($totalPrice / (100 + $vat->getPercentage()) * $vat->getPercentage());
                }

                if ($vatPrice === 0) {
                    continue;
                }

                $vatId = $vat->getId();

                $vats[$vatId] = [
                    'percentage' => $vat->getPercentage(),
                    'vatPrice' => isset($vats[$vatId]['vatPrice']) ? $vats[$vatId]['vatPrice'] += $vatPrice : $vatPrice,
                    'totalPrice' => isset($vats[$vatId]['totalPrice']) ? $vats[$vatId]['totalPrice'] += $totalPrice : $totalPrice,
                ];
            }
        }

        return $vats;
    }

    /**
     * Returns the count of different delivery address currently present in the cart
     *
     * @return int
     */
    public function getDeliveryAddressCount()
    {
        $addresses = [];

        foreach ($this->getOrders() as $order) {
            $addressKey = $order->getDeliveryAddressPostcode() . $order->getDeliveryAddressNumber() . 'X' . $order->getDeliveryAddressNumberAddition();

            if (\in_array($addressKey, $addresses, true)) {
                continue;
            }

            $addresses[] = $addressKey;
        }

        return \count($addresses);
    }

    /**
     * Get invoice address street, number and number addition as a single string
     *
     * @return string
     */
    public function getInvoiceAddressStreetAndNumber(): string
    {
        $invoiceAddressStreetAndNumber = trim($this->getInvoiceAddressStreet() . " " . $this->getInvoiceAddressNumber());

        if ($this->getInvoiceAddressNumberAddition()) {
            $invoiceAddressStreetAndNumber .= '-' . $this->getInvoiceAddressNumberAddition();
        }

        return $invoiceAddressStreetAndNumber;
    }
}
