<?php

namespace AppBundle\Model;

use AppBundle\Manager\Order\OrderCollectionManager;
use AppBundle\Services\Fraud\OrderFraudCheckService;

/**
 * Class OrderCollectionModel
 * @package AppBundle\Model
 */
class OrderCollectionModel extends BaseModel
{
    /**
     * @return int
     */
    public function count()
    {
        $count = 0;

        foreach ($this->getOrders() as $order) {

            foreach ($order->getLines() as $line) {

                // @Todo: Check perchasable based on line instead of product.
                if (!is_null($line->getProduct()) && !$line->getProduct()->getPurchasable()) {
                    continue;
                }

                $count += $line->getQuantity();
            }
        }

        return $count;
    }


    /**
     * @return float
     */
    public function getPaymentAmount()
    {
        if($this->container) {
            return $this->container->get(OrderCollectionManager::class)->calculateOutstanding($this);
        }

    }

    /**
     * @deprecated use OrderCollectionManager instead
     * @return float
     */
    public function calculateOutstanding()
    {
        if($this->container) {
            return $this->container->get(OrderCollectionManager::class)->calculateOutstanding($this);
        }
    }

    /**
     * @return mixed
     */
    public function getTotalDisplayPrice()
    {
        switch ($this->getSite()->getDefaultDisplayPrice()) {
            case "exclusive":
                return $this->getTotalPrice();
            default:
                return $this->getTotalPriceIncl();
        }
    }

    /**
     * @return bool
     * @deprecated
     */
    public function isEditAllowed() {
        return $this->isEditable();
    }

    /**
     * @return bool
     */
    public function isEditable() {
        return $this->container->get(OrderFraudCheckService::class)->canEditOrder($this);
    }

    /**
     * @param bool $auto
     *
     * @return bool
     * @throws \Exception
     */
    public function isProcessable(bool $auto = false): bool {
        return $this->container->get(OrderFraudCheckService::class)->canProcessOrder($this, $auto);
    }

    /**
     * @return bool
     */
    public function isFraudCheckable() {
        return $this->container->get(OrderCollectionManager::class)->isFraudCheckable($this);
    }

    /**
     * Get locale based on Customer
     *
     * @return string
     */
    public function getLocale(): string
    {
        /** @var OrderCollection $orderCollection */
        $orderCollection = $this;

        $customer = $orderCollection->getCustomer();

        if(null !== $customer && null !== ($locale = $customer->getLocale())) {
            return $locale;
        }

        return 'nl_NL';
    }
}
