<?php

namespace AppBundle\Decorator\ProductProperty;

use AppBundle\Decorator\ProductDecorator;
use AppBundle\Decorator\ProductPropertyDecorator;
use AppBundle\Entity\Catalog\Product\Product;

/**
 * Class CardDecorator
 *
 * @package AppBundle\Decorator\ProductProperty
 */
class CardDecorator extends ProductPropertyDecorator
{
    /**
     * @return ProductDecorator|Product|bool
     */
    public function getCardProduct()
    {
        /** @var Product $cardProduct */
        $cardProduct = $this->container->get('doctrine')->getRepository(Product::class)->findOneBy(['sku' => 'CARD']);

        if (!$cardProduct) {
            return false;
        }

        $cartProductDecorator = $this->container->get('app.product.factory')->get($cardProduct);

        return $cartProductDecorator;
    }

    /**
     * @return mixed
     */
    public function getMinimumPrice()
    {
        $prices = [];

        foreach ($this->getCardProduct()->getVariations() as $product) {
            $prices[] = $product->getPrice();
        }

        asort($prices);

        return array_shift($prices);
    }

    /**
     * @return string
     */
    public function getMinimumDisplayPrice()
    {
        return number_format($this->getMinimumPrice(), 2, ',', '.');
    }

    /**
     * @return mixed
     */
    public function getMinimumPriceIncl()
    {
        $prices = [];

        foreach ($this->getCardProduct()->getVariations() as $product) {
            $prices[] = $product->getPriceIncl();
        }

        asort($prices);

        return array_shift($prices);
    }

    /**
     * @return mixed|string
     */
    public function getMinimumDisplayPriceIncl()
    {
        $price = $this->getMinimumPriceIncl();
        //check if number is already formatted
        if (!is_numeric($price)) {
            return $price;
        }

        return number_format($price, 2, ',', '.');
    }

    /**
     * @return string
     */
    public function getTwigFile()
    {
        return 'AppBundle:blocks/Product/options:card.html.twig';
    }
}
