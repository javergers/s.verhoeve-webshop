<?php

namespace AppBundle\Decorator;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Site\Site;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AssortmentDecorator
{
    use ContainerAwareTrait;

    /**
     * @var Assortment;
     */
    private $assortment;

    /** @var Product[]|ArrayCollection */
    private $products;

    public function setAssortment(Assortment $assortment)
    {
        $this->assortment = $assortment;

        return $this;
    }

    /**
     * @return Assortment
     */
    public function get()
    {
        return $this->assortment;
    }

    /**
     * @return ArrayCollection
     */
    public function getProducts()
    {
        if (empty($this->products)) {
            //$this->products = $this->assortment->getProducts();
            $this->products = new ArrayCollection();
            foreach ($this->assortment->getProducts() as $product) {
                if (!$product) {
                    continue;
                }

                $decoratedProduct = $this->container->get("app.product.factory")->get($product);

                $this->products->add($decoratedProduct);
            }
        }

        return $this->products;
    }

    public function getUrl($language = null, Site $site = null)
    {
        return $this->container->get("app.slug")->getUrl($this->assortment, $language, $site);
    }

    public function getAbsoluteUrl($language = null, Site $site = null)
    {
        return $this->container->get("app.slug")->getUrl($this->assortment, $language, $site,
            UrlGeneratorInterface::ABSOLUTE_URL);
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->assortment, $name], $arguments);
    }
}