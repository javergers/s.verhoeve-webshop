<?php

namespace AppBundle\Decorator\Factory;

use AppBundle\Decorator\AssortmentDecorator;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AssortmentDecoratorFactory
{
    use ContainerAwareTrait;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Assortment $assortment
     * @return AssortmentDecorator
     */
    public function get(Assortment $assortment)
    {
        $decoratedAssortment = new AssortmentDecorator();
        $decoratedAssortment->setContainer($this->container);
        $decoratedAssortment->setAssortment($assortment);

        return $decoratedAssortment;
    }
}