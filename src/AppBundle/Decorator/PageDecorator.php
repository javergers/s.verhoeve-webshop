<?php

namespace AppBundle\Decorator;

use AppBundle\Entity\Site\Banner;
use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Site\Site;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PageDecorator
{
    use ContainerAwareTrait;

    /**
     * @var Page
     */
    private $page;

    /**
     * @var Banner
     */
    private $banner;

    /**
     * @param Page $page
     * @return $this
     */
    public function setPage(Page $page)
    {
        $this->page = $page;

        try {
            $this->banner = $this->page->getBanner();

            if ($this->banner) {
                $this->banner->getCreatedAt();
            }
        } catch (EntityNotFoundException $e) {
            $this->banner = null;
        }

        return $this;
    }

    /**
     * @return Page
     */
    public function get()
    {
        return $this->page;
    }

    /**
     * @return Banner
     */
    public function getBanner()
    {
        return $this->banner;
    }

    /**
     * @param null      $language
     * @param Site|null $site
     * @param null      $referenceType
     * @return null|string
     */
    public function getUrl($language = null, Site $site = null, $referenceType = null)
    {
        return $this->container->get("app.slug")->getUrl($this->page, $language, $site, $referenceType);
    }

    /**
     * @param null      $language
     * @param Site|null $site
     * @return null|string
     */
    public function getAbsoluteUrl($language = null, Site $site = null)
    {
        return $this->container->get("app.slug")->getUrl($this->page, $language, $site,
            UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return call_user_func_array([$this->page, $name], $arguments);
    }
}
