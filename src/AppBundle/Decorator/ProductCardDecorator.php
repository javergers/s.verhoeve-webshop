<?php

namespace AppBundle\Decorator;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class CardDecorator
 *
 * @package AppBundle\Decorator\ProductProperty
 */
class ProductCardDecorator extends ProductPropertyDecorator
{
    /**
     * @var Assortment
     */
    protected $assortment;

    /**
     * @var ArrayCollection
     */
    protected $cards;

    /**
     * @param AssortmentDecorator $assortment
     */
    public function setCards(AssortmentDecorator $assortment)
    {
        $this->assortment = $assortment;
        $this->cards = new ArrayCollection();

        foreach ($assortment->getProducts() as $product) {
            $this->cards->add($product);
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getCards()
    {
        return $this->cards;
    }

    /**
     * @deprecated
     * @return mixed
     */
    public function getMinimumPrice()
    {
        return $this->assortment->getMinimumPrice();
    }

    /**
     * @deprecated
     * @return string
     */
    public function getMinimumDisplayPrice()
    {
        return $this->assortment->getMinimumDisplayPrice();
    }

    /**
     * @deprecated
     * @return mixed
     */
    public function getMinimumPriceIncl()
    {
        return $this->assortment->getMinimumPriceIncl();
    }

    /**
     * @deprecated
     * @return string
     */
    public function getMinimumDisplayPriceIncl()
    {
        return $this->assortment->getMinimumDisplayPriceIncl();
    }

    /**
     * @return string
     */
    public function getTwigFile()
    {
        return 'AppBundle:blocks/Product/options:card.html.twig';
    }
}
