<?php

namespace AppBundle\ThirdParty\Twinfield\Entity;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Security\Employee\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity()
 * @ORM\Table("twinfield_export")
 */
class Export
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Relation\Company", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="twinfield_export_company")
     */
    protected $companies;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Order\Order", inversedBy="twinfieldExportOrders", fetch="EXTRA_LAZY"))
     * @ORM\JoinTable(name="twinfield_export_order")
     */
    protected $orders;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $invoiceDate;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Employee\User")
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $createdBy;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companies = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Export
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add company
     *
     * @param Company $company
     *
     * @return Export
     */
    public function addCompany(Company $company)
    {
        $this->companies[] = $company;

        return $this;
    }

    /**
     * Remove company
     *
     * @param Company $company
     */
    public function removeCompany(Company $company)
    {
        $this->companies->removeElement($company);
    }

    /**
     * Get companies
     *
     * @return Collection|Company[]
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * @return int
     */
    public function getCompaniesCount()
    {
        return $this->getCompanies()->count();
    }

    /**
     * Add order
     *
     * @param Order $order
     *
     * @return Export
     */
    public function addOrder(Order $order)
    {
        $this->orders[] = $order;

        return $this;
    }

    /**
     * Remove order
     *
     * @param Order $order
     */
    public function removeOrder(Order $order)
    {
        $this->orders->removeElement($order);
    }

    /**
     * Get orders
     *
     * @return Collection|Order[]
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * @return int
     */
    public function getOrdersCount()
    {
        return $this->getOrders()->count();
    }

    /**
     * Set createdBy
     *
     * @param User $createdBy
     *
     * @return Export
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set invoiceDate
     *
     * @param \DateTime $invoiceDate
     *
     * @return Export
     */
    public function setInvoiceDate(\DateTime $invoiceDate)
    {
        $this->invoiceDate = $invoiceDate;

        return $this;
    }

    /**
     * Get invoiceDate
     *
     * @return \DateTime
     */
    public function getInvoiceDate()
    {
        return $this->invoiceDate;
    }
}
