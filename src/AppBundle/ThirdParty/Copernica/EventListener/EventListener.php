<?php

namespace AppBundle\ThirdParty\Copernica\EventListener;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Customer\NewsletterPreferences;
use AppBundle\Services\JobManager;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Exception;
use JMS\JobQueueBundle\Entity\Job;

/**
 * Class EventListener
 * @package AppBundle\ThirdParty\Copernica\EventListener
 */
class EventListener implements EventSubscriber
{
    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @var array
     */
    private $entityChangeSet;

    /**
     * EventListener constructor.
     *
     * @param JobManager $jobManager
     */
    public function __construct(JobManager $jobManager)
    {
        $this->jobManager = $jobManager;
        $this->entityChangeSet = [];
    }

    /**
     * @return array
     */
    public function getSubscribedEvents(): array
    {
        return [
//            'preUpdate',
//            'postUpdate',
//            'postPersist',
        ];
    }

    /**
     * @param PreUpdateEventArgs $eventArgs
     */
    public function preUpdate(PreUpdateEventArgs $eventArgs): void
    {
        $this->entityChangeSet = $eventArgs->getEntityChangeSet();
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     *
     * @throws Exception
     */
    public function postUpdate(LifecycleEventArgs $eventArgs): void
    {
        $this->post($eventArgs);
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     *
     * @throws Exception
     */
    public function postPersist(LifecycleEventArgs $eventArgs): void
    {
        $this->post($eventArgs);
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     *
     * @throws Exception
     */
    public function post(LifecycleEventArgs $eventArgs): void
    {
        $entity = $eventArgs->getEntity();

        $changeSet = $eventArgs->getEntityManager()->getUnitOfWork()->getEntityChangeSet($entity);
        unset($changeSet['updatedAt']);
        if(count($changeSet) === 0) {
            return;
        }

        if ($entity instanceof Company) {
            $this->handleCopernicaCompany($entity);
            return;
        }
        if ($entity instanceof Customer) {
            $this->handleCopernicaCustomer($entity);
            return;
        }

        $orderLines = [];

        //Insert all lines after invoice change
        if ($entity instanceof OrderCollection) {
            $updateOrderCollection = false;
            foreach ($this->entityChangeSet as $memberName => $changedValues) {
                if (0 === strncmp($memberName, 'invoice', 7)) {
                    $updateOrderCollection = true;
                    break;
                }
            }

            if ($updateOrderCollection) {
                foreach ($entity->getOrders() as $order) {
                    foreach ($order->getLines() as $line) {
                        $orderLines[] = $line;
                    }
                }
            }
        }

        //Insert all lines after delivery change
        if ($entity instanceof Order) {
            $updateOrder = false;
            foreach ($this->entityChangeSet as $memberName => $changedValues) {
                if (0 === strncmp($memberName, 'delivery', 8)) {
                    $updateOrder = true;
                    break;
                }
            }
            if ($entity instanceof NewsletterPreferences) {
                $this->handleCopernicaNewsletterPreferences($entity);
            }

            if ($updateOrder) {
                foreach ($entity->getLines() as $line) {
                    $orderLines[] = $line;
                }
            }
        }

        //Update if changed
        if ($entity instanceof OrderLine) {
            $orderLines[] = $entity;
        }

        foreach (array_unique($orderLines) as $entity) {
            if (false !== $entity) {
                $this->handleCopernicaOrderLine($entity);
            }
        }
    }

    /**
     * @param string $name
     * @param int $id
     * @param        $entity
     *
     * @throws Exception
     */
    private function createJob(string $name, int $id, $entity): void
    {
        $job = new Job($name, [$id], true, 'copernica');
        $job->addRelatedEntity($entity);
        $this->jobManager->addJob($job, 10);
    }

    /**
     * @param Company $entity
     *
     * @throws Exception
     */
    private function handleCopernicaCompany(Company $entity): void
    {
        if (null !== $entity->getId()) {
            $this->createJob('copernica:update:company', $entity->getId(), $entity);
        }
    }

    /**
     * @param Customer $entity
     *
     * @throws Exception
     */
    private function handleCopernicaCustomer(Customer $entity): void
    {
        if (null !== $entity->getId()) {
            $this->createJob('copernica:update:customer', $entity->getId(), $entity);
        }
    }

    /**
     * @param OrderLine $entity
     *
     * @throws Exception
     */
    private function handleCopernicaOrderLine(OrderLine $entity): void
    {
        if (null !== $entity->getId()) {
            $this->createJob('copernica:update:orderline', $entity->getId(), $entity);
        }
    }

    /**
     * @param NewsletterPreferences $entity
     * @throws \Exception
     */
    private function handleCopernicaNewsletterPreferences(NewsletterPreferences $entity)
    {
        if (null !== $entity->getId()) {
            $this->createJob('copernica:update:newsletterPreferences', $entity->getId(), $entity);
        }
    }
}
