<?php

namespace AppBundle\ThirdParty\Copernica\Exceptions;

/**
 * Class CopernicaException
 * @package AppBundle\ThirdParty\Copernica\Exceptions
 */
class CopernicaNotFoundException extends \Exception
{

}