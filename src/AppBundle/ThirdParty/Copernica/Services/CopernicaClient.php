<?php

namespace AppBundle\ThirdParty\Copernica\Services;

use AppBundle\Services\Parameter\ParameterService;
use AppBundle\ThirdParty\Copernica\Exceptions\CopernicaDuplicateException;
use AppBundle\ThirdParty\Copernica\Exceptions\CopernicaNotFoundException;
use GuzzleHttp;

/**
 * Class CopernicaClient
 * @package AppBundle\ThirdParty\Copernica\Services
 */
class CopernicaClient
{
    /**
     * @var GuzzleHttp\Client
     */
    private $guzzleClient;

    /**
     * @var ParameterService
     */
    protected $parameterService;

    /**
     * @var null|string
     */
    private $accessToken;

    /**
     * CopernicaService constructor.
     * @param GuzzleHttp\Client $guzzleClient
     * @param ParameterService  $parameterService
     * @throws \Exception
     */
    public function __construct(
        GuzzleHttp\Client $guzzleClient,
        ParameterService $parameterService
    ) {
        $this->guzzleClient = $guzzleClient;
        $this->parameterService = $parameterService->setEntity();
        //$this->accessToken = $this->parameterService->getValue('copernica_access_token');
    }


    /**
     * @param string $database
     * @param $id
     * @param string $identifier
     * @return bool
     * @throws CopernicaDuplicateException
     */
    public function doesCopernicaObjectExist(string $database, $id, string $identifier)
    {
        try {
            return (bool)$this->getCopernicaObject($database, $id, $identifier);
        } catch (CopernicaNotFoundException $e) {
            return false;
        }
    }


    /**
     * @param string $database
     * @param $id
     * @param string $identifier
     * @return null|int
     * @throws CopernicaDuplicateException
     * @throws CopernicaNotFoundException
     */
    public function getExternalId(string $database, $id, string $identifier)
    {
        $externalObject = $this->getCopernicaObject($database, $id, $identifier);
        return $externalObject->ID ?? null;
    }

    /**
     * @param string $database
     * @param $id
     * @param string $identifier
     * @return null|array
     * @throws CopernicaDuplicateException
     * @throws CopernicaNotFoundException
     */
    public function getCopernicaObject(string $database, $id, string $identifier)
    {
        $result = $this->get('/database/' . $database . '/profiles', [
            'query' => [
                'fields' => [
                    $identifier . '==' . $id,
                ],
            ],
        ]);

        if ($result === null || $result === false || \count($result) === 0) {
            throw new CopernicaNotFoundException(sprintf('Found no entries in Copernica for %s = %s in database %s',
                $identifier, $id, $database));
        }

        if (\count($result) === 1) {
            return $result[0];
        }

        throw new CopernicaDuplicateException(sprintf('Found duplicates in Copernica for %s = %s in database %s',
            $identifier, $id, $database));
    }

    /**
     * @param string $database
     * @param $id
     * @param string $identifier
     * @param mixed  $data
     * @return int
     * @throws CopernicaDuplicateException
     * @throws CopernicaNotFoundException
     */
    public function updateCopernicaObject(string $database, $id, string $identifier, $data)
    {
        try {
            if (($externalId = $this->getExternalId($database, $id, $identifier)) !== null) {
                $this->put('/database/' . $database . '/profiles', [
                    'query' => [
                        'fields' => [
                            $identifier . '==' . $id,
                        ],
                    ],
                    'data' => $data,
                ]);
                return $externalId;
            }
        } catch (CopernicaNotFoundException $e) {
            return $this->post('/database/' . $database . '/profiles', [
                'data' => $data,
            ]);
        }
        return null;
    }

    /**
     * @param string $endpoint
     * @param array  $params ['query' => [], 'data' => []]
     * @param string $method
     * @return null|mixed
     * @throws CopernicaNotFoundException
     */
    public function doRequest(string $endpoint, array $params = [], $method = 'GET')
    {
        if ($this->accessToken === '' || $this->accessToken === null) {
            throw new CopernicaNotFoundException('Access token parameter not set');
        }
        $endpoint = ltrim($endpoint, '/');
        $httpOptions = [
            'json' => $params['data'] ?? [],
            'query' => $params['query'] ?? [],
        ];
        $httpOptions['query']['access_token'] = $this->accessToken;
        try {
            $response = $this->guzzleClient->request($method, $endpoint, $httpOptions);
            $responseContents = json_decode($response->getBody()->getContents());
            if (null === $responseContents) {
                if ($method === 'POST') {
                    return $response->getHeader('X-Created')[0] ?: null;
                }
                if ($method === 'DELETE') {
                    return $response->getHeader('X-Deleted')[0] ?: null;
                }
                if ($method === 'PUT') {
                    return true;
                }
            }
            return $responseContents->data ?? null;
        } catch (GuzzleHttp\Exception\GuzzleException $e) {
            return null;
        }
    }

    /**
     * @param string $endpoint
     * @param array  $params
     * @return null|mixed
     * @throws CopernicaNotFoundException
     */
    public function get(string $endpoint, array $params = [])
    {
        return $this->doRequest($endpoint, $params, 'GET');
    }

    /**
     * @param string $endpoint
     * @param array  $params
     * @return null|mixed
     * @throws CopernicaNotFoundException
     */
    public function post(string $endpoint, array $params = [])
    {
        return $this->doRequest($endpoint, $params, 'POST');
    }

    /**
     * @param string $endpoint
     * @param array  $params
     * @return null|mixed
     * @throws CopernicaNotFoundException
     */
    public function delete(string $endpoint, array $params = [])
    {
        return $this->doRequest($endpoint, $params, 'DELETE');
    }

    /**
     * @param string $endpoint
     * @param array  $params
     * @return null|mixed
     * @throws CopernicaNotFoundException
     */
    public function put(string $endpoint, array $params = [])
    {
        return $this->doRequest($endpoint, $params, 'PUT');
    }
}
