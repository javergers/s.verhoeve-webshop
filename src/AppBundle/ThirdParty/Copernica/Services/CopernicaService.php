<?php

namespace AppBundle\ThirdParty\Copernica\Services;

use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Customer\NewsletterPreferences;
use AppBundle\ThirdParty\Copernica\Adapters\CompanyAdapter;
use AppBundle\ThirdParty\Copernica\Adapters\CustomerAdapter;
use AppBundle\ThirdParty\Copernica\Adapters\NewsletterPreferencesAdapter;
use AppBundle\ThirdParty\Copernica\Adapters\OrderLineAdapter;
use AppBundle\ThirdParty\Copernica\Exceptions\CopernicaDuplicateException;
use AppBundle\ThirdParty\Copernica\Exceptions\CopernicaNotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;

/**
 * Class CopernicaService
 * @package AppBundle\ThirdParty\Copernica\Services
 */
class CopernicaService
{
    /** @var string  */
    public const DB_COMPANIES = 'TG_Companies';
    /** @var string  */
    public const DB_CONTACTS = 'TG_Contacts';
    /** @var string  */
    public const DB_ORDERS = 'TG_Orders';
    /** @var string  */
    public const DB_NEWSLETTER_PREFRENCES = 'TG_Newsletter_preferences';

    /**
     * @var CopernicaClient
     */
    private $copernicaClient;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CopernicaService constructor.
     * @param CopernicaClient        $copernicaClient
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        CopernicaClient $copernicaClient,
        EntityManagerInterface $entityManager
    ) {
        $this->copernicaClient = $copernicaClient;
        $this->entityManager = $entityManager;
    }


    /**
     * @param Customer $customer
     * @return int externalCustomerId
     * @throws CopernicaDuplicateException
     * @throws CopernicaNotFoundException
     */
    public function updateCopernicaCustomer(Customer $customer): ?int
    {
        $customerAdapter = new CustomerAdapter($customer);
        $externalCustomerId = $this->copernicaClient->updateCopernicaObject($customerAdapter->getDatabase(),
            $customerAdapter->getId(), $customerAdapter->getIdentifier(), $customerAdapter->createRemoteObject());

        $meta = $customer->getMetadata();
        $meta['externalCopernicaCustomerId'] = $externalCustomerId;
        $customer->setMetadata($meta);

        $this->entityManager->flush();
        return $externalCustomerId;
    }

    /**
     * @param Company $company
     * @return int externalCompanyId
     * @throws CopernicaDuplicateException
     * @throws CopernicaNotFoundException
     */
    public function updateCopernicaCompany(Company $company): ?int
    {
        $companyAdapter = new CompanyAdapter($company);
        $externalCompanyId = $this->copernicaClient->updateCopernicaObject($companyAdapter->getDatabase(),
            $companyAdapter->getId(), $companyAdapter->getIdentifier(), $companyAdapter->createRemoteObject());

        $meta = $company->getMetadata();
        $meta['externalCopernicaCompanyId'] = $externalCompanyId;
        $company->setMetadata($meta);

        $this->entityManager->flush();
        return $externalCompanyId;
    }

    /**
     * @param OrderLine $orderLine
     * @return int externalOrderLineId
     * @throws CopernicaDuplicateException
     * @throws CopernicaNotFoundException
     */
    public function updateCopernicaOrderLine(OrderLine $orderLine): ?int
    {
        $orderLineAdapter = new OrderLineAdapter($orderLine);
        $externalOrderLineId = $this->copernicaClient->updateCopernicaObject($orderLineAdapter->getDatabase(),
            $orderLineAdapter->getId(), $orderLineAdapter->getIdentifier(), $orderLineAdapter->createRemoteObject());

        $meta = $orderLine->getMetadata();
        $meta['externalCopernicaOrderLineId'] = $externalOrderLineId;
        $orderLine->setMetadata($meta);

        $this->entityManager->flush();
        return $externalOrderLineId;
    }

    /**
     * @param NewsletterPreferences $newsletterPreferences
     * @return mixed|null
     * @throws CopernicaDuplicateException
     * @throws CopernicaNotFoundException
     */
    public function updateNewsletterPreferences(NewsletterPreferences $newsletterPreferences)
    {
        $newsletterPreferencesAdapter = new NewsletterPreferencesAdapter($newsletterPreferences);

        $externalNewsletterPreferencesId = $this->copernicaClient->updateCopernicaObject($newsletterPreferencesAdapter->getDatabase(),
            $newsletterPreferencesAdapter->getId(), $newsletterPreferencesAdapter->getIdentifier(),
            $newsletterPreferencesAdapter->createRemoteObject());

        return $this->copernicaClient->put(sprintf('/profile/%d/interests', $externalNewsletterPreferencesId),
            ['data' => $newsletterPreferencesAdapter->createPreferences()]);
    }
}
