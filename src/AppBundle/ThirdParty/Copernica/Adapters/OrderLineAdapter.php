<?php

namespace AppBundle\ThirdParty\Copernica\Adapters;

use AppBundle\Entity\Order\OrderLine;
use AppBundle\ThirdParty\Copernica\Services\CopernicaService;
use libphonenumber\PhoneNumberFormat;

/**
 * Class OrderLineAdapter
 * @package AppBundle\ThirdParty\Copernica\Adapters
 */
class OrderLineAdapter extends AbstractCopernicaAdapter
{
    /**
     * @var OrderLine $orderLine
     */
    private $orderLine;

    /**
     * OrderLineAdapter constructor.
     * @param OrderLine $orderLine
     */
    public function __construct(OrderLine $orderLine)
    {
        $this->orderLine = $orderLine;
        parent::__construct(CopernicaService::DB_ORDERS, $orderLine->getUuid(), 'order_line_id');
    }

    /**
     * @return array
     */
    public function createRemoteObject()
    {
        $profile = [];
        $profile['order_line_id'] = $this->orderLine->getUuid();
        $profile['sku'] = $this->orderLine->getProduct()->getSku();
        $profile['quantity'] = $this->orderLine->getQuantity();
        $profile['delivery_date'] = (null !== $this->orderLine->getOrder()->getDeliveryDate()) ? $this->orderLine->getOrder()->getDeliveryDate()->format('Y-m-d') : '';
        $profile['status'] = $this->orderLine->getOrder()->getStatus()->getId();
        $profile['title'] = $this->orderLine->getTitle();
        $profile['price'] = $this->orderLine->getPrice();
        $profile['site'] = $this->orderLine->getOrder()->getOrderCollection()->getSite()->getUrl();

        $profile['invoice_address_country'] = $this->orderLine->getOrder()->getOrderCollection()->getInvoiceAddressCountry();
        $profile['invoice_address_company_name'] = $this->orderLine->getOrder()->getOrderCollection()->getInvoiceAddressCompanyName();
        $profile['invoice_address_attn'] = $this->orderLine->getOrder()->getOrderCollection()->getInvoiceAddressAttn();
        $profile['invoice_address_street'] = $this->orderLine->getOrder()->getOrderCollection()->getInvoiceAddressStreet();
        $profile['invoice_address_number'] = $this->orderLine->getOrder()->getOrderCollection()->getInvoiceAddressNumber();
        $profile['invoice_address_postcode'] = $this->orderLine->getOrder()->getOrderCollection()->getInvoiceAddressPostcode();
        $profile['invoice_address_city'] = $this->orderLine->getOrder()->getOrderCollection()->getInvoiceAddressCity();
        $profile['invoice_address_number_addition'] = $this->orderLine->getOrder()->getOrderCollection()->getInvoiceAddressNumberAddition();
        $profile['invoice_address_phone_number'] = $this->orderLine->getOrder()->getOrderCollection()->getInvoiceAddressFormattedPhoneNumber(PhoneNumberFormat::E164);

        $profile['delivery_address_country'] = $this->orderLine->getOrder()->getDeliveryAddressCountry();
        $profile['delivery_address_company_name'] = $this->orderLine->getOrder()->getDeliveryAddressCompanyName();
        $profile['delivery_address_attn'] = $this->orderLine->getOrder()->getDeliveryAddressAttn();
        $profile['delivery_address_street'] = $this->orderLine->getOrder()->getDeliveryAddressStreet();
        $profile['delivery_address_number'] = $this->orderLine->getOrder()->getDeliveryAddressNumber();
        $profile['delivery_address_postcode'] = $this->orderLine->getOrder()->getDeliveryAddressPostcode();
        $profile['delivery_address_city'] = $this->orderLine->getOrder()->getDeliveryAddressCity();
        $profile['delivery_address_number_addition'] = $this->orderLine->getOrder()->getDeliveryAddressNumberAddition();
        $profile['delivery_address_phone_number'] = $this->orderLine->getOrder()->getDeliveryAddressPhoneNumber();

        if (!$this->orderLine->getOrder()->getOrderCollection()->getCustomer()) {
            $profile['customer_id'] = null;
            $profile['customer'] = null;
        } else {
            $profile['customer_id'] = $this->orderLine->getOrder()->getOrderCollection()->getCustomer()->getUuid();
            $profile['customer'] = $this->orderLine->getOrder()->getOrderCollection()->getCustomer()->getMetadata()['externalCopernicaCustomerId'] ?? null;
        }

        $profile['order_reference'] = $this->orderLine->getOrder()->getOrderNumber();
        $profile['supplier_remark'] = $this->orderLine->getOrder()->getSupplierRemark();
        $profile['delivery_remark'] = $this->orderLine->getOrder()->getDeliveryRemark();

        $profile['created_at'] = $this->orderLine->getCreatedAt()->format('Y-m-d H:i:s');
        $profile['updated_at'] = $this->orderLine->getUpdatedAt()->format('Y-m-d H:i:s');

        return $profile;
    }
}