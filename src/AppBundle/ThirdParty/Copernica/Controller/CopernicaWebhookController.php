<?php

namespace AppBundle\ThirdParty\Copernica\Controller;

use AppBundle\Entity\Security\Customer\Customer;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Http\Discovery\Exception\NotFoundException;
use Ramsey\Uuid\Exception\InvalidUuidStringException;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CopernicaWebhookController
 * @package AppBundle\ThirdParty\Copernica\Controller
 * @Route("/copernica/webhook")
 */
class CopernicaWebhookController extends Controller
{

    /**
     * @Route("/unsubscribeNewsletter/{customerUuid}")
     * @param string $customerUuid
     * @param EntityManager $entityManager
     * @return Response|string
     */
    public function unsubscribeNewsletter(string $customerUuid, EntityManager $entityManager)
    {
        try {
            $customer = $entityManager->getRepository(Customer::class)->findOneBy(['uuid' => Uuid::fromString($customerUuid)]);
            if($customer === null){
                throw new NotFoundException('Customer not found');
            }
            $customer->setNewsletter(false);
            $entityManager->flush();
            return new Response('Unsubscribe processed', 200);
        } catch (OptimisticLockException | InvalidUuidStringException | NotFoundException $e) {
            return new Response(sprintf('Unsubscribe not successful: %s', $e->getMessage()), 500);
        }
    }

}
