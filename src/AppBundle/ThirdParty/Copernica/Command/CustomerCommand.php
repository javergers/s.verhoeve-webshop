<?php

namespace AppBundle\ThirdParty\Copernica\Command;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\ThirdParty\Copernica\Exceptions\CopernicaNotFoundException;
use AppBundle\ThirdParty\Copernica\Exceptions\CopernicaDuplicateException;
use AppBundle\ThirdParty\Copernica\Services\CopernicaService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CustomerCommand
 * @package AppBundle\ThirdParty\Copernica\Command
 */
class CustomerCommand extends Command
{

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * @var CopernicaService $copernicaService
     */
    private $copernicaService;

    /**
     * Configures command
     */
    protected function configure()
    {
        $this
            ->setName('copernica:update:customer')
            ->setDescription('Command to send new or existing customer to copernica')
            ->addArgument('id', InputArgument::REQUIRED, 'Customer identifier');
    }

    /**
     * CustomerCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param CopernicaService   $copernicaService
     */
    public function __construct(EntityManagerInterface $entityManager, CopernicaService $copernicaService)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->copernicaService = $copernicaService;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws CopernicaDuplicateException
     * @throws CopernicaNotFoundException
     * @throws OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $customerId = (int)$input->getArgument('id');
        $customer = $this->entityManager->getRepository(Customer::class)->find($customerId);

        if (null === $customer) {
            throw new CopernicaNotFoundException(sprintf('Cannot find customer with id %s', $customerId));
        }

        $externalRef = $this->copernicaService->updateCopernicaCustomer($customer);
        $output->write(sprintf('Copernica id %s (internal id %s) has been inserted or updated', $externalRef,
            $customerId));
        return 0;
    }
}
