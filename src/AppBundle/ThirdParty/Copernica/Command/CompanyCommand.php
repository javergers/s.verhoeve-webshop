<?php

namespace AppBundle\ThirdParty\Copernica\Command;

use AppBundle\Entity\Relation\Company;
use AppBundle\ThirdParty\Copernica\Exceptions\CopernicaNotFoundException;
use AppBundle\ThirdParty\Copernica\Exceptions\CopernicaDuplicateException;
use AppBundle\ThirdParty\Copernica\Services\CopernicaService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CompanyCommand
 * @package AppBundle\ThirdParty\Copernica\Command
 */
class CompanyCommand extends Command
{

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * @var CopernicaService $copernicaService
     */
    private $copernicaService;

    /**
     * Configures command
     */
    protected function configure()
    {
        $this
            ->setName('copernica:update:company')
            ->setDescription('Command to send new or existing company to copernica')
            ->addArgument('id', InputArgument::REQUIRED, 'Company identifier');
    }

    /**
     * CompanyCommand constructor.
     * @param EntityManagerInterface $entityManager
     * @param CopernicaService       $copernicaService
     */
    public function __construct(EntityManagerInterface $entityManager, CopernicaService $copernicaService)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->copernicaService = $copernicaService;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws CopernicaDuplicateException
     * @throws CopernicaNotFoundException
     * @throws OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $companyId = (int)$input->getArgument('id');
        $company = $this->entityManager->getRepository(Company::class)->find($companyId);

        if (null === $company) {
            throw new CopernicaNotFoundException(sprintf('Cannot find company with id %s', $companyId));
        }

        $externalRef = $this->copernicaService->updateCopernicaCompany($company);
        $output->write(sprintf('Copernica id %s (internal id %s) has been inserted or updated', $externalRef,
            $companyId));
        return 0;
    }
}
