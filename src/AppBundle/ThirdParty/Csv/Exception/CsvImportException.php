<?php


namespace AppBundle\ThirdParty\Csv\Exception;

use RuntimeException;

/**
 * Class CsvImportException
 * @package AppBundle\Exceptions
 */
class CsvImportException extends RuntimeException
{

}