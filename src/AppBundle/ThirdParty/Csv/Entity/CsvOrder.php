<?php

namespace AppBundle\ThirdParty\Csv\Entity;

use AppBundle\Entity\Order\Cart;
use AppBundle\Entity\Relation\Company;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class CsvOrder
 * @package AppBundle\Entity
 *
 * @ORM\Entity()
 */
class CsvOrder
{
    use TimestampableEntity;

    /**
     * @var int $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Company $company
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Relation\Company")
     */
    protected $company;

    /**
     * @var string $fileName
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $fileName;

    /**
     * @var string $status
     *
     * @ORM\Column(type="string", columnDefinition="ENUM('registered', 'error', 'archive')", options={"default": "registered"})
     */
    protected $status;

    /**
     * @var Cart[] $orders
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Order\Cart", cascade={"persist"})
     */
    protected $carts;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Company $company
     * @return $this
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param $fileName
     * @return $this
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @param $carts []
     * @return $this
     */
    public function setCarts($carts)
    {
        foreach ($carts as $cart) {
            $this->addCart($cart);
        }

        return $this;
    }

    /**
     * @param Cart $cart
     * @return $this
     */
    public function addCart(Cart $cart)
    {
        $this->carts[] = $cart;

        return $this;
    }

    /**
     * @return Cart[]
     */
    public function getCarts()
    {
        return $this->carts;
    }
}
