<?php

namespace AppBundle\ThirdParty\Robin\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table("robin_customer")
 */
class Customer
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Customer\Customer")
     */
    protected $customer;

    /**
     * @ORM\Column(type="json_array")
     */
    protected $data;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $success;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $syncedAt;

    /**
     * Set data
     *
     * @param mixed $data
     *
     * @return Customer
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set customer
     *
     * @param \AppBundle\Entity\Security\Customer\Customer $customer
     *
     * @return Customer
     */
    public function setCustomer(\AppBundle\Entity\Security\Customer\Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \AppBundle\Entity\Security\Customer\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set syncedAt
     *
     * @param \DateTime $syncedAt
     *
     * @return Customer
     */
    public function setSyncedAt($syncedAt)
    {
        $this->syncedAt = $syncedAt;

        return $this;
    }

    /**
     * Get syncedAt
     *
     * @return \DateTime
     */
    public function getSyncedAt()
    {
        return $this->syncedAt;
    }

    /**
     * Set success
     *
     * @param boolean $success
     *
     * @return Customer
     */
    public function setSuccess($success)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * Get success
     *
     * @return boolean
     */
    public function getSuccess()
    {
        return $this->success;
    }
}
