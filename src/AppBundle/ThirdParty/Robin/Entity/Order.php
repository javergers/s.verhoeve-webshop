<?php

namespace AppBundle\ThirdParty\Robin\Entity;

use AppBundle\Entity\Order\Order as OrderEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table("robin_order")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\Order")
     */
    protected $order;

    /**
     * @ORM\Column(type="json_array")
     */
    protected $data;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $success;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $syncedAt;

    /**
     * Set data
     *
     * @param mixed $data
     *
     * @return Order
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set order
     *
     * @param OrderEntity $order
     *
     * @return Order
     */
    public function setOrder(OrderEntity $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return OrderEntity
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set syncedAt
     *
     * @param \DateTime $syncedAt
     *
     * @return Order
     */
    public function setSyncedAt($syncedAt)
    {
        $this->syncedAt = $syncedAt;

        return $this;
    }

    /**
     * Get syncedAt
     *
     * @return \DateTime
     */
    public function getSyncedAt()
    {
        return $this->syncedAt;
    }

    /**
     * Set success
     *
     * @param boolean $success
     *
     * @return Order
     */
    public function setSuccess($success)
    {
        $this->success = $success;

        return $this;
    }

    /**
     * Get success
     *
     * @return boolean
     */
    public function getSuccess()
    {
        return $this->success;
    }
}
