<?php

namespace AppBundle\ThirdParty\Wics\Services;

use AdminBundle\Entity\Settings\WmsOrderType;
use AdminBundle\Filter\PublishableFilter;
use AppBundle\Entity\Carrier\Carrier;
use AppBundle\Entity\Carrier\DeliveryMethod;
use AppBundle\Entity\Catalog\Product\Personalization;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Order\Collo;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderInternalRemark;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\OrderGiftCard;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Entity\Supplier\SupplierOrderLine;
use AppBundle\Manager\Order\OrderLineManager;
use AppBundle\Repository\ProductRepository;
use AppBundle\Services\Designer;
use AppBundle\Services\JobManager;
use AppBundle\Services\Letter;
use AppBundle\Services\Mailer;
use AppBundle\Services\Parameter\ParameterService;
use AppBundle\Services\Sales\Order\OrderActivityService;
use AppBundle\Services\Sales\Order\PackingSlip;
use AppBundle\Manager\StockManager;
use AppBundle\ThirdParty\Wics\Exceptions\WicsException;
use AppBundle\ThirdParty\Wics\Factory\ExportFactory;
use AppBundle\ThirdParty\Wics\Xml\Import\OrderFreezeAdapter;
use AppBundle\ThirdParty\Wics\Xml\Import\ShippingConfirmationAdapter;
use AppBundle\ThirdParty\Wics\Xml\Import\StockCorrectionAdapter;
use AppBundle\ThirdParty\Wics\Xml\Import\StockLevelAdapter;
use AppBundle\ThirdParty\Wics\Xml\Import\StoreConfirmationAdapter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use JMS\JobQueueBundle\Entity\Job;
use League\Flysystem\Adapter\Ftp;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\Filesystem;
use LogicException;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use RuleBundle\Service\ResolverService;
use RuntimeException;
use SimpleXMLElement;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Twig\Environment;

use function abs;
use function array_key_exists;
use function count;
use function in_array;

/**
 * Class WicsService
 * @package AppBundle\ThirdParty\Wics\Services
 */
class WicsService
{
    public const IN_PROCESS_2 = '02'; // Octals
    public const IN_PROCESS_6 = '06'; // Octals
    public const IN_PROCESS_9 = '09'; // Octals
    public const IN_TRANSFER = 13;
    public const LOST_AND_FOUND = 14;
    public const FREE = 31;
    public const DAMAGED = 33;
    public const BLOCKED = 91;

    public const STATUS_NEW = 'new';
    public const STATUS_PENDING = 'pending';
    public const STATUS_PROCESSED = 'processed';
    public const STATUS_SENT = 'sent';
    public const STATUS_COMPLETED = 'complete';

    private const FOLDER_SUCCESS = 'Processed';
    private const FOLDER_ERROR = 'Error';

    /**
     * @var OrderActivityService $orderActivityManager
     */
    private $orderActivityManager;

    /**
     * @var LoggerInterface $logger
     */
    private $logger;

    /**
     * @var Filesystem $fileSystem
     */
    private $fs;

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * @var StockManager $stockManager
     */
    private $stockManager;

    /**
     * @var ParameterService $parameterService
     */
    protected $parameterService;

    /**
     * @var Environment $twig
     */
    private $twig;

    /**
     * @var ResolverService $ruleService
     */
    private $ruleService;

    /**
     * @var Mailer $mailer
     */
    private $mailer;

    /**
     * @var PackingSlip $packingSlip
     */
    private $packingSlip;

    /**
     * @var PublishableFilter
     */
    private $publishableFilter;

    /**
     * @var Letter $letter
     */
    private $letter;

    /**
     * @var Designer $designer
     */
    private $designer;

    /**
     * @var OrderLineManager
     */
    private $orderLineManager;

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * WicsService constructor.
     *
     * @param OrderActivityService $orderActivityManager
     * @param EntityManagerInterface $entityManager
     * @param StockManager $stockManager
     * @param ParameterService $parameterService
     * @param Environment $twig
     * @param ResolverService $ruleService
     * @param Swift_Mailer $mailer
     * @param PackingSlip $packingSlip
     * @param Letter $letter
     * @param Designer $designer
     * @param LoggerInterface $logger
     * @param OrderLineManager $orderLineManager
     * @param JobManager $jobManager
     */
    public function __construct(
        OrderActivityService $orderActivityManager,
        EntityManagerInterface $entityManager,
        StockManager $stockManager,
        ParameterService $parameterService,
        Environment $twig,
        ResolverService $ruleService,
        Swift_Mailer $mailer,
        PackingSlip $packingSlip,
        Letter $letter,
        Designer $designer,
        LoggerInterface $logger,
        OrderLineManager $orderLineManager,
        JobManager $jobManager
    ) {
        $this->orderActivityManager = $orderActivityManager;
        $this->entityManager = $entityManager;
        $this->stockManager = $stockManager;
        $this->parameterService = $parameterService;
        $this->twig = $twig;
        $this->ruleService = $ruleService;
        $this->mailer = $mailer;
        $this->packingSlip = $packingSlip;
        $this->letter = $letter;
        $this->designer = $designer;
        $this->logger = $logger;
        $this->publishableFilter = $entityManager->getFilters()->getFilter('publishable');
        $this->orderLineManager = $orderLineManager;
        $this->jobManager = $jobManager;
    }

    /**
     * @return Filesystem
     */
    private function getFilesystem(): Filesystem
    {
        if (null === $this->fs) {
            $this->fs = new Filesystem(new Ftp([
                'host' => $this->parameterService->setEntity()->getValue('wics_ftp_host'),
                'username' => $this->parameterService->setEntity()->getValue('wics_ftp_username'),
                'password' => $this->parameterService->setEntity()->getValue('wics_ftp_password'),
            ]));
        }

        return $this->fs;
    }

    /**
     * @param      $oldPath
     * @param bool $moveToFolderSuccess (true: file move to FOLDER_SUCCESS, false: file move to FOLDER_ERROR)
     *
     * @throws FileNotFoundException
     */
    public function move($oldPath, $moveToFolderSuccess = true): void
    {
        $pathParts = explode('/', $oldPath);
        $fileName = end($pathParts);
        $pathParts[count($pathParts) - 1] = ($moveToFolderSuccess ? self::FOLDER_SUCCESS : self::FOLDER_ERROR);
        $pathParts[] = $fileName;

        $newPath = implode('/', $pathParts);

        if ($this->getFilesystem()->has($newPath)) {
            $this->getFilesystem()->delete($newPath);
        }

        try {
            $this->getFilesystem()->rename($oldPath, $newPath);
        } catch (FileExistsException $e) {
            // Shouldn't happen because the file will be deleted first
            $this->logger->error($e->getMessage(), ['exception' => $e]);
        }
    }

    /**
     * @param string $path
     *
     * @return bool|false|string
     * @throws FileNotFoundException
     */
    public function read($path)
    {
        return $this->getFilesystem()->read($path);
    }

    /**
     * @param string $path
     * @param string $data
     * @param bool $overwrite
     *
     * @return bool
     * @throws FileExistsException
     * @throws FileNotFoundException
     */
    public function write($path, $data, $overwrite = false): bool
    {
        if (true === $overwrite && $this->getFilesystem()->has($path)) {
            $this->getFilesystem()->delete($path);
        }
        return $this->getFilesystem()->write($path, $data);
    }

    /**
     * @param OrderFreezeAdapter $orderFreeze
     *
     * @throws WicsException
     */
    public function handleOrderFreeze(OrderFreezeAdapter $orderFreeze): void
    {
        $this->publishableFilter->disableForEntity(Product::class);

        $ordernummer = $orderFreeze->getOrderNumber();

        $order = $this->entityManager->getRepository(Order::class)->findOneByOrderNumber($ordernummer);
        if (null === $order) {
            throw new WicsException(sprintf('Order %s not found', $ordernummer));
        }

        if (!in_array($order->getStatus(), [self::STATUS_NEW, self::STATUS_PENDING],
            false)) {// @todo: workflow guard refactor
            throw new WicsException('Could not process order', 1);
        }

        $order->setStatus(self::STATUS_PROCESSED);// @todo: workflow refactor

        $this->entityManager->flush();
    }

    /**
     * @param ShippingConfirmationAdapter $shippingConfirmation
     *
     * @throws WicsException
     * @throws Exception
     */
    public function handleShippingConfirmation(ShippingConfirmationAdapter $shippingConfirmation): void
    {
        $this->publishableFilter->disableForEntity(Product::class);

        $orderStatuses = $shippingConfirmation->getOrderStatuses();

        $orderRepository = $this->entityManager->getRepository(Order::class);
        $productRepository = $this->entityManager->getRepository(Product::class);
        $orderGiftcardRepository = $this->entityManager->getRepository(OrderGiftCard::class);
        $colloRepository = $this->entityManager->getRepository(Collo::class);

        foreach ($orderStatuses as $orderStatus) {
            $externalReference = (string)$orderStatus->xpath('ExternalReference')[0];

            /** @var Order $order */
            $order = $orderRepository->findOneByOrderNumber($externalReference);
            if (null === $order) {
                throw new WicsException(sprintf('Order %s not found', $externalReference));
            }

            $supplierOrder = $order->getSupplierOrder();
            if ($order->getStatus() === self::STATUS_COMPLETED || (null !== $supplierOrder && in_array($supplierOrder->getStatus(),
                        ['sent', 'completed']))) {
                throw new WicsException(sprintf('order %s already completed', $externalReference));
            }

            foreach ($orderStatus->xpath('SerialNos') as $giftCardNumber) {
                $orderGiftCard = [
                    'number' => $giftCardNumber,
                    'order' => $externalReference,
                ];

                /** @var OrderLine $line */
                foreach ($order->getLines() as $line) {
                    $product = $line->getProduct();

                    $orderGiftCard['product'] = $productRepository->find($product);
                    $orderGiftCard['value'] = $product->getSku();
                }

                $orderGiftcardRepository->findOneOrCreate($orderGiftCard);
            }

            $tntCodes = [];

            foreach ($orderStatus->xpath('orderStatusLine') as $line) {
                if (empty($trackAndTraceCode = (string)$line->xpath('TrackAndTraceCode')[0])
                    || in_array($trackAndTraceCode, $tntCodes, true)) {
                    continue;
                }

                $tntCodes[] = $trackAndTraceCode;
            }

            foreach ($tntCodes as $tnt) {
                $colloRepository->findOneOrCreate([
                    'order' => $order,
                    'code' => $tnt,
                ]);
            }

            $this->checkOrderShipping($supplierOrder, $orderStatus);
            $order->setStatus(self::STATUS_SENT);// @todo: workflow refactor
            $supplierOrder->setStatus('sent');

            $this->orderActivityManager->add('order_sent', $order);

            $job = new Job('app:mailer:send-delivery-status', [
                $order->getId(),
            ], true, 'order_delivery_status_mail');

            $this->jobManager->addJob($job);
        }

        $this->entityManager->flush();
    }

    /**
     * @param StockCorrectionAdapter $stockCorrection
     *
     * @throws InvalidArgumentException
     * @throws OptimisticLockException
     */
    public function handleStockCorrection(StockCorrectionAdapter $stockCorrection): void
    {
        $this->publishableFilter->disableForEntity(Product::class);

        /** @var ProductRepository $productRepository */
        $productRepository = $this->entityManager->getRepository(Product::class);

        foreach ($stockCorrection->getCorrectionLines() as $line) {
            $sku = (string)$line->itemNo;

            /** @var Product $product */
            $product = $productRepository->findOneBySku($sku);

            if (null === $product) {
                continue;
            }

            if (!$product->getHasStock()) {
                throw new UnprocessableEntityHttpException(sprintf("Couldn't process product %s since stock is not applied",
                    $product->getSku()));
            }

            switch ((int)$line->Voorraadstatus) {
                case self::IN_TRANSFER:
                case self::FREE:
                    $qty = (int)$line->quantity;
                    $absQty = abs($qty);

                    if ($qty > 0) {
                        $this->stockManager->addPhysicalStock($product, $absQty);
                    } elseif ($qty < 0) {
                        $this->stockManager->subtractPhysicalStock($product, $absQty);
                    }

                    $this->logger->debug(sprintf('Corrected stock for product %s with delta: %d', $sku, $qty));

                    break;
            }

        }

        $this->entityManager->flush();
    }

    /**
     * @param StockLevelAdapter $stockLevel
     *
     * @throws InvalidArgumentException
     * @throws OptimisticLockException
     */
    public function handleStockLevel(StockLevelAdapter $stockLevel): void
    {
        $this->publishableFilter->disableForEntity(Product::class);

        $stockMap = [];
        $productRepository = $this->entityManager->getRepository(Product::class);

        foreach ($stockLevel->getStockUpdateLines() as $line) {
            $sku = (string)$line->ItemNum;
            $product = $productRepository->findOneBySku($sku);

            if (null === $product) {
                continue;
            }

            $productId = $product->getId();

            if (!isset($stockMap[$productId])) {
                $stockMap[$productId] = [
                    'product' => $product,
                    'quantity' => 0,
                ];
            }

            $stockMap[$productId]['quantity'] += (int)$line->Quantity;
        }

        foreach ($stockMap as $array) {
            $this->stockManager->setPhysicalStock($array['product'], $array['quantity']);
        }

        $this->entityManager->flush();

        $this->logger->info('WICS import stock level complete');
    }

    /**
     * todo: (on-hold)
     *
     * @param StoreConfirmationAdapter $storeConfirmation
     *
     * @throws WicsException
     */
    public function handleStoreConfirmation(StoreConfirmationAdapter $storeConfirmation): void
    {
        void($storeConfirmation);

        throw new WicsException('Not implemented');
    }

    /**
     * @param Product $product
     *
     * @throws FileExistsException
     * @throws FileNotFoundException
     * @throws OptimisticLockException
     * @throws ORMException
     * @throws Exception
     */
    public function exportProduct(Product $product): void
    {
        if ($product->isCombination()) {
            $this->exportCombinationProduct($product);
            return;
        }

        if (false === $product->getHasStock()) {
            throw new LogicException('Product must have stock');
        }

        $factoryAdapter = ExportFactory::createItemAdapter($product, $this->entityManager);

        $this->write($factoryAdapter->getPath('xml'), $factoryAdapter->generateXml());
    }

    /**
     * @param Product $product
     *
     * @throws FileExistsException
     * @throws FileNotFoundException
     * @throws OptimisticLockException
     * @throws ORMException
     * @throws Exception
     */
    private function exportCombinationProduct(Product $product): void
    {
        if (null === $product->getCombinations()) {
            throw new LogicException('Product is not a combination product');
        }

        if (null === $product->getParent()) {
            throw new LogicException('Product must be a variation');
        }

        $combinationHasStock = $product->getCombinations()->forAll(function ($k, $combination) {
            return $combination->getProduct()->getHasStock();
        });

        if (false === $combinationHasStock) {
            throw new RuntimeException('Some products in the combination have stock management disabled.');
        }

        $factoryAdapter = ExportFactory::createCombiItemAdapter($product, $this->entityManager);

        $this->write($factoryAdapter->getPath('xml'), $factoryAdapter->generateXml());
    }

    /**
     * Todo: handle when available
     * @throws WicsException
     */
    public function exportPreAlert(): void
    {
        throw new WicsException('Not implemented');
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @param string $packingslipPaperSize
     *
     * @throws FileExistsException
     * @throws FileNotFoundException
     * @throws OptimisticLockException
     * @throws WicsException
     * @throws ORMException
     * @throws Exception
     */
    public function exportOrder(SupplierOrder $supplierOrder, $packingslipPaperSize = 'A5'): void
    {
        $factoryAdapter = ExportFactory::createOrderAdapter($this, $supplierOrder, $this->entityManager);

        if ($this->write($factoryAdapter->getPath(), $factoryAdapter->generateXml())) {
            /** @var SupplierOrderLine $line */
            foreach ($supplierOrder->getLines() as $line) {

                $concludedProduct = $line->getConcludedSupplierProduct()->getProduct();
                $metaData = $line->getOrderLine()->getMetadata();
                if ($metaData === null || empty($metaData)) {
                    continue;
                }

                //is_card
                if ($concludedProduct->isCard()) {
                    $this->packingSlip->setItem($supplierOrder->getOrder());
                    $this->packingSlip->setPaperSize($packingslipPaperSize);

                    $attachmentType = $this->getAttachmentType($concludedProduct, 'CRD');

                    $this->write($factoryAdapter->getAttachmentPath('pdf', $supplierOrder->getOrder()->getOrderNumber(), $attachmentType),
                        $this->packingSlip->getContents(), true);
                }

                //letter
                if ($concludedProduct->isLetter()) {
                    if(isset($metaData['letter_url'])){
                        $letterUrl = $metaData['letter_url'];
                    } else {
                        $this->letter->setUuid($metaData['letter']['uuid']);
                        $letterUrl = $this->letter->getUrl();
                    }

                    $attachmentType = $this->getAttachmentType($concludedProduct, 'LTR');

                    $this->write($factoryAdapter->getAttachmentPath('pdf', $supplierOrder->getOrder()->getOrderNumber(), $attachmentType),
                        file_get_contents($letterUrl), true);
                }

                //designer
                if ($concludedProduct->isPersonalization()) {
                    $printUrl = $this->orderLineManager->getDesignPrintUrl($line->getOrderLine());

                    $attachmentType = $this->getAttachmentType($concludedProduct, null);
                    if ($attachmentType === null) {
                        throw new WicsException(vsprintf('No WICS code set for product id: %d, and/or productgroup id: %d',
                                [
                                    $concludedProduct->getId(),
                                    $concludedProduct->getProductgroup() ? $concludedProduct->getProductgroup()->getId() : 0,
                                ])
                        );
                    }

                    $this->write($factoryAdapter->getAttachmentPath('pdf', $supplierOrder->getOrder()->getOrderNumber(), $attachmentType),
                        $this->designer->getPdf($printUrl), true);
                }
            }
        }

        $supplierOrder->getOrder()->setStatus(self::STATUS_PENDING);
    }

    /**
     * @param Product $product
     * @param string $fallback
     *
     * @return string|null
     */
    private function getAttachmentType(Product $product, $fallback = ''): ?string
    {
        $attachmentType = null;
        try {
            $attachmentType = $this->parameterService->setEntity($product)->getValue('wics_document_type');
        } catch (RuntimeException $e) {
            //Just try the next one
        }

        try {
            if ($attachmentType === null) {
                $attachmentType = $this->parameterService->setEntity($product->getProductgroup())->getValue('wics_document_type');
            }
        } catch (RuntimeException $e) {
            //Just fallback
        }

        return $attachmentType ? strtoupper($attachmentType) : $fallback;
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @throws FileExistsException
     * @throws FileNotFoundException
     * @throws OptimisticLockException
     * @throws ORMException
     * @throws Exception
     */
    public function exportCancelOrder(SupplierOrder $supplierOrder): void
    {
        if ($supplierOrder->getStatus() !== self::STATUS_NEW) {
            $factoryAdapter = ExportFactory::createCancelOrderAdapter($this, $supplierOrder, $this->entityManager);
            $this->write($factoryAdapter->getPath('xml'), $factoryAdapter->generateXml());
        }
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @return string
     * @throws Exception
     */
    public function getOrderTypeCode(SupplierOrder $supplierOrder): string
    {
        $qb = $this->entityManager->getRepository(WmsOrderType::class)->createQueryBuilder('WMS')
            ->orderBy('WMS.priority', 'DESC');

        $orderTypes = $qb->getQuery()->getResult();
        $order = $supplierOrder->getOrder();

        /**
         * @var WmsOrderType $orderType
         */
        foreach ($orderTypes as $orderType) {
            if ($this->ruleService->satisfies($order, $orderType->getRule())) {
                return $orderType->getCode();
            }
        }

        return $this->parameterService->setEntity()->getValue('wms_order_type_fallback');
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @param SimpleXMLElement $orderStatus
     */
    private function checkOrderShipping(SupplierOrder $supplierOrder, SimpleXMLElement $orderStatus): void
    {
        $orderLineErrors = [];

        /** @var SupplierOrderLine $orderLine */
        foreach ($supplierOrder->getLines() as $orderLine) {
            foreach ($orderStatus->xpath('//orderStatusLine') as $line) {
                if ($orderLine->getQuantity() === (int)$line->xpath('//quantity')[0]) {
                    continue;
                }

                $product = $orderLine->getOrderLine()->getProduct();

                if ($product instanceof TransportType) {
                    continue;
                }

                $orderLineErrors[] = [
                    'productSku' => $product->getSku(),
                    'productTitle' => $product->translate()->getTitle(),
                    'expectedQuantity' => $orderLine->getQuantity(),
                    'givenQuantity' => (int)$line->xpath('//quantity')[0],
                ];
            }
        }

        if (count($orderLineErrors) > 0) {
            // Add internal remark to the order
            $internalRemark = new OrderInternalRemark();
            $internalRemark
                ->setRemark('Deze order is incompleet naar de klant verzonden.' . PHP_EOL
                    . '(De aantallen in de order en de verzendbevestiging vanuit WICS WMS '
                    . 'komen niet met elkaar overeen.)');

            $this->entityManager->persist($internalRemark);

            $supplierOrder->getOrder()->addInternalRemark($internalRemark);

            // Notify customer service by e-mail
            try {
                $view = $this->twig->render('@App/mail/wics/incomplete-order.html.twig',
                    [
                        'orderNumber' => $supplierOrder->getOrder()->getOrderNumber(),
                        'errors' => $orderLineErrors,
                    ]);

                $message = (new Swift_Message())
                    ->setSubject(sprintf('Order  %s is niet compleet naar de klant verzonden',
                        $supplierOrder->getOrder()->getOrderNumber()))
                    ->setFrom($this->parameterService->setEntity()->getValue('wics_incomplete_order_from'))
                    ->setTo($this->parameterService->setEntity()->getValue('wics_incomplete_order_to'))
                    ->setBody($view, 'text/html');

                $this->mailer->send($message);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), ['exception' => $e]);
            }
        }
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @return TransportType
     * @throws WicsException
     */
    private function getTransportTypeFromSupplierOrder(SupplierOrder $supplierOrder)
    {
        /** @var TransportType $transportType */
        $transportType = null;
        /** @var SupplierOrderLine $line */
        foreach ($supplierOrder->getLines() as $line) {
            if ($line->getOrderLine()->getProduct() instanceof TransportType) {
                $transportType = $line->getOrderLine()->getProduct();
                break;
            }
        }

        if ($transportType === null) {
            throw new WicsException(sprintf('No TransportType found on SupplierOrder %d', $supplierOrder->getId()));
        }

        return $transportType;
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @return Carrier
     * @throws WicsException
     */
    public function getCarrierFromSupplierOrder(SupplierOrder $supplierOrder)
    {
        /** @var TransportType $transportType */
        $transportType = $this->getTransportTypeFromSupplierOrder($supplierOrder);

        //Find by preferred carrier
        if ($transportType->getPreferredCarrier() !== null) {
            return $transportType->getPreferredCarrier();
        }

// TODO: Implement linking of carriers to suppliers
//        $supplierCarriers = $supplierOrder->getSupplier()->getCarriers();

        //Find by preferred delivery method and supplier
        if (($preferredDeliveryMethod = $transportType->getPreferredDeliveryMethod()) !== null) {
            /** @var Carrier $carrier */
            foreach ($preferredDeliveryMethod->getCarriers() as $carrier) {
//                if ($supplierCarriers->contains($carrier)) {
                    return $carrier;
//                }
            }
        }

        //Find by supplier
        $carrierRepository = $this->entityManager->getRepository(Carrier::class);
        $carrierByCompany = $carrierRepository->findBy(['company' => $supplierOrder->getSupplier()->getId()]);
        if (count($carrierByCompany) > 0) {
            return $carrierByCompany[0];
        }

        return null;
    }

    /**
     * @param SupplierOrder $supplierOrder
     *
     * @return DeliveryMethod
     * @throws WicsException
     */
    public function getDeliveryMethodFromSupplierOrder(SupplierOrder $supplierOrder)
    {
        /** @var TransportType $transportType */
        $transportType = $this->getTransportTypeFromSupplierOrder($supplierOrder);

        //Find by preferred carrier
        if ($transportType->getPreferredDeliveryMethod() !== null) {
            return $transportType->getPreferredDeliveryMethod();
        }

        //TODO Are there extra business rules for finding the correct DeliveryMethod from a Supplier

        return null;
    }

    /**
     * @param Carrier $carrier
     *
     * @return string
     */
    public function getWicsCarrierId(?Carrier $carrier)
    {
        if ($carrier === null) {
            return '';
        }
        return $this->parameterService->setEntity($carrier)->getValue('wics_carrier_id') ?: '';
    }

    /**
     * @param DeliveryMethod $deliveryMethod
     *
     * @return string
     */
    public function getWicsCarrierOption(?DeliveryMethod $deliveryMethod)
    {
        if ($deliveryMethod === null) {
            return '';
        }
        return $this->parameterService->setEntity($deliveryMethod)->getValue('wics_carrier_option') ?: '';
    }

    /**
     * @return string
     */
    public function getWicsPickupCarrierId()
    {
        return $this->parameterService->setEntity()->getValue('wics_pickup_carrier_id') ?: '';
    }

    /**
     * @return string
     */
    public function getWicsPickupCarrierOption()
    {
        return $this->parameterService->setEntity()->getValue('wics_pickup_carrier_option') ?: '';
    }
}
