<?php

namespace AppBundle\ThirdParty\Wics\EventListener;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductPackagingUnit;
use AppBundle\Entity\Catalog\Product\ProductTranslation;
use AppBundle\Entity\Relation\Company;
use AppBundle\Services\JobManager;
use AppBundle\Services\SupplierManagerService;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\UnitOfWork;
use JMS\JobQueueBundle\Entity\Job;

/**
 * Class ProductListener
 * @package AppBundle\ThirdParty\Wics\EventListener
 */
class ProductListener implements EventSubscriber
{
    /** @var JobManager $jobManager */
    private $jobManager;

    /** @var SupplierManagerService $supplierManager */
    private $supplierManager;

    /** @var array $productsToUpdate */
    private $productsToUpdate;

    /**
     * ProductListener constructor.
     * @param JobManager             $jobManager
     * @param SupplierManagerService $supplierManager
     */
    public function __construct(JobManager $jobManager, SupplierManagerService $supplierManager)
    {
        $this->jobManager = $jobManager;
        $this->supplierManager = $supplierManager;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'preUpdate',
            'postPersist',
            'postUpdate',
        ];
    }

    /** @var array */
    private const ENTITY_WHITELIST = [
        ProductPackagingUnit::class,
        ProductTranslation::class,
    ];

    /**
     * @param $entity
     * @return Product|null
     */
    private function getProductFromWhitelistedEntity($entity): ?Product
    {
        if ($entity instanceof Product) {
            return $entity;
        }

        if (\is_callable([$entity, 'getProduct']) && \in_array(\get_class($entity), self::ENTITY_WHITELIST, true)) {
            return $entity->getProduct();
        }

        return null;
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postPersist(LifecycleEventArgs $eventArgs)
    {
        $entity = $eventArgs->getEntity();
        $product = $this->getProductFromWhitelistedEntity($entity);

        if (null !== $product) {
            $this->handleWicsProduct($product);
        }
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     */
    public function preUpdate(LifecycleEventArgs $eventArgs)
    {
        /** @var UnitOfWork $uow */
        $uow = $eventArgs->getEntityManager()->getUnitOfWork();
        $this->productsToUpdate = [];

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            $product = $this->getProductFromWhitelistedEntity($entity);

            if (null !== $product) {
                $this->productsToUpdate[] = $product;
            }
        }
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Exception
     */
    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        foreach (array_unique($this->productsToUpdate) as $product) {
            if (false !== $product) {
                $this->handleWicsProduct($product);
            }
        }
    }

    /**
     * @param Product $product
     * @throws \Exception
     */
    private function handleWicsProduct(Product $product)
    {
        if (!$product->getId() || null === $product->getParent()) {
            return;
        }
        $suppliers = $this->supplierManager->getAvailableSuppliersForProduct($product);
        $suppliers = $suppliers->filter(function(Company $supplier){
            return $supplier->getSupplierConnector() === 'Wics';
        });
        if ($suppliers->isEmpty()) {
            return;
        }

        $job = new Job('wics:export:product', [$product->getId()]);
        $job->addRelatedEntity($product);

        $this->jobManager->addJob($job);
    }
}
