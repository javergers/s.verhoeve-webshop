<?php

namespace AppBundle\ThirdParty\Wics\Factory;

use AppBundle\ThirdParty\Wics\Interfaces\ExportXmlInterface;
use AppBundle\ThirdParty\Wics\Services\WicsService;
use AppBundle\ThirdParty\Wics\Xml\Export\CancelOrderAdapter;
use AppBundle\ThirdParty\Wics\Xml\Export\CombiItemAdapter;
use AppBundle\ThirdParty\Wics\Xml\Export\ItemAdapter;
use AppBundle\ThirdParty\Wics\Xml\Export\OrderAdapter;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ExportFactory
 */
class ExportFactory
{
    public const ITEM = 'ImportItem';
    public const PRE_ALERT = 'ImportPreAlert';
    public const ORDER = 'ImportOrder';
    public const CANCEL_ORDER = 'ImportCancelOrder';

    /**
     * @todo: remove function?
     *
     * @param string        $type
     * @param object        $object
     *
     * @param EntityManagerInterface $em
     * @return ExportXmlInterface
     * @throws \RuntimeException
     * @throws \Exception
     */
    public static function create(string $type, object $object, EntityManagerInterface $em): ExportXmlInterface
    {
        switch ($type) {
            case self::ITEM:
                return self::createItemAdapter($object, $em);
            case self::PRE_ALERT:
                return self::createPreAlertAdapter($object, $em);
/*
            case self::ORDER:
                return self::createOrderAdapter($object, $em);
            case self::CANCEL_ORDER:
                return self::createCancelOrderAdapter($object, $em);
*/
            default:
                throw new \RuntimeException('Unknown type');
        }
    }

    /**
     * @param object        $object
     * @param EntityManagerInterface $em
     * @return ItemAdapter
     * @throws \Exception
     */
    public static function createItemAdapter(object $object, EntityManagerInterface $em): ItemAdapter
    {
        $adapter = new ItemAdapter($object);
        $adapter->setEntityManager($em);

        return $adapter;
    }

    /**
     * @param object        $object
     * @param EntityManagerInterface $em
     * @return CombiItemAdapter
     * @throws \Exception
     */
    public static function createCombiItemAdapter(object $object, EntityManagerInterface $em): CombiItemAdapter
    {
        $adapter = new CombiItemAdapter($object);
        $adapter->setEntityManager($em);

        return $adapter;
    }

    /**
     * @param object        $object
     * @param EntityManagerInterface $em
     * @return bool
     * @throws \RuntimeException
     */
    public static function createPreAlertAdapter(object $object, EntityManagerInterface $em)
    {
        throw new \RuntimeException('Not implemented');
        // Todo: create PreAlertAdapter

        return false;
    }

    /**
     * @param WicsService   $wics
     * @param object        $object
     * @param EntityManagerInterface $em
     * @return OrderAdapter
     * @throws \Exception
     */
    public static function createOrderAdapter(WicsService $wics, object $object, EntityManagerInterface $em): OrderAdapter
    {
        $adapter = new OrderAdapter($object);
        $adapter->setWics($wics);
        $adapter->setEntityManager($em);

        return $adapter;
    }

    /**
     * @param WicsService   $wics
     * @param object        $object
     * @param EntityManagerInterface $em
     * @return CancelOrderAdapter
     * @throws \Exception
     */
    public static function createCancelOrderAdapter(WicsService $wics, object $object, EntityManagerInterface $em): CancelOrderAdapter
    {
        $adapter = new CancelOrderAdapter($object);
        $adapter->setWics($wics);
        $adapter->setEntityManager($em);

        return $adapter;
    }
}
