<?php

namespace AppBundle\ThirdParty\Wics\Factory;

use AppBundle\ThirdParty\Wics\Interfaces\ImportXmlInterface;
use AppBundle\ThirdParty\Wics\Xml\Import\OrderFreezeAdapter;
use AppBundle\ThirdParty\Wics\Xml\Import\ShippingConfirmationAdapter;
use AppBundle\ThirdParty\Wics\Xml\Import\StockCorrectionAdapter;
use AppBundle\ThirdParty\Wics\Xml\Import\StockLevelAdapter;
use AppBundle\ThirdParty\Wics\Xml\Import\StoreConfirmationAdapter;

/**
 * Class ImportFactory
 */
class ImportFactory
{
    public const ORDER_FREEZE = 'ExportOrderFreeze';
    public const SHIPPING_CONFIRMATION = 'ExportShippingConfirmation';
    public const STOCK_CORRECTION = 'ExportStockCorrection';
    public const STOCK_LEVEL = 'ExportStockLevel';
    public const STORE_CONFIRMATION = 'ExportStoreConfirmation';

    /**
     * @param $type
     * @param string $xml
     *
     * @return ImportXmlInterface
     * @throws \Exception
     */
    public static function create($type, string $xml)
    {
        switch ($type) {
            case self::ORDER_FREEZE:
                return self::createOrderFreeze($xml);
            case self::SHIPPING_CONFIRMATION:
                return self::createShippingConfirmation($xml);
            case self::STOCK_CORRECTION:
                return self::createStockCorrection($xml);
            case self::STOCK_LEVEL:
                return self::createStockLevel($xml);
            case self::STORE_CONFIRMATION:
                return self::createStoreConfirmation($xml);
            default:
                throw new \Exception('Unknown type');
        }
    }

    /**
     * @param string $xml
     * @return OrderFreezeAdapter
     */
    public static function createOrderFreeze(string $xml)
    {
        return new OrderFreezeAdapter($xml);
    }

    /**
     * @param string $xml
     * @return ShippingConfirmationAdapter
     */
    public static function createShippingConfirmation(string $xml)
    {
        return new ShippingConfirmationAdapter($xml);
    }

    /**
     * @param string $xml
     * @return StockCorrectionAdapter
     */
    public static function createStockCorrection(string $xml)
    {
        return new StockCorrectionAdapter($xml);
    }

    /**
     * @param string $xml
     * @return StockLevelAdapter
     */
    public static function createStockLevel(string $xml)
    {
        return new StockLevelAdapter($xml);
    }

    /**
     * @param string $xml
     * @return StoreConfirmationAdapter
     */
    public static function createStoreConfirmation(string $xml)
    {
        return new StoreConfirmationAdapter($xml);
    }
}
