<?php

namespace AppBundle\ThirdParty\Wics\Interfaces;

/**
 * Interface ExportXmlInterface
 * @package AppBundle\ThirdParty\Wics\Interfaces
 */
interface ExportXmlInterface
{
    /**
     * ExportXmlInterface constructor.
     * @param $object
     */
    public function __construct($object);

    /**
     * @return string
     */
    public function generateXml();
}
