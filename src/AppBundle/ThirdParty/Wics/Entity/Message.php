<?php

namespace AppBundle\ThirdParty\Wics\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Class WicsMessage
 * Todo: Create a command to repeat WicsMessage actions
 *
 * @package AppBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table("wics_message")
 */
class Message
{
    use TimestampableEntity;

    /**
     * @var int $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $xml
     *
     * @ORM\Column(type="blob", nullable=true)
     */
    protected $xml;

    /**
     * @var string $type
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $type;

    /**
     * @var bool $status
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $status;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $xml
     * @return $this
     */
    public function setXml(string $xml)
    {
        $this->xml = $xml;

        return $this;
    }

    /**
     * @return string
     */
    public function getXml()
    {
        return $this->xml;
    }

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $status
     * @return Message
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }
}
