<?php

namespace AppBundle\ThirdParty\Wics\Xml\Import;

use AppBundle\ThirdParty\Wics\Interfaces\ImportXmlInterface;

/**
 * Class StockLevelAdapter
 * @package AppBundle\ThirdParty\Wics\Xml\Import
 */
class StockLevelAdapter extends AbstractAdapter implements ImportXmlInterface
{
    /**
     * @return \SimpleXMLElement[]
     */
    public function getStockUpdateLines()
    {
        return $this->xml->xpath("//StockLevel");
    }
}
