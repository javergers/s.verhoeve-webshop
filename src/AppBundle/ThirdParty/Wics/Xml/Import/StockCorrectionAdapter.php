<?php

namespace AppBundle\ThirdParty\Wics\Xml\Import;

use AppBundle\ThirdParty\Wics\Interfaces\ImportXmlInterface;

/**
 * Class StockCorrectionAdapter
 * @package AppBundle\ThirdParty\Wics\Xml\Import
 */
class StockCorrectionAdapter extends AbstractAdapter implements ImportXmlInterface
{
    /**
     * @return \SimpleXMLElement[]
     */
    public function getCorrectionLines()
    {
        return $this->xml->xpath("//StockCorrectionLine");
    }
}
