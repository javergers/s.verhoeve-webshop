<?php

namespace AppBundle\ThirdParty\Wics\Xml\Export;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductPackagingUnit;
use AppBundle\Entity\Supplier\SupplierProduct;
use AppBundle\ThirdParty\Wics\Interfaces\ExportXmlInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Sabre\Xml as Sabre;

/**
 * Class ItemAdapter
 * @package AppBundle\ThirdParty\Wics\Xml\Export
 */
class ItemAdapter extends AbstractAdapter implements ExportXmlInterface
{
    /**
     * @var Product $product
     */
    private $product;

    /**
     * @var string $type
     */
    protected $type = 'Item';

    /**
     * ItemAdapter constructor.
     * @param Product $object
     * @throws \Exception
     */
    public function __construct($object)
    {
        parent::__construct();

        if (!$object instanceof Product) {
            throw new \RuntimeException('Invalid request');
        }

        $this->product = $object;
    }

    /**
     * @return string
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function generateXml()
    {
        $xml = $this->sabre->write('Message', function (Sabre\Writer $writer) {
            $this->generateXmlHeader($writer);

            $writer->writeElement('Item', function () use ($writer) {
                /** @var SupplierProduct $supplierProduct */
                $supplierProduct = $this->product->getSupplierProducts()->current();
                $itemDescription = $this->product->translate()->getTitle();

                if (false !== $supplierProduct && null !== $supplierProduct && null !== ($supplierProductTitle = $supplierProduct->translate()->getTitle())) {
                    $itemDescription = $supplierProductTitle;
                }

                $writer->writeElement('ItemNum', $this->product->getSku());
                $writer->writeElement('ItemDesc', $itemDescription);
                $writer->writeElement('SupplierCode', $this->product->getSku());

                if (null === $this->product->getProductPackagingUnits()) {
                    throw new \RuntimeException(sprintf('No stock units found for product %s',
                        $this->product->translate()->getTitle()));
                }

                /** @var ProductPackagingUnit[] $packagingUnits */
                $packagingUnits = $this->product->getProductPackagingUnits()->toArray();

                if (empty($packagingUnits)) {
                    throw new \RuntimeException('Empty stock unit array');
                }

                usort($packagingUnits, function (ProductPackagingUnit $a, ProductPackagingUnit $b) {
                    return $a->getQuantity() <=> $b->getQuantity();
                });

                $stockUnitCode = null;
                $weight = null;
                /** @var ProductPackagingUnit $packageingUnit */
                if (null !== ($packageingUnit = $packagingUnits[0])) {
                    $stockUnitCode = $packageingUnit->getPackagingUnit()->getCode();
                    $weight = $packageingUnit->getWeight();
                }

                if (null === $stockUnitCode) {
                    throw new \RuntimeException('No stockunit defined');
                }

                if (null === $weight) {
                    $weight = 1;
                }

                $writer->writeElement('StockUnit', $stockUnitCode);
                $writer->writeElement('ItemWeight', $weight/1000);

                $writer->writeElement('ProductGroup',
                    (null !== ($productGroup = $this->product->getProductgroup()) ? $productGroup->getSkuPrefix() : ''));

                $bbeInfo = $this->getProductBBEInfo();

                $writer->writeElement('BBE', function () use ($writer, $bbeInfo) {
                    $writer->writeElement('BBEItem', $bbeInfo['BBEItem']);
                    $writer->writeElement('BBEPeriod', $bbeInfo['BBEPeriod']);
                    $writer->writeElement('BBEGuarantee', $bbeInfo['BBEGuarantee']);
                });

                $writer->writeElement('OutboundAlgorithm', $bbeInfo['OutboundAlgorithm']);

                $writer->writeElement('CountryOfOrigin', 'NL');
                $writer->writeElement('SuggestedRetailPrice', $this->product->getPrice());
                $writer->writeElement('ItemUnits', function () use ($writer) {

                    /**
                     * @var ProductPackagingUnit $unit
                     */
                    foreach ($this->product->getProductPackagingUnits() as $unit) {
                        $writer->writeElement('ItemUnit', function () use ($writer, $unit) {
                            $writer->writeElement('UnitMeasure', $unit->getPackagingUnit()->getCode());
                            $writer->writeElement('ItemUnitHeight', $unit->getDimensionsHeight());
                            $writer->writeElement('ItemUnitWidth', $unit->getDimensionsWidth());
                            $writer->writeElement('ItemUnitDepth', $unit->getDimensionsLength());
                            $writer->writeElement('skuInHU', $unit->getQuantity());
                            $writer->writeElement('EanDunCode', $this->product->getEan());
                        });
                    }
                });

                $serialNumber = $this->product->getSerialNumberMeta();
                if (null !== $serialNumber) {
                    $writer->writeElement('SerialNumber', function () use ($writer, $serialNumber) {
                        $writer->writeElement('seqNo', 10);
                        $writer->writeElement('scan', 'yes');
                        $writer->writeElement('level', $serialNumber->getLevel());
                        $writer->writeElement('FixSer', $serialNumber->getFixSer());
                        $writer->writeElement('mask', $serialNumber->getMask());
                    });
                }
            });
        });

        $this->setMessageXml($xml);

        return $xml;
    }

    /**
     * @return array
     */
    private function getProductBBEInfo()
    {
        $bbeInfo = [
            'BBEItem' => 'n',
            'BBEPeriod' => 0,
            'BBEGuarantee' => 0,
            'OutboundAlgorithm' => 1,
        ];

        $expiration = $this->product->getExpirationMeta();
        if (null !== $expiration) {
            $bbeInfo['BBEItem'] = 'j';
            $bbeInfo['BBEPeriod'] = $expiration->getPeriod();
            $bbeInfo['BBEGuarantee'] = $expiration->getGuarantee();
            $bbeInfo['OutboundAlgorithm'] = 2;
        }

        return $bbeInfo;
    }
}
