<?php

namespace AppBundle\ThirdParty\Wics\Xml\Export;

use AppBundle\ThirdParty\Wics\Entity\Message;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Sabre\Xml as Sabre;

/**
 * Class AbstractAdapter
 */
abstract class AbstractAdapter
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var string $type
     */
    protected $type;

    /**
     * @var Sabre\Service $sabre
     */
    protected $sabre;

    /**
     * @var Message
     */
    private $message;

    /**
     * AbstractAdapter constructor.
     */
    public function __construct()
    {
        $this->sabre = new Sabre\Service();
    }

    /**
     * @param EntityManagerInterface $em
     */
    public function setEntityManager(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Sabre\Writer $writer
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function generateXmlHeader(Sabre\Writer $writer)
    {
        $writer->writeElement('Type', $this->type);
        $writer->writeElement('MessageNo', $this->getMessageNumber());
        $writer->writeElement('Date', date('Y-m-d'));
        $writer->writeElement('Time', date('H:i:s'));
    }

    /**
     * @return int
     * @throws OptimisticLockException
     * @throws ORMException
     */
    protected function getMessageNumber()
    {
        return $this->getMessage()->getId();
    }

    /**
     * @return Message
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function getMessage()
    {
        if (!$this->hasMessage()) {
            $this->createMessage();
        }

        return $this->message;
    }

    /**
     * @return bool
     */
    public function hasMessage()
    {
        return (bool)$this->message;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function createMessage()
    {
        $this->message = new Message();
        $this->message->setType($this->type);

        $this->em->persist($this->message);
        $this->em->flush();
    }

    /**
     * @param string $xml
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function setMessageXml(string $xml)
    {
        $this->message->setXml($xml);

        $this->em->flush();
    }

    /**
     * @param string $extension
     * @param string $filename
     * @param string $suffix
     * @return string
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getPath($extension = 'xml', $filename = '', $suffix = '')
    {
        $path = sprintf('/In/%s', $this->type);

        $date = new \DateTime();

        if (empty($filename)) {
            $filename = $extension;
        }

        $filename .= sprintf('_%s_%s%s', $date->format('YmdHis'), $this->getMessageNumber(), $suffix);

        return sprintf('%s/%s.%s', $path, $filename, $extension);
    }

    /**
     * @param string $extension
     * @param string $filename
     * @param string $attachmentType
     * @return string
     */
    public function getAttachmentPath($extension = 'xml', $filename = '', $attachmentType = ''): string
    {
        $path = sprintf('/In/%s', $this->type);

        if (empty($filename)) {
            $filename = $extension;
        }

        $filename .= sprintf('-%s', $attachmentType);

        return sprintf('%s/%s.%s', $path, $filename, $extension);
    }
}
