<?php

namespace AppBundle\ThirdParty\Wics\Xml\Export;

use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\ThirdParty\Wics\Interfaces\ExportXmlInterface;
use AppBundle\ThirdParty\Wics\Services\WicsService;
use Sabre\Xml as Sabre;

/**
 * Class OrderAdapter
 * @package AppBundle\ThirdParty\Wics\Xml\Export
 */
class CancelOrderAdapter extends AbstractAdapter implements ExportXmlInterface
{

    /** @var SupplierOrder $supplierOrder */
    private $supplierOrder;
    /** @var WicsService $wics */
    private $wics;
    /** @var string $type */
    protected $type = 'Order';

    /**
     * OrderAdapter constructor.
     * @param $object
     * @throws \Exception
     */
    public function __construct($object)
    {
        parent::__construct();

        if (!$object instanceof SupplierOrder) {
            throw new \RuntimeException('Invalid request');
        }

        $this->supplierOrder = $object;
    }

    /**
     * @param WicsService $wics
     * @return $this
     */
    public function setWics(WicsService $wics)
    {
        $this->wics = $wics;

        return $this;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function generateXml()
    {
        $wicsService = $this->wics;

        return $this->sabre->write('Message', function (Sabre\Writer $writer) use ($wicsService) {
            $this->generateXmlHeader($writer);

            $writer->writeElement('orders', function () use ($writer, $wicsService) {
                $writer->writeElement('order', function () use ($writer, $wicsService) {
                    $deliveryDate = $this->supplierOrder->getOrder()->getDeliveryDate();
                    if (null === $deliveryDate) {
                        $deliveryDate = new \DateTime('tomorrow');
                        if ($deliveryDate->format('N') === '7') {
                            $deliveryDate->add(new \DateInterval('P1D'));
                        }
                    }
                    $deliveryDateFormatted = $deliveryDate->format('Y-m-d');

                    $writer->writeElement('OrderType', $wicsService->getOrderTypeCode($this->supplierOrder));
                    $writer->writeElement('DeliveryDate', $deliveryDateFormatted);
                    $writer->writeElement('ReqShipDate', $deliveryDateFormatted);
                    $writer->writeElement('ExternalReference', $this->supplierOrder->getOrder()->getOrderNumber());
                    $writer->writeElement('ExternalReference2', '');
                    $writer->writeElement('CustomerName', 'CANCELLED');
                    $writer->writeElement('CustomerContactName', '');
                    $writer->writeElement('Notes', 'CANCELLED');
                });

                $writer->writeElement('Address', function () use ($writer) {
                    $writer->writeElement('AddressType', 'Delivery');
                    $writer->writeElement('Address', '');
                    $writer->writeElement('HouseNumber', '');
                    $writer->writeElement('HouseNumberExt', '');
                    $writer->writeElement('Address2', '');
                    $writer->writeElement('Zipcode', '');
                    $writer->writeElement('City', '');
                    $writer->writeElement('State', '');
                    $writer->writeElement('Country', '');
                    $writer->writeElement('Email', '');
                    $writer->writeElement('PhoneNumber', '');
                });

                $writer->writeElement('OrderLines', '');
            });
        });
    }
}
