<?php

namespace AppBundle\ThirdParty\Wics\Controller;

use AppBundle\Services\JobManager;
use AppBundle\ThirdParty\Wics\Exceptions\WicsException;
use AppBundle\ThirdParty\Wics\Factory\ImportFactory;
use AppBundle\ThirdParty\Wics\Services\WicsService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use League\Flysystem\FileNotFoundException;
use JMS\JobQueueBundle\Entity\Job;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Class WicsController
 * @package AppBundle\Controller
 * @Route("/wics")
 */
class WicsController extends Controller
{
    /**
     * @Route("/import/order-freeze")
     * @param WicsService $wics
     * @param Request     $request
     * @return Response
     * @throws FileNotFoundException
     * @throws OptimisticLockException
     */
    public function importOrderFreeze(WicsService $wics, Request $request)
    {
        $path = $request->query->get('path');

        if(\strlen($path) > 3) {
            try {
                $wics->handleOrderFreeze(ImportFactory::createOrderFreeze($wics->read($path)));
                $wics->move($path, true);
            } catch (FileNotFoundException $e) {
                return new Response($e->getMessage(), 400);
            } catch (WicsException $e) {
                $wics->move($path, false);
                return new Response($e->getMessage(), 400);
            }
            return new Response('Ok');
        }

        return new Response('No file');
    }

    /**
     * @Route("/import/shipping-confirmation")
     * @param WicsService $wics
     * @param Request     $request
     * @return Response
     * @throws FileNotFoundException
     * @throws \Exception
     */
    public function importOrderConfirmationAction(WicsService $wics, Request $request)
    {
        $path = $request->query->get('path');

        if(\strlen($path) > 3) {
            try {
                $wics->handleShippingConfirmation(ImportFactory::createShippingConfirmation($wics->read($path)));
                $wics->move($path, true);
            } catch (FileNotFoundException $e) {
                return new Response($e->getMessage(), 400);
            } catch (WicsException $e) {
                $wics->move($path, false);
                return new Response($e->getMessage(), 400);
            }
            return new Response('Ok');
        }

        return new Response('No file');
    }

    /**
     * @Route("/import/stock-correction")
     * @param WicsService $wics
     * @param Request $request
     *
     * @return Response
     * @throws FileNotFoundException
     * @throws InvalidArgumentException
     * @throws OptimisticLockException
     */
    public function importStockCorrection(WicsService $wics, Request $request)
    {
        $path = $request->query->get('path');

        if(\strlen($path) > 3) {
            try {
                $wics->handleStockCorrection(ImportFactory::createStockCorrection($wics->read($path)));
                $wics->move($path, true);
            } catch (FileNotFoundException $e) {
                return new Response($e->getMessage(), 400);
            } catch(UnprocessableEntityHttpException $e) {
                $wics->move($path, false);

                throw $e;
            }
            return new Response('Ok');
        }

        return new Response('No file');
    }

    /**
     * @Route("/import/stock-level")
     * @param JobManager             $jobManager
     * @param EntityManagerInterface $entityManager
     * @param Request                $request
     * @return Response
     */
    public function importStockLevel(JobManager $jobManager, EntityManagerInterface $entityManager, Request $request)
    {
        $path = $request->query->get('path');

        if(\strlen($path) > 3) {
            try {
                $job = new Job('wics:import:stockLevel', [$path]);

                $jobManager->addJob($job);
                $entityManager->flush();
                return new Response('Job created');
            } catch (\Exception $e) {
                return new Response($e->getMessage(), 500);
            }
        }
        return new Response('No file');
    }

    /**
     * @Route("/import/store-confirmation")
     * @param WicsService $wics
     * @param Request     $request
     * @return Response
     */
    public function importStoreConfirmation(WicsService $wics, Request $request)
    {
        //TODO Remove return statement after $wicsService->handleStoreConfirmation is implemented
        return new Response('Not implemented', 500);
        //TODO End
    }
}
