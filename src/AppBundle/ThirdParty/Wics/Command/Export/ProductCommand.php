<?php

namespace AppBundle\ThirdParty\Wics\Command\Export;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\ThirdParty\Wics\Services\WicsService;
use AppBundle\Utils\DisableFilter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ProductCommand
 * @package AppBundle\ThirdParty\Wics\Command
 */
class ProductCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * @var LoggerInterface $logger
     */
    private $logger;

    /**
     * @var WicsService $wicsService
     */
    private $wicsService;

    /**
     * Configures command
     */
    protected function configure()
    {
        $this
            ->setName('wics:export:product')
            ->setDescription('Export products to WICS WMS')
            ->addArgument('productId', InputArgument::OPTIONAL, 'Product identifier')
            ->addOption('all', null, InputOption::VALUE_NONE, 'Export all relevant products');
    }

    /**
     * ProductCommand constructor.
     * @param EntityManagerInterface   $entityManager
     * @param LoggerInterface $logger
     * @param WicsService     $wicsService
     */
    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger, WicsService $wicsService)
    {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->logger = $logger;
        $this->wicsService = $wicsService;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return bool
     * @throws OptimisticLockException
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $filter = DisableFilter::temporaryDisableFilter('publishable');

        $productId = $input->getArgument('productId');

        $products = [];

        if (null !== $productId) {
            $productId = (int)$productId;

            /**
             * @var Product $product
             */
            $product = $this->entityManager->getRepository(Product::class)->find($productId);

            if (null === $product) {
                throw new \RuntimeException(sprintf('Cannot find product with id %s', $productId));
            }

            if ($input->getOption('all')) {
                if ($product->getParent()) {
                    throw new \RuntimeException(sprintf('Option --all makes no sense for product %d (it\'s a variation).', $productId));
                }

                $products = $product->getVariations();
            } else {
                if (null === $product->getSku()) {
                    throw new \RuntimeException(sprintf('Product %d is a main product, use --all to export its variations.', $productId));
                }

                $products = [ $product ];
            }
        } else if ($input->getOption('all')) {
            $products = $this->entityManager->getRepository(Product::class)->findBy([
                'hasStock' => true
            ]);
        } else {
            throw new \RuntimeException('Must specifiy at least one of argument productId or option --all');
        }

        foreach ($products as $product) {
            try {
                $this->wicsService->exportProduct($product);
                $this->logger->info(sprintf('Synchronized product to WICS: %s (%d)', $product->getSku(), $product->getId()));
            } catch (\RuntimeException $e) {
                $this->logger->error($e->getMessage(), [
                    'exception' => $e
                ]);
            }
        }

        $filter->reenableFilter();

        return 0;
    }
}
