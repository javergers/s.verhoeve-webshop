<?php

namespace AppBundle\ThirdParty\Adyen\Entity;

use AppBundle\Entity\Order\OrderCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * @ORM\Entity
 *
 * @link https://docs.adyen.com/developers/api-reference/notifications-api/notificationrequestitem
 * @method array getAmount
 * @method string getEventCode
 * @method string getMerchantReference
 * @method string getOriginalReference
 * @method string getPspReference
 * @method string getReason
 * @method boolean getSuccess
 */
class AdyenNotification
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Order\OrderCollection")
     */
    private $orderCollection;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime;

    /**
     * @ORM\Column(type="json_array")
     */
    private $data;

    /**
     * @ORM\Column(type="boolean")
     */
    private $processed = false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return AdyenNotification
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set data
     *
     * @param array $data
     *
     * @return AdyenNotification
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set order
     *
     * @param OrderCollection $orderCollection
     *
     * @return AdyenNotification
     */
    public function setOrder(OrderCollection $orderCollection = null)
    {
        $this->orderCollection = $orderCollection;

        return $this;
    }

    /**
     * Get order
     *
     * @return OrderCollection
     */
    public function getOrder()
    {
        return $this->orderCollection;
    }

    /**
     * Set processed
     *
     * @param boolean $processed
     *
     * @return AdyenNotification
     */
    public function setProcessed($processed)
    {
        $this->processed = $processed;

        return $this;
    }

    /**
     * Get processed
     *
     * @return boolean
     */
    public function getProcessed()
    {
        return $this->processed;
    }

    /**
     * @param       $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, array $arguments)
    {
        return (new PropertyAccessor())->getValue($this->data, "[" . lcfirst(substr($name, 3)) . "]");
    }

    /**
     * Set orderCollection
     *
     * @param OrderCollection $orderCollection
     *
     * @return AdyenNotification
     */
    public function setOrderCollection(OrderCollection $orderCollection = null)
    {
        $this->orderCollection = $orderCollection;

        return $this;
    }

    /**
     * Get orderCollection
     *
     * @return OrderCollection
     */
    public function getOrderCollection()
    {
        return $this->orderCollection;
    }
}
