<?php

namespace AppBundle\ThirdParty\Kiyoh\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class KiyohReviewRepository
 * @package AppBundle\ThirdParty\Kiyoh\Entity
 */
class KiyohReviewRepository extends EntityRepository
{
    /**
     * @param KiyohCompany $kiyohCompany
     * @param int          $limit
     * @return array
     */
    public function findRecentForCompany(KiyohCompany $kiyohCompany, $limit = 50)
    {
        $queryBuilder = $this->getEntityManager()
            ->getRepository(KiyohReview::class)
            ->createQueryBuilder('kire');

        $queryBuilder
            ->select('kire')
            ->where('kire.kiyohCompany = :kiyohCompany')
            ->addOrderBy('kire.datetime', 'DESC')
            ->setParameter('kiyohCompany', $kiyohCompany)
            ->setMaxResults($limit);

        return $queryBuilder->getQuery()->getResult();
    }
}
