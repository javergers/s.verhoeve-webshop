<?php

namespace AppBundle\ThirdParty\Kiyoh\Entity;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Order\OrderCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\ThirdParty\Kiyoh\Entity\KiyohReviewRepository")
 */
class KiyohReview
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $identifier;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Order\OrderCollection", inversedBy="review")
     */
    private $orderCollection;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\ThirdParty\Kiyoh\Entity\KiyohCompany", inversedBy="reviews")
     */
    private $kiyohCompany;

    /**
     * @ORM\Column(type="string", length=130)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="float")
     */
    private $score;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $positiveFeedback;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $negativeFeedback;

    /**
     * @ORM\Column(type="boolean")
     */
    private $recommended;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Catalog\Product\Product")
     */
    private $product;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return KiyohReview
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return KiyohReview
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return KiyohReview
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return KiyohReview
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set score
     *
     * @param float $score
     *
     * @return KiyohReview
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return float
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * Get score
     *
     * @return float
     */
    public function getStarRating()
    {
        return $this->score / 2;
    }

    /**
     * Set positiveFeedback
     *
     * @param string $positiveFeedback
     *
     * @return KiyohReview
     */
    public function setPositiveFeedback($positiveFeedback)
    {
        $this->positiveFeedback = $positiveFeedback;

        return $this;
    }

    /**
     * Get positiveFeedback
     *
     * @return string
     */
    public function getPositiveFeedback()
    {
        return $this->positiveFeedback;
    }

    /**
     * Set negativeFeedback
     *
     * @param string $negativeFeedback
     *
     * @return KiyohReview
     */
    public function setNegativeFeedback($negativeFeedback)
    {
        $this->negativeFeedback = $negativeFeedback;

        return $this;
    }

    /**
     * Get negativeFeedback
     *
     * @return string
     */
    public function getNegativeFeedback()
    {
        return $this->negativeFeedback;
    }

    /**
     * Set recommended
     *
     * @param boolean $recommended
     *
     * @return KiyohReview
     */
    public function setRecommended($recommended)
    {
        $this->recommended = $recommended;

        return $this;
    }

    /**
     * Get recommended
     *
     * @return boolean
     */
    public function getRecommended()
    {
        return $this->recommended;
    }

    /**
     * Set kiyohCompany
     *
     * @param KiyohCompany $kiyohCompany
     *
     * @return KiyohReview
     */
    public function setKiyohCompany(KiyohCompany $kiyohCompany = null)
    {
        $this->kiyohCompany = $kiyohCompany;

        return $this;
    }

    /**
     * Get kiyohCompany
     *
     * @return KiyohCompany
     */
    public function getKiyohCompany()
    {
        return $this->kiyohCompany;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return KiyohReview
     */
    public function setProduct(Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set orderCollection
     *
     * @param OrderCollection $orderCollection
     *
     * @return KiyohReview
     */
    public function setOrderCollection(OrderCollection $orderCollection = null)
    {
        $this->orderCollection = $orderCollection;

        return $this;
    }

    /**
     * Get orderCollection
     *
     * @return OrderCollection
     */
    public function getOrderCollection()
    {
        return $this->orderCollection;
    }

    /**
     * Set identifier
     *
     * @param integer $identifier
     *
     * @return KiyohReview
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier
     *
     * @return integer
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }
}
