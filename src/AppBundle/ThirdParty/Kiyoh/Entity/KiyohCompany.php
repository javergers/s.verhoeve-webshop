<?php

namespace AppBundle\ThirdParty\Kiyoh\Entity;

use AppBundle\Entity\Site\Domain;
use AppBundle\Entity\Site\Page;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class KiyohCompany
{
    use TimestampableEntity;
    use SoftDeleteableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Site\Domain", inversedBy="kiyohCompany")
     * @ORM\JoinColumn(name="domain_id", referencedColumnName="id")
     */
    protected $domain;

    /**
     * @ORM\Column(type="integer")
     */
    private $identifier;

    /**
     * @ORM\Column(type="boolean")
     */
    private $syncEnabled;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $xmlUrl;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $connectorCode;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $totalScore;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalReviews;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $totalViews;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $totalRecommendationScore;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\ThirdParty\Kiyoh\Entity\KiyohReview", mappedBy="kiyohCompany", cascade={"persist"})
     */
    private $reviews;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Site\Page")
     */
    private $page;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier
     *
     * @param integer $identifier
     *
     * @return KiyohCompany
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier
     *
     * @return integer
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return KiyohCompany
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return KiyohCompany
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set xmlUrl
     *
     * @param string $xmlUrl
     *
     * @return KiyohCompany
     */
    public function setXmlUrl($xmlUrl)
    {
        $this->xmlUrl = $xmlUrl;

        return $this;
    }

    /**
     * Get xmlUrl
     *
     * @return string
     */
    public function getXmlUrl()
    {
        return $this->xmlUrl;
    }

    /**
     * Set connectorCode
     *
     * @param string $connectorCode
     *
     * @return KiyohCompany
     */
    public function setConnectorCode($connectorCode)
    {
        $this->connectorCode = $connectorCode;

        return $this;
    }

    /**
     * Get connectorCode
     *
     * @return string
     */
    public function getConnectorCode()
    {
        return $this->connectorCode;
    }

    /**
     * Set totalScore
     *
     * @param float $totalScore
     *
     * @return KiyohCompany
     */
    public function setTotalScore($totalScore)
    {
        $this->totalScore = $totalScore;

        return $this;
    }

    /**
     * Get totalScore
     *
     * @return float
     */
    public function getTotalScore()
    {
        return $this->totalScore;
    }

    /**
     * Get score
     *
     * @return float
     */
    public function getStarRating()
    {
        return $this->totalScore / 2;
    }

    /**
     * Set totalReviews
     *
     * @param integer $totalReviews
     *
     * @return KiyohCompany
     */
    public function setTotalReviews($totalReviews)
    {
        $this->totalReviews = $totalReviews;

        return $this;
    }

    /**
     * Get totalReviews
     *
     * @return integer
     */
    public function getTotalReviews()
    {
        return $this->totalReviews;
    }

    /**
     * Set totalViews
     *
     * @param integer $totalViews
     *
     * @return KiyohCompany
     */
    public function setTotalViews($totalViews)
    {
        $this->totalViews = $totalViews;

        return $this;
    }

    /**
     * Get totalViews
     *
     * @return integer
     */
    public function getTotalViews()
    {
        return $this->totalViews;
    }

    /**
     * Set totalRecommendationScore
     *
     * @param float $totalRecommendationScore
     *
     * @return KiyohCompany
     */
    public function setTotalRecommendationScore($totalRecommendationScore)
    {
        $this->totalRecommendationScore = $totalRecommendationScore;

        return $this;
    }

    /**
     * Get totalRecommendationScore
     *
     * @return float
     */
    public function getTotalRecommendationScore()
    {
        return $this->totalRecommendationScore;
    }

    /**
     * Set domain
     *
     * @param Domain $domain
     *
     * @return KiyohCompany
     */
    public function setDomain(Domain $domain = null)
    {
        $this->domain = $domain;

        return $this;
    }

    /**
     * Get domain
     *
     * @return Domain
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reviews = new ArrayCollection();
    }

    /**
     * Add review
     *
     * @param KiyohReview $review
     *
     * @return KiyohCompany
     */
    public function addReview(KiyohReview $review)
    {
        $this->reviews[] = $review;

        return $this;
    }

    /**
     * Remove review
     *
     * @param KiyohReview $review
     */
    public function removeReview(KiyohReview $review)
    {
        $this->reviews->removeElement($review);
    }

    /**
     * Get reviews
     *
     * @return Collection
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * Set syncEnabled
     *
     * @param boolean $syncEnabled
     *
     * @return KiyohCompany
     */
    public function setSyncEnabled($syncEnabled)
    {
        $this->syncEnabled = $syncEnabled;

        return $this;
    }

    /**
     * Get syncEnabled
     *
     * @return boolean
     */
    public function getSyncEnabled()
    {
        return $this->syncEnabled;
    }

    /**
     * Get syncEnabled
     *
     * @return boolean
     */
    public function isSyncEnabled()
    {
        return (bool)$this->getSyncEnabled();
    }

    /**
     * Set page
     *
     * @param Page $page
     *
     * @return KiyohCompany
     */
    public function setPage(Page $page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return Page
     */
    public function getPage()
    {
        return $this->page;
    }
}
