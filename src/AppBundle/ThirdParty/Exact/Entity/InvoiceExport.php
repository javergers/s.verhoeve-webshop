<?php

namespace AppBundle\ThirdParty\Exact\Entity;

use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use RuleBundle\Annotation as Rule;
use AppBundle\Entity\Security\Employee\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity()
 * @ORM\Table("exact_invoice_export")
 */
class InvoiceExport
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=false)
     */
    protected $deliveryDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=false)
     */
    protected $invoiceDate;

    /**
     * @DoctrineAssert\Enum(entity="AppBundle\DBAL\Types\ExactInvoiceExportStatusType")
     * @ORM\Column(type="ExactInvoiceExportStatusType", options={"default" : "new"})
     * @Rule\Property(description="Export status")
     */
    protected $exportStatus;

    /**
     * @var User
     *
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Security\Employee\User")
     * @ORM\JoinColumn(referencedColumnName="id", nullable=false)
     */
    private $createdBy;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $processedAt;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     * @var array
     */
    private $companiesToProcess;

    /**
     * InvoiceExport constructor.
     */
    public function __construct()
    {
        $this->exportStatus = 'new';
    }

    /**
     * @param integer $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param \DateTime $deliveryDate
     */
    public function setDeliveryDate(\DateTime $deliveryDate): void
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return \DateTime
     */
    public function getDeliveryDate(): \DateTime
    {
        return $this->deliveryDate;
    }

    /**
     * @param \DateTime $invoiceDate
     */
    public function setInvoiceDate(\DateTime $invoiceDate): void
    {
        $this->invoiceDate = $invoiceDate;
    }

    /**
     * @return \DateTime
     */
    public function getInvoiceDate(): \DateTime
    {
        return $this->invoiceDate;
    }

    /**
     * Get exportStatus
     *
     * @return string
     */
    public function getExportStatus(): string
    {
        return $this->exportStatus;
    }

    /**
     * Set exportStatus
     *
     * @param string $exportStatus
     *
     * @return InvoiceExport
     */
    public function setExportStatus(string $exportStatus): InvoiceExport
    {
        $this->exportStatus = $exportStatus;

        return $this;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy(User $createdBy = null): void
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return User
     */
    public function getCreatedBy(): User
    {
        return $this->createdBy;
    }

    /**
     * @return \DateTime|null
     */
    public function getProcessedAt(): ?\DateTime
    {
        return $this->processedAt;
    }

    /**
     * @param \DateTime|null $processedAt
     */
    public function setProcessedAt(?\DateTime $processedAt): void
    {
        $this->processedAt = $processedAt;
    }

    /**
     * @param array $companiesToProcess
     * @return InvoiceExport
     */
    public function setCompaniesToProcess(array $companiesToProcess): InvoiceExport
    {
        $this->companiesToProcess = $companiesToProcess;
        return $this;
    }

    /**
     * @return array
     */
    public function getCompaniesToProcess(): array
    {
        return $this->companiesToProcess;
    }
}