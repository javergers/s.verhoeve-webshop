<?php

namespace AppBundle\ThirdParty\Exact\Xml;

/**
 * Interface XmlInterface
 * @package AppBundle\ThirdParty\Exact\Xml
 */
interface ExactXmlInterface
{
    /**
     * @return string
     */
    public function generateXml();

    public function getObjectClass();
}
