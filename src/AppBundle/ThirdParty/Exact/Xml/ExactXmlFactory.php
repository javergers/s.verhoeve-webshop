<?php

namespace AppBundle\ThirdParty\Exact\Xml;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class XmlFactory
 */
class ExactXmlFactory
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * XmlFactory constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     *
     * @param string $xmlAdapter
     * @param object $object
     *
     * @return ExactXmlInterface
     */
    public function create(string $xmlAdapter, object $object): ExactXmlInterface
    {
        try {
            $reflectionClass = new \ReflectionClass($xmlAdapter);
        } catch (\ReflectionException $e) {
            return null;
        }
        if(!$reflectionClass->implementsInterface(ExactXmlInterface::class)){
            return null;
        }
        /** @var ExactAbstractAdapter $adapter */
        $adapter = $reflectionClass->newInstance();
        $adapter->setEntityManager($this->entityManager);
        if(get_class($object) !== $adapter->getObjectClass()){
            return null;
        }
        $adapter->setObject($object);
        return $adapter;
    }
}
