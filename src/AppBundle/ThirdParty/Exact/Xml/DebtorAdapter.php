<?php

namespace AppBundle\ThirdParty\Exact\Xml;

use AppBundle\DBAL\Types\CompanyInvoiceTypeType;
use AppBundle\Entity\Relation\Company;
use AppBundle\ThirdParty\Exact\Entity\InvoiceExport;
use Sabre\Xml as Sabre;


/**
 * Class DebtorAdapter
 * @package AppBundle\ThirdParty\Exact\Xml
 */
class DebtorAdapter extends ExactAbstractAdapter
{
    /**
     * @var InvoiceExport $object
     */
    protected $object;

    /**
     * @return string
     */
    public function getObjectClass()
    {
        return InvoiceExport::class;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function generateXml()
    {
        $companyRepository = $this->entityManager->getRepository(Company::class);
        $companies = $companyRepository->findByInvoiceExport($this->object);

        //Add parent to array first so that the debtor will be created first
        $companiesWithParents = [];
        /** @var Company $company */
        foreach($companies as $company){
            if(($parent = $company->getParent()) !== null){
                $companiesWithParents[$parent->getId()] = $parent;
            }
            $companiesWithParents[$company->getId()] = $company;
        }

        return $this->_generateXml($companiesWithParents);
    }

    /**
     * @param array $companies
     *
     * @return string
     * @internal
     */
    public function _generateXml(array $companies)
    {
        return $this->sabre->write('eExact', function (Sabre\Writer $writer) use ($companies) {
            $this->generateXmlHeader($writer);
            $writer->writeElement('Accounts', static function () use ($writer, $companies) {

                /** @var Company $company */
                foreach ($companies as $company) {
                    $writer->writeElement('Account', static function () use ($writer, $company) {
                        // UITLEG status, A=Actief C=Customer
                        $writer->writeAttributes([
                            'code' => $company->getExactRelationNumber(),
                            'status' => 'A',
                            'type' => 'C',
                        ]);

                        $writer->writeElement('Accountcategory', 'TG');

                        $writer->writeElement('Name', $company->getName());

                        $writer->writeElement('Email', $company->getInvoiceEmail());

                        if(($parent = $company->getParent()) !== null){
                            $writer->writeElement('Parent', static function () use ($writer, $parent) {
                                $writer->writeAttributes([
                                    'code' => $parent->getExactRelationNumber(),
                                ]);
                            });
                        }

                        $writer->writeElement('Debtor', static function () use ($writer, $company) {
                            $writer->writeAttributes([
                                'code' => $company->getExactRelationNumber(),
                            ]);
                        });

                        //FreeTexts
                        $companyInvoiceSettings = $company->getCompanyInvoiceSettings();
                        $invoiceSettings = [
                            'useUbl' => false,
                            'invoiceMethod' => null,
                            'invoiceReminderEmail' => $company->getInvoiceEmail(),
                        ];
                        if ($companyInvoiceSettings !== null) {
                            if($companyInvoiceSettings->getInvoiceType() === CompanyInvoiceTypeType::INVOICE_TYPE_UBL_GOV_NL){
                                $invoiceSettings['invoiceMethod'] = 'E';
                            }
                            $invoiceSettings['useUbl'] = $companyInvoiceSettings->getInvoiceType() === CompanyInvoiceTypeType::INVOICE_TYPE_UBL_MAIL;
                            $invoiceSettings['invoiceReminderEmail'] = $companyInvoiceSettings->getInvoiceReminderEmail() ?: $invoiceSettings['invoiceReminderEmail'];
                        }

                        if($invoiceSettings['invoiceMethod'] !== null){
                            $writer->writeElement('InvoiceMethod', $invoiceSettings['invoiceMethod']);

                            $writer->writeElement('Mailbox', 'TIEKINETIX@tiekinetix.com');

                            $writer->writeElement('ExemptNumber', $company->getOinNumber());
                        }

                        $writer->writeElement('Contacts', static function () use ($writer, $company) {

                            $writer->writeElement('Contact', static function () use ($writer, $company) {
                                // gender="{{man/vrouw (1/0)}}"
                                $writer->writeAttributes([
                                    'default' => '1',
                                    'gender' => 'O', //Onbekend
                                    'status' => 'A',
                                ]);

                                $writer->writeElement('LastName',
                                    $company->getInvoiceAddress() !== null && $company->getInvoiceAddress()->getAttn() !== null ? $company->getInvoiceAddress()->getAttn() : '_');

                                $writer->writeElement('Email', $company->getInvoiceEmail());

                                $writer->writeElement('Addresses', static function () use ($writer, $company) {

                                    $writer->writeElement('Address', static function () use ($writer, $company) {
                                        $writer->writeAttributes([
                                            'type' => 'I',
                                            'desc' => 'INV',
                                        ]);

                                        $address = $company->getInvoiceAddress();
                                        $streetAndNumber = '';
                                        if ($address !== null) {
                                            $streetAndNumber = $address->getStreet() . ' ' . $address->getNumber() . (!empty($address->getNumberAddition()) ? $address->getNumberAddition() : '');
                                        }

                                        $writer->writeElement('AddressLine1', $streetAndNumber);

                                        $writer->writeElement('PostalCode',
                                            ($address !== null) ? $address->getPostcode() : '');

                                        $writer->writeElement('City', ($address !== null) ? $address->getCity() : '');

                                        $writer->writeElement('Country',
                                            ($address !== null) ? $address->getCountry()->getCode() : '');
                                    });
                                });
                            });
                        });

                        $writer->writeElement('FreeFields',
                            static function () use ($writer, $invoiceSettings) {

                                $writer->writeElement('FreeTexts',
                                    static function () use ($writer, $invoiceSettings) {

                                        $writer->writeElement('FreeText',
                                            static function () use ($writer, $invoiceSettings) {
                                                $writer->writeAttributes([
                                                    'number' => '6',
                                                    'label' => 'TextField6',
                                                ]);

                                                $writer->write($invoiceSettings['invoiceReminderEmail']);
                                            });
                                    });

                                $writer->writeElement('FreeYesNos', static function () use ($writer, $invoiceSettings) {

                                    $writer->writeElement('FreeYesNo', static function () use ($writer, $invoiceSettings) {
                                        $writer->writeAttributes([
                                            'number' => '1',
                                            'label' => 'YesNoField1',
                                        ]);

                                        $writer->write($invoiceSettings['useUbl'] ? '1' : '0');
                                    });
                                });
                            });
                    });
                }
            });
        });
    }
}
