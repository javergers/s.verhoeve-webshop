<?php


namespace AppBundle\Filter;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Employee\User;
use AppBundle\EventListener\AccessibleListener;
use AppBundle\Traits\AccessibleEntity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\MappingException;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * Class AccessibleFilter
 *
 * @package AppBundle\Filter
 *
 */
class AccessibleFilter extends SQLFilter
{
    /**
     * @var
     */
    protected $listener;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var array
     */
    protected $disabled = [];

    /**
     * Gets the SQL query part to add to a query.
     *
     * @param ClassMetaData $targetEntity
     * @param string        $targetTableAlias
     *
     * @return string The constraint SQL if there is available, empty string otherwise.
     * @throws MappingException
     * @throws \ReflectionException
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        $addCondSql = '';
        $addCondCustomerGroupsSql = '';

        $user = null;
        $customerGroupId = null;
        $class = $targetEntity->getName();

        $tokenStorage = $this->getListener()->getTokenStorage();

        if ((
            !$tokenStorage
            || !$tokenStorage->getToken()
            || ($tokenStorage->getToken() && $tokenStorage->getToken()->getUser() instanceof User))
            || !\in_array(AccessibleEntity::class, class_uses($class), true)
            || (array_key_exists($class, $this->disabled) && $this->disabled[$class] === true)
            || (array_key_exists($targetEntity->rootEntityName,
                    $this->disabled) && $this->disabled[$targetEntity->rootEntityName] === true)
        ) {
            return $addCondSql;
        }

        if ($tokenStorage->getToken()->getUser() instanceof Customer) {
            $user = $tokenStorage->getToken()->getUser();

            if ($user->getCustomerGroup()) {
                $customerGroupId = $user->getCustomerGroup()->getId();
            }
        }

        $conn = $this->getEntityManager()->getConnection();
        $platform = $conn->getDatabasePlatform();

        $columnLoginRequired = 'login_required';

        $columnLoginRequiredNull = $platform->getIsNullExpression($targetTableAlias . '.' . $columnLoginRequired);

        $addCondSql = "
            ({$columnLoginRequiredNull} OR {$targetTableAlias}.{$columnLoginRequired} = 0)
        ";

        if ($targetEntity->hasAssociation('accessibleCustomerGroups')) {
            $associationCustomerGroup = $targetEntity->getAssociationMapping('accessibleCustomerGroups');

            if (!empty($customerGroupId) && !empty($associationCustomerGroup['joinTable']) && !empty($associationCustomerGroup['joinTable']['name'])) {
                $associationCustomerGroupTable = $associationCustomerGroup['joinTable']['name'];
                $associationCustomerGroupJoinColumn = $associationCustomerGroup['joinTable']['joinColumns'][0]['name'];

                $addCondCustomerGroupsSql = "
                    AND '{$customerGroupId}' IN (
                        SELECT customer_group_id
                        FROM {$associationCustomerGroupTable}
                        WHERE {$associationCustomerGroupTable}.{$associationCustomerGroupJoinColumn} = {$targetTableAlias}.id
                    ) OR (SELECT COUNT(customer_group_id)
                        FROM {$associationCustomerGroupTable}
                        WHERE {$associationCustomerGroupTable}.{$associationCustomerGroupJoinColumn} = {$targetTableAlias}.id) = 0
                ";
            }
        }


        if ($user) {
            $addCondSqlExtra = "{$targetTableAlias}.{$columnLoginRequired} = 1";

            if (!empty($addCondCustomerGroupsSql)) {
                $addCondSqlExtra = '(' . $addCondSqlExtra . '' . $addCondCustomerGroupsSql . ')';
            }

            $addCondSql .= ' OR ' . $addCondSqlExtra;
        }

        return $addCondSql;
    }

    /**
     * @param $class
     */
    public function disableForEntity($class)
    {
        $this->disabled[$class] = true;
    }

    /**
     * @param $class
     */
    public function enableForEntity($class)
    {
        $this->disabled[$class] = false;
    }

    /**
     * @return AccessibleListener
     * @throws \ReflectionException
     */
    public function getListener()
    {
        if ($this->listener === null) {
            $evm = $this->getEntityManager()->getEventManager();

            foreach ($evm->getListeners() as $listeners) {
                foreach ($listeners as $listener) {
                    if ($listener instanceof AccessibleListener) {
                        $this->listener = $listener;

                        break 2;
                    }
                }
            }

            if ($this->listener === null) {
                throw new \RuntimeException('Listener "AccessibleListener" was not added to the EventManager!');
            }
        }

        return $this->listener;
    }

    /**
     * @return EntityManagerInterface
     * @throws \ReflectionException
     */
    protected function getEntityManager()
    {
        if ($this->entityManager === null) {
            $reflectionProperty = new \ReflectionProperty(SQLFilter::class, 'em');
            $reflectionProperty->setAccessible(true);

            $this->entityManager = $reflectionProperty->getValue($this);
        }

        return $this->entityManager;
    }
}
