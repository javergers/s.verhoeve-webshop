<?php

namespace AppBundle\Services\Designer;

use AppBundle\Exceptions\Designer\ProcessDesignException;
use AppBundle\Services\Designer;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Class CsvDesignGeneratorService
 * @deprecated use DesignGeneratorService instead
 * @package AppBundle\Services\Designer
 */
class CsvDesignGeneratorService
{
    // Designer constants
    public const MAX_SENTENCE_LENGTH = 45;

    public const PIXELS_Y = 293.8;
    public const PIXELS_X = 0.754;

    /**
     * @var Designer
     */
    private $designer;

    /**
     * DesignGeneratorService constructor.
     * @param Designer $designer
     */
    public function __construct(
        Designer $designer
    ) {
        $this->designer = $designer;
    }

    /**
     * @param string $text
     * @param string $sourceFilesPath
     * @param string $fontPath
     * @param float $imageWidth
     * @param float $imageHeight
     * @param null|string $imageUrl
     * @return array
     * @throws ProcessDesignException
     */
    public function generatePersonalization(
        string $text,
        string $sourceFilesPath,
        string $fontPath,
        float $imageWidth = 1,
        float $imageHeight = 1,
        ?string $imageUrl = null
    ): array {
        if (!file_exists($fontPath)) {
            throw new ProcessDesignException('Personalisation font not found.');
        }

        try {
            $uuid = Uuid::uuid4();
        } catch (Exception $e) {
            throw new ProcessDesignException($e->getMessage());
        }

        $this->designer->setUuid($uuid);

        // Download and move image to object store.
        if (null !== $imageUrl) {
            $filename = $this->downloadAndMoveImageToObjectStore($uuid, $imageUrl);
            $imageUrl = $this->designer->getObjectStoreUrl($filename);
        }

        $svg = null;
        $json = null;

        $finder = new Finder();
        /**
         * @var SplFileInfo $file
         */
        foreach ($finder->in($sourceFilesPath) as $file) {
            switch ($file->getExtension()) {
                case 'svg':
                    $svg = $file->getContents();
                    break;
                case 'json':
                    $json = $file->getContents();
                    break;
            }
        }

        if (null === $svg || null === $json) {
            throw new ProcessDesignException('Personalisation layout not found.');
        }

        $lastPosition = 0;
        $positions = [];

        $jsonText = $text;

        if (strlen($text) > self::MAX_SENTENCE_LENGTH) {
            while (($lastPosition = strpos($text, '\n', $lastPosition)) !== false) {
                $positions[] = $lastPosition;
                $lastPosition += 2;
            }

            if (count($positions) === 0) {
                $text = str_replace([',', '.'], [',' . PHP_EOL, '.' . PHP_EOL], $text);
            }
        }

        $textObject = explode('\n', $text);

        if (count($textObject) === 1) {
            $textObject = wordwrap($textObject[0], self::MAX_SENTENCE_LENGTH);
            $textObject = explode(PHP_EOL, $textObject);
        }

        if (count($textObject) > 6) {
            throw new ProcessDesignException('Text to long');
        }

        $designerWidth = 14;
        $designerHeight = 14;

        $textElm = '';
        foreach ($textObject as $key => $line) {
            if (strlen($line) > self::MAX_SENTENCE_LENGTH) {
                throw new ProcessDesignException('Single line text was to long');
            }

            $sizes = imagettfbbox(200, 0, $fontPath, $line);

            $x = 0 - ((($sizes[2] - $sizes[0]) / 2) * self::PIXELS_X);
            $y = 0 - (((count($textObject) - 1) * self::PIXELS_Y) / 2) + ($key * self::PIXELS_Y);

            $textElm .= sprintf('<tspan x="%s" y="%s" fill="rgb(0, 0, 0)">%s</tspan>', $x, $y, $line);
        }

        $svg = str_replace('%text%', $textElm, $svg);

        $json = str_replace(
            ['%text%', '%designer_width%', '%designer_height%'],
            [$jsonText, $designerWidth, $designerHeight],
            $json
        );

        if (null !== $imageUrl) {

            $cmPx = 59.055;
            $maxImageWidthPx = $imageWidth * ($designerWidth * $cmPx);
            $maxImageHeightPx = $imageHeight * ($designerHeight * $cmPx);

            [$imageWidthPx, $imageHeightPx] = $this->calcImageSize($imageUrl, $maxImageWidthPx, $maxImageHeightPx);

            $imagePx_X = ($maxImageWidthPx / 2);
            $imagePx_Y = ($maxImageHeightPx / 2);

            $svg = str_replace([
                '%image%',
                '%image_width%',
                '%image_height%',
                '%image_x%',
                '%image_y%',
            ], [
                str_replace('&', '&amp;', $imageUrl),
                round($imageWidthPx, 2),
                round($imageHeightPx, 2),
                round($imagePx_X - ($imageWidthPx / 2), 2),
                round($imagePx_Y - ($imageHeightPx / 2), 2),
            ], $svg);

            $json = str_replace([
                '%image%',
                '%image_width%',
                '%image_height%',
                '%image_x%',
                '%image_y%',
            ], [
                $imageUrl,
                round($imageWidthPx, 2),
                round($imageHeightPx, 2),
                round($imagePx_X, 2),
                round($imagePx_Y, 2),
            ], $json);
        }

        // Cleanup readable json.
        $json = trim(preg_replace('/\s\s+/', '', $json));

        $this->designer->moveFileDataToObjectStore($svg, 'design.svg');
        $this->designer->moveFileDataToObjectStore($json, 'design.json');
        $this->designer->convertSVGtoPngAndJpg();

        return ['designer' => $uuid];
    }

    /**
     * @param string $imageUrl
     * @return mixed
     * @throws ProcessDesignException
     */
    public function generateImagePersonalization(string $imageUrl)
    {
        $metadata = [];

        $uuid = (string)Uuid::uuid5(Uuid::NAMESPACE_URL, $imageUrl);

        $filename = $this->downloadAndMoveImageToObjectStore($uuid, $imageUrl, true);

        $metadata['design_url_uuid'] = $uuid;
        $metadata['design_url_filename'] = $filename;

        return $metadata;
    }

    /**
     * @param        $uuid
     * @param string $imageUrl
     * @param bool   $isUrlDesign
     * @return string
     * @throws ProcessDesignException
     */
    private function downloadAndMoveImageToObjectStore($uuid, $imageUrl, $isUrlDesign = false)
    {
        if (!$data = file_get_contents($imageUrl)) {
            throw new ProcessDesignException(sprintf("Can't download image (%s) data", $imageUrl));
        }

        $filename = pathinfo($imageUrl, PATHINFO_FILENAME);
        $ext = pathinfo($imageUrl, PATHINFO_EXTENSION);

        if (!in_array($ext, Designer::ALLOWED_IMAGE_EXTENSIONS, true)) {
            throw new ProcessDesignException(sprintf('Extension %s for personalisation not supported.',
                $ext));
        }

        $this->designer->setIsUrlDesign($isUrlDesign);
        $this->designer->setUuid($uuid);

        $filename .= '.' . $ext;

        if ($isUrlDesign) {
            $filename = 'print.' . $ext;
        }

        $this->designer->moveFileDataToObjectStore($data, $filename);

        return $filename;
    }

    /**
     * @param $imageUrl
     * @param $maxWidth
     * @param $maxHeight
     * @return array
     */
    private function calcImageSize($imageUrl, $maxWidth, $maxHeight)
    {
        [$imageWidth, $imageHeight] = getimagesize($imageUrl);

        if ($imageWidth >= $imageHeight) {
            $ratio = $maxWidth / $imageWidth;

            $imageWidth = $maxWidth;
            $imageHeight *= $ratio;
        } else {
            $ratio = $maxHeight / $imageHeight;

            $imageHeight = $maxHeight;
            $imageWidth *= $ratio;
        }

        return [$imageWidth, $imageHeight];
    }

}
