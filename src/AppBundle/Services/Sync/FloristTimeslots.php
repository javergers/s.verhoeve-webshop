<?php

namespace AppBundle\Services\Sync;

use AppBundle\Client\FloristClient;
use AppBundle\Entity\Common\Timeslot;
use AppBundle\Entity\Relation\CompanyEstablishment;
use AppBundle\Entity\Supplier\PickupLocation;
use AppBundle\Exceptions\FloristSyncException;
use AppBundle\Services\Sync\AbstractFloristSync;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use PDO;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FloristTimeslots
 * @package AppBundle\Services\Sync
 */
class FloristTimeslots extends AbstractFloristSync
{
    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var ProgressBar
     */
    private $progressBar;

    /**
     * @return array
     * @throws FloristSyncException
     */
    private function getTimeslots(): array
    {
        $response = $this->floristClient->get('/export/exportOpeningHours');

        if (stripos($response->getHeaderLine('Content-Type'), 'application/json') === false) {
            throw new FloristSyncException(sprintf("Expected 'application/json', got '%s'",
                $response->getHeaderLine('Content-Type')));
        }

        $timeslots = json_decode($response->getBody());

        if (false === $timeslots) {
            throw new FloristSyncException('Invalid json');
        }

        return $timeslots;
    }

    /**
     * @param OutputInterface $output
     *
     * @return $this
     */
    public function setOutput(OutputInterface $output): FloristTimeslots
    {
        $this->output = $output;

        if ($output && $output->isVerbose()) {
            $this->progressBar = new ProgressBar($this->output);
        }

        return $this;
    }

    /**
     * @return object[]
     * @throws DBALException
     */
    private function getCompanyIndex(): array
    {
        $connection = $this->entityManager->getConnection();

        $companyIndex = [];

        $sql = '
            SELECT c.id
              , c.chamber_of_commerce_number
              , c.twinfield_relation_number
              , a.country
              , coes.id as company_establishment_id
              , IF(pilo.id IS NULL, FALSE, TRUE) has_pickup
            FROM company c
            LEFT JOIN company_establishment AS coes ON coes.company_id = c.id
            LEFT JOIN company_supplier_group AS cosg ON cosg.company_id = c.id
            LEFT JOIN pickup_location AS pilo ON pilo.company_establishment_id = coes.id
            LEFT JOIN address a ON a.company_id = c.id
            WHERE
                c.is_supplier = 1
                AND `cosg`.`supplier_group_id` = :supplierGroupId
                AND `a`.`main_address` = 1
        ';

        $stmt = $connection->prepare($sql);
        $stmt->execute([
            "supplierGroupId" => $this->getFloristSupplierGroup()->getId(),
        ]);

        $companies = $stmt->fetchAll(PDO::FETCH_OBJ);

        foreach ($companies as $company) {
            if (empty($company->twinfield_relation_number)) {
                continue;
            }

            $companyIndex[$company->twinfield_relation_number] = $company;
        }

        return $companyIndex;
    }

    /**
     * @return array
     * @throws FloristSyncException
     */
    private function getTimeslotIndex(): array
    {
        $timeslotIndex = [];

        $timeslots = $this->getTimeslots();

        foreach ($timeslots as $timeslot) {
            $twinfieldRelationNumber = $this->determineTwinfieldRelationNumber($timeslot);

            if (!isset($timeslotIndex[$twinfieldRelationNumber])) {
                $timeslotIndex[$twinfieldRelationNumber] = [];
            }

            $timeslotIndex[$twinfieldRelationNumber][] = $timeslot;
        }

        return $timeslotIndex;
    }

    /**
     * @return bool
     *
     * @throws DBALException
     * @throws FloristSyncException
     * @throws ORMException
     * @throws \Exception
     */
    public function import(): bool
    {
        $timeslotIndex = $this->getTimeslotIndex();
        $companyIndex = $this->getCompanyIndex();
        $timeslotRepository = $this->entityManager->getRepository(Timeslot::class);
        $companyEstablishmentRepository = $this->entityManager->getRepository(CompanyEstablishment::class);
        $pickupLocationRepository = $this->entityManager->getRepository(PickupLocation::class);

        if ($this->progressBar) {
            $this->progressBar->start(\count($timeslotIndex));
        }

        $messages = [];

        foreach ($timeslotIndex as $twinfieldRelationNumber => $timeslotItems) {
            if (\count($timeslotItems) === 0) {
                $messages[] = '<info>No timeslots</info>';

                continue;
            }

            if (!isset($companyIndex[$twinfieldRelationNumber])) {
                $messages[] = sprintf('<info>Company with twinfield relation number "%s" not found</info>', $twinfieldRelationNumber);
                continue;
            }

            $company = $companyIndex[$twinfieldRelationNumber];

            /** @noinspection DisconnectedForeachInstructionInspection */
            $this->advance();

            if ($company->has_pickup) {
                $messages[] = sprintf('<info>PickupLocation already exists for companyId: %d</info>',
                    $company->id);
                continue;
            }

            if ($company->company_establishment_id === null) {
                $messages[] = sprintf('<info>No main company establishment found for companyId: %d</info>',
                    $company->id);
                continue;
            }

            $companyEstablishment = $companyEstablishmentRepository->find($company->company_establishment_id);

            $pickupLocation = $pickupLocationRepository->findOneOrCreate([
                'companyEstablishment' => $companyEstablishment
            ]);

            foreach ($timeslotItems as $timeslotItem) {

                /** @var Timeslot $timeslot */
                $timeslot = $timeslotRepository->findOneOrCreate([
                    'from' => new \DateTime($timeslotItem->from),
                    'till' => new \DateTime($timeslotItem->till),
                    'rruleString' => $timeslotItem->rrule_string,
                    'excludeCompanyClosingDates' => true,
                    'excludeHolidays' => true,
                ]);

                $pickupLocation->addTimeslot($timeslot);
            }

            $this->entityManager->persist($pickupLocation);
        }

        $this->entityManager->flush();

        if ($this->progressBar) {
            $this->progressBar->finish();
            $this->output->writeln($messages);
        }

        return true;
    }

    private function advance()
    {
        if ($this->progressBar) {
            $this->progressBar->advance();
        }
    }
}
