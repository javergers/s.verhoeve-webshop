<?php

namespace AppBundle\Services\Sync;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Services\Convert\CompanyService;
use AppBundle\Services\Convert\CustomerService;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Response;

class BloemorderService
{

    use ContainerAwareTrait;

    /**
     * @param Company $company
     * @return string
     */
    public function companySync(Company $company)
    {

        try {

            if (!isset($company->getMetadata()['bloemorderCompanyId'])) {
                throw new \Exception('Company `' . $company->getId() . '` isn\'t imported from bloemorder.');
            }

            $data = $this->container->get(CompanyService::class)
                ->getJsonData($company, true);
            $data = ['company' => $data];

            $response = $this->doRequest($data);

        } catch (\Exception $e) {
            return $e->getMessage();
        }

        if (
            $response->getStatusCode() == 200
            && (strtoupper((string)$response->getBody()) == 'OK')) {
            return new Response('OK', 200);
        };

        return new Response('FAILED', 500);
    }

    /**
     * @param Customer $customer
     * @return string
     */
    public function customerSync(Customer $customer)
    {
        try {

            if (!$customer->getCompany()
                || !isset($customer->getCompany()->getMetadata()['bloemorderCompanyId'])) {
                throw new \Exception('Customer `' . $customer->getId() . '` isn\'t imported from bloemorder.');
            }

            $data = $this->container->get(CustomerService::class)->getJsonData($customer, false);
            $data = ['customer' => $data];

            $response = $this->doRequest($data);

        } catch (\Exception $e) {
            return $e->getMessage();
        }

        if (
            $response->getStatusCode() == 200
            && (strtoupper((string)$response->getBody()) == 'OK')) {
            return new Response('OK', 200);
        };

        return new Response('FAILED', 500);
    }

    private function doRequest($data = [])
    {
        $authToken = $this->container
            ->getParameter('temp_api_authorization_token');

        $client = $this->container->get("eight_points_guzzle.client.bloemorder");

        $response = $client->post(null, [
            "json" => $data,
            'headers' => [
                'Authorization' => $authToken,
            ],
        ]);

        return $response;
    }

}
