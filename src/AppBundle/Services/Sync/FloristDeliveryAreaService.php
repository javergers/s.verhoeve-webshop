<?php

namespace AppBundle\Services\Sync;

use AppBundle\Client\FloristClient;
use AppBundle\Exceptions\FloristSyncException;
use AppBundle\Services\Sync\AbstractFloristSync;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PDO;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FloristDeliveryAreaService
 * @package AppBundle\Services\Sync
 */
class FloristDeliveryAreaService extends AbstractFloristSync
{
    /**
     * @var InputInterface
     */
    private $input;

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var ProgressBar
     */
    private $progressBar;

    /**
     * @var int
     */
    private $totalItems = 0;

    /**
     * @throws FloristSyncException
     */
    private function getRawDeliveryAreas()
    {
        $response = $this->floristClient->get('/export/exportDeliveryAreas');

        if (strpos($response->getHeaderLine('Content-Type'), 'application/json') === false) {
            throw new FloristSyncException(sprintf("Expected 'application/json', got '%s'",
                $response->getHeaderLine('Content-Type')));
        }

        $rawDeliveryAreas = json_decode($response->getBody());

        $this->totalItems = \count($rawDeliveryAreas);

        if (false === $rawDeliveryAreas) {
            throw new FloristSyncException('Invalid json');
        }

        $rawDeliveryAreasIndex = [];

        foreach ($rawDeliveryAreas as $rawDeliveryArea) {
            $twinfield = $this->determineTwinfieldRelationNumber($rawDeliveryArea);

            if (!isset($rawDeliveryAreasIndex[$twinfield])) {
                $rawDeliveryAreasIndex[$twinfield] = [];
            }

            $rawDeliveryAreasIndex[$twinfield][] = $rawDeliveryArea;
        }

        return $rawDeliveryAreasIndex;
    }

    /**
     * @return bool
     * @throws DBALException
     * @throws FloristSyncException
     * @throws Exception
     */
    public function import(): bool
    {
        $rawDeliveryAreas = $this->getRawDeliveryAreas();

        if (empty($rawDeliveryAreas)) {
            throw new \RuntimeException('No delivery areas found');
        }

        if ($this->progressBar) {
            $this->progressBar->start($this->totalItems);
        }

        $connection = $this->entityManager->getConnection();

        $notifications = [];

        $companyIndex = $this->getCompanyIndex();
        $postcodeIndex = $this->getPostcodeIndex();

        foreach ($rawDeliveryAreas as $twinfieldRelationNumber => $rawDeliveryAreaArray) {
            if (!isset($companyIndex[$twinfieldRelationNumber])) {
                $notifications[] = sprintf('<info>Florist with twinfield relation number "%s" not found.</info>',
                    $twinfieldRelationNumber);
                continue;
            }

            $company = $companyIndex[$twinfieldRelationNumber];

            $existingDeliveryAreaIndex = $this->getExistingDeliveryAreaIndex($company);
            $existingDeliveryAreaDeleteIndex = $existingDeliveryAreaIndex;

            foreach ($rawDeliveryAreaArray as $rawDeliveryArea) {
                if ($this->progressBar) {
                    $this->progressBar->advance();
                }

                $country = $rawDeliveryArea->country;
                $postcodeStr = $rawDeliveryArea->postcode;
                $postcodeKey = $country . '_' . $postcodeStr;

                if (!isset($postcodeIndex[$postcodeKey])) {
                    $notifications[] = sprintf('<info>Postcode "%s" not found</info>',
                        $rawDeliveryArea->postcode);
                    continue;
                }

                $postcode = $postcodeIndex[$postcodeKey];

                $existingDeliveryArea = current(array_filter($existingDeliveryAreaIndex,
                    function ($da) use ($postcode, $company) {
                        return $da->postcode_id === $postcode->id
                            && $da->company_id === $company->id;
                    }));

                if (!$existingDeliveryArea) {
                    $existingDeliveryAreaId = $this->createDeliveryArea($postcode, $company, $rawDeliveryArea);
                } else {
                    $existingDeliveryAreaId = $this->updateDeliveryArea($existingDeliveryArea->id, $rawDeliveryArea);
                }

                $existingDeliveryAreaDeleteIndex = array_filter($existingDeliveryAreaDeleteIndex,
                    function ($da) use ($postcode, $company) {
                        return $da->postcode_id !== $postcode->id
                            && $da->company_id === $company->id;
                    }
                );

                if (!$existingDeliveryAreaId) {
                    continue;
                }

                $deliveryAreaId = (int)$existingDeliveryAreaId;
                $position = (int)$rawDeliveryArea->position;

                if ($position > 0) {
                    $preferredSuppliers = $this->findPreferredSuppliers($deliveryAreaId);

                    if (empty($preferredSuppliers)) {
                        $this->createPreferredSupplier($deliveryAreaId, $position);
                    } else {
                        $this->updatePreferredSupplier($deliveryAreaId, $position);
                    }
                } else {
                    $this->deletePreferredSuppliers([$deliveryAreaId]);
                }
            }

            $deliveryAreaIdsToDelete = array_map(function ($existingDeliveryAreaDeleteItem) {
                return $existingDeliveryAreaDeleteItem->id;
            }, $existingDeliveryAreaDeleteIndex);

            if (count($deliveryAreaIdsToDelete) > 0) {
                $this->deletePreferredSuppliers($deliveryAreaIdsToDelete);
                $this->deleteDeliveryAreasByIds($deliveryAreaIdsToDelete);
            }

            unset($rawDeliveryAreas[$twinfieldRelationNumber]);
        }

        if ($this->progressBar) {
            $this->progressBar->finish();
        }

        if (!empty($notifications) && $this->output) {
            $this->output->writeln($notifications);
        }

        return true;
    }

    /**
     * @param $company
     *
     * @return array
     * @throws DBALException
     */
    private function getExistingDeliveryAreaIndex($company)
    {
        $connection = $this->entityManager->getConnection();

        $sql = 'SELECT * FROM supplier_delivery_area WHERE company_id = :companyId';

        $stmt = $connection->prepare($sql);
        $stmt->execute([
            'companyId' => $company->id,
        ]);

        return $stmt->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * @return array
     * @throws DBALException
     */
    private function getPostcodeIndex()
    {
        $postcodeIndex = [];

        $connection = $this->entityManager->getConnection();

        $sql = '
          SELECT  pos.id
            ,     pos.postcode
            ,     pro.country
            FROM postcode pos 
            LEFT JOIN province pro ON pro.id = pos.province_id
        ';

        $postcodes = $connection->query($sql)->fetchAll(PDO::FETCH_OBJ);

        foreach ($postcodes as $postcode) {
            $key = $postcode->country . '_' . $postcode->postcode;
            $postcodeIndex[$key] = $postcode;
        }

        return $postcodeIndex;
    }

    /**
     * @throws DBALException
     */
    private function getCompanyIndex()
    {
        $connection = $this->entityManager->getConnection();

        $sql = '
            SELECT `c`.`id`
              , `c`.`chamber_of_commerce_number`
              , `c`.`twinfield_relation_number`
              , `a`.`country`
            FROM `company` `c`
            LEFT JOIN `company_supplier_group` AS `cosg` ON `cosg`.`company_id` = `c`.`id`
            LEFT JOIN `address` `a` ON `a`.`company_id` = `c`.`id`
            WHERE
                `c`.`is_supplier` = 1
                AND `cosg`.`supplier_group_id` = :supplierGroupId
                AND `a`.`main_address` = 1
        ';

        $stmt = $connection->prepare($sql);
        $stmt->execute([
            "supplierGroupId" => $this->getFloristSupplierGroup()->getId(),
        ]);

        $companies = $stmt->fetchAll(PDO::FETCH_OBJ);

        $companyIndex = [];

        foreach ($companies as $company) {
            $companyIndex[$company->twinfield_relation_number] = $company;
        }

        if (empty($companyIndex)) {
            throw new \RuntimeException('No companies found');
        }

        return $companyIndex;
    }

    /**
     * @param object $postcode
     * @param object $company
     * @param object $rawDeliveryArea
     *
     * @return string
     * @throws DBALException
     */
    private function createDeliveryArea(object $postcode, object $company, object $rawDeliveryArea)
    {
        $connection = $this->entityManager->getConnection();

        $sql = '
          INSERT INTO supplier_delivery_area (`postcode_id`, `company_id`, `delivery_price`, `delivery_interval`)
          VALUES (:postcode, :company, :deliveryPrice, :deliveryInterval)
        ';

        $stmt = $connection->prepare($sql);

        $stmt->execute([
            'postcode' => $postcode->id,
            'company' => $company->id,
            'deliveryPrice' => (float)$rawDeliveryArea->delivery_price,
            'deliveryInterval' => $rawDeliveryArea->delivery_interval,
        ]);

        return (int)$connection->lastInsertId();
    }

    /**
     * @param int $deliveryAreaId
     * @param object $rawDeliveryArea
     *
     * @return int
     * @throws DBALException
     */
    private function updateDeliveryArea(int $deliveryAreaId, object $rawDeliveryArea)
    {
        $connection = $this->entityManager->getConnection();

        $sql = '
          UPDATE `supplier_delivery_area` 
          SET delivery_price = :deliveryPrice, delivery_interval = :deliveryInterval
          WHERE id = :id
        ';

        $stmt = $connection->prepare($sql);

        $stmt->execute([
            'deliveryPrice' => (float)$rawDeliveryArea->delivery_price,
            'deliveryInterval' => $rawDeliveryArea->delivery_interval,
            'id' => $deliveryAreaId,
        ]);

        return (int)$deliveryAreaId;
    }

    /**
     * @param array $deliveryAreaIdsToDelete
     *
     * @throws DBALException
     */
    private function deleteDeliveryAreasByIds(array $deliveryAreaIdsToDelete): void
    {
        $connection = $this->entityManager->getConnection();

        // verplaatsen naar eigen method
        $sql = 'DELETE FROM `supplier_delivery_area` WHERE id IN (:ids)';

        $stmt = $connection->prepare($sql);

        $stmt->execute([
            'ids' => implode(',', $deliveryAreaIdsToDelete),
        ]);
    }

    /**
     * @param int $deliveryAreaId
     * @param int|null $viewCompanyId
     *
     * @return mixed
     * @throws DBALException
     */
    private function findPreferredSuppliers(int $deliveryAreaId, ?int $viewCompanyId = null)
    {
        $connection = $this->entityManager->getConnection();

        $sql = '
            SELECT * 
            FROM company_preferred_supplier
            WHERE
              `delivery_area_id` = :deliveryAreaId
        ';

        $binds = [
            'deliveryAreaId' => $deliveryAreaId,
        ];

        if ($viewCompanyId) {
            $sql .= ' AND `company_id` = :viewCompanyId';
            $binds['viewCompanyId'] = $viewCompanyId;
        } else {
            $sql .= ' AND `company_id` IS NULL';
        }

        $stm = $connection->prepare($sql);

        $stm->execute($binds);

        return $stm->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * @param int $deliveryAreaId
     * @param int|null $position
     * @param int|null $viewCompanyId
     *
     * @return bool
     *
     * @throws DBALException
     */
    private function createPreferredSupplier(int $deliveryAreaId, int $position, ?int $viewCompanyId = null)
    {
        $connection = $this->entityManager->getConnection();

        $sql = '
            INSERT INTO company_preferred_supplier
                (`company_id`, `delivery_area_id`, `position`)
            VALUES 
                (:viewCompanyId, :deliveryAreaId, :position)
        ';

        $stm = $connection->prepare($sql);

        return $stm->execute([
            'viewCompanyId' => $viewCompanyId,
            'deliveryAreaId' => $deliveryAreaId,
            'position' => $position,
        ]);
    }

    /**
     * @param int $deliveryAreaId
     * @param int $position
     * @param int|null $viewCompanyId
     *
     * @return bool
     *
     * @throws DBALException
     */
    private function updatePreferredSupplier(int $deliveryAreaId, int $position, ?int $viewCompanyId = null)
    {
        $connection = $this->entityManager->getConnection();

        $sql = '
            UPDATE
                `company_preferred_supplier`
            SET
                `company_id` = ?
                , `delivery_area_id` = ?
                , `position` = ?
            WHERE
                `delivery_area_id` = ?
        ';

        $binds = [
            $viewCompanyId,
            $deliveryAreaId,
            $position,
            $deliveryAreaId,
        ];

        if ($viewCompanyId) {
            $sql .= ' AND `company_id` = ?';
            $binds[] = $viewCompanyId;
        } else {
            $sql .= ' AND `company_id` IS NULL';
        }

        $stm = $connection->prepare($sql);

        return $stm->execute($binds);
    }

    /**
     * @param array $deliveryAreaIds
     * @param int|null $viewCompanyId
     *
     * @return void
     * @throws DBALException
     */
    private function deletePreferredSuppliers(array $deliveryAreaIds, ?int $viewCompanyId = null): void
    {
        $connection = $this->entityManager->getConnection();

        $sql = '
            DELETE FROM `company_preferred_supplier`
            WHERE 
                `delivery_area_id` IN (:deliveryAreaIds)
        ';

        $binds = ['deliveryAreaIds' => implode(',', $deliveryAreaIds)];

        if ($viewCompanyId) {
            $sql .= ' AND `company_id` = :companyId';
            $binds['companyId'] = $viewCompanyId;
        } else {
            $sql .= ' AND `company_id` IS NULL';
        }

        $stm = $connection->prepare($sql);
        $stm->execute($binds);
    }

    /**
     * @param InputInterface $input
     *
     * @return FloristDeliveryAreaService
     */
    public function setInput(InputInterface $input): FloristDeliveryAreaService
    {
        $this->input = $input;

        return $this;
    }

    /**
     * @param OutputInterface $output
     *
     * @return FloristDeliveryAreaService
     */
    public function setOutput(OutputInterface $output): FloristDeliveryAreaService
    {
        $this->output = $output;

        if ($output && $output->isVerbose()) {
            $this->progressBar = new ProgressBar($this->output);
        }

        return $this;
    }
}
