<?php

namespace AppBundle\Services;

use AppBundle\Entity\Site\Domain as DomainEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class Domain
 *
 * @package AppBundle\Services
 */
class Domain
{
    private $entityManager;
    private $requestStack;
    private $domain;

    /**
     * Domain constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param RequestStack  $requestStack
     */
    public function __construct(EntityManagerInterface $entityManager, RequestStack $requestStack)
    {
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
    }

    /**
     *
     * @return DomainEntity
     */
    public function getDomain()
    {
        if ($this->domain == null && $this->requestStack->getMasterRequest()) {

            $hostname = $this->requestStack->getMasterRequest()->getHost();

            $this->domain = $this->entityManager->getRepository(DomainEntity::class)->findOneBy([
                'domain' => $hostname,
            ]);
        }

        return $this->domain;
    }
}
