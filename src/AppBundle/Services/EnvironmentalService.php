<?php

namespace AppBundle\Services;

use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class EnvironmentalService
 * @package AppBundle\Services
 */
class EnvironmentalService
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @param array $environments
     * @return bool
     */
    public function is(array $environments = [])
    {
        if (in_array($this->kernel->getEnvironment(), $environments, true)) {
            return true;
        }

        return false;
    }
}
