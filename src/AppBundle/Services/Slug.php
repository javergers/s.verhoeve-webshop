<?php

namespace AppBundle\Services;

use AppBundle\Entity\Site\Domain;
use AppBundle\Entity\Site\Site;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Class Slug
 * @package AppBundle\Services
 */
class Slug
{
    /**
     * @var EntityManagerInterface $manager
     */
    private $manager;

    /**
     * @var Router $router
     */
    private $router;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var Domain $lookup
     */
    private $lookup;

    /**
     * @var String
     */
    private $defaultLocale;

    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * Slug constructor.
     * @param EntityManagerInterface         $manager
     * @param Router                $router
     * @param RequestStack          $requestStack
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(EntityManagerInterface $manager, Router $router, RequestStack $requestStack, ParameterBagInterface $parameterBag)
    {
        $this->manager = $manager;
        $this->router = $router;
        $this->requestStack = $requestStack;
        $this->parameterBag = $parameterBag;
    }

    /**
     * @return Request|null
     */
    private function getRequest(): ?Request
    {
        $request = $this->requestStack->getCurrentRequest();

        if ($request !== null){
            $this->defaultLocale = $request->getDefaultLocale();
        } else {
            $this->defaultLocale = $this->parameterBag->get('locale');
        }

        return $request;
    }

    public function createLookupData(): void
    {
        if (\is_array($this->lookup)) {
            return;
        }

        $qb = $this->manager->getRepository(Domain::class)->createQueryBuilder('d')
            ->leftJoin('d.site', 's')
            ->where('s.deletedAt is NULL');

        $query = $qb->getQuery();

        $this->lookup = [];

        $domains = $query->getResult();

        /** @var Domain $domain */
        foreach ($domains as $domain) {
            if (!array_key_exists($domain->getSite()->getId(), $this->lookup)) {
                $this->lookup[$domain->getSite()->getId()] = [];
            }
            foreach($domain->getSite()->getAvailableLocales() as $locale) {
                $this->lookup[$domain->getSite()->getId()][$locale] = $domain->getDomain();
            }
        }
    }

    /**
     * @param           $entity
     * @param null      $locale
     * @param Site|null $site
     * @param null      $referenceType
     * @return null|string
     */
    public function getUrl($entity, $locale = null, Site $site = null, $referenceType = null): ?string
    {
        $this->createLookupData();

        $routeName = $this->getRouteName($entity, $locale, $site);

        try {
            return $this->router->getGenerator()->generate($routeName, [], $referenceType);
        } catch (RouteNotFoundException $e) {
            return null;
        }
    }

    /**
     * @param           $entity
     * @param null      $locale
     * @param Site|null $site
     * @return string
     */
    public function getRouteName($entity, $locale = null, Site $site = null): string
    {
        $this->createLookupData();

        $request = $this->getRequest();

        if ($locale === null) {
            $locale = $request? $request->getLocale(): $this->defaultLocale;
        }

        if ($site === null && $request !== null) {
            $name = DynamicRoutesService::generateRouteNamePrefix($entity) . '-' . $request->getHost() . '-' . $locale;
        } else {
            $siteSlug = null;

            if ($site->getParent() && $site->getDomains()->isEmpty()) {
                $site = $site->getParent();
            }

            $hostname = $this->lookup[$site->getId()][$locale];

            $name = DynamicRoutesService::generateRouteNamePrefix($entity) . '-' . $hostname . '-' . $locale;
        }

        return $name;
    }
}
