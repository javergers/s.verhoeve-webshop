<?php

namespace AppBundle\Services\Supplier;

use AppBundle\Exceptions\InvalidHashException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class HashService
 * @package AppBundle\Services
 */
class HashService
{
    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;

    /**
     * HashService constructor.
     *
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    /**
     * @param $hashObj
     * @return object
     * @throws InvalidHashException
     */
    private function resolveHash($hashObj)
    {
        $resolver = new OptionsResolver();

        $resolver->setDefaults([
            'orderId' => null,
            'orderLineId' => null,
            'data' => '',
        ]);

        $resolver->setRequired([
            'type',
            'supplierId'
        ]);

        $resolver->setAllowedTypes('type', 'string');
        $resolver->setAllowedTypes('orderId', ['int', 'null']);
        $resolver->setAllowedTypes('orderLineId', ['int', 'null']);
        $resolver->setAllowedTypes('supplierId', 'int');
        $resolver->setAllowedTypes('data', ['array', 'string']);

        try {
            return (object)$resolver->resolve((array)$hashObj);
        } catch (\Exception $exception) {
            throw new InvalidHashException();
        }
    }

    /**
     * @param \stdClass $newHashObj
     * @return string
     */
    public function encryptHash(\stdClass $newHashObj)
    {
        $data = '';

        $hashObj = new \stdClass();

        if (!empty($newHashObj->type)) {
            $hashObj->type = $newHashObj->type;
        }
        if (!empty($newHashObj->data)) {
            $hashObj->data = $newHashObj->data;
        }
        if (!empty($newHashObj->orderId)) {
            $hashObj->orderId = $newHashObj->orderId;
        }
        if(!empty($newHashObj->orderLineId)) {
            $hashObj->orderLineId = $newHashObj->orderLineId;
        }
        if(!empty($newHashObj->supplierId)) {
            $hashObj->supplierId = $newHashObj->supplierId;
        }

        switch ($hashObj->type) {
            case 'deliveryStatus':
            case 'processStatus':
            case 'packingslip':
                $data = (string)$hashObj->orderId;
                break;

            case 'design':
                $data = (string)$hashObj->orderLineId;
                break;
        }

        $data .= ':' . $hashObj->supplierId;

        if (!empty($hashObj->data)) {
            $data .= ':' . http_build_query($hashObj->data);
        }

        $hashObj->secret = $this->parameterBag->get('secret');

        $data .= ':' . md5(json_encode($hashObj));

        return base64_encode($data);
    }

    /**
     * @param $hash
     * @param $type
     * @return \stdClass
     * @throws InvalidHashException
     */
    public function decryptHash($type, $hash)
    {
        $parts = explode(':', base64_decode($hash));

        // Fix requests without data property.
        if (count($parts) === 3) {
            $parts[3] = $parts[2];
            $parts[2] = false;
        }

        if (!isset($parts[0], $parts[1], $parts[2], $parts[3])) {
            throw new InvalidHashException;
        }

        $hashObj = new \stdClass();

        $hashObj->type = $type;

        if (!empty($parts[2])) {
            parse_str($parts[2], $data);
            $hashObj->data = $data;
        }

        switch ($type) {
            case 'deliveryStatus':
            case 'processStatus':
            case 'packingslip':
                $hashObj->orderId = (int)$parts[0];
                break;

            case 'design':
                $hashObj->orderLineId = (int)$parts[0];
                break;
        }

        $hashObj->supplierId = (int)$parts[1];

        $hashObj->secret = $this->parameterBag->get('secret');

        $sig = $parts[3];

        if ($sig !== md5(json_encode($hashObj))) {
            throw new InvalidHashException;
        }

        unset($hashObj->secret);

        return $this->resolveHash($hashObj);
    }
}