<?php

namespace AppBundle\Services\Supplier;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Supplier\SupplierGroup;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class Bakery
 * @package AppBundle\Services\Supplier
 */
class Bakery
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $bakkerApiUrl;

    /**
     * @var string
     */
    private $apiAuthorizationToken;

    /**
     * Bakery constructor.
     * @param EntityManagerInterface $entityManager
     * @param Client $guzzleClientBakery
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        Client $guzzleClientBakery,
        ParameterBagInterface $parameterBag
    ) {
        $this->entityManager = $entityManager;
        $this->client = $guzzleClientBakery;
        $this->bakkerApiUrl = $parameterBag->get('connector_bakker_api_url');
        $this->apiAuthorizationToken = $parameterBag->get('temp_api_authorization_token');
    }

    /**
     * @param Company $company
     * @return mixed
     * @throws \Exception
     */
    public function sync(Company $company)
    {
        if (null === $this->bakkerApiUrl) {
            throw new \RuntimeException('No export url set to sync bakery suppliers.');
        }

        $bakeryGroup = $this->entityManager->getRepository(SupplierGroup::class)->findOneBy([
            'name' => 'Bakkers',
        ]);

        $isBakery = $company->getSupplierGroups()->exists(function ($key, $supplierGroup) use ($bakeryGroup) {
            void($key);

            return $supplierGroup === $bakeryGroup;
        });

        if (!$isBakery) {
            throw new \RuntimeException('Only companies which belong to the bakery supplier group can be synced');
        }

        /** @var Customer $customer */
        $customer = $company->getCustomers()->filter(function (Customer $customer) {
            return $customer->isEnabled();
        })->first();

        $user = null;
        $address = null;

        if ($customer) {
            $user = [
                'gender' => $customer->getGender(),
                'firstname' => $customer->getFirstname(),
                'lastname' => $customer->getLastname(),
                'username' => $customer->getUsername(),
                'password' => $customer->getPassword(),
                'email' => $customer->getEmail(),
                'phone' => $customer->getPhoneNumber(),
            ];
        }

        if ($company->getDefaultDeliveryAddress()) {
            $address = [
                'name' => $company->getDefaultDeliveryAddress()->getCompanyName(),
                'street' => $company->getDefaultDeliveryAddress()->getStreet(),
                'number' => $company->getDefaultDeliveryAddress()->getNumber(),
                'postcode' => $company->getDefaultDeliveryAddress()->getPostcode(),
                'city' => $company->getDefaultDeliveryAddress()->getCity(),
                'country' => $company->getDefaultDeliveryAddress()->getCountry()->getCode(),
            ];
        }

        $data = [
            'id' => $company->getId(),
            'parent_id' => $company->getParent() && $company->getParent()->getHandleChildOrders() ? $company->getParent()->getId() : null,
            'name' => $company->getName(),
            'user' => $user,
            'address' => $address,
            'active' => true,
        ];

        return $this->doRequest(
            $this->bakkerApiUrl . '/tgapi/import/baker',
            $data
        );
    }

    /**
     * @param       $uri
     * @param array $data
     * @return mixed
     */
    private function doRequest($uri, array $data = [])
    {
        $response = $this->client->post($uri, [
            'json' => $data,
            'headers' => [
                'Authorization' => $this->apiAuthorizationToken,
            ],
        ]);

        return (string)$response->getBody();
    }
}
