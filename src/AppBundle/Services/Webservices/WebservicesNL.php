<?php

namespace AppBundle\Services\Webservices;

use Doctrine;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;
use Snc\RedisBundle\Client\Phpredis\Client;
use SoapClient;
use SoapFault;
use SoapHeader;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class WebservicesNL
 * @package AppBundle\Services\Webservices
 * @link    https://webview.webservices.nl/account_purchase_view_action_costs/10598
 */
class WebservicesNL
{
    use ContainerAwareTrait;

    private $endpoints = [
        'https://ws1.webservices.nl/soap_doclit?wsdl',
        'https://ws2.webservices.nl/soap_doclit?wsdl',
    ];

    /**
     * @var string
     */
    private $endpoint;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var SoapClient
     */
    private $client;

    /**
     * @var Client
     */
    private $redis;

    /**
     * WebservicesNL constructor.
     * @param           $username
     * @param           $password
     * @param Container $container
     * @throws \Exception
     */
    public function __construct($username, $password, Container $container)
    {
        $this->username = $username;
        $this->password = $password;
        $this->container = $container;

        $this->redis = $container->get('snc_redis.default');

        $this->endpoint = $this->endpoints[0];
    }

    /**
     * Get account balance
     *
     * @return float
     * @throws SoapFault
     */
    public function getBalance()
    {
        $response = $this->doRequest('accountViewBalance', [
            'accountid' => $this->username,
        ], 60);

        return (float)$response->balance;
    }

    /**
     * @param      $chamberOfCommerceNumber
     * @param null $chamberOfCommerceEstablishmentNumber
     * @return bool
     * @throws \RuntimeException
     */
    public function isValidChamberOfCommerceNumber(
        $chamberOfCommerceNumber,
        $chamberOfCommerceEstablishmentNumber = null
    ) {
        try {
            $response = $this->doRequest('dutchBusinessGetDossier', [
                'dossier_number' => $chamberOfCommerceNumber,
                'establishment_number' => $chamberOfCommerceEstablishmentNumber,
            ], 60 * 60 * 24 * 7)->out;
        } catch (\Exception $e) {

            // Not enough credits
            if ($e->getCode() === 0) {
                throw new \RuntimeException('Er zijn niet genoeg credits aanwezig in het account.', 0);
            }

            return false;
        }

        return ($response && $this->extractName($response));
    }

    /**
     * @param $response \stdClass
     * @return mixed
     */
    protected function extractName($response)
    {
        $legalNames = [
            $response->trade_name_full,
            $response->trade_name_45,
            $response->legal_name,
        ];

        return current(array_filter($legalNames));
    }

    /**
     * @param      $chamberOfCommerceNumber
     * @param null $chamberOfCommerceEstablishmentNumber
     * @return \stdClass
     * @throws SoapFault
     * @throws \Exception
     * @throws \libphonenumber\NumberParseException
     */
    public function getCompanyDataByChamberOfCommerceNumber(
        $chamberOfCommerceNumber,
        $chamberOfCommerceEstablishmentNumber = null
    ) {
        try {
            $response = $this->doRequest('dutchBusinessGetDossier', [
                'dossier_number' => $chamberOfCommerceNumber,
                'establishment_number' => $chamberOfCommerceEstablishmentNumber,
            ], 60 * 60 * 24 * 7)->out;
        } catch (\Exception $e) {
            throw new \RuntimeException('Er is iets foutgegaan met het ophalen van bedrijfsgegevens via het kvk nummer: ' . $e->getMessage());
        }

        $return = new \stdClass();
        $return->name = $this->extractName($response);
        $return->chamberOfCommerce = $response->dossier_number;
        $return->chamberOfCommerceEstablishment = @$response->establishment_number;
        $return->chamberOfCommerceMainEstablishment = @$response->main_establishment_number;

        $return->address = $this->container->get('address');
        $return->address->street = $response->establishment_street;
        $return->address->houseNumber = $response->establishment_house_number;
        $return->address->houseNumberAddition = $response->establishment_house_number_addition;
        $return->address->postcode = $response->establishment_postcode;
        $return->address->city = $response->establishment_city;
        $return->address->countryCode = 'NL';
        $return->address->normalize();

        $return->postaddress = $this->container->get('address');
        $return->postaddress->street = $response->correspondence_street;
        $return->postaddress->houseNumber = $response->correspondence_house_number;
        $return->postaddress->houseNumberAddition = $response->correspondence_house_number_addition;
        $return->postaddress->postcode = $response->correspondence_postcode;
        $return->postaddress->city = $response->correspondence_city;
        $return->postaddress->countryCode = (($response->correspondence_country === 'NLD' || empty($response->correspondence_country)) ? 'NL' : null);
        $return->postaddress->normalize();

        if ($response->telephone_number) {
            $number = PhoneNumberUtil::getInstance()->parse($response->telephone_number, 'NL');

            $return->phonenumber = PhoneNumberUtil::getInstance()->format($number, PhoneNumberFormat::E164);
        } else {
            $return->phonenumber = null;
        }

        if ($response->mobile_number) {
            $number = PhoneNumberUtil::getInstance()->parse($response->mobile_number, 'NL');

            $return->mobilenumber = PhoneNumberUtil::getInstance()->format($number, PhoneNumberFormat::E164);
        } else {
            $return->mobilenumber = null;
        }

        return $return;
    }

    /**
     * @param $chamberOfCommerceNumber
     * @return string
     */
    public function getVatNumberByChamberOfCommerceNumber($chamberOfCommerceNumber)
    {
        $vatNumber = null;

        try {
            $response = $this->doRequest('dutchBusinessGetVatNumber', [
                'dossier_number' => $chamberOfCommerceNumber,
            ], 60 * 60 * 24 * 7)->out;

            if ($response->vat_number) {
                $vatNumber = $response->vat_number;
            }
        } catch (SoapFault $e) {
            return $vatNumber;
        }

        return $vatNumber;
    }

    /**
     * @param array $parameters
     * @return array
     */
    public function getAddressSuggestions($parameters)
    {
        try {
            return $this->doRequest('addressPerceelFullParameterSearchV2', $parameters, 60 * 60 * 24 * 7)->out;
        } catch (SoapFault $e) {
            return [];
        }
    }

    /**
     * @param $establishmentNumber
     * @return array|\stdClass
     */
    public function getDetailsByEstablishmentNumber($establishmentNumber)
    {
        $details = [];

        try {
            $result = $this->doRequest('dutchBusinessSearchDossierNumber', [
                'dossier_number' => false,
                'establishment_number' => $establishmentNumber,
                'rsin_number' => false,
                'page' => 1,
            ], 60 * 60 * 24 * 7)->out;

            $chamberOfCommerceNumber = $result->results->item->dossier_number;

            $details = $this->getCompanyDataByChamberOfCommerceNumber($chamberOfCommerceNumber, $establishmentNumber);
        } catch (\Exception $e) {
            print $e->getMessage();
        }

        return $details;
    }

//    /**
//     * @param $vatNumber
//     * @return bool
//     * @throws SoapFault
//     */
//    public function isValidVatNumber($vatNumber)
//    {
//        $response = $this->doRequest("vatValidate", [
//            'vat_number' => $vatNumber
//        ], 60 * 60 * 24 * 365)->out;
//
//        return (bool)$response->valid;
//    }
//
//    /**
//     * @param $vatNumber
//     * @return \stdClass
//     * @throws SoapFault
//     */
//    public function getAddressFromVatNumber($vatNumber)
//    {
//        $response = $this->doRequest("vatViesProxyCheckVat", [
//            'vat_number' => $vatNumber
//        ], 60 * 60 * 24 * 31)->out;
//
//        $parts = explode("\n", $response->address);
//
//        /**
//         * Modified regex from devotis/splitsAdres.js to support unicode addresses
//         * @link https://gist.github.com/devotis/c574beaf73adcfd74997
//         */
//        $regex = "/^(\d*[\w\p{L}\d '\-\.\(\)\/]+)[,\s]+(\d+)\s*([\p{L}\s\d\-\/]*)$/iu";
//
//        preg_match($regex, trim($parts[0]), $parts2);
//
//        $result = new \stdClass();
//        $result->street = trim($parts2[1]);
//        $result->houseNumber = (int)trim($parts2[2]);
//        $result->houseNumberAddition = trim($parts2[3]);
//        $result->postcode = substr($parts[1], 0, strpos($parts[1], " "));
//        $result->city = substr($parts[1], strpos($parts[1], " ") + 1);
//        $result->country = $response->country_code;
//
//        return $result;
//    }

    /**
     * @return SoapClient
     * @throws SoapFault
     */
    private function getClient()
    {
        if (!$this->client) {
            $this->endpoint = current($this->endpoints);

            try {
                $this->client = new SoapClient($this->endpoint);
            } catch (SoapFault $e) {
                $this->switchEndpoint($e);

                return $this->getClient();
            }

            $headerLogin = new SoapHeader(
                'http://www.webservices.nl/soap/',
                'HeaderLogin',
                [
                    'username' => $this->username,
                    'password' => $this->password,
                ],
                true
            );
            $this->client->__setSoapHeaders($headerLogin);
        }

        return $this->client;
    }

    /**
     * @param SoapFault $lastException
     * @throws SoapFault
     * @return bool
     */
    private function switchEndpoint(SoapFault $lastException)
    {
        if (!($endpoint = next($this->endpoints))) {
            throw $lastException;
        }

        $this->endpoint = $endpoint;
        $this->client = null;

        return true;
    }

    /**
     * @param     $method
     * @param     $params
     * @param int $cacheTTL
     * @return bool|mixed|string
     * @throws SoapFault
     */
    private function doRequest($method, $params, $cacheTTL = 0)
    {
        $key = $this->calculateCachingKey($method, $params);

        if ($cacheTTL === 0 || !($response = $this->redis->get($key))) {
            try {
                $response = \call_user_func([$this->getClient(), $method], $params);
            } catch (SoapFault $e) {
                if ($e->getCode() === 'HTTP') {
                    $this->switchEndpoint($e);

                    $response = $this->doRequest($method, $params);
                } else {
                    throw $e;
                }
            }

            if ($cacheTTL > 0) {
                $this->redis->set($key, serialize($response), $cacheTTL);
            }
        } else {
            $response = unserialize($response, []);
        }

        return $response;
    }

    /**
     * @param $method
     * @param $params
     * @return string
     */
    private function calculateCachingKey($method, $params)
    {
        return 'webservices.nl:' . $method . ':' . json_encode(array_filter(array_values($params)));
    }
}
