<?php

namespace AppBundle\Services\Webservices;

use Doctrine;
use Redis;
use SoapClient;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class VatInformationExchangeSystem
 * @package AppBundle\Services\Webservices
 */
class VatInformationExchangeSystem
{
    use ContainerAwareTrait;

    /**
     * @var string $endpoint
     */
    private $endpoint = "http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl";

    /**
     * @var SoapClient $client
     */
    private $client;

    /**
     * @var Redis $redis
     */
    private $redis;

    /**
     * VatInformationExchangeSystem constructor.
     * @param Redis     $redis
     * @param Container $container
     */
    public function __construct(Redis $redis, Container $container)
    {
        $this->redis = $redis;
        $this->container = $container;
    }

    /**
     * @param $vatNumber
     * @return bool
     */
    public function isValidVatNumber($vatNumber)
    {
        $response = $this->doRequest("checkVat", [
            'countryCode' => substr($vatNumber, 0, 2),
            'vatNumber' => substr($vatNumber, 2),
        ], 60 * 60 * 24 * 31);

        return (bool)$response->valid;
    }

    /**
     * @param $vatNumber
     * @return \stdClass
     */
    public function getCompanyNameByVatNumber($vatNumber)
    {
        $response = $this->doRequest("checkVat", [
            'countryCode' => substr($vatNumber, 0, 2),
            'vatNumber' => substr($vatNumber, 2),
        ], 60 * 60 * 24 * 31);

        $return = new \stdClass();
        $return->name = $response->name;

        return $return;
    }

    /**
     * @param $vatNumber
     * @return \AppBundle\Utils\Address|object
     * @throws \Exception
     */
    public function getAddressByVatNumber($vatNumber)
    {
        $response = $this->doRequest("checkVat", [
            'countryCode' => substr($vatNumber, 0, 2),
            'vatNumber' => substr($vatNumber, 2),
        ], 60 * 60 * 24 * 31);

        $return = $this->container->get("address");
        $return->parseAddress($response->address);
        $return->countryCode = $response->countryCode;
        $return->normalize();

        return $return;
    }

    /**
     * @param $vatNumber
     * @return \stdClass
     * @throws \Exception
     */
    public function getCompanyDataByVatNumber($vatNumber)
    {
        try {
            $response = $this->doRequest("checkVat", [
                'countryCode' => substr($vatNumber, 0, 2),
                'vatNumber' => substr($vatNumber, 2),
            ], 60 * 60 * 24 * 31);
        } catch (\Exception $e) {
            throw new \RuntimeException('Er is iets foutgegaan bij het ophalen van bedrijfsgegevens via het BTW nummer');
        }


        $return = new \stdClass();
        $return->name = $response->name;
        $return->vatNumber = $vatNumber;

        $return->address = $this->container->get("address");
        $return->address->parseAddress($response->address);
        $return->address->countryCode = $response->countryCode;
        $return->address->normalize();

        return $return;
    }

    /**
     * @return SoapClient
     */
    private function getClient()
    {
        if (!$this->client) {
            $this->client = new SoapClient($this->endpoint);
        }

        return $this->client;
    }

    /**
     * @param     $method
     * @param     $params
     * @param int $cacheTTL
     * @return bool|mixed|string
     */
    private function doRequest($method, $params, $cacheTTL = 0)
    {
        $key = $this->calculateCachingKey($params);

        if ($cacheTTL == 0 || !($response = $this->redis->get($key))) {
            $response = call_user_func([$this->getClient(), $method], $params);

            if ($cacheTTL > 0) {
                $this->redis->set($key, serialize($response), $cacheTTL);
            }
        } else {
            $response = unserialize($response, []);
        }

        return $response;
    }

    /**
     * @param $params
     * @return string
     */
    private function calculateCachingKey($params)
    {
        return "ec.europa.eu:vies:" . json_encode(array_filter(array_values($params)));
    }
}
