<?php /** @noinspection SummerTimeUnsafeTimeManipulationInspection */

namespace AppBundle\Services\Webservices;

use Doctrine;
use GuzzleHttp;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use Redis;

/**
 * Class PostcodeNL
 * @package AppBundle\Services\Webservices
 */
class PostcodeNL
{
    /**
     * @var GuzzleHttp\Client $guzzleClient
     */
    private $guzzleClient;

    /**
     * @var Redis $redis
     */
    private $redis;

    /**
     * @var string
     */
    private $base_uri_host;

    /**
     * PostcodeNL constructor.
     * @param GuzzleHttp\Client $guzzleClient
     * @param Redis             $redis
     */
    public function __construct(GuzzleHttp\Client $guzzleClient, Redis $redis)
    {
        $this->guzzleClient = $guzzleClient;

        if (null === ($baseUrl = $this->guzzleClient->getConfig('base_uri'))) {
            throw new \RuntimeException('No base uri configured');
        }

        $this->redis = $redis;
        $this->base_uri_host = $baseUrl->getHost();
    }

    /**
     * @param      $postcode
     * @param      $houseNumber
     * @param null $houseNumberAddition
     * @return \stdClass
     * @throws GuzzleException
     */
    public function getAddress($postcode, $houseNumber, $houseNumberAddition = null)
    {
        $result = $this->get('/rest/addresses/' . $postcode . '/' . $houseNumber . '/' . $houseNumberAddition);

        $return = new \stdClass();
        $return->street = $result->street;
        $return->houseNumber = $result->houseNumber;
        $return->houseNumberAddition = (string)$result->houseNumberAddition;
        $return->houseNumberAdditions = $result->houseNumberAdditions;
        $return->postcode = $result->postcode;
        $return->city = $result->city;
        $return->country = 'NL';

        return $return;
    }

    /**
     * @param $url
     * @return mixed
     * @throws GuzzleException
     */
    private function get($url)
    {
        $key = $this->calculateCachingKey($url);

        $request = new GuzzleHttp\Psr7\Request('GET', $url);

        if (!($response = $this->redis->get($key))) {
            try {
                $response = $this->guzzleClient->send($request);

                $this->redis->set($key, GuzzleHttp\Psr7\str($response), 60 * 60 * 24 * 365);
            } catch (GuzzleHttp\Exception\ClientException $e) {
                if (null !== $e->getResponse() && $e->getResponse()->getStatusCode() === 404) {
                    $this->redis->set($key, GuzzleHttp\Psr7\str($e->getResponse()), 60 * 60 * 24 * 31);
                }

                throw $e;
            }
        } else {
            $response = GuzzleHttp\Psr7\parse_response($response);

            if ($response->getStatusCode() >= 400) {
                throw GuzzleHttp\Exception\RequestException::create($request, $response);
            }
        }

        return $this->parseResponse($response);
    }

    /**
     * @param $url
     * @return string
     */
    private function calculateCachingKey($url)
    {
        preg_match('|/rest/([a-z]*)/|', $url, $match);

        $method = $match[1];
        $params = json_encode(array_values(explode('/', trim(str_replace($match[0], '', $url), '/'))));

        return $this->base_uri_host . ':' . $method . ':' . $params;
    }

    /**
     * @param Response $response
     * @return mixed
     */
    private function parseResponse(Response $response)
    {
        return json_decode($response->getBody());
    }
}
