<?php

namespace AppBundle\Services\Webservices;

use AppBundle\Entity\Security\Customer\Customer;
use Doctrine;
use GuzzleHttp;
use libphonenumber\PhoneNumberFormat;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class Copernica
 *
 * @package AppBundle\Services\Webservices
 */
class Copernica
{
    use ContainerAwareTrait;

    /**
     * @var GuzzleHttp\Client
     */
    private $guzzleClient;

    private $database;

    private $accessToken;

    /**
     * @param GuzzleHttp\Client $guzzleClient
     * @param                   $database
     * @param                   $accessToken
     */
    public function __construct(GuzzleHttp\Client $guzzleClient, $database, $accessToken)
    {
        $this->guzzleClient = $guzzleClient;
        $this->database = $database;
        $this->accessToken = $accessToken;
    }

    /**
     * @param Customer $customer
     *
     * @return bool
     * @throws \Exception
     */
    public function customerExists(Customer $customer)
    {
        return (bool)$this->getCustomer($customer);
    }

    /**
     * @param Customer $customer
     *
     * @return null
     * @throws \Exception
     */
    public function getCustomer(Customer $customer)
    {
        $result = $this->get("/database/" . $this->database . "/profiles", [
            "fields" => [
                "email==" . $customer->getEmail(),
            ],
        ]);

        if (!array_key_exists("ID", $result)) {
            throw new \Exception("Multiple results found");
        }

        return $result;
    }

    /**
     * @param Customer $customer
     */
    public function createCustomer(Customer $customer)
    {
        $data = $this->buildProfile($customer);

        $this->post("/database/" . $this->database . "/profiles", null, $data);
    }

    /**
     * @param Customer $customer
     */
    public function updateCustomer(Customer $customer)
    {
        $data = $this->buildProfile($customer);

        $this->put("/database/" . $this->database . "/profiles", [
            "fields" => [
                "email==" . $customer->getEmail(),
            ],
        ], $data);

        //2395
    }

    /**
     * @param Customer $customer
     *
     * @return array
     */
    private function buildProfile(Customer $customer)
    {
        $profile = [];
        $profile['customer_id'] = $customer->getId();
        $profile['gender'] = $customer->getGender();
        $profile['name'] = $customer->getFullname();
        $profile['dateOfBirth'] = $customer->getBirthday()->format("Y-m-d");
        $profile['firstname'] = $customer->getFirstname();
//        $profile['prepposition'] = $customer->getLastnamePrefix();
        $profile['lastname'] = $customer->getLastName();
        $profile['email'] = $customer->getEmail();
        $profile['phone'] = $customer->getFormattedPhonenumber(PhoneNumberFormat::E164);
        $profile['mobile'] = $customer->getFormattedMobilenumber(PhoneNumberFormat::E164);

        if (!$customer->getcompany()) {
            $profile['company_id'] = null;
            $profile['company'] = null;
        } else {
            $profile['company_id'] = $customer->getCompany()->getId();
            $profile['company'] = $customer->getCompany()->getName();
        }

        $profile['facebook_id'] = $customer->getFacebookId();
        $profile['customer_group'] = $customer->getCustomerGroup()->getDescription();

        $profile['last_login'] = $customer->getLastLogin() ? $customer->getLastLogin()->format("Y-m-d H:i:s") : null;
        $profile['created_at'] = $customer->getCreatedAt()->format("Y-m-d H:i:s");
        $profile['updated_at'] = $customer->getUpdatedAt()->format("Y-m-d H:i:s");
        $profile['deleted_at'] = $customer->getDeletedAt() ? $customer->getDeletedAt()->format("Y-m-d H:i:s") : null;

        return $profile;
    }

    /**
     * @param       $url
     * @param array $params
     *
     * @return mixed
     */
    private function get($url, $params = [])
    {
        $params += ["access_token" => $this->accessToken];

        $response = $this->guzzleClient->get($url, ["query" => $params]);

        return $this->parseResponse($response);
    }

    /**
     * @param       $url
     * @param array $params
     * @param       $body
     */
    private function post($url, $params = [], $body)
    {
        $params += ["access_token" => $this->accessToken];

        $response = $this->guzzleClient->post($url, [
            "json" => $body,
            "query" => $params,
        ]);

        void($response);
    }

    /**
     * @param       $url
     * @param array $params
     * @param       $body
     */
    private function put($url, $params = [], $body)
    {
        $params += ["access_token" => $this->accessToken];

        $response = $this->guzzleClient->put($url, [
            "json" => $body,
            "query" => $params,
        ]);

        void($response);
    }

    /**
     * @param ResponseInterface $response
     *
     * @return null
     */
    private function parseResponse(ResponseInterface $response)
    {
        $result = json_decode($response->getBody());

        if ($result->data) {
            if (empty($result->data)) {
                return null;
            } elseif (count($result->data) == 1) {
                return $result->data[0];
            } else {
                return $result->data;
            }
        }

        return null;
    }
}
