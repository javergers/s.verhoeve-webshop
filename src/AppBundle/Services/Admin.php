<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Routing\RequestContext;

class Admin
{
    use ContainerAwareTrait;

    /**
     * @return RequestContext
     */
    public function getRequestContext()
    {
        $requestContext = new RequestContext();

        $scheme = $this->container->getParameter('admin_scheme');
        $host = $this->container->getParameter('admin_host');
        $port = $this->container->getParameter('admin_port');

        if (!$scheme) {
            $scheme = 'https';
        }

        $requestContext->setScheme($scheme);
        $requestContext->setHost($host);

        if ($port) {
            switch ($scheme) {
                case 'http':
                    $requestContext->setHttpPort($port);
                    break;
                case 'https':
                    $requestContext->setHttpsPort($port);
                    break;
            }
        }

        return $requestContext;
    }
}