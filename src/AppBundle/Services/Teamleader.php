<?php

namespace AppBundle\Services;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Services\Teamleader\CompanyTransformer;
use AppBundle\Services\Teamleader\CustomerTransformer;
use AppBundle\Services\Teamleader\ProductTransformer;
use AppBundle\ThirdParty\Teamleader\Entity\Company as TeamleaderCompany;
use AppBundle\ThirdParty\Teamleader\Entity\Contact as TeamleaderContact;
use AppBundle\ThirdParty\Teamleader\Entity\Product as TeamleaderProduct;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;

/**
 * Class Teamleader
 * @package AppBundle\Services
 */
class Teamleader
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var ProductTransformer
     */
    private $teamLeaderProductTransformer;

    /**
     * @var ProductTransformer
     */
    private $teamLeaderCustomerTransformer;

    /**
     * @var CompanyTransformer
     */
    private $teamLeaderCompanyTransformer;

    /**
     * @var string
     */
    private $teamleaderApiGroup;

    /**
     * @var string
     */
    private $teamleaderApiKey;

    /**
     * Teamleader constructor.
     * @param EntityManagerInterface $entityManager
     * @param Client $guzzleClientTeamleader
     * @param ProductTransformer $teamLeaderProductTransformer
     * @param CustomerTransformer $teamLeaderCustomerTransformer
     * @param CompanyTransformer $teamLeaderCompanyTransformer
     * @param null|string $teamleaderApiGroup
     * @param null|string $teamleaderApiKey
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        Client $guzzleClientTeamleader,
        ProductTransformer $teamLeaderProductTransformer,
        CustomerTransformer $teamLeaderCustomerTransformer,
        CompanyTransformer $teamLeaderCompanyTransformer,
        ?string $teamleaderApiGroup,
        ?string $teamleaderApiKey
    ) {
        $this->entityManager = $entityManager;
        $this->client = $guzzleClientTeamleader;
        $this->teamLeaderProductTransformer = $teamLeaderProductTransformer;
        $this->teamLeaderCustomerTransformer = $teamLeaderCustomerTransformer;
        $this->teamLeaderCompanyTransformer = $teamLeaderCompanyTransformer;
        $this->teamleaderApiGroup = $teamleaderApiGroup;
        $this->teamleaderApiKey = $teamleaderApiKey;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function test(): bool
    {
        $result = $this->doRequest('/api/helloWorld.php');

        return ($result === 'Successful Teamleader API request.');
    }

    /**
     * @param Product $product
     * @throws \RuntimeException
     * @throws \Exception
     */
    public function syncProduct(Product $product): void
    {
        if (!$product->getVariations()->isEmpty()) {
            throw new \RuntimeException('This product has variations, not syncing');
        }

        $data = $this->teamLeaderProductTransformer->setProduct($product)->transform();

        /** @var TeamleaderProduct $teamleaderProduct */
        $teamleaderProduct = $this->entityManager->getRepository(TeamleaderProduct::class)->findOneBy([
            'product' => $product,
        ]);

        if ($teamleaderProduct && json_encode($data) === json_encode($teamleaderProduct->getData())) {
            return;
        }

        if (!$teamleaderProduct) {
            $result = $this->doRequest('/api/addProduct.php', $data);

            $teamleaderProduct = new TeamleaderProduct();
            $teamleaderProduct->setProduct($product);
            $teamleaderProduct->setId($result);

            $this->entityManager->persist($teamleaderProduct);
        } else {
            $data['product_id'] = $teamleaderProduct->getId();

            $this->doRequest('/api/updateProduct.php', $data);
        }

        $teamleaderProduct->setData($data);
        $teamleaderProduct->setSyncedAt(new \DateTime());

        $this->entityManager->flush();
    }

    /**
     * @param Customer $customer
     * @throws \Exception
     */
    public function syncCustomer(Customer $customer): void
    {
        if (!$customer->getCompany()) {
            throw new \RuntimeException("Customer doesn't belong to a company, not syncing");
        }

        /** @var TeamleaderCompany $teamleaderCompany */
        $teamleaderCompany = $this->entityManager->getRepository(TeamleaderCompany::class)->findOneBy([
            'company' => $customer->getCompany(),
        ]);

        if ($teamleaderCompany === null) {
            $this->syncCompany($customer->getCompany());
        }

        $data = $this->teamLeaderCustomerTransformer->setCustomer($customer)->transform();

        if ($teamleaderCompany && json_encode($data) === json_encode($teamleaderCompany->getData())) {
            return;
        }

        /** @var TeamleaderContact $teamleaderContact */
        $teamleaderContact = $this->entityManager->getRepository(TeamleaderContact::class)->findOneBy([
            'customer' => $customer,
        ]);

        if (!$teamleaderContact) {
            $result = $this->doRequest('/api/addContact.php', $data);

            $teamleaderContact = new TeamleaderContact();
            $teamleaderContact->setCustomer($customer);
            $teamleaderContact->setId($result);

            $this->entityManager->persist($teamleaderContact);

            $this->doRequest('/api/linkContactToCompany.php', [
                'contact_id' => $result,
                'company_id' => $teamleaderCompany->getId(),
                'mode' => 'link',
            ]);
        } else {
            $data['contact_id'] = $teamleaderContact->getId();
            $data['track_changes'] = true;

            $this->doRequest('/api/updateContact.php', $data);
        }

        $teamleaderContact->setData($data);
        $teamleaderContact->setSyncedAt(new \DateTime());

        $this->entityManager->flush();
    }

    /**
     * @param Company $company
     * @throws \Exception
     */
    public function syncCompany(Company $company): void
    {
        $data = $this->teamLeaderCompanyTransformer->setCompany($company)->transform();

        /** @var TeamleaderCompany $teamleaderCompany */
        $teamleaderCompany = $this->entityManager->getRepository(TeamleaderCompany::class)->findOneBy([
            'company' => $company,
        ]);

        if ($teamleaderCompany && json_encode($data) === json_encode($teamleaderCompany->getData())) {
            return;
        }

        if (!$teamleaderCompany) {
            $result = $this->doRequest('/api/addCompany.php', $data);

            $teamleaderCompany = new TeamleaderCompany();
            $teamleaderCompany->setCompany($company);
            $teamleaderCompany->setId($result);

            $this->entityManager->persist($teamleaderCompany);
        } else {
            $data['company_id'] = $teamleaderCompany->getId();
            $data['track_changes'] = true;

            $this->doRequest('/api/updateCompany.php', $data);
        }

        $teamleaderCompany->setData($data);
        $teamleaderCompany->setSyncedAt(new \DateTime());

        $this->entityManager->flush();
    }

    /**
     * @param       $uri
     * @param array $data
     * @return mixed
     */
    private function doRequest($uri, array $data = [])
    {
        if (null === $this->teamleaderApiGroup || null === $this->teamleaderApiKey) {
            throw new \RuntimeException('Teamleader api_group and api_secret must be set');
        }

        if (!is_numeric($this->teamleaderApiGroup)) {
            throw new \RuntimeException('Teamleader api_group must be numeric');
        }

        $data += [
            'api_group' => $this->teamleaderApiGroup,
            'api_secret' => $this->teamleaderApiKey,
        ];

        $response = $this->client->post($uri, [
            'form_params' => $data,
        ]);

        return json_decode($response->getBody());
    }
}
