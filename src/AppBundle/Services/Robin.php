<?php

namespace AppBundle\Services;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Services\Robin\CustomerTransformer;
use AppBundle\Services\Robin\OrderTransformer;
use AppBundle\ThirdParty\Robin\Entity\Customer as RobinCustomer;
use AppBundle\ThirdParty\Robin\Entity\Order as RobinOrder;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

/**
 * Class Robin
 * @package AppBundle\Services
 */
class Robin
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var Client */
    private $client;

    /** @var OrderTransformer */
    private $robinOrderTransformer;

    /** @var CustomerTransformer */
    private $robinCustomerTransformer;

    /**
     * Robin constructor.
     * @param EntityManagerInterface       $entityManager
     * @param Client              $guzzleClientRobin
     * @param OrderTransformer    $robinOrderTransformer
     * @param CustomerTransformer $robinCustomerTransformer
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        Client $guzzleClientRobin,
        OrderTransformer $robinOrderTransformer,
        CustomerTransformer $robinCustomerTransformer
    ) {
        $this->entityManager = $entityManager;
        $this->client = $guzzleClientRobin;
        $this->robinOrderTransformer = $robinOrderTransformer;
        $this->robinCustomerTransformer = $robinCustomerTransformer;
    }

    /**
     * @param OrderCollection $orderCollection
     * @throws \Exception
     */
    public function syncOrder(OrderCollection $orderCollection): void
    {
        foreach ($orderCollection->getOrders() as $order) {
            $data = $this->robinOrderTransformer->setOrder($order)->transform();

            /** @var RobinOrder $robinOrder */
            $robinOrder = $this->entityManager->getRepository(RobinOrder::class)->findOneBy([
                'order' => $order,
            ]);

            if ($robinOrder && $robinOrder->getSuccess() && json_encode($data) === json_encode($robinOrder->getData())) {
                return;
            }

            if (!$robinOrder) {
                $robinOrder = new RobinOrder();
                $robinOrder->setOrder($order);

                $this->entityManager->persist($robinOrder);
            }

            $robinOrder->setData($data);
            $robinOrder->setSyncedAt(new \DateTime());

            try {
                $this->doPostRequest('/orders', $data);

                $robinOrder->setSuccess(true);
            } catch (RequestException $requestException) {
                $robinOrder->setSuccess(false);

                throw $requestException;
            } finally {
                $this->entityManager->flush();
            }
        }
    }

    /**
     * @param Customer $customer
     * @throws \Exception
     */
    public function syncCustomer(Customer $customer): void
    {
        $data = $this->robinCustomerTransformer->setCustomer($customer)->transform($this->getCustomerData($customer->getEmail()));

        /** @var RobinCustomer $robinCustomer */
        $robinCustomer = $this->entityManager->getRepository(RobinCustomer::class)->findOneBy([
            'customer' => $customer,
        ]);

        if ($robinCustomer && json_encode($data) === json_encode($robinCustomer->getData())) {
            return;
        }

        if (!$robinCustomer) {
            $robinCustomer = new RobinCustomer();
            $robinCustomer->setCustomer($customer);

            $this->entityManager->persist($robinCustomer);
        }

        $robinCustomer->setData($data);
        $robinCustomer->setSyncedAt(new \DateTime());

        try {
            $this->doPostRequest('/customers', $data);

            $robinCustomer->setSuccess(true);
        } catch (RequestException $requestException) {
            $robinCustomer->setSuccess(false);

            throw $requestException;
        } finally {
            $this->entityManager->flush();
        }
    }

    /**
     * @param  string          $uri
     * @param  \stdClass|array $data
     * @return array|false
     */
    private function doPostRequest(string $uri, $data = [])
    {
        $response = $this->client->post($uri, [
            'json' => $data,
        ]);

        return json_decode($response->getBody());
    }

    /**
     * @param $email
     *
     * @return bool|mixed
     */
    private function getCustomerData($email)
    {
        try {
            $response = $this->client->get('/customer', [
                'query' => [
                    'id' => $email
                ],
            ]);
        } catch (ClientException $exception) {
            return false;
        }

        if($response->getStatusCode() === 200) {
            return json_decode($response->getBody()->getContents());
        }

        return false;
    }
}
