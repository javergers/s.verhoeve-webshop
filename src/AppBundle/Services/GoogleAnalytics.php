<?php

namespace AppBundle\Services;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use AppBundle\Entity\Order\Order;

/**
 * Class GoogleAnalytics
 * @package AppBundle\Services
 */
class GoogleAnalytics
{
    /**
     * @var array
     */
    private $events = [];

    /**
     * @var \stdClass
     */
    public $dataLayer;

    use ContainerAwareTrait;

    /**
     * GoogleAnalytics constructor.
     */
    public function __construct()
    {
        $this->dataLayer = new \stdClass();
    }

    /**
     * @param Assortment $assortment
     * @param Product    $product
     *
     * @return $this
     * @see https://developers.google.com/tag-manager/enhanced-ecommerce#product-impressions
     */
    public function productImpression(Assortment $assortment, Product $product)
    {
        if (!isset($this->dataLayer->ecommerce)) {
            $this->dataLayer->ecommerce = new \stdClass();
        }

        if (!isset($this->dataLayer->ecommerce->impressions)) {
            $this->dataLayer->ecommerce->impressions = [];
        }

        $impression = $this->impressionData($assortment, $product);

        array_push($this->dataLayer->ecommerce->impressions, $impression);

        $this->events['product_click_' . $product->getId()] = '
            $("a[data-product-id=' . $product->getId() . '], button[data-product-id=' . $product->getId() . ']").on("click", function() {
                GoogleAnalytics.productClick($(this), ' . json_encode($this->productData($product)) . ', \'' . $assortment->translate()->getTitle() . '\'); return false;
            });
        ';

        return $this;
    }

    /**
     * @param Product $product
     *
     * @return void
     * @throws \Exception
     * @see https://developers.google.com/tag-manager/enhanced-ecommerce#product-clicks
     */
    public function productClick(Product $product)
    {
        void($product);

        throw new \Exception("productClick must be done in JavaScript");
    }

    /**
     * @param \AppBundle\Entity\Catalog\Product\Product $product
     * @param                           $quantity
     */
    public function addToCartJs(ProductInterface $product, $quantity)
    {
        void($quantity);

        $this->events['add_to_cart_' . $product->getId()] = '
            $("button[data-product-id=' . $product->getId() . '], button[data-product-id=' . $product->getId() . ']").on("click", function() {
                GoogleAnalytics.addToCart($(this), ' . json_encode($this->productData($product)) . ');                
            });
        ';
    }

    /**
     * @param CartService                     $cart
     * @param                                 $step
     * @throws NonUniqueResultException
     * @throws OptimisticLockException
     */
    public function checkout(CartService $cart, $step)
    {
        $products = [];

        foreach ($cart->getOrders() as $order) {
            foreach ($order->getLines() as $line) {
                /**
                 * @var OrderLine $line ;
                 */
                if (null === $line->getProduct()) {
                    continue;
                }

                $product = $this->productData($line->getProduct());
                $product['price'] = $line->getPrice();
                $product['quantity'] = $line->getQuantity();

                array_push($products, $product);
            }
        }

        if (!$products) {
            return;
        }

        $data = new \stdClass();
        $data->event = 'checkout';
        $data->ecommerce = new \stdClass();
        $data->ecommerce->checkout = new \stdClass();
        $data->ecommerce->checkout->actionField = new \stdClass();
        $data->ecommerce->checkout->actionField->step = $step;
        $data->ecommerce->checkout->products = $products;

        $this->events['checkout'] = "
            dataLayer.push(" . json_encode($data, JSON_PRETTY_PRINT) . ");
        ";
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @see https://developers.google.com/tag-manager/enhanced-ecommerce#purchases
     * @throws \Exception
     */
    public function purchase(OrderCollection $orderCollection)
    {
        $products = [];

        $shippingCosts = 0;

        foreach ($orderCollection->getOrders() as $order) {
            foreach ($order->getLines() as $line) {
                /**
                 * @var OrderLine $line ;
                 */

                if ($line->getProduct() && $line->getProduct()->getProductgroup() && $line->getProduct()->getProductgroup()->getSkuPrefix() == "DLV") {
                    $shippingCosts += $line->getPrice();

                    continue;
                }

                $product = $this->productDataFromProduct($line->getProduct());
                $product['name'] = $line->getTitle();
                $product['price'] = round($line->getPrice(), 2);
                $product['quantity'] = $line->getQuantity();

                array_push($products, $product);
            }
        }

        $data = new \stdClass();
        $data->event = 'checkout';
        $data->ecommerce = new \stdClass();
        $data->ecommerce->purchase = new \stdClass();
        $data->ecommerce->purchase->actionField = new \stdClass();
        $data->ecommerce->purchase->actionField->id = $orderCollection->getNumber();
        //$data->ecommerce->purchase->actionField->affiliation = "";
        $data->ecommerce->purchase->actionField->revenue = round($orderCollection->getTotalPrice() - $shippingCosts, 2);
        $data->ecommerce->purchase->actionField->tax = round($orderCollection->getTotalPriceIncl() - $orderCollection->getTotalPrice(),
            2);
        $data->ecommerce->purchase->actionField->shipping = round($shippingCosts, 2);
        $data->ecommerce->purchase->actionField->city = $orderCollection->getInvoiceAddressCity();
        $data->ecommerce->purchase->actionField->country = $orderCollection->getInvoiceAddressCountry()->getCode();
//        $data->ecommerce->purchase->actionField->coupon = "";
        $data->ecommerce->purchase->products = $products;

        $this->events['purchase'] = "
            dataLayer.push(" . json_encode($data, JSON_PRETTY_PRINT) . ");
        ";
    }

    /**
     *
     * @see https://developers.google.com/analytics/devguides/collection/analyticsjs/enhanced-ecommerce#impression-data
     *
     * @param Assortment $assortment
     * @param Product    $product
     *
     * @return array
     */
    public function impressionData(Assortment $assortment, Product $product)
    {
        $data = [
            'id' => $product->getSku(),
            'name' => $product->getDisplayName(),
            'list' => $assortment->translate()->getTitle(),
            'brand' => (string)$product->getManufacturer(),
            'category' => implode("/", $this->getCategories($product)),
            'position' => ($assortment->getProducts()->indexOf($product) + 1),
            'price' => (float)$product->getPrice(),
        ];

        return $data;
    }

    /**
     * @param Product $product
     *
     * @return array
     * @see https://developers.google.com/analytics/devguides/collection/analyticsjs/enhanced-ecommerce#product-data
     */
    public function productData(ProductInterface $product)
    {
        $data = [
            'id' => $product->getSku(),
            'name' => $product->getDisplayName(),
            'brand' => (string)$product->getManufacturer(),
            'category' => implode("/", $this->getCategories($product)),
            'price' => (float)$product->getPrice(),
        ];

        return $data;
    }

    /**
     * @param Product $product
     *
     * @return array
     * @see https://developers.google.com/analytics/devguides/collection/analyticsjs/enhanced-ecommerce#product-data
     */
    public function productDataFromProduct(Product $product)
    {
        $data = [
            'id' => $product->getSku(),
            'name' => null,
            'brand' => (string)$product->getManufacturer(),
            'category' => implode("/", $this->getCategories($product)),
            'price' => (float)$product->getPrice(),
        ];

        return $data;
    }

    /**
     * @param $product
     *
     * @return array
     */
    private function getCategories($product)
    {
        if (!$product->getProductgroup()) {
            return [];
        }

        $categories = [];

        $productGroup = $product->getProductgroup();

        array_unshift($categories, $productGroup->getTitle());

        while ($productGroup->getParent()) {
            $productGroup = $productGroup->getParent();

            array_unshift($categories, $productGroup->getTitle());
        }

        return $categories;
    }

    /**
     * @return array
     */
    public function getEvents()
    {
        return $this->events;
    }
}
