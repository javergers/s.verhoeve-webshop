<?php

namespace AppBundle\Services\Report;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Report\CompanyReport;
use AppBundle\Entity\Report\Report;
use AppBundle\Entity\Report\ReportChartData;
use AppBundle\Entity\Report\ReportColumn;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\UnitOfWork;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use RuleBundle\Entity\Entity;
use RuleBundle\Entity\Rule;
use RuleBundle\Service\ResolverService;
use RuleBundle\Service\RuleTransformerService;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class ReportService
 * @package AppBundle\Services\Report
 */
class ReportService
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var ResolverService
     */
    protected $ruleResolver;

    /**
     * @var RuleTransformerService
     */
    protected $ruleTransformer;

    /** @var Report $report */
    protected $report;

    /** @var ArrayCollection $data */
    protected $data = [];

    /** @var array $requirements */
    protected $requirements = [];

    /** @var array $requirementLinks */
    private $requirementLinks = [
        'fromDate' => '.createdAt',
        'tillDate' => '.createdAt',
        'company' => '.company.id',
        'customer' => '.customer.id',
    ];

    /**
     * ReportService constructor.
     * @param EntityManagerInterface $entityManager
     * @param ResolverService        $resolverService
     * @param RuleTransformerService $ruleTransformerService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ResolverService $resolverService,
        RuleTransformerService $ruleTransformerService
    ) {
        $this->entityManager = $entityManager;
        $this->ruleResolver = $resolverService;
        $this->ruleTransformer = $ruleTransformerService;
    }

    /**
     * @return BinaryFileResponse
     * @throws Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \Exception
     */
    private function buildExcel()
    {
        $spreadSheet = new Spreadsheet();

        $sheet = $spreadSheet->getActiveSheet();

        if (!$this->data) {
            $this->retrieveData($this->report, $this->requirements);
        }

        $cell = 'A';
        /** @var ReportColumn $column */
        foreach ($this->report->getColumns() as $column) {
            $sheet->setCellValue($cell . '1', $column->getName());
            $sheet->getStyle($cell . '1')->getFont()->setBold(true);
            $cell++;
        }

        $propertyAccessor = new PropertyAccessor();

        $cell = 'A';
        $index = 2;
        foreach ($this->data as $entityData) {
            /** @var ReportColumn $reportColumn */
            foreach ($this->report->getColumns() as $reportColumn) {
                $sheet->setCellValue($cell . $index,
                    $propertyAccessor->getValue($entityData, $reportColumn->getPath()));

                $cell++;
            }

            $cell = 'A';
            $index++;
        }

        $path = tempnam(sys_get_temp_dir(), 'export');

        $writer = new Xlsx($spreadSheet);
        $writer->save($path);

        $name = 'Rapportage';

        $response = new BinaryFileResponse($path, 200, [
            'Content-type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition' => 'attachment; filename="' . $name . '.xlsx"',
        ]);

        $response->deleteFileAfterSend(true);

        return $response;
    }

    /**
     * @param Report $report
     * @param array  $requirements
     * @return ArrayCollection|\Traversable
     * @throws \Exception
     */
    public function retrieveData(Report $report, array $requirements = [])
    {
        $this->report = $report;
        $this->requirements = $requirements;

        $rule = $this->report->getRule();
        /** @var QueryBuilder $qb */
        $qb = $this->entityManager->getRepository($rule->getStart())->createQueryBuilder(ResolverService::QB_ALIAS);

        $ruleText = $this->ruleResolver->expressionToString($rule->getExpression(), []);
        $ruleText = $this->addRequirementsToRuleText($ruleText);

        $qb = $this->ruleResolver->applyFilter($qb, $ruleText);

        $this->data =  $qb->getQuery()->getResult();
    }

    /**
     * This is a temporary function, will be replaced with the new Wrapper service in the RuleBundle
     *
     * @param string $ruleText
     * @return string
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    protected function addRequirementsToRuleText(string $ruleText): string
    {
        $rule = $this->report->getRule();

        $inputs = [
            ResolverService::QB_ALIAS => $this->entityManager->getRepository(Entity::class)->findOneBy([
                'fullyQualifiedName' => urldecode($rule->getStart()),
            ]),
        ];

        $filters = $this->ruleTransformer->getFilters($inputs);

        foreach ($this->requirements as $key => $requirement) {
            $filterableString = $this->requirementLinks[$key];

            $fieldId = null;
            foreach ($filters as $filter) {
                if (substr($filter['id'], -strlen($filterableString)) === $filterableString) {
                    $fieldId = $filter['id'];
                    if ($fieldId) {
                        switch ($key) {
                            case 'fromDate':
                                $operator = '>';
                                $requirement = $requirement->format('Y-m-d');
                                break;
                            case 'tillDate':
                                $operator = '<';
                                $requirement = $requirement->format('Y-m-d');
                                break;
                            default:
                                $operator = '=';
                                $requirement = $requirement->getId();
                        }

                        $ruleText .= sprintf(' and %s %s "%s"', $fieldId, $operator, $requirement);
                    }

                    break;
                }
            }

            if(null === $fieldId) {
                throw new \RuntimeException(sprintf("Can't find relation for %s for entity %s", $key, $rule->getStart()));
            }
        }

        return $ruleText;
    }

    /**
     * @param Report $report
     * @param array  $requirements
     *
     * @return BinaryFileResponse
     * @throws Exception
     * @throws MappingException
     * @throws OptimisticLockException
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \ReflectionException
     */
    public function generate(Report $report, array $requirements = [])
    {
        //TODO add role check, this is an topgeschenken employee function only!
        $this->report = $report;
        $this->requirements = $requirements;

        return $this->buildExcel();
    }

    /**
     * @return array
     */
    public function getAvailableReports()
    {
        /** @var QueryBuilder $qb */
        $qb = $this->entityManager->getRepository(Report::class)->createQueryBuilder('r')
            ->leftJoin(CompanyReport::class, 'cr', Join::WITH, 'cr.report = r.id')
            ->where('cr.report IS NULL');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Company $company
     * @param Report  $report
     */
    public function cloneForCompany(Company $company, Report $report)
    {
        $companyReport = new CompanyReport();
        $companyReport->setCompany($company);

        $clonedReport = clone $report;
        $companyReport->setReport($clonedReport);

        $this->entityManager->persist($companyReport);
        $this->entityManager->flush();
    }
}