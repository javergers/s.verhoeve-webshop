<?php

namespace AppBundle\Services\Robin;

use AppBundle\Entity\Security\Customer\Customer;
use libphonenumber\PhoneNumberFormat;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;

/**
 * Class CustomerTransformer
 * @package AppBundle\Services\Robin
 */
class CustomerTransformer
{
    /** @var Router */
    private $router;

    public const EMPTY_KEY = '<div style="position: absolute; width: 15px; height: 15px; background-color: white; visibility: visible !important;"></div>';

    /** @var Customer */
    private $customer;

    /**
     * CustomerTransformer constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @param Customer $customer
     * @return $this
     */
    public function setCustomer(Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return \stdClass
     */
    public function transform($remoteCustomerData): \stdClass
    {
        $panel_view = new \stdClass();

        switch ($this->customer->getGender()) {
            case 'male':
                $panel_view->Geslacht = 'Man';
                break;
            case 'female';
                $panel_view->Geslacht = 'Vrouw';
                break;
            default;
                $panel_view->Geslacht = '<i>Niet bekend</i>';
        }

        $languageCode = explode('_', $this->customer->getLocale());

        if ($languageCode[0] === 'en') {
            unset($languageCode[1]);
        }

        $panel_view->Voorkeurstaal = Intl::getLanguageBundle()->getLanguageName($languageCode[0],
            !empty($languageCode[1]) ? $languageCode[1] : null, 'nl_NL');
        $panel_view->Klantgroep = $this->customer->getCustomerGroup() ? $this->customer->getCustomerGroup()->getDescription() : '';

        if ($this->customer->getCompany()) {
            $panel_view->Bedrijf = $this->customer->getCompany()->getName();
        }

        $address = $this->customer->getMainAddress();

        if ($address) {
            $panel_view->Adres = (string)$address;
        }

        $url = $this->router->generate('admin_customer_customer_view', [
            'id' => $this->customer->getId(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);

        $panel_view->{self::EMPTY_KEY} = '
            <div style="text-align: center; margin-left: -110px; margin-top: 15px;">
                <a href="' . $url . '" target="_blank"><button class="send_button gray_button">Open klant in beheer</button></a>
            </div>
        ';

        $orders = $this->customer->getOrders();
        $spent = 0;

        foreach ($orders as $order) {
            $spent += $order->getTotalPriceIncl();
        }

        $decodedCustomerData = null;
        if ($remoteCustomerData !== false){
            $decodedCustomerData = json_decode($remoteCustomerData->CustomerData);
        }

        $this->customer = [
            'name' => $this->customer->getFullname(),
            'email_address' => $this->customer->getEmail(),
            'customer_since' => $this->customer->getCreatedAt()->format('Y-m-d'),
            'order_count' => \count($orders),
            'total_spent' => '€ ' . number_format($spent, 2, ',', '.'),
            'phone_number' => $this->customer->getFormattedPhoneNumber(PhoneNumberFormat::E164),
            'panel_view' => $panel_view,
        ];

        // Add missing TB panel keys and values.
        if(isset($decodedCustomerData->panel_view->topbloemen)){
            $this->customer['panel_view']->topbloemen = $decodedCustomerData->panel_view->topbloemen;
        }

        $data = new \stdClass();
        $data->customers = [$this->customer];

        return $data;
    }
}
