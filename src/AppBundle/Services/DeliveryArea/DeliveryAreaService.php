<?php

namespace AppBundle\Services\DeliveryArea;

use AppBundle\Connector\Bakker;
use AppBundle\Connector\BakkerMail;
use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Entity\Finance\Tax\VatRate;
use AppBundle\Entity\Geography\Postcode;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyPreferredSupplier;
use AppBundle\Entity\Supplier\DeliveryArea;
use AppBundle\Entity\Supplier\SupplierGroup;
use AppBundle\Interfaces\DeliveryArea\DatasetInterface;
use AppBundle\Services\PriceService;
use AppBundle\Utils\DeliveryArea\Datasets\AbstractDataset;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use ReflectionClass;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class DeliveryAreaService
 * @package AppBundle\Services\DeliveryArea
 */
class DeliveryAreaService
{
    use ContainerAwareTrait;
    /**
     * @var DatasetInterface $dataset
     */
    private $dataset;
    /**
     * @var array
     */
    private $datasets;
    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    /** @var Request */
    private $request;

    /**
     * @var PriceService
     */
    private $priceService;

    /**
     * DeliveryAreaService constructor.
     * @param EntityManagerInterface $em
     * @param RequestStack  $requestStack
     */
    public function __construct(EntityManagerInterface $em, RequestStack $requestStack, PriceService $priceService)
    {
        $this->em = $em;
        $this->request = $requestStack->getCurrentRequest();
        $this->priceService = $priceService;
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param DatasetInterface $dataset
     *
     * @return $this
     */
    public function setDataset(DatasetInterface $dataset)
    {
        $this->dataset = $dataset;
        return $this;
    }

    /**
     * @return DatasetInterface
     */
    public function getDataset()
    {
        return $this->dataset;
    }

    /**
     * @param $postcode
     *
     * @return DeliveryArea[]
     */
    public function getDeliveryAreas($postcode)
    {
        return $this->em->getRepository(DeliveryArea::class)->findBy([
            'postcode' => $postcode,
        ]);
    }

    /**
     * Get a company SupplierGroups of a DeliveryArea
     *
     * @param Company $supplier
     * @return SupplierGroup|null
     */
    public function getRequestedSupplierSupplierGroup(Company $supplier)
    {
        if (null !== $supplier && $supplierGroups = $supplier->getSupplierGroups()) {
            foreach ($supplierGroups as $supplierGroup) {
                if ($supplierGroup->getName() === $this->request->get('supplierGroup')) {
                    return $supplierGroup;
                }
            }
        }

        return null;
    }

    /**
     * @param array $bounds
     *
     * @return array
     * @throws DBALException
     */
    public function getPostcodesWithinBounds(array $bounds = [])
    {
        $postcodes = [];

        $sql = "
            SELECT `postcode`.`postcode`
            FROM `postcode_geometry`
            LEFT JOIN `postcode` ON `postcode_geometry`.`id` = `postcode`.`geometry_id`            
            WHERE MBRIntersects(GeomFromText('Polygon((
              :NorthEastLng :NorthEastLat,
              :SouthWestLng :NorthEastLat,
              :SouthWestLng :SouthWestLat,
              :NorthEastLng :SouthWestLat,
              :NorthEastLng :NorthEastLat    
            ))'), multipolygon);    
        ";

        $sql = str_replace([':NorthEastLat', ':NorthEastLng', ':SouthWestLat', ':SouthWestLng'], [
            (float)$bounds['NorthEast']['lat'],
            (float)$bounds['NorthEast']['lng'],
            (float)$bounds['SouthWest']['lat'],
            (float)$bounds['SouthWest']['lng'],
        ], $sql);

        /** @var \PDOStatement $stmt */
        $stmt = $this->em->getConnection()->prepare($sql);

        $stmt->execute();

        foreach ($stmt->fetchAll(\PDO::FETCH_COLUMN) as $postcode) {
            $postcodes[] = [
                'country' => 'NL',
                'postcode' => $postcode,
            ];
        }

        return $postcodes;
    }

    /**
     * @param Company $supplier
     *
     * @return DeliveryArea[]
     */
    public function getSupplierDeliveryAreas(Company $supplier)
    {
        return $this->em->getRepository(DeliveryArea::class)->findBy([
            'company' => $supplier,
        ]);
    }

    /**
     * Get current of dataset
     *
     * @param $name
     *
     * @return DatasetInterface
     * @throws \RuntimeException
     * @throws \ReflectionException
     */
    public function getDatasetClass(string $name)
    {
        if (isset($this->getDatasetClasses()[$name])) {
            return $this->getDatasetClasses()[$name];
        }

        throw new \RuntimeException(sprintf("Dataset '%s' not found", $name));
    }

    /**
     * Get list of dataset classes
     * @return array
     * @throws \ReflectionException
     */
    public function getDatasetClasses()
    {
        if (!$this->datasets) {
            $fs = new Filesystem();
            $datasetsPath = './../src/AppBundle/Utils/DeliveryArea/Datasets';

            $files = null;
            if ($fs->exists($datasetsPath)) {

                $finder = new Finder();
                $files = $finder->files()->in($datasetsPath);
            }

            if ($files) {

                foreach ($files as $file) {

                    $className = str_replace('/', "\\", substr($file, 9, -4));

                    $reflectionClass = new ReflectionClass($className);
                    if ($reflectionClass->isAbstract()) {
                        continue;
                    }

                    /** @var DatasetInterface $class */
                    $class = new $className($this, $this->em);
                    $this->datasets[$class->getId()] = $class;
                }
            }
        }

        uasort($this->datasets, function (AbstractDataset $a, AbstractDataset $b) {
            return $a->getOrder() <=> $b->getOrder();
        });

        return $this->datasets;
    }

    /**
     * @return array
     */
    public function getLegend()
    {

        $curDataset = $this->getDataset();
        return $curDataset->getLegend();
    }

    /**
     * Get delivery interval list for choose types.
     * @return array
     */
    public function getDeliveryIntervalList()
    {
        $list = [];
        if ($this->getDeliveryIntervals()) {
            foreach ($this->getDeliveryIntervals() as $interval) {
                $list[$interval['description']] = $interval['interval'];
            }
        }
        return $list;
    }

    /**
     * @param $interval
     *
     * @return array|null
     */
    public function getDeliveryInterval($interval)
    {
        foreach ($this->getDeliveryIntervals() as $item) {
            if ($item['interval'] === $interval) {
                return $item;
            }
        }
        return null;
    }

    /**
     * @return array
     */
    public function getDeliveryIntervals()
    {
        return [
            [
                'interval' => 'PT15H',
                'description' => '09:00',
            ],
            [
                'interval' => 'PT14H',
                'description' => '10:00',
            ],
            [
                'interval' => 'PT13H',
                'description' => '11:00',
            ],
            [
                'interval' => 'PT12H',
                'description' => '12:00',
            ],
            [
                'interval' => 'PT11H',
                'description' => '13:00',
            ],
            [
                'interval' => 'PT10H',
                'description' => '14:00',
            ],
            [
                'interval' => 'PT9H',
                'description' => '15:00',
            ],
            [
                'interval' => 'PT8H',
                'description' => '16:00',
            ],
            [
                'interval' => 'PT7H',
                'description' => '17:00',
            ],
            [
                'interval' => 'P1DT7H',
                'description' => '17:00 (V.Wd)',
            ],
        ];
    }

    /**
     * @param Company $company
     *
     * @return \PHPExcel
     * @throws \PHPExcel_Exception
     */
    public function getSupplierExcelExport(Company $company)
    {
        $deliveryAreas = $this->getSupplierDeliveryAreas($company);
        $deliveryIntervals = $this->getDeliveryIntervals();
        $deliveryAreaExportService = $this->container->get('delivery_area_export');

        return $deliveryAreaExportService->generateSupplierExcelExport($company, $deliveryAreas, $deliveryIntervals);
    }

    /**
     * @param $country  string
     * @param $postcode integer
     *
     * @return Postcode
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getPostcode(string $country, int $postcode)
    {
        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->em->getRepository(Postcode::class);

        $qb = $entityRepository->createQueryBuilder('postcode');
        $qb->leftJoin('postcode.province', 'province');
        $qb->leftJoin('province.country', 'country');
        $qb->andWhere('postcode.postcode = :postcode');
        $qb->setParameter('postcode', $postcode);
        $qb->andWhere('country.id = :country');
        $qb->setParameter('country', $country);

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @param string $supplierGroup
     * @return Company[]|null
     */
    public function getSuppliers(string $supplierGroup = null)
    {
        $qb = $this->em
            ->getRepository(Company::class)
            ->createQueryBuilder('supplier')
            ->andWhere('supplier.isSupplier = 1')
            ->andWhere('supplier.supplierConnector IS NOT NULL');

        if ($supplierGroup) {
            $qb->leftJoin('supplier.supplierGroups', 'supplierGroup');
            $qb->andWhere('supplierGroup.name = :supplierGroup');
            $qb->setParameter('supplierGroup', $supplierGroup);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param DeliveryArea $deliveryArea
     * @param Company|null $company
     * @return CompanyPreferredSupplier
     */
    public function getPreferredSupplier(DeliveryArea $deliveryArea, Company $company = null)
    {
        return $deliveryArea->getPreferredSuppliers()->filter(function (
            CompanyPreferredSupplier $preferredSupplier
        ) use ($company) {
            return $preferredSupplier->getCompany() === $company;
        })->current();
    }

    /**
     * Get colors of connectors
     * @throws \ReflectionException
     */
    public function getConnectorColors()
    {
        $rfBakker = new ReflectionClass(Bakker::class);
        $rfBakkerMail = new ReflectionClass(BakkerMail::class);

        return [
            'bakker' => $rfBakker->getDefaultProperties()['color'],
            'bakkerMail' => $rfBakkerMail->getDefaultProperties()['color'],
        ];
    }

    /**
     * @param SupplierGroup     $supplierGroup
     * @param Postcode          $postcode
     * @param DeliveryArea|null $deliveryArea
     * @param bool              $formatted
     * @return array
     * @throws \Exception
     */
    public function calcRegionPrices(SupplierGroup $supplierGroup, Postcode $postcode, DeliveryArea $deliveryArea = null, $formatted = true)
    {
        $price = '0.000';
        $priceIncl = '0.00';
        $vat = 0;
        $interval = '';

        if ($supplierGroup !== null && $postcode !== null) {
            /** @var VatGroup $deliveryVatGroup */
            $deliveryVatGroup = $supplierGroup->getDeliveryVatGroupForCountry($postcode->getCountry());

            /** @var VatRate $deliveryVatRate */
            $deliveryVatRate = null !== $deliveryVatGroup ? $deliveryVatGroup->getRateByDate(null, true) : null;

            if($deliveryArea !== null) {
                $price = $deliveryArea->getDeliveryPrice();
                $priceIncl = $this->priceService->calcPrice($deliveryArea->getDeliveryPrice(), $deliveryVatRate);
            } else {
                $price = $supplierGroup->getDeliveryCost();
                $priceIncl = $this->priceService->calcPrice($supplierGroup->getDeliveryCost(), $deliveryVatRate);
            }

            if($formatted) {
                $price = number_format($price, 2, ',', '');
                $priceIncl = number_format($priceIncl, 2, ',', '');
            }

            if ($deliveryVatRate) {
                $vat = $deliveryVatRate->getFactor() * 100;
            }

            $interval = $supplierGroup->getDeliveryInterval();
        }

        return [
            $price,
            $priceIncl,
            $vat,
            $interval,
        ];
    }
}
