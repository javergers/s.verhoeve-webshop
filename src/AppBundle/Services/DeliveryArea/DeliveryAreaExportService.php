<?php

namespace AppBundle\Services\DeliveryArea;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Supplier\DeliveryArea;
use PHPExcel;
use PHPExcel_Cell_DataType;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Fill;

/**
 * Class DeliveryAreaExportService
 * @package AppBundle\Services\DeliveryArea
 */
class DeliveryAreaExportService
{
    /** @var string */
    private $author = 'Topgeschenken Nederland';

    /**
     * @param Company        $company
     * @param DeliveryArea[] $deliveryAreas
     * @param array          $deliveryIntervals
     *
     * @return PHPExcel
     * @throws \PHPExcel_Exception
     */
    public function generateSupplierExcelExport(Company $company, $deliveryAreas, array $deliveryIntervals)
    {
        $row = 2;
        $excel = new PHPExcel();
        $title = $company->getName() . ' bezorggebieden';

        $excel->getProperties()->setCreator($this->author)
            ->setLastModifiedBy($this->author)
            ->setTitle($title)
            ->setSubject($title);

        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(3);
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(5);
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, 'Bezorgkosten');
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(11);
        $excel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, 'Aannametijd');
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);

        $excel->getActiveSheet()->getStyle('A1:E' . 1)->applyFromArray([
            'font' => [
                'bold' => true,
                'size' => 8,
            ],
            'alignment' => [
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
        ]);

        $excel->getActiveSheet()->getStyle('A1:E' . 1)->getFill()->applyFromArray([
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'startcolor' => [
                'rgb' => 'FCFCFC',
            ],
        ]);

        $excel->getActiveSheet()->getRowDimension(1)->setRowHeight(18);

        foreach ($deliveryAreas as $deliveryArea) {
            $excel->getActiveSheet()->getCellByColumnAndRow(1,
                $row)->setValueExplicit($deliveryArea->getPostcode()->getPostcode(),
                PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $excel->getActiveSheet()->getCellByColumnAndRow(2,
                $row)->setValueExplicit($deliveryArea->getPostcode()->getCities()[0]->translate()->getName());
            $excel->getActiveSheet()->getCellByColumnAndRow(3,
                $row)->setValueExplicit(number_format($deliveryArea->getDeliveryPrice() * 1.06, 2, '.', ','),
                PHPExcel_Cell_DataType::TYPE_NUMERIC);

            $description = $deliveryArea->getDeliveryInterval();

            foreach ($deliveryIntervals as $deliveryInterval) {
                if ($deliveryInterval['interval'] === $deliveryArea->getDeliveryInterval()) {
                    $description = $deliveryInterval['description'];

                    break;
                }
            }

            $excel->getActiveSheet()->getCellByColumnAndRow(4, $row)->setValue($description);

            $excel->getActiveSheet()->getStyle('A' . $row . ':E' . $row)->getFill()->applyFromArray([
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'startcolor' => [
                    'rgb' => 'B4CC8D',
                ],
            ]);

            $row++;
        }

        $excel->getActiveSheet()->getStyle('A2:C' . $row)->applyFromArray([
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
            ],
        ]);

        $excel->getActiveSheet()->getStyle('D1:D' . $row)->applyFromArray([
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ],
        ]);

        $excel->getActiveSheet()->getStyle('D2:D' . $row)->getNumberFormat()->setFormatCode('0.00');

        $excel->getActiveSheet()->getStyle('A2:E' . $row)->applyFromArray([
            'font' => [
                'size' => 9,
            ],
        ]);

        return $excel;
    }
}
