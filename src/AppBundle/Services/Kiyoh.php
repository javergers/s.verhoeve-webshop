<?php

namespace AppBundle\Services;

use AppBundle\Entity\Site\Domain as DomainEntity;
use AppBundle\ThirdParty\Kiyoh\Entity\KiyohCompany;
use AppBundle\ThirdParty\Kiyoh\Entity\KiyohReview;
use AppBundle\Services\Domain as DomainService;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\Paginator;
use Redis;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Validation;

/**
 * Class Kiyoh
 *
 * @package AppBundle\Services
 */
class Kiyoh
{
    private $options = [];
    private $entityManager;
    private $redis;
    private $domainService;
    private $paginator;

    /**
     * Kiyoh constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param Redis         $redis
     * @param DomainService $domainService
     * @param Paginator     $paginator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        Redis $redis,
        DomainService $domainService,
        Paginator $paginator
    ) {
        $this->entityManager = $entityManager;
        $this->redis = $redis;
        $this->domainService = $domainService;
        $this->paginator = $paginator;
    }

    /**
     * @param array $options
     * @return KiyohCompany
     */
    public function getCompany($options = [])
    {
        $this->resolveDomain($options);

        $kiyohCompany = $this->entityManager->getRepository(KiyohCompany::class)->findOneBy([
            'domain' => $this->getOptions()['domain'],
        ]);

        return $kiyohCompany;
    }

    /**
     * @param array $options
     *
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getReviews($options = [])
    {
        $this->resolveDomain($options);
        $this->resolveReviewOptions($options);

        $key = 'kiyoh:reviews:' . $this->getOptions()['domain']->getDomain();

        $kiyohCompany = $this->entityManager->getRepository(KiyohCompany::class)->findOneBy([
            'domain' => $this->getOptions()['domain'],
        ]);

        $reviews = $this->entityManager->getRepository(KiyohReview::class)
            ->findRecentForCompany($kiyohCompany);

        // Redis unserialize doesn't work since the Object to be unserialized contains closures.
        // Commented out since this feature is not used at the moment.

        /*if (!($reviews = unserialize($this->redis->get($key), []))) {
            $kiyohCompany = $this->entityManager->getRepository(KiyohCompany::class)->findOneBy([
                'domain' => $this->getOptions()['domain'],
            ]);

            $reviews = $this->entityManager->getRepository(KiyohReview::class)->findRecentForCompany($kiyohCompany);

            $this->redis->set($key, serialize($reviews), 60 * 5);
        }*/

        $paginator = $this->paginator;
        $pagination = $paginator->paginate(
            $reviews,
            $this->getOptions()['page'],
            $this->getOptions()['limit']
        );

        return $pagination;
    }

    /**
     * @param array $options
     */
    private function resolveDomain($options)
    {
        if (empty($options['domain'])) {
            $options['domain'] = $this->domainService->getDomain();
        }

        $options = array_intersect_key($options, ['domain' => true]);

        $resolver = new OptionsResolver();

        $resolver->setDefined([
            'domain',
        ]);

        $resolver->setAllowedTypes('domain', ['object']);

        $resolver->setAllowedValues('domain', function ($value) {
            return ($value instanceof DomainEntity);
        });

        $this->setOptions(array_merge($this->getOptions(), $resolver->resolve($options)));
    }

    /**
     * @param array $options
     */
    private function resolveReviewOptions($options)
    {
        $resolver = new OptionsResolver();

        $resolver->setDefined([
            'limit',
            'page',
        ]);

        $resolver->setDefaults(
            [
                "limit" => 10,
                "page" => 1,
            ]
        );

        $resolver->setAllowedTypes('limit', ['integer']);

        $resolver->setAllowedValues('limit', function ($value) {
            $validator = Validation::createValidator();
            $constraint = new Range([
                'min' => 1,
                'max' => 50,
            ]);

            $violations = $validator->validate($value, $constraint);

            return 0 === $violations->count();
        });

        $this->setOptions(array_merge($this->getOptions(), $resolver->resolve($options)));
    }

    /**
     * @param $options
     */
    private function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @return mixed
     */
    public function getOptions()
    {
        return $this->options;
    }
}
