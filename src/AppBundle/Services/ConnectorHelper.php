<?php

namespace AppBundle\Services;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Interfaces\ConnectorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;

/**
 * Class ConnectorHelper
 * @package AppBundle\Services
 */
class ConnectorHelper
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * ConnectorHelper constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param Company|SupplierOrder $object
     * @return bool
     */
    public function hasConnector($object): bool
    {
        if ($object instanceof SupplierOrder) {
            $connector = $object->getConnector();
            $supplier = $object->getSupplier();
        } elseif ($object instanceof Company) {
            $connector = $object->getSupplierConnector();
            $supplier = $object;
        } else {
            return false;
        }

        return $supplier && $connector;
    }

    /**
     * @param Company|SupplierOrder $object
     * @return null|ConnectorInterface
     */
    public function getConnector($object): ?ConnectorInterface
    {
        if ($object instanceof SupplierOrder) {
            $connector = $object->getConnector();
            $supplier = $object->getSupplier();
        } elseif ($object instanceof Company) {
            $connector = $object->getSupplierConnector();
            $supplier = $object;
        } else {
            return null;
        }

        $className = 'AppBundle\\Connector\\' . $connector;

        if (!$supplier || !$connector) {
            return null;
        }

        return $this->findConnector($className, $supplier);
    }

    /**
     * @param $className
     * @param $object
     * @return null|ConnectorInterface
     */
    public function findConnector($className, $object){
        if(stripos($className, 'AppBundle\\Connector\\') === false) {
            $className = 'AppBundle\\Connector\\' . $className;
        }
        $connectorInstance = $this->container->get($className);
        if($connectorInstance instanceof ConnectorInterface){
            $connectorInstance->setSupplier($object);
            return $connectorInstance;
        }
        return null;
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function getConnectors(): array
    {
        $finder = (new Finder())
            ->files()
            ->depth(0)
            ->in($this->container->getParameter('kernel.project_dir') . '/src/AppBundle/Connector');

        $connectors = [];

        foreach ($finder as $file) {
            $class = $this->getConnectorClassFromPath($file);
            $reflectionClass = new \ReflectionClass($class);

            if ($reflectionClass->isAbstract()) {
                continue;
            }

            $connectors[] = $class;
        }

        sort($connectors);

        return $connectors;
    }

    /**
     * @param string $path
     * @return mixed
     */
    private function getConnectorClassFromPath(string $path)
    {
        return str_replace('/', '\\',
            substr(str_replace($this->container->getParameter('kernel.project_dir') . '/src/', null, $path), 0, -4));
    }
}
