<?php

namespace AppBundle\Services\Auto;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Geography\Holiday;
use AppBundle\Entity\Order\Order;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class ForwardAtCalculatorService
 * @package AppBundle\Services\Auto
 */
class ForwardAtCalculatorService
{
    use ContainerAwareTrait;

    /**
     * @param Order   $order
     * @param Company $supplier
     * @return \DateTime|null
     * @throws \Exception
     */
    public function calculateForwardDateTime(Order $order, Company $supplier)
    {
        $holidays = $this->getRelevantHolidays($order);

        $earliestForwardDate = $this->calculateEarliestDateTime($order, $holidays);

        if (null === $order->getDeliveryDate()) {
            return $earliestForwardDate;
        }

        $latestForwardDateTime = $this->calculateLatestDateTime($order, $holidays, $supplier);
        $proposedForwardDateTime = $this->calculateProposedDateTime($order, $holidays);

        if ($latestForwardDateTime < new \DateTime()) {
            return null;
        }

        if ($earliestForwardDate > $latestForwardDateTime) {
            return null;
        }

        if ($proposedForwardDateTime < $earliestForwardDate) {
            $forwardDateTime = $earliestForwardDate;
        } elseif ($proposedForwardDateTime > $latestForwardDateTime) {
            $forwardDateTime = $latestForwardDateTime;
        } else {
            $forwardDateTime = $proposedForwardDateTime;
        }

        return $forwardDateTime;
    }

    /**
     * @param Order                     $order
     * @param ArrayCollection|Holiday[] $holidays
     * @return \Datetime
     * @throws \Exception
     */
    private function calculateProposedDateTime(Order $order, ArrayCollection $holidays)
    {
        $proposedForwardDateTime = new \Datetime('@' . round(array_sum([
                    $order->getDeliveryDate()->format('U'),
                    $order->getCreatedAt()->format('U'),
                ]) / 2));

        // Indien er een zondag tussen bestelling en levering zit één dag eerder doorsturen
        if ($order->getDeliveryDate()->format('w') < $order->getCreatedAt()->format('w')) {
            $proposedForwardDateTime->sub(new \DateInterval('P1D'));
        }

        $proposedForwardDateTime->setTimezone(new \DateTimeZone('Europe/Amsterdam'));

        while (!$this->isAcceptableDate($holidays, $proposedForwardDateTime)) {
            $proposedForwardDateTime->sub(new \DateInterval('P1D'));
        }

        if ($proposedForwardDateTime->format('H:i:s') < '5:00:00') {
            $proposedForwardDateTime->setTime(5, 0, 0);
        }

        if ($proposedForwardDateTime->format('H:i:s') > '22:00:00') {
            $proposedForwardDateTime->setTime(22, 0, 0);
        }

        return $proposedForwardDateTime;
    }

    /**
     * @param Order                     $order
     * @param ArrayCollection|Holiday[] $holidays
     * @return \DateTime
     * @throws \Exception
     */
    private function calculateEarliestDateTime(Order $order, ArrayCollection $holidays)
    {
        $deliveryDate = $order->getDeliveryDate();

        if ($deliveryDate === null || $deliveryDate < $order->getCreatedAt()) {
            $deliveryDate = $order->getCreatedAt();
            $deliveryDate = $deliveryDate->add(new \DateInterval('PT5M'));
        }

        $deliveryDate = (clone $deliveryDate);

        /** @var \DateTime $earliestForwardDateTime */
        $earliestForwardDateTime = max([
            $order->getCreatedAt(),
            $deliveryDate->sub(new \DateInterval('P6D'))->setTime(7, 0, 0),
        ]);

        while (!$this->isAcceptableDate($holidays, $earliestForwardDateTime)) {
            $earliestForwardDateTime->add(new \DateInterval('P1D'));
            $earliestForwardDateTime->setTime(0, 0, 0);
        }

        return $earliestForwardDateTime;
    }

    /**
     * @param Order                     $order
     * @param ArrayCollection|Holiday[] $holidays
     * @param Company|null              $supplier
     * @return \DateTime|null
     * @throws \Exception
     */
    private function calculateLatestDateTime(Order $order, ArrayCollection $holidays, Company $supplier = null)
    {
        if (!$order->getDeliveryDate()) {
            return null;
        }

        $latestForwardDateTime = (clone $order->getDeliveryDate());

        $sameDay = false;

        $productGroup = $order->getLines()[0]->getProduct()->getProductgroup();

        if ($productGroup !== null && $productGroup->getSkuPrefix() === 'FLO') {
            $sameDay = true;
        }

        if ($sameDay) {
            $latestForwardDateTime->setTime(14, 0, 0);
        } else {
            $latestForwardDateTime->sub(new \DateInterval('P1D'));
            $latestForwardDateTime->setTime(22, 0, 0);
        }

        while (!$this->isAcceptableDate($holidays, $latestForwardDateTime)) {
            $latestForwardDateTime->sub(new \DateInterval('P1D'));
        }

        /// TODO add supplier relevant conditions when they become available at later releases

        return $latestForwardDateTime;
    }

    /**
     * @param ArrayCollection|Holiday[] $holidays
     * @param \DateTime                 $forwardDatetime
     * @return bool
     */
    private function isAcceptableDate(ArrayCollection $holidays, \DateTime $forwardDatetime)
    {
        if ($forwardDatetime->format('l') === 'Sunday') {
            return false;
        }

        if ($this->isHoliday($holidays, $forwardDatetime)) {
            return false;
        }

        return true;
    }

    /**
     * @param ArrayCollection|Holiday[] $holidays
     * @param \DateTime                 $datetime
     * @return bool
     */
    private function isHoliday($holidays, \DateTime $datetime)
    {
        $filteredHolidays = $holidays->filter(function (Holiday $holiday) use ($datetime) {
            return $holiday->getDate()->format('Y-m-d') === $datetime->format('Y-m-d');
        });

        return !$filteredHolidays->isEmpty();
    }

    /**
     * @param Order $order
     * @return ArrayCollection|Holiday[]
     * @throws \Exception
     */
    private function getRelevantHolidays(Order $order)
    {
        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->getDoctrine()->getRepository(Holiday::class);

        $deliveryDate = $order->getDeliveryDate();

        if (!$deliveryDate) {
            $deliveryDate = (new \DateTime())->add(new \DateInterval('P7D'));
        }

        $qb = $entityRepository->createQueryBuilder('holiday')
            ->select('holiday')
            ->leftJoin('holiday.countries', 'holiday_country')
            ->where('holiday.date BETWEEN :createdAt AND :deliveryDate')
            ->andWhere('holiday_country.country = :country')
            ->setParameter('createdAt', $order->getCreatedAt())
            ->setParameter('deliveryDate', $deliveryDate)
            ->setParameter('country', $order->getDeliveryAddressCountry())
            ->orderBy('holiday.date');

        return new ArrayCollection($qb->getQuery()->getResult());
    }

    /**
     * @return Registry
     */
    protected function getDoctrine()
    {
        return $this->container->get('doctrine');
    }
}
