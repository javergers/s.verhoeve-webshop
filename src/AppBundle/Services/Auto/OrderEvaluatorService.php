<?php

namespace AppBundle\Services\Auto;

use AppBundle\Connector\BakkerMail;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderInternalRemark;
use AppBundle\Entity\Order\OrderJob;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Manager\Order\OrderCollectionManager;
use AppBundle\Manager\Order\OrderManager;
use AppBundle\Services\JobManager;
use AppBundle\Services\OrderManagerService;
use AppBundle\Services\Parameter\ParameterService;
use AppBundle\Services\Webservices\PostcodeNL;
use AppBundle\Services\Sales\Order\OrderActivityService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Exception\ClientException;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Component\Console\Formatter\OutputFormatter;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Services\ConnectorHelper;
use AppBundle\Services\SupplierManagerService;

/**
 * Class OrderEvaluatorService
 * @package AppBundle\Services\Auto
 */
class OrderEvaluatorService
{

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var OutputFormatter
     */
    private $outputFormatter;

    /**
     * @var bool
     */
    private $dryRun;

    /**
     * @var ManagerRegistry
     */
    private $managerRegistry;

    /**
     * @var bool
     */
    private $debug = false;

    /**
     * @var PostcodeNL
     */
    private $postcode;

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @var SupplierManagerService
     */
    private $supplierManagerService;

    /**
     * @var OrderActivityService
     */
    private $orderActivityService;

    /**
     * @var ForwardAtCalculatorService
     */
    private $forwardAtCalculatorService;

    /**
     * @var ConnectorHelper
     */
    private $connectorHelper;

    /**
     * @var OrderManagerService
     */
    private $orderManagerService;

    /**
     * @var ParameterService
     */
    private $parameterService;
    /**
     * @var OrderCollectionManager
     */
    private $orderCollectionManager;
    /**
     * @var OrderManager
     */
    private $orderManager;

    /**
     * OrderEvaluatorService constructor.
     *
     * @param ManagerRegistry $managerRegistry
     * @param PostcodeNL $postcode
     * @param JobManager $jobManager
     * @param SupplierManagerService $supplierManagerService
     * @param OrderActivityService $orderActivityService
     * @param ForwardAtCalculatorService $forwardAtCalculatorService
     * @param ConnectorHelper $connectorHelper
     * @param OrderManagerService $orderManagerService
     * @param ParameterService $parameterService
     * @param OrderCollectionManager $orderCollectionManager
     * @param OrderManager $orderManager
     */
    public function __construct(
        ManagerRegistry $managerRegistry,
        PostcodeNL $postcode,
        JobManager $jobManager,
        SupplierManagerService $supplierManagerService,
        OrderActivityService $orderActivityService,
        ForwardAtCalculatorService $forwardAtCalculatorService,
        ConnectorHelper $connectorHelper,
        OrderManagerService $orderManagerService,
        ParameterService $parameterService,
        OrderCollectionManager $orderCollectionManager,
        OrderManager $orderManager
    ) {
        $this->managerRegistry = $managerRegistry;

        $this->output = new NullOutput();
        $this->postcode = $postcode;
        $this->jobManager = $jobManager;
        $this->supplierManagerService = $supplierManagerService;
        $this->orderActivityService = $orderActivityService;
        $this->forwardAtCalculatorService = $forwardAtCalculatorService;
        $this->connectorHelper = $connectorHelper;
        $this->orderManagerService = $orderManagerService;
        $this->parameterService = $parameterService;
        $this->orderCollectionManager = $orderCollectionManager;
        $this->orderManager = $orderManager;
    }

    /**
     * @param OutputInterface $output
     *
     * @return $this
     */
    public function setOutput(OutputInterface $output)
    {
        $this->output = $output;

        return $this;
    }

    /**
     * @param bool $debug
     *
     * @return $this
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;

        return $this;
    }

    /**
     * @param Order $order
     * @param \DateTime $forwardAt
     * @param Company $supplier
     *
     * @throws \Exception
     */
    private function schedule(Order $order, \DateTime $forwardAt, Company $supplier)
    {
        if (!$this->orderManager->isProcessable($order)) {
            return;
        }

        $autoOrder = $order->getAutoOrder();
        /** @var EntityManager $em */
        $em = $this->managerRegistry->getManager();

        if (!$autoOrder) {
            $autoOrder = new OrderJob();
            $autoOrder->setOrder($order);

            $em->persist($autoOrder);
        }

        $autoOrder->setForwardAt($forwardAt);
        $autoOrder->setSupplier($supplier);

        $job = $autoOrder->getJob();
        if (!$job || $job->isInFinalState()) {
            $job = new Job('auto:process-order', [$order->getId()], true, 'auto');
            $job->addRelatedEntity($order);
            $this->jobManager->addJob($job);

            $autoOrder->setJob($job);
        }

        $job->setExecuteAfter($forwardAt);
        $classMeta = $em->getClassMetadata(Job::class);
        $em->getUnitOfWork()->computeChangeSet($classMeta, $job);

        $em->flush();
    }

    /**
     * @param Order $order
     *
     */
    private function cancel(Order $order)
    {
        $autoOrder = $order->getAutoOrder();

        $em = $this->managerRegistry->getManager();

        if(!$em->isOpen()) {
            $em = $em->create(
                $em->getConnection(),
                $em->getConfiguration()
            );
        }

        if (!$autoOrder) {
            $em->flush();
            return;
        }

        $job = $autoOrder->getJob();

        if (null !== $job && \in_array($job->getState(), [Job::STATE_NEW, Job::STATE_PENDING], true)) {
            $job->setState(Job::STATE_CANCELED);
        }

        $order->setAutoOrder(null);

        if(!$em->contains($autoOrder)) {
            $autoOrder = $em->merge($autoOrder);
        }

        $em->remove($autoOrder);
        $em->flush();
    }

    /**
     * @param Order $order
     * @param bool $dryRun
     * @param bool $process
     * @param bool $force
     *
     * @return bool
     * @throws \Exception
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function process(Order $order, bool $dryRun = false, bool $process = false, bool $force = false): bool
    {
        $this->dryRun = $dryRun;

        $this->debug('<fg=cyan;options=bold>' . $order->getOrderNumber() . '</>');

        if (!$force && $order->getForceManual()) {
            $this->debug(' ! Order mag niet automatisch worden verwerkt.', 'comment');

            if (!$dryRun) {
                $this->cancel($order);
            }

            return false;
        }

        if (!$this->orderManager->isProcessable($order)) {
            $this->debug(' ! Order kan niet worden doorgestuurd (niet verwerkbaar)', 'comment');

            if (!$dryRun) {
                $this->cancel($order);
            }

            return false;
        }

        //fraud check is still due execution, order not ready for auto order yet.
        if (null === $order->getOrderCollection()->getFraudScore() && $this->orderCollectionManager->isFraudCheckable($order->getOrderCollection())) {
            $this->debug(' ! Order kan niet worden doorgestuurd (fraude check niet uitgevoerd)', 'comment');

            if (!$dryRun) {
                $job = new Job('auto:process-order', [$order->getId(), '--no-processing'], true, 'auto');
                $job->addRelatedEntity($order);

                $this->jobManager->addJob($job, 60);
                $this->cancel($order);
            }

            return false;
        }

        //check fraud score
        $orderCollection = $order->getOrderCollection();
        if (!$orderCollection->isProcessable(!$force)) {
            $fraudScore = $orderCollection->getFraudScore();
            $this->debug(sprintf(' ! Order kan niet worden doorgestuurd (fraude score te hoog %s)', $fraudScore),
                'comment');

            if (!$dryRun) {
                $this->cancel($order);
            }

            return false;
        }

        if (null === $order->getPickupAddress() && $order->getDeliveryAddressCountry()->getCode() === 'NL') {
            try {
                $address = $this->postcode->getAddress($order->getDeliveryAddressPostcode(),
                    $order->getDeliveryAddressNumber());
            } catch (ClientException $e) {
                $this->addInternalRemark($order, 'Controleer adres (geen bestaande postcode/huisnummer combinatie).');

                $this->debug(' ! Controleer adres (geen bestaande postcode/huisnummer combinatie).', 'comment');

                if (!$dryRun) {
                    $order->setForceManual(true);

                    /** @var OrderActivityService $orderActivityService */
                    $orderActivityService = $this->orderActivityService;
                    $orderActivityService->add('force_manual', $order, null, ['text' => 'Mogelijk onjuist adres']);

                    $this->cancel($order);
                }

                return false;
            }

            $city = $order->getDeliveryAddressCity();

            switch (strtolower($city)) {
                case 'den haag':
                    $city = "'s-Gravenhage";
                    break;
                case 'den bosch':
                    $city = "'s-Hertogenbosch";
                    break;
            }

            similar_text(strtolower($address->street), strtolower($order->getDeliveryAddressStreet()),
                $streetSimilarity);
            similar_text(strtolower($address->city), strtolower($city), $citySimilarity);

            $matchingPercentage = $this->parameterService->setEntity()->getValue('order_evaluator_matching_percentage');

            if ($streetSimilarity < $matchingPercentage || $citySimilarity < $matchingPercentage) {
                $this->addInternalRemark($order,
                    vsprintf('Controleer adres (postcode/huisnummer combinatie mogelijk onjuist (%d/%d)).', [
                        ceil($streetSimilarity),
                        ceil($citySimilarity),
                    ]));

                $this->debug(vsprintf(' ! Controleer adres (postcode/huisnummer combinatie mogelijk onjuist (%d/%d)).',
                    [
                        ceil($streetSimilarity),
                        ceil($citySimilarity),
                    ]), 'comment');

                if (!$dryRun) {
                    $this->cancel($order);
                }

                return false;
            }
        }

        $supplier = $this->determineSupplier($order);

        // Uitgebreide debug informatie is nog niet af, in overleg met Mathijs volgt dit later
        //        if ($this->output->isVeryVerbose()) {
        //            /** @var Company[] $suppliers */
        //            $suppliers = array_values($this->getSuppliers($order)->toArray());
        //
        //            $tableStyle = (new TableStyle())
        //                ->setHorizontalBorderChar('')
        //                ->setVerticalBorderChar(' ')
        //                ->setCrossingChar('')
        //                ->setCellHeaderFormat('%s')
        //                ->setCellRowContentFormat('%s');
        //
        //            $table = (new Table($this->output))
        //                ->setStyle($tableStyle)
        //                ->setHeaders([
        //                    '▸ Leveranciers:',
        //                    'Voorkeur',
        //                    'Bezorging',
        //                    'Volgorde'
        //                ])
        //                ->setColumnWidths(array(2, 10, 10, 10));;
        //
        //            foreach ($suppliers as $k => $supplier) {
        //                if ($k === 0) {
        //                    $table->getStyle()->setCellRowContentFormat("<options=bold>%s</>");
        //                } else {
        //                    $table->getStyle()->setCellRowContentFormat("%s");
        //                }
        //
        //                $table->addRow([
        //                    "    " . ($k + 1) . ": " . $supplier->getName(),
        //                    "?",
        //                    "?",
        //                    "?",
        //                ]);
        //            }
        //
        //            $table->render();

        //        } elseif ($this->output->isVerbose()) {
        $availableSuppliers = $this->supplierManagerService->getAvailableSuppliersForDelivery($order);

        if ($this->debug) {
            $this->debug(' ▸ Producten:');

            /** @var ArrayCollection|Product[] $products */
            $products = new ArrayCollection();

            foreach ($order->getLines() as $orderLine) {
                if (!$products->contains($orderLine->getProduct())) {
                    $products->add($orderLine->getProduct());
                }
            }

            foreach ($products as $product) {
                $this->debug('    ' . $product->translate()->getTitle() . ' (' . $product->getSku() . ') <comment>#' . $product->getId() . '</comment>');

                $productSuppliers = $this->supplierManagerService->getAvailableSuppliersForProduct($product);

                $this->debug('     ▸ Leveranciers:');

                /** @var ArrayCollection|Company[] $productSuppliers */
                $productSuppliers = $productSuppliers->filter(function (Company $supplier) use ($order) {
                    if (null !== $supplier && $supplier->getSupplierGroups()->isEmpty()) {
                        return true;
                    }

                    if (null === $this->supplierManagerService->getSupplierDeliveryArea($supplier,
                            $order)) {
                        return false;
                    }

                    return true;
                });

                foreach ($productSuppliers as $productSupplier) {
                    $this->debug('         ' . $productSupplier->getName() . ' (' . $productSupplier->getSupplierConnector() . ') <comment>#' . $productSupplier->getId() . '</comment>');
                }
            }
        }

        $this->debug(' ▸ Leveranciers:');

        foreach ($availableSuppliers as $k => $availableSupplier) {
            $isValid = true;

            if ($availableSupplier->getSupplierConnector() === 'BakkerMail') {
                $isValid = false;
            }

            if ($k >= 5) {
                $this->debug('     ...');

                break;
            }

            $output = '     ' . ($k + 1) . ': ' . $availableSupplier->getName() . ' (' . $availableSupplier->getSupplierConnector() . ') <comment>#' . $availableSupplier->getId() . '</comment>';

            if (!$isValid) {
                $this->debug('<fg=red>' . $output . '</>');
            } elseif ($supplier === $availableSupplier) {
                $this->debug('<options=bold>' . $output . '</>');
            } else {
                $this->debug($output);
            }
        }

        //        }

        if (!$supplier) {
            $this->debug('     ! Er kon geen leverancier worden bepaald…', 'comment');

            return false;
        }

        $forwardAt = null;

        $this->debug(' ▸ Doorsturen:');

        try {
            $forwardAt = $this->forwardAtCalculatorService->calculateForwardDateTime($order, $supplier);
        } catch (\Exception $e) {
            $this->debug('     ' . $e->getMessage(), 'error');
        }

        if (!$forwardAt) {
            $this->debug('     ! Doorstuurmoment kan niet worden berekend…', 'comment');

            if (!$dryRun) {
                $this->cancel($order);
            }

            return false;
        }

        $intlDateFormatter = new \IntlDateFormatter('nl_NL', \IntlDateFormatter::FULL, \IntlDateFormatter::NONE);
        $intlTimeFormatter = new \IntlDateFormatter('nl_NL', \IntlDateFormatter::NONE, \IntlDateFormatter::SHORT);

        $this->debug(sprintf('     %s',
            ucfirst($intlDateFormatter->format($forwardAt) . ' vanaf ' . $intlTimeFormatter->format($forwardAt))));

        if (!$supplier || !$forwardAt) {
            if (!$dryRun) {
                $this->cancel($order);
            }

            return false;
        }

        if (!$dryRun) {
            if (!$force && (!$process || $forwardAt > new \DateTime())) {
                $this->schedule($order, $forwardAt, $supplier);
            } elseif ($force || $forwardAt <= new \DateTime()) {
                $connector = $this->connectorHelper->getConnector($supplier);

                if ($connector === null) {
                    throw new \RuntimeException(sprintf("No connector configured for '%s'", $supplier->getName()));
                }

                $supplierOrder = $this->orderManagerService->createSupplierOrder($order, $supplier);
                $this->managerRegistry->getManager()->persist($supplierOrder);
                $this->managerRegistry->getManager()->flush();

                if ($connector->requireForwardingPrices()) {
                    $this->applyDefaultForwardingPrices($supplierOrder);
                }

                $this->managerRegistry->getManager()->flush();
                try {
                    $this->orderManagerService->forwardToSupplier($supplierOrder);
                } catch (\Exception $e) {
                    $order->setForceManual(true);

                    /** @var OrderActivityService $orderActivityService */
                    $orderActivityService = $this->orderActivityService;
                    $orderActivityService->add('force_manual', $order, null, ['text' => $e->getMessage()]);

                    $this->cancel($order);

                    throw $e;
                }

                $this->debug('');
                $this->debug(' <fg=green;options=bold>Doorgestuurd :-)</>');
            }
        }

        return true;
    }

    /**
     * @param SupplierOrder $supplierOrder
     */
    private function applyDefaultForwardingPrices(SupplierOrder $supplierOrder)
    {
        $supplierManager = $this->supplierManagerService;
        $supplier = $supplierOrder->getSupplier();
        $order = $supplierOrder->getOrder();
        foreach ($supplierOrder->getLines() as $supplierOrderLine) {
            if (null !== $supplierOrderLine->getPrice()) {
                continue;
            }

            $supplierProduct = $supplierOrderLine->getConcludedSupplierProduct();

            $supplierOrderLine->setPrice($supplierProduct->getForwardPrice());

            if ($supplierOrderLine->getConcludedSupplierProduct()->getProduct() instanceof TransportType) {
                $supplierDeliveryAreaPrice = $supplierManager->getSupplierDeliveryAreaPrice($supplier, $order);

                if (null !== $supplierDeliveryAreaPrice) {
                    $supplierOrderLine->setForwardPrice($supplierDeliveryAreaPrice);

                    break;
                }
            }
        }
    }

    /**
     * @param Order $order
     *
     * @return Company|null
     */
    private function determineSupplier(Order $order)
    {
        $suppliers = $this->supplierManagerService->getAvailableSuppliersForDelivery($order);
        $suppliers = $suppliers->filter(function (Company $supplier) {
            $connector = $this->connectorHelper->getConnector($supplier);

            // TODO this is sufficient for now, could be more dynamic in the future
            return !($connector instanceof BakkerMail);
        });

        if ($suppliers->count() === 0) {
            return null;
        }

        return $suppliers->first();
    }

    /**
     * @param string $message
     * @param string $style
     */
    private function debug(string $message, string $style = '')
    {
        if (!$this->output->isVerbose()) {
            return;
        }

        if (null === $this->outputFormatter) {
            $this->outputFormatter = new FormatterHelper();
        }

        if ($this->dryRun) {
            $this->output->write('<info>[dry-run]</info>');
        }

        if (!$style) {
            $this->output->writeln($message);
        } else {
            $this->output->writeln(sprintf('<%s>%s</%s>', $style, $message, $style));
        }
    }

    /**
     * @param Order $order
     * @param string $remark
     */
    private function addInternalRemark(Order $order, string $remark): void
    {
        foreach ($order->getInternalRemarks() as $internalRemark) {
            if ($internalRemark->getRemark() === $remark) {
                return;
            }
        }

        $internalRemark = new OrderInternalRemark();
        $internalRemark->setOrder($order);
        $internalRemark->setRemark($remark);

        $order->addInternalRemark($internalRemark);
    }
}
