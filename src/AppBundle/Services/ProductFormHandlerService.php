<?php

namespace AppBundle\Services;

use AppBundle\Entity\Catalog\Product\Personalization;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Exceptions\Sales\CartInvalidSupplierCombinationException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use function in_array;
use RuntimeException;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Services\Letter as LetterService;
use AppBundle\Entity\Catalog\Product\Letter;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class ProductFormHandlerService
 * @package AppBundle\Services
 */
class ProductFormHandlerService
{
    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var LetterService
     */
    private $letterService;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var Designer
     */
    private $designer;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var SiteService
     */
    private $siteService;

    /**
     * ProductFormHandlerService constructor.
     *
     * @param CartService $cartService
     * @param EntityManagerInterface $entityManager
     * @param LetterService $letterService
     * @param RouterInterface $router
     * @param Designer $designer
     * @param SessionInterface $session
     * @param SiteService $siteService
     */
    public function __construct(
        CartService $cartService,
        EntityManagerInterface $entityManager,
        LetterService $letterService,
        RouterInterface $router,
        Designer $designer,
        SessionInterface $session,
        SiteService $siteService
    ) {
        $this->cartService = $cartService;
        $this->entityManager = $entityManager;
        $this->letterService = $letterService;
        $this->router = $router;
        $this->designer = $designer;
        $this->session = $session;
        $this->siteService = $siteService;
    }


    /**
     * @param Form    $form
     * @param Request $request
     * @return string
     * @throws CartInvalidSupplierCombinationException
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function process(Form $form, Request $request): string
    {
        $options = [];
        $card = null;

        $determineSite = $this->siteService->determineSite();
        $addToCartModelEnabled = null !== $determineSite && $determineSite->getAddToCartModalEnabled();

        $productId = $form->get('variation')->getData();

        if (is_object($productId)) {
            $productId = $productId->getId();
        }

        $product = $this->entityManager->getRepository(Product::class)->find($productId);

        if ($form->has('price')) {
            $options['price'] = $form->get('price')->getData();
        }

        if ($form->has('quantity')) {
            $options['quantity'] = $form->get('quantity')->getData();
        }

        if ($request->get('add_card')) {
            $card = $this->entityManager->getRepository(ProductCard::class)->find($request->get('card'));

            if ($card) {
                $options['card_quantity'] = $request->get('card_quantity', null);
                $options['card_text'] = $request->get('card_text');
            }
        }

        $upsells = [];
        if ($request->request->has('upsell')) {
            foreach ($request->request->get('upsell', []) as $productId => $value) {
                //get optional quantities, otherwise use quantity of main product
                $quantity = $options['quantity'];
                if ($request->request->has('upsell_quantity')) {
                    $upsellQuantityArray = $request->request->get('upsell_quantity');
                    if(isset($upsellQuantityArray[$productId])) {
                        $quantity = $upsellQuantityArray[$productId];
                    }
                }

                //change the productId to the variationId to make sure we have the right product
                if (is_numeric($value)) {
                    $productId = $value;
                }

                $upsells[$productId] = $quantity;
            }
        }

        if ($form->has('letter_file') && $letter = $product->getParent()->getLetter()) {
            $letterOptions = $this->uploadLetter($letter, $form);

            if(!empty($letterOptions)) {
                $options['letter'] = $letterOptions;
            }
        }

        $personalizationId = $request->request->get('product_form')['personalization_product_id'] ?? null;
        if ($personalizationId && $request->get('add_personalization')) {
            $personalization = $this->entityManager->getRepository(Personalization::class)->find($personalizationId);

            $options['designer'] = $this->designer->getUuid();

            $this->session
                ->set('current_product', [
                    'product_uuid' => $product->getUuid(),
                    'product_card_uuid' => $card !== null ? $card->getUuid() : null,
                    'product_personalization_uuid' => $personalization->getUuid(),
                    'options' => $options,
                    'upsells' => $upsells
                ]);

            $currentOrderUuid = $this->cartService->getCurrentOrder()->getUuid();

            // Cart still empty
            return $this->router->generate('app_checkout_personalization', [
                'cartOrder' => $currentOrderUuid,
            ]);
        }

        $cartOrderLine = $this->cartService->addProduct($product, $card, null, $options);

        foreach($upsells as $productId => $quantity) {
            $upsellProduct = $this->entityManager->getRepository(Product::class)->find($productId);

            $metadata = ['upsell' => true];

            if($request->request->has('input_fields') && isset($request->request->get('input_fields')[$productId])) {
                $metadata['input_fields'] = $request->request->get('input_fields')[$productId];
            }

            $cartOrder = $cartOrderLine->getCartOrder();
            $cartOrder->addProduct($upsellProduct, $quantity, $cartOrderLine, true, $metadata);
        }

        if ($addToCartModelEnabled) {
            $this->session->set('show_add_to_cart_modal', true);

            return $this->router->generate($request->attributes->get('_route'));
        }

        return $this->router->generate('app_checkout_order', [
            'cartOrder' => $cartOrderLine->getCartOrder()->getUuid(),
            'cartOrderLine' => $cartOrderLine->getUuid(),
        ]);
    }

    /**
     * @param Letter        $letter
     * @param FormInterface $form
     * @return array
     */
    private function uploadLetter(Letter $letter, FormInterface $form): array
    {
        $allowedExtensions = ['pdf'];
        $allowedMimeTypes = ['application/pdf'];

        $options = [];
        if ($form->has('letter_file')) {
            $file = $form->get('letter_file')->getData();
            $options = [];

            if (null !== $file) {
                if (
                    !in_array($file->guessClientExtension(), $allowedExtensions, true)
                    || !in_array($file->getClientMimeType(), $allowedMimeTypes, true)
                ) {
                    throw new RuntimeException('Extension not allowed');
                }

                $options = [
                    'product' => $letter->getId(),
                    'file' => $this->letterService->uploadFile($file->getPathname()),
                    'uuid' => $this->letterService->getUuid(),
                    'filename' => $this->letterService->getFilename(),
                    'path' => $this->letterService->getUploadPath(),
                ];
            }
        }

        return $options;
    }
}
