<?php

namespace AppBundle\Services\Autocomplete;

use AppBundle\Entity\Geography\Postcode;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PostcodeCity
 * @package AppBundle\Services\Autocomplete
 */
class PostcodeCity
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param     $query
     * @param int $limit
     * @return \stdClass
     */
    public function get($query, $limit = 10)
    {
        if (!$limit) {
            $limit = 10;
        }

        if ($limit > 25) {
            $limit = 25;
        }

        $qb = $this->entityManager->getRepository(Postcode::class)->createQueryBuilder("p");
        $qb->distinct();
        $qb->select([
            "p.postcode",
            "ct.name",
        ]);
        $qb->leftJoin("p.cities", "c");
        $qb->leftJoin("c.translations", "ct");
        $qb->leftJoin("c.province", "province");
        $qb->andWhere("province.country = 'BE'");
        $qb->setMaxResults($limit);

        if (is_numeric($query)) {
            $qb->orderBy("p.postcode");
            $qb->andWhere("p.postcode LIKE :q");
            $qb->setParameter("q", $query . "%");
        } else {
            $qb->orderBy("ct.name");
            $qb->andWhere("ct.name LIKE :q");
            $qb->setParameter("q", "%" . $query . "%");
        }

        $results = $qb->getQuery()->getArrayResult();

        $data = new \stdClass();
        $data->items = [];
        $data->results = count($results);
        $data->query = $query;

        foreach ($results as $result) {
            $data->items[] = $result['postcode'] . " " . $result['name'];
        }

        return $data;
    }
}
