<?php

namespace AppBundle\Services;

use AppBundle\Entity\Discount\Voucher;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Exceptions\InvalidSearchException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\PDOStatement;
use Doctrine\ORM\EntityRepository;
use FS\SolrBundle\Doctrine\Hydration\HydrationModes;
use FS\SolrBundle\Query\AbstractQuery;
use FS\SolrBundle\Query\QueryBuilder;
use function is_array;
use Solarium\Core\Client\Client;
use Solarium\Core\Client\Response;
use Solarium\QueryType\Select\Query\Query;
use Solarium\QueryType\Select\Result\Result;
use function strlen;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class OrderSearch
 * @package AppBundle\Services
 */
class OrderSearch
{
    use ContainerAwareTrait;

    private const SEARCH_TYPE_ORDERNUMBER = 0;
    private const SEARCH_TYPE_DATE = 1;
    private const SEARCH_TYPE_TEXT = 2;

    /**
     * @param           $q
     *
     * @param array     $sites
     * @param array     $statuses
     * @param int       $limit
     * @return ArrayCollection
     * @throws \Exception
     */
    public function search($q, ?array $sites = null, ?array $statuses = null, $limit = 100)
    {
        $q = trim($q);

        /** @noinspection NotOptimalRegularExpressionsInspection */
        if (preg_match('/[0-9]{1,2}-[0-9]{1,2}-[0-9]{4}/', $q, $match)) {
            $type = self::SEARCH_TYPE_DATE;
        } /** @noinspection NotOptimalRegularExpressionsInspection */
        elseif (preg_match('/^[0-9]{8}(\-[0-8]{4})?$/', $q, $match)) {
            $type = self::SEARCH_TYPE_ORDERNUMBER;
        } else {
            $type = self::SEARCH_TYPE_TEXT;
        }

        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->container->get('doctrine')->getRepository(Order::class);

        $qb = $entityRepository->createQueryBuilder('o');
        $qb->setMaxResults($limit);

        $qb->leftJoin('o.orderCollection', 'c');

        $qb->leftJoin('o.lines', 'order_lines');
        $qb->addSelect('order_lines');

        $qb->leftJoin('order_lines.product', 'product');
        $qb->addSelect('product');

        if (false !== is_array($sites)) {
            $qb->andWhere('c.site IN (:sites)');
            $qb->setParameter('sites', $sites, Connection::PARAM_INT_ARRAY);
        }

        if (false !== is_array($statuses)) {
            $qb->andWhere('o.status IN (:statuses)');
            $qb->setParameter('statuses', $statuses, Connection::PARAM_STR_ARRAY);
        }

        if ($type === self::SEARCH_TYPE_ORDERNUMBER) {
            $qb->andWhere('c.number = :orderNumber');
            $qb->setParameter('orderNumber', substr($q, 0, 8));

            if (strlen($q) === 13) {
                $qb->andWhere('o.number = :orderOrderNumber');
                $qb->setParameter('orderOrderNumber', substr($q, 9, 4));
            }
        } else {

            $qb->andWhere('c.id IN (:orderIds) OR o.id IN (:orderOrderIds)');

            $qb->setParameter('orderIds', $this->searchOrder($q, $type));
            $qb->setParameter('orderOrderIds', $this->searchOrderOrder($q, $type));
        }

        $result = $qb->getQuery()->getResult();

        return new ArrayCollection($result);
    }

    /**
     * @param $q
     * @param $type
     *
     * @return array
     * @throws InvalidSearchException
     */
    private function searchOrder($q, $type)
    {
        /** @var QueryBuilder $qbVouchers */
        $qbVouchers = $this->container->get('solr.client')->getQueryBuilder(Voucher::class);
        /** @var QueryBuilder $qbPayments */
        $qbPayments = $this->container->get('solr.client')->getQueryBuilder(Payment::class);
        /** @var QueryBuilder $qb */
        $qb = $this->container->get('solr.client')->getQueryBuilder(OrderCollection::class);
        $orderCollectionIds = [];

        switch ($type) {
            case self::SEARCH_TYPE_DATE:
            case self::SEARCH_TYPE_ORDERNUMBER:
                return [];
            case self::SEARCH_TYPE_TEXT:
                $q = '"' . str_replace(' ', '\ ', $q) . '"';

                $qbVouchers->where('code')->expression($q);
                $qbPayments->where('reference')->expression($q);

                $qb
                    ->where('ip')->expression($q)
                    ->orWhere('invoiceAddressAttn')->expression($q)
                    ->orWhere('invoiceAddressCompanyName')->expression($q)
                    ->orWhere('invoiceAddressStreetAndNumber')->expression($q)
                    ->orWhere('invoiceAddressCity')->expression($q)
                    ->orWhere('invoiceAddressPostcode')->expression($q)
                    ->orWhere('invoiceAddressPhoneNumber')->expression($q)
                    ->orWhere('customer')->expression($q) // FIXME issue with floriansemm/SolrBundle, name set to email in Entity annotation, but not followed by bundle
                ;
                break;
            default:
                throw new InvalidSearchException('Invalid search type');
        }

        $query = $qb->getQuery();
        $query->addField('id');
        $result = $query->getResult();

        /** @var OrderCollection $collection **/
        foreach($result as $collection){
            $orderCollectionIds[] = $collection->getId();
        }

        $queryVouchers = $qbVouchers->getQuery();
        $resultVouchers = $queryVouchers->getResult();

        /** @var Voucher $voucher */
        foreach($resultVouchers as $voucher) {
            foreach($voucher->getOrderCollections() as $collection) {
                $orderCollectionIds[] = $collection->getId();
            }
        }

        $queryPayments = $qbPayments->getQuery();
        $resultPayments = $queryPayments->getResult();

        /** @var Payment $payment */
        foreach($resultPayments as $payment) {
            $orderCollectionIds[] = (string) $payment->getOrderCollection()->getId();
        }

        return $orderCollectionIds;
    }

    /**
     * @param $q
     * @param $type
     *
     * @return array
     * @throws InvalidSearchException
     */
    private function searchOrderOrder($q, $type)
    {
        $qb = $this->container->get('solr.client')->getQueryBuilder(Order::class);

        switch ($type) {
            case self::SEARCH_TYPE_DATE:
                $date = implode('-', array_reverse(explode('-', $q)));

                $qb->where('deliveryDate')->is(new \DateTime($date));
                break;
            case self::SEARCH_TYPE_ORDERNUMBER:
                if (strlen($q) === 13) {
                    $qb->where('orderNumber')->expression('"' . $q . '"');
                } else {
                    $qb->where('orderNumber')->expression('"' . $q . '*"');
                }
                break;
            case self::SEARCH_TYPE_TEXT:
                $q = '"' . str_replace(' ', '\ ', $q) . '"';

                $qb
                    ->where('deliveryAddressAttn')->expression($q)
                    ->orWhere('deliveryAddressCompanyName')->expression($q)
                    ->orWhere('deliveryAddressStreetAndNumber')->expression($q)
                    ->orWhere('deliveryAddressCity')->expression($q)
                    ->orWhere('deliveryAddressPostcode')->expression($q)
                    ->orWhere('deliveryAddressPhoneNumber')->expression($q)
                    ->orWhere('invoiceReference')->expression($q);
                break;
            default:
                throw new InvalidSearchException('Invalid search type');
        }

        $query = $qb->getQuery();
        $query->addField('id');

        $result = $query->getResult();

        $result = array_map(function (Order $order) {
            return $order->getId();
        }, $result);

        return $result;
    }
}
