<?php

namespace AppBundle\Services\Teamleader;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Manager\CustomerManager;

/**
 * Class CustomerTransformer
 * @package AppBundle\Services\Teamleader
 */
class CustomerTransformer
{
    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var CustomerManager
     */
    private $customerManager;

    /**
     * CustomerTransformer constructor.
     * @param CustomerManager $customerManager
     */
    public function __construct(CustomerManager $customerManager)
    {
        $this->customerManager = $customerManager;
    }

    /**
     * @param Customer $customer
     * @return $this
     */
    public function setCustomer(Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return array
     */
    public function transform(): array
    {
        return [
            'forename' => $this->customer->getFirstname(),
            'surname' => $this->customer->getLastname(),
            'email' => $this->customer->getEmail(),
            'salutation' => $this->customerManager->getSalutation($this->customer),
            'telephone' => $this->customer->getFormattedPhoneNumber(),
            'gsm' => $this->customer->getFormattedMobileNumber(),
            'country' => $this->customer->getMainAddress()->getCountry()->getCode(),
            'zipcode' => $this->customer->getMainAddress()->getPostcode(),
            'city' => $this->customer->getMainAddress()->getCity(),
            'street' => $this->customer->getMainAddress()->getStreet(),
            'number' => $this->customer->getMainAddress()->getNumber(),
            'language' => $this->customer->getLanguage(),
            'gender' => $this->getGenderCode($this->customer->getGender()),
            'dob' => $this->customer->getBirthday() ? $this->customer->getBirthday()->format('U') : null,
            'add_tag_by_string' => implode(',', $this->getTags())
        ];
    }

    /**
     * @return array
     */
    private function getTags(): array
    {
        $tags = [];

        if ($this->customer->getCustomerGroup() && $this->customer->getCustomerGroup()->getTeamleaderTag()) {
            $tags[] = $this->customer->getCustomerGroup()->getTeamleaderTag();
        }

        return $tags;
    }

    /**
     * @param $gender
     * @return string
     */
    private function getGenderCode($gender): string
    {
        switch ($gender) {
            case 'male':
                return 'M';
            case 'female':
                return 'F';
            default:
                return 'U';
        }
    }
}
