<?php

namespace AppBundle\Services;

use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Order\Cart;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Site\Site;
use AppBundle\Entity\Site\SitePaymentMethodSettings;
use AppBundle\Exceptions\PaymentException;
use AppBundle\Interfaces\GatewayInterface;
use AppBundle\Interfaces\MethodInterface;
use AppBundle\Interfaces\PayableInterface;
use AppBundle\Manager\Order\OrderLineManager;
use AppBundle\Manager\Relation\CompanyManager;
use AppBundle\Method\AfterPay;
use AppBundle\Method\Bancontact;
use AppBundle\Method\CreditCard;
use AppBundle\Method\FreeOfCharge;
use AppBundle\Method\GiftCard;
use AppBundle\Method\Ideal;
use AppBundle\Method\Invoice;
use AppBundle\Method\PayPal;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class PaymentService
 * @package AppBundle\Services
 */
class PaymentService
{
    use ContainerAwareTrait;

    /**
     * @var ArrayCollection|MethodInterface[]
     */
    private $methods;

    public function __construct()
    {
        $this->methods = new ArrayCollection();
    }

    /**
     * @return ArrayCollection|GatewayInterface[]
     */
    public function getGateways(): ArrayCollection
    {
        $gateways = new ArrayCollection();
        $gateways->add($this->container->get('app.gateway.adyen'));
        $gateways->add($this->container->get('app.gateway.afterpay'));

        return $gateways;
    }

    /**
     * @param Site $site
     * @return bool
     */
    private function isAdyenGatewayAvailable(Site $site): bool
    {
        return
            $site->getAdyenMerchantAccount() &&
            $site->getAdyenUsername() &&
            $site->getAdyenPassword() &&
            $site->getAdyenSkinCode() &&
            $site->getAdyenHmacKey();
    }

    /**
     * @param Site $site
     * @return bool
     */
    private function isAfterpayGatewayAvailable(Site $site): bool
    {
        return
            $site->getAfterpayMerchantId() &&
            $site->getAfterpayPortfolioId() &&
            $site->getAfterpayPassword();
    }

    /**
     * @param PayableInterface $payable
     * @param string           $customerType
     * @param string           $country
     * @return ArrayCollection
     * @throws \ReflectionException
     */
    public function getMethods(PayableInterface $payable, $customerType = 'customer', $country = 'NL'): ArrayCollection
    {
        if ($this->methods->isEmpty()) {
            if (null === $customerType) {
                if ($payable->getCustomer() && $payable->getCustomer()->getCompany()) {
                    $customerType = 'company';
                } else {
                    $customerType = 'customer';
                }
            }

            $this->addIdeal($payable);
            $this->addCreditCard($payable);
            $this->addPayPal($payable);
            $this->addAfterpay($payable, $customerType, $country);
            $this->addBancontact($payable);
            $this->addInvoice($payable, $customerType, $country);
            $this->addGiftCard($payable);
            $this->addFreeOfCharge($payable);

            $this->methods = $this->methods->filter(function (MethodInterface $method) use ($payable, $country) {
                if ($method->getEntity() && $method->getEntity()->getMaxAmount() && $payable->getPaymentAmount() > $method->getEntity()->getMaxAmount()) {
                    return false;
                }

                $sitePaymentSettings = $payable->getSite()->getPaymentMethodSettings()->filter(function (
                    SitePaymentMethodSettings $sitePaymentMethodSettings
                ) use ($method) {
                    return $sitePaymentMethodSettings->getPaymentMethod() === $method->getEntity();
                })->first();

                if (!$sitePaymentSettings) {
                    return false;
                }

                if ($sitePaymentSettings->getEnabled() === false) {
                    return false;
                }

                if (!$method->getEntity()->getCountries()->isEmpty()) {
                    $contains = !$method->getEntity()->getCountries()->filter(function (Country $paymentMethodCountry
                    ) use ($country) {
                        return $paymentMethodCountry->getCode() === $country;
                    })->isEmpty();

                    if (!$contains) {
                        return false;
                    }
                }

                return true;
            });
        }

        return $this->methods;
    }

    /**
     * @param PayableInterface $payable
     */
    private function addIdeal(PayableInterface $payable): void
    {
        if (!$this->isAdyenGatewayAvailable($payable->getSite())) {
            return;
        }

        if ($payable->getPaymentAmount() <= 0) {
            return;
        }

        $this->methods->add(new Ideal($this->container->get('app.gateway.adyen')));
    }

    /**
     * @param PayableInterface $payable
     */
    private function addCreditCard(PayableInterface $payable): void
    {
        if (!$this->isAdyenGatewayAvailable($payable->getSite())) {
            return;
        }

        //check if this payment is still allowed to be shown
        if ($payable) {
            $failedPayments = 0;

            foreach ($payable->getPayments() as $payment) {
                if (null !== $payment->getPaymentmethod() && $payment->getPaymentmethod()->getCode() === 'creditcard' && \in_array($payment->getStatus()->getId(),
                        ['refused', 'failed'])) {
                    $failedPayments++;
                }

                if ($failedPayments >= 2) {
                    return;
                }
            }
        }

        $site = $payable->getSite();

        if (null === $site->getAdyenCseToken() || null === $site->getAdyenCseKey()) {
            return;
        }

        if ($payable->getPaymentAmount() <= 0) {
            return;
        }

        $this->methods->add(new CreditCard($this->container->get('app.gateway.adyen')));
    }

    /**
     * @param PayableInterface $payable
     */
    private function addPayPal(PayableInterface $payable): void
    {
        if (!$this->isAdyenGatewayAvailable($payable->getSite())) {
            return;
        }

        if ($payable->getPaymentAmount() <= 0) {
            return;
        }

        $this->methods->add(new PayPal($this->container->get('app.gateway.adyen')));
    }

    /**
     * @param PayableInterface $payable
     */
    private function addBancontact(PayableInterface $payable): void
    {
        if (!$this->isAdyenGatewayAvailable($payable->getSite())) {
            return;
        }

        if ($payable->getPaymentAmount() <= 0) {
            return;
        }

        if (!$this->isBelgiumCustomer($payable)) {
            return;
        }

        $this->methods->add(new Bancontact($this->container->get('app.gateway.adyen')));
    }

    /**
     * @param PayableInterface $payable
     * @return bool
     */
    private function isBelgiumCustomer(PayableInterface $payable): bool
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request !== null) {
            $cart = $request->request->get('cart');

            if ($cart !== null && array_key_exists('customerCountry', $cart) && $cart['customerCountry'] === 'BE') {
                return true;
            }
        }

        /** @var OrderCollection $payable */
        return $payable->getInvoiceAddressCountry() !== null && $payable->getInvoiceAddressCountry()->getCode() === 'BE';
    }

    /**
     * @param PayableInterface $payable
     * @param string           $customerType
     * @param string           $country
     */
    private function addInvoice(PayableInterface $payable, $customerType, $country): void
    {
        if ($payable->getPaymentAmount() <= 0) {
            return;
        }

        if ($customerType !== 'company') {
            return;
        }

        if (!$payable->getCustomer() || !$payable->getCustomer()->getCompany() || $payable->getCustomer()->getCompany()->getVerified() === false) {
            return;
        }

        if (false === $this->getCompanyManager()->canPurchaseByCredit($payable->getCustomer()->getCompany(), $payable->getTotalPrice())) {
            return;
        }

        $invoice = new Invoice();
        $invoice->setDoctrine($this->container->get('doctrine'));
        $invoice->setJobManager($this->container->get('job.manager'));

        $this->methods->add($invoice);
    }

    /**
     * @param PayableInterface $payable
     */
    private function addGiftCard(PayableInterface $payable): void
    {
        if ($this->container->get('kernel')->getEnvironment() !== 'dev') {
            return;
        }

        if (!$this->isAdyenGatewayAvailable($payable->getSite())) {
            return;
        }

        if ($payable->getPaymentAmount() <= 0) {
            return;
        }

        $this->methods->add(new GiftCard($this->container->get('app.gateway.adyen')));
    }

    /**
     * @param PayableInterface $payable
     * @param string           $customerType
     * @param string           $country
     * @throws \ReflectionException
     */
    private function addAfterpay(PayableInterface $payable, $customerType, $country): void
    {
        if ($payable->getPaymentAmount() <= 0) {
            return;
        }

        if (!$this->isAfterpayGatewayAvailable($payable->getSite())) {
            return;
        }

        if ($customerType !== 'customer') {
            return;
        }

        $afterpay = new AfterPay($this->container->get('app.gateway.afterpay'), $this->container->get(OrderLineManager::class));

        $orderCollection = null;

        if ($payable instanceof OrderCollection) {
            $orderCollection = $payable;
        } elseif ($payable instanceof Cart) {
            if ($payable->getOrderCollection()) {
                $orderCollection = $payable->getOrderCollection();
            }
        }

        if ($orderCollection === null || !$this->getEntityManager()->getRepository(Payment::class)->findBy([
            'orderCollection' => $orderCollection,
            'paymentmethod' => $afterpay->getEntity(),
            'status' => 'refused',
        ])) {
            $this->methods->add($afterpay);
        }
    }

    /**
     * @param PayableInterface $payable
     */
    private function addFreeOfCharge(PayableInterface $payable): void
    {
        if ($payable->getPaymentAmount() > 0) {
            return;
        }

        $freeOfCharge = new FreeOfCharge();
        $freeOfCharge->setDoctrine($this->container->get('doctrine'));

        $this->methods->add($freeOfCharge);
    }

    /**
     * @param PayableInterface $payable
     * @param                  $methodName
     * @return MethodInterface
     * @throws \Exception
     */
    public function getMethod(PayableInterface $payable, $methodName): MethodInterface
    {
        /** @var OrderCollection $payable */
        $customerType = (!$payable->getCompany() ? 'customer' : 'company');
        $country = $payable->getInvoiceAddressCountry() ? $payable->getInvoiceAddressCountry()->getCode() : 'NL';

        $paymentMethod = $this->getMethods($payable, $customerType, $country)->filter(function (
            MethodInterface $paymentMethod
        ) use ($methodName) {
            return strcasecmp((new \ReflectionClass($paymentMethod))->getShortName(), $methodName) === 0;
        })->current();

        if (!$paymentMethod) {
            Throw new \RuntimeException("Payment method '" . $methodName . "' doesn't exists or isn't valid for payable");
        }

        return $paymentMethod;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->container->get('doctrine')->getManager();
    }

    /**
     * @return CompanyManager
     */
    public function getCompanyManager(): CompanyManager
    {
        return $this->container->get(CompanyManager::class);
    }

    /**
     * @return array
     *
     * @throws PaymentException
     */
    public function getAdyenCseParams(): array
    {
        return [
            'adyen_cse_js' => $this->container->get('app.gateway.adyen')->getCseJs(),
            'adyen_cse_key' => $this->container->get('app.domain')->getDomain()->getSite()->getAdyenCseKey(),
        ];
    }
}
