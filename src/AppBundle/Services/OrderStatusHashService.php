<?php

namespace AppBundle\Services;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderDeliveryStatus;
use AppBundle\Exceptions\DeliveryStatusNotFoundException;
use AppBundle\Exceptions\InvalidHashException;
use AppBundle\Exceptions\OrderNotFoundException;
use AppBundle\Exceptions\OrderStatusNotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OrderStatusHashService
 * @package AppBundle\Services
 * @deprecated
 */
class OrderStatusHashService
{

    /** @var array */
    private $allowedTypes = [
        'processed',
        'delivered',
    ];

    /** @var string */
    private $type;

    /** @var string */
    private $status;

    /** @var string */
    private $message = '';

    /** @var Order */
    private $order;

    /** @var EntityManagerInterface $em */
    private $em;

    /** @var bool */
    private $modified = false;

    /**
     * OrderStatusService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * Process order / delivery status based on a hash.
     *
     * @param string $hash
     * @throws DeliveryStatusNotFoundException
     * @throws InvalidHashException
     * @throws OrderNotFoundException
     * @throws \Exception
     */
    public function processHash($hash)
    {
        $this->modified = false;
        $hashObj = $this->decryptHash($hash);

        $hashObj = $this->validateHash($hashObj);

        // Set and validate supplied params of hash.
        $this
            ->setType($hashObj->type)
            ->setStatus($hashObj->status)
            ->setMessage($hashObj->status)
            ->setOrderById($hashObj->orderOrderId);

        switch ($this->getType()) {
            case 'delivered':
                $this->processDeliveryStatus();
                break;
            case 'processed':
            default:
                $this->processStatus();
                break;
        }

    }

    /**
     * @param $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $message
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function isStatusModified()
    {
        return (bool)$this->modified;
    }

    /**
     * Get order by id throws exception if not found.
     *
     * @param $orderId
     * @return $this
     * @throws OrderNotFoundException
     */
    public function setOrderById($orderId)
    {
        $order = $this->em
            ->getRepository(Order::class)
            ->find($orderId);

        if (is_null($order)) {
            throw new OrderNotFoundException;
        }

        $this->order = $order;

        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    public function processStatus()
    {
        $orderStatus = $this->getStatus();

        if ($orderStatus === null) {
            throw new OrderStatusNotFoundException;
        }

        $newStatus = $this->order->getStatus();

        // update status if not already completed and is differently.
        if ($newStatus !== $orderStatus && $newStatus !== 'complete'
        ) {
            $this->order->setStatus($orderStatus); // @todo: workflow refactor
            $this->modified = true;
        }

        return $this;
    }

    /**
     * @return $this
     * @throws DeliveryStatusNotFoundException
     * @throws \Exception
     */
    private function processDeliveryStatus()
    {
        $deliveryStatus = $this->em
            ->getRepository(OrderDeliveryStatus::class)
            ->find($this->getStatus());

        if (is_null($deliveryStatus)) {
            throw new DeliveryStatusNotFoundException;
        }

        // Only assign status if not set or equal
        if (
            !$this->order->getDeliveryStatus()
            || $this->order->getDeliveryStatus()->getId() != $deliveryStatus->getId()
        ) {

            $this->order->setDeliveryStatus($deliveryStatus);

            if (!empty($this->message)) {
                $this->order->setMetadata([
                    'delivery_message' => $this->message,
                ]);
            }

            $this->modified = true;
        }

        // If delivery succeeds, turn status to complete
        if ($this->getStatus() !== 'failed') {
            $this->setType('processed');
            $this->setStatus('complete');
            $this->processStatus();
        }

        return $this;

    }

    /**
     * @param array $hashObj
     * @return bool|string
     */
    public function encryptHash($hashObj = [])
    {
        return base64_encode(base64_encode(json_encode($hashObj) . 'epicstring'));
    }

    /**
     * @param $hash
     * @return mixed
     */
    public function decryptHash($hash)
    {
        return json_decode(str_replace('epicstring', '', base64_decode(base64_decode($hash))));
    }

    /**
     * @param $hashObj
     * @return object
     * @throws InvalidHashException
     */
    private function validateHash($hashObj)
    {
        $resolver = new OptionsResolver();

        $resolver->setDefaults([
            'message' => '',
            'data' => '',
        ]);

        $resolver->setRequired([
            'type',
            'status',
            'orderOrderId',
        ]);

        $resolver->setAllowedTypes('status', 'string');
        $resolver->setAllowedTypes('message', 'string');
        $resolver->setAllowedTypes('orderOrderId', 'int');
        $resolver->setAllowedValues('type', $this->allowedTypes);

        try {
            return (object)$resolver->resolve((array)$hashObj);
        } catch(\Exception $exception) {
            throw new InvalidHashException();
        }
    }
}
