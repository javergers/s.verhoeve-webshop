<?php

namespace AppBundle\Services;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Manager\Catalog\Product\ProductPriceManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use JsonLd\Context;

/**
 * Class JsonLdGenerator
 * @package AppBundle\Services
 */
class JsonLdGenerator
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var ProductPriceManager
     */
    protected $productPriceManager;

    /**
     * JsonLdGenerator constructor.
     * @param RequestStack        $requestStack
     * @param ProductPriceManager $productPriceManager
     */
    public function __construct(RequestStack $requestStack, ProductPriceManager $productPriceManager)
    {
        $this->requestStack = $requestStack;
        $this->productPriceManager = $productPriceManager;
    }

    /**
     * @param OrderCollection $orderCollection
     * @return string
     */
    public function Order(OrderCollection $orderCollection): string
    {
        return $this->getOrderContext($orderCollection)->generate();
    }

    /**
     * @return string
     */
    public function Invoice(): string
    {
        return $this->getInvoiceContext()->generate();
    }

    /**
     * @param Product $product
     * @return string
     */
    public function Product(ProductInterface $product)
    {
        return $this->getProductContext($product)->generate();
    }

    /**
     * Private generators
     */

    /**
     * @return Context
     */
    private function getOrganizationContext(): Context
    {
        return Context::create('organization', [
            'name' => 'Topbloemen.nl',
        ]);
    }

    /**
     * @return Context
     */
    private function getInvoiceContext(): Context
    {
        return Context::create('invoice', [
            'accountId' => 'Order 123456',
            'paymentDueDate' => '2015-11-22',
            'provider' => $this->getOrganizationContext(),
            'PaymentStatus' => 'PaymentDue',
            'totalPaymentDue' => $this->getPriceSpecificationContext(),
            'referencesOrder' => $this->getOrderContext(null),
        ]);
    }

    /**
     * @param OrderCollection|null $orderCollection
     * @return Context
     */
    private function getOrderContext(OrderCollection $orderCollection = null): Context
    {
        void($orderCollection);

        return Context::create('order', [
            'merchant' => $this->getOrganizationContext(),
            'orderNumber' => 123,
            'orderStatus' => 'http://schema.org/OrderProcessing',
            'priceCurrency' => 'EUR',
            'price' => 13.95,
            'acceptedOffer' => $this->getOfferContext(),
            'url' => 'https://topbloemen.nl/order-bewerken',
        ]);
    }

    /**
     * @return Context
     */
    private function getPriceSpecificationContext(): Context
    {
        return Context::create('price_specification', [
            'price' => '70.00',
            'priceCurrency' => 'EUR',
        ]);
    }

    /**
     * @param Product $product
     * @return Context
     * @throws \Exception
     */
    private function getOfferContext(ProductInterface $product = null)
    {
        $price = null;
        if ($product) {
            $price = $this->productPriceManager->getPrice($product);
        }

        return Context::create('offer', [
            'price' => str_replace(',', '.', $price),
            'priceCurrency' => 'EUR',
            'itemCondition' => 'http://schema.org/NewCondition',
            'availability' => 'http://schema.org/InStock',
        ]);
    }

    /**
     * @param ProductInterface $product
     * @return Context
     */
    private function getProductRatingContext(ProductInterface $product){
        try {
            return Context::create('AggregateRating', [
                'ratingValue' => random_int(40, 50) / 10,
                'reviewCount' => date_diff($product->getCreatedAt(), date_create())->days,
            ]);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param Product $product
     *
     * @return Context
     */
    private function getProductContext(ProductInterface $product)
    {
        $request = $this->requestStack->getMasterRequest();
        $imageObject = $product->getMainImage(true);
        $absoluteMediaUrl = null;
        if(null !== $request && null !== $imageObject) {
            $absoluteMediaUrl = sprintf('%s/media/cache/product/%s',
                $request->getSchemeAndHttpHost(),
                $imageObject->getPath()
            );
        }

        return Context::create('product', [
            'name' => $product->getDisplayName(),
            'image' => $absoluteMediaUrl,
            'description' => $product->translate()->getShortDescription(),
            'sku' => $product->getSku(),
            'mpn' => $product->getSku(),
            'url' => $product->getAbsoluteUrl(),
            'offers' => $this->getOfferContext($product),
            'aggregateRating' => $this->getProductRatingContext($product),
        ]);
    }
}
