<?php

namespace AppBundle\Services;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Site\Redirect;
use AppBundle\Entity\Site\Site;
use AppBundle\Interfaces\Seo\SlugInterface;
use AppBundle\Services\Parameter\ParameterService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\TagAwareAdapterInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

//use AppBundle\Entity\Relation\Company;
//use Doctrine\ORM\QueryBuilder;

/**
 * Class DynamicRouteService
 * @package AppBundle\Services
 */
class DynamicRoutesService
{
    public const CACHE_KEY = 'dynamic_routes';

    /**
     * @var TagAwareAdapterInterface
     */
    private $cache;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ParameterService
     */
    private $parameterService;

    /**
     * @param TagAwareAdapterInterface $cache
     * @param EntityManagerInterface   $entityManager
     * @param ParameterService         $parameterService
     */
    public function __construct(
        TagAwareAdapterInterface $cache,
        EntityManagerInterface $entityManager,
        ParameterService $parameterService
    ) {
        $this->cache = $cache;
        $this->entityManager = $entityManager;
        $this->parameterService = $parameterService;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function clearCache()
    {
        $this->cache->deleteItem(self::CACHE_KEY);
    }

    /**
     * @return RouteCollection
     * @throws InvalidArgumentException
     */
    public function getRouteCollection()
    {
        $cacheItem = $this->cache->getItem(self::CACHE_KEY);

        if (!$cacheItem->isHit()) {
            $this->build();
        }

        return $cacheItem->get();
    }

    /**
     * @return RouteCollection
     * @throws InvalidArgumentException
     */
    public function build()
    {
        $routeCollection = $this->generateRouteCollection();

        $cacheItem = $this->cache->getItem(self::CACHE_KEY);
        $cacheItem->set($routeCollection);

        $this->cache->save($cacheItem);

        return $routeCollection;
    }

    /**
     * @return RouteCollection
     */
    private function generateRouteCollection()
    {
        $routeCollection = new RouteCollection();

        $this->assortments($routeCollection);
        $this->subSites($routeCollection);
        $this->pages($routeCollection);
        $this->redirects($routeCollection);
        //$this->customCompanyPages($routeCollection);

        return $routeCollection;
    }

    /**
     * Deze code is pas nodig op het moment dat we topbloemen gaan implementeren in het topgeschenken systeem
     *
     * @param RouteCollection $routes
     */
//    private function countries(RouteCollection $routes)
//    {
//        $qb = $this->em->getRepository(Country::class)->createQueryBuilder("country");
//        $qb->addSelect("country_translations");
//        $qb->leftJoin("country.translations", "country_translations");
//
//        $countries = $qb->getQuery()->getResult();
//
//        foreach ($countries as $country) {
//            $slug = $country->translate("nl")->getSlug();
//
//            $route = new Route("/". $slug, array(
//                "_controller" => 'AppBundle:Geographic\Country:index',
//                "country" => $country->getCode()
//            ), array());
//
//            $name = "country-". $slug;
//
//            $routes->add($name, $route);
//        }
//    }

    /**
     * Deze code is pas nodig op het moment dat we topbloemen gaan implementeren in het topgeschenken systeem
     *
     * @param RouteCollection $routes
     */
//    private function cities(RouteCollection $routes)
//    {
//        $qb = $this->em->getRepository(City::class)->createQueryBuilder("city");
//        $qb->addSelect("city_translations");
//        $qb->leftJoin("city.translations", "city_translations");
//
//        $cities = $qb->getQuery()->getResult();
//
//        foreach ($cities as $city) {
//            $slug = $city->translate("nl")->getSlug();
//
//            $route = new Route("/". $slug, array(
//                "_controller" => 'AppBundle:Geographic\City:index',
//                "city" => $city->getId()
//            ), array());
//
//            $name = "city-". $slug;
//
//            $routes->add($name, $route);
//        }
//    }

    /**
     * @param RouteCollection $routeCollection
     */
    private function assortments(RouteCollection $routeCollection)
    {
        foreach ($this->getAssortments() as $assortment) {
            //set default site for company assortments
            if (null !== $assortment->getOwner() && $assortment->getSites()->isEmpty()) {
                $defaultSiteId = $this->parameterService->setEntity()->getValue('default_company_site');
                $defaultSite = $this->entityManager->getRepository(Site::class)->find((int)$defaultSiteId);

                if (null !== $defaultSite) {
                    $this->generateAssortmentRoutes($routeCollection, $assortment, $defaultSite);
                }
            } else {
                foreach ($assortment->getSites() as $site) {
                    $this->generateAssortmentRoutes($routeCollection, $assortment, $site);
                }
            }
        }
    }

    /**
     * @return Assortment[]
     */
    private function getAssortments(): array
    {
        $assortmentRespository = $this->entityManager->getRepository(Assortment::class);

        $qb = $assortmentRespository->createQueryBuilder('assortment');
        $qb->addSelect('assortment_translations');
        $qb->addSelect('sites');
        $qb->addSelect('domains');
        $qb->addSelect('kiyoh_company');
        $qb->addSelect('assortment_products');
        $qb->addSelect('product');
        $qb->addSelect('product_translations');
        $qb->leftJoin('assortment.translations', 'assortment_translations');
        $qb->leftJoin('assortment.sites', 'sites');
        $qb->leftJoin('sites.domains', 'domains');
        $qb->leftJoin('domains.kiyohCompany', 'kiyoh_company');
        $qb->leftJoin('assortment.assortmentProducts', 'assortment_products');
        $qb->leftJoin('assortment_products.product', 'product');
        $qb->leftJoin('product.translations', 'product_translations');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param RouteCollection $routeCollection
     * @param Assortment      $assortment
     * @param Site            $site
     */
    private function generateAssortmentRoutes(
        RouteCollection $routeCollection,
        Assortment $assortment,
        Site $site
    ): void {
        $routeLocales = $this->getRouteLocales($site);

        foreach ($routeLocales as $key => $routeLocale) {
            $this->createRoute($routeCollection, $assortment, 'AppBundle:Assortment:index', $routeLocale, $key,
                'assortment');
        }

        ## products
        foreach ($routeLocales as $key => $routeLocale) {
            /** @var Product $product */
            foreach (array_filter($assortment->getProducts()->toArray()) as $product) {
                $product = $product->getParent() ?? $product;

                $this->createRoute($routeCollection, $product, 'AppBundle:Product:index', $routeLocale, $key,
                    'product');
            }
        }
    }

    /**
     * Deze code is pas nodig op het moment dat we topbloemen gaan implementeren in het topgeschenken systeem
     *
     * @param RouteCollection $routes
     */
//    private function countries(RouteCollection $routes)
//    {
//        $qb = $this->em->getRepository(Country::class)->createQueryBuilder("country");
//        $qb->addSelect("country_translations");
//        $qb->leftJoin("country.translations", "country_translations");
//
//        $countries = $qb->getQuery()->getResult();
//
//        foreach ($countries as $country) {
//            $slug = $country->translate("nl")->getSlug();
//
//            $route = new Route("/". $slug, array(
//                "_controller" => 'AppBundle:Geographic\Country:index',
//                "country" => $country->getCode()
//            ), array());
//
//            $name = "country-". $slug;
//
//            $routes->add($name, $route);
//        }
//    }

    /**
     * Deze code is pas nodig op het moment dat we topbloemen gaan implementeren in het topgeschenken systeem
     *
     * @param RouteCollection $routes
     */
//    private function cities(RouteCollection $routes)
//    {
//        $qb = $this->em->getRepository(City::class)->createQueryBuilder("city");
//        $qb->addSelect("city_translations");
//        $qb->leftJoin("city.translations", "city_translations");
//
//        $cities = $qb->getQuery()->getResult();
//
//        foreach ($cities as $city) {
//            $slug = $city->translate("nl")->getSlug();
//
//            $route = new Route("/". $slug, array(
//                "_controller" => 'AppBundle:Geographic\City:index',
//                "city" => $city->getId()
//            ), array());
//
//            $name = "city-". $slug;
//
//            $routes->add($name, $route);
//        }
//    }

    /**
     * @param RouteCollection $routeCollection
     */
    private function pages(RouteCollection $routeCollection)
    {
        foreach ($this->getPages() as $page) {
            $this->generatePageRoutes($routeCollection, $page, $page->getSite());
        }
    }

    /**
     * @return Page[]
     */
    private function getPages(): array
    {
        $pageRespository = $this->entityManager->getRepository(Page::class);

        $qb = $pageRespository->createQueryBuilder('page');
        $qb->addSelect('page_translations');
        $qb->addSelect('site');
        $qb->addSelect('domains');
        $qb->addSelect('kiyoh_company');
        $qb->leftJoin('page.site', 'site');
        $qb->leftJoin('page.translations', 'page_translations');
        $qb->leftJoin('site.domains', 'domains');
        $qb->leftJoin('domains.kiyohCompany', 'kiyoh_company');
        $qb->andWhere('page_translations.slug IS NOT NULL');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param RouteCollection $routeCollection
     * @param Page            $page
     * @param Site            $site
     */
    private function generatePageRoutes(RouteCollection $routeCollection, Page $page, Site $site): void
    {
        $routeLocales = $this->getRouteLocales($site);

        foreach ($routeLocales as $key => $routeLocale) {
            $this->createRoute($routeCollection, $page, 'AppBundle:Page:index', $routeLocale, $key, 'page');
        }
    }

    /**
     * @param RouteCollection $routeCollection
     */
    private function subSites(RouteCollection $routeCollection)
    {
        foreach ($this->getSubSites() as $subSite) {
            $this->generatesubSiteRoutes($routeCollection, $subSite, $subSite->getParent());
        }
    }

    /**
     * @return Site[]
     */
    private function getSubSites(): array
    {
        $subSiteRespository = $this->entityManager->getRepository(Site::class);

        $qb = $subSiteRespository->createQueryBuilder('site');
        $qb->where($qb->expr()->isNotNull('site.parent'));

        return $qb->getQuery()->getResult();
    }

    /**
     * @param RouteCollection $routeCollection
     * @param Site            $subSite
     * @param Site            $site
     */
    private function generatesubSiteRoutes(RouteCollection $routeCollection, Site $subSite, Site $site): void
    {
        $routeLocales = $this->getRouteLocales($site);

        if($subSite->getHomepage() !== null) {
            foreach ($routeLocales as $key => $routeLocale) {
                $this->createRoute($routeCollection, $subSite, 'AppBundle:Page:index', $routeLocale, $key, 'page',
                    $subSite->getHomepage()->getId());
            }
        } else {
            print sprintf('No homepage found for subsite %d %s', $subSite->getId(), $subSite->getDescription()) . PHP_EOL;
        }
    }

    /**
     * @param RouteCollection $routeCollection
     */
    private function redirects(RouteCollection $routeCollection)
    {
        foreach ($this->getRedirects() as $redirect) {
            $hostnames = [];

            foreach ($redirect->getDomains() as $domain) {
                $hostnames[] = $domain->getDomain();
            }

            if (empty($hostnames)) {
                $hostnames[] = null;
            }

            foreach ($hostnames as $hostname) {
                $route = new Route($redirect->getOldUrl());
                $route->setDefaults([
                    '_controller' => 'AppBundle:Seo\Redirect:index',
                    'redirect' => $redirect->getId(),
                ]);

                if ($hostname) {
                    $route->setHost($hostname);
                }

                $name = self::generateRouteNamePrefix($redirect) . ($hostname ? '-' . $hostname : '');

                $routeCollection->add($name, $route);
            }
        }
    }

    /**
     * @return Redirect[]
     */
    private function getRedirects(): array
    {
        /** @var EntityRepository $redirectRespository */
        $redirectRespository = $this->entityManager->getRepository(Redirect::class);

        $qb = $redirectRespository->createQueryBuilder('redirect');
        $qb->addSelect('redirect');
        $qb->addSelect('domains');
        $qb->leftJoin('redirect.domains', 'domains');

        return $qb->getQuery()->getResult();
    }

//    /**
//     * @param RouteCollection $routeCollection
//     */
//    private function customCompanyPages(RouteCollection $routeCollection)
//    {
//        /** @var EntityRepository $redirectRespository */
//        $companyRespository = $this->entityManager->getRepository(Company::class);
//
//        /** @var QueryBuilder $qb */
//        $companies = $companyRespository->createQueryBuilder('c')
//            ->leftJoin('c.site', 'cs')
//            ->andWhere('cs.slug is not null')
//            ->getQuery()
//            ->getResult();
//
//        /**
//         * @var Company[] $companies
//         */
//        foreach ($companies as $company) {
//            $base = '/' . $company->getSite()->getSlug();
//            $companySlug = str_replace('/', null, $company->getSite()->getSlug());
//            $host = str_replace('/', null, $base) . '.dev.topgeschenken.nl';
//
//            //setup default shop url
//            $route = new Route('');
//            $route->setDefaults([
//                '_controller' => 'AppBundle:Company\Sitee:index',
//                'company' => $company->getId(),
//            ]);
//
//            $name = $companySlug . '_' . 'custom_authentication_index';
//
//            $route->setHost($host);
//
//            $routeCollection->add($name, $route);
//
//            //setup login url
//            $route = new Route('/inloggen');
//            $route->setDefaults([
//                '_controller' => 'AppBundle:Company\Login:index',
//                'company' => $company->getId(),
//            ]);
//
//            $route->setHost($host);
//
//            $name = $companySlug . '_' . 'custom_authentication_login';
//            $routeCollection->add($name, $route);
//
//            if ($company->getSite()->getRegistrationEnabled()) {
//                //setup register url
//                $route = new Route('/registreren');
//                $route->setDefaults([
//                    '_controller' => 'AppBundle:Company\Registration:index',
//                    'company' => $company->getId(),
//                ]);
//
//                $route->setHost($host);
//
//                $name = $companySlug . '_' . 'custom_authentication_register';
//                $routeCollection->add($name, $route);
//            }
//
//            /** @var Assortment $assortment */
//            foreach ($company->getAssortments() as $assortment) {
//                $assortmentSlug = $assortment->translate()->getSlug();
//
//                if ($assortmentSlug) {
//                    $route = new Route('/' . $assortmentSlug, [
//                        '_controller' => 'AppBundle:Company\Site:assortment',
//                        'assortment' => $assortment->getId(),
//                        'company' => $company->getId(),
//                    ]);
//
//                    $route->setHost($host);
//
//                    $routeCollection->add($companySlug . '_' . $assortmentSlug, $route);
//
//                    foreach ($assortment->getProducts() as $product) {
//                        if (!$product) {
//                            continue;
//                        }
//
//                        $product_slug = $product->translate()->getSlug();
//
//                        if ($product_slug) {
//                            $route = new Route('/' . $product_slug, [
//                                '_controller' => 'AppBundle:Product:index',
//                                'company' => $company->getId(),
//                                'product' => $product->getId(),
//                            ], [], [], str_replace('/', null, $base) . '.dev.topgeschenken.nl');
//
//                            $name = $product_slug . '-' . $host . '-' . 'nl_NL';
//
//                            $routeCollection->add($name, $route);
//                        }
//                    }
//                }
//            }
//        }
//    }

    /**
     * @param Site $site
     * @return array
     */
    private function getRouteLocales(Site $site): array
    {
        if (null !== $site->getParent() && $site->getDomains()->isEmpty()) {
            $domains = $site->getParent()->getDomains();
        } else {
            $domains = $site->getDomains();
        }

        $locales = [];

        foreach ($domains as $domain) {
            $hostname = $domain->getDomain();
            $defaultLocale = $domain->getDefaultLocale();

            $primaryKey = $hostname . '-' . $defaultLocale;

            $locales[$primaryKey] = [
                'locale' => $defaultLocale,
                'prefix' => '/',
                'hostname' => $hostname,
            ];

            $locales[$hostname . '-' . $defaultLocale . '-redirect'] = [
                'locale' => $defaultLocale,
                'prefix' => '/' . $defaultLocale . '/',
                'hostname' => $hostname,
                'redirect' => $primaryKey,
            ];
        }

        foreach ($site->getAvailableLocales() as $locale) {
            foreach ($domains as $domain) {
                $hostname = $domain->getDomain();

                $key = $hostname . '-' . $locale;

                if (array_key_exists($key, $locales)) {
                    continue;
                }

                $locales[$key] = [
                    'locale' => $locale,
                    'prefix' => '/' . $locale . '/',
                    'hostname' => $hostname,
                ];
            }
        }

        return $locales;
    }

    /**
     * @param RouteCollection $routeCollection
     * @param object          $entity
     * @param string          $controller
     * @param array           $routeLocale
     * @param string          $key
     * @param string          $parameter
     * @param int|null        $entityId
     */
    private function createRoute(
        RouteCollection $routeCollection,
        object $entity,
        string $controller,
        array $routeLocale,
        string $key,
        string $parameter,
        ?int $entityId = null
    ) {
        $slug = $this->getSlug($entity, $routeLocale['locale']);

        $name = implode('-', [self::generateRouteNamePrefix($entity), $key]);

        if (empty($routeLocale['redirect'])) {
            $route = new Route($routeLocale['prefix'] . $slug, [
                '_controller' => $controller,
                '_locale' => $routeLocale['locale'],
                $parameter => $entityId ?: $entity->getId(),
            ], [], [], $routeLocale['hostname']);
        } else {
            $route = new Route($routeLocale['prefix'] . $slug, [
                '_controller' => 'Symfony\Bundle\FrameworkBundle\Controller\RedirectController::redirectAction',
                'route' => $slug . '-' . $routeLocale['redirect'],
                '_locale' => $routeLocale['locale'],
                'permanent' => false,
                // 'keepQueryParams' => true, // TODO Enable when on Symfony >=4.1
            ], [], [], $routeLocale['hostname']);
        }

        $routeCollection->add($name, $route);
    }

    /**
     * @param object $entity
     * @param string $locale
     * @return string|null
     */
    private function getSlug(object $entity, string $locale): ?string
    {
        $slug = null;

        if (method_exists($entity, 'translate') && $entity->translate() instanceof SlugInterface) {
            $slug = $entity->translate($locale)->getSlug();
        } elseif ($entity instanceof SlugInterface) {
            $slug = $entity->getSlug();
        }

        if ($slug === null || empty($slug)) {
            return null;
        }

        return $slug;
    }

    /**
     * @param object $entity
     * @return string
     */
    public static function generateRouteNamePrefix(object $entity): string
    {
        $class = get_class($entity);

        return vsprintf('%s:%d', [
            str_replace('\\', '_', strtolower(substr($class, strpos($class, 'Entity') + 7))),
            $entity->getId(),
        ]);
    }
}
