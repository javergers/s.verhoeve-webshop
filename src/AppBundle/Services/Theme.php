<?php

namespace AppBundle\Services;

/**
 * Class Theme
 *
 * @package AppBundle\Services
 */
class Theme
{
    private $domain;

    /**
     * Domain constructor.
     *
     * @param Domain $domain
     */
    public function __construct(Domain $domain)
    {
        $this->domain = $domain;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        if ($this->domain->getDomain()) {
            return strtolower($this->domain->getDomain()->getSite()->getTheme());
        }

        return '';
    }

    /**
     *
     * @return string
     */
    public function getCssDir()
    {
        if (null === $this->domain->getDomain()) {
            return 'bundles/admin/css/';
        }

        return 'build/themes/';
    }

    /**
     *
     * @return string
     */
    public function getImagesDir()
    {
        if (null === $this->domain->getDomain()) {
            return 'bundles/admin/images/';
        }

        return 'build/images/' . $this->domain->getDomain()->getSite()->getTheme() . '-';
    }
}
