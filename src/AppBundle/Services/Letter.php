<?php

namespace AppBundle\Services;

use AppBundle\Services\Filesystem\RackspaceTempUrlPlugin;
use League\Flysystem\Filesystem;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class Letter
 * @package AppBundle\Services
 */
class Letter
{
    use ContainerAwareTrait;

    public const ALLOWED_IMAGE_EXTENSIONS = ['pdf'];

    /** @var string */
    public const STORE_FOLDER = '/letters/';

    /** @var string */
    private $uuid;

    /** @var Filesystem */
    private $filesystem;

    /** @var string */
    private $openstackObjectStore;

    /** @var string */
    private $openstackTempUrlKey;

    /**
     * Designer constructor.
     * @param Filesystem $filesystem
     * @param string     $openstackObjectStore
     * @param string     $openstackTempUrlKey
     */
    public function __construct(
        Filesystem $filesystem,
        string $openstackObjectStore,
        string $openstackTempUrlKey
    ) {
        $this->uuid = Uuid::uuid4()->toString();
        $this->filesystem = $filesystem;
        $this->openstackObjectStore = $openstackObjectStore;

        // Get openstack key.
        $this->openstackTempUrlKey = $openstackTempUrlKey;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     * @return Letter
     */
    public function setUuid(string $uuid = null)
    {
        if (null === $uuid) {
            throw new \RuntimeException('No uuid received');
        }

        $this->uuid = $uuid;

        return $this;
    }

    /**
     * @param string $filename
     * @return string
     */
    public function getUploadPath(string $filename = ''): string
    {
        return self::STORE_FOLDER . $this->uuid . '/' . $filename;
    }

    /**
     * @param string $file
     * @return string
     */
    public function uploadFile(string $file)
    {
        if (!$this->moveFileToObjectStore($file)) {
            throw new \RuntimeException("Letter couldn't be uploaded");
        }

        return $this->getUrl();
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return 'letter.pdf';
    }

    /**
     * @param string $file
     * @return bool
     */
    private function moveFileToObjectStore(string $file): ?bool
    {
        try {
            if (!file_exists($file)) {
                throw new \RuntimeException('Letter not found.');
            }

            return $this->filesystem->putStream($this->getUploadPath($this->getFilename()), fopen($file, 'rb+'));
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param int $expiresIn
     * @return string
     */
    public function getUrl(int $expiresIn = 60): ?string
    {
        if (!$this->filesystem->has($this->getUploadPath($this->getFilename()))) {
            return null;
        }

        return $this->getObjectStoreUrl($expiresIn);
    }

    /**
     * @param $expiresIn
     * @return string
     */
    private function getObjectStoreUrl(int $expiresIn = 60)
    {
        // Get path file path.
        $path = $this->getUploadPath($this->getFilename());

        $params = RackspaceTempUrlPlugin::calculateQueryParams(
            trim($path, '/'),
            time() + $expiresIn,
            $this->openstackTempUrlKey
        );

        return $this->openstackObjectStore . '/private' . $path . '?' . http_build_query($params);
    }
}
