<?php

namespace AppBundle\Services\Finance\Tax;

use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Entity\Finance\Tax\VatRule;
use AppBundle\Entity\Geography\Country;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Interfaces\Sales\OrderLineInterface;
use AppBundle\Services\SiteService;
use AppBundle\Utils\DisableFilter;
use Doctrine\ORM\EntityManagerInterface;
use RuleBundle\Service\ResolverService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class VatGroupService
 * @package AppBundle\Services\Finance\Tax
 */
class VatGroupService
{
    /**
     * @var ResolverService
     */
    protected $resolverService;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var SiteService
     */
    protected $siteService;

    /**
     * VatGroupService constructor.
     * @param ResolverService        $resolverService
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(ResolverService $resolverService, EntityManagerInterface $entityManager, SiteService $siteService)
    {
        $this->resolverService = $resolverService;
        $this->entityManager = $entityManager;
        $this->siteService = $siteService;
    }

    /**
     * @param OrderLineInterface $orderLine
     * @return null|VatGroup
     * @throws \Exception
     */
    public function determineVatGroupForLineInterface(OrderLineInterface $orderLine): ?VatGroup
    {
        $vatGroup = null;

        $filter = DisableFilter::temporaryDisableFilter('publishable');
        $product = $orderLine->getProduct();
        $filter->reenableFilter();

        //get vatGroup based on rules
        $rules = $this->entityManager->getRepository(VatRule::class)->findAll();
        foreach ($rules as $rule) {
            $vatGroups = $this->resolverService->filter(VatGroup::class, $rule->getRule());

            $totalGroups = \iterator_count($vatGroups);
            if ($totalGroups !== 1) {
                continue;
            }

            return current($vatGroups);
        }

        //get vatGroup based on country of order
        $country = $orderLine->getOrder()->getCollection()->getInvoiceAddressCountry();
        if (null !== $country) {
            // @todo: Remove when fixing WEB-3333
            // Belgian supplier shop HACK
            $site = $this->siteService->determineSite();

            if (null !== $site && $site->getId() === 7) {
                return $this->entityManager->getRepository(VatGroup::class)->find(4);
            }
            // END OF HACK

            $vatGroup = null;
            if($product !== null) {
                $vatGroup = $product->getVatGroupForCountry($country);
            }

            if($vatGroup) {
                return $vatGroup;
            }
        }

        if($orderLine->getProduct() instanceof CompanyProduct) {
            $product = $orderLine->getProduct()->getRelatedProduct();
        }

        return $this->determineVatGroupForProduct($product);
    }

    /**
     * @param ProductInterface $product
     * @param Country|null $country
     *
     * @return VatGroup
     */
    public function determineVatGroupForProduct(ProductInterface $product, ?Country $country = null): VatGroup
    {
        $site = $this->siteService->determineSite();

        if(null === $country) {
            if (null === $site) {
                $country = $this->entityManager->getRepository(Country::class)->find('NL');
            } else {
                $country = $site->getCountry();

                // @todo: Remove when fixing WEB-3333
                // Belgian supplier shop HACK
                if ($site->getId() === 7) {
                    return $this->entityManager->getRepository(VatGroup::class)->find(4);
                }
                // END OF HACK
            }
        }

        if($product instanceof CompanyProduct){
            $product = $product->getRelatedProduct();
        }

        $vatGroup = $product->getVatGroupForCountry($country);

        if (null === $vatGroup) {
            throw new \RuntimeException(sprintf('No VAT group found for product %s (%d), country (%s)',
                $product->getName(), $product->getId(), $country->getCode()));
        }

        return $vatGroup;
    }
}
