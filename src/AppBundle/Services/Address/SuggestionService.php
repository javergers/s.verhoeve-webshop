<?php

namespace AppBundle\Services\Address;

use AppBundle\Services\Webservices\WebservicesNL;

/**
 * Class SuggestionService
 * @package AppBundle\Services\Address
 */
class SuggestionService
{
    /**
     * @var WebservicesNL
     */
    private $webservicesNL;

    /**
     * SuggestionService constructor.
     * @param WebservicesNL $webservicesNL
     */
    public function __construct(WebservicesNL $webservicesNL)
    {
        $this->webservicesNL = $webservicesNL;
    }

    /**
     * @param $parameters
     * @return array
     */
    public function suggestAddress($parameters)
    {
        $parameters['province'] = '';
        $parameters['district'] = '';
        $parameters['lettercombination'] = '';
        $parameters['addresstype'] = '';
        $parameters['page'] = 0;
        $parameters['houseNoAddition'] = '';

        $params = $parameters;

        //check on address, housenumber and city first
        $params['nbcode'] = '';

        $addresses = $this->webservicesNL->getAddressSuggestions($params);

        $results = (array)$addresses->results;

        //if no addresses are found, check on postcode + housenumber
        if (empty($results)) {
            $params = $parameters;

            $params['nbcode'] = substr(trim($params['nbcode']), 0, 4);
            $params['lettercombination'] = '';
            $params['street'] = '';
            $params['city'] = '';

            $addresses = $this->webservicesNL->getAddressSuggestions($params);

            if (!empty($addresses)) {
                $results = (array)$addresses->results;
            }
        }

        if (empty($results) || !isset($results['item'])) {
            return null;
        }

        if (!\is_array($results['item'])) {
            $item = (array)$results['item'];
            $results['item'] = [$item];
        }

        $returnAddresses = [];
        $i = 0;

        foreach ($results['item'] as $address) {
            if ($i === 2) {
                break;
            }

            $address = (array)$address;

            $returnAddresses[] = [
                'housenumber' => $address['huisnr'],
                'street' => $address['straatnaam'],
                'postcode' => $address['wijkcode'] . '' . $address['lettercombinatie'],
                'city' => $address['plaatsnaam'],
            ];

            $i++;
        }

        return $returnAddresses;
    }

}
