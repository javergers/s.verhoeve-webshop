<?php

namespace AppBundle\Services\Company;

use AppBundle\DBAL\Types\AppliesToOrderType;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyCustomField;
use AppBundle\Entity\Relation\CompanyCustomFieldOption;
use AppBundle\Entity\Relation\CompanyCustomOrderFieldValue;
use AppBundle\Form\Type\ContainerType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class CompanyCustomFieldService
 * @package AppBundle\Services\Company
 */
class CompanyCustomFieldService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * CompanySkuService constructor.
     * @param EntityManagerInterface $entityManager
     * @param TranslatorInterface    $translator
     */
    public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * @param Company $company
     * @param FormInterface $form
     * @param null $customFieldsApplyToType
     * @return FormInterface
     */
    public function generateCustomFieldsForm(
        Company $company,
        FormInterface $form,
        $customFieldsApplyToType = null
    ): FormInterface {
        if (!$company->getCustomFields()->isEmpty()) {
            $form->add('extra_fields', ContainerType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'extra-field-padding',
                ],
            ]);

            $extraFields = $form->get('extra_fields');

            /** @var CompanyCustomField $customField */
            foreach ($company->getCustomFields() as $customField) {
                $appliesTo = $customField->getAppliesTo();
                if ($customFieldsApplyToType !== null && $customFieldsApplyToType !== $appliesTo && $appliesTo !== (string)AppliesToOrderType::APPLIES_TO_NONE) {
                    continue;
                }

                $options = [
                    'label' => $customField->translate()->getLabel(),
                    'mapped' => false,
                ];

                if ($customField->getType() === ChoiceType::class) {
                    if (!$customField->getOptions()->isEmpty()) {
                        $choices = [];
                        /** @var CompanyCustomFieldOption $option */
                        foreach ($customField->getOptions() as $option) {
                            $choices[$option->getOptionKey()] = $option->getOptionValue();
                        }
                        $options['choices'] = $choices;
                        $options['attr'] = [
                            'class' => 'extra-field-dropdown',
                        ];
                        $options['placeholder'] = '';
                        $options['multiple'] = $customField->getMultiple();
                    } else {
                        continue;
                    }
                }

                $options['required'] = $customField->getRequired();

                $extraFields->add($customField->getFieldKey(), $customField->getType(), $options);
            }
        }

        return $form;
    }

    /**
     * @param Company              $company
     * @param FormInterface        $form
     * @param OrderCollection|null $orderCollection
     * @param Order|null           $order
     * @return bool
     */
    public function saveCustomFieldsFromForm(Company $company, FormInterface $form, ?OrderCollection $orderCollection = null,?Order $order = null): bool
    {
        if (!$company->getCustomFields()->isEmpty()) {
            $extraFieldsForm = $form->get('extra_fields');

            /** @var CompanyCustomField $customField */
            foreach ($company->getCustomFields() as $customField) {
                $key = $customField->getFieldKey();
                if($extraFieldsForm->has($key) === false){
                    continue;
                }

                if($extraFieldsForm->get($key)->getData() === null) {
                    if ($customField->getRequired()) {
                        $form->addError(new FormError(sprintf('%s %s', $customField->translate()->getLabel(),
                            $this->translator->trans('label.is_required'))));
                    }
                    continue;
                }

                $values = [];
                if($customField->getMultiple() === false) {
                    $values[] = $extraFieldsForm->get($key)->getData();
                } else {
                    $values = (array)$extraFieldsForm->get($key)->getData();
                }

                foreach($values as $value) {
                    $customFieldValue = new CompanyCustomOrderFieldValue();
                    $customFieldValue->setCompanyCustomField($customField);
                    $customFieldValue->setOrder($order);
                    $customFieldValue->setOrderCollection($orderCollection);
                    $customFieldValue->setValue($value);
                    $this->entityManager->persist($customFieldValue);
                }
            }
            return true;
        }

        return false;
    }
}
