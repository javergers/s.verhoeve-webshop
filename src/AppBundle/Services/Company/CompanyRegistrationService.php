<?php

namespace AppBundle\Services\Company;

use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyRegistration;
use AppBundle\Exceptions\CouldNotCreateException;
use AppBundle\Form\DataTransformer\CountryCodeToCountryTransformer;
use AppBundle\Services\Webservices\VatInformationExchangeSystem;
use AppBundle\Services\Webservices\WebservicesNL;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

/**
 * Class CompanyRegistrationService
 * @package AppBundle\Services\Company
 */
class CompanyRegistrationService
{

    /**
     * @var WebservicesNL
     */
    private $webservicesNL;

    /**
     * @var VatInformationExchangeSystem
     */
    private $vatInformationExchangeSystem;

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * CompanyRegistrationService constructor.
     * @param EntityManagerInterface       $entityManager
     * @param WebservicesNL                $webservicesNL
     * @param VatInformationExchangeSystem $vatInformationExchangeSystem
     */
    public function __construct(EntityManagerInterface $entityManager, WebservicesNL $webservicesNL, VatInformationExchangeSystem $vatInformationExchangeSystem)
    {
        $this->entityManager = $entityManager;
        $this->webservicesNL = $webservicesNL;
        $this->vatInformationExchangeSystem = $vatInformationExchangeSystem;
    }

    /**
     * @param $country
     * @param $data
     * @return CompanyRegistration|null|object
     */
    public function createCompanyRegistration($country, $data)
    {
        $companyRegistration = null;

        //check country to retrieve companyregistration on registrationNumber of VAT number
        if (strtolower($country) === 'nl') {
            $companyRegistration = $this->entityManager->getRepository(CompanyRegistration::class)->findOneBy([
                'registrationNumber' => $data->chamberOfCommerce,
            ]);
        } else {
            if (strtolower($country) === 'be') {
                $companyRegistration = $this->entityManager->getRepository(CompanyRegistration::class)->findOneBy([
                    'vatNumber' => $data->vatNumber,
                ]);
            }
        }

        //if no registration is found create a new one
        if (!$companyRegistration) {
            $companyRegistration = new CompanyRegistration();
        }

        $countryCodeDataTransformer = new CountryCodeToCountryTransformer($this->entityManager);

        //setup Company Registration Main address
        $companyRegistrationMainAddress = new Address();
        $companyRegistrationMainAddress->setPostcode($data->address->postcode);
        $companyRegistrationMainAddress->setMainAddress(true);
        $companyRegistrationMainAddress->setCity($data->address->city);
        $companyRegistrationMainAddress->setStreet($data->address->street);
        $companyRegistrationMainAddress->setNumber(trim(implode(' ',
            [$data->address->houseNumber, $data->address->houseNumberAddition])));
        $companyRegistrationMainAddress->setCountry($countryCodeDataTransformer->reverseTransform($data->address->countryCode));

        if ($country !== 'nl') {
            $data->postaddress = $data->address;
        }

        //setup Company Registration Post address
        $companyRegistrationPostAddress = new Address();
        $companyRegistrationPostAddress->setInvoiceAddress(true);
        $companyRegistrationPostAddress->setPostcode($data->postaddress->postcode);
        $companyRegistrationPostAddress->setCity($data->postaddress->city);
        $companyRegistrationPostAddress->setStreet($data->postaddress->street);
        $companyRegistrationPostAddress->setNumber(trim(implode(' ',
            [$data->postaddress->houseNumber, $data->postaddress->houseNumberAddition])));
        $companyRegistrationPostAddress->setCountry($countryCodeDataTransformer->reverseTransform($data->postaddress->countryCode));

        //assign country independent values
        $companyRegistration->setName($data->name);
        $companyRegistration->setCountry($countryCodeDataTransformer->reverseTransform($country));
        $companyRegistration->setMainAddress($companyRegistrationMainAddress);
        $companyRegistration->setPostAddress($companyRegistrationPostAddress);

        //assign registration and location number in case of dutch registration, VAT number in case of belgium registration

        if (strtoupper($country) === 'NL') {
            $companyRegistration->setRegistrationNumber($data->chamberOfCommerce);
            $companyRegistration->setEstablishmentNumber($data->chamberOfCommerceMainEstablishment);

            //search for a VAT number based on the chamber of commerce number for dutch companies
            $companyRegistration->setVatNumber($this->webservicesNL->getVatNumberByChamberOfCommerceNumber($data->chamberOfCommerce));
        } else {
            if (strtoupper($country) === 'BE') {
                $companyRegistration->setVatNumber($data->vatNumber);
            }
        }

        return $companyRegistration;
    }

    /**
     * @param null $limit
     * @return array
     */
    public function migrateCompanyRegistrations($limit = null)
    {
        /**
         * @var QueryBuilder $companies ;
         */
        $companies = $this->entityManager->getRepository(Company::class)->createQueryBuilder('c')
            ->where('c.chamberOfCommerceNumber > 0 OR c.vatNumber > 0')
            ->andWhere('c.companyRegistration IS NULL');

        if ($limit) {
            $companies->setMaxResults($limit);
        }

        $companies = $companies->getQuery()->execute();

        $messages = [];

        /** @var Company[] $companies */
        foreach ($companies as $company) {
            try {
                $this->migrateCompany($company);
                $this->entityManager->flush();
            } catch(\Exception $exception) {
                $messages[] = $exception->getMessage();
            }
        }

        return $messages;
    }

    /**
     * @param Company $company
     *
     * @return CompanyRegistration
     */
    public function migrateCompany(Company $company): CompanyRegistration
    {
        $companyRegistration = null;

        try {
            if (
                null !== $company->getChamberOfCommerceNumber()
                && $company->getChamberOfCommerceNumber() > 0
            ) {
                $companyRegistration = $this->createByRegistrationNumber($company->getChamberOfCommerceNumber());
            } elseif (null !== $company->getVatNumber()) {
                $companyRegistration = $this->createByVatNumber($company->getVatNumber());
            }
        } catch (\Exception $exception) {
            throw new \RuntimeException(sprintf('Migrate method failed for company %s with error %s', $company->getName(), $exception->getMessage()));
        }

        if ($companyRegistration) {
            $company->setCompanyRegistration($companyRegistration);
            $this->entityManager->persist($company);
        } else {
            throw new \RuntimeException(sprintf('Could not migrate "%s" to company registration.', $company->getName()));
        }

        return $companyRegistration;
    }

    /**
     * @param        $registrationNumber
     * @param string $countryCode
     * @return CompanyRegistration|null|object
     * @throws CouldNotCreateException
     * @throws \SoapFault
     * @throws \libphonenumber\NumberParseException
     */
    public function createByRegistrationNumber($registrationNumber, $countryCode = 'NL')
    {
        // Check if registation not already exist.
        $companyRegistration = $this->entityManager->getRepository(CompanyRegistration::class)->findOneBy([
            'registrationNumber' => $registrationNumber,
            'country' => $countryCode,
        ]);

        if ($companyRegistration) {
            return $companyRegistration;
        }

        if ($this->webservicesNL->isValidChamberOfCommerceNumber($registrationNumber)) {

            $data = $this->webservicesNL
                ->getCompanyDataByChamberOfCommerceNumber($registrationNumber);

            return $this->createCompanyRegistration($countryCode, $data);

        }

        throw new CouldNotCreateException('Could not create `CompanyRegistration` by registration number');
    }

    /**
     * @param        $vatNumber
     * @param string $countryCode
     * @return CompanyRegistration|null|object
     * @throws CouldNotCreateException
     * @throws \Exception
     */
    public function createByVatNumber($vatNumber, $countryCode = 'BE')
    {
        if ($this->vatInformationExchangeSystem->isValidVatNumber($vatNumber)) {

            $data = $this->vatInformationExchangeSystem->getCompanyDataByVatNumber($vatNumber);

            return $this->createCompanyRegistration($countryCode, $data);
        }

        throw new CouldNotCreateException(sprintf("Could not create 'CompanyRegistration' by vat number '%s'.", $vatNumber));
    }

}
