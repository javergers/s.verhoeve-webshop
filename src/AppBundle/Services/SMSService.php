<?php

namespace AppBundle\Services;

use MessageBird\Client;
use MessageBird\Objects\Message;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class SMSService
 * @package AppBundle\Services
 */
class SMSService
{
    use ContainerAwareTrait;

    /**
     * @var Client
     */
    protected $messageBird;

    /**
     * @param array  $numbers
     * @param string $content
     * @param string $originator
     * @throws \Exception
     */
    public function send(array $numbers, string $content, string $originator)
    {
        $this->messageBird = new Client($this->container->getParameter('messagebird_access_key'));

        if (strlen($originator) > 11) {
            throw new \Exception('The originator cannot contain more than 11 characters.');
        }

        $message = new Message();
        $message->originator = $originator;

        $message->recipients = $numbers;
        $message->body = $content;

        $this->messageBird->messages->create($message);
    }

}
