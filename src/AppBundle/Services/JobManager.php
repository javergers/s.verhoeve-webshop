<?php

namespace AppBundle\Services;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\Proxy;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\ORM\UnitOfWork;
use JMS\JobQueueBundle\Entity\Job;
use Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class JobManager
 * @package AppBundle\Services
 */
class JobManager
{
    /**
     * @var Job[]
     */
    private $scheduledJobs = [];

    /**
     * @var ManagerRegistry
     */
    protected $managerRegistry;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var KernelInterface $kernel
     */
    protected $kernel;

    /**
     * @var bool
     */
    protected $stopNewJobs;

    /**
     * JobManager constructor.
     * @param ManagerRegistry $managerRegistry
     * @param Logger          $logger
     * @param KernelInterface $kernel
     */
    public function __construct(ManagerRegistry $managerRegistry, Logger $logger, KernelInterface $kernel)
    {
        $this->managerRegistry = $managerRegistry;
        $this->logger = $logger;
        $this->kernel = $kernel;
        $this->stopNewJobs = false;
    }

    /**
     * @return bool
     */
    private function getNoJobEnvironment(): bool
    {
        return getenv('DISABLEJOBS') === '1' && PHP_SAPI === 'cli' && $this->kernel->getEnvironment() === 'dev';
    }

    /**
     * @param Job  $job
     * @param int|null
     * @param bool $checkExisting
     * @return Job
     * @throws \Exception
     */
    public function addJob(Job $job, $delay = 2, $checkExisting = true): ?Job
    {
        if($this->getNoJobEnvironment() === true){
            return $job;
        }

        if ($delay) {
            $executeAfter = clone ($job->getExecuteAfter() ?? $job->getCreatedAt());
            $executeAfter->modify(sprintf('+%d second', $delay));

            $job->setExecuteAfter($executeAfter);

        }

        if ($checkExisting) {
            if ($job->getRelatedEntities()) {
                $startableJobs = $this->getStartableJobs($job->getCommand(), $job->getRelatedEntities()[0]);
            } else {
                $startableJobs = $this->getStartableJobs($job->getCommand());
            }

            if (\count($startableJobs) > 0) {
                $this->logger->debug(sprintf("Job '%s' not created, a startable job already exists",
                    trim($job->getCommand() . ' ' . implode(' ', $job->getArgs()))));

                return current($startableJobs);
            }
        }

        $this->logger->debug(sprintf("Job '%s' created",
            trim($job->getCommand() . ' ' . implode(' ', $job->getArgs()))));

        $em = $this->managerRegistry->getManager();
        $em->persist($job);
        $em->getUnitOfWork()->commit([$job]);

        $this->scheduledJobs[] = $job;

        return $job;
    }

    /**
     * @param string $command
     * @param null   $relatedEntity
     * @return Job[]|array|null
     */
    public function getStartableJobs($command, $relatedEntity = null)
    {
        $relClass = null;
        $relId = null;

        if ($relatedEntity) {
            [$relClass, $relId] = $this->getRelatedEntityIdentifier($relatedEntity);
        }

        /** @var EntityManagerInterface $em */
        $em = $this->getDoctrine()->getManager();

        $rsm = new ResultSetMappingBuilder($em);
        $rsm->addRootEntityFromClassMetadata('JMSJobQueueBundle:Job', 'j');

        $sql = ' SELECT j.* FROM jms_jobs j';

        if ($relatedEntity) {
            $sql .= ' INNER JOIN jms_job_related_entities r ON r.job_id = j.id';
        }

        $sql .= ' WHERE j.command = :command';
        $sql .= ' AND j.state IN (:states)';

        if ($relatedEntity) {
            $sql .= ' AND r.related_class = :relClass AND r.related_id = :relId ';
        }

        $params = new ArrayCollection();
        $params->add(new Parameter('command', $command));

        if ($relatedEntity) {
            $params->add(new Parameter('relClass', $relClass));
            $params->add(new Parameter('relId', $relId));
        }

        $params->add(new Parameter('states', [
            Job::STATE_PENDING,
            Job::STATE_NEW,
        ], Connection::PARAM_STR_ARRAY));

        $result = $em->createNativeQuery($sql, $rsm)->setParameters($params)->getResult();

        if ($result) {
            return $result;
        }

        return array_filter($this->scheduledJobs, function (Job $scheduledJob) use ($command, $relatedEntity) {
            return $scheduledJob->getCommand() === $command && $scheduledJob->getRelatedEntities()->contains($relatedEntity);
        });
    }

    /**
     * @param string $command
     * @param null   $relatedEntity
     * @return Job|null
     */
    public function getStartableJob($command, $relatedEntity = null)
    {
        $startableJobs = $this->getStartableJobs($command, $relatedEntity);

        if (!$startableJobs) {
            return null;
        }

        return current($startableJobs);
    }

    /**
     * @param $entity
     * @return array
     */
    private function getRelatedEntityIdentifier($entity)
    {
        \assert('is_object($entity)');

        if ($entity instanceof Proxy) {
            $entity->__load();
        }

        $relClass = ClassUtils::getClass($entity);

        $manager = $this->getDoctrine()->getManagerForClass($relClass);

        if ($manager === null) {
            throw new \RuntimeException(sprintf("No manager found for '%s'", $relClass));
        }

        $relId = $manager->getMetadataFactory()->getMetadataFor($relClass)->getIdentifierValues($entity);

        if (!$relId) {
            throw new \InvalidArgumentException(sprintf('The identifier for entity of class "%s" was empty.',
                $relClass));
        }

        return [$relClass, json_encode($relId)];
    }

    /**
     * @return Registry
     */
    private function getDoctrine()
    {
        return $this->managerRegistry;
    }
}
