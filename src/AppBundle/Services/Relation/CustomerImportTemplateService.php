<?php

namespace AppBundle\Services\Relation;

use AppBundle\Entity\Relation\Company;

use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Exception as WriterException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Class CustomerImportTemplateService
 */
class CustomerImportTemplateService
{
    /** @var string */
    private const FILENAME = 'gebruikers_import';

    /** @var CustomerImportService */
    private $customerImportService;

    /**
     * CustomerImportTemplateService constructor.
     * @param CustomerImportService $customerImportService
     */
    public function __construct(CustomerImportService $customerImportService)
    {
        $this->customerImportService = $customerImportService;
    }

    /**
     *
     * @return BinaryFileResponse
     *
     * @throws Exception
     * @throws WriterException
     */
    public function downloadCustomerImportTemplate(): BinaryFileResponse
    {
        $columns = $this->customerImportService->getUserUploadColumns();

        $spreadsheet = new Spreadsheet();

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('import');

        $cell = 'A';
        foreach ($columns as $columnName) {
            $sheet->getColumnDimension($cell)->setAutoSize(true);
            $sheet->setCellValue($cell . '1', $columnName);
            $sheet->getStyle($cell . '1')->getFont()->setBold(true);
            $cell++;
        }

        $this->appendExampleData($sheet);

        $path = tempnam(sys_get_temp_dir(), self::FILENAME);

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($path);

        $response = new BinaryFileResponse($path, 200, [
            'Content-type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Content-Disposition' => 'attachment; filename="' . self::FILENAME . '.xlsx"',
        ]);

        $response->deleteFileAfterSend(true);

        return $response;
    }

    /**
     * @param Worksheet $sheet
     * @return Worksheet
     */
    private function appendExampleData(Worksheet $sheet): Worksheet
    {
        $cell = 'A';//
        $index = 2;

        $exampleData = [
            [
                'Voornaam',
                '',
                'Achternaam',
                'man/vrouw',
                '',
                '0612345678',
                'Email (Gebruikersnaam)',
                'Wachtwoord12345',
                '1',
            ],
        ];

        foreach ($exampleData as $data) {
            foreach ($data as $column) {
                $sheet->setCellValue($cell . $index, $column);
                $cell++;
            }

            $cell = 'A';
            $index++;
        }

        return $sheet;
    }
}