<?php

namespace AppBundle\Services\Commission;

use AppBundle\Entity\Finance\CommissionInvoice;
use AppBundle\Entity\Finance\CommissionInvoiceSupplierOrder;
use AppBundle\Entity\Site\Site;
use AppBundle\Interfaces\Commission\InvoiceCalculatorInterface;
use AppBundle\Services\PdfService;
use Doctrine\ORM\EntityManagerInterface;
use Swift_Mailer;
use Swift_Message;
use Swift_Mime_SimpleMessage;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Twig\Error\Error;

/**
 * Class InvoiceService
 * @package AppBundle\Services\Commission
 */
class InvoiceService implements InvoiceCalculatorInterface
{
    /**
     * @var int
     */
    private $version = 1;

    /**
     * @var CommissionInvoice
     */
    private $commissionInvoice;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var TwigEngine
     */
    private $twigEngine;
    /**
     * @var ParameterBagInterface
     */
    private $parameterBag;
    /**
     * @var PdfService
     */
    private $pdfService;
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * InvoiceService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param TwigEngine $twigEngine
     * @param ParameterBagInterface $parameterBag
     * @param PdfService $pdfService
     * @param Swift_Mailer $mailer
     */
    public function __construct(EntityManagerInterface $entityManager, TwigEngine $twigEngine, ParameterBagInterface $parameterBag, PdfService $pdfService, Swift_Mailer $mailer)
    {
        $this->entityManager = $entityManager;
        $this->twigEngine = $twigEngine;
        $this->parameterBag = $parameterBag;
        $this->pdfService = $pdfService;
        $this->mailer = $mailer;
    }

    /**
     * @param int $version
     */
    public function setVersion($version): void
    {
        $this->version = $version;
    }

    /**
     * @param CommissionInvoice $commissionInvoice
     * @return InvoiceService
     */
    public function setCommissionInvoice(CommissionInvoice $commissionInvoice): InvoiceService
    {
        $this->commissionInvoice = $commissionInvoice;

        return $this;
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function renderView(): Response
    {
        $commissionOrders = $this->commissionInvoice->getCommissionInvoiceSupplierOrders();
        $commissionOrders = $commissionOrders->filter(function (CommissionInvoiceSupplierOrder $commissionInvoiceSupplierOrder) {
            return !($commissionInvoiceSupplierOrder->getTotalIncl() === 0.00 && $commissionInvoiceSupplierOrder->getRecommissioned());
        });

        $commissionOrders = $commissionOrders->toArray();

        $rowsOnFirstPage = 35;
        $rowsPerPage = 45;

        $pages = [];
        $pages[0] = \array_slice($commissionOrders, 0, $rowsOnFirstPage);

        $rest = \array_slice($commissionOrders, $rowsOnFirstPage);

        $pages = array_merge($pages, array_chunk($rest, $rowsPerPage));

        $template = null;

        switch ($this->version) {
            case 1:
                $template = '@App/pdf/commission/invoice.html.twig';
                break;
            default:
                throw new \RuntimeException(sprintf('Invoice version %d not implemented', $this->version));
        }

        $logo = \dirname($this->parameterBag->get('kernel.root_dir')) . '/web/bundles/app/pdf/briefpapier_topgeschenken.png';

        $html = $this->twigEngine->render($template, [
            'commissionInvoice' => $this->commissionInvoice,
            'pages' => $pages,
            'period' => $this->formatPeriod(),
            'logo' => $logo,
        ]);

        return new Response($html);
    }

    /***
     * @return BinaryFileResponse
     * @throws \Exception
     */
    public function renderPdf(): BinaryFileResponse
    {
        $response = $this->renderView();

        return $this->pdfService->renderFromResponse($response, [
            'Content-Disposition' => 'inline; filename="Orderspecificatie-' . $this->commissionInvoice->getNumber() . '.pdf"',
        ]);
    }

    /**
     * @return string
     */
    private function formatPeriod(): string
    {
        $locale = $this->parameterBag->get('locale');

        $intlDateFormatter = new \IntlDateFormatter($locale, \IntlDateFormatter::FULL, \IntlDateFormatter::NONE);

        return implode(' ',
            \array_slice(explode(' ', $intlDateFormatter->format($this->commissionInvoice->getDate())), 2, 2));
    }

    /**
     * @return bool
     * @throws Error
     * @throws \Exception
     */
    public function send(): bool
    {
        $path = tempnam(sys_get_temp_dir(), 'pdf');

        $response = $this->renderView();

        $this->pdfService->saveFromResponse($response, $path);

        $commissionInvoiceAttachment = \Swift_Attachment::fromPath($path);
        $commissionInvoiceAttachment->setContentType('application/pdf');
        $commissionInvoiceAttachment->setFilename('Orderspecificatie-' . $this->commissionInvoice->getNumber() . '.pdf');

        $html = $this->twigEngine->render('@App/mail/commission/invoice.html.twig', [
            'primaryColor' => '#b5251a',
            'brandUrl' => 'https://topgeschenken.nl',
            'site' => $this->entityManager->getRepository(Site::class)->findOneBy([
                'theme' => 'toptaarten',
            ]),
        ]);

        /** @var Swift_Mime_SimpleMessage $message */
        $message = (new Swift_Message())
            ->setSubject('Orderspecificatie ' . $this->commissionInvoice->getNumber())
            ->setFrom('administratie@topgeschenken.nl', 'Administratie Topgeschenken Nederland BV')
            ->setBody($html, 'text/html');

        $message->attach($commissionInvoiceAttachment);

        $company = $this->commissionInvoice->getCompany();
        $email = $company->getInvoiceEmail() ?: $company->getEmail();

        if (empty($email)) {
            throw new RuntimeException(sprintf("No mail address for company '%s'", $company->getName()));
        }

        $message->setTo($email, $company->getName());

        $mailerResult = (bool)$this->mailer->send($message);
        unlink($path);
        return $mailerResult;
    }
}
