<?php

namespace AppBundle\Services\Commission;

use AppBundle\Entity\Finance\CommissionBatch;
use AppBundle\Services\JobManager;
use Doctrine\ORM\EntityManagerInterface;
use JMS\JobQueueBundle\Entity\Job;

/**
 * Class BatchService
 * @package AppBundle\Services\Commission
 */
class BatchService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * BatchService constructor.
     * @param EntityManagerInterface $entityManager
     * @param JobManager             $jobManager
     */
    public function __construct(EntityManagerInterface $entityManager, JobManager $jobManager)
    {
        $this->entityManager = $entityManager;
        $this->jobManager = $jobManager;
    }

    /**
     * @var CommissionBatch
     */
    private $commissionBatch;

    /**
     * @param CommissionBatch $commissionBatch
     * @return $this
     */
    public function setCommissionBatch(CommissionBatch $commissionBatch)
    {
        $this->commissionBatch = $commissionBatch;

        return $this;
    }

    /**
     * @throws \Exception
     */
    public function send()
    {
        $this->entityManager->getConnection()->beginTransaction();

        foreach ($this->commissionBatch->getCommissionInvoices() as $commissionInvoice) {
            $job = new Job('commission-invoice:send', [
                $commissionInvoice->getId(),
            ]);

            $job->addRelatedEntity($commissionInvoice);

            $this->jobManager->addJob($job);
        }
        $this->entityManager->flush();
        $this->entityManager->getConnection()->commit();
    }
}
