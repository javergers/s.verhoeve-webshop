<?php

namespace AppBundle\Services\Commission\Batch;

use AppBundle\Entity\Finance\CommissionBatch;
use AppBundle\Entity\Finance\CommissionInvoice;
use AppBundle\Entity\Finance\CommissionInvoiceLine;
use AppBundle\Entity\Finance\CommissionInvoiceSupplierOrder;
use AppBundle\Entity\Finance\CommissionInvoiceSupplierOrderLine;
use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Entity\Finance\Tax\VatRate;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyCommission;
use AppBundle\Entity\Supplier\SupplierGroupCommission;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Entity\Supplier\SupplierOrderLine;
use AppBundle\Exceptions\ProductGroupPercentageNotSetException;
use AppBundle\Services\CommissionService;
use AppBundle\Services\Parameter\ParameterService;
use AppBundle\Services\Sales\Order\OrderActivityService;
use AppBundle\Utils\DisableFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class GeneratorService
 * @package AppBundle\Services\Commission\Batch
 */
class GeneratorService
{
    use ContainerAwareTrait;

    /**
     * @var ArrayCollection
     */
    private $commissionSuppliers;

    /**
     * @var CommissionBatch
     */
    private $batch;

    /**
     * @var VatRate
     */
    private $vatRate;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ParameterService
     */
    private $parameterService;
    /**
     * @var CommissionService
     */
    private $commissionService;

    /**
     * @var OrderActivityService
     */
    private $orderActivityService;

    /**
     * GeneratorService constructor.
     * @param EntityManagerInterface $entityManager
     * @param ParameterService       $parameterService
     * @param CommissionService      $commissionService
     * @param OrderActivityService   $orderActivityService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ParameterService $parameterService,
        CommissionService $commissionService,
        OrderActivityService $orderActivityService
    ) {
        $this->commissionSuppliers = new ArrayCollection();
        $this->entityManager = $entityManager;
        $this->parameterService = $parameterService;
        $this->commissionService = $commissionService;
        $this->orderActivityService = $orderActivityService;
    }

    /**
     * @param CommissionBatch $commissionBatch
     * @return CommissionBatch|null
     * @throws ProductGroupPercentageNotSetException
     * @throws \Exception
     */
    public function generate(CommissionBatch $commissionBatch)
    {
        $this->batch = $commissionBatch;

        $publishableFilter = DisableFilter::temporaryDisableFilter('publishable');
        $softdeletableFilter = DisableFilter::temporaryDisableFilter('softdeletable');

        $commissionableSupplierOrders = $this->getCommissionSupplierOrders();

        if ($commissionableSupplierOrders->count() === 0) {
            return null;
        }

        $vatGroupId = $this->parameterService->setEntity()->getValue('commission_fee_vatgroup');

        if (!$vatGroupId) {
            throw new \RuntimeException('Parameter "commission_fee_vatgroup" is not set!');
        }

        /** @var VatGroup $vatGroup */
        $vatGroup = $this->entityManager->getRepository(VatGroup::class)->find($vatGroupId);
        $this->vatRate = $vatGroup->getRateByDate($this->batch->getInvoiceDate());

        foreach ($this->batch->getSuppliers() as $supplier) {
            $commissionableOrders = $this->commissionService->getCommissionableSupplierOrders($this->batch->getInvoiceDate(),
                $supplier);

            foreach ($commissionableOrders as $commissionableOrder) {
                $this->commissionOrder($commissionableOrder);
            }

            /** @var CommissionInvoice $commissionInvoice */
            $commissionInvoice = $this->commissionSuppliers->get($supplier->getId());

            if (null === $commissionInvoice) {
                $this->addCommissionInvoiceLines($commissionInvoice);
            }

            $this->entityManager->flush();
        }

        $publishableFilter->reenableFilter();
        $softdeletableFilter->reenableFilter();

        return $this->batch;
    }

    /**
     * @return ArrayCollection|SupplierOrder[]
     * @throws ProductGroupPercentageNotSetException
     */
    private function getCommissionSupplierOrders()
    {
        $commissionableSupplierOrders = $this->commissionService->getCommissionableSupplierOrders($this->batch->getInvoiceDate());

        $commissionableOrders = [];

        foreach ($commissionableSupplierOrders as $commissionableSupplierOrder) {
            if ($this->batch->getSuppliers()->filter(function (Company $supplier) use ($commissionableSupplierOrder) {
                    $commissionableSupplier = $commissionableSupplierOrder->getSupplier();

                    if ($commissionableSupplier->getBillOnParent()) {
                        $commissionableSupplier = $commissionableSupplier->getParent();
                    }

                    return $supplier === $commissionableSupplier;
                })->count() === 0) {
                continue;
            }

            foreach ($commissionableSupplierOrder->getLines() as $line) {
                $this->getProductGroupPercentage($line);

            }

            $commissionableOrders[] = $commissionableSupplierOrder;
        }

        return new ArrayCollection($commissionableOrders);
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @return CommissionInvoiceSupplierOrder
     * @throws \RuntimeException
     * @throws ProductGroupPercentageNotSetException
     */
    private function commissionOrder(SupplierOrder $supplierOrder)
    {
        $commissionableSupplier = $supplierOrder->getSupplier();

        if ($commissionableSupplier->getBillOnParent()) {
            $commissionableSupplier = $commissionableSupplier->getParent();
        }

        $commissionInvoiceSupplierOrder = new CommissionInvoiceSupplierOrder();
        $commissionInvoiceSupplierOrder->setSupplierOrder($supplierOrder);

        $recommission = $supplierOrder->getRecommission();

        if ($supplierOrder->getCommissionInvoiceSupplierOrders()->isEmpty()) {
            $recommission = false;
        }

        if ($recommission) {
            $commissionInvoiceSupplierOrder->setRecommissioned(true);
        }

        foreach ($supplierOrder->getLines() as $supplierOrderLine) {
            if (null === $supplierOrderLine->getPrice()) {
                $format = 'Leveranciersorder #%d bevat één of meerdere regels zonder doorgeefprijs';

                throw new \RuntimeException(sprintf($format, $supplierOrder->getId()));
            }

            $orderLineTotalPrice = $supplierOrderLine->getTotalPrice();

            if ($recommission) {
                $orderLineTotalPrice -= $this->getPreviousCommissionedLineTotalPrice($supplierOrderLine);

                if ($orderLineTotalPrice === 0) {
                    continue;
                }
            }

            $commissionSupplierOrderLine = new CommissionInvoiceSupplierOrderLine();
            $commissionSupplierOrderLine->setSupplierOrderLine($supplierOrderLine);
            $commissionSupplierOrderLine->setPercentage($this->getProductGroupPercentage($supplierOrderLine));
            $commissionSupplierOrderLine->setVatRate($supplierOrderLine->getVatRate());
            $commissionSupplierOrderLine->setOrderLineTotalPrice($orderLineTotalPrice);

            $commissionInvoiceSupplierOrder->addCommissionSupplierOrderLine($commissionSupplierOrderLine);
        }

        if ($supplierOrder->getOrder()) {
            $this->orderActivityService->add('order_commissioned', $supplierOrder->getOrder());
        }

        if ($commissionInvoiceSupplierOrder->getCommissionInvoiceSupplierOrderLines()->isEmpty()) {
            return null;
        }

        /** @var CommissionInvoice $commissionInvoice */
        $commissionInvoice = $this->commissionSuppliers->get($commissionableSupplier->getId());

        if (null === $commissionInvoice) {
            $commissionInvoice = new CommissionInvoice();
            $commissionInvoice->setCompany($commissionableSupplier);
            $commissionInvoice->setCommissionBatch($this->batch);
            $commissionInvoice->setDate($this->batch->getInvoiceDate());

            $this->batch->addCommissionInvoice($commissionInvoice);

            $this->commissionSuppliers->set($commissionableSupplier->getId(), $commissionInvoice);
        }

        $commissionInvoice->addCommissionInvoiceSupplierOrder($commissionInvoiceSupplierOrder);

        return $commissionInvoiceSupplierOrder;
    }

    /**
     * @param CommissionInvoice $commissionInvoice
     */
    private function addCommissionInvoiceLines(CommissionInvoice $commissionInvoice)
    {
        $financialFee = 2;

        $totalOrderValue = 0;
        foreach ($commissionInvoice->getCommissionInvoiceSupplierOrders() as $commissionInvoiceSupplierOrder) {
            $totalOrderValue += $commissionInvoiceSupplierOrder->getSubtotalIncl();
        }

        $commissionInvoiceLine = new CommissionInvoiceLine();
        $commissionInvoiceLine->setVatRate($this->vatRate);
        $commissionInvoiceLine->setDescription(sprintf('Kosten %d%% financiële afhandeling orderbedrag',
            $financialFee));
        $commissionInvoiceLine->setPrice(0 - round($totalOrderValue / 100 * $financialFee, 2));

        $commissionInvoice->addLine($commissionInvoiceLine);
    }

    /**
     * @param SupplierOrderLine $supplierOrderLine
     * @return float
     */
    private function getPreviousCommissionedLineTotalPrice(SupplierOrderLine $supplierOrderLine)
    {
        $supplierOrder = $supplierOrderLine->getSupplierOrder();

        /** @var ArrayCollection $commissionedLines */
        $commissionedLines = new ArrayCollection();

        foreach ($supplierOrder->getCommissionInvoiceSupplierOrders() as $commissionInvoiceSupplierOrder) {
            $commissionInvoiceSupplierOrderLines = $commissionInvoiceSupplierOrder->getCommissionInvoiceSupplierOrderLines();

            $commissionInvoiceSupplierOrderLines = $commissionInvoiceSupplierOrderLines->filter(function (
                CommissionInvoiceSupplierOrderLine $commissionInvoiceSupplierOrderLine
            ) use ($supplierOrderLine) {
                return $commissionInvoiceSupplierOrderLine->getSupplierOrderLine() === $supplierOrderLine;
            });

            foreach ($commissionInvoiceSupplierOrderLines as $commissionInvoiceSupplierOrderLine) {
                $commissionedLines->add($commissionInvoiceSupplierOrderLine);
            }
        }

        $totalPreviousCommissionSupplierOrderLineTotal = 0;

        foreach ($commissionedLines as $commissionedLine) {
            $totalPreviousCommissionSupplierOrderLineTotal += round($commissionedLine->getOrderLineTotalPrice(), 3);
        }

        return round($totalPreviousCommissionSupplierOrderLineTotal, 3);
    }

    /**
     * @param SupplierOrderLine $supplierOrderLine
     * @return int
     * @throws ProductGroupPercentageNotSetException
     * @throws \Exception
     */
    private function getProductGroupPercentage(SupplierOrderLine $supplierOrderLine): int
    {
        $commissionableSupplier = $supplierOrderLine->getSupplierOrder()->getSupplier();

        if ($commissionableSupplier->getBillOnParent()) {
            $commissionableSupplier = $commissionableSupplier->getParent();
        }

        $mainProduct = $supplierOrderLine->getConcludedSupplierProduct()->getProduct();

        if ($mainProduct->getParent()) {
            $mainProduct = $mainProduct->getParent();
        }

        $supplierCommissions = $commissionableSupplier->getSupplierCommissions();

        $supplierCommissions = $supplierCommissions->filter(function (CompanyCommission $commission) use ($mainProduct
        ) {
            return null !== $mainProduct->getProductgroup() && $mainProduct->getProductgroup()->getId() === $commission->getProductGroup()->getId();
        });

        if (!$supplierCommissions->isEmpty()) {
            /** @var CompanyCommission $supplierCommission */
            $supplierCommission = $supplierCommissions->current();

            return $supplierCommission->getPercentage();
        }

        $supplierGroups = $commissionableSupplier->getSupplierGroups();

        foreach ($supplierGroups as $supplierGroup) {
            $supplierGroupCommissions = $supplierGroup->getSupplierCommissions();

            $supplierGroupCommissions = $supplierGroupCommissions->filter(function (SupplierGroupCommission $commission
            ) use ($mainProduct) {
                return null !== $mainProduct->getProductgroup() && $mainProduct->getProductgroup()->getId() === $commission->getProductGroup()->getId();
            });

            if (!$supplierGroupCommissions->isEmpty()) {
                $supplierGroupCommission = $supplierGroupCommissions->current();

                return $supplierGroupCommission->getPercentage();
            }
        }

        $productGroupCommission = new ProductGroupPercentageNotSetException();
        $productGroupCommission->setProductGroup($mainProduct->getProductgroup());
        $productGroupCommission->setSupplier($commissionableSupplier);

        throw $productGroupCommission;
    }
}
