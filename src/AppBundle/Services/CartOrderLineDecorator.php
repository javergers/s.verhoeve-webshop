<?php

namespace AppBundle\Services;

use AppBundle\Decorator\ProductDecorator;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Interfaces\CartOrderInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class CartOrderLineDecorator
 * @package AppBundle\Services
 */
class CartOrderLineDecorator implements CartOrderInterface
{
    use ContainerAwareTrait;

    public $autoFlush;

    /**
     * @var CartOrderLine
     */
    protected $cartOrderLine;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @param CartOrderLine $cartOrderLine
     * @return CartOrderLineDecorator
     */
    public function setCartOrderLine(CartOrderLine $cartOrderLine)
    {
        $this->cartOrderLine = $cartOrderLine;

        return $this;
    }

    /**
     * @param EntityManagerInterface $em
     */
    public function setEntityManager(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

//    public function setCartService(CartService $cartService)
//    {
//        $this->cartService = $cartService;
//    }

    /**
     * @return CartOrderLine
     */
    public function get()
    {
        return $this->cartOrderLine;
    }

    /**
     * @return Product|ProductDecorator
     */
    public function getProduct()
    {
        return $this->cartOrderLine->getProduct();
    }

    /**
     * @deprecated
     * @return string
     */
    public function getDisplayPrice()
    {
        return $this->cartOrderLine->getDisplayPrice();
    }

    /**
     * @deprecated
     * @return string
     */
    public function getTotalDisplayPrice()
    {
        return $this->cartOrderLine->getTotalDisplayPrice();
    }

    /**
     * @deprecated
     * @param $quantity integer
     * @throws \Exception
     */
    public function setQuantity($quantity)
    {
        $this->cartOrderLine->setQuantity($quantity);
    }

    /**
     * @param $name
     * @param $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        $result = call_user_func_array([$this->cartOrderLine, $name], $arguments);

        if (substr($name, 0, 3) == "set") {
            if ($this->autoFlush) {
                $this->em->flush();
            }
        }

        return $result;
    }

    /**
     *
     * @return ArrayCollection|CartOrderLine[]|CartOrderLineDecorator[]
     */
    public function getChildren()
    {
        return new ArrayCollection(array_map(function (CartOrderLine $cartOrderLine) {
            $cartOrderLineDecorator = $this->container->get("app.cart_order_line")->setCartOrderLine($cartOrderLine);
            $cartOrderLineDecorator->autoFlush = &$this->cartService->autoFlush;

            return $cartOrderLineDecorator;
        }, $this->get()->getChildren()->toArray()));
    }

    /**
     * @deprecated
     * Checks whether this line has personalization for the product
     *
     * @return boolean $hasProductPersonalization
     */
    public function hasProductPersonalization()
    {
        return $this->cartOrderLine->hasProductPersonalization();
    }
}
