<?php

namespace AppBundle\Services\Common;

use AppBundle\Entity\Relation\Company;
use AppBundle\Exceptions\Customer\BatchCreate\InvalidMimeTypeException;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use AppBundle\Exceptions\Common\SpreadsheetInvalidExtensionException;
use PhpOffice\PhpSpreadsheet\Exception as PhpSpreadsheetException;
use PhpOffice\PhpSpreadsheet\Reader\Exception as ReaderException;
use PhpOffice\PhpSpreadsheet\Reader\IReader;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use AppBundle\Exceptions\Common\SpreadsheetReaderException;

/**
 * Class SpreadsheetReaderService
 * @package AppBundle\Services
 */
class SpreadsheetReaderService
{
    /** @var Spreadsheet */
    private $excel;

    /** @var Worksheet */
    private $spreadsheet;

    /** @var array */
    private $highestRowAndColumn;

    /** @var array */
    private $headers;

    /** @var array */
    private $data;

    /** @var array */
    private $resolver;

    public const INVALID_EXTENSION = 'Ongeldige bestandsextensie';

    /**
     * @param array $resolver
     *
     * @return SpreadsheetReaderService
     */
    public function setResolver(array $resolver): SpreadsheetReaderService
    {
        $this->resolver = $resolver;

        return $this;
    }

    /**
     * @param UploadedFile $file
     *
     * @return SpreadsheetReaderService
     *
     * @throws InvalidMimeTypeException
     * @throws PhpSpreadsheetException
     * @throws ReaderException
     * @throws SpreadsheetInvalidExtensionException
     */
    public function readFile(UploadedFile $file): SpreadsheetReaderService
    {
        $this->validateExtension($file);
        $this->validateMimeType($file);

        $pathname = $file->getPathname();

        $inputFileType = IOFactory::identify($pathname);

        /** @var IReader $reader */
        $reader = IOFactory::createReader($inputFileType);

        $reader->setReadDataOnly(true);

        $this->excel = $reader->load($pathname);

        $this->init();

        return $this;
    }


    /**
     * init
     *
     * @throws Exception
     *
     * @return SpreadsheetReaderService
     */
    private function init(): SpreadsheetReaderService
    {
        $this->spreadsheet = $this->excel->getActiveSheet();
        $this->highestRowAndColumn = $this->spreadsheet->getHighestRowAndColumn();

        return $this;
    }

    /**
     * @param UploadedFile $file
     *
     * @return SpreadsheetReaderService
     *
     * @throws SpreadsheetInvalidExtensionException
     */
    private function validateExtension(UploadedFile $file)
    {
        $extension = $this->getExtension($file);
        $allowedExtensions = ['xls', 'xlsx'];

        if (!\in_array($extension, $allowedExtensions, true)) {
            throw new SpreadsheetInvalidExtensionException(self::INVALID_EXTENSION);
        }

        return $this;
    }

    /**
     * @param UploadedFile $file
     * @throws ReaderException
     * @throws InvalidMimeTypeException
     */
    private function validateMimeType(UploadedFile $file)
    {
        $pathname = $file->getPathname();

        $inputFileType = IOFactory::identify($pathname);

        $extension = $this->getExtension($file);

        if(
            (strtolower($inputFileType) !== 'xls' && strtolower($extension) === 'xls')
            || (strtolower($inputFileType) !== 'xlsx' && strtolower($extension) === 'xlsx')
        ) {
            throw new InvalidMimeTypeException('Het geuploade bestand is geen geldig excel document.');
        }

    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    private function getExtension(UploadedFile $file): string
    {
        $filenameParts = explode('.', $file->getClientOriginalName());

        return array_pop($filenameParts);
    }

    /**
     * Set active sheet index
     *
     * @param int $index
     *
     * @return void
     *
     * @throws Exception
     */
    public function setActiveSheet($index = 0): void
    {
        $this->clear(true);
        $this->excel = $this->excel->setActiveSheetIndex($index);

        $this->init();
    }

    /**
     * @param callable|null $mapping
     *
     * @return array
     *
     * @throws SpreadsheetReaderException
     * @throws Exception
     */
    public function getHeaders(callable $mapping = null): array
    {
        if (null === $this->highestRowAndColumn || null === $this->spreadsheet) {
            throw new SpreadsheetReaderException('missing required info');
        }

        $useMapping = null !== $mapping && \is_callable($mapping);
        $headers = [];
        foreach (range('A', $this->highestRowAndColumn['column']) as $column) {
            if (null !== ($col = $this->spreadsheet->getCell($column . '1'))
                && $value = $col->getValue()) {
                if ($useMapping) {
                    if ($value = $mapping($value)) {
                        $headers[$column] = $value;
                    }
                } else {
                    $headers[$column] = $value;
                }
            } else {
                break;
            }
        }

        return ($this->headers = $headers);
    }

    /**
     * @param Company $company
     * @return array
     *
     * @throws PhpSpreadsheetException
     * @throws SpreadsheetReaderException
     * @throws \Exception
     */
    public function getData(Company $company): array
    {
        if (null === $this->highestRowAndColumn || null === $this->spreadsheet || null === $this->headers) {
            throw new SpreadsheetReaderException('missing required info');
        }

        $totalHeaders = \count($this->headers);

        $data = [];
        for ($i = 2; $i <= $this->highestRowAndColumn['row']; $i++) {
            $row = [];
            $totalColumns = 0;
            foreach ($this->headers as $headerColumn => $headerName) {
                if (null !== ($col = $this->spreadsheet->getCell($headerColumn . $i))
                    && $value = $col->getValue()) {
                    $row[$headerName] = $value;
                } else {
                    $row[$headerName] = null;
                }

                if ($row[$headerName] === null || empty($row[$headerName])) {
                    $totalColumns++;
                }
            }

            if ($totalHeaders === $totalColumns) {
                break;
            }

            $data[] = $this->rowResolver($row, $i, $company);
        }

        return $data;
    }

    /**
     * @param array        $row
     * @param int|null     $rowNumber
     * @param Company|null $company
     *
     * @return array
     * @throws \Exception
     */
    public function rowResolver(array $row = [], int $rowNumber = null, Company $company = null): array
    {
        if (\is_callable($this->resolver)) {
            return \call_user_func($this->resolver, $row, $rowNumber, $company);
        }

        return $row;
    }

    /**
     * Reset Excel Reader properties
     *
     * @param bool $onlyData
     *
     * @return SpreadsheetReaderService
     */
    public function clear($onlyData = false): SpreadsheetReaderService
    {
        $this->headers = null;
        $this->data = null;

        if ($onlyData) {
            $this->excel = null;
        }

        return $this;
    }
}
