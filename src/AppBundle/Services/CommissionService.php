<?php

namespace AppBundle\Services;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Supplier\SupplierOrder;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class CommissionService
 * @package AppBundle\Services
 */
class CommissionService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CommissionService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param \Datetime $invoiceDate
     *
     * @return ArrayCollection|Company[]
     */
    public function getCommissionableSuppliers(\DateTime $invoiceDate)
    {
        $suppliers = new ArrayCollection();

        foreach ($this->getCommissionableSupplierOrders($invoiceDate) as $supplierOrder) {
            $commissionableSupplier = $supplierOrder->getSupplier();

            if ($commissionableSupplier->getBillOnParent()) {
                $commissionableSupplier = $commissionableSupplier->getParent();
            }

            if ($suppliers->get($commissionableSupplier->getId())) {
                continue;
            }

            $suppliers->set($commissionableSupplier->getId(), $commissionableSupplier);

            continue;
        }

        $suppliers = $suppliers->toArray();

        usort($suppliers, function (Company $supplier1, Company $supplier2) {
            return $supplier1->getName() <=> $supplier2->getName();
        });

        return new ArrayCollection($suppliers);
    }

    /**
     * @param \DateTime $invoiceDate
     *
     * @return ArrayCollection|SupplierOrder[]
     */
    public function getCommissionableSupplierOrders(\DateTime $invoiceDate, Company $supplier = null)
    {
        $deliveryDate = clone $invoiceDate;
        $deliveryDate->setTime(23, 59, 59);

        $excludedStatuses = ['pending', 'failed', 'cancelled'];

        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->entityManager->getRepository(SupplierOrder::class);

        $qb = $entityRepository->createQueryBuilder('supplier_order');
        $qb->select('supplier_order');

        $qb->leftJoin('supplier_order.order', 'o');

        $qb->andWhere('supplier_order.commissionable = true');
        $qb->andWhere('supplier_order.supplier IS NOT NULL');
        $qb->andWhere('supplier_order.status NOT IN(:excludedStatuses)');
        $qb->setParameter('excludedStatuses', $excludedStatuses);
        $qb->andWhere('o.deliveryDate <= :deliveryDate OR o.deliveryDate IS NULL');
        $qb->setParameter('deliveryDate', $deliveryDate);

        $qb->andWhere('o.status IN(:statuses)')
            ->setParameter('statuses', ['archived', 'complete', 'processed', 'sent']);

        $qb->join('o.orderCollection', 'order_collection');

        $qb->join('supplier_order.supplier', 'supplier');
        $qb->addSelect('supplier');

        if($supplier !== null) {
            $qb->andWhere('supplier_order.supplier = :supplier')
                ->setParameter('supplier', $supplier->getId());
        }

        $qb->leftJoin('supplier_order.commissionInvoiceSupplierOrders', 'commission_invoice_supplier_order');
        $qb->andWhere('(commission_invoice_supplier_order.id IS NULL OR supplier_order.recommission = 1)');

        $qb->andWhere('order_collection.deletedAt IS NULL');
        $qb->andWhere('o.deletedAt IS NULL');

        return new ArrayCollection($qb->getQuery()->getResult());
    }
}
