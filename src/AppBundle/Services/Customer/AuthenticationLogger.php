<?php


namespace AppBundle\Services\Customer;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Customer\AuthenticationLog;
use AppBundle\Entity\Security\Employee\User;
use Doctrine\ORM\EntityManagerInterface;
use Recurr\Exception;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\HttpFoundation\RequestStack;

class AuthenticationLogger
{
    const REASON_USERNAME_UNKNOWN = 1;
    const REASON_INVALID_CREDENTIALS = 2;
    const REASON_DISABLED = 3;
    const REASON_LOCKED = 4;
    const REASON_EXPIRED = 5;
    const REASON_IP_BLOCKED = 6;
    const REASON_TOO_MANY_ATTEMPTS = 7;
    const REASON_UNKNOWN = 255;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * AuthenticationSuccessHandler constructor.
     *
     * @param RequestStack  $requestStack
     * @param EntityManagerInterface $entityManager
     * @param Logger        $logger
     */
    public function __construct(RequestStack $requestStack, EntityManagerInterface $entityManager, Logger $logger)
    {
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    /**
     * @param               $type
     * @param string|null   $username
     * @param Customer|null $customer
     * @param string|null   $ip
     * @param string|null   $userAgent
     * @param User|null     $employee
     * @return bool
     */

    public function success(
        $type,
        $username = null,
        Customer $customer,
        $ip = null,
        $userAgent = null,
        User $employee = null
    ) {
        $this->log($type, 'success', null, $username, $customer, $ip, $userAgent, $employee);

        return false;
    }

    /**
     * @param string|null $username
     * @param integer     $reason
     * @param string|null $ip
     * @param string|null $userAgent
     */
    public function failure($username = null, $reason, $ip = null, $userAgent = null): void
    {
        $this->log('login', 'failure', $reason, $username, null, $ip, $userAgent);
    }

    /**
     * @param string        $type
     * @param string        $event
     * @param integer|null  $reason
     * @param string|null   $username
     * @param Customer|null $customer
     * @param string|null   $ip
     * @param string|null   $userAgent
     * @param User|null     $employee
     *
     * @return bool
     */
    private function log(
        $type,
        $event,
        $reason,
        $username = null,
        $customer = null,
        $ip = null,
        $userAgent = null,
        $employee = null
    ) {
        try {
            if (is_null($ip) && $this->requestStack->getMasterRequest()->getClientIp()) {
                $ip = $this->requestStack->getMasterRequest()->getClientIp();
            }

            if (is_null($userAgent) && $this->requestStack->getMasterRequest()->headers->has('User-Agent')) {
                $userAgent = $this->requestStack->getMasterRequest()->headers->get('User-Agent');
            }

            $authenticationLog = new AuthenticationLog();
            $authenticationLog->setType($type)
                ->setCustomer($customer)
                ->setEvent($event)
                ->setReason($reason)
                ->setUsername($username)
                ->setEmployee($employee)
                ->setCreatedAt(new \DateTime())
                ->setIp($ip)
                ->setUserAgent($userAgent);

            if ($type == 'login' && $event == 'success') {
                $customer->setLastLogin(new \DateTime());
            }


            $this->entityManager->persist($authenticationLog);
            $this->entityManager->flush();

            // Implement monolog messages
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param $reason
     * @return null|string
     */
    public static function getReasonDescription($reason)
    {
        $description = null;

        switch ($reason) {
            case self::REASON_USERNAME_UNKNOWN:
                $description = 'Gebruikersnaam onbekend';

                break;
            case self::REASON_INVALID_CREDENTIALS:
                $description = 'Ongeldige combinatie gebruikersnaam/wachtwoord';

                break;
            case self::REASON_DISABLED:
                $description = 'Account uitgeschakeld';

                break;
            case self::REASON_LOCKED:
                $description = 'Account geblokkeerd';

                break;
            case self::REASON_EXPIRED:
                $description = 'Account verlopen';

                break;
            case self::REASON_IP_BLOCKED:
                $description = 'IP-adres geblokkeerd';

                break;
            case self::REASON_TOO_MANY_ATTEMPTS:
                $description = 'Teveel pogingen';

                break;
            case self::REASON_UNKNOWN:
            default:
                $description = 'Onbekende reden';

                break;
        }

        return $description;
    }
}
