<?php

namespace AppBundle\Services\Payment;

use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\PaymentStatus;
use AppBundle\Services\JobManager;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PaymentRequestService
 * @package AppBundle\Services\Payment
 */
class PaymentRequestService
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var JobManager
     */
    protected $jobManager;

    /**
     * PaymentRequestService constructor.
     * @param EntityManagerInterface $entityManager
     * @param JobManager    $jobManager
     */
    public function __construct(EntityManagerInterface $entityManager, JobManager $jobManager)
    {
        $this->entityManager = $entityManager;
        $this->jobManager = $jobManager;
    }

    /**
     * @param array $parameters
     *
     * @return Payment
     * @throws Exception
     */
    public function create(array $parameters): Payment
    {
        $payment = $this->createPayment($parameters);

        $job = new Job('app:payment:send-request', [$payment->getId()]);
        $this->jobManager->addJob($job);
        $this->entityManager->flush();

        return $payment;
    }

    /**
     * @param array $parameters
     *
     * @return Payment
     * @throws Exception
     */
    public function createPayment(array $parameters): Payment
    {
        $parameters = $this->resolvePaymentRequestParameters($parameters);

        $paymentStatus = $this->entityManager->getRepository(PaymentStatus::class)->find('pending');

        $payment = new Payment();
        $payment->setAmount($parameters['amount']);
        $payment->setOrderCollection($parameters['orderCollection']);
        $payment->setStatus($paymentStatus);
        $payment->setMetadata([
            'request' => [
                'email' => $parameters['email'],
                'name' => $parameters['name'],
                'methods' => $parameters['methods'],
                'description' => $parameters['description'],
            ],
        ]);
        $payment->setDatetime(new DateTime());

        $this->entityManager->persist($payment);
        $this->entityManager->flush();

        return $payment;
    }

    /**
     * @param array $parameters
     * @return array
     */
    private function resolvePaymentRequestParameters(array $parameters): array
    {
        $resolver = new OptionsResolver();
        $resolver->setRequired([
            'orderCollection',
            'amount',
            'name',
            'email',
            'description',
            'methods',
        ]);

        return $resolver->resolve($parameters);
    }
}
