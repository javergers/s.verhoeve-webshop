<?php

namespace AppBundle\Services\Payment;

use Adyen\AdyenException;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Manager\Order\OrderCollectionManager;
use AppBundle\Services\JobManager;
use AppBundle\ThirdParty\Adyen\Entity\AdyenNotification;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderActivity;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Entity\Payment\PaymentStatus;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class AdyenNotificationService
 * @package AppBundle\Services\Payment
 */
class AdyenNotificationService
{
    use ContainerAwareTrait;

    /**
     * @param $data
     * @return AdyenNotification
     */
    public function save($data)
    {
        $notification = new AdyenNotification();
        $notification->setDatetime(new \DateTime());
        $notification->setData((array)$data);
        $notification->setOrder($this->findOrderCollection($notification));

        $this->container->get("doctrine")->getManager()->persist($notification);
        $this->container->get("doctrine")->getManager()->flush();

        $job = new Job("adyen:notification", [
            $notification->getId(),
            "--process",
        ]);

        $job->addRelatedEntity($notification);

        $this->container->get("job.manager")->addJob($job);

        $this->container->get("doctrine")->getManager()->flush();

        return $notification;
    }

    /**
     * @param AdyenNotification $notification
     * @throws AdyenException
     */
    public function process(AdyenNotification $notification)
    {
        switch ($notification->getEventCode()) {
            case "AUTHORISATION":
                $this->handleAuthorization($notification);
                break;
            case "CAPTURE":
                $this->handleCapture($notification);
                break;
            case "CANCELLATION":
                $this->handleCancellation($notification);
                break;
            case "REFUND":
                $this->handleRefund($notification);
                break;
            case "CHARGEBACK":
                $this->handleChargeback($notification);
                break;
            default:
                throw new AdyenException(sprintf("Unprocessable event code '%s'", $notification->getEventCode()));
        }

        $notification->setProcessed(true);

        $this->getDoctrine()->getManager()->flush();
    }

    /**
     * @param AdyenNotification $notification
     * @throws \Exception
     */
    private function handleAuthorization(AdyenNotification $notification)
    {
        $payment = $this->findPayment($notification);

        if (!$payment || !$notification->getOrder()) {
            return;
        }

        if ($notification->getSuccess() == "true") {
            $payment->setStatus($this->getDoctrine()->getRepository(PaymentStatus::class)->find("authorized"));

            if ($this->container->get(OrderCollectionManager::class)->calculateOutstanding($notification->getOrder()) == 0) {
                foreach ($notification->getOrder()->getOrders() as $order) {
                    if ($order->getStatus() === 'payment_pending') {// @todo: workflow guard refactor
                        $order->setStatus('new');// @todo: workflow refactor
                    }
                }

                $this->getDoctrine()->getManager()->flush();
            }
        } else {
            $payment->setStatus($this->getDoctrine()->getRepository(PaymentStatus::class)->find("failed"));
        }
    }

    /**
     * @param AdyenNotification $notification
     */
    private function handleCapture(AdyenNotification $notification)
    {
        $originalPayment = $this->findPaymentByReference($notification->getOriginalReference());

        if (!$originalPayment) {
            return;
        }

        $originalPayment->setStatus($this->getDoctrine()->getRepository(PaymentStatus::class)->find("captured"));
    }

    /**
     * @param AdyenNotification $notification
     */
    private function handleCancellation(AdyenNotification $notification)
    {
        $payment = $this->findPayment($notification);

        if (!$payment) {
            return;
        }

        $payment->setStatus($this->getDoctrine()->getRepository(PaymentStatus::class)->find("cancelled"));
    }

    /**
     * @param AdyenNotification $notification
     */
    private function handleRefund(AdyenNotification $notification)
    {
        $originalPayment = $this->findPaymentByReference($notification->getOriginalReference());

        if (!$originalPayment) {
            return;
        }

        $payment = $this->findPaymentByReference($notification->getPspReference());

        // Already processed
        if ($payment) {
            return;
        }

        $payment = new Payment();
        $payment->setRelated($originalPayment);
        $payment->setOrder($originalPayment->getOrderCollection());
        $payment->setstatus($this->getDoctrine()->getRepository(PaymentStatus::class)->find("refunded"));
        $payment->setAmount(0 - ($notification->getAmount()['value'] / 100));
        $payment->setReference($notification->getPspReference());
        $payment->setDatetime(new \DateTime());

        $this->getDoctrine()->getManager()->persist($payment);

        $orderActivity = new OrderActivity();
        $orderActivity->setOrderCollection($originalPayment->getOrderCollection());
        $orderActivity->setActivity("payment_refund");
        $orderActivity->setDatetime(new \DateTime());

        $this->getDoctrine()->getManager()->persist($orderActivity);
    }

    /**
     * @param AdyenNotification $notification
     */
    private function handleChargeback(AdyenNotification $notification)
    {
        if (!$notification->getOrder()) {
            return;
        }

        preg_match("/[0-9a-z]*$/", $notification->getOriginalReference(), $result);

        $originalReference = $result[0];

        $relatedPayment = $this->getDoctrine()->getRepository(Payment::class)->findOneBy([
            "reference" => $originalReference,
        ]);

        $payment = new Payment();
        $payment->setRelated($relatedPayment);
        $payment->setOrder($notification->getOrder());
        $payment->setstatus($this->getDoctrine()->getRepository(PaymentStatus::class)->find("reversed"));
        $payment->setAmount(0 - ($notification->getAmount()['value'] / 100));
        $payment->setReference($notification->getPspReference());
        $payment->setDatetime(new \DateTime());
        $payment->setMetadata([
            "reason" => $notification->getReason(),
        ]);

        $this->getDoctrine()->getManager()->persist($payment);
    }

    /**
     * @param AdyenNotification $notification
     * @return Order|null
     */
    private function findOrderCollection(AdyenNotification $notification)
    {
        if (!preg_match("/[0-9]{8}/", $notification->getMerchantReference(), $match)) {
            return null;
        }

        return $this->getDoctrine()->getRepository(OrderCollection::class)->findOneBy([
            "number" => $match[0],
        ]);
    }

    /**
     * @param AdyenNotification $notification
     * @return Payment|null
     */
    private function findPayment(AdyenNotification $notification)
    {
        if (!empty($notification->getOriginalReference())) {
            $payment = $this->findPaymentByReference($notification->getOriginalReference());
        } else {
            $payment = $this->findPaymentByReference($notification->getPspReference());
        }

        if ($payment) {
            return $payment;
        }

        $orderCollection = $this->findOrderCollection($notification);

        if (!$orderCollection) {
            return null;
        }

        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->getDoctrine()->getManager()->getRepository(Payment::class);

        $qb = $entityRepository->createQueryBuilder("p")
            ->leftJoin("p.orderCollection", "o")
            ->andWhere("p.reference IS NULL")
            ->andWhere("p.amount = :amount")
            ->andWhere("o.id = :orderId")
            ->addOrderBy("p.datetime", "DESC")
            ->setParameter("amount", ($notification->getAmount()['value'] / 100))
            ->setParameter("orderId", $orderCollection->getId());

        $results = new ArrayCollection($qb->getQuery()->getResult());

        $payment = $results->current();

        if ($payment) {
            $payment->setReference($notification->getPspReference());

            $this->getDoctrine()->getManager()->flush();

            return $payment;
        }

        return null;
    }

    /**
     * @param $reference
     * @return null|Payment
     */
    private function findPaymentByReference($reference)
    {
        $payment = $this->getDoctrine()->getRepository(Payment::class)->findOneBy([
            "reference" => $reference,
        ]);

        return $payment;
    }

    /**
     * @return Registry
     */
    protected function getDoctrine()
    {
        return $this->container->get('doctrine');
    }
}
