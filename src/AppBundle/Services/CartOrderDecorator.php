<?php

namespace AppBundle\Services;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Exceptions\Sales\CartInvalidSupplierCombinationException;
use AppBundle\Interfaces\CartOrderInterface;
use AppBundle\Manager\Order\CartOrderManager;
use AppBundle\Utils\DeliveryDates;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class CartOrderDecorator
 * @package AppBundle\Services
 */
class CartOrderDecorator implements CartOrderInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var CartOrder
     */
    protected $cartOrder;

    /**
     * @var CartService
     */
    protected $cartService;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @param CartOrder $cartOrder
     *
     * @return $this
     */
    public function setCartOrder(CartOrder $cartOrder)
    {
        $this->cartOrder = $cartOrder;

        return $this;
    }

    /**
     * @param EntityManagerInterface $em
     */
    public function setEntityManager(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param CartService $cartService
     */
    public function setCartService(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    /**
     * @return CartOrder
     */
    public function get()
    {
        return $this->cartOrder;
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return mixed
     * @throws OptimisticLockException
     */
    public function __call($name, $arguments)
    {
        $result = \call_user_func_array([$this->cartOrder, $name], $arguments);

        if ($this->cartService->autoFlush && 0 === strpos($name, 'set')) {
            $this->em->flush($this->cartOrder);
        }

        return $result;
    }

    /**
     * @deprecated
     *
     * @param Product            $product
     * @param int                $quantity
     * @param CartOrderLine|null $parentCartOrderLine
     * @param bool               $skipSupplierCheck
     *
     * @return void
     * @throws CartInvalidSupplierCombinationException
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function addProduct(
        Product $product,
        $quantity = 1,
        CartOrderLine $parentCartOrderLine = null,
        $skipSupplierCheck = false
    ) {
        $this->cartOrder->addProduct($product, $quantity, $parentCartOrderLine, $skipSupplierCheck);
    }

    /**
     * @param CartOrderLine $decoratedCartOrderLine
     * @throws \Exception
     */
    public function removeLine(CartOrderLine $decoratedCartOrderLine)
    {
        $decoratedCartOrderLine->setQuantity(0);
    }

    /**
     * @throws \RuntimeException
     * @deprecated
     */
    public function getProduct()
    {
        throw new \RuntimeException('Due to issue #360 this functionality is no longer available');
    }

    /**
     * @param CartOrderLine $cartOrderLine
     *
     * @return CartOrderLineDecorator|null
     */
    public function getLine(CartOrderLine $cartOrderLine)
    {
        $result = $this->cartOrder->getLines()->filter(function (CartOrderLine $cartOrderLine1) use ($cartOrderLine
        ) {
            return ($cartOrderLine === $cartOrderLine1);
        })->current();

        if ($result) {
            $cartOrderLineDecorator = $this->container->get('app.cart_order_line')->setCartOrderLine($result);
            $cartOrderLineDecorator->autoFlush = &$this->cartService->autoFlush;

            return $cartOrderLineDecorator;
        }

        return null;
    }

    /**
     * @return ArrayCollection|CartOrderLine[]|CartOrderLineDecorator[]
     */
    public function getLines()
    {
        return new ArrayCollection(array_map(function (CartOrderLine $cartOrderLine) {
            $cartOrderLineDecorator = $this->container->get('app.cart_order_line')->setCartOrderLine($cartOrderLine);
            $cartOrderLineDecorator->autoFlush = &$this->cartService->autoFlush;

            return $cartOrderLineDecorator;
        }, $this->cartOrder->getLines()->toArray()));
    }

    /**
     * @deprecated
     *
     * @param Product $product
     *
     * @return ArrayCollection|CartOrderLine[]|CartOrderLineDecorator[]
     */
    public function getLinesContainingProduct(Product $product)
    {
        return $this->cartOrder->getLinesContainingProduct($product);
    }

    /**
     * @deprecated
     *
     * @param Productgroup $productgroup
     *
     * @return ArrayCollection|CartOrderLine[]|CartOrderLineDecorator[]
     */
    public function getLinesContainingProductgroup(Productgroup $productgroup)
    {
        return $this->cartOrder->getLinesContainingProductgroup($productgroup);
    }

    /**
     * @deprecated
     * @return string
     */
    public function getTotalDisplayPrice()
    {
        return $this->cartOrder->getTotalDisplayPrice();
    }

    /**
     * @deprecated
     * @return ArrayCollection
     */
    public function getAvailableCountries()
    {
        return $this->cartOrder->getAvailableCountries();
    }

    /**
     * @deprecated
     * @todo move internal logic to seperate service
     * @return DeliveryDates
     * @throws \Exception
     */
    public function getDeliveryDates()
    {
        return $this->cartOrder->getDeliveryDates();
    }

    /**
     * @deprecated
     * @return bool
     */
    public function isPickupPossible()
    {
        //todo SF4
        return $this->container->get(CartOrderManager::class)->isPickupPossible($this->cartOrder);
    }

    /**
     * @deprecated
     *
     * @param Product $product
     * @param int     $quantity
     *
     * @return CartOrderLine
     * @throws CartInvalidSupplierCombinationException
     */
    public function replaceContentsWithProduct(Product $product, int $quantity)
    {
        return $this->cartOrder->replaceContentsWithProduct($product, $quantity);
    }

    /**
     * @deprecated
     * Method that removes all CartOrderLines
     */
    public function removeAllLines()
    {
        $this->cartOrder->removeAllLines();
    }
}
