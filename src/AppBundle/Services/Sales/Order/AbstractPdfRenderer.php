<?php

namespace AppBundle\Services\Sales\Order;

use AppBundle\Utils\Browsershot\Browsershot;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

/**
 * Class AbstractPdfRenderer
 * @package AppBundle\Services\Sales\Order
 */
abstract class AbstractPdfRenderer implements PdfRendererInterface
{
    /**
     * @var string
     */
    protected $paperSize = 'A4';

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var Environment
     */
    protected $twig;

    /** @var string */
    protected $output;

    /** @var string */
    protected $html;

    /** @var string */
    protected $action;

    /** @var array */
    protected $parameters = [];

    /** @var array */
    protected $outputOptions = [];

    /**
     * AbstractPdfRenderer constructor.
     * @param RequestStack $requestStack
     * @param Environment  $twig
     */
    public function __construct(RequestStack $requestStack, Environment $twig)
    {
        $this->requestStack = $requestStack;
        $this->twig = $twig;
    }

    /**
     * Method to set the action needed for the Content Disposition determination
     *
     * @param string $action
     * @return $this
     */
    public function setAction($action = 'inline')
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Set parameters needed in the render method
     *
     * @param array $parameters
     *
     * @return $this
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * Set options for PDF output
     *
     * @param $options
     *
     * @return $this
     */
    public function setOutputOptions($options)
    {
        $this->outputOptions = $options;

        return $this;
    }

    /**
     * @param string $paperSize
     * @return $this
     */
    public function setPaperSize(string $paperSize): AbstractPdfRenderer
    {
        if(!in_array($paperSize, ['A5', 'A4'])) {
            throw new \RuntimeException('Only "A5" and "A4" are supported for the packingslip');
        }

        $this->paperSize = $paperSize;

        return $this;
    }

    /**
     *
     * @return Response
     */
    public function getResponse()
    {
        $this->generateHtml();

        $headers = [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => $this->getContentDisposition() . '; filename="' . $this->getFilename() . '"',
        ];

        $request = $this->getRequest();

        if ($request->query->has('output') && $request->query->get('output') === 'html') {
            $headers = [
                'Content-Type' => 'text/html',
            ];

            $this->output = $this->html;
        } else {
            $this->generateOutput();
        }

        return new Response($this->output, 200, $headers);
    }

    /**
     * @return string
     */
    public function getContents()
    {
        $this->generateHtml();
        $this->generateOutput();

        return $this->output;
    }

    /**
     * @return Request
     */
    private function getRequest()
    {
        return $this->requestStack->getCurrentRequest();
    }

    /**
     * Generate output and store in $output
     */
    private function generateOutput(): void
    {
        $pdf = Browsershot::html($this->html);

        $pdf->setOption('format', $this->paperSize);
        $pdf->setOption('printBackground', true);

        foreach ($this->outputOptions as $option => $optionValue) {
            $pdf->setOption($option, $optionValue);
        }

        if($this->paperSize === 'A5') {
            $pdf->setOption('landscape', false);
        }

        $this->output = $pdf->pdf();
    }

    /**
     * Returns the Content Disposition for the passed $action
     *
     * @param string|null $action
     *
     * @return string
     */
    private function getContentDisposition($action = null)
    {
        if (!$action) {
            $action = $this->action;
        }

        $contentDisposition = 'inline';

        if ($action === 'download') {
            $contentDisposition = 'attachment';
        }

        return $contentDisposition;
    }
}
