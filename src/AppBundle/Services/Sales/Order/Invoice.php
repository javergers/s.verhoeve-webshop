<?php

namespace AppBundle\Services\Sales\Order;

use function abs;
use AppBundle\Entity\Discount\Voucher;
use AppBundle\Entity\Order\BillingItem;
use AppBundle\Entity\Order\BillingItemGroup;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Interfaces\Order\LineInterface;
use AppBundle\Manager\Order\OrderLineManager;
use AppBundle\Services\PriceService;
use DateTime;
use Exception;
use RuntimeException;
use stdClass;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Environment;
use Twig_Error;

/**
 * Class Invoice
 * @package AppBundle\Services\Sales\Order
 */
class Invoice extends AbstractPdfRenderer
{
    /** @var string */
    private $kernelRootDir;

    /**
     * @var PriceService
     */
    protected $priceService;

    /**
     * @var OrderLineManager
     */
    protected $orderLineManager;

    /**
     * Invoice constructor.
     *
     * @param RequestStack $requestStack
     * @param Environment $twig
     * @param PriceService $priceService
     * @param OrderLineManager $orderLineManager
     * @param string $kernelRootDir
     */
    public function __construct(
        RequestStack $requestStack,
        Environment $twig,
        PriceService $priceService,
        OrderLineManager $orderLineManager,
        string $kernelRootDir
    ) {
        parent::__construct($requestStack, $twig);

        $this->kernelRootDir = $kernelRootDir;
        $this->priceService = $priceService;
        $this->orderLineManager = $orderLineManager;

        $this->outputOptions = [
            'landscape' => false,
            'margin' => [
                'top' => '30mm',
                'right' => '10mm',
                'bottom' => '10mm',
                'left' => '10mm',
            ],
            'printBackground' => true,
            'displayHeaderFooter' => true,
        ];
    }

    /**
     * @var OrderCollection
     */
    protected $orderCollection;

    /**
     * @return string
     * @throws Exception
     * @throws Twig_Error
     */
    public function generateHtml()
    {
        $pdfData = [
            'invoices' => $this->getData($this->orderCollection),
        ];

        $this->outputOptions['headerTemplate'] = $this->twig->render('AppBundle:pdf\order:invoice-header.html.twig',
            $pdfData);
        $this->outputOptions['footerTemplate'] = $this->twig->render('AppBundle:pdf\order:invoice-footer.html.twig',
            $pdfData);

        $this->html = $this->twig->render('AppBundle:pdf\order:invoice.html.twig', $pdfData);

        return $this;
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @return $this
     */
    public function setItem(OrderCollection $orderCollection)
    {
        $this->orderCollection = $orderCollection;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return 'Factuur_' . $this->orderCollection->getNumber() . '.pdf';
    }

    private function getTransactionTypeValue(float $value, bool $debit = true): float
    {
        return $debit ? $value : ($value * -1);
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @return array
     * @throws Exception
     */
    private function getData(OrderCollection $orderCollection)
    {
        $billingItemGroups = $this->orderCollection->getBillingItemGroups();

        $invoices = [];

        /** @var BillingItemGroup $billingItemGroup */
        foreach ($billingItemGroups as $billingItemGroup) {
            if ($orderCollection !== $billingItemGroup->getOrderCollection()) {
                throw new RuntimeException('Mismatch between order collection and billing item group.');
            }

            if ($orderCollection->hasPaymentMethod('invoice')) {
                throw new RuntimeException('Rendering a invoice for orders paid with invoice isn\'t allowed.');
            }

            $data = new stdClass();
            $data->orderCollection = $orderCollection;

            $logoPath = $this->kernelRootDir . '/../web/bundles/admin/img/theme/' . $orderCollection->getSite()->getTheme() . '/logo.svg';

            if (file_exists($logoPath)) {
                $data->logoPath = $logoPath;
                $data->logoData = 'data:image/svg+xml;charset=utf-8;base64,' . base64_encode(file_get_contents($logoPath));
            }

            $data->theme = $orderCollection->getSite()->getTheme();

            $data->invoiceAddress = $this->getInvoiceAddress($data->orderCollection);

            // customer number
            $data->customerNumber = $orderCollection->getCustomer()->getCustomerNumber();
            $data->invoiceDate = $billingItemGroup->getCreatedAt()->format('d-m-Y');

            $data->invoiceNumber = sprintf('ORD%08dF%03d', $orderCollection->getNumber(),
                $billingItemGroup->getSequence());

            $data->invoiceLines = [];
            $totalVat = [];
            $data->totalPrice = 0;
            $data->totalPriceIncl = 0;

            /** @var Voucher $voucher */
            $voucher = $orderCollection->getVouchers()->current();

            //Lines

            /** @var BillingItem $billingItem */
            foreach ($billingItemGroup->getBillingItems() as $billingItem) {
                $debit = $billingItem->getTransactionType() === 'debit';

                $lines = [];

                $order = $billingItem->getOrder();
                if ($order !== null) {
                    $lines = $order->getLines();
                }

                $orderCollectionLine = $billingItem->getOrderCollectionLine();
                if ($orderCollectionLine !== null) {
                    $lines = [$orderCollectionLine];
                }

                //Parse Lines
                /** @var LineInterface $line */
                foreach ($lines as $lineCount => $line) {
                    $invoiceLine = new stdClass();

                    if ($lineCount <= 1) {
                        $invoiceLine->number = $order->getOrderNumber();
                        $invoiceLine->deliveryDate = '';

                        if ($order->getDeliveryDate()) {
                            $invoiceLine->deliveryDate = $order->getDeliveryDate()->format('d-m-Y');
                        }

                        $invoiceLine->firstRow = true;
                    } else {
                        $invoiceLine->number = '';
                        $invoiceLine->deliveryDate = '';
                        $invoiceLine->firstRow = false;
                    }

                    $invoiceLine->quantity = $this->getTransactionTypeValue($line->getQuantity(), $debit);
                    $invoiceLine->title = $line->getTitle();
                    $invoiceLine->description = $line->getDescription();
                    $invoiceLine->totalPrice = $this->getTransactionTypeValue($this->priceService->getRawDisplayPrice($line->getTotalPrice()),
                        $debit);
                    $invoiceLine->totalPriceIncl = $this->getTransactionTypeValue($this->orderLineManager->getAmountIncl($line),
                        $debit);
                    $invoiceLine->vatAmount = $line->getVatAmount();

                    $vatText = $line->getVatLines()->count() > 1 ? 'Gemengd' : ($line->getVatLines()->current()->getVatRate()->getFactor() * 100) . '%';
                    $invoiceLine->vat = $vatText;

                    $lineTotalExcl = $lineExcl = $invoiceLine->totalPrice;

                    $data->invoiceLines[] = $invoiceLine;
                    $data->totalPrice += $invoiceLine->totalPrice;
                    $data->totalPriceIncl += $invoiceLine->totalPriceIncl;

                    if ($line->getDiscountAmount()) {
                        $invoiceLine = new stdClass();
                        $invoiceLine->number = '';
                        $invoiceLine->deliveryDate = '';
                        $invoiceLine->firstRow = false;
                        $invoiceLine->quantity = $this->getTransactionTypeValue(1, $debit);
                        $invoiceLine->title = $voucher ? '- ' . $voucher->getDescription() : '- Korting';
                        $invoiceLine->description = '';
                        $invoiceLine->price = $this->getTransactionTypeValue($line->getDiscountAmount(), !$debit);
                        $invoiceLine->totalPrice = $this->getTransactionTypeValue($line->getDiscountAmount(), !$debit);
                        $invoiceLine->totalPriceIncl = $this->getTransactionTypeValue($line->getDiscountAmount(), !$debit);
                        //TODO check 0 value
                        $invoiceLine->vatAmount = 0;
                        $invoiceLine->vat = null;

                        $lineTotalExcl -= $this->getTransactionTypeValue($this->orderLineManager->getDiscountAmountExcl($line), $debit);
                        $data->invoiceLines[] = $invoiceLine;
                        $data->totalPrice += $invoiceLine->totalPrice;
                        $data->totalPriceIncl += $invoiceLine->totalPriceIncl;
                    }

                    foreach ($line->getVatLines() as $vatLine) {
                        $vatRate = $vatLine->getVatRate();

                        if ($vatRate === null) {
                            continue;
                        }

                        $vatFactor = $vatRate->getFactor();
                        $vatPercentage = $vatFactor * 100;
                        if (!isset($totalVat[$vatPercentage])) {
                            $totalVat[$vatPercentage]['vat'] = 0;
                            $totalVat[$vatPercentage]['totalPrice'] = 0;
                        }

                        $vatLineTotalExcl = 0;
                        if(abs($lineExcl) > 0) {
                            $vatLineTotalExcl = $lineTotalExcl * ($vatLine->getAmount() / abs($lineExcl));
                        }

                        $totalVat[$vatPercentage]['vat'] += $vatLineTotalExcl * $vatFactor;
                        $totalVat[$vatPercentage]['totalPrice'] += $vatLineTotalExcl;
                    }

                }

                if ($order !== null && $order->getInvoiceReference()) {
                    $invoiceLine = new stdClass();
                    $invoiceLine->number = '';
                    $invoiceLine->deliveryDate = '';
                    $invoiceLine->firstRow = false;
                    $invoiceLine->quantity = null;
                    $invoiceLine->title = 'Referentie: ' . $order->getInvoiceReference();
                    $invoiceLine->description = null;
                    $invoiceLine->price = null;
                    $invoiceLine->vat = null;
                    $invoiceLine->totalPrice = null;

                    $data->invoiceLines[] = $invoiceLine;
                }

                ksort($totalVat);

                $data->totalVat = $totalVat;
                $data->paymentMethod = $orderCollection->getPayments()->first()->getPaymentMethod()->translate()->getDescription();
            }

            $invoices[] = $data;
        }

        return $invoices;
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @return string
     */
    private function getInvoiceAddress(OrderCollection $orderCollection)
    {
        $invoiceAddress = '';
        if (null !== ($companyName = $orderCollection->getInvoiceAddressCompanyName())) {
            $invoiceAddress .= $companyName . PHP_EOL;
        }

        if (null !== ($addressAttn = $orderCollection->getInvoiceAddressAttn())) {
            $invoiceAddress .= $addressAttn . PHP_EOL;
        }

        if (!empty($addressStreetAndNumber = $orderCollection->getInvoiceAddressStreetAndNumber())) {
            $invoiceAddress .= $addressStreetAndNumber . PHP_EOL;
        }

        if (null !== ($addressPostcode = $orderCollection->getInvoiceAddressPostcode()) && null !== ($addressCity = $orderCollection->getInvoiceAddressCity())) {
            $invoiceAddress .= $addressPostcode . ' ' . $addressCity . PHP_EOL;
        }

        return $invoiceAddress;
    }
}
