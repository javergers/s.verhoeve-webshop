<?php

namespace AppBundle\Services\Sales\Order;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderActivity;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Security\Employee\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class OrderActivityService
 * @package AppBundle\Services\Sales\Order
 */
class OrderActivityService
{
    /** @var TokenStorage */
    private $tokenStorage;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * OrderActivityService constructor.
     * @param TokenStorage  $tokenStorage
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(TokenStorage $tokenStorage, EntityManagerInterface $entityManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
    }

    /**
     * @param string                $activity
     * @param Order|OrderCollection $order
     * @param User|null             $user
     * @param array                 $options
     * @return OrderActivity
     */
    public function add(string $activity, $order, User $user = null, array $options = null)
    {
        if (!($order instanceof OrderCollection) && !($order instanceof Order)) {
            throw new \RuntimeException(\sprintf('The class %s is not allowed', \get_class($order)));
        }

        if (null === $user && null !== ($token = $this->tokenStorage->getToken()) && $token->getUser() instanceof User) {
            $user = $token->getUser();
        }

        $orderActivity = new OrderActivity();
        $orderActivity->setUser($user);
        $orderActivity->setActivity($activity);

        if ($order instanceof OrderCollection) {
            $orderActivity->setOrderCollection($order);
        } else {
            $orderActivity->setOrder($order);
        }

        if (null !== $options && \is_array($options)) {
            if (isset($options['customer'])) {
                $orderActivity->setCustomer($options['customer']);
            }

            if (isset($options['text'])) {
                $orderActivity->setText($options['text']);
            }
        }

        if(!$this->entityManager->isOpen()) {
            $this->entityManager = $this->entityManager->create(
                $this->entityManager->getConnection(),
                $this->entityManager->getConfiguration()
            );
        }

        $this->entityManager->persist($orderActivity);

        return $orderActivity;
    }
}
