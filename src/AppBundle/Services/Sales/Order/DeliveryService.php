<?php

namespace AppBundle\Services\Sales\Order;

use AppBundle\Entity\Catalog\Product\CompanyTransportType;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Order\Cart;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Employee\User;
use AppBundle\Exceptions\Sales\CartInvalidSupplierCombinationException;
use AppBundle\Manager\Order\CartManager;
use AppBundle\Manager\Order\CartOrderManager;
use AppBundle\Services\CartService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class DeliveryService
 * @package AppBundle\Services\Sales\Order
 */
class DeliveryService
{
    use ContainerAwareTrait;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var CartManager
     */
    private $cartManager;

    /**
     * DeliveryService constructor.
     * @param TokenStorageInterface  $tokenStorage
     * @param EntityManagerInterface $entityManager
     * @param CartManager            $cartManager
     */
    public function __construct(TokenStorageInterface $tokenStorage, EntityManagerInterface $entityManager, CartManager $cartManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
        $this->cartManager = $cartManager;
    }

    /**
     * @param CartOrder $cartOrder
     * @param Request   $request
     * @return bool
     * @throws CartInvalidSupplierCombinationException
     * @throws \Exception
     */
    public function setDeliveryAddressForOrder(CartOrder $cartOrder, Request $request): bool
    {
        $doctrine = $this->container->get('doctrine');
        $em = $doctrine->getManager();

        if ($request->request->has('deliveryMethod') && $request->request->get('deliveryMethod') === 'pickup') {
            $this->removeDeliveryProducts($cartOrder);
        } else {
            $this->updateDeliveryProduct($cartOrder);
        }

        if (isset($request->request->get('cart_order')['lines'])) {
            foreach ($request->request->get('cart_order')['lines'] as $key => $line) {
                $cartOrder->getLines()[$key]->setQuantity($line['quantity']);
            }
        }

        $this->container->get(CartService::class)->mergeOrder($cartOrder);

        $cart = $this->container->get(CartService::class)->getCart();
        $this->cartManager->calculateTotals($cart);

        $em->flush();

        return true;
    }

    /**
     * @return null|Productgroup
     */
    private function getDeliveryProductGroup(): ?Productgroup
    {
        /** @var Productgroup $deliveryCostsProductgroup */
        return $this->container->get('doctrine')->getRepository(Productgroup::class)->findOneBy([
            'skuPrefix' => 'DLV',
        ]);
    }

    /**
     * @param CartOrder $cartOrder
     * @throws CartInvalidSupplierCombinationException
     * @throws NoResultException
     * @throws NonUniqueResultException
     * @throws \Exception
     */
    public function updateDeliveryProduct(CartOrder $cartOrder): void
    {
        $transportTypeHelper = $this->container->get('app.transport_type_helper');
        $deliveryProduct = $transportTypeHelper->getTransportType($cartOrder);

        if($cartOrder->getPickupAddress() !== null){
            $this->removeDeliveryProducts($cartOrder);
            return;
        }

        //check if deliveryInformation is complete
        if(null === $cartOrder->getDeliveryAddressPostcode()
        && null === $cartOrder->getDeliveryAddressNumber()
        && null === $cartOrder->getDeliveryAddressCountry()) {
            return;
        }

        if (null === $deliveryProduct) {
            $this->removeDeliveryProducts($cartOrder);

            return;
        }

        $deliveryPrice = $deliveryProduct->getPrice();

        $cartDeliveryProducts = $cartOrder->getLinesContainingProductgroup($this->getDeliveryProductGroup());
        if($cartDeliveryProducts->count() === $cartOrder->getLines()->count()) {
            $this->removeDeliveryProducts($cartOrder);

            return;
        }

        $em = $this->container->get('doctrine')->getManager();

        $foundCorrectLine = false;

        foreach ($cartDeliveryProducts as $cartDeliveryProduct) {
            if (!$foundCorrectLine && $cartDeliveryProduct->getProduct()->getId() === $deliveryProduct->getId()) {
                $foundCorrectLine = true;
                continue;
            }

            $cartOrder->removeLine($cartDeliveryProduct);
            $em->remove($cartDeliveryProduct);
        }

        if (false === $foundCorrectLine) {
            $cartOrder->addProduct($deliveryProduct, 1);
        }

        $company = $cartOrder->getCollection()->getCompany() ?: null;

        if(null !== $company) {
            $companyDeliveryPrice = $this->container->get(CartOrderManager::class)->getCompanyDeliveryPrice(
                $cartOrder,
                $deliveryProduct,
                $company
            );

            if(null !== $companyDeliveryPrice) {
                $deliveryPrice = $companyDeliveryPrice;
            }
        }

        $cartOrder->getLinesContainingProduct($deliveryProduct)->current()->setPrice($deliveryPrice);
    }

    /**
     * @param CartOrder $cartOrder
     * @param Product   $deliveryProduct
     */
    private function removeDeliveryProducts(CartOrder $cartOrder, Product $deliveryProduct = null): void
    {
        $cartDeliveryProducts = $cartOrder->getLinesContainingProductgroup($this->getDeliveryProductGroup());
        $em = $this->container->get('doctrine')->getManager();

        foreach ($cartDeliveryProducts as $cartDeliveryProduct) {
            if ($cartDeliveryProduct->getProduct() !== $deliveryProduct) {
                $cartOrder->removeLine($cartDeliveryProduct);
                $em->remove($cartDeliveryProduct);
            }
        }
    }

    /**
     * @param Cart $cart
     * @return bool
     */
    public function deliveryDatesInCartAreValid(Cart $cart): bool
    {
        $currentDate = new \DateTime();
        $currentDate->setTime(0, 0);

        if (null === $cart->getSite()->getDeliveryDateMethod()) {
            return true;
        }

        /** @var CartOrder $cartOrder */
        foreach ($cart->getOrders() as $cartOrder) {
            if (null === $cartOrder->getPickupAddress() && $cartOrder->getDeliveryDate() < $currentDate) {
                return false;
            }
        }

        return true;
    }
}
