<?php

namespace AppBundle\Services\Sales\Order;

use AppBundle\Interfaces\RenderablePdfInterface;
use Symfony\Component\HttpFoundation\Response;

interface PdfRendererInterface
{
    /**
     * @return string
     */
    public function getFilename();

    /**
     * @return Response
     */
    public function getResponse();

    /**
     * @return string
     */
    public function generateHtml();

    /**
     * @param string $action
     *
     * @return $this
     */
    public function setAction($action);
}