<?php

namespace AppBundle\Services\Sales\Order;

use AppBundle\Entity\Catalog\Product\Letter;
use AppBundle\Entity\Catalog\Product\Personalization;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Supplier\SupplierOrderLine;
use AppBundle\Manager\Supplier\SupplierOrderManager;
use AppBundle\Services\ConnectorHelper;
use AppBundle\Services\Designer;
use AppBundle\Services\SupplierManagerService;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Environment;

/**
 * Class PackingSlip
 * @package AppBundle\Services\Sales\Order
 */
class PackingSlip extends AbstractPdfRenderer
{
    /** @var float */
    private $totalPrice = 0.000;

    /** @var float */
    private $totalPriceIncl = 0.00;

    /** @var float */
    private $totalDeliveryCosts = 0.000;

    /** @var float */
    private $totalDeliveryCostsIncl = 0.00;

    /** @var Order */
    protected $item;

    /** @var ConnectorHelper|null */
    protected $connectorHelper;

    /** @var SupplierManagerService|null */
    protected $supplierManager;

    /** @var CacheManager|null */
    protected $imageCacheManager;

    /** @var Designer|null */
    protected $designerService;

    /** @var SupplierOrderManager */
    protected $supplierOrderManager;

    /** @var array  */
    protected $outputOptions = [
        'landscape' => true,
        'margin' => [
            'top' => 0,
            'right' => 0,
            'bottom' => 0,
            'left' => 0
        ],
    ];

    /** @var array */
    private $pages = [];

    /** @var array  */
    private $processedChildLines = [];

    /**
     * PackingSlip constructor.
     * @param RequestStack           $requestStack
     * @param Environment            $twig
     * @param ConnectorHelper        $connectorHelper
     * @param SupplierManagerService $supplierManager
     * @param CacheManager           $imageCacheManager
     * @param Designer               $designerService
     * @param SupplierOrderManager   $supplierOrderManager
     */
    public function __construct(RequestStack $requestStack, Environment $twig, ConnectorHelper $connectorHelper, SupplierManagerService $supplierManager, CacheManager $imageCacheManager, Designer $designerService, SupplierOrderManager $supplierOrderManager)
    {
        $this->connectorHelper = $connectorHelper;
        $this->supplierManager = $supplierManager;
        $this->imageCacheManager = $imageCacheManager;
        $this->designerService = $designerService;
        $this->supplierOrderManager = $supplierOrderManager;

        parent::__construct($requestStack, $twig);
    }

    /**
     * @return string
     * @throws \Twig_Error
     */
    public function generateHtml()
    {
        $supplierOrder = $this->item->getSupplierOrder();
        $hasForwardPricesToShow = $supplierOrder ? $this->supplierOrderManager->hasForwardPriceToShow($supplierOrder) : false;

        $this->parameters['order'] = $this->item;
        $this->parameters['supplierOrder'] = $supplierOrder;
        $this->parameters['hasForwardPricesToShow'] = $hasForwardPricesToShow;
        $this->parameters['pages'] = $this->getPages();
        $this->parameters['theme_colors'] = $this->getThemeColors();
        $this->parameters['totalPrice'] = $this->totalPrice;
        $this->parameters['totalPriceIncl'] = $this->totalPriceIncl;
        $this->parameters['totalDeliveryCosts'] = $this->totalDeliveryCosts;
        $this->parameters['totalDeliveryCostsIncl'] = $this->totalDeliveryCostsIncl;
        $this->parameters['paperSize'] = $this->paperSize;

        $this->html = $this->twig->render('@App/pdf/order/packing_slip.html.twig',
            $this->parameters);

        return $this->html;
    }

    /**
     * @param Order $order
     *
     * @return $this
     */
    public function setItem(Order $order)
    {
        $this->item = $order;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return 'Pakbon ' . $this->item->getOrderCollection()->getNumber() . '-' . $this->item->getNumber() . '.pdf';
    }

    /**
     * @return array
     */
    private function getPages()
    {
        /** @var Order $order */
        $order = $this->item;

        if ($order->getSupplierOrder() === null) {
            throw new \RuntimeException('Order has no supplierOrder');
        }

        $lines = $order->getSupplierOrder()->getRootLines();

        $pageIndex = [];
        $i = 0;
        foreach ($lines as $line) {
            $orderLine = $line->getOrderLine();

            $orderLineProduct = $orderLine->getProduct();
            $isDelivery = $orderLineProduct->isTransportType();

            $page = null;

            if (!$isDelivery) {
                if (\in_array($orderLine->getId(), $pageIndex, true)) {
                    $key = \array_search($orderLine->getId(), $pageIndex, true);

                    $page = $this->pages[$key];
                }

                if (null === $page) {
                    $page = $this->createPage();
                    $page->number = $i + 1;
                    $page->lineId = $orderLine->getId();

                    $this->pages[$i] = $page;
                    $pageIndex[$i] = $orderLine->getId();
                    $i++;
                }

                if (null !== $page) {
                    $this->assignPageProducts($page, $line);
                }
            } else {
                $this->totalDeliveryCosts += $line->getTotalPrice();
                $this->totalDeliveryCostsIncl += $line->getTotalPriceIncl();
            }
        }

        // Sort page products and calculate totals.
        foreach ($this->pages as $page) {
            $this->sortProducts($page->products);

            $this->totalPrice += $page->totalPrice;
            $this->totalPriceIncl += $page->totalPriceIncl;
        }

        $this->totalPrice += $this->totalDeliveryCosts;
        $this->totalPriceIncl += $this->totalDeliveryCostsIncl;

        // Remove pages without card text
        if(strtoupper($this->paperSize) === 'A5') {
            foreach ($this->pages as $key => $page) {
                if($page->card === null) {
                    unset($this->pages[$key]);
                    continue;
                }
            }
        }

        return $this->pages;
    }

    /**
     * @param $theme
     *
     * @return array
     */
    private function getThemeColors()
    {
        /** @var Order $order */
        $order = $this->item;

        $site = $order->getOrderCollection()->getSite();

        $theme = $site->getTheme();

        $colors = [];

        // @todo: Colors based on theme settings.
        if ($theme === 'suppliers' || $theme === 'topgeschenken') {
            $colors['primaryColor'] = '#b5251a';
        } elseif ($theme === 'toptaarten') {
            $colors['primaryColor'] = '#d77710';
        } elseif ($theme === 'topbloemen') {
            $colors['primaryColor'] = '#886d4f';
        } else {
            $colors['primaryColor'] = '#919B16';
        }

        return $colors;
    }

    /**
     * @param \stdClass         $page
     * @param SupplierOrderLine $supplierOrderLine
     * @return \stdClass
     */
    private function createPageProduct(\stdClass $page, SupplierOrderLine $supplierOrderLine)
    {
        $product = $supplierOrderLine->getConcludedSupplierProduct()->getProduct();

        $supplier = $supplierOrderLine->getSupplierOrder()->getSupplier();

        $supplierProduct = $this->supplierManager->getSupplierProduct($product, $supplier);

        $orderLine = $supplierOrderLine->getOrderLine();
        $pageProduct = new \stdClass();
        $pageProduct->title = $supplierProduct->translate()->getTitle();
        $pageProduct->desciption = $supplierProduct->translate()->getDescription();
        $pageProduct->quantity = $supplierOrderLine->getQuantity();
        $pageProduct->displayPrice = $supplierOrderLine->getPriceIncl();
        $pageProduct->totalDisplayPrice = $supplierOrderLine->getTotalPriceIncl();
        $pageProduct->isDelivery = $product->isTransportType();
        $pageProduct->sortOrder = $this->applyProductSortOrder($product);
        $pageProduct->sku = $supplierProduct->getSku();

        $this->assignPageProductImages($page, $orderLine, $product);
        $this->assignPageCard($page, $orderLine, $product);

        return $pageProduct;
    }

    /**
     * @param \stdClass         $page
     * @param SupplierOrderLine $supplierOrderLine
     * @return array
     */
    private function assignPageProducts(\stdClass $page, SupplierOrderLine $supplierOrderLine): \stdClass
    {
        $pageProduct = $this->createPageProduct($page, $supplierOrderLine);

        $page->totalPrice += $supplierOrderLine->getTotalPrice();
        $page->totalPriceIncl += $supplierOrderLine->getTotalPriceIncl();

        $supplierOrder = $this->item->getSupplierOrder();

        $pageProduct->childs = [];
        if (null !== $supplierOrder) {
            $childLines = $supplierOrder->getChildLines($supplierOrderLine);

            foreach ($childLines as $childLine) {
                //Since combination products are split in the supplier order childlines can already be added to packing slip by previous combinations
                $orderline = $childLine->getOrderLine();
                if(in_array($orderline->getId(), $this->processedChildLines, true)) {
                    continue;
                }

                $pageProduct->childs[$orderline->getId()] = $this->createPageProduct($page, $childLine);

                $page->totalPrice += $childLine->getTotalPrice();
                $page->totalPriceIncl += $childLine->getTotalPriceIncl();

                $this->processedChildLines[] = $orderline->getId();
            }
        }

        $this->sortProducts($pageProduct->childs);

        $page->products[] = $pageProduct;

        return $page;
    }

    /**
     * @return \stdClass
     */
    private function createPage(): \stdClass
    {
        $page = new \stdClass();
        $page->products = [];
        $page->totalPrice = 0;
        $page->totalPriceIncl = 0;
        $page->card = null;
        $page->images = [];

        return $page;
    }

    /**
     * @param Product $product
     * @return int
     */
    private function applyProductSortOrder(Product $product)
    {
        if ($product instanceof ProductCard) {
            return 1;
        }

        if ($product instanceof Personalization) {
            return 2;
        }

        if ($product instanceof Letter) {
            return 3;
        }

        if ($product->isTransportType()) {
            return 4;
        }

        return 0;
    }

    /**
     * @param array $products
     * @return array
     */
    private function sortProducts(array &$products): array
    {
        uasort($products, function (\stdClass $a, \stdClass $b) {
            return $a->sortOrder <=> $b->sortOrder;
        });

        return $products;
    }

    /**
     * @param \stdClass $page
     * @param OrderLine $orderLine
     * @param Product   $product
     * @return void
     */
    private function assignPageProductImages(\stdClass $page, OrderLine $orderLine, Product $product): void
    {
        $imageUrl = null;

        if ($product instanceof Personalization) {
            $imageUrl = $this->getProductDesign($orderLine);
        } elseif (false === $product instanceof ProductCard || false === $product instanceof letter) {
            $imageUrl = $this->getProductImage($product);
        }

        if (null !== $imageUrl) {
            $page->images[] = $imageUrl;
        }
    }

    /**
     * @param \stdClass $page
     * @param OrderLine $orderLine
     * @param Product   $product
     * @return void
     */
    private function assignPageCard(\stdClass $page, OrderLine $orderLine, Product $product): void
    {
        if ($product instanceof ProductCard) {
            $metadata = $orderLine->getMetadata();

            $page->card = new \stdClass();

            $page->card->text = $metadata['text'];

            $isGb = (bool)($metadata['is_gb'] ?? false);
            $page->card->is_gb = $isGb;

            if ($isGb) {
                $hasLogo = (bool)($metadata['logo'] ?? false);

                if ($hasLogo) {
                    $logo = (object)$metadata['logo'];

                    $cardWidth = 148; // mm
                    $cardHeight = 105; // mm

                    $logo->width = ($cardWidth / 100) * $logo->width;
                    $logo->height = ($cardHeight / 100) * $logo->height;
                    $logo->margin_x = ($cardHeight / 100) * $logo->margin_x;
                    $logo->margin_y = ($cardHeight / 100) * $logo->margin_y;

                    $page->card->logo = $logo;
                }

                $theme = $metadata['theme'] ?: null;

                if (null !== $theme) {
                    $page->card->image = $theme;
                }

            } elseif (null !== $product->getImagePath()) {
                $page->card->image = $this->imageCacheManager->getBrowserPath($product->getImagePath(), 'product_card_print');
            }
        }
    }

    /**
     * @param OrderLine $orderLine
     * @return null|string
     */
    private function getProductDesign(OrderLine $orderLine)
    {
        $imageUrl = null;

        // Attach designs.
        if (isset($orderLine->getMetadata()['designer'])) {
            $uuid = $orderLine->getMetadata()['designer'];
            $this->designerService->setUuid($uuid);

            $imageUrl = $this->designerService->getFileUrl(Designer::FILE_PRINT, 60, Designer::SOURCE_LOCAL);
        }

        if (isset($orderLine->getMetadata()['design_url_uuid'], $orderLine->getMetadata()['design_url_filename'])) {
            $this->designerService->setUuid($orderLine->getMetadata()['design_url_uuid']);
            $this->designerService->setIsUrlDesign(true);

            $imageUrl = $this->designerService->getFileUrl($orderLine->getMetadata()['design_url_filename'], 60, Designer::SOURCE_LOCAL);
        }

        if (isset($orderLine->getMetadata()['design_url'])) {
            $imageUrl = $orderLine->getMetadata()['design_url'];
        }

        return $imageUrl;
    }

    /**
     * @param Product $product
     * @return null|string
     */
    private function getProductImage(Product $product)
    {
        if (false !== $product instanceof ProductCard) {
            return null;
        }

        $mainImage = $product->getMainImage();
        if (null === $mainImage) {
            return null;
        }

        return $this->imageCacheManager->getBrowserPath($mainImage->getPath(), 'product_thumbnail');
    }
}
