<?php

namespace AppBundle\Services\Fraud;

use AppBundle\Entity\Order\FraudDetectionReport;
use AppBundle\Entity\Order\FraudDetectionRule;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Model\OrderCollectionModel;
use AppBundle\Repository\OrderCollectionRepository;
use AppBundle\Services\OrderCollectionManagerService;
use AppBundle\Services\Parameter\ParameterService;
use AppBundle\Services\Sales\Order\OrderActivityService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use RuleBundle\Entity\Rule;
use RuleBundle\Service\ResolverService;
use RuleBundle\Service\RuleTransformerService;

/**
 * Class OrderFraudCheckService
 * @package AppBundle\Services\Fraud
 */
class OrderFraudCheckService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var ArrayCollection */
    private $fraudDetectionRules;

    /** @var ArrayCollection */
    private $fraudFields;

    /** @var ParameterService */
    private $parameterService;

    /** @var ResolverService */
    private $ruleResolver;

    /** @var RuleTransformerService */
    private $ruleTransformer;

    /** @var OrderActivityService */
    private $orderActivityService;
    
    /** @var bool */
    private $fraudFieldsChecked = false;

    /**
     * OrderFraudCheckService constructor.
     * @param EntityManagerInterface          $entityManager
     * @param ParameterService       $parameterService
     * @param ResolverService        $ruleResolver
     * @param RuleTransformerService $ruleTransformer
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ParameterService $parameterService,
        ResolverService $ruleResolver,
        RuleTransformerService $ruleTransformer,
        OrderActivityService $orderActivityService
    ) {
        $this->entityManager = $entityManager;
        $this->parameterService = $parameterService;
        $this->ruleResolver = $ruleResolver;
        $this->ruleTransformer = $ruleTransformer;
        $this->orderActivityService = $orderActivityService;

        $this->fraudDetectionRules = new ArrayCollection();
        $this->fraudFields = new ArrayCollection();
    }

    /**
     * @param OrderCollection|OrderCollectionModel $orderCollection
     *
     * @return bool
     */
    public function canEditOrder(OrderCollection $orderCollection): bool
    {
        $fraudCheckableOrders = $this->entityManager->getRepository(OrderCollection::class)->findFraudCheckableOrders($orderCollection);

        return !(null === $orderCollection->getFraudScore() && $fraudCheckableOrders);
    }

    /**
     * @param OrderCollection|OrderCollectionModel $orderCollection
     * @param bool                                 $auto
     *
     * @return bool
     * @throws \Exception
     */
    public function canProcessOrder(OrderCollection $orderCollection, bool $auto = false): bool
    {
        $fraudThresholdParameter = $auto ? 'fraud_warning_score' : 'fraud_blocked_score';

        if ($this->canEditOrder($orderCollection)) {
            $fraudThreshold = $this->parameterService->setEntity()->getValue($fraudThresholdParameter);
            if ($orderCollection->getFraudScore() < $fraudThreshold) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param                 $field
     * @param OrderCollection $orderCollection
     *
     * @return bool|mixed
     */
    public function getFraudField($field, OrderCollection $orderCollection)
    {
        if ($this->fraudFields->isEmpty() && $this->fraudFieldsChecked != $orderCollection->getId()) {
            $this->getFraudFields($orderCollection);
        }

        if (isset($this->fraudFields[$field])) {
            return $this->fraudFields[$field];
        }

        return null;
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @return ArrayCollection|null
     */
    private function getFraudFields(OrderCollection $orderCollection)
    {
        $this->fraudFieldsChecked = $orderCollection->getId();

        try {
            /** @var Connection $connection */
            $sql = 'SELECT r.id as rule_id, fdre.fraud_detection_rule_id FROM rule as r
                LEFT JOIN fraud_detection_rule as fdru on fdru.rule_id = r.id
                LEFT JOIN fraud_detection_report as fdre on fdre.fraud_detection_rule_id = fdru.id
                WHERE fdre.order_collection_id = :orderCollection';

            $connection = $this->entityManager->getConnection();
            $stmt = $connection->prepare($sql);
            $stmt->execute(['orderCollection' => $orderCollection->getId()]);

            foreach ($stmt->fetchAll() as $ruleIds) {
                /** @var Rule $rule */
                $rule = $this->entityManager->getRepository(Rule::class)->find($ruleIds['rule_id']);
                $fraudDetectionRule = $this->entityManager->getRepository(FraudDetectionRule::class)->find($ruleIds['fraud_detection_rule_id']);
                $transformedRule = $this->ruleTransformer->transformData($rule);

                if ($this->findFieldKeys($transformedRule)) {
                    $this->fraudFields[str_replace(ResolverService::QB_ALIAS . '.', null,
                        $this->findFieldKeys($transformedRule))] = [
                        'fraudDetectionRule' => $fraudDetectionRule,
                        'rule' => $rule,
                    ];
                }
            }
        } catch (\Exception $e) {
            return null;
        }

        return $this->fraudFields;
    }

    /**
     * @param OrderCollection $orderCollection
     *
     * @return int
     * @throws \Exception
     */
    public function checkOrder(OrderCollection $orderCollection): int
    {
        $score = 0;

        $fraudCheckableOrders = $this->entityManager->getRepository(OrderCollection::class)->findFraudCheckableOrders($orderCollection);

        if (!$fraudCheckableOrders) {
            throw new \RuntimeException(sprintf('The order collection "%s" can not be checked for fraud',
                $orderCollection->getId()));
        }

        if ($this->fraudDetectionRules->isEmpty()) {
            $this->fraudDetectionRules = $this->entityManager->getRepository(FraudDetectionRule::class)->findBy(['publish' => true]);
        }

        $fraudDetectionReportRepository = $this->entityManager->getRepository(FraudDetectionReport::class);
        /** @var FraudDetectionRule $fraudRule */
        foreach ($this->fraudDetectionRules as $fraudRule) {
            if ($this->ruleResolver->satisfies($orderCollection, $fraudRule->getRule())) {
                $score += $fraudRule->getScore();

                $fraudDetectionReportRepository->findOneOrCreate([
                    'orderCollection' => $orderCollection,
                    'fraudDetectionRule' => $fraudRule,
                ]);
            } else {
                $fraudDetectionReport = $fraudDetectionReportRepository->findOneBy([
                    'orderCollection' => $orderCollection,
                    'fraudDetectionRule' => $fraudRule,
                ]);

                if ($fraudDetectionReport) {
                    $this->entityManager->remove($fraudDetectionReport);
                }
            }
        }

        $activity = $this->orderActivityService->add('order_fraud_check_completed', $orderCollection, null, [
            'text' => \sprintf('Berekende fraude score: %d', $score),
        ]);

        $orderCollection->addActivity($activity);
        $orderCollection->setFraudScore($score);

        $this->entityManager->flush();

        return $score;
    }

    /**
     * @param $array
     * @return bool|string
     */
    private function findFieldKeys($array)
    {
        foreach ($array as $key => $item) {
            if ($key === 'field') {
                return $item['field'];
            }

            if (\is_array($item)) {
                return $this->findFieldKeys($item);
            }
        }

        return false;
    }

}
