<?php

namespace AppBundle\Services;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Manager\Order\OrderLineManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use AppBundle\Services\Sales\Order\PackingSlip;
use AppBundle\Services\Supplier\HashService;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class OrderAssetsHashService
 * @package AppBundle\Services
 */
class OrderAssetsHashService
{
    /**
     * @var OrderLineManager
     */
    private $orderLineManager;

    /**
     * @var Doctrine
     */
    private $doctrine;

    /**
     * @var array
     */
    private $hashes = [];

    /**
     * @var Order
     */
    private $order;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var PackingSlip
     */
    private $packingSlip;

    /**
     * @var HashService
     */
    private $hashService;

    /**
     * OrderAssetsHashService constructor.
     *
     * @param Doctrine $doctrine
     * @param RouterInterface $router
     * @param OrderLineManager $orderLineManager
     * @param PackingSlip $packingSlip
     * @param HashService $hashService
     */
    public function __construct(
        Doctrine $doctrine,
        RouterInterface $router,
        OrderLineManager $orderLineManager,
        PackingSlip $packingSlip,
        HashService $hashService
    ) {
        $this->doctrine = $doctrine;
        $this->router = $router;
        $this->orderLineManager = $orderLineManager;
        $this->packingSlip = $packingSlip;
        $this->hashService = $hashService;
    }

    /**
     * @param Order $order
     */
    public function setOrder(Order $order)
    {
        $this->order = $order;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        if (!$this->order) {
            throw new NotFoundHttpException;
        }

        return $this->order;
    }

    /**
     * @param $hashObj
     * @return StreamedResponse
     * @throws \Exception
     */
    public function processDesignHash($hashObj)
    {
        $orderLineId = (int) $hashObj->orderLineId;
        $supplierId = (int) $hashObj->supplierId;

        /** @var OrderLine $orderLine */
        $orderLine = $this->doctrine->getRepository(OrderLine::class)->find($orderLineId);

        if (!$orderLine) {
            throw new NotFoundHttpException('OrderLine not found');
        }

        if ($orderLine->getOrder()->getSupplierOrder() === null) {
            throw new \RuntimeException(sprintf("No supplier order for order '%s'", $orderLine->getOrder()->getOrderNumber()));
        }

        $supplier = $orderLine->getOrder()->getSupplierOrder()->getSupplier();

        $response = null;
        if ($orderLine->getMetadata() && $orderLine->getOrder() && $supplier->getId() === $supplierId) {
            $response = $this->orderLineManager->downloadPrintDesign($orderLine);
        }

        if ($response !== null) {
            return $response;
        }

        throw new NotFoundHttpException('Design not found');
    }

    /**
     * @param $hashObj
     * @return Response
     */
    public function processPackingSlipHash($hashObj)
    {
        $orderId = (int) $hashObj->orderId;
        $supplierId = (int) $hashObj->supplierId;

        /** @var Order $order */
        $order = $this->doctrine->getRepository(Order::class)->find($orderId);

        if (!$order) {
            throw new NotFoundHttpException('Order not found');
        }

        if ($order->getSupplierOrder() === null) {
            throw new \RuntimeException(sprintf("No supplier order for order '%s'", $order->getOrderNumber()));
        }

        if (null !== $order->getSupplierOrder() && $order->getSupplierOrder()->getSupplier()->getId() === $supplierId) {
            return $this->packingSlip
                ->setParameters([])
                ->setItem($order)
                ->getResponse();
        }

        throw new NotFoundHttpException('Packingslip not found');
    }

    /**
     * @param $type
     * @param $hash
     * @return Response|StreamedResponse
     * @throws \Exception
     */
    public function processHash($type, $hash)
    {
        $hashObj = $this->hashService->decryptHash($type, $hash);

        if ($type === 'design') {
            return $this->processDesignHash($hashObj);
        }

        if ($type === 'packingslip') {
            return $this->processPackingSlipHash($hashObj);
        }

        throw new \RuntimeException('Failed to load.');
    }

    /**
     * @return array|bool
     */
    public function buildHashes()
    {
        if ($this->getOrder() === null) {
            return false;
        }

        if ($this->getOrder()->getSupplierOrder() === null) {
            return false;
        }

        /** @var OrderLine $orderLine */
        foreach ($this->getOrder()->getLines() as $orderLine) {
            if ($orderLine->getMetadata() === null) {
                continue;
            }

            if (
                isset($orderLine->getMetadata()['designer'])
                || isset($orderLine->getMetadata()['design_url'])
                || isset($orderLine->getMetadata()['design_url_uuid'], $orderLine->getMetadata()['design_url_filename'])
            ) {
                if (!isset($this->hashes['designs'])) {
                    $this->hashes['designs'] = [];
                }

                $objHash = new \stdClass();
                $objHash->type = 'design';
                $objHash->data = [];
                $objHash->orderLineId = $orderLine->getId();
                $objHash->supplierId = $this->getOrder()->getSupplierOrder()->getSupplier()->getId();

                $this->hashes['designs'][] = $this->getTempApiRenderDesignUrl(
                    ['hash' => $this->hashService->encryptHash($objHash)]
                );
            }

        }

        // Genereate packingslip hash.
        $objHash = new \stdClass();
        $objHash->type = 'packingslip';
        $objHash->data = [];
        $objHash->orderId = $this->getOrder()->getId();
        $objHash->supplierId = $this->getOrder()->getSupplierOrder()->getSupplier()->getId();

        $this->hashes['packingslip'] = $this->getTempApiRenderPackingslipUrl(
            ['hash' => $this->hashService->encryptHash($objHash)]
        );

        return $this->hashes;
    }

    /**
     * @param $parameters
     * @return string
     */
    private function getTempApiRenderDesignUrl($parameters)
    {
        return rtrim($this->router->getGenerator()->generate('app_tempapi_orderorder_getdesign',
            $parameters, UrlGeneratorInterface::ABSOLUTE_URL), '/');
    }

    /**
     * @param $parameters
     * @return string
     */
    private function getTempApiRenderPackingslipUrl($parameters)
    {
        return rtrim($this->router->generate('app_tempapi_orderorder_getpackingslip',
            $parameters, UrlGeneratorInterface::ABSOLUTE_URL), '/');
    }
}
