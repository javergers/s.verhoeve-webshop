<?php

namespace AppBundle\Services\Convert;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Entity\Site\Site;
use AppBundle\Repository\CustomerRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;

/**
 * Class CustomerService
 * @package AppBundle\Services\Convert
 */
class CustomerService
{

    /**
     * @var Customer $customer
     */
    private $customer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var CustomerRepository
     */
    private $customerRepository;

    /**
     * CustomerService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->customerRepository = $this->entityManager->getRepository(Customer::class);
    }

    /**
     * @param null $rawData
     * @return object
     * @throws \Exception
     * @internal param array|null $data
     */
    public function validateRawData($rawData = null)
    {

        $optionResolver = new OptionsResolver();

        if (isset($rawData->metadata)) {
            $rawData->metadata = (array)$rawData->metadata;
        }

        $optionResolver->setDefined([
            "id",
            "companyId",
            "firstname",
            "lastnamePrefix",
            "lastname",
            "phoneNumber",
            "email",
            "gender",
            "username",
            "password",
            "internalRemark",
            "enabled",
            "locked",
            "metadata",
            "registeredOnSiteId",
            "gender",
            "customerGroup",
        ]);

        $optionResolver->setDefaults([
            'id' => null,
            'registeredOnSite' => null,
            'phoneNumber' => null,
            'metadata' => null,
            'gender' => null,
        ]);

        $optionResolver->setAllowedTypes('enabled', ['boolean']);
        $optionResolver->setAllowedTypes('locked', ['boolean']);
        $optionResolver->setAllowedValues('gender', ['male', 'female']);

        $optionResolver->setRequired([
            "username",
        ]);

        try {

            $rawData = (object)$optionResolver->resolve((array)$rawData);

        } catch (\Exception $e) {
            //throw new \Exception('Customer data fields missing.');
            throw new \Exception($e->getMessage());
        }

        if (!$this->isValidUsername($rawData->username)) {

            // Use email address if username isn't valid.
            if (!$this->isValidUsername($rawData->email)) {
                throw new \Exception('Invalid username');
            }

            $rawData->username = $rawData->email;
        }

        if (empty($rawData->id) && $this->customerRepository->findByUsername($rawData->username)) {

            $e = new \Exception('Username `' . $rawData->username . '` already exist.');
            $e->type = 'username_exist';

            $customer = $this->customerRepository->findByUsername($rawData->username);

            if ($customer) {
                $e->customer = $this->getJsonData($customer);
            }

            throw $e;
        }

        return $rawData;

    }

    /**
     * @param null $username
     * @return bool
     */
    private function isValidUsername($username = null)
    {

        $violations = Validation::createValidator()->validate($username, [
            new NotBlank(),
            new Email(),
        ]);

        if ($violations->count() > 0) {
            return false;
        }

        return true;
    }

    /**
     * @param Customer $customer
     * @param bool     $withRelations
     * @return \stdClass
     */
    public function getJsonData(Customer $customer, bool $withRelations = false)
    {

        $data = new \stdClass();

        $data->id = $customer->getId();
        $data->gender = $customer->getGender();
        $data->fullname = $customer->getFullname();
        $data->firstname = $customer->getFirstname();
        $data->lastnamePrefix = $customer->getLastnamePrefix();
        $data->lastname = $customer->getLastname();
        $data->birthday = $customer->getBirthday();
        $data->phoneNumber = $customer->getPhoneNumber();
        $data->mobilenumber = $customer->getMobilenumber();
        $data->username = $customer->getUsername();
        $data->password = $customer->getPassword();
        $data->metadata = $customer->getMetadata();
        $data->email = $customer->getEmail();
        $data->internalRemark = $customer->getInternalRemark();
        $data->createAt = $customer->getCreatedAt();
        $data->updatedAt = $customer->getUpdatedAt();
        $data->deletedAt = $customer->getDeletedAt();
        $data->locked = $customer->getLocked();
        $data->expired = $customer->getExpired();
        $data->expiredAt = $customer->getExpiresAt();

        if ($withRelations) {
            $data->customerGroup = ($customer->getCustomerGroup() ? $customer->getCustomerGroup()->getId() : null);
        }

        if ($customer->getCompany()) {
            $data->company = new \stdClass();
            $data->company->id = $customer->getCompany()->getId();
            $data->company->name = $customer->getCompany()->getName();

            if ($customer->getCompany()->getCompanyRegistration()) {
                $data->company->companyRegistration = new \stdClass();
                $data->company->companyRegistration->registrationNumber = $customer->getCompany()->getCompanyRegistration()->getRegistrationNumber();
            }

            $data->company->customerCount = 1;
            if ($customer->getCompany()->getCustomers()) {
                $data->company->customerCount = $customer->getCompany()->getCustomers()->count();
            }
        }

        return $data;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    private function setAttributes(array $attributes = [])
    {

        foreach ($attributes as $key => $value) {

            $method = 'set' . ucwords($key);
            if (method_exists($this->customer, $method) && !is_null($value)) {
                $this->customer = call_user_func_array([$this->customer, $method], [$value]);
            }
        }

        return $this;
    }

    /**
     * @param array $attributes
     * @return Customer
     */
    public function createOrUpdate(array $attributes)
    {


        $persist = false;

        if (
            !isset($attributes['id'])
            || !$this->customer = $this->customerRepository->find($attributes['id'])
        ) {
            $this->customer = new Customer();

            $persist = true;
        }

        if (isset($attributes['registeredOnSiteId'])) {

            $site = $this->entityManager->getRepository(Site::class)->findOneBy(['theme' => $attributes['registeredOnSiteId']]);

            if ($site) {
                $attributes['registeredOnSite'] = $site;
            }

            unset($attributes['registeredOnSiteId']);
        }

        if (isset($attributes['companyId'])) {

            $company = $this->entityManager->getRepository(Company::class)->find($attributes['companyId']);

            if ($company) {
                $attributes['company'] = $company;
            }

            unset($attributes['companyId']);
        }

        if (isset($attributes['customerGroup'])) {

            $customerGroup = $this->entityManager->getRepository(CustomerGroup::class)->findOneBy([
                'description' => $attributes['customerGroup'],
            ]);

            if (!is_null($customerGroup) && !$this->customer->getCustomerGroup()) {
                $this->customer->setCustomerGroup($customerGroup);
            }
            unset($attributes['customerGroup']);

        }

        $this->setAttributes($attributes);

        if ($persist) {
            $this->entityManager->persist($this->customer);
        }

        return $this->customer;
    }

}
