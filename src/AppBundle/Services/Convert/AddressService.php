<?php

namespace AppBundle\Services\Convert;

use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Geography\Country;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AddressService
 * @package AppBundle\Services\Convert
 */
class AddressService
{

    /**
     * @var Address $address ;
     */
    private $address;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * AddressService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param array $rawData
     * @return object
     * @throws \Exception
     */
    public function validateRawData(array $rawData)
    {

        /* Validate data */
        $optionResolver = new OptionsResolver();

        $optionResolver->setDefined([
            'id',
            'email',
            'phoneNumber',
            'mainAddress',
            'invoiceAddress',
            'deliveryAddress',
            'description',
            'companyName',
            'attn',
            'street',
            'number',
            'postcode',
            'city',
            'country',
        ]);

        // Set defaults for optional paramaters
        $optionResolver->setDefaults([
            'id' => null,
            'email' => null,
            'phoneNumber' => null,
            'mainAddress' => false,
            'invoiceAddress' => false,
            'deliveryAddress' => false,
        ]);

        $optionResolver->setRequired([
            "description",
            "companyName",
            "attn",
            "street",
            "number",
            "postcode",
            "city",
            "country",
        ]);

        try {
            return (object)$optionResolver->resolve((array)$rawData);
        } catch (\Exception $e) {
            throw new \Exception('Address data fields missing.');
        }
    }

    /**
     * @param Address $address
     * @return \stdClass
     */
    public function getJsonData(Address $address)
    {

        $data = new \stdClass();
        $data->id = $address->getId();
        $data->attn = $address->getAttn();
        $data->description = $address->getDescription();
        $data->companyName = $address->getCompanyName();
        $data->email = $address->getEmail();
        $data->phoneNumber = $address->getPhoneNumber();
        $data->mainAddress = $address->getMainAddress();
        $data->invoiceAddress = $address->getInvoiceAddress();
        $data->deliveryAddress = $address->getDeliveryAddress();
        $data->street = $address->getStreet();
        $data->number = $address->getNumber();
        $data->postcode = $address->getPostcode();
        $data->city = $address->getCity();
        $data->country = ($address->getCountry() ? $address->getCountry()->getId() : null);

        return $data;
    }

    /**
     * @param array $attributes
     * @return Address
     */
    public function createOrUpdate(array $attributes)
    {

        $persist = false;

        if (!isset($attributes['id']) || !$this->address = $this->entityManager->getRepository(Address::class)->find($attributes['id'])) {
            $this->address = new Address();
            $persist = true;
        }

        $this->setAttributes($attributes);

        if ($persist) {
            $this->entityManager->persist($this->address);
        }

        return $this->address;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    private function setAttributes(array $attributes = [])
    {
        foreach ($attributes as $key => $value) {
            $method = 'set' . ucwords($key);
            if (null !== $value && method_exists($this->address, $method)) {
                if ($key === 'country') {
                    $value = $this->entityManager->getRepository(Country::class)->find($value);
                }
                $this->address = call_user_func([$this->address, $method], $value);
            }
        }

        return $this;
    }

}
