<?php

namespace AppBundle\Services\Search;

use Interfaces\Entity\EntityInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Select;
use Doctrine\ORM\QueryBuilder;
use FS\SolrBundle\Query\SolrQuery;
use Knp\DoctrineBehaviors\Model\Translatable\Translation;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class AbstractSearch
 * @package AppBundle\Services\Search
 */
abstract class AbstractSearch
{
    use ContainerAwareTrait;

    /**
     * @var QueryBuilder
     */
    protected $qb;

    /**
     * @var string
     */
    protected $entity;

    /**
     * @var string|null
     */
    protected $translationEntity;

    /**
     * @var string
     */
    private $locale;

    /**
     * @var int
     */
    private $defaultLimit = 50;

    /**
     * @param QueryBuilder $qb
     */
    public function setQueryBuilder(QueryBuilder $qb): void
    {
        $this->qb = $qb;
    }

    /**
     * @return QueryBuilder
     * @throws \Exception
     */
    public function getQueryBuilder(): QueryBuilder
    {
        if (null === $this->qb) {
            $this->createQueryBuilder();
        }

        return $this->qb;
    }

    /**
     * @throws \Exception
     */
    protected function createQueryBuilder(): void
    {
        $namespaceParts = explode('\\', $this->entity);
        $rootAlias = strtolower(substr(array_pop($namespaceParts), 0, 1));

        /** @var EntityRepository $entityRepository */
        $entityRepository = $this->container->get('doctrine')->getRepository($this->entity);

        $this->qb = $entityRepository->createQueryBuilder($rootAlias);
    }

    /**
     * @return SolrQuery
     */
    protected function getSolrQuery(): SolrQuery
    {
        if ($this->translationEntity) {
            $solrQuery = $this->container->get('solr.client')->createQuery($this->translationEntity);
        } else {
            $solrQuery = $this->container->get('solr.client')->createQuery($this->entity);
        }

        $solrQuery->addField('id');

        return $solrQuery;
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function determineRootAlias(): string
    {
        if (!$this->getQueryBuilder()->getDQLPart('select')) {
            throw new \RuntimeException('Select part must be set before doing search');
        }

        /** @var Select $select */
        $select = $this->getQueryBuilder()->getDQLPart('select')[0];

        return $select->getParts()[0];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getResult(): array
    {
        return $this->getQueryBuilder()->getQuery()->getResult();
    }

    /**
     * @param array|null $results
     * @throws \Exception
     */
    protected function applyResults($results): void
    {
        if (empty($results)) {
            // No results from solr, make sure the query builder will never return any result
            $this->getQueryBuilder()->andWhere('1 = 0');
        } else {
            $ids = array_map(function ($result) {
                /** @var Translation|EntityInterface $result */
                if ($this->translationEntity) {
                    $result = $result->getTranslatable();
                }

                if(null === $result) {
                    return null;
                }

                return $result->getId();
            }, $results);

            $ids = array_filter($ids);

            $alias = $this->determineRootAlias();

            $this->getQueryBuilder()->andWhere($this->getQueryBuilder()->expr()->in($alias . '.id', $ids));
        }
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale): void
    {
        $this->locale = $locale;
    }

    /**
     * @return string
     */
    protected function getLocale(): string
    {
        if (!$this->locale) {
            $this->locale = $this->container->get('translator')->getLocale();
        }

        return $this->locale;
    }

    /**
     * @param string|array $keys
     * @param string       $q
     * @param int          $limit
     * @return $this
     * @throws \Exception
     */
    public function searchBy($keys, $q, $limit = null): self
    {
        $keys = (array)$keys;
        $q = str_replace(' ', '\ ', trim($q));

        $query = $this->generateQuery($keys, $q);

        if ($this->translationEntity) {
            $query .= ' AND (locale_t:' . $this->getLocale() . ')';
        }

        $solrQuery = $this->getSolrQuery();

        $solrQuery->setCustomQuery($query);
        $solrQuery->setRows($limit ?: $this->defaultLimit);

        $this->applyResults($solrQuery->getResult());

        return $this;
    }

    /**
     * @param array  $keys
     * @param string $q
     * @return string
     * @throws \Exception
     */
    private function generateQuery(array $keys, $q): string
    {
        $query = [];

        foreach ($keys as $key => $type) {
            if (\is_int($key)) {
                $key = $type;
                $type = 'text';
            }

            /** See \FS\SolrBundle\Doctrine\Annotation\Field::TYP_SIMPLE_MAPPING for all available types */
            switch ($type) {
                case 'text':
                    $query[] = sprintf('%s_%s:*%s*', $key, 't', $q);
                    break;
                default:
                    throw new \RuntimeException(sprintf("Type '%s' not implemented", $type));
            }
        }

        $query = implode(' OR ', $query);

        return $query;
    }
}
