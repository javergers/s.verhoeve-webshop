<?php

namespace AppBundle\Services\TempAPI;

use AppBundle\Entity\Order\Collo;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Order\OrderDeliveryStatus;
use AppBundle\Exceptions\OrderCollectionNotFoundException;
use AppBundle\Exceptions\OrderNotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OrderStatusService
 * @package AppBundle\Services\TempAPI
 */
class OrderStatusService
{
    /** @var ManagerRegistry */
    private $managerRegistry;

    /**
     * OrderStatusService constructor.
     * @param ManagerRegistry $managerRegistry
     */
    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @param string|null $json
     * @return bool
     * @throws OrderCollectionNotFoundException
     * @throws OrderNotFoundException
     * @throws OptimisticLockException
     */
    public function process(string $json = null): bool
    {
        $data = $this->transformJsonData($json);

        /** @var EntityManagerInterface $em */
        $em = $this->managerRegistry->getManager();

        if (null !== $data) {
            foreach ($data as $orderRef => $rawColli) {

                $orderCollection = $this->managerRegistry->getRepository(OrderCollection::class)->findOneBy(['number' => $orderRef]);

                $orderNumber = explode('-', $orderRef);

                if ($orderCollection === null) {
                    throw new OrderCollectionNotFoundException('OrderCollection not found.');
                }

                $order = $this->managerRegistry->getRepository(Order::class)->findOneBy([
                    'orderCollection' => $orderCollection->getId(),
                    'number' => end($orderNumber),
                ]);

                $order->setStatus($this->managerRegistry->getRepository(OrderStatus::class)->find('complete'));
                $order->setDeliveryStatus($this->managerRegistry->getRepository(OrderDeliveryStatus::class)->find('delivered'));

                if (!$order) {
                    throw new OrderNotFoundException('Order not found.');
                }

                foreach ($rawColli as $rawCollo) {
                    // Validate order data throws exception on fail.
                    $this->validateData($rawCollo);

                    $exist = $this->managerRegistry->getRepository(Collo::class)->findOneBy([
                        'code' => $rawCollo['barcode'],
                    ]);

                    if ($exist) {
                        continue;
                    }

                    $newCollo = new Collo();
                    $newCollo->setCode($rawCollo['barcode']);
                    $newCollo->setOrder($order);

                    $order->addCollo($newCollo);

                    $em->persist($newCollo);
                }
            }

            $em->flush();

            return true;
        }

        return false;
    }

    /**
     * @param $data
     *
     * @return object
     */
    private function validateData(array $data): object
    {
        $rawOrderResolver = new OptionsResolver();
        $rawOrderResolver->setRequired([
            'id',
            'order_id',
            'barcode',
            'order_tg_id',
            'trace_url',
        ]);

        return (object)$rawOrderResolver->resolve($data);
    }

    /**
     * @param string $data
     *
     * @return array
     */
    private function transformJsonData(string $data): array
    {
        $data = json_decode($data, true);

        if($data) {
            return (array)$data;
        }

        return null;
    }
}