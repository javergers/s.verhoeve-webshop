<?php

namespace AppBundle\Services;

use AppBundle\Entity\Catalog\Product\Combination;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductTransportType;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Interfaces\Sales\OrderInterface;
use AppBundle\Interfaces\Sales\OrderLineInterface;
use AppBundle\Manager\Catalog\Product\ProductTransportTypeManager;
use function array_merge;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class TransportTypeHelper
 * @package AppBundle\Services
 */
class TransportTypeHelper
{
    /**
     * @var ProductTransportTypeManager
     */
    private $productTransportTypeManager;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * TransportTypeHelper constructor.
     *
     * @param ProductTransportTypeManager $productTransportTypeManager
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        ProductTransportTypeManager $productTransportTypeManager,
        EntityManagerInterface $entityManager
    ) {
        $this->productTransportTypeManager = $productTransportTypeManager;
        $this->entityManager = $entityManager;
    }

    /**
     * @param OrderInterface $order
     *
     * @return TransportType|null
     * @throws \Exception
     */
    public function getTransportType(OrderInterface $order)
    {
        $fillGrades = [];
        $transportTypes = $this->getTransportTypes($order);

        /** @var OrderLineInterface $line */
        foreach ($order->getLines() as $line) {
            $mainProduct = $line->getProduct();

            if ($mainProduct instanceof CompanyProduct) {
                $mainProduct = $mainProduct->getRelatedProduct();
            }

            if (!$mainProduct instanceof GenericProduct) {
                continue;
            }

            $fillGrades = $this->setFillGrades($mainProduct, $fillGrades, $line->getQuantity());
        }

        $fillGrades = array_filter($fillGrades, function ($fillGrade) {
            return !(\is_float($fillGrade) && $fillGrade > 100);
        });

        reset($fillGrades);

        $transportTypes = $transportTypes->filter(function (TransportType $transportType) use ($fillGrades) {
            return array_key_exists($transportType->getId(), $fillGrades);
        });

        $transportTypesIterator = $transportTypes->getIterator();
        $transportTypesIterator->uasort(function (TransportType $transportType1, TransportType $transportType2) use (
            $fillGrades
        ) {
            return $fillGrades[$transportType2->getId()] <=> $fillGrades[$transportType1->getId()];
        });

        /** @var TransportType[]|ArrayCollection $transportTypes */
        $transportTypes = new ArrayCollection(iterator_to_array($transportTypesIterator));

        if ($transportTypes->isEmpty()) {
            return $this->getDefaultTransportType($order);
        }

        return $transportTypes->current();
    }

    /**
     * @param Product $product
     * @param                    $fillGrades
     * @param                    $lineQuantity
     *
     * @return mixed
     */
    private function setFillGrades(Product $product, $fillGrades, $lineQuantity)
    {
        if ($product->isCombination() && $product->getProductTransportTypes()->isEmpty()) {
            /** @var Combination $combination */
            foreach ($product->getCombinations() as $combination) {
                $fillGrades = $this->setFillGrades($combination->getProduct(), $fillGrades,
                    $combination->getQuantity() * $lineQuantity);
            }
        } else {
            foreach ($this->productTransportTypeManager->getProductTransportTypesForProduct($product) as $productTransportType) {
                $transportTypeId = $productTransportType->getTransportType()->getId();

                if (!array_key_exists($transportTypeId, $fillGrades)) {
                    $fillGrades[$transportTypeId] = null;
                }

                if ($productTransportType->getQuantity()) {
                    $fillGrades[$transportTypeId] += (float)100 / $productTransportType->getQuantity() * $lineQuantity;
                }
            }
        }

        return $fillGrades;
    }

    /**
     * @param OrderInterface $order
     *
     * @return TransportType
     * @throws \RuntimeException
     */
    public function getDefaultTransportType(OrderInterface $order)
    {
        $site = $order->getCollection()->getSite();

        if (!$site->getDefaultProductTransportType()) {
            throw new \RuntimeException('Geen standaard bezorgmethode ingesteld!');
        }

        return $site->getDefaultProductTransportType();
    }

    /**
     * @param OrderInterface $order
     *
     * @return ArrayCollection|TransportType[]
     */
    private function getTransportTypes(OrderInterface $order)
    {
        $transportTypes = new ArrayCollection($this->entityManager->getRepository(TransportType::class)->findAll());
        foreach ($order->getLines() as $line) {
            $mainProduct = $line->getProduct();

            if ($mainProduct instanceof CompanyProduct) {
                $mainProduct = $mainProduct->getRelatedProduct();
            }

            if (!$mainProduct instanceof GenericProduct) {
                continue;
            }

            $productTransportTypes = $this->productTransportTypeManager->getProductTransportTypesForProduct($mainProduct)->map(function (
                ProductTransportType $productTransportType
            ) {
                return $productTransportType->getTransportType();
            });

            //if product is combination and no transporttypes are set on the combination, retrieve all transporttypes from products within combination
            if ($mainProduct->isCombination() && $productTransportTypes->isEmpty()) {
                /** @var Combination $combination */
                foreach ($mainProduct->getCombinations() as $combination) {
                    $productTransportTypes = new ArrayCollection(
                        array_merge($productTransportTypes->toArray(),
                            $this->productTransportTypeManager->getProductTransportTypesForProduct($combination->getProduct())->map(function (
                                ProductTransportType $productTransportType
                            ) {
                                return $productTransportType->getTransportType();
                            })->toArray()
                        )
                    );
                }
            }

            foreach ($transportTypes as $transportType) {
                if (!$productTransportTypes->contains($transportType)) {
                    $transportTypes->removeElement($transportType);
                }
            }
        }

        return $transportTypes;
    }
}
