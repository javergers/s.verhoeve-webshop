<?php

namespace AppBundle\Services;

use AppBundle\Connector\AbstractMailConnector;
use AppBundle\Decorator\Catalog\Product\CompanyProductDecorator;
use AppBundle\Entity\Catalog\Product\CompanyCard;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Geography\Postcode;
use AppBundle\Entity\Order\CartOrder;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyPreferredSupplier;
use AppBundle\Entity\Supplier\DeliveryArea;
use AppBundle\Entity\Supplier\SupplierGroup;
use AppBundle\Entity\Supplier\SupplierGroupProduct;
use AppBundle\Entity\Supplier\SupplierProduct;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Interfaces\Sales\OrderInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

/**
 * Class SupplierManagerService
 * @package AppBundle\Services
 */
class SupplierManagerService
{
    /**
     * @var ConnectorHelper
     */
    private $connectorHelper;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * SupplierManagerService constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager, ConnectorHelper $connectorHelper)
    {
        $this->entityManager = $entityManager;
        $this->connectorHelper = $connectorHelper;
    }

    /**
     * @param OrderInterface $order
     * @param bool $ignoreTransportType
     *
     * @return ArrayCollection|Company[]
     */
    public function getAvailableSuppliers(OrderInterface $order, $ignoreTransportType = false): ArrayCollection
    {
        /**
         * @var ArrayCollection $suppliers
         */
        $suppliers = null;

        foreach ($order->getLines() as $orderLine) {
            $product = $orderLine->getProduct();
            $productSuppliers = $this->getAvailableSuppliersForProduct($product);

            if ($productSuppliers->isEmpty()) {
                if (null !== $suppliers) {
                    $suppliers->clear();
                }
                break;
            }

            if ($suppliers === null) {
                $suppliers = $productSuppliers;
            } else {
                foreach ($suppliers as $supplier) {
                    if (!$productSuppliers->contains($supplier)) {
                        $suppliers->removeElement($supplier);
                    }
                }
            }

            // If no suppliers are available at this point, skip checking other products
            if ($suppliers->isEmpty()) {
                return $suppliers;
            }

        }

        if ($suppliers === null) {
            $suppliers = new ArrayCollection();
        }
        return $suppliers;
    }

    /**
     * @param OrderInterface $order
     *
     * @return SupplierGroup
     * @throws DBALException
     */
    public function getSharedSupplierGroup(OrderInterface $order): ?SupplierGroup
    {
        $stmt = $this->entityManager->getConnection()->prepare('
            SELECT sugr.id    
            FROM order_line AS orli            
            INNER JOIN supplier_group_product AS sugp ON sugp.product_id = orli.product_id
            INNER JOIN supplier_group AS sugr ON sugp.supplier_group_id = sugr.id            
            WHERE orli.order_id = :orderId            
            GROUP BY sugr.id            
            HAVING COUNT(*) = (
                SELECT COUNT(orli.id) AS count
                FROM order_line AS orli
                WHERE orli.order_id = :orderId
            )
        ');

        $stmt->execute([
            'orderId' => $order->getId(),
        ]);

        $supplierGroupId = $stmt->fetchColumn();
        if (false === $supplierGroupId) {
            return null;
        }

        return $this->entityManager->getRepository(SupplierGroup::class)->find($supplierGroupId);
    }

    /**
     * @param OrderInterface $order
     *
     * @return ArrayCollection|Company[]
     */
    public function getAvailableSuppliersForDelivery(OrderInterface $order)
    {
        $suppliers = $this->getAvailableSuppliers($order);

        if (null !== $order->getPickupAddress()) {
            /** @var QueryBuilder $qb */
            $qb = $this->entityManager->getRepository(Company::class)->createQueryBuilder('company');
            $qb->leftJoin('company.establishments', 'establishment')
                ->andWhere('establishment.address = :address')
                ->setParameter('address', $order->getPickupAddress()->getId());

            $supplierQueryResult = $qb->getQuery()->getResult();
            $supplier = !empty($supplierQueryResult) ? current($supplierQueryResult) : null;
            if (null !== $supplier && $suppliers->contains($supplier)) {
                return new ArrayCollection([$supplier]);
            }

            return new ArrayCollection();
        }

        // Filter suppliers without a connector
        /** @var ArrayCollection $suppliers */
        $suppliers = $suppliers->filter(function (Company $supplier) {
            return (bool)$this->connectorHelper->getConnector($supplier);
        });

        $suppliersWithinDeliveryArea = $this->getSuppliersWithinDeliveryArea($order);

        $suppliers = new ArrayCollection(array_uintersect($suppliers->toArray(), $suppliersWithinDeliveryArea,
            function (Company $a, Company $b) {
                return $a->getId() <=> $b->getId();
            }));

        $suppliers = $this->sortSuppliers($suppliers, $order);

        return $suppliers;
    }

    /**
     * @param OrderInterface $order
     *
     * @return Company[]
     */
    private function getSuppliersWithinDeliveryArea(OrderInterface $order): array
    {
        $country = $order->getDeliveryAddressCountry()->getCode();
        $postcode = $order->getDeliveryAddressPostcode();

        $postcode = $this->entityManager->getRepository(Postcode::class)->findOneByCountryAndPostcode($country,
            $postcode);

        return array_merge(
            $postcode === null ? [] : $this->entityManager->getRepository(Company::class)->findSuppliersForPostcode($postcode),
            $this->entityManager->getRepository(Company::class)->findSuppliersWithoutGroup()
        );
    }

    /**
     * @param Product $product
     *
     * @return ArrayCollection|Company[]
     */
    public function getAvailableSuppliersForProduct(ProductInterface $product)
    {
        if ($product instanceof CompanyProductDecorator) {
            $product = $product->getEntity();
        }

        if ($product instanceof CompanyProduct) {
            $product = $product->getRelatedProduct();
        }

        if ($product instanceof CompanyCard) {
            $product = $product->getParent();
        }

        if ($product->isCombination()) {
            $allSuppliers = [];
            $suppliers = new ArrayCollection();

            foreach ($product->getCombinations() as $combinationProduct) {
                /** @var Company[] $availableSuppliers */
                $availableSuppliers = $this->getAvailableSuppliersForProduct($combinationProduct->getProduct());

                foreach ($availableSuppliers as $supplier) {
                    if (!isset($allSuppliers[$supplier->getId()])) {
                        $allSuppliers[$supplier->getId()] = $supplier;
                    } else {
                        if (!$suppliers->contains($supplier)) {
                            $suppliers->add($supplier);
                        }
                    }
                }
            }
        } elseif (!$product instanceof ProductCard && !$product->getVariations()->isEmpty()) {
            $suppliers = new ArrayCollection();

            foreach ($product->getVariations() as $variation) {
                $variationSuppliers = $this->getAvailableSuppliersForProduct($variation);

                foreach ($variationSuppliers as $variationSupplier) {
                    if (!$suppliers->contains($variationSupplier)) {
                        $suppliers->add($variationSupplier);
                    }
                }
            }
        } else {
            $suppliers = new ArrayCollection();
            foreach ($product->getSupplierProducts() as $supplierProduct) {
                $supplier = $supplierProduct->getSupplier();

                if (!$supplier || !$supplier->getIsSupplier() || null === $supplier->getSupplierConnector() || $suppliers->contains($supplier)) {
                    continue;
                }

                $suppliers->add($supplier);
            }

            foreach ($product->getSupplierGroupProducts() as $supplierGroupProduct) {
                foreach ($supplierGroupProduct->getSupplierGroup()->getSuppliers() as $supplier) {
                    if (!$supplier || !$supplier->getIsSupplier() || null === $supplier->getSupplierConnector() || $suppliers->contains($supplier)) {
                        continue;
                    }

                    $suppliers->add($supplier);
                }
            }
        }

        return $suppliers;
    }

    /**
     * @param ProductInterface $product
     *
     * @return bool
     */
    public function hasVariationWithSupplier(ProductInterface $product)
    {
        if (false !== $product->getVariations()->isEmpty()) {
            return false;
        }

        return $product->getVariations()->exists(function ($key, Product $variation) {
            return false === $this->getAvailableSuppliersForProduct($variation)->isEmpty();
        });
    }

    /**
     * @param Company $supplier
     * @param OrderInterface $order
     *
     * @return DeliveryArea|null
     */
    public function getSupplierDeliveryArea(Company $supplier, OrderInterface $order): ?DeliveryArea
    {
        $deliveryAreas = $supplier->getDeliveryAreas()->filter(function (DeliveryArea $deliveryArea) use ($order) {
            if ($deliveryArea->getPostcode()->getCountry() !== $order->getDeliveryAddressCountry()) {
                return false;
            }

            if ((int)$deliveryArea->getPostcode()->getPostcode() !== (int)$order->getDeliveryAddressPostcode()) {
                return false;
            }

            return true;
        });

        if ($deliveryAreas->isEmpty()) {
            return null;
        }

        return $deliveryAreas->first();
    }

    /**
     * @param Company[]|ArrayCollection $suppliers
     * @param OrderInterface $order
     *
     * @return ArrayCollection
     */
    private function sortSuppliers(ArrayCollection $suppliers, OrderInterface $order): ArrayCollection
    {
        $preferredSuppliers = new ArrayCollection();

        foreach ($suppliers as $supplier) {
            $supplierDeliveryArea = $this->getSupplierDeliveryArea($supplier, $order);

            if ($supplierDeliveryArea) {
                $companyPreferredSuppliers = $supplierDeliveryArea->getPreferredSuppliers();
                $companyPreferredSuppliers = $companyPreferredSuppliers->filter(function (
                    CompanyPreferredSupplier $companyPreferredSupplier
                ) use ($order) {
                    if ($companyPreferredSupplier->getCompany() === null) {
                        return true;
                    }

                    $orderCompany = $this->getOrderCompany($order);

                    if ($orderCompany === null) {
                        return false;
                    }

                    if ($companyPreferredSupplier->getCompany()->getId() === $orderCompany->getId()) {
                        return true;
                    }

                    return false;
                });

                foreach ($companyPreferredSuppliers as $companyPreferredSupplier) {
                    $preferredSuppliers->add($companyPreferredSupplier);
                }
            }
        }

        $iterator = $preferredSuppliers->getIterator();
        $iterator->uasort(function (
            CompanyPreferredSupplier $companyPreferredSupplier1,
            CompanyPreferredSupplier $companyPreferredSupplier2
        ) {
            $connector1 = $this->connectorHelper->getConnector($companyPreferredSupplier1->getDeliveryArea()->getCompany());
            $connector2 = $this->connectorHelper->getConnector($companyPreferredSupplier2->getDeliveryArea()->getCompany());

            $result = $connector1 instanceof AbstractMailConnector <=> $connector2 instanceof AbstractMailConnector;

            if ($result !== 0) {
                return $result;
            }

            $result = (null !== $companyPreferredSupplier1->getCompany()) <=> (null !== $companyPreferredSupplier2->getCompany());

            if ($result !== 0) {
                return $result;
            }

            $result = $companyPreferredSupplier1->getPosition() <=> $companyPreferredSupplier2->getPosition();

            if ($result !== 0) {
                return $result;
            }

            return $companyPreferredSupplier1->getDeliveryArea()->getDeliveryPrice() <=> $companyPreferredSupplier2->getDeliveryArea()->getDeliveryPrice();
        });

        /** @var CompanyPreferredSupplier[] $preferredSuppliers */
        $preferredSuppliers = iterator_to_array($iterator);

        $suppliersNew = new ArrayCollection();

        foreach ($preferredSuppliers as $preferredSupplier) {
            $supplier = $preferredSupplier->getDeliveryArea()->getCompany();

            if ($suppliersNew->contains($supplier)) {
                continue;
            }

            $suppliersNew->add($supplier);
        }

        $remainingSuppliers = $suppliers->filter(function (Company $remainingSupplier) use ($suppliersNew) {
            return !$suppliersNew->contains($remainingSupplier);
        });

        $iterator = $remainingSuppliers->getIterator();
        $iterator->uasort(function (Company $company1, Company $company2) use ($order) {
            $connector1 = $this->connectorHelper->getConnector($company1);
            $connector2 = $this->connectorHelper->getConnector($company2);

            $sortIndex1 = implode('|', [
                $connector1 instanceof AbstractMailConnector ? '-' : '+',
                $this->getSupplierDeliveryAreaPrice($company1, $order),
            ]);

            $sortIndex2 = implode('|', [
                $connector2 instanceof AbstractMailConnector ? '-' : '+',
                $this->getSupplierDeliveryAreaPrice($company2, $order),
            ]);

            return $sortIndex1 <=> $sortIndex2;
        });

        $remainingSuppliers = iterator_to_array($iterator);

        foreach ($remainingSuppliers as $remainingSupplier) {
            $suppliersNew->add($remainingSupplier);
        }

        return $suppliersNew;
    }

    /**
     * @param OrderInterface|CartOrder|Order $order
     *
     * @return Company|null
     */
    private function getOrderCompany(OrderInterface $order): ?Company
    {
        if ($order instanceof CartOrder) {
            return $order->getCart()->getCompany();
        }

        if ($order instanceof Order) {
            return $order->getOrderCollection()->getCompany();
        }

        return null;
    }

    /**
     * @param Company $supplier
     * @param OrderInterface $order
     *
     * @return float|null
     */
    public function getSupplierDeliveryAreaPrice(Company $supplier, OrderInterface $order): ?float
    {
        $supplierDeliveryArea = $this->getSupplierDeliveryArea($supplier, $order);

        if (null !== $supplierDeliveryArea) {
            return $supplierDeliveryArea->getDeliveryPrice();
        }

        return null;
    }

    /**
     * @param Product $product
     * @param Company $supplier
     *
     * @return SupplierProduct|SupplierGroupProduct
     *
     *                   // TODO VALIDATE ALL CALLS
     */
    public function getSupplierProduct(Product $product, Company $supplier)
    {
        if (!$product->getPurchasable() && !$product->getSupplierProducts()->count() && !$product->getSupplierGroupProducts()->count()) {
            return null;
        }

        $supplierProduct = $product->getSupplierProducts()->filter(function (SupplierProduct $supplierProduct) use (
            $supplier
        ) {
            return !($supplierProduct->getSupplier() !== $supplier);
        })->current();

        if (!$supplierProduct) {
            $supplierProduct = $product->getSupplierGroupProducts()->filter(function (
                SupplierGroupProduct $supplierGroupProduct
            ) use ($supplier) {
                $contains = false;

                foreach ($supplierGroupProduct->getSupplierGroup()->getSuppliers() as $supplierGroupSupplier) {
                    if ($supplierGroupSupplier->getId() === $supplier->getId()) {
                        $contains = true;
                    }
                }

                if (!$contains) {
                    return false;
                }

                return true;
            })->current();
        }

        if (!$supplierProduct) {
            throw new \RuntimeException(vsprintf("Leverancier '%s' niet gekoppeld aan '%s'", [
                $supplier->getName(),
                $product->translate()->getTitle(),
            ]));
        }

        return $supplierProduct;
    }

    /**
     * @param Product $product
     * @param Product $combine
     *
     * @return bool
     */
    public function canCombineProductWith(ProductInterface $product, ProductInterface $combine): bool
    {
        $productSuppliers = $this->getAvailableSuppliersForProduct($product);
        $combineSuppliers = $this->getAvailableSuppliersForProduct($combine);

        if (!$combineSuppliers->isEmpty()) {
            foreach ($combineSuppliers as $supplier) {
                if ($productSuppliers->contains($supplier)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param Order $order
     *
     * @return bool
     */
    public function hasAvailableSuppliersForOrder(Order $order)
    {
        $availableSuppliers = [];
        foreach ($order->getLines() as $line) {
            $suppliers = $this->retrieveAvailableSuppliersForProduct($line->getProduct());

            if (empty($suppliers)) {
                return false;
            }

            if (empty($availableSuppliers)) {
                $availableSuppliers = $suppliers;
                continue;
            }

            $availableSuppliers = array_intersect($availableSuppliers, $suppliers);

            if (empty($availableSuppliers)) {
                return false;
            }
        }

        return !empty($availableSuppliers);
    }

    /**
     * @param ProductInterface $product
     *
     * @return array
     */
    private function retrieveAvailableSuppliersForProduct(ProductInterface $product)
    {
        if ($product instanceof CompanyProductDecorator) {
            $product = $product->getEntity();
        }

        if ($product instanceof CompanyProduct) {
            $product = $product->getRelatedProduct();
        }

        if ($product instanceof CompanyCard) {
            $product = $product->getParent();
        }

        if ($product->isCombination()) {
            $availableSuppliers = [];

            foreach ($product->getCombinations() as $combinationProduct) {
                $suppliers = $this->retrieveAvailableSuppliersForProduct($combinationProduct->getProduct());

                if (empty($suppliers)) {
                    return [];
                }

                if (empty($availableSuppliers)) {
                    $availableSuppliers = $suppliers;
                    continue;
                }

                $availableSuppliers = array_intersect($availableSuppliers, $suppliers);

                if (empty($availableSuppliers)) {
                    return [];
                }
            }

            return $availableSuppliers;
        } elseif (!$product instanceof ProductCard && !$product->getVariations()->isEmpty()) {
            $suppliers = [];

            foreach ($product->getVariations() as $variation) {
                $availableSuppliers = $this->retrieveAvailableSuppliersForProduct($variation);

                foreach ($availableSuppliers as $variationSupplier) {
                    if (!isset($suppliers[$variationSupplier])) {
                        $suppliers[$variationSupplier] = $suppliers;
                    }
                }
            }

            return $suppliers;
        } else {
            $qb = $this->entityManager->getRepository(Company::class)->createQueryBuilder('supplier');
            $qb->select('supplier.id')
                ->leftJoin('supplier.supplierProducts', 'supplierProducts')
                ->leftJoin('supplier.supplierGroups', 'supplierGroups')
                ->leftJoin('supplierGroups.supplierGroupProducts', 'supplierGroupProducts')
                ->andWhere('supplier.isSupplier = true');

            $orX = $qb->expr()->orX(
                $qb->expr()->eq('supplierGroupProducts.product', $product->getId()),
                $qb->expr()->eq('supplierProducts.product', $product->getId())
            );
            $qb->andWhere($orX);
            $qb->groupBy('supplier.id');

            $suppliers = array_map(function ($value) {
                return $value['id'];
            }, $qb->getQuery()->getArrayResult());

            return $suppliers;
        }
    }
}
