<?php

namespace AppBundle\Services\Mailer;

use AppBundle\Entity\Site\Site;
use AppBundle\Interfaces\Mailer\MailerInterface;
use Psr\Log\LoggerInterface;
use Swift_Mailer;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Twig\Error\Error;

/**
 * Class AbstractMailer
 * @package AppBundle\Services\Mailer
 */
abstract class AbstractMailer implements MailerInterface
{
    /**
     * @var Swift_Mailer
     */
    protected $mailer;

    /**
     * @var TwigEngine
     */
    protected $twig;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * AbstractMailer constructor.
     * @param Swift_Mailer        $mailer
     * @param TwigEngine          $twig
     * @param TranslatorInterface $translator
     * @param RouterInterface     $router
     * @param LoggerInterface     $logger
     */
    public function __construct(
        Swift_Mailer $mailer,
        TwigEngine $twig,
        TranslatorInterface $translator,
        RouterInterface $router,
        LoggerInterface $logger
    ){
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->translator = $translator;
        $this->router = $router;
        $this->logger = $logger;
    }

    /**
     * @return OptionsResolver
     */
    protected function getOptionsResolver(): OptionsResolver
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults([
            'utm' => [],
        ]);

        return $resolver;
    }

    /**
     * @param array $parameters
     *
     * @return array
     */
    public function resolveParameters(array $parameters): array
    {
        $resolver = $this->getOptionsResolver();

        return $resolver->resolve($parameters);
    }

    /**
     * @param Site $site
     * @param      $parameters
     * @return mixed
     */
    protected function defineBrandParams(Site $site, $parameters)
    {
        $theme = $site->getTheme();

        switch ($theme) {
            case 'topfruit':
                $parameters['primaryColor'] = '#919B16';
                $parameters['brandName'] = 'Topfruit.nl';
                break;
            case 'toptaarten':
                $parameters['primaryColor'] = '#d77710';
                $parameters['brandName'] = 'Toptaarten.nl';
                break;
            case 'topbloemen':
                $parameters['primaryColor'] = '#886d4f';
                $parameters['brandName'] = 'Topbloemen.nl';
                break;
            default:
                $parameters['primaryColor'] = '#b5251a';
                $parameters['brandName'] = 'Topgeschenken.nl';
        }

        $parameters['brandUrl'] = 'https://topgeschenken.nl';
        if ($site->getPrimaryDomain()) {
            $parameters['brandUrl'] = $site->getPrimaryDomain()->getUri();
        }

        return $parameters;
    }

    /**
     * @param               $template
     * @param array         $parameters
     * @param callable|null $callback
     * @return bool
     * @throws \Exception
     */
    public function sendEmailMessage($template, $parameters, callable $callback = null): bool
    {
        /* @var Site $site */
        $site = $parameters['site'];

        $parameters = $this->defineBrandParams($site, $parameters);

        $html = $this->render($template, $parameters);

        $subject = $this->getTitleFromRenderedTemplate($html);

        $clientServiceLabel = $this->translator->trans('customer_service');

        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom($site->getEmail(), $clientServiceLabel . ' | ' . ucfirst($parameters['brandName']))
            ->setBody($html, 'text/html');

        $message->setTo($parameters['to']);

        if (!empty($parameters['bcc'])) {
            $message->setBcc($parameters['bcc']);
        }

        if (!empty($parameters['attachments'])) {
            foreach ($parameters['attachments'] as $attachment) {
                $message->attach($attachment);
            }
        }

        if ($callback) {
            $result = $callback($message);

            if (!\is_bool($result)) {
                throw new \RuntimeException('Callback must return a boolean');
            }

            if (!$result) {
                return false;
            }
        }

        return $this->mailer->send($message);
    }

    /**
     * @param array         $parameters
     * @param callable|null $callback
     * @return bool
     */
    abstract public function send(array $parameters, callable $callback = null): bool;

    /**
     * @param       $template
     * @param array $parameters
     *
     * @return string
     *
     * @throws Error
     */
    private function render($template, $parameters)
    {
        return $this->twig->render($template, $parameters);
    }

    /**
     * @param $html
     * @return string
     */
    private function getTitleFromRenderedTemplate($html): string
    {
        $res = preg_match("/<title>(.*)<\\/title>/siU", $html, $title);

        if (!$res) {
            return '';
        }

        return $title[1];
    }

    /**
     * @param Site   $site
     *
     * @param string $name
     * @return string
     */
    public function determineTemplate(Site $site, $name = ''): string
    {
        $theme = $site->getTheme();

        $template = sprintf('@AppBundle/Resources/views/Emails/%s.html.twig', $name);
        $themeTemplate = sprintf('@AppBundle/Resources/themes/%s/Emails/%s-site.html.twig', $theme, $name);

        if ($this->twig->exists($themeTemplate)) {
            $template = $themeTemplate;
        }

        return $template;
    }
}
