<?php

namespace AppBundle\Services\Mailer;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Services\Sales\Order\Invoice;
use Exception;
use Psr\Log\LoggerInterface;
use Swift_Attachment;
use Swift_Mailer;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class OrderCancellationMailer
 * @package AppBundle\Services\Mailer
 */
class OrderCancellationMailer extends AbstractMailer
{
     /**
     * @param               $parameters
     * @param callable|null $callback
     * @return bool
     * @throws Exception
     */
    public function send(array $parameters, callable $callback = null): bool
    {
        if (PHP_SAPI !== 'cli' && !isset($parameters['preview'])) {
            throw new \LogicException('Should always be done via command.');
        }

        $parameters = $this->resolveParameters($parameters);

        /** @var OrderCollection $orderCollection */
        $orderCollection = $parameters['orderCollection'];

        // Translate mail/assets into user defined locale.
        $locale = $orderCollection->getLocale();
        $this->translator->setLocale($locale);
        $site = $orderCollection->getSite();
        $to = $orderCollection->getCustomer()->getEmail();
        $template = $this->determineTemplate($site, 'order-cancellation');
        $utm = [
            'utm_campaign' => 'orderannulering',
        ];

        $parameters['to'] = $to;
        $parameters['site'] = $site;
        $parameters['locale'] = $locale;
        $parameters['utm'] = $utm;

        return $this->sendEmailMessage($template, $parameters, $callback);
    }


    /**
     * @param $parameters
     * @return array
     */
    public function resolveParameters(array $parameters): array
    {
        $resolver = $this->getOptionsResolver();

        $resolver
            ->setDefaults([
                'preview' => false
            ])
            ->setRequired([
                'orderCollection'
            ])
            ->setAllowedTypes(
                'orderCollection',
                [OrderCollection::class]
            );

        return $resolver->resolve($parameters);
    }
}
