<?php

namespace AppBundle\Services\Mailer;


use AppBundle\Entity\Payment\Payment;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class PaymentRequestMailer
 * @package AppBundle\Services\Mailer
 */
class PaymentRequestMailer extends AbstractMailer
{
    /**
     * @param               $parameters
     * @param callable|null $callback
     *
     * @return bool
     * @throws \Exception
     */
    public function send(array $parameters, callable $callback = null): bool
    {
        $parameters = $this->resolveParameters($parameters);

        /** @var Payment $payment */
        $payment = $parameters['payment'];
        $orderCollection = $payment->getOrderCollection();
        $site = $orderCollection->getSite();
        $locale = $orderCollection->getLocale();
        $this->translator->setLocale($locale);

        $metadata = $payment->getMetadata();

        $template = $this->determineTemplate($site, 'payment-request');

        $url = $site->getUrl() . $this->router->generate(
            'app_payment_paymentlink',
            ['payment' => $payment->getUuid()],
            UrlGeneratorInterface::ABSOLUTE_PATH
        );

        $parameters['to'] = [$metadata['request']['email'] => $metadata['request']['name']];
        $parameters['site'] = $site;
        $parameters['locale'] = $locale;
        $parameters['orderCollection'] = $orderCollection;
        $parameters['metadata'] = $metadata;
        $parameters['url'] = $url;
        $parameters['utm'] = ['utm_campaign' => 'paymentrequest'];
        $this->defineBrandParams($site, $parameters);

        return $this->sendEmailMessage($template, $parameters, $callback);
    }

    /**
     * @param array $parameters
     *
     * @return array
     */
    public function resolveParameters(array $parameters): array
    {
        $resolver = $this->getOptionsResolver();
        $resolver->setRequired([
            'payment',
        ]);

        return $resolver->resolve($parameters);
    }
}
