<?php

namespace AppBundle\Services\Mailer;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Site;
use AppBundle\Exceptions\PasswordResetException;
use AppBundle\Interfaces\Mailer\MailerInterface;
use AppBundle\Services\Mailer;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class PasswordResetMailer
 * @package AppBundle\Services\Mailer
 */
class PasswordResetMailer extends AbstractMailer
{
    /**
     * @param               $parameters
     * @param callable|null $callback
     * @return bool
     * @throws \Exception
     */
    public function send(array $parameters, callable $callback = null): bool
    {
        $parameters = $this->resolveParameters($parameters);

        /** @var Customer $customer */
        $customer = $parameters['customer'];

        /** @var Site $site */
        $site = $parameters['site'];

        // Translate mail/assets into user defined locale.
        $locale = $customer->getLocale() ?: 'nl_NL';
        $this->translator->setLocale($locale);

        $parameters['to'] = [$customer->getEmail() => $customer->getFullname()];
        $parameters['locale'] = $locale;
        $parameters['url'] = $this->generateUrl($customer);

        $template = $this->determineTemplate($site, 'reset-password');

        return $this->sendEmailMessage($template, $parameters, $callback);
    }

    /**
     * @param $parameters
     *
     * @return array
     */
    public function resolveParameters(array $parameters): array
    {
        $resolver = $this->getOptionsResolver();

        $resolver->setRequired([
            'customer',
            'site',
        ]);

        $resolver->setAllowedTypes('customer', Customer::class);
        $resolver->setAllowedTypes('site', Site::class);

        return $resolver->resolve($parameters);
    }

    /**
     * @param Customer $customer
     *
     * @return string
     * @throws PasswordResetException
     */
    private function generateUrl(Customer $customer): string
    {
        $token = $customer->getConfirmationToken();

        if(null === $token) {
            throw new PasswordResetException('No password reset token defined');
        }

        return $this->router->generate(
            'app_customer_resetting_reset',
            [
                'token' => $customer->getConfirmationToken(),
            ],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
    }
}