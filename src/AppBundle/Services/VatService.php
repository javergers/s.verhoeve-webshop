<?php

namespace AppBundle\Services;

use AppBundle\DBAL\Types\DisplayPriceType;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class VatService
 * @package AppBundle\Services
 */
class VatService
{

    use ContainerAwareTrait;

    /**
     * @return bool
     */
    public function isVatApplied()
    {

        /** depens on site and user settings */
        $site = null;
        if ($this->container->get('app.domain')->getDomain()) {
            $site = $this->container->get('app.domain')->getDomain()->getSite();
        }

        $priceType = ($site) ? $site->getDefaultDisplayPrice() : DisplayPriceType::INCLUSIVE;

        if ($priceType == DisplayPriceType::INCLUSIVE) {
            return true;
        }

        return false;
    }

}
