<?php

namespace AppBundle\Services;

use AppBundle\Controller\CartController;
use AppBundle\Controller\DataController;
use AppBundle\Controller\HomeController;
use AppBundle\Controller\PreviewController;
use AppBundle\Entity\Site\Site;
use AppBundle\Exceptions\Preview\PreviewInvalidDataException;
use AppBundle\Exceptions\Preview\PreviewNotActiveException;
use AppBundle\Exceptions\Preview\PreviewRequestFailedException;
use AppBundle\Interfaces\Preview\PreviewableEntityInterface;
use AppBundle\Utils\Security\Encryptor;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Proxy\Proxy;
use Exception;
use Knp\DoctrineBehaviors\Model\Translatable\Translatable;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;

/**
 * Class PreviewService
 *
 * @package AppBundle\Services
 */
class PreviewService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var RequestStack */
    private $requestStack;

    /** @var array */
    private $subject = [];

    /** @var HttpKernel */
    private $httpKernel;

    /** @var Router */
    private $router;

    /** @var array */
    private $doctrineFilters = ['publishable', 'accessible'];

    /** @var Slug */
    private $slugService;

    /** @var Encryptor */
    private $encryptor;

    /**
     * PreviewService constructor.
     *
     * @param RequestStack  $requestStack
     * @param EntityManagerInterface $entityManager
     * @param HttpKernel    $httpKernel
     * @param Router        $router
     * @param Slug          $slugService
     * @param Encryptor     $encryptor
     */
    public function __construct(
        RequestStack $requestStack,
        EntityManagerInterface $entityManager,
        HttpKernel $httpKernel,
        Router $router,
        Slug $slugService,
        Encryptor $encryptor
    ) {
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
        $this->httpKernel = $httpKernel;
        $this->router = $router;
        $this->slugService = $slugService;
        $this->encryptor = $encryptor;
    }

    /**
     * Set the preview subject and store it in the preview session
     *
     * @param string      $class
     * @param int         $identifier
     * @param string|null $adminUri
     *
     * @return $this
     */
    public function setSubject(string $class, int $identifier, string $adminUri = null)
    {
        $this->subject = [
            'class' => $class,
            'identifier' => $identifier,
            'admin_url' => $adminUri,
        ];

        $this->storeSubjectInSession();

        return $this;
    }

    /**
     * Get the subject from the preview session
     *
     * @return array
     *
     * @throws PreviewNotActiveException
     */
    public function getSubject()
    {
        return $this->getSession()->get('preview')['subject'];
    }

    /**
     * Get the admin uri from the stored preview session
     *
     * @return null|string
     *
     * @throws PreviewNotActiveException
     */
    public function getAdminUrl()
    {
        if (!$this->isActive()) {
            throw new PreviewNotActiveException();
        }

        return $this->getSubject()['admin_url'];
    }

    /**
     * Returns the attributes from the preview session
     *
     * @return array
     * @throws PreviewNotActiveException
     */
    public function getAttributes()
    {
        if (!$this->isActive()) {
            throw new PreviewNotActiveException();
        }

        return $this->getSession()->get('preview')['attributes'];
    }

    /**
     * Create the preview instance
     *
     * @return $this
     */
    public function create()
    {
        $subject = $this->getSubject();

        $this->checkPrerequisites($subject['class'], $subject['identifier']);

        return $this;
    }

    /**
     * Destroys the preview data in the session
     *
     * @return string
     * @throws Exception
     */
    public function destroy()
    {
        if (!(bool)$this->getSession()->remove('preview')) {
            throw new Exception("Destroying of the preview session failed");
        }

        return $this->router->generate('app_home_index');
    }

    /**
     * Execute the preview request
     *
     * @return string
     *
     * @throws PreviewRequestFailedException
     */
    public function request()
    {
        // Destroy any previous sessions
        $this->destroy();

        $request = $this->getRequest();

        $previewSessionArgument = $request->query->get('preview_session');
        $previewSessionResult = null;

        if ($previewSessionArgument === null) {
            throw new PreviewRequestFailedException('Missing mandatory session data');
        }

        $previewSessionDecrypted = $this->encryptor->decrypt(rawurldecode($previewSessionArgument));

        if (($previewSessionResult = @unserialize($previewSessionDecrypted, [])) === false) {
            throw new PreviewRequestFailedException();
        }

        $this->setSubject($previewSessionResult['class'], $previewSessionResult['identifier'],
            $previewSessionResult['admin_url'])->create();

        $url = $this->getRedirectAfterRequestUrl($previewSessionResult['class'], $previewSessionResult['identifier'],
            $request->get('_locale'), $previewSessionResult['siteId']);

        if ($url === null) {
            throw new PreviewRequestFailedException();
        }

        $qsConnector = '?';

        if (strpos($url, '?') !== false) {
            $qsConnector = '&';
        }

        return $url . $qsConnector . 'preview=true';
    }

    /**
     * Returns the request url
     *
     * @param string $url
     * @param array  $parameters
     *
     * @return string
     */
    public function getRequestUrl(string $url, array $parameters)
    {
        $requestSession = $this->encryptor->encrypt(serialize($parameters));

        return $url . '?preview=true&action=request&preview_session=' . rawurlencode($requestSession);
    }

    /**
     * Get the url that is needed for redirecting to the preview
     * after successfully creating the preview session
     *
     * @param string $class
     * @param int    $identifier
     * @param string $locale
     * @param int    $siteId
     * @param int    $referenceType
     *
     * @return null|string
     *
     * @internal param $entity
     * @internal Site $site
     */
    private function getRedirectAfterRequestUrl(
        string $class,
        int $identifier,
        string $locale = null,
        int $siteId,
        int $referenceType = UrlGeneratorInterface::ABSOLUTE_URL
    ) {
        $entity = $this->entityManager->getRepository($class)->find($identifier);
        $site = $this->entityManager->getRepository(Site::class)->find($siteId);

        $url = $this->slugService->getUrl($entity, $locale, $site, $referenceType);

        return $url;
    }

    /**
     * Returns whether the preview mode is active
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->getSession()->has('preview')
            && array_key_exists('attributes', $this->getSession()->get('preview'));
    }

    /**
     * @param ControllerResolverInterface $resolver
     * @param FilterControllerEvent       $event
     *
     * @return callable|false
     */
    public function getPreviewController(ControllerResolverInterface $resolver, FilterControllerEvent $event)
    {
        $sessionData = $this->getSession()->get('preview', []);
        $sessionData['enabled'] = true;
        $sessionData['attributes'] = $event->getRequest()->attributes->all();

        $this->getSession()->set('preview', $sessionData);

        $subRequest = $event->getRequest()->duplicate(null, null, [
            '_controller' => 'AppBundle:Preview:index',
        ]);

        $controller = $resolver->getController($subRequest);

        return $controller;
    }

    /**
     * Render the preview
     *
     * @return Response
     * @throws PreviewInvalidDataException
     * @throws PreviewNotActiveException
     */
    public function render()
    {
        $this->checkRequirements();

        $this->toggleDoctrineFilter('disable');

        $request = $this->getRequest();

        $path = $this->getAttributes();
        $path['_forwarded'] = $request->attributes;
        $path['_preview'] = true;

        $subRequest = $request->duplicate(null, null, $path);

        return $this->httpKernel->handle($subRequest, HttpKernelInterface::SUB_REQUEST, false);
    }

    /**
     * Returns if the session is being requested
     *
     * @return bool
     */
    public function isRequested()
    {
        $request = $this->getRequest();

        return ($request->attributes->has('_preview') || $request->query->has('preview'));
    }

    /**
     * Disable doctrine filters
     */
    public function disableDoctrineFilters()
    {
        $this->toggleDoctrineFilter('disable');
    }

    /**
     * Enable doctrine filters
     */
    public function enableDoctrineFilters()
    {
        $this->toggleDoctrineFilter('enable');
    }

    /**
     * Toggles the doctrine filters, it also performs the check if a toggle can be performed
     * I.E. an enabled filter cannot be enabled again
     *
     * @param string $toggle
     *
     * @throws Exception
     */
    private function toggleDoctrineFilter($toggle)
    {
        $allowedToggles = ['enable', 'disable'];

        if (!in_array($toggle, $allowedToggles)) {
            throw new Exception(sprintf("Invalid toggle, %s is not in %s"), $toggle, implode(", ", $allowedToggles));
        }

        $filterCollection = $this->entityManager->getFilters();

        $checkMethod = !($toggle == 'enable');

        foreach ($this->doctrineFilters as $filter) {
            if (!$filterCollection->has($filter)) {
                continue;
            }

            $filterStatus = $filterCollection->isEnabled($filter);

            // Check if the method has the inversed status of $toggle
            if ($filterStatus == $checkMethod) {
                call_user_func([$filterCollection, $toggle], $filter);
            }
        }
    }

    /**
     * Checks the required pre requisites for creating a preview session
     *
     * @param $class
     * @param $identifier
     *
     * @throws Exception
     * @throws PreviewInvalidDataException
     */
    private function checkPrerequisites($class, $identifier)
    {
        if (!class_exists($class)) {
            throw new Exception(sprintf("Entity %s does not exists", $class));
        }

        if (!in_array(PreviewableEntityInterface::class, class_implements($class))) {
            throw new Exception(sprintf("Entity %s does not implement PreviewableEntityInterface",
                $class));
        }

        if (!method_exists($class, 'getController')) {
            throw new PreviewInvalidDataException(
                sprintf("Missing controller that is required for rendering preview, check the getController method in (%s)",
                    $class)
            );
        }

        if (!method_exists($class, 'getControllerParam')) {
            throw new PreviewInvalidDataException(
                sprintf("Missing parameters that are required for rendering preview, check the getControllerParam method in (%s)",
                    $class)
            );
        }

        if (!$this->entityManager->getRepository($class)->find($identifier)) {
            throw new Exception(sprintf("Entity %s with id (%d) was not found", $class, $identifier));
        }
    }

    /**
     * Stores the subject into the current session
     */
    private function storeSubjectInSession()
    {
        $sessionData = $this->getSession()->get('preview');
        $sessionData['subject'] = $this->subject;

        $this->getSession()->set('preview', $sessionData);
    }

    /**
     * Returns the current Session object from the request
     *
     * @return null|SessionInterface
     */
    private function getSession()
    {
        return $this->requestStack->getMasterRequest()->getSession();
    }

    /**
     * Return the current request
     *
     * @return Request
     */
    private function getRequest()
    {
        return $this->requestStack->getCurrentRequest();
    }

    /**
     * Check all requirements before rendering the actual preview
     *
     * @throws PreviewInvalidDataException
     */
    private function checkRequirements()
    {
        $session = new ParameterBag($this->getSession()->get('preview'));

        if (!$session->has('subject')) {
            throw new PreviewInvalidDataException("Missing subject in preview data");
        }

        if (empty($session->get('subject')['class'])) {
            throw new PreviewInvalidDataException("Missing class in preview subject data");
        }

        if (empty($session->get('subject')['identifier'])) {
            throw new PreviewInvalidDataException("Missing identifier in preview subject data");
        }

        if (!$session->has('attributes')) {
            throw new PreviewInvalidDataException("Missing attributes in preview data");
        }

        $params = [];

        //Locale is always set by kernel
        $params['_locale'] = $this->requestStack->getMasterRequest()->getLocale();

        // Check if the requested identifier is correct
        $params[\call_user_func([
            $this->getSubject()['class'],
            'getControllerParam',
        ])] = $this->getSubject()['identifier'];

        /*
         * Check if there is a difference between the parameters being present and the ones
         * that are needed for rendering the preview.
         */
        if (\count(array_diff($this->getAttributes()['_route_params'], $params)) > 0) {
            throw new PreviewInvalidDataException("Parameters of requested preview and current parameters do not match");
        }
    }

    /**
     * Check if requested controller is allowed to be called from within the preview
     *
     * @param null|string $controller
     *
     * @return bool
     */
    public function checkAllowedControllers($controller = null)
    {
        //
        if (is_null($controller)) {
            $controller = $this->getRequest()->attributes->get('_controller');
        }

        // Set-up list of general controllers
        $whitelist = [
            DataController::class,
            HomeController::class . '::indexAction',
            CartController::class,
            PreviewController::class,
            'app.exception_controller:showAction',
        ];

        // Get all entities that implement the PreviewableEntityInterface and add them to the controller array
        $controllers = array_merge($whitelist, array_unique(
            array_map(function ($entity) {
                return call_user_func([$entity, 'getController']);
            }, array_filter(
                get_declared_classes(),
                function ($className) {
                    $implements = class_implements($className);

                    return in_array(PreviewableEntityInterface::class, $implements) && !in_array(Proxy::class,
                            $implements);
                }
            ))
        ));

        // Check a full match of the controller path
        if (in_array($controller, $controllers)) {
            return true;
        }

        // If there is no full match, check for controller wildcard
        $controllerPart = substr($controller, 0, strpos($controller, '::'));

        if (in_array($controllerPart, $controllers)) {
            return true;
        }

        return false;
    }
}
