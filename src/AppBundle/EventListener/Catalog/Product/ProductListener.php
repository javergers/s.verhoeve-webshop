<?php

namespace AppBundle\EventListener\Catalog\Product;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Finance\Tax\VatGroup;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;

/**
 * Class ProductListener
 * @package AppBundle\EventListener\Catalog\Product
 */
class ProductListener
{
    /**
     * @ORM\PreRemove()
     * @param Product            $product
     * @param LifecycleEventArgs $args
     * @throws DBALException
     */
    public function removeFromAssortment(Product $product, LifecycleEventArgs $args)
    {
        $sql = '
                UPDATE assortment_product
                SET deleted_at = NOW()
                WHERE product_id = :product
            ';

        $em = $args->getEntityManager();

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue(':product', $product->getId());
        $stmt->execute();
    }
}