<?php

namespace AppBundle\EventListener;

use AppBundle\Model\BaseModel;
use Doctrine\Common\EventArgs;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class ModelContainerInjectorListener
 * @package AppBundle\EventListener
 */
class ModelContainerInjectorListener implements EventSubscriber
{
    use ContainerAwareTrait;

    /**
     * @param EventArgs $eventArgs
     */
    public function postLoad(EventArgs $eventArgs)
    {
        $object = $eventArgs->getObject();

        if ($object instanceof ContainerAwareInterface) {
            $object->setContainer($this->container);
        }

        if ($object instanceof BaseModel) {
            $object->init();
        }
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'postLoad',
        ];
    }
}
