<?php

namespace AppBundle\EventListener;

use AppBundle\Services\PreviewService;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Profiler\Profiler;
use Symfony\Component\Routing\Router;

/**
 * Class PreviewListener
 *
 * @package AppBundle\EventListener
 */
class PreviewListener
{
    use ContainerAwareTrait;

    /** @var ControllerResolverInterface */
    private $resolver;

    /** @var PreviewService */
    private $preview;

    /** @var Profiler */
    private $profiler;

    /** @var Router */
    private $router;

    /**
     * PreviewListener constructor.
     *
     * @param PreviewService              $preview
     * @param ControllerResolverInterface $resolver
     * @param Router                      $router
     */
    public function __construct(PreviewService $preview, ControllerResolverInterface $resolver, Router $router)
    {
        $this->resolver = $resolver;
        $this->preview = $preview;
        $this->router = $router;
    }

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;

        if ($this->container->has("profiler")) {
            $this->profiler = $this->container->get("profiler");
        }
    }

    /**
     * Perform several checks and disable unwanted filters before loading the preview mode
     *
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        /*
         * If the preview is active or being requested disable doctrine filters
         * in order to display unpublished items
         */
        if ($this->preview->isActive() || $this->preview->isRequested()) {
            if (!$this->preview->checkAllowedControllers()) {
                $event->setResponse(new RedirectResponse($this->router->generate("app_preview_blocked")));

                return;
            }

            $this->preview->disableDoctrineFilters();

            if ($this->profiler) {
                $this->profiler->disable();
            }
        }
    }

    /**
     * Process specific preview mode requests in the requested uri
     *
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        // If the preview isn't called, skip the action checks
        if (!$event->getRequest()->query->has('preview')) {
            return;
        }

        switch ($event->getRequest()->query->get("action", null)) {
            case 'destroy':
                if (!(bool)($url = $this->preview->destroy())) {
                    return;
                }

                break;
            case 'request':
                if (!(bool)($url = $this->preview->request())) {
                    return;
                }

                break;
            case 'return_to_admin':
                if (!(bool)($url = $this->preview->getAdminUrl())) {
                    return;
                }

                if (!$this->preview->destroy()) {
                    return;
                }

                break;
            default:
                $url = null;
        }

        // If an url is set in the previous action checks redirect to it
        if (!is_null($url)) {
            $event->setResponse(new RedirectResponse($url));
        }
    }

    /**
     * Override the controller for the preview mode
     *
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        // If preview mode is requested render the preview controller and pass the request to it
        if ($event->getRequest()->query->has('preview')) {
            $event->setController(
                $this->preview->getPreviewController($this->resolver, $event)
            );
        }
    }
}
