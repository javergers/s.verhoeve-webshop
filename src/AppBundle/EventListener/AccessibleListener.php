<?php

namespace AppBundle\EventListener;

use Doctrine\Common\EventArgs;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class AccessibleListener implements EventSubscriber
{
    private $tokenStorage;

    public function getSubscribedEvents()
    {
        // @todo maybe remove this class because the function is empty
        return [
            'loadClassMetadata',
        ];
    }

    /**
     * AccessibleListener constructor.
     *
     * @param TokenStorage $tokenStorage
     */
    public function __construct(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;

    }

    /**
     * Return the Token Storage
     *
     * @return TokenStorage
     */
    public function getTokenStorage()
    {
        return $this->tokenStorage;
    }

    /**
     * Maps additional metadata
     *
     * @param EventArgs $eventArgs
     *
     * @return void
     */
    public function loadClassMetadata(EventArgs $eventArgs)
    {
        void($eventArgs);
    }
}
