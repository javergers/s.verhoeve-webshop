<?php

namespace AppBundle\EventListener\Teamleader;

use AppBundle\Entity\Catalog\Product\Product;
use Doctrine\Common\EventArgs;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class ProductListener
 * @package AppBundle\EventListener\Teamleader
 */
class ProductListener
{
    use ContainerAwareTrait;

    /**
     * @param Product   $product
     * @param EventArgs $eventArgs
     * @throws \Exception
     */
    public function postPersist(Product $product, EventArgs $eventArgs)
    {
        $this->scheduleJob($product, $eventArgs);
    }

    /**
     * @param Product   $product
     * @param EventArgs $eventArgs
     * @throws \Exception
     */
    public function preUpdate(Product $product, EventArgs $eventArgs)
    {
        $this->scheduleJob($product, $eventArgs);
    }

    /**
     * @param Product   $product
     * @param EventArgs $eventArgs
     * @throws \Exception
     */
    private function scheduleJob(Product $product, EventArgs $eventArgs)
    {
        $job = new Job('teamleader:sync-product', [$product->getId()], true, 'teamleader');
        $job->addRelatedEntity($product);

        $this->container->get('job.manager')->addJob($job);
    }
}