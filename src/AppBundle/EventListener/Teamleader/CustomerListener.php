<?php

namespace AppBundle\EventListener\Teamleader;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Services\JobManager;
use Doctrine\Common\EventArgs;
use Doctrine\ORM\UnitOfWork;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class CustomerListener
 * @package AppBundle\EventListener\Teamleader
 */
class CustomerListener
{
    use ContainerAwareTrait;

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @param Customer  $customer
     * @param EventArgs $eventArgs
     * @throws \Exception
     */
    public function postUpdate(Customer $customer, EventArgs $eventArgs) {
        $this->scheduleJobs($customer, $eventArgs);
    }

    /**
     * @param Customer  $customer
     * @param EventArgs $eventArgs
     * @throws \Exception
     */
    public function postPersist(Customer $customer, EventArgs $eventArgs) {
        $this->scheduleJobs($customer, $eventArgs);
    }

    /**
     * @param Customer  $customer
     * @param EventArgs $eventArgs
     * @throws \Exception
     */
    private function scheduleJobs(Customer $customer, EventArgs $eventArgs) {
        if (!$customer->getCompany() || $customer->getCompany()->getId() === null) {
            return;
        }

        if ($this->getJobManager()->getStartableJobs('teamleader:sync-customer', $customer)) {
            return;
        }

        $job = new Job('teamleader:sync-customer', [$customer->getId()], true, 'teamleader');
        $job->addRelatedEntity($customer);

        /** @var $uow UnitOfWork */
        $uow = $this->container->get('doctrine')->getManager()->getUnitOfWork();

        if ($uow->isEntityScheduled($customer->getCompany())) {
            $companyJob = $this->getJobManager()->getStartableJob('teamleader:sync-company', $customer->getCompany());

            if (!$companyJob) {
                $companyJob = new Job('teamleader:sync-company', [$customer->getCompany()->getId()]);
                $companyJob->addRelatedEntity($customer->getCompany());

                $this->getJobManager()->addJob($companyJob);
            }

            $job->addDependency($companyJob);
        }

        $this->getJobManager()->addJob($job);
    }

    /**
     * @return JobManager
     */
    private function getJobManager()
    {
        if($this->jobManager) {
            return $this->jobManager;
        }

        $this->jobManager = $this->container->get('job.manager');

        return $this->jobManager;
    }
}