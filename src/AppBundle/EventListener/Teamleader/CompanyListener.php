<?php

namespace AppBundle\EventListener\Teamleader;

use AppBundle\Entity\Relation\Company;
use Doctrine\Common\EventArgs;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class CompanyListener
 * @package AppBundle\EventListener\Teamleader
 */
class CompanyListener
{
    use ContainerAwareTrait;

    /**
     * @param Company   $company
     * @param EventArgs $eventArgs
     * @throws \Exception
     */
    public function postPersist(Company $company, EventArgs $eventArgs)
    {
        $this->scheduleJob($company, $eventArgs);
    }

    /**
     * @param Company   $company
     * @param EventArgs $eventArgs
     * @throws \Exception
     */
    public function preUpdate(Company $company, EventArgs $eventArgs)
    {
        $this->scheduleJob($company, $eventArgs);
    }

    /**
     * @param Company   $company
     * @param EventArgs $eventArgs
     * @throws \Exception
     */
    private function scheduleJob(Company $company, EventArgs $eventArgs)
    {
        $job = new Job('teamleader:sync-company', [$company->getId()], true, 'teamleader');
        $job->addRelatedEntity($company);

        $this->container->get('job.manager')->addJob($job);
    }
}