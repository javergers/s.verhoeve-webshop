<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Order\CartStatus;
use AppBundle\Entity\Payment\Payment;
use AppBundle\Manager\Order\OrderCollectionManager;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;
use Symfony\Component\Workflow\Registry;

/**
 * Class PaymentStatusListener
 * @package AppBundle\EventListener
 */
class PaymentStatusListener implements EventSubscriber
{
    /**
     * @var Registry
     */
    protected $workflow;

    /**
     * @var OrderCollectionManager
     */
    private $orderCollectionManager;

    /**
     * PaymentStatusListener constructor.
     *
     * @param Registry $registry
     * @param OrderCollectionManager $orderCollectionManager
     */
    public function __construct(Registry $registry, OrderCollectionManager $orderCollectionManager)
    {
        $this->workflow = $registry;
        $this->orderCollectionManager = $orderCollectionManager;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'postUpdate',
        ];
    }

    /**
     * @param LifecycleEventArgs $event
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     * @throws \Exception
     */
    public function postUpdate(LifecycleEventArgs $event)
    {
        if (!$event->getEntity() instanceof Payment) {
            return;
        }

        /**
         * @var Payment $payment ;
         */
        $payment = $event->getEntity();

        $this->updateCart($event, $payment);
        $this->updateCustomer($payment);
        $this->updateOrders($event, $payment);

        $event->getEntityManager()->flush();
    }

    /**
     * @param LifecycleEventArgs $event
     * @param Payment            $payment
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     */
    private function updateCart(LifecycleEventArgs $event, Payment $payment)
    {
        if($this->orderCollectionManager->calculateOutstanding($payment->getOrderCollection()) > 0) {
            return;
        }

        if ($payment->getOrderCollection()->getCart()) {
            $payment->getOrderCollection()->getCart()->setStatus($event->getEntityManager()->find(CartStatus::class,
                'processed'));
        }
    }

    /**
     * @param LifecycleEventArgs $event
     * @param Payment $payment
     *
     */
    private function updateOrders(LifecycleEventArgs $event, Payment $payment)
    {
        if($this->orderCollectionManager->calculateOutstanding($payment->getOrderCollection()) > 0) {
            return;
        }

        foreach ($payment->getOrderCollection()->getOrders() as $order) {
            $workflow = $this->workflow->get($order);

            if ($workflow->can($order, 'accept_payment')) {
                $workflow->apply($order, 'accept_payment');
            }
        }
    }

    /**
     * @param Payment $payment
     */
    private function updateCustomer(Payment $payment)
    {
        $cart = $payment->getOrderCollection()->getCart();
        if (null !== $cart && !$cart->getCustomer()->getPreferredPaymentmethod()) {
            $cart->getCustomer()->setPreferredPaymentmethod($payment->getPaymentmethod());

            if ($payment->getPaymentmethod()->getCode() === 'ideal') {
                $cart->getCustomer()->setPreferredIdealIssuer($payment->getMetadata()['issuer']);
            }
        }
    }
}
