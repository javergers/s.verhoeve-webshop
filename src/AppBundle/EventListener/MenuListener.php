<?php

namespace AppBundle\EventListener;

use AppBundle\Cache\Adapter\TagAwareAdapter;
use AppBundle\Entity\Site\Menu;
use Doctrine\ORM\Mapping as ORM;
use ReflectionException;

/**
 * Class MenuListener
 * @package AppBundle\EventListener
 */
class MenuListener
{
    /**
     * @var TagAwareAdapter
     */
    private $cache;

    /**
     * MenuListener constructor.
     *
     * @param TagAwareAdapter $cache
     */
    public function __construct(TagAwareAdapter $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     *
     * @param Menu $menu
     *
     * @throws ReflectionException
     */
    public function invalidateCache(Menu $menu)
    {
        $this->cache->delete($this->cache->generateTagKey($menu));
    }
}