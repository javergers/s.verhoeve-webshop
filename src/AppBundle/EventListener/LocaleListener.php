<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Services\SiteService;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

/**
 * Class LocaleListener
 * @package AppBundle\EventListener
 */
class LocaleListener implements EventSubscriberInterface
{
    /**
     * @var string
     */
    private $defaultLocale;

    /**
     * @var SiteService
     */
    private $siteService;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var HttpKernelInterface
     */
    private $httpKernel;

    /**
     * LocaleListener constructor.
     * @param SessionInterface $session
     * @param SiteService      $siteService
     * @param RouterInterface  $router
     * @param string           $defaultLocale
     */
    public function __construct(
        SessionInterface $session,
        SiteService $siteService,
        RouterInterface $router,
        HttpKernelInterface $httpKernel,
        $defaultLocale = 'nl_NL'
    ) {
        $this->session = $session;
        $this->siteService = $siteService;
        $this->router = $router;
        $this->httpKernel = $httpKernel;
        $this->defaultLocale = $defaultLocale;

        if (null !== ($domain = $siteService->determineDomain()) && null !== $domain->getDefaultLocale()) {
            $this->defaultLocale = $domain->getDefaultLocale();
        }
    }

    /**
     * @param GetResponseEvent $event
     * @return Response|void
     * @throws \Exception
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $site = $this->siteService->determineSite();
        $request = $event->getRequest();
        $request->setDefaultLocale($this->defaultLocale);

        //Site is null on admin
        if ($site === null) {
            return;
        }

        $availableLocalesForSite = $site->getAvailableLocales();

        $locale = null;
        $localePath = null;
        if ($request->attributes->get('language') !== null) {
            $locale = $request->attributes->get('language');
        }
        if ($request->query->get('language') !== null) {
            $locale = $request->query->get('language');
        }

        if ($locale === null && !$request->hasPreviousSession()) {
            return;
        }

        //Takes /{_locale} and removes it
        if ($locale === null) {
            $localeExplode = explode('/', $request->getPathInfo());
            if (isset($localeExplode[1])) {
                foreach ($availableLocalesForSite as $locales) {
                    if ($locales === $localeExplode[1]) {
                        $locale = $locales;
                        break;
                    }
                }
                if ($locale !== null) {
                    $localePath = str_replace('/' . $locale, '', $request->getPathInfo());
                    $localePath = $localePath !== '' ? $localePath : '/';
                    //Remove locale from path and forward internally
                }
            }
        }

        if ($request->getSession() !== null) {
            if ($locale !== null && $locale !== '') {
                $request->getSession()->set('_locale', $locale);
            } else {
                // if no explicit locale has been set on this request, use one from the session
                $locale = $request->getSession()->get('_locale', $this->defaultLocale);
            }

            if (false === \in_array($locale, $availableLocalesForSite, true)) {
                $locale = $this->defaultLocale;
                $request->getSession()->set('_locale', $locale);
            }
        } else {
            $locale = $this->defaultLocale;
        }

        $request->setLocale($locale);

        if($localePath !== null){
            try {
                $this->router->match($localePath);
                $response = new RedirectResponse($localePath);
                $event->setResponse($response);
            } catch (ResourceNotFoundException $e) {
                return;
            }
        }
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();

        if ($user instanceof Customer && null !== $user->getLocale()) {
            $this->session->set('_locale', $user->getLocale());
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            // must be registered before (i.e. with a higher priority than) the default Locale listener
            KernelEvents::REQUEST => [['onKernelRequest', 20]],
            SecurityEvents::INTERACTIVE_LOGIN => 'onInteractiveLogin',
        ];
    }
}
