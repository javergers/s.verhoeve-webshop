<?php

namespace AppBundle\EventListener\Supplier;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * Class BakeryUserListener
 * @package AppBundle\EventListener\Supplier
 */
class BakeryUserListener
{
    /**
     * @param Customer           $customer
     * @param PreUpdateEventArgs $eventArgs
     * @throws \Exception
     */
    public function preUpdate(Customer $customer, PreUpdateEventArgs $eventArgs)
    {
        // Trigger company update to update bakkery users
        if ($customer->getCompany() && $customer->getCompany()->getSupplierConnector() && $customer->getCompany()->getSupplierConnector() === 'Bakker') {
            $customer->getCompany()->setUpdatedAt(new \DateTime());

            $metadata = $eventArgs->getEntityManager()->getClassMetadata(Company::class);

            $uow = $eventArgs->getEntityManager()->getUnitOfWork();
            $uow->computeChangeSet($metadata, $customer->getCompany());
        }
    }
}
