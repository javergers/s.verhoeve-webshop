<?php

namespace AppBundle\EventListener\Workflow;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollectionLine;
use AppBundle\Entity\Order\OrderCollectionLineVat;
use AppBundle\Manager\Order\OrderCollectionManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\Event;
use Symfony\Component\Workflow\Event\GuardEvent;
use Symfony\Component\Workflow\Registry;

/**
 * Class OrderStatusListener
 */
class OrderListener implements EventSubscriberInterface
{
    /**
     * @var OrderCollectionManager
     */
    private $orderCollectionManager;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Registry
     */
    private $workflow;

    /**
     * OrderListener constructor.
     *
     * @param OrderCollectionManager $orderCollectionManager
     * @param EntityManagerInterface $entityManager
     * @param Registry $workflow
     */
    public function __construct(
        OrderCollectionManager $orderCollectionManager,
        EntityManagerInterface $entityManager,
        Registry $workflow
    ) {
        $this->orderCollectionManager = $orderCollectionManager;
        $this->entityManager = $entityManager;
        $this->workflow = $workflow;
    }

    /**
     * @param GuardEvent $event
     *
     * @return void
     */
    public function guardForwardToSupplier(GuardEvent $event): void
    {
        /**  @var Order $order */
        $order = $event->getSubject();

        // Block transition in case fraude check has not taken place yet
        if (
            null === $order->getOrderCollection()->getFraudScore()
            && $this->orderCollectionManager->isFraudCheckable($order->getOrderCollection())
        ) {
            $event->setBlocked(true);
            // @todo sf4 $event->setBlocked(true); vervangen met hieronder
//            $event->addTransitionBlocker(
//                new TransitionBlocker('The fraud check has not taken place yet')
//            );
        }
    }

    /**
     * @param GuardEvent $event
     *
     * @return void
     */
    public function guardCancelInvoicePaymentMethod(GuardEvent $event): void
    {
        /**  @var Order $order */
        $order = $event->getSubject();

        if (! $this->orderCollectionManager->hasAuthorizedInvoice($order->getOrderCollection())) {
            $event->setBlocked(true);
            // @todo sf4 $event->setBlocked(true); vervangen met hieronder
//            $event->addTransitionBlocker(
//                new TransitionBlocker('The OrderCollection has no authorized cancellable invoice payment methods.')
//            );
        }
    }

    /**
     * @param Event $event
     *
     * @throws Exception
     */
    public function transitionCancel(Event $event): void
    {
        /** @var Order $order */
        $order = $event->getSubject();
        $orderCollection = $order->getOrderCollection();

        /** @var OrderCollectionLine $collectionLine */
        foreach ($orderCollection->getLines() as $collectionLine) {
            if ($collectionLine->getRelatedOrder() === $order) {
                $workflow = $this->workflow->get($collectionLine);
                $workflow->apply($collectionLine, 'cancel');
            }
        }

        $this->orderCollectionManager->calculateTotals($orderCollection);
        $this->orderCollectionManager->sendOrderCancellationMail($order);
    }

    /**
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.order_status.guard.forward_to_supplier' => 'guardForwardToSupplier',
            'workflow.order_status.guard.cancel_invoice_payment_method' => 'guardCancelInvoicePaymentMethod',
            'workflow.order_status.transition.cancel' => 'transitionCancel',
        ];
    }

}
