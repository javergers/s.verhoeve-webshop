<?php

namespace AppBundle\EventListener\Order;

use AppBundle\Entity\Supplier\SupplierOrder;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\Event;

/**
 * Class SupplierOrderWorkflowListener
 * @package AppBundle\EventListener\Order
 */
class SupplierOrderWorkflowListener implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'workflow.supplier_order.completed.fail' => 'onFailCompleted',
        ];
    }

    /**
     * @param Event $event
     */
    public function onFailCompleted(Event $event)
    {
        /** @var SupplierOrder $supplierOrder */
        $supplierOrder = $event->getSubject();
        $supplierOrder->getOrder()->setSupplierOrder(null);
    }
}