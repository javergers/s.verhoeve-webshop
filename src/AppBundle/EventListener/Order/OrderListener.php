<?php

namespace AppBundle\EventListener\Order;

use AppBundle\Entity\Order\Order;
use AppBundle\Manager\Order\OrderManager;
use AppBundle\Utils\DisableFilter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Class OrderListener
 */
class OrderListener
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var OrderManager
     */
    private $orderManager;

    /**
     * OrderCollectionListener constructor.
     * @param EntityManagerInterface $entityManager
     * @param OrderManager $orderManager
     */
    public function __construct(EntityManagerInterface $entityManager, OrderManager $orderManager)
    {
        $this->entityManager = $entityManager;
        $this->orderManager = $orderManager;
    }

    /**
     * @var array
     */
    protected $orders = [];

    /**
     * @return array
     */
    public function getSubscribedEvents(): array
    {
        return [
            'preUpdate',
            'prePersist',
        ];
    }

    /**
     * @param Order $order
     * @param LifecycleEventArgs $event
     */
    public function preUpdate(Order $order, LifecycleEventArgs $event): void
    {
        $this->calculateTotals($order, $event);
    }

    /**
     * @param Order $order
     * @param LifecycleEventArgs $event
     */
    public function prePersist(Order $order, LifecycleEventArgs $event): void
    {
        $this->calculateTotals($order, $event);
    }

    /**
     * @param Order $order
     * @param LifecycleEventArgs $event
     */
    public function calculateTotals(Order $order, LifecycleEventArgs $event): void
    {
        void($event);

        $filter = DisableFilter::temporaryDisableFilter('publishable');

        $this->orderManager->calculateTotals($order);

        $filter->reenableFilter();
    }
}
