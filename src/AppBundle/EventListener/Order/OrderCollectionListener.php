<?php

namespace AppBundle\EventListener\Order;

use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Manager\Order\OrderCollectionManager;
use AppBundle\Utils\DisableFilter;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Class OrderNumberListener
 */
class OrderCollectionListener
{
    /**
     * @var OrderCollectionManager
     */
    protected $orderCollectionManager;

    /**
     * OrderCollectionListener constructor.
     *
     * @param OrderCollectionManager $orderCollectionManager
     */
    public function __construct(OrderCollectionManager $orderCollectionManager)
    {
        $this->orderCollectionManager = $orderCollectionManager;
    }

    /**
     * @var array
     */
    protected $orders = [];

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'preUpdate',
            'prePersist',
        ];
    }

    /**
     * @param OrderCollection    $orderCollection
     * @param LifecycleEventArgs $event
     */
    public function preUpdate(OrderCollection $orderCollection, LifecycleEventArgs $event)
    {
        $this->calculateTotals($orderCollection, $event);
    }

    /**
     * @param OrderCollection    $orderCollection
     * @param LifecycleEventArgs $event
     */
    public function prePersist(OrderCollection $orderCollection, LifecycleEventArgs $event)
    {
        $this->calculateTotals($orderCollection, $event);
    }

    /**
     * @param OrderCollection    $orderCollection
     * @param LifecycleEventArgs $event
     */
    public function calculateTotals(OrderCollection $orderCollection, LifecycleEventArgs $event)
    {
        void($event);

        $filter = DisableFilter::temporaryDisableFilter('publishable');

        $this->orderCollectionManager->calculateTotals($orderCollection);

        $filter->reenableFilter();
    }
}
