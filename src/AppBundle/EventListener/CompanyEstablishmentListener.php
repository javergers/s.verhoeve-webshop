<?php

namespace AppBundle\EventListener;

use AppBundle\Exceptions\NonDeletableException;
use AppBundle\Entity\Relation\CompanyEstablishment;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Class CompanyEstablishmentListener
 * @package AppBundle\EventListener
 */
class CompanyEstablishmentListener implements EventSubscriber
{

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'preRemove',
        ];
    }

    /**
     * @param LifecycleEventArgs $event
     * @throws NonDeletableException
     */
    public function preRemove(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();
        if($entity instanceof CompanyEstablishment){
            if($entity->isMain() && $entity->isVerified()){
                throw new NonDeletableException(sprintf('Geverifieerde hoofdvestigingen (%s %d) mogen niet verwijderd worden..', $entity->getName(), $entity->getId()));
            }
        }
    }
}