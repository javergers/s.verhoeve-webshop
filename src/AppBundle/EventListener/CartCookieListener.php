<?php

namespace AppBundle\EventListener;

use AppBundle\Services\CartService;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class CartCookieListener
{
    use ContainerAwareTrait;

    /**
     * @var CartService
     */
    protected $cartService;

    /**
     * CartCookieListener constructor.
     * @param CartService $cartService
     */
    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    /**
     * @param FilterResponseEvent $event
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if ($event->getRequest()->getHost() == $this->container->getParameter("admin_host")) {
            return;
        }

        if (strpos($event->getRequest()->getRequestUri(), "/admin") === false && $this->cartService->getCart(null,
                false)) {
            $event->getResponse()->headers->setCookie($this->generateCookie());
        }
    }

    /**
     * @return Cookie
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function generateCookie()
    {
        return new Cookie('cart', $this->cartService->getCart()->getUuid(), new \DateTime('+5 days'));
    }
}
