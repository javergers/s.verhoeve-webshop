<?php

namespace AppBundle\EventListener;

use AppBundle\Services\SiteService;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

/**
 * Class RequireAuthenticationListener
 * @package AppBundle\EventListener
 */
class RequireAuthenticationListener
{
    use ContainerAwareTrait;

    private $loginRouteName = 'app_customer_account_index';
    private $registerRouteName = 'app_customer_register_index';
    private $resettingRouteName = 'app_customer_resetting_request';
    private $resettingResetRouteName = 'app_customer_resetting_reset';

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if ($event->getRequest()->getHost() === $this->container->getParameter('admin_host')) {
            return;
        }

        if ($this->requiresAuthentication()) {
            $response = new RedirectResponse($this->container->get('router')->generate($this->loginRouteName));

            $event->setResponse($response);
        }
    }

    /**
     * @return bool
     */
    private function requiresAuthentication()
    {
        $site = $this->container->get(SiteService::class)->determineSite();

        if (!$site || !$site->getAuthenticationRequired()) {
            return false;
        }

        /**
         * This only happens when the url did not match any route, no token will be set
         */
        if (!$this->container->get('security.token_storage')->getToken()) {
            return false;
        }

        if ($this->container->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            return false;
        }

        $request = $this->container->get('request_stack')->getMasterRequest();

        if (null !== $request) {
            $route = $request->get('_route');

            if (\in_array($route, [
                '_wdt',
                '_profiler',
                '_errors',
                $this->loginRouteName,
                $this->registerRouteName,
                $this->resettingRouteName,
                $this->resettingResetRouteName,
            ], true)) {
                return false;
            }
        }

        return true;
    }
}
