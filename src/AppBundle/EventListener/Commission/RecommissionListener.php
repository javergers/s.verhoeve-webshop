<?php

namespace AppBundle\EventListener\Commission;

use AppBundle\Entity\Supplier\SupplierOrderLine;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * Class RecommissionListener
 * @package AppBundle\EventListener\Commission
 */
class RecommissionListener
{
    /**
     * @param SupplierOrderLine  $supplierOrderLine
     * @param PreUpdateEventArgs $eventArgs
     */
    public function preUpdate(SupplierOrderLine $supplierOrderLine, PreUpdateEventArgs $eventArgs)
    {
        if (!array_key_exists('price', $eventArgs->getEntityChangeSet())) {
            return;
        }

        $supplierOrder = $supplierOrderLine->getSupplierOrder();

        if (!$supplierOrder->isCommissionable() || $supplierOrder->getRecommission()) {
            return;
        }

        if ($supplierOrder->getCommissionInvoiceSupplierOrders()->isEmpty()) {
            return;
        }

        $supplierOrder->setRecommission(true);
    }
}
