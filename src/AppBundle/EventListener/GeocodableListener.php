<?php

namespace AppBundle\EventListener;

use AppBundle\Interfaces\GeocodableInterface;
use CrEOF\Spatial\PHP\Types\Geometry\Point;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Geocoder\Exception\InvalidServerResponse;
use Geocoder\ProviderAggregator;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class GeocodableListener
 * @package AppBundle\EventListener
 */
class GeocodableListener implements EventSubscriber
{
    use ContainerAwareTrait;

    /**
     * @var ProviderAggregator
     */
    private $geocoder;

    /**
     * GeocodableListener constructor.
     * @param ProviderAggregator $geocoder
     */
    public function __construct(ProviderAggregator $geocoder)
    {
        $this->geocoder = $geocoder;
    }


    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'prePersist',
            'preUpdate',
        ];
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Geocoder\Exception\Exception
     */
    public function prePersist(LifecycleEventArgs $eventArgs)
    {
        if (!($eventArgs->getObject() instanceof GeocodableInterface)) {
            return;
        }

        /** @var GeocodableInterface $entity */
        $entity = $eventArgs->getObject();

        $this->geocode($entity);
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws \Geocoder\Exception\Exception
     */
    public function preUpdate(LifecycleEventArgs $eventArgs)
    {
        if (!($eventArgs->getObject() instanceof GeocodableInterface)) {
            return;
        }

        /** @var GeocodableInterface $entity */
        $entity = $eventArgs->getObject();

        /** @var EntityManagerInterface $entityManager */
        $entityManager = $eventArgs->getObjectManager();
        $changeSet = $entityManager->getUnitOfWork()->getEntityChangeSet($eventArgs->getObject());

        if (!$this->hasChanged($changeSet)) {
            return;
        }

        $this->geocode($entity);
    }

    /**
     * @param $changeSet
     * @return bool
     */
    private function hasChanged($changeSet)
    {
        return (bool)array_intersect(array_keys($changeSet), [
            "address",
            "number",
            "postcode",
            "city",
            "country",
        ]);
    }

    /**
     * @param GeocodableInterface $geocodableEntity
     * @throws \Geocoder\Exception\Exception
     */
    private function geocode(GeocodableInterface $geocodableEntity)
    {
        if (strlen((string)$geocodableEntity) < 10) {
            $geocodableEntity->setPoint(null);

            return;
        }

        try {
            $result = $this->geocoder->geocode((string)$geocodableEntity);
            if ($result && $result->has(0)) {
                $coordinates = $result->get(0)->getCoordinates();

                if(null === $coordinates){
                    new \Exception('No coordinates found');
                }

                $location = new Point([
                    $coordinates->getLongitude(),
                    $coordinates->getLatitude(),
                ]);

                $geocodableEntity->setPoint($location);
            }
        } catch (InvalidServerResponse $e) {
            $geocodableEntity->setPoint(null);

            $this->container->get("logger")->notice(sprintf("'%s' could't be geocoded", (string)$geocodableEntity));
        } catch (\Exception $e) {
            $this->container->get("logger")->emergency($e->getMessage());
        }
    }
}
