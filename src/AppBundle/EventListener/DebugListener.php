<?php

namespace AppBundle\EventListener;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\IpUtils;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class DebugListener
 * @package AppBundle\EventListener
 */
class DebugListener implements EventSubscriberInterface
{
    /** @var ParameterBagInterface $parameterBag */
    private $parameterBag;

    /**
     * DebugListener constructor.
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        $debug = null;
        if ($request->query->get('_debug') !== null) {
            $debug = $request->query->get('_debug');
        }

        if ($debug !== null && $request->getSession() !== null && $this->parameterBag->has('debug_ips') && IpUtils::checkIp($request->getClientIp(), $this->parameterBag->get('debug_ips'))) {
            $debug = $debug === '1';
            $request->getSession()->set('_debug', $debug);
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 25]],
        ];
    }
}
