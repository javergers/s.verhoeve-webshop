<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Utils\DisableFilter;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class CustomerEventSubscriber
 *
 * @package AppBundle\EventListener
 */
class CustomerListener
{
    use ContainerAwareTrait;

    /**
     * @param Customer $customer
     *
     * @throws \Exception
     */
    function preRemove(Customer $customer)
    {
        if (!$customer->getOrders()->isEmpty()) {
            $this->container->get('session')->getFlashBag()->add("backend.error",
                "Kan '" . $customer->getFullname() . "' niet verwijderen.<br /><i>Klant heeft reeds bestellingen geplaatst&hellip;</i>");

            throw new \Exception();
        }

        $em = $this->container->get('doctrine')->getManager();
        $filter = DisableFilter::temporaryDisableFilter('softdeletable');

        $customer->setUsernameCanonical(null);
        $customer->setUsername(null);

        $em->flush();
        $filter->reenableFilter();
    }
}
