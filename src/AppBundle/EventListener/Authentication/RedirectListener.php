<?php

namespace AppBundle\EventListener\Authentication;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class RedirectListener
{
    use ContainerAwareTrait;

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            // don't do anything if it's not the master request
            return;
        }

        $session = $this->container->get("session");

        if (!$session->isStarted()) {
            return;
        }

//        $route = $this->container->get("router")->getRouteCollection()->get($event->getRequest()->get("_route"));
//
//        if (!$route) {
//            return;
//        }
//
//        if ($route->getMethods() && !in_array("GET", $route->getMethods())) {
//            return;
//        }

        if ($event->getResponse()->getStatusCode() !== 200) {
            return;
        }

        if ($event->getRequest()->isXmlHttpRequest()) {
            return;
        }

        if (in_array($event->getRequest()->get("_route"), [
            "app_customer_account_index",
        ])) {
            return;
        }

        // Only save url when it dont has an dot (.) like an extension
        if (strpos(($url = $event->getRequest()->getRequestUri()), '.') === false) {
            $session->set('login_target', [
                'uri' => $url,
            ]);
        }
    }
}
