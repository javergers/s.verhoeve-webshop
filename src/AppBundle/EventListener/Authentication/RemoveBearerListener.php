<?php

namespace AppBundle\EventListener\Authentication;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class RemoveBearerListener
{
    use ContainerAwareTrait;

    /**
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        if (!$event->getRequest()->query->get("bearer")) {
            return;
        }

        $route = $event->getRequest()->get("_route");

        if(!$route) {
            $route = 'app_home_index';
        }

        $url = $this->container->get('router')->generate($route);

        $event->setResponse(new RedirectResponse($url));
    }
}