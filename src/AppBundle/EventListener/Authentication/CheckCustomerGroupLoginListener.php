<?php

namespace AppBundle\EventListener\Authentication;

use AppBundle\Services\SiteService;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class CheckCustomerGroupLoginListener
{
    use ContainerAwareTrait;

    /**
     * @var AuthorizationChecker
     */
    protected $authorizationChecker;

    /**
     * @param AuthorizationChecker $authorizationChecker
     */
    public function __construct(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * Do the magic.
     *
     * @param InteractiveLoginEvent $event
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        // @TODO Should use the firewall to decide if this can be skipped
        if (strpos($event->getRequest()->getRequestUri(), "/admin") === 0) {
            return;
        }

        /**
         * @var Customer $user
         */
        $user = $event->getAuthenticationToken()->getUser();

        $site = $this->container->get(SiteService::class)->determineSite();

        if (!$site->getAuthenticationRequired()) {
            return;
        }

        if ($site->getCustomerGroups()->isEmpty()) {
            return;
        }

        if (!$site->getCustomerGroups()->contains($user->getCustomerGroup())) {
            $this->container->get('security.token_storage')->setToken(null);
            $this->container->get('request_stack')->getCurrentRequest()->getSession()->invalidate();

            return;
        }
    }
}