<?php

namespace AppBundle\EventListener\Seo;

use AppBundle\Entity\Site\Redirect;
use AppBundle\Interfaces\Seo\SlugInterface;
use Doctrine\Common\EventArgs;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Doctrine\DBAL\DBALException;

/**
 * Class SlugListener
 * @package AppBundle\EventListener\Seo
 */
class SlugListener implements EventSubscriber
{
    use ContainerAwareTrait;

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'postUpdate',
        ];
    }

    /**
     * @param LifecycleEventArgs $eventArgs
     * @throws DBALException
     */
    public function postUpdate(LifecycleEventArgs $eventArgs)
    {
        if (!($eventArgs->getObject() instanceof SlugInterface)) {
            return;
        }

        /** @var EntityManagerInterface $em */
        $em = $eventArgs->getObjectManager();

        $changeSet = $em->getUnitOfWork()->getEntityChangeSet($eventArgs->getObject());

        if (!isset($changeSet['slug'])) {
            return;
        }

        if (empty($changeSet['slug'][0])) {
            return;
        }

        $redirect = new Redirect();
        $redirect->setOldUrl($changeSet['slug'][0]);

        if ($changeSet['slug'][1]) {
            $redirect->setNewUrl($changeSet['slug'][1]);
            $redirect->setStatus(301);
        } else {
            $redirect->setStatus(410);
        }

        $redirect->setEntityClass(\get_class($eventArgs->getObject()));
        $redirect->setEntityId($eventArgs->getObject()->getId());

        $eventArgs->getObjectManager()->persist($redirect);

        $this->addRawRedirect($redirect, $eventArgs);

        $href = $this->container->get('router')->generate('admin_seo_redirect_delete', [
            'id' => $redirect->getId(),
        ]);

        $this->container->get('router')->getRouteCollection()->get('admin_seo_redirect_delete');

        $flashBag = $this->container->get('session')->getFlashBag()->all();

        $this->container->get('session')->getFlashBag()->add('backend.success', '301 redirect aangemaakt!<br /><a 
            onclick="var notification = $(this).closest(\'.ui-pnotify\'); $.get($(this).attr(\'href\'), function() { notification.find(\'.ui-pnotify-closer\').trigger(\'click\'); }); return false;"
        href="' . $href . '">Ongedaan maken</a>');

        foreach ($flashBag as $type => $flashes) {
            foreach ($flashes as $flash) {
                $this->container->get('session')->getFlashBag()->add($type, $flash);
            }
        }
    }

    /**
     * @param Redirect                     $redirect
     * @param LifecycleEventArgs|EventArgs $eventArgs
     * @throws DBALException
     * @throws \RuntimeException
     */
    public function addRawRedirect(Redirect &$redirect, EventArgs $eventArgs)
    {
        $connection = $eventArgs->getEntityManager()->getConnection();

        $checkRedirect = $eventArgs->getEntityManager()->createQuery("SELECT r FROM AppBundle:Site\Redirect r WHERE r.status = :status AND r.oldUrl = :old_url AND r.newUrl = :new_url AND r.entityClass = :entity_class AND r.entityId = :entity_id AND r.deletedAt IS NULL ORDER BY r.id ASC")
            ->setParameter('status', $redirect->getStatus())
            ->setParameter('old_url', $redirect->getOldUrl())
            ->setParameter('new_url', $redirect->getNewUrl())
            ->setParameter('entity_class', $redirect->getEntityClass())
            ->setParameter('entity_id', $redirect->getEntityId())
            ->setMaxResults(1)
            ->execute();

        if (!$checkRedirect) {
            $affectedRows = $connection->executeUpdate(
                'INSERT INTO seo_redirect (status, old_url, new_url, entity_class, entity_id, created_at, updated_at) VALUES(:status, :old_url, :new_url, :entity_class, :entity_id, :created_at, :updated_at)',
                [
                    'status' => $redirect->getStatus(),
                    'old_url' => $redirect->getOldUrl(),
                    'new_url' => $redirect->getNewUrl(),
                    'entity_class' => $redirect->getEntityClass(),
                    'entity_id' => $redirect->getEntityId(),
                    'created_at' => $redirect->getCreatedAt()->format('Y-m-d H:i:s'),
                    'updated_at' => $redirect->getUpdatedAt()->format('Y-m-d H:i:s'),
                ]
            );

            if ($affectedRows) {
                $redirect = $this->container->get('doctrine')->getRepository(Redirect::class)->findById($connection->lastInsertId())[0];

            } else {
                throw new \RuntimeException('Failed on adding redirect');
            }
        }
    }
}
