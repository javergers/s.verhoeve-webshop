<?php

namespace AppBundle\Form\Checkout;

use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CartType extends AbstractFormType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($builder, $options);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cart';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
//            'auto_initialize' => false,
//            'mapped' => false,
            'label' => false,
        ]);
    }
}
