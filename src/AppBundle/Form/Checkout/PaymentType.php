<?php

namespace AppBundle\Form\Checkout;

use AppBundle\Services\CartService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PaymentType
 * @package AppBundle\Form\Checkout
 */
class PaymentType extends AbstractType
{
    /**
     * @var CartService
     */
    private $cart;

    /**
     * @param CartService $cart
     */
    public function setCart(CartService $cart)
    {
        $this->cart = $cart;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            if (null !== $this->cart && $event->getForm()->has('afterpay') && $this->cart->getCart()->getCustomer() && $this->cart->getCart()->getCustomer()->getBirthday()) {
                $event->getForm()->get('afterpay')->remove('birthday');
            }
        });
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'site_checkout_payment';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'auto_initialize' => false,
            'mapped' => false,
            'label' => false,
            'allow_extra_fields' => true,
        ]);
    }
}
