<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\DataTransformer\DateTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DateType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined([
            'datepicker',
        ]);

        $resolver->setDefaults([
            'icon' => null,
            'disabled' => false,
            'widget' => 'single_text',
            'widget_form_control_feedback' => [
                'position' => 'left',
                'icon' => 'calendar22',
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new DateTransformer());

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['valid'] = !$form->isSubmitted() || $form->isValid();
        $view->vars['widget'] = $options['widget'];
        $view->vars['widget_form_control_feedback'] = $options['widget_form_control_feedback'];

        if (isset($options['datepicker'])) {
            $view->vars['datepicker'] = $options['datepicker'];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return TextType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'date';
    }
}
