<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class AbstractEntityFormType
 * @package AppBundle\Form
 */
class AbstractEntityFormType extends AbstractFormType
{
    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @required
     * @param RequestStack           $requestStack
     * @param EntityManagerInterface $entityManager
     */
    public function loadDependencies(RequestStack $requestStack, EntityManagerInterface $entityManager)
    {
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
    }

    /**s
     * @param $class
     * @return null|object
     */
    protected function getEntity($class)
    {
        $masterRequest = $this->requestStack->getMasterRequest();
        if($masterRequest === null){
            return null;
        }
        if ($masterRequest->get('entity')) {
            $match = $masterRequest->get('entity');
        } else {
            preg_match("/[0-9]+/", $masterRequest->getRequestUri(), $match);

            if (!$match) {
                return null;
            }

            $match = $match[0];
        }

        return $this->entityManager->getRepository($class)->find($match);
    }

}