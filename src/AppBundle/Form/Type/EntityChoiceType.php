<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\Type\Transformer\EntityToIdTransformer;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use RuntimeException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EntityChoiceType
 * @package AppBundle\Form\Type
 */
class EntityChoiceType extends AbstractType
{
    /**
     * @var Registry
     */
    private $registry;

    /**
     * EntityChoiceType constructor.
     *
     * @param Registry $registry
     */
    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(
            new EntityToIdTransformer($this->registry->getManager()->getRepository($options['class']))
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $registry = $this->registry;
        $resolver->setDefaults([
            'empty_value' => false,
            'empty_data' => null,
            'em' => null,
            'query_builder' => null,
            'field' => 'id',
        ]);

        $resolver->setRequired([
            'class',
        ]);

        $resolver->setDefault('choices', function (Options $options) {
            if (null === $options['query_builder']) {
                $results = $options['em']
                    ->createQueryBuilder()
                    ->select('e.id', 'e.' . $options['field'])
                    ->from($options['class'], 'e', 'e.id')
                    ->orderBy('e.' . $options['field'], 'ASC')
                    ->getQuery()
                    ->getArrayResult();
            } else {
                $results = $options['query_builder']
                    ->getQuery()
                    ->getArrayResult();
            }

            $choices = [];

            foreach ($results as $result) {
                $choices[$result[$options['field']]] = $result['id'];
            }

            return $choices;
        });

        $queryBuilderNormalizer = function (Options $options, $queryBuilder) {
            if (is_callable($queryBuilder)) {
                $queryBuilder = $queryBuilder($options['em']->getRepository($options['class']));
            }

            return $queryBuilder;
        };

        $emNormalizer = function (Options $options, $em) use ($registry) {
            /* @var ManagerRegistry $registry */
            if (null !== $em) {
                if ($em instanceof ObjectManager) {
                    return $em;
                }
                return $registry->getManager($em);
            }

            $em = $registry->getManagerForClass($options['class']);

            if (null === $em) {
                throw new RuntimeException(sprintf(
                    'Class "%s" seems not to be a managed Doctrine entity. ' .
                    'Did you forget to map it?',
                    $options['class']
                ));
            }

            return $em;
        };

        $resolver->setNormalizer('em', $emNormalizer);
        $resolver->setNormalizer('query_builder', $queryBuilderNormalizer);
    }

    /**
     * @return string|null
     */
    public function getParent()
    {
        return ChoiceType::class;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'entity_choice';
    }
}