<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\DataTransformer\RruleWeeklyTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Type;

/**
 * Class RruleWeeklyType
 * @package AppBundle\Form\Type
 */
class RruleWeeklyType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'compound' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $daysOfTheWeek = ['Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag', 'Zondag'];

        $column1 = $builder->create('rrule_column_1', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => false,
        ]);
        $column2 = $builder->create('rrule_column_2', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => false,
        ]);

        foreach ($daysOfTheWeek as $index => $dayOfTheWeek) {
            $column = $index < 4 ? $column1 : $column2;
            $column->add($index, CheckboxType::class, [
                'label' => $dayOfTheWeek,
                'required' => false,
            ]);
        }

        $builder->add($column1);
        $builder->add($column2);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'recurring_event_weekly';
    }
}
