<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DayType
 * @package AppBundle\Form\Type
 */
class DayType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefined([
            'days',
        ]);

        $resolver->setDefaults([
            'days'  => null,
        ]);

        $resolver->addAllowedTypes('days', ['array', 'null']);

        $resolver->setNormalizer('choices', function (Options $options) {
            if (null !== $options['days'] && \is_array($options['days'])) {
                return $options['days'];
            }

            return [
                'Maandag' => 1,
                'Dinsdag' => 2,
                'Woensdag' => 3,
                'Donderdag' => 4,
                'Vrijdag' => 5,
                'Zaterdag' => 6,
                'Zondag' => 7,
            ];
        });
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['valid'] = !$form->isSubmitted() || $form->isValid();
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return ChoiceType::class;
    }
}
