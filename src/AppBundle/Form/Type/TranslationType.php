<?php

namespace AppBundle\Form\Type;

use A2lix\TranslationFormBundle\Locale\LocaleProviderInterface;
use AppBundle\EventListener\TranslationListener;
use AppBundle\EventListener\TranslationsListener;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TranslationType extends AbstractFormType
{
    /** @var TranslationsListener */
    private $translationListener;

    /** @var LocaleProviderInterface */
    private $localeProvider;

    /**
     * @param TranslationListener     $translationListener
     * @param LocaleProviderInterface $localeProvider
     */
    public function __construct(TranslationListener $translationListener, LocaleProviderInterface $localeProvider)
    {
        $this->translationListener = $translationListener;
        $this->localeProvider = $localeProvider;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder->addEventSubscriber($this->translationListener);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        void($form, $options);

        $view->vars['default_locale'] = $this->localeProvider->getDefaultLocale();
    }

    /**
     * @param OptionsResolver $resolver
     * @todo: implement own locale provider instead of using a2lix dependency
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'mapped' => false,
            'field_type' => TextType::class,
            'by_reference' => false,
            'locales' => $this->localeProvider->getLocales(),
            'default_locale' => $this->localeProvider->getDefaultLocale(),
            'required_locales' => $this->localeProvider->getRequiredLocales(),
        ]);
    }

    public function getBlockPrefix()
    {
        return 'translation';
    }
}
