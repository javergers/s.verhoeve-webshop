<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Intl\Intl;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Translation\Bundle\Service\ConfigurationManager;

/**
 * Class AvailableLocaleType
 * @package AppBundle\Form\Type
 */
class PhpTranslationsLocaleType extends AbstractType
{
    /**
     * @var ConfigurationManager $translator
     */
    private $translationConfig;

    /**
     * AvailableLocaleType constructor.
     * @param ConfigurationManager $translationConfig
     */
    public function __construct(ConfigurationManager $translationConfig)
    {
        $this->translationConfig = $translationConfig;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $locales = $this->translationConfig->getConfiguration()->getLocales();

        $choices = [];

        foreach ($locales as $locale) {
            $codes = explode('_', $locale);

            if ($codes[0] === 'en') {
                unset($codes[1]);
            }

            $name = Intl::getLanguageBundle()->getLanguageName($codes[0], $codes[1] ?? null);

            $choices[$locale] = $name;
        }

        $choices = \array_flip($choices);

        if(null === $choices) {
            $choices = [];
        }

        $resolver->setDefaults([
            'choices' => $choices,
        ]);
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return ChoiceType::class;
    }
}
