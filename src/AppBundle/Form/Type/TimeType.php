<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\DataTransformer\TimeTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TimeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined([
            'timepicker',
        ]);

        $resolver->setDefaults([
            'icon' => null,
            'disabled' => false,
            'widget' => 'single_text',
            'widget_form_control_feedback' => [
                'position' => 'left',
                'icon' => 'alarm',
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addModelTransformer(new TimeTransformer());

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['valid'] = !$form->isSubmitted() || $form->isValid();
        $view->vars['widget'] = $options['widget'];
        $view->vars['widget_form_control_feedback'] = $options['widget_form_control_feedback'];

        if (isset($options['timepicker'])) {
            $view->vars['timepicker'] = $options['timepicker'];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return TextType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'time';
    }
}
