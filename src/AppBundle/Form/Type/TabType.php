<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\Extension\TabbedFormTypeExtension;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TabType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'icon' => null,
            'error_icon' => 'warning22',
            'disabled' => false,
            'inherit_data' => true,
            'alignment' => TabbedFormTypeExtension::ALIGNMENT_HORIZONTAL,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['valid'] = $valid = !$form->isSubmitted() || $form->isValid();
        $view->vars['icon'] = $valid ? $options['icon'] : $options['error_icon'];
        $view->vars['tab_active'] = false;
        $view->vars['disabled'] = $options['disabled'];

        $view->parent->vars['tabbed'] = true;
        $view->parent->vars['has_tabs'] = true;
        $view->parent->vars['alignment'] = $options['alignment'];
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'tab';
    }
}
