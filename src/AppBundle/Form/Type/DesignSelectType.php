<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Manager\Catalog\Product\ProductManager;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class DesignSelectType
 * @package AppBundle\Form\Type
 */
class DesignSelectType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            if (!empty($event->getData()['design'])) {
                $event->setData((string)$event->getData()['design']);
            }
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $form = $event->getForm();
            $rootForm = $form->getRoot();

            /** @var Product $product */
            $product = $rootForm->getData();

            $metadata = $product->getMetadata();

            if (!\is_array($metadata)) {
                $metadata = [];
            }

            $metadata['design'] = $event->getData();

            if (empty($metadata['design'])) {
                unset($metadata['design']);
            }

            if (count($metadata) === 0) {
                $metadata = null;
            }

            $event->setData($metadata);
        });
    }

    /**
     * @param FormView      $view
     * @param FormInterface $form
     *
     * @param array         $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['valid'] = !$form->isSubmitted() || $form->isValid();

        $rootForm = $form->getRoot();

        /** @var Product $product */
        $product = $rootForm->getData();

        if (empty($view->vars['label']) && $view->vars['label'] !== false) {
            $view->vars['label'] = 'Ontwerp';
        }

        if (empty($view->vars['disabled'])) {
            $view->vars['attr']['readonly'] = true;
        }

        $view->vars['product'] = $product;

        if (\is_array($view->vars['value']) && isset($view->vars['value']['design'])) {
            $view->vars['value'] = $view->vars['value']['design'];
        }

        if($product instanceof CompanyProduct) {
            $view->vars['company'] = $product->getCompany()->getId();
            $product = $product->getRelatedProduct();
        }

        $view->vars['personalization'] = null;
        if ($product->isCombination() && $product->hasPersonalization() && false === $product->getPersonalizations()->isEmpty()) {
            $view->vars['personalization'] = $product->getPersonalizations()->first();
        }
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return TextType::class;
    }

}
