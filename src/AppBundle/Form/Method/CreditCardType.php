<?php

namespace AppBundle\Form\Method;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CreditCardType
 * @package AppBundle\Form\Method
 */
class CreditCardType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('number', TextType::class, [
                'label' => 'label.creditcard.number',
                'hide_name' => true,
                'required' => false,
                'attr' => [
                    'data-encrypted-name' => 'number',
                    'data-validation' => 'creditcard',
                    'data-validation-allowing' => 'visa, mastercard, amex',
                    'autocomplete' => 'off',
                ],
            ])
            ->add('holderName', TextType::class, [
                'label' => 'label.creditcard.holder_name',
                'hide_name' => true,
                'required' => false,
                'attr' => [
                    'data-encrypted-name' => 'holderName',
                    'data-validation' => 'length',
                    'data-validation-length' => 'min3',
                    'data-validation-error-msg' => 'Naam op kaart moet minimaal 3 tekens lang zijn',
                    'autocomplete' => 'off',
                ],
            ])
            ->add('expiryMonth', ChoiceType::class, [
                'label' => 'label.creditcard.expire_month',
                'hide_name' => true,
                'required' => false,
                'attr' => [
                    'data-encrypted-name' => 'expiryMonth',
                    'data-validation' => 'number',
                    'autocomplete' => 'off',
                ],
                'choices' => $this->getExpiryMonths(),
                'choice_translation_domain' => false,
            ])
            ->add('expiryYear', ChoiceType::class, [
                'label' => 'label.creditcard.expire_year',
                'hide_name' => true,
                'required' => false,
                'attr' => [
                    'data-encrypted-name' => 'expiryYear',
                    'data-validation' => 'number',
                    'autocomplete' => 'off',
                    'maxlength' => 4,
                    'min' => date('y'),
                    'max' => date('y') + 7,
                ],
                'choices' => $this->getExpiryYears(),
                'choice_translation_domain' => false,
            ])
            ->add('cvc', TextType::class, [
                'label' => 'label.creditcard.cvc',
                'hide_name' => true,
                'required' => false,
                'attr' => [
                    'data-encrypted-name' => 'cvc',
                    'data-validation' => 'cvv',
                    'autocomplete' => 'off',
                    'maxlength' => 4,
                ],
            ])
            ->add('generationtime', HiddenType::class, [
                'hide_name' => true,
                'required' => false,
                'attr' => [
                    'data-encrypted-name' => 'generationtime',
                    'value' => date_format(date_create('now'), 'c'),
                ],
            ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'payment_method_creditcard';
    }

    /**
     * @return array
     */
    private function getExpiryMonths()
    {
        return [
            '01' => 1,
            '02' => 2,
            '03' => 3,
            '04' => 4,
            '05' => 5,
            '06' => 6,
            '07' => 7,
            '08' => 8,
            '09' => 9,
            '10' => 10,
            '11' => 11,
            '12' => 12,
        ];
    }

    /**
     * @return array
     */
    private function getExpiryYears()
    {
        $years = [];
        $currentYear = date('Y');

        for ($y = $currentYear; $y <= $currentYear + 5; $y++) {
            $years[$y] = $y;
        }

        return $years;
    }
}
