<?php

namespace AppBundle\Form\Method;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class IdealType extends AbstractType
{
    protected $gateway;

    public function __construct($gateway)
    {
        $this->gateway = $gateway;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('issuer', ChoiceType::class, [
                'metadata' => true,
                'label' => false,
                'required' => true,
                'placeholder' => 'placeholder.ideal.issuer',
                'translation_domain' => 'messages',
                'choices' => array_flip($this->gateway->getIdealIssuers()),
                'choice_translation_domain' => false,
                'invalid_message' => "payment.method.ideal.empty_issuer",
            ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'payment_method_ideal';
    }
}
