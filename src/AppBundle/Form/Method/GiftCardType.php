<?php

namespace AppBundle\Form\Method;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class GiftCardType
 * @package AppBundle\Form\Method
 */
class GiftCardType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('giftcard', ChoiceType::class, [
                'metadata' => true,
                'label' => false,
                'required' => true,
                'placeholder' => 'placeholder.giftcard.choice',
                'choices' => $this->getGiftCards(),
                'choice_translation_domain' => false,
            ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'payment_method_giftcard';
    }

    /**
     * @return array
     */
    private function getGiftCards()
    {
        return [
            'Bloemen Cadeaubon' => 'bloemencadeaubon',
            'Bloemen Giftcard' => 'bloemengiftcard',
            'Nationale Verwen Cadeaubon' => 'nationaleverwencadeaubon',
            'VVV Giftcard' => 'vvvgiftcard',
            'Your Gift' => 'yourgift',
        ];
    }
}
