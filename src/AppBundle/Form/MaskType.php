<?php

namespace AppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\TextType;

class MaskType extends TextType
{

    public function getBlockPrefix()
    {
        return 'mask';
    }

    public function getParent()
    {
        return TextType::class;
    }

}