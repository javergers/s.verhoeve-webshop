<?php

namespace AppBundle\Form\Admin;

use AppBundle\DBAL\Types\YesNoType;
use AppBundle\Entity\Common\Parameter\Parameter;
use AppBundle\Form\Type\BooleanType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ParameterType
 * @package AppBundle\Form\Admin
 */
class ParameterType extends AbstractType
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * ParameterType constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entities = $this->container->get('doctrine')->getManager()->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();

        $entityChoices = [];
        foreach ($entities as $entity) {
            $entityChoices[$entity] = $entity;
        }

        $container = $builder->create('parameter_container', ContainerType::class, []);

        $column = $builder->create('parameter_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
        ]);

        $column->add('key', TextType::class, [
            'label' => 'Sleutel',
            'required' => true,
        ]);

        $column->add('formType', ChoiceType::class, [
            'choices' => [
                ColumnType::class => ColumnType::class,
                TextType::class => TextType::class,
                TextareaType::class => TextareaType::class,
                BooleanType::class => BooleanType::class,
                EntityType::class => EntityType::class,
            ],
            'required' => true,
        ]);

        $column->add('formTypeClass', ChoiceType::class, [
            'label' => 'FormType class',
            'choices' => $entityChoices,
            'select2' => [
                'placeholder' => 'Select field relation class',
            ],
        ]);

        $column->add('multiple', ChoiceType::class, [
            'label' => 'Meerdere waardes toestaan',
            'choices' => YesNoType::getChoices(),
        ]);

        $container->add($column);
        $builder->add($container);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_parameter_form';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Parameter::class,
            'translation_domain' => false
        ]);
    }
}
