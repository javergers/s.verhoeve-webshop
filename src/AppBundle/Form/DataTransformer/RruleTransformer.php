<?php

namespace AppBundle\Form\DataTransformer;

use Recurr;
use Symfony\Component\Form\DataTransformerInterface;

class RruleTransformer implements DataTransformerInterface
{
    /**
     * @param mixed $rrule
     * @return mixed|\stdClass
     * @see http://stackoverflow.com/questions/5183630/calendar-recurring-repeating-events-best-storage-method
     * @see https://tools.ietf.org/html/rfc2445
     * @see https://tools.ietf.org/html/rfc5545
     * @see https://github.com/simshaun/recurr
     * @see http://worthfreeman.com/projects/online-icalendar-recurrence-event-parser/
     */
    public function transform($rrule)
    {
        return new Recurr\Rule($rrule);
    }

    public function reverseTransform($value)
    {
        if (!$value) {
            return false;
        }

        $rrule = [];

        foreach (explode(";", $value['RRULE']) as $parts) {
            $part = explode("=", $parts);

            switch ($part[0]) {
                case "FREQ":
                    $rrule['FREQ'] = $part[1];
                    break;
                case "BYDAY":
                    $rrule['BYDAY'] = implode(",", (array)$value['BYDAY']);
                    break;
                case "BYMONTHDAY":
                case "INTERVAL":
                    $rrule[$part[0]] = $value[$part[0]];
                    break;
            }
        }

        $rule = new Recurr\Rule($rrule);

        return $rule->getString();
    }
}