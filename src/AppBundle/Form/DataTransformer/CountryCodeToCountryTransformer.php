<?php

namespace AppBundle\Form\DataTransformer;

use AppBundle\Entity\Geography\Country;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class CountryCodeToCountryTransformer implements DataTransformerInterface
{
    private $em;

    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    /**
     * Transforms entity to country code
     *
     * @param  Country|null $country
     * @return string
     */
    public function transform($country)
    {
        if (null === $country) {
            return '';
        }

        return $country->getCode();
    }

    /**
     * Transforms a string (Country code) to a Country Entity).
     *
     * @param  string $countryCode
     * @return Country|null
     * @throws TransformationFailedException if country is not found.
     */
    public function reverseTransform($countryCode)
    {
        $country = $this->em
            ->getRepository(Country::class)
            // query for the issue with this id
            ->find($countryCode);

        if (null === $country) {
            throw new TransformationFailedException(sprintf(
                'A country with code "%s" does not exist!',
                $country
            ));
        }

        return $country;
    }
}
