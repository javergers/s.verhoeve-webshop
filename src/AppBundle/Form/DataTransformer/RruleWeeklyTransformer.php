<?php

namespace AppBundle\Form\DataTransformer;

use Recurr\Exception\InvalidRRule;
use Recurr\Rule AS RRule;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class RruleWeeklyTransformer
 * @package AppBundle\Form\DataTransformer
 */
class RruleWeeklyTransformer implements DataTransformerInterface
{

    /**
     * @param mixed $value
     * @return array
     */
    public function transform($value)
    {
        $daysOfTheWeek = [
            true,
            true,
            true,
            true,
            true,
            false,
            false,
        ];

        if ($value === null || false === ($value instanceof RRule)) {
            return $daysOfTheWeek;
        }

        $days = $value->getByDay() ?? [];

        $daysOfTheWeek = [
            in_array('MO', $days, true),
            in_array('TU', $days, true),
            in_array('WE', $days, true),
            in_array('TH', $days, true),
            in_array('FR', $days, true),
            in_array('SA', $days, true),
            in_array('SU', $days, true),
        ];

        return $daysOfTheWeek;
    }

    /**
     * @param mixed $value
     * @return string
     * @throws InvalidRRule
     */
    public function reverseTransform($value)
    {
        $baseRrule = 'FREQ=WEEKLY;INTERVAL=1;DTSTART=20190101';
        if (!is_array($value)) {
            return $baseRrule;
        }

        $daysOfTheWeek = [
            'MO',
            'TU',
            'WE',
            'TH',
            'FR',
            'SA',
            'SU',
        ];
        $days = [];
        foreach ($value as $column) {
            foreach ($column as $key => $day) {
                $days[] = $daysOfTheWeek[$key];
            }
        }
        if (count($days) > 0) {
            $rrule = RRule::createFromString($baseRrule);
            $rrule->setByDay($days);
            return $rrule->getString();
        }
        return $baseRrule;
    }
}
