<?php

namespace AppBundle\Form\DataTransformer;

use DateTime;
use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class TimeTransformer
 * @package AppBundle\Form\DataTransformer
 */
class TimeTransformer implements DataTransformerInterface
{
    /**
     * @param mixed $time
     * @return mixed|null
     * @throws \Exception
     */
    public function transform($time)
    {
        if (null === $time) {
            return '';
        }

        /** @var DateTime $from */
        return $time->format('H:i');
    }

    /**
     * @param mixed $time
     * @return mixed|null
     * @throws \Exception
     */
    public function reverseTransform($time)
    {
        if (! $time) {
            return null;
        }

        return new DateTime($time);
    }
}
