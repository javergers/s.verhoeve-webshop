<?php

namespace AppBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class IpTransformer implements DataTransformerInterface
{
    public function transform($ip)
    {
        return long2ip($ip);
    }

    public function reverseTransform($ip)
    {
        return ip2long($ip);
    }
}