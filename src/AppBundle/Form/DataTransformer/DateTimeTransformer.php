<?php

namespace AppBundle\Form\DataTransformer;

use DateTime;
use Symfony\Component\Form\DataTransformerInterface;

class DateTimeTransformer implements DataTransformerInterface
{
    public function transform($datetime)
    {
        if ($datetime === null) {
            return null;
        }

        return $datetime->format("Y-m-d H:i:s");
    }

    public function reverseTransform($datetime)
    {
        if ($datetime === null) {
            return null;
        }

        return new DateTime($datetime);
    }
}