<?php

namespace AppBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ValidationTypeExtension extends AbstractTypeExtension
{

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {

        if (isset($options['client_validation'])) {
            $validationResolver = new OptionsResolver();


            $validationResolver->setDefined([
                'validation',
                'validation-allowing',
                'validation-format',
                'validation-qty',
                'validation-regexp',
                'validation-optional',
                'validation-if-checked',
                'validation-help',
                'suggestions',
                'validation-length',
                'validation-error-msg',
                'validation-url',
                'validation-param-name',
                'validation-req-params',
            ]);

            $validationResolver->setDefaults([
                'validation' => null,
                'validation-allowing' => null,
                'validation-format' => null,
                'validation-qty' => null,
                'validation-regexp' => null,
                'validation-optional' => null,
                'validation-if-checked' => null,
                'validation-help' => null,
                'suggestions' => null,
                'validation-length' => null,
                'validation-error-msg' => null,
                'validation-url' => null,
                'validation-param-name' => null,
                'validation-req-params' => null,
            ]);

            if (empty($options['client_validation']['validation-error-msg']) && !empty($options['invalid_message'])) {
                $options['client_validation']['validation-error-msg'] = $options['invalid_message'];
            }

            if (empty($options['client_validation'])) {
                $view->vars['attr']['data-ignored'] = true;
            }

            $view->vars['client_validation'] = $options['client_validation'];
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined('client_validation');
        $resolver->setAllowedTypes('client_validation', ['array']);
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return FormType::class;
    }
}
