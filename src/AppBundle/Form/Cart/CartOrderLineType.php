<?php

namespace AppBundle\Form\Cart;

use AppBundle\Entity\Order\CartOrderLine;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CartOrderLineType
 * @package AppBundle\Form\Cart
 */
class CartOrderLineType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('quantity', ChoiceType::class, [
                'label' => false,
                'choices' => [
                ],
            ]);

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            /** @var CartOrderLine $cartOrderLine */
            $cartOrderLine = $event->getForm()->getData();

            if ($cartOrderLine) {
                $orderableQuantities = $cartOrderLine->getOrderableQuantities();

                $choices = [];

                foreach ($orderableQuantities as $key => $value) {
                    $choices[$key] = $value;
                }

                if ($orderableQuantities) {
                    $data = $event->getForm()->get('quantity')->getData();

                    $event->getForm()->remove('quantity');
                    $event->getForm()->add('quantity', ChoiceType::class, [
                        'label' => false,
                        'choices' => $choices,
                        'data' => $data,
                    ]);
                }
            }
        });
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_cart_order_line';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CartOrderLine::class,
        ]);
    }
}
