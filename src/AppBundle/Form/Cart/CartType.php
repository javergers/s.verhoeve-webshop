<?php

namespace AppBundle\Form\Cart;

use AppBundle\Entity\Order\Cart;
use AppBundle\Form\Customer\LoginType;
use AppBundle\Services\CartService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class CartType
 * @package AppBundle\Form\Cart
 */
class CartType extends AbstractType
{
    /**
     * @var CartService
     */
    private $cartService;

    /**
     * CartType constructor.
     *
     * @param CartService $cartService
     */
    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
            $form = $event->getForm();

            if (null === $this->cartService->getCart()->getCompany()) {
                $form
                    ->add('login', LoginType::class, [
                        'label' => false,
                        'mapped' => false,
                    ])
                    ->add('customer', CustomerType::class, [
                        'label' => false,
                    ])
                    ->add('customerStatus', ChoiceType::class, [
                        'label' => false,
                        'choices' => [
                            'label.new_customer' => 'new',
                            'label.existing_customer' => 'existing',
                        ],
                        'data' => 'new',
                        'expanded' => true,
                        'mapped' => false,
                    ])
                    ->add('customerType', ChoiceType::class, [
                        'choices' => [
                            'label.customer' => 'customer',
                            'label.company' => 'company',
                        ],
                        'expanded' => true,
                        'mapped' => false,
                        'required' => true,
                        'attr' => [
                            'placeholder' => 'label.sender_type',
                        ]
                    ])
                    ->add('companyAccountRegistration', ChoiceType::class, [
                        'label' => false,
                        'choices' => [
                            'label.yes_please' => 1,
                            'label.no_thankyou' => 0,
                        ],
                        'expanded' => true,
                        'mapped' => false,
                        'attr' => [
                            'placeholder' => 'label.company.account_registration.title',
                        ]
                    ])
                    ->add('chamberOfCommerce', TextType::class, [
                        'label' => 'customer.company.chamber_of_commerce_number',
                        'mapped' => false,
                    ])
                    ->add('vatNumber', TextType::class, [
                        'label' => 'customer.company.vat_number',
                        'mapped' => false,
                    ]);
            } else {
                $form
                    ->add('customerNewsletter', CheckboxType::class, [
                        'label' => 'label.newsletter',
                        'required' => false,
                        'property_path' => 'customer.newsletter'
                    ]);
            }

            $form
                ->add('terms', CheckboxType::class, [
                    'label' => 'label.terms',
                    'mapped' => false,
                    'required' => true,
                ]);
        });

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            if ($event->getForm()->has('customerStatus') && ($event->getForm()->get('customerStatus')->getData() === 'existing' || $event->getForm()->get('login')->get('login_form_active')->getData())) {
                $event->stopPropagation();
            }
        }, 900);
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'app_cart';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Cart::class,
            'attr' => [
                'novalidate' => 'novalidate',
                'id' => 'cart',
                'data-client-validation' => true,
            ],
            'allow_extra_fields' => true,
        ]);
    }
}
