<?php

namespace AppBundle\Form\Cart;

use AppBundle\DBAL\Types\SalutationType;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Form\Customer\AddressType;
use AppBundle\Validator\Constraints\CustomerEmail;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Intl\Intl;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class CustomerType
 * @package AppBundle\Form\Cart
 */
class CustomerType extends AbstractType
{
    use ContainerAwareTrait;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        void($options);

        $country = 'NL';

        $token = $this->container->get('security.token_storage')->getToken();
        if (null !== $token && $token->getUser() instanceof Customer) {
            /** @var Customer $customer */
            $customer = $token->getUser();
            $address = null;
            
            if (null !== $customer->getDefaultInvoiceAddress()) {
                $address = $customer->getDefaultInvoiceAddress();
            }

            $company = $customer->getCompany();
            if(null !== $company && null !== $company->getInvoiceAddress()) {
                $address = $company->getInvoiceAddress();
            }

            if(null !== $address) {
                $country = $address->getCountry()->getCode();
            }
        }

        $genderChoices = [];

        foreach (SalutationType::getChoices() as $key => $value) {
            $genderChoices[$key] = $value;
        }

        $builder
            ->add('postcode_and_city', ChoiceType::class, [
                'label' => 'label.postcode_and_city',
                'mapped' => false,
                'required' => false,
                'placeholder' => 'label.postcode_and_city.placeholder',
                'translation_domain' => 'messages',
                'invalid_message' => 'address.postcode_and_city.not_valid',
            ])
            ->add('country_choice', ChoiceType::class, [
                'label' => false,
                'required' => true,
                'expanded' => true,
                'multiple' => false,
                'choices' => [
                    Intl::getRegionBundle()->getCountryName('NL') => 'NL',
                    Intl::getRegionBundle()->getCountryName('BE') => 'BE',
                    'customer.address.country.other' => 'other',
                ],
                'data' => strtoupper($country),
                'choice_translation_domain' => false,
                'mapped' => false,
                'attr' => [
                    'class' => 'country-choice',
                ],
            ])
            ->add('gender', ChoiceType::class, [
                'label' => 'label.salutation',
                'required' => true,
                'choices' => $genderChoices,
                'expanded' => true,
                'multiple' => false,
                'invalid_message' => 'customer.account.gender.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ])
            ->add('firstname', TextType::class, [
                'label' => 'label.firstname',
                'required' => true,
                'invalid_message' => 'customer.account.firstname.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ])
            ->add('lastnamePrefix', TextType::class, [
                'label' => 'label.lastname_prefix',
                'required' => false,
            ])
            ->add('lastname', TextType::class, [
                'label' => 'label.lastname',
                'required' => true,
                'invalid_message' => 'customer.account.lastname.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ]);

        $address = new Address();
        $address->setMainAddress(true);

        $builder
            ->add('default_invoice_address', AddressType::class, [
                'label' => false,
            ])
            ->add('phonenumber', TextType::class, [
                'label' => 'label.phonenumber',
                'required' => false,
                'invalid_message' => 'customer.account.phonenumber.not_valid',
            ])
            ->add('email', EmailType::class, [
                'label' => false,
                'required' => true,
                'invalid_message' => 'customer.account.email.not_valid',
                'client_validation' => [
                    'validation' => 'email',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'customer.account.email.not_blank',
                    ]),
                ],
            ])
            ->add('plain_password', PasswordType::class, [
                'label' => 'label.password',
                'required' => false,
                'invalid_message' => 'customer.account.password.not_valid',
            ])
            ->add('newsletter', CheckboxType::class, [
                'label' => 'label.newsletter',
                'required' => false,
            ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $form = $event->getForm();

            $cart = $this->container->get('request_stack')->getCurrentRequest()->request->get('cart');

            if ($cart) {
                $postcode_and_city_element_options = [
                    'label' => 'label.postcode_and_city',
                    'translation_domain' => 'messages',
                    'mapped' => false,
                    'required' => true,
                    'placeholder' => 'label.postcode_and_city.placeholder',
                ];

                if (!empty($cart['customer']['country_choice']) && $cart['customer']['country_choice'] === 'BE') {
                    $notBlank = new NotBlank();
                    $notBlank->message = 'address.postcode_and_city.not_blank';

                    $element_options['constraints'] = $notBlank;

                    $form->add('postcode_and_city', ChoiceType::class, $postcode_and_city_element_options);
                }

                if (!empty($cart['customer']['postcode_and_city'])) {
                    $postcode_and_city_element_options['choices'] = [
                        $cart['customer']['postcode_and_city'] => $cart['customer']['postcode_and_city'],
                    ];

                    $form->add('postcode_and_city', ChoiceType::class, $postcode_and_city_element_options);
                }

                /** @var EntityManagerInterface $em */
                $em = $this->container->get('doctrine.orm.entity_manager');

                $customerEmailValidator = new CustomerEmail($em);
                $customerEmailValidator->message = 'cart.customer.email.not_allowed';

                $email_element_options = [
                    'label' => false,
                    'required' => true,
                    'invalid_message' => 'customer.account.email.not_valid',
                    'client_validation' => [
                        'validation' => 'email',
                    ],
                    'constraints' => [
                        new NotBlank([
                            'message' => 'customer.account.email.not_blank',
                        ]),
                        $customerEmailValidator,
                    ],
                ];

                if (!empty($cart['customer']['email']) && (!empty($cart['customerType']) && $cart['customerType'] === 'company')) {
                    $form->add('email', EmailType::class, $email_element_options);
                }
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
            'attr' => [
                'novalidate' => 'novalidate',
                'data-client-validation' => true,
            ],
            'validation_groups' => [
                'Default',
                'customer_registration',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'customer';
    }
}
