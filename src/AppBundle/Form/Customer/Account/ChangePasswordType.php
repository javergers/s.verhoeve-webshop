<?php

namespace AppBundle\Form\Customer\Account;

use AppBundle\Model\ChangePassword;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ChangePasswordType
 * @package AppBundle\Form\Customer
 *
 */
class ChangePasswordType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder->add('current', PasswordType::class, [
            'label' => 'label.current_password',
        ]);

        $builder->add('new', RepeatedType::class, [
            'required' => true,
            'type' => PasswordType::class,
            'first_options' => ['label' => 'label.new_password', 'required' => true],
            'second_options' => ['label' => 'label.new_password_confirmation'],
            'invalid_message' => 'customer.account.change_password.password.mismatch',
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ChangePassword::class,
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'site_customer_change_password';
    }
}
