<?php

namespace AppBundle\Form\Customer;

use AppBundle\DBAL\Types\SalutationType;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Validator\Constraints\CustomerEmail;
use AppBundle\Validator\Constraints\CustomerUsername;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class CompanyCustomerType extends AbstractType
{
    use ContainerAwareTrait;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $customerEmailValidator = new CustomerEmail($this->container->get('doctrine.orm.entity_manager'));
        $customerEmailValidator->message = 'customer.account.email.not_allowed';
        $customerEmailValidator->groups = ['company_registration'];

        $customerUsernameValidator = new CustomerUsername($this->container->get('doctrine.orm.entity_manager'));
        $customerUsernameValidator->groups = ['company_registration'];

        $genderChoices = [];

        foreach (SalutationType::getChoices() as $key => $value) {
            $genderChoices[$key] = $value;
        }

        $builder
            ->add('gender', ChoiceType::class, [
                'label' => 'label.salutation',
                'required' => true,
                'choices' => $genderChoices,
                'expanded' => true,
                'multiple' => false,
                'invalid_message' => 'customer.account.gender.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ])
            ->add('firstname', TextType::class, [
                'label' => 'label.firstname',
                'required' => true,
                'invalid_message' => 'customer.account.firstname.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ])
            ->add('lastname', TextType::class, [
                'label' => 'label.lastname',
                'required' => true,
                'invalid_message' => 'customer.account.lastname.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ])
            ->add('phonenumber', TextType::class, [
                'label' => 'label.phonenumber',
                'required' => true,
                'invalid_message' => 'customer.account.phonenumber.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ]);

        $builder
            ->add('username', EmailType::class, [
                'label' => 'label.email',
                'required' => true,
                'invalid_message' => 'customer.account.email.not_valid',
                'constraints' => [
                    new NotBlank([
                        'message' => 'customer.email.not_blank',
                        'groups' => ['company_registration'],
                    ]),
                    $customerEmailValidator,
                    $customerUsernameValidator,
                ],
                'client_validation' => [
                    'validation' => 'email',
                ],
            ])
            ->add('plain_password', PasswordType::class, [
                'label' => 'label.password',
                'required' => true,
                'constraints' => [
                    new NotBlank([
                        'message' => 'customer.password.not_blank',
                        'groups' => ['company_registration'],
                    ]),
                ],
                'invalid_message' => 'customer.account.password.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(['request']);

        $resolver->setDefaults([
            'data_class' => Customer::class,
            'attr' => [
                'novalidate' => 'novalidate',
            ],
//            'validation_groups' => array('company_registration')
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'site_customer_company_customer';
    }
}
