<?php

namespace AppBundle\Form\Customer;

use AppBundle\Entity\Relation\Address;
use AppBundle\Form\DataTransformer\CountryCodeToCountryTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AddressType
 * @package AppBundle\Form\Customer
 */
class AddressType extends AbstractType
{
    private $em;

    /**
     * @param ObjectManager $em
     */
    public function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        void($options);

        $builder
            ->add('companyName', TextType::class, [
                'label' => 'label.companyName',
                'required' => false,
            ]);

        $builder
            ->add('country_list', ChoiceType::class, [
                'label' => false,
                'required' => true,
                'choices' => array_flip(array_filter(Intl::getRegionBundle()->getCountryNames(), function ($code) {
                    return !\in_array(strtolower($code), ['nl', 'be']);
                }, ARRAY_FILTER_USE_KEY)),
                'choice_translation_domain' => false,
                'placeholder' => 'label.country.choose',
                'translation_domain' => 'messages',
                'mapped' => false,
                'attr' => [
                    'class' => 'country-list-other',
                ],
                'client_validation' => [
                    'validation' => 'required',
                ],
                'invalid_message' => 'address.invalid_message.country',
            ])
            ->add('country', CountryType::class, [
                'label' => false,
                'required' => true,
                'choices' => array_flip(Intl::getRegionBundle()->getCountryNames()),
                'choice_translation_domain' => false,
                'attr' => [
                    'class' => 'country-list',
                ],
            ])
            ->add('postcode', TextType::class, [
                'label' => 'label.postcode',
                'required' => true,
                'client_validation' => [
                    'validation' => 'alphanumeric',
                ],
                'invalid_message' => 'address.invalid_message.postcode',
            ])
            ->add('street', TextType::class, [
                'label' => 'label.street',
                'required' => true,
                'client_validation' => [
                    'validation' => 'required',
                ],
                'invalid_message' => 'address.invalid_message.street',
            ])
            ->add('number', TextType::class, [
                'label' => 'label.number',
                'client_validation' => [
                    'validation' => 'required',
                ],
                'invalid_message' => 'address.invalid_message.number',
            ])
            ->add('numberAddition', TextType::class, [
                'label' => 'label.housenumber_addition',
                'required' => false,
            ])
            ->add('city', TextType::class, [
                'label' => 'label.city',
                'client_validation' => [
                    'validation' => 'required',
                ],
                'invalid_message' => 'address.invalid_message.city',
            ]);

        $builder->get('country')
            ->addModelTransformer(new CountryCodeToCountryTransformer($this->em));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefined(['request']);

        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'site_customer_address';
    }
}
