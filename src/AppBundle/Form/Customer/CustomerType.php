<?php

namespace AppBundle\Form\Customer;

use AppBundle\DBAL\Types\SalutationType;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Form\DataTransformer\CountryCodeToCountryTransformer;
use AppBundle\Form\Type\AvailableLocaleType;
use AppBundle\Validator\Constraints\CustomerUsername;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Intl\Intl;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class CustomerType
 * @package AppBundle\Form\Customer
 */
class CustomerType extends AbstractType
{
    use ContainerAwareTrait;

    /** @var EntityManagerInterface $em */
    private $em;

    /** @var TranslatorInterface $translator */
    private $translator;

    /**
     * CustomerType constructor.
     *
     * @param EntityManagerInterface       $em
     * @param TranslatorInterface $translator
     */
    public function __construct(EntityManagerInterface $em, TranslatorInterface $translator)
    {
        $this->em = $em;

        $this->translator = $translator;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $customerUsernameValidator = new CustomerUsername($this->em);
        $customerUsernameValidator->message = null;

        $choices = [];

        foreach (SalutationType::getChoices() as $key => $value) {
            $choices[$key] = $value;
        }

        // TODO FIX
//        $placeHolder = null;
//
//        if ($builder->getData()->getMainAddress() && $builder->getData()->getMainAddress()->getPostcode() && $builder->getData()->getMainAddress()->getCity()) {
//            $placeHolder = $builder->getData()->getMainAddress()->getPostcode() . ' ' . $builder->getData()->getMainAddress()->getCity();
//        }

        $builder
            ->add('postcode_and_city', ChoiceType::class, [
                'label' => 'label.postcode_and_city',
                'translation_domain' => 'messages',
                'mapped' => false,
                'required' => false,
                'placeholder' => 'label.postcode_and_city.placeholder',
                'invalid_message' => 'address.postcode_and_city.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ])
            ->add('country_choice', ChoiceType::class, [
                'label' => 'label.country',
                'required' => true,
                'expanded' => true,
                'multiple' => false,
                'choices' => [
                    Intl::getRegionBundle()->getCountryName('NL') => 'NL',
                    Intl::getRegionBundle()->getCountryName('BE') => 'BE',
                    'customer.address.country.other' => 'other',
                ],
                'data' => (!$builder->getData()->getMainAddress()) ? 'NL' : (\in_array($builder->getData()->getMainAddress()->getCountry()->getId(),
                    ['NL', 'BE']) ? $builder->getData()->getMainAddress()->getCountry()->getId() : 'other'),
                'choice_translation_domain' => false,
                'mapped' => false,
                'attr' => [
                    'class' => 'country-choice',
                ],
            ])
            ->add('gender', ChoiceType::class, [
                'label' => 'label.salutation',
                'required' => true,
                'choices' => $choices,
                'expanded' => true,
                'multiple' => false,
                'invalid_message' => 'customer.account.gender.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ])
            ->add('firstname', TextType::class, [
                'label' => 'label.firstname',
                'required' => true,
                'invalid_message' => 'customer.account.firstname.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ])
            ->add('lastname', TextType::class, [
                'label' => 'label.lastname',
                'required' => true,
                'invalid_message' => 'customer.account.lastname.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ]);

        $address_options = [
            'label' => false,
            'entry_type' => AddressType::class,
            'entry_options' => [
                'required' => true,
            ],
        ];

        if ($options['intention'] === 'create') {
            $address = new Address();
            $address->setMainAddress(true);

            $address_options['data'] = [
                $address,
            ];
        } else {
            $address_options['data'] = [
                $builder->getForm()->getData()->getMainAddress(),
            ];
        }

        $builder
            ->add('addresses', CollectionType::class, $address_options);

        $builder
            ->add('phonenumber', TextType::class, [
                'label' => 'label.phonenumber',
                'required' => true,
                'invalid_message' => 'customer.account.phonenumber.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ])
            ->add('birthday', BirthdayType::class, [
                'label' => 'label.birthdate',
                'format' => 'd MMMM yyyy',
                'years' => array_reverse(range(date('Y') - 100, date('Y') - 13)),
                'required' => false,
                'invalid_message' => 'customer.birthday.not_valid',
                'client_validation' => [
                    'validation' => 'date',
                    'validation-format' => 'dd-mm-yyyy',
                    'validation-optional' => true,
                ],
            ]);


        if ($options['intention'] === 'create') {
            $builder
                ->add('username', EmailType::class, [
                    'label' => 'label.email',
                    'required' => true,
                    'invalid_message' => 'customer.account.email.not_valid',
                    'client_validation' => [
                        'validation' => 'email',
                    ],
                    'constraints' => [
//                        new NotBlank(array(
//                            'message' => 'customer.account.email.not_blank'
//                        )),
                        $customerUsernameValidator,
                    ],
                ])
                ->add('plain_password', PasswordType::class, [
                    'label' => 'label.password',
                    'required' => true,
                    'invalid_message' => 'customer.account.password.not_valid',
                    'client_validation' => [
                        'validation' => 'required',
                    ],
                    'constraints' => [
                        new NotBlank([
                            'message' => 'customer.account.password.not_blank',
                        ]),
                    ],
                ]);
        }

        $builder
            ->add('locale', AvailableLocaleType::class, [
                'label' => 'label.preferred_language',
                'required' => true,
            ]);

        $container = $this->container;
        $em = $this->em;

        $builder->addEventListener(FormEvents::POST_SET_DATA,
            function (FormEvent $event) use ($options, $em, $container) {
                $form = $event->getForm();

                if (isset($options['request']) && !empty($options['request']->query->get('email'))) {
                    $form->get('username')->setData($options['request']->query->get('email'));
                }

                if (isset($options['request']) && !empty($options['request']->query->get('chamberOfCommerce'))) {
                    try {
                        $countryCodeDataTransformer = new CountryCodeToCountryTransformer($em);
                        $chamberOfCommerceData = $container->get('webservices_nl')->getCompanyDataByChamberOfCommerceNumber($options['request']->query->get('chamberOfCommerce'));

                        $address = new Address();
                        $address->setMainAddress(true);
                        $address->setCompanyName($chamberOfCommerceData->name);
                        $address->setPostcode($chamberOfCommerceData->address->postcode);
                        $address->setCity($chamberOfCommerceData->address->city);
                        $address->setStreet($chamberOfCommerceData->address->street);
                        $address->setNumber(implode(' ', [
                            $chamberOfCommerceData->address->houseNumber,
                            $chamberOfCommerceData->address->houseNumberAddition,
                        ]));
                        $address->setCountry($countryCodeDataTransformer->reverseTransform($chamberOfCommerceData->address->countryCode));

                        $form->add('addresses', CollectionType::class, [
                            'entry_type' => AddressType::class,
                            'entry_options' => [
                                'required' => true,
                            ],
                            'data' => [
                                $address,
                            ],
                        ]);
                    } catch (\Exception $e) {
                    }
                }

                if (isset($options['request']) && !empty($options['request']->query->get('country')) && !empty($options['request']->query->get('vatNumber'))) {
                    try {
                        $countryCodeDataTransformer = new CountryCodeToCountryTransformer($em);
                        $vatData = $container->get('ec_europa_eu.vies')->getCompanyDataByVatNumber($options['request']->query->get('vatNumber'));

                        $form->add('country_choice', ChoiceType::class, [
                            'label' => 'label.country',
                            'required' => true,
                            'expanded' => true,
                            'multiple' => false,
                            'choices' => array_flip([
                                'NL' => Intl::getRegionBundle()->getCountryName('NL'),
                                'BE' => Intl::getRegionBundle()->getCountryName('BE'),
                                'other' => 'customer.address.country.other',
                            ]),
                            'data' => 'BE',
                            'choice_translation_domain' => false,
                            'mapped' => false,
                        ]);

                        $form->add('postcode_and_city', ChoiceType::class, [
                            'label' => 'label.postcode_and_city',
                            'translation_domain' => 'messages',
                            'choice_translation_domain' => false,
                            'mapped' => false,
                            'required' => true,
                            'placeholder' => 'label.postcode_and_city.placeholder',
                            'choices' => [
                                $vatData->address->postcode . ' ' . $vatData->address->city => $vatData->address->postcode . ' ' . $vatData->address->city,
                            ],
                            'data' => $vatData->address->postcode . ' ' . $vatData->address->city,
                        ]);

                        $address = new Address();
                        $address->setCompanyName($vatData->name);
                        $address->setMainAddress(true);
                        $address->setPostcode($vatData->address->postcode);
                        $address->setCity($vatData->address->city);
                        $address->setStreet($vatData->address->street);
                        $address->setNumber(implode(' ',
                            [$vatData->address->houseNumber, $vatData->address->houseNumberAddition]));
                        $address->setCountry($countryCodeDataTransformer->reverseTransform($vatData->address->countryCode));

                        $form->add('addresses', CollectionType::class, [
                            'entry_type' => AddressType::class,
                            'entry_options' => [
                                'required' => true,
                            ],
                            'data' => [
                                $address,
                            ],
                        ]);
                    } catch (\Exception $e) {
                    }
                }
            });

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($options) {
            $form = $event->getForm();

            $element_options = [
                'label' => 'label.postcode_and_city',
                'mapped' => false,
                'required' => true,
                'placeholder' => 'label.postcode_and_city.placeholder',
                'translation_domain' => 'messages',
            ];

            if (isset($options['request']) && !empty($options['request']->get($this->getBlockPrefix())['country_choice']) && $options['request']->get($this->getBlockPrefix())['country_choice'] === 'BE') {
                $notBlank = new NotBlank();
                $notBlank->message = 'address.postcode_and_city.not_blank';

                $element_options['constraints'] = $notBlank;

                $form->add('postcode_and_city', ChoiceType::class, $element_options);
            }

            if (isset($options['request']) && !empty($options['request']->get($this->getBlockPrefix())['postcode_and_city'])) {
                $element_options['choices'] = [
                    $options['request']->get($this->getBlockPrefix())['postcode_and_city'] => $options['request']->get($this->getBlockPrefix())['postcode_and_city'],
                ];

                $form->add('postcode_and_city', ChoiceType::class, $element_options);
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
            'attr' => [
                'novalidate' => 'novalidate',
                'data-client-validation' => true,
            ],
            'validation_groups' => [
                'Default',
                'customer_registration',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'customer';
    }
}
