<?php

namespace AppBundle\Form\Customer;

use AppBundle\Entity\Security\Customer\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class LoginType
 * @package AppBundle\Form\Customer
 */
class LoginType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', TextType::class, [
            'label' => 'label.email',
            'required' => false,
        ]);

        $builder->add('plain_password', PasswordType::class, [
            'label' => 'label.password',
            'required' => false,
        ]);

        $builder->add('login_form_active', HiddenType::class, [
            'required' => false,
            'mapped' => false,
        ]);

        $builder->add('submit', ButtonType::class, [
            'translation_domain' => 'messages',
            'label' => 'label.login',
            'attr' => [
                'class' => 'btn-theme',
            ],
        ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
            'attr' => [
                'novalidate' => 'novalidate',
                'data-client-validation' => true,
            ],
            'validation_groups' => [
                'Default',
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app_cart';
    }
}
