<?php

namespace AppBundle\Form\Company;

use AppBundle\DBAL\Types\SalutationType;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Validator\Constraints\CustomerUsername;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class CustomCustomerFormType
 * @package AppBundle\Form\Company
 */
class CustomCustomerFormType extends AbstractType
{
    use ContainerAwareTrait;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->em = $this->container->get('doctrine')->getManager();

        $customerUsernameValidator = new CustomerUsername($this->em);
        $customerUsernameValidator->message = null;

        $genderChoices = [];

        foreach (SalutationType::getChoices() as $key => $value) {
            $genderChoices[$key] = $value;
        }

        $builder
            ->add('gender', ChoiceType::class, [
                'label' => 'label.salutation',
                'required' => true,
                'choices' => $genderChoices,
                'expanded' => true,
                'multiple' => false,
                'invalid_message' => 'customer.account.gender.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ])
            ->add('firstname', TextType::class, [
                'label' => 'label.firstname',
                'required' => true,
                'invalid_message' => 'customer.account.firstname.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ])
            ->add('lastname', TextType::class, [
                'label' => 'label.lastname',
                'required' => true,
                'invalid_message' => 'customer.account.lastname.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ])
            ->add('phonenumber', TextType::class, [
                'label' => 'label.phonenumber',
                'required' => true,
                'invalid_message' => 'customer.account.phonenumber.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
            ])
            ->add('username', EmailType::class, [
                'label' => 'label.email',
                'required' => true,
                'invalid_message' => 'customer.account.email.not_valid',
                'client_validation' => [
                    'validation' => 'email',
                ],
                'constraints' => [
                    $customerUsernameValidator,
                ],
            ])
            ->add('plain_password', PasswordType::class, [
                'label' => 'label.password',
                'required' => true,
                'invalid_message' => 'customer.account.password.not_valid',
                'client_validation' => [
                    'validation' => 'required',
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'customer.account.password.not_blank',
                    ]),
                ],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
            'attr' => [
                'novalidate' => 'novalidate',
                'data-client-validation' => true,
            ],
            'validation_groups' => [
                'Default',
                'customer_registration',
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'custom_customer';
    }
}