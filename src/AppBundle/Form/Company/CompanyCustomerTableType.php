<?php

namespace AppBundle\Form\Company;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyCustomerTableType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'Voornaam',
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Achternaam',
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mailadres',
                'disabled' => (strpos($_SERVER['REQUEST_URI'], "bewerken") !== false),
            ])
            ->add('phonenumber', TextType::class, [
                'label' => 'Telefoonnummer',
            ])
            ->add('mobilenumber', TextType::class, [
                'label' => 'Mobielnummer',
            ])
            ->add('id', HiddenType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
        ]);
    }
}
