<?php

namespace AppBundle\Exceptions;

/**
 * Class ImageUploadException
 * @package AppBundle\Exceptions
 */
class ImageUploadException extends \Exception
{
}
