<?php

namespace AppBundle\Exceptions;

/**
 * Class CustomerDisabledException
 * @package AppBundle\Exceptions
 */
class CustomerDisabledException extends \Exception
{

}