<?php

namespace AppBundle\Exceptions;

/**
 * Class CustomerNotFoundException
 * @package AppBundle\Exceptions
 */
class CustomerNotFoundException extends \Exception
{

}