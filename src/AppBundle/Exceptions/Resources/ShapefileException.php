<?php

namespace AppBundle\Exceptions\Resources;

/**
 * Class ShapefileException
 * @package AppBundle\Exceptions\Resources
 */
class ShapefileException extends \RuntimeException
{
}