<?php

namespace AppBundle\Exceptions\Discount;

/**
 * Class InvalidVoucherCodeException
 * @package AppBundle\Exceptions\Discount
 */
class InvalidVoucherCodeException extends \Exception
{

}
