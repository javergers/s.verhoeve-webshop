<?php

namespace AppBundle\Exceptions;

/**
 * Class CustomerPasswordAlreadyRequestedException
 * @package AppBundle\Exceptions
 */
class CustomerPasswordAlreadyRequestedException extends \Exception
{

}