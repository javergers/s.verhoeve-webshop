<?php

namespace AppBundle\Exceptions;

/**
 * Class InvalidHousenumberException
 * @package AppBundle\Exceptions
 */
class InvalidBuildingNumberException extends \Exception
{
}