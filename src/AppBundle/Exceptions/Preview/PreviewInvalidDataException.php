<?php

namespace AppBundle\Exceptions\Preview;

class PreviewInvalidDataException extends \Exception
{
    protected $message = "Invalid preview data";
}