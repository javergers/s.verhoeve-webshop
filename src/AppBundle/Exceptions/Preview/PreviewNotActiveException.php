<?php

namespace AppBundle\Exceptions\Preview;

class PreviewNotActiveException extends \Exception
{
    protected $message = "Invalid Request: Preview is not active";
}