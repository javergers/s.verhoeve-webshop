<?php

namespace AppBundle\Exceptions;

/**
 * Class InvalidOrderStatusHashException
 * @package AppBundle\Exceptions
 * @deprecated
 */
class InvalidOrderStatusHashException extends \Exception
{
    protected $code = 422;
    protected $message = 'Invalid hash';
}
