<?php

namespace AppBundle\Exceptions\Activity;

use Exception;

/**
 * Class TableNotSupportedException
 * @package AppBundle\Exceptions\Activity
 */
class TableNotSupportedException extends Exception
{
}
