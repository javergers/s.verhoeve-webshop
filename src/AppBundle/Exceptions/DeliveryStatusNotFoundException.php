<?php

namespace AppBundle\Exceptions;

class DeliveryStatusNotFoundException extends \Exception
{
    protected $code = 422;
    protected $message = 'DeliveryStatus not found';
}
