<?php

namespace AppBundle\Exceptions\Common;

/**
 * Class CommandContextException
 * @package AppBundle\Exceptions\Common
 */
class CommandContextException extends \RuntimeException
{
}
