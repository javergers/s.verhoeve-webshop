<?php

namespace AppBundle\Exceptions\Common;

/**
 * Class SpreadsheetInvalidExtensionException
 * @package AppBundle\Exceptions\Common
 */
class SpreadsheetInvalidExtensionException extends \Exception
{
}