<?php
namespace AppBundle\Exceptions\Designer;

use Exception;

/**
 * Class DesignerProcessException
 * @package AppBundle\Exceptions\Designer
 */
class ProcessDesignException extends Exception
{
    /**
     * @var string
     */
    protected $message = "Can't process the design";
}