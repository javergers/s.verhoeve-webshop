<?php

namespace AppBundle\Events;

/**
 * Class CustomerEvents
 * @package App\Events
 *
 * Contains all customer related events thrown in the application.
 */
final class CustomerEvents
{
    /**
     * The CREATED event occurs when the customer is created with the CustomerService.
     *
     * This event allows you to access the created customer and to add some behaviour after the creation.
     *
     * @Event("App\Event\CustomerEvent")
     */
    public const CREATED = 'customer.created';
    
    /**
     * The MODIFIED event occurs when the customer is modified with the CustomerService.
     *
     * This event allows you to access the modified customer and to add some behaviour after the modification.
     *
     * @Event("App\Event\CustomerEvent")
     */
    public const MODIFIED = 'customer.modified';

    /**
     * The REMOVED event occurs when the customer is removed with the CustomerService.
     *
     * This event allows you to access the removed customer and to add some behaviour after the removal.
     *
     * @Event("App\Event\CustomerEvent")
     */
    public const REMOVED = 'customer.removed';

    /**
     * The SECURITY_IMPLICIT_LOGIN event occurs when the customer is logged in programmatically.
     *
     * This event allows you to access the response which will be sent.
     *
     * @Event("App\Event\CustomerEvent")
     */
    public const SECURITY_IMPLICIT_LOGIN = 'customer.security.implicit_login';

    /**
     * The PASSWORD_CHANGED event occurs when the customer is created with the CustomerService.
     *
     * This event allows you to access the created customer and to add some behaviour after the password change.
     *
     * @Event("App\Event\CustomerEvent")
     */
    public const PASSWORD_CHANGED = 'customer.password.changed';

    /**
     * The ACTIVATED event occurs when the customer is created with the CustomerService.
     *
     * This event allows you to access the activated customer and to add some behaviour after the activation.
     *
     * @Event("App\Event\CustomerEvent")
     */
    public const ACTIVATED = 'customer.activated';

    /**
     * The CONVERTED_TO_CUSTOMER_ACCOUNT event occurs when the customer without an account is converted to a customer with an account with the CustomerService.
     *
     * This event allows you to access the converted customer and to add some behaviour after the conversion.
     *
     * @Event("App\Event\CustomerEvent")
     */
    public const CONVERTED_TO_CUSTOMER_ACCOUNT = 'customer.convert.to_customer_account';

    /**
     * The CONVERTED_TO_COMPANY_ACCOUNT event occurs when the customer account is converted to a company account with the CustomerService.
     *
     * This event allows you to access the converted customer and company and to add some behaviour after the conversion.
     *
     * @Event("App\Event\CustomerEvent")
     */
    public const CONVERTED_TO_COMPANY_ACCOUNT = 'customer.convert.to_company_account';

    /**
     * The DEACTIVATED event occurs when the customer is created with the CustomerService.
     *
     * This event allows you to access the deactivated customer and to add some behaviour after the deactivation.
     *
     * @Event("App\Event\CustomerEvent")
     */
    public const DEACTIVATED = 'customer.deactivated';

    /**
     * The ROLE_ADDED event occurs when a role is added for the customer with the CustomerService.
     *
     * This event allows you to access the customer and to add some behaviour after adding the role.
     *
     * @Event("App\Event\CustomerEvent")
     */
    public const ROLE_ADDED = 'customer.role.added';

    /**
     * The ROLE_REMOVED event occurs when a role is removed for the customer with the CustomerService.
     *
     * This event allows you to access the customer and to add some behaviour after removing the role.
     *
     * @Event("App\Event\CustomerEvent")
     */
    public const ROLE_REMOVED = 'customer.role.removed';
}

