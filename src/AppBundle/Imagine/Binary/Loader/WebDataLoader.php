<?php

namespace AppBundle\Imagine\Binary\Loader;

use AppBundle\Traits\Imagine\WebDataTrait;
use League\Flysystem\Filesystem;
use Liip\ImagineBundle\Binary\BinaryInterface;
use Liip\ImagineBundle\Binary\Loader\LoaderInterface;
use Liip\ImagineBundle\Model\Binary;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class WebDataLoader
 * @package AppBundle\Imagine\Binary\Loader
 */
class WebDataLoader implements LoaderInterface
{
    use ContainerAwareTrait;
    use WebDataTrait;

    /**
     * @var LoaderInterface
     */
    private $defaultDataLoader;

    /**
     * @var Filesystem
     */
    private $publicFilesystem;

    /**
     * @var Filesystem
     */
    private $privateFilesystem;

    /**
     * @var string
     */
    private $objectStoreUrl;

    /**
     * WebResolver constructor.
     * @param LoaderInterface $filesystem
     * @param Filesystem      $publicFilesystem
     * @param Filesystem      $privateFilesystem
     * @param                 $cache
     */
    public function __construct(
        LoaderInterface $filesystem,
        Filesystem $publicFilesystem,
        Filesystem $privateFilesystem,
        $cache
    ) {
        $this->defaultDataLoader = $filesystem;
        $this->publicFilesystem = $publicFilesystem;
        $this->privateFilesystem = $privateFilesystem;

        $this->cache = $cache;
    }

    /**
     * @param mixed $path
     *
     * @return BinaryInterface
     */
    public function find($path)
    {
        if (!$this->objectStoreUrl) {
            $this->objectStoreUrl = (string)$this->container->getParameter('openstack_object_store');
        }

        if ($remoteContent = $this->getRemoteContent($path)) {

            $finfo = new \finfo(FILEINFO_MIME);
            $mime = $finfo->buffer($remoteContent);
            $fileExtention = substr($path, strrpos($path, '.') + 1);

            $format = ($fileExtention === 'jpg' ? 'jpeg' : $fileExtention);

            // return binary instance with data
            return new Binary($remoteContent, $mime, $format);
        }

        [$path] = explode('?', $path,2);

        return $this->defaultDataLoader->find($path);
    }
}
