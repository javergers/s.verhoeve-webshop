<?php

namespace AppBundle\Imagine\Binary\Loader;

use League\Flysystem\FileNotFoundException;
use Liip\ImagineBundle\Binary\BinaryInterface;
use Liip\ImagineBundle\Binary\Loader\LoaderInterface;
use Liip\ImagineBundle\Model\Binary;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ObjectstoreLoader
 * @package AppBundle\Imagine\Binary\Loader
 */
class ObjectstoreLoader implements LoaderInterface
{
    use ContainerAwareTrait;

    private static $formats = ['gif', 'jpeg', 'png', 'wbmp', 'xbm'];

    /**
     * @param mixed $path
     *
     * @return BinaryInterface
     * @throws FileNotFoundException
     */
    public function find($path)
    {
        $path = str_replace($this->container->getParameter('openstack_object_store'), '', $path);

        if (0 === strpos($path, '/private/')) {
            $path = str_replace('/private/', '', $path);
            $publicStore = $this->container->get('filesystem_private');
        } else {
            $path = str_replace('/public/', '', $path);
            $publicStore = $this->container->get('filesystem_public');
        }

        // Stip url params
        $path = explode('?', $path)[0];

        if ($publicStore->has($path)) {
            $data = $publicStore->read($path);
            $mime = $publicStore->getMimetype($path);

            $fileExtention = substr($path, strrpos($path, '.') + 1);

            $format = ($fileExtention === 'jpg' ? 'jpeg' : $fileExtention);

            if (!\in_array($format, self::$formats, true)) {
                throw new NotFoundHttpException('Image doesn\'t exist');
            }

            // return binary instance with data
            return new Binary($data, $mime, $format);
        }

        throw new NotFoundHttpException('Image doesnt exists');
    }
}

