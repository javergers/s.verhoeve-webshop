<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Services\SiteService;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class OpeningHoursExtension
 * @package AppBundle\Twig\Extension
 */
class OpeningHoursExtension extends AbstractExtension
{
    /**
     * @var SiteService $site
     */
    private $site;

    /**
     * OpeningHoursExtension constructor.
     * @param SiteService $site
     */
    public function __construct(SiteService $site)
    {
        $this->site = $site;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('app_site_opening_hours', function ($site = null) {
                return $this->site->getOpeningHours($site);
            }),
        ];
    }
}
