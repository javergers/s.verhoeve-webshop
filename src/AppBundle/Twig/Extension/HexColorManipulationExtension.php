<?php

namespace AppBundle\Twig\Extension;

use Twig_Extension;
use Twig_SimpleFilter;

/**
 * Class HexColorManipulationExtension
 * @package AppBundle\Twig\Extension
 */
class HexColorManipulationExtension extends Twig_Extension
{
    /**
     * @param string $hexcode
     * @param int $percentage
     *
     * @return string
     */
    public function darken($hexcode, int $percentage = 20)
    {
        if (!preg_match('/^#?([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i', $hexcode, $parts)) {
            throw new \RuntimeException('Not a value color');
        }

        $out = ''; // Prepare to fill with the results
        for ($i = 1; $i <= 3; $i++) {
            $parts[$i] = hexdec($parts[$i]);
            $parts[$i] = round($parts[$i] * (100 - $percentage) / 100);
            $out .= str_pad(dechex($parts[$i]), 2, '0', STR_PAD_LEFT);
        }
        return "#{$out}";
    }

    /**
     * @param string $hexcode
     * @param int $percentage
     *
     * @return string
     */
    public function lighten($hexcode, int $percentage = 20)
    {
        if (!preg_match('/^#?([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$/i', $hexcode, $parts)) {
            throw new \RuntimeException('Not a value color');
        }
        $out = ''; // Prepare to fill with the results
        for ($i = 1; $i <= 3; $i++) {
            $parts[$i] = hexdec($parts[$i]);
            $parts[$i] = round($parts[$i] * (100 + $percentage) / 100);
            $out .= str_pad(dechex($parts[$i]), 2, '0', STR_PAD_LEFT);
        }
        return "#{$out}";
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new Twig_SimpleFilter('darken', [$this, 'darken']),
            new Twig_SimpleFilter('lighten', [$this, 'lighten']),
        ];
    }
}