<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Decorator\OrderDecorator;
use AppBundle\Entity\Order\Order;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class OrderOrderDecoratorExtension
 * @package AppBundle\Twig\Extension
 */
class OrderOrderDecoratorExtension extends \Twig_Extension
{
    use ContainerAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $options = [
            'is_safe' => ['html'],
            'needs_environment' => true,
        ];

        $functions = [
            new \Twig_SimpleFunction('topbloemen_order_order_decorator', [$this, 'getOrderOrderDecorator'], $options),
        ];

        return $functions;
    }

    /**
     * @param \Twig_Environment $env
     * @param Order             $order
     *
     * @return OrderDecorator
     */
    public function getOrderOrderDecorator(\Twig_Environment $env, Order $order)
    {
        $orderOrderDecorator = $this->container->get('app.order_order');
        $orderOrderDecorator->setOrderOrder($order);

        return $orderOrderDecorator;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'topbloemen_site_order_order_decorator';
    }
}
