<?php

namespace AppBundle\Twig\Extension;

/**
 * Class InstanceofExtension
 * @package AppBundle\Twig\Extension
 */
class InstanceofExtension extends \Twig_Extension
{

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('instanceof', [$this, 'isInstanceof']),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getTests()
    {
        return [
            new \Twig_SimpleTest('instanceof', [$this, 'isInstanceof']),
        ];
    }

    /**
     * @param $var
     * @param $instance
     * @return bool
     */
    public function isInstanceof($var, $instance) {
        try {
            $reflectionClass = new \ReflectionClass($instance);

            return $reflectionClass->isInstance($var) && (!isset($var->__isInitialized__) || $var->__isInitialized__ === null || $var->__isInitialized__ !== false);
        } catch (\ReflectionException $e) {
            return false;
        }
    }
}
