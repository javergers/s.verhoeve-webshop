<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Manager\Supplier\SupplierOrderManager;

/**
 * Class OrderLineManagerExtension
 * @package AppBundle\Twig\Extension
 */
class SupplierOrderManagerExtension extends \Twig_Extension
{
    /**
     * @var SupplierOrderManager
     */
    protected $supplierOrderManager;

    /**
     * PriceExtension constructor.
     * @param SupplierOrderManager $supplierOrderManager
     */
    public function __construct(SupplierOrderManager $supplierOrderManager)
    {
        $this->supplierOrderManager = $supplierOrderManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $functions = [
            new \Twig_SimpleFunction('supplier_order_manager_hasForwardPricesToShow', [$this, 'hasForwardPricesToShow']),
        ];

        return $functions;
    }

    /**
     * @param SupplierOrder $supplierOrder
     * @return float|null
     */
    public function hasForwardPricesToShow(SupplierOrder $supplierOrder)
    {
        return $this->supplierOrderManager->hasForwardPriceToShow($supplierOrder);
    }
}
