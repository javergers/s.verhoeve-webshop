<?php

namespace AppBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class GoogleAnalyticsExtension
 * @package AppBundle\Twig\Extension
 */
class GoogleAnalyticsExtension extends \Twig_Extension
{
    use ContainerAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $options = [
            'is_safe' => ['html'],
            'needs_environment' => true,
        ];

        $functions = [
            new \Twig_SimpleFunction('ga_datalayer', [$this, 'ga_datalayer'], $options),
            new \Twig_SimpleFunction('ga_events', [$this, 'ga_events'], $options),
        ];

        return $functions;
    }

    /**
     * @param \Twig_Environment $env
     * @return string
     * @throws \Twig_Error
     */
    public function ga_datalayer(\Twig_Environment $env)
    {
        void($env);
        return $this->container->get('twig')->render('@App/google-analytics-datalayer.html.twig', [
            'datalayer' => $this->container->get('app.google_analytics')->dataLayer,
        ]);
    }

    /**
     * @param \Twig_Environment $env
     * @return string
     * @throws \Twig_Error
     */
    public function ga_events(\Twig_Environment $env)
    {
        void($env);

        return $this->container->get('twig')->render('@App/google-analytics-events.html.twig', [
            'events' => $this->container->get('app.google_analytics')->getEvents(),
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'topbloemen_google_analytics';
    }
}
