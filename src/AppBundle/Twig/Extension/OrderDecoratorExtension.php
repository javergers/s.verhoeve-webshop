<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Decorator\OrderDecorator;
use AppBundle\Entity\Order\OrderCollection;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class OrderDecoratorExtension
 * @package AppBundle\Twig\Extension
 */
class OrderDecoratorExtension extends \Twig_Extension
{
    use ContainerAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $options = [
            'is_safe' => ['html'],
            'needs_environment' => true,
        ];

        $functions = [
            new \Twig_SimpleFunction('topbloemen_order_decorator', [$this, 'getOrderDecorator'], $options),
        ];

        return $functions;
    }

    /**
     * @param \Twig_Environment $env
     * @param OrderCollection   $orderCollection
     *
     * @return OrderDecorator
     */
    public function getOrderDecorator(\Twig_Environment $env, OrderCollection $orderCollection)
    {
        void($env);
        $orderDecorator = $this->container->get('app.order');
        $orderDecorator->setOrder($orderCollection);

        return $orderDecorator;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_order_decorator';
    }
}
