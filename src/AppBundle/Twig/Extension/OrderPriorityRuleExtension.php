<?php

namespace AppBundle\Twig\Extension;

use Twig\Extension\AbstractExtension;

/**
 * Class OrderPriorityRuleExtension
 * @package AppBundle\Twig
 */
class OrderPriorityRuleExtension extends AbstractExtension
{
    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('calculate_priority', [$this, 'calculate']),
        ];
    }

    /**
     * @param float $priority
     * @return array
     */
    public function calculate(float $priority)
    {
        $attribute = [
            'class' => '',
            'text' => '',
        ];

        if ($priority >= 75) {
            $attribute['class'] = 'danger';
            $attribute['text'] = 'Zeer hoge';
        } elseif ($priority >= 50) {
            $attribute['class'] = 'warning';
            $attribute['text'] = 'Hoge';
        } elseif ($priority >= 25) {
            $attribute['class'] = 'success';
            $attribute['text'] = 'Verhoogde';
        }

        return $attribute;
    }
}
