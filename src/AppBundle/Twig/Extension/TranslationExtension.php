<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Utils\Translation;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class TranslationExtension
 * @package AppBundle\Twig\Extension
 */
class TranslationExtension extends \Twig_Extension
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var Translation
     */
    private $translationUtil;

    /**
     * @param TranslatorInterface $translator
     * @param Translation         $translationUtil
     */
    public function __construct(TranslatorInterface $translator, Translation $translationUtil)
    {
        $this->translator = $translator;
        $this->translationUtil = $translationUtil;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return [
            new \Twig_SimpleFilter('trans', [$this, 'trans'], ['is_safe' => ['all']]),
            new \Twig_SimpleFilter('transchoice', [$this, 'transChoice'], ['is_safe' => ['all']]),
        ];
    }

    /**
     * @param string $message
     * @param array  $arguments
     * @param null   $domain
     * @param null   $locale
     * @return string
     */
    public function trans(string $message, array $arguments = [], $domain = null, $locale = null): string
    {
        $arguments = array_map('htmlspecialchars', $arguments);

        $translatedString = $this->translator->trans($message, $arguments, $domain, $locale);

        return $this->translationUtil->sanitizeTranslationString($translatedString);
    }

    /**
     * @param string $message
     * @param int    $number
     * @param array  $arguments
     * @param null   $domain
     * @param null   $locale
     *
     * @return string
     */
    public function transChoice(string $message, int $number, array $arguments = [], $domain = null, $locale = null): string
    {
        $arguments = array_map('htmlspecialchars', $arguments);

        $translatedString = $this->translator->transChoice($message, $number, $arguments, $domain, $locale);

        return $this->translationUtil->sanitizeTranslationString($translatedString);
    }
}
