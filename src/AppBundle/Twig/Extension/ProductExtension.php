<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Decorator\Catalog\Product\CompanyProductDecorator;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductSticker;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Manager\Catalog\Product\ProductManager;

/**
 * Class ProductExtension
 * @package AppBundle\Twig\Extension
 */
class ProductExtension extends \Twig_Extension
{
    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * ProductExtension constructor.
     * @param ProductManager $productManager
     */
    public function __construct(ProductManager $productManager)
    {
        $this->productManager = $productManager;
    }

    /**
     * Get al filters
     *
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('get_product_sticker', [$this, 'getProductSticker']),
        ];
    }

    /**
     * @param Product $product
     * @return ProductSticker|null
     * @throws \Exception
     */
    public function getProductSticker(ProductInterface $product)
    {
        if($product instanceof CompanyProductDecorator) {
            $product = $product->getEntity();
        }

        if($product instanceof CompanyProduct) {
            $product = $product->getRelatedProduct();
        }

        return $this->productManager->getSticker($product);
    }
}
