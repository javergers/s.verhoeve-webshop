<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Services\SiteService;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class SiteTabsExtension
 * @package AppBundle\Twig\Extension
 */
class SiteTabsExtension extends \Twig_Extension
{
    /**
     * @var SiteService
     */
    private $siteService;

    /**
     * @var SessionInterface
     */
    private $session;

    /** @var TokenStorageInterface */
    private $tokenStorage;

    /**
     * SiteTabsExtension constructor.
     *
     * @param SiteService           $siteService
     * @param SessionInterface      $session
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(SiteService $siteService, SessionInterface $session, TokenStorageInterface $tokenStorage)
    {
        $this->siteService = $siteService;
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions(): array
    {
        $options = [
            'is_safe' => ['html'],
            'needs_environment' => true,
        ];

        $functions = [
            new \Twig_SimpleFunction('site_tabs', [$this, 'get'], $options),
        ];

        return $functions;
    }

    /**
     * @param \Twig_Environment $environment
     *
     * @return string
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function get(\Twig_Environment $environment)
    {
        $sites = [];
        $site = $this->siteService->determineSite();

        if (null === $site) {
            return '';
        }

        if (null === ($parentSite = $site->getParent())) {
            $parentSite = $site;
        }

        $sites[] = [
            'id' => $parentSite->getId(),
            'slug' => $parentSite->getSlug(),
            'active' => (!$this->session->has('site_id') && $site->getSlug() === null) || ($parentSite->getId() === $this->session->get('site_id'))
        ];

        $skipChildrenSites = false;
        if(null !== $this->tokenStorage->getToken() && $this->tokenStorage->getToken()->getUser() instanceof Customer) {
            /** @var Customer $customer */
            $customer = $this->tokenStorage->getToken()->getUser();

            if(null !== $customer->getCompany() && false === $customer->getCompany()->getAssortments()->isEmpty()) {
                $skipChildrenSites = true;
            }
        }

        if(false === $skipChildrenSites) {
            foreach ($parentSite->getChildren() as $childSite) {
                $active = false;

                if ($this->session->has('site_id') && $this->session->get('site_id') === $childSite->getId()) {
                    $active = true;
                } else if (!$this->session->has('site_id') && $site->getSlug() === $childSite->getSlug()) {
                    $active = true;
                }

                $sites[] = [
                    'id' => $childSite->getId(),
                    'slug' => $childSite->getSlug(),
                    'active' => $active
                ];
            }
        }

        return $environment->render('AppBundle:blocks:nav-tab.html.twig', [
            'sites' => $sites
        ]);
    }
}
