<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Services\PreviewService;

/**
 * Class PreviewExtension
 * @package AppBundle\Twig\Extension
 */
class PreviewExtension extends \Twig_Extension
{
    /**
     * @var PreviewService
     */
    private $preview;

    /**
     * PreviewExtension constructor.
     * @param PreviewService $preview
     */
    public function __construct(PreviewService $preview)
    {
        $this->preview = $preview;
    }

    /**
     * Get al filters
     *
     * @return array
     */
    public function getFunctions()
    {
        $functions = [
            new \Twig_SimpleFilter('is_preview_active', [$this->preview, 'isActive']),
        ];

        return $functions;
    }
}
