<?php

namespace AppBundle\Twig\Extension;


use AppBundle\Entity\Order\CartOrderLine;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Manager\Order\CartOrderLineManager;

/**
 * Class CartOrderLineManagerExtension
 * @package AppBundle\Twig\Extension
 */
class CartOrderLineManagerExtension extends \Twig_Extension
{
    /**
     * @var CartOrderLineManager
     */
    protected $cartOrderLineManager;

    /**
     * PriceExtension constructor.
     * @param CartOrderLineManager $cartOrderLineManager
     */
    public function __construct(CartOrderLineManager $cartOrderLineManager)
    {
        $this->cartOrderLineManager = $cartOrderLineManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $functions = [
            new \Twig_SimpleFunction('cartorderline_manager_get_discountamount_excl', [$this, 'getDiscountAmountExcl']),
            new \Twig_SimpleFunction('cartorderline_manager_get_discount_description', [$this, 'getDiscountDescription'])
        ];

        return $functions;
    }

    /**
     * @param CartOrderLine $orderLine
     * @return float|null
     * @throws \Exception
     */
    public function getDiscountAmountExcl(CartOrderLine $orderLine)
    {
        return $this->cartOrderLineManager->getDiscountAmountExcl($orderLine);
    }

    /**
     * @param CartOrderLine $orderLine
     * @return string|null
     * @throws \Exception
     */
    public function getDiscountDescription(CartOrderLine $orderLine)
    {
        return $this->cartOrderLineManager->getDiscountDescription($orderLine);
    }
}
