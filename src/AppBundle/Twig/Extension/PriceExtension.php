<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Vat;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Interfaces\Catalog\Product\ProductInterface;
use AppBundle\Interfaces\Sales\OrderLineInterface;
use AppBundle\Manager\Catalog\Product\ProductManager;
use AppBundle\Manager\Catalog\Product\ProductPriceManager;
use AppBundle\Manager\Order\OrderLineManager;
use AppBundle\Services\Finance\Tax\VatGroupService;
use AppBundle\Services\PriceService;
use Exception;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Twig_Environment;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;

/**
 * Class PriceExtension
 * @package AppBundle\Twig\Extension
 */
class PriceExtension extends AbstractExtension
{
    /**
     * @var ProductManager
     */
    protected $productPriceManager;

    /**
     * @var PriceService
     */
    protected $priceService;

    /**
     * @var VatGroupService
     */
    protected $vatGroupService;

    /**
     * @var OrderLineManager
     */
    private $orderLineManager;

    /**
     * PriceExtension constructor.
     *
     * @param ProductPriceManager $productPriceManager
     * @param PriceService $priceService
     * @param VatGroupService $vatGroupService
     * @param OrderLineManager $orderLineManager
     */
    public function __construct(ProductPriceManager $productPriceManager, PriceService $priceService, VatGroupService $vatGroupService, OrderLineManager $orderLineManager)
    {
        $this->productPriceManager = $productPriceManager;
        $this->priceService = $priceService;
        $this->vatGroupService = $vatGroupService;
        $this->orderLineManager = $orderLineManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        $options = [
            'is_safe' => ['html'],
            'needs_environment' => true,
        ];

        return [
            new TwigFunction('price_incl', [$this, 'getPriceIncl']),
            new TwigFunction('price_vat', [$this, 'getPriceVat']),
            new TwigFunction('get_price', [$this, 'getPrice']),
            new TwigFunction('get_price_html', [$this, 'getProductPriceHtml'], $options),
            new TwigFunction('orderline_amount_incl', [$this, 'getOrderLineAmountIncl'], $options),
        ];
    }

    /**
     * @param float $price
     * @param Vat   $vat
     * @return float
     */
    public function getPriceIncl(float $price, Vat $vat)
    {
        return round($price * ((100 + $vat->getPercentage()) / 100), 2);
    }

    /**
     * @param float $price
     * @param Vat   $vat
     * @return float
     */
    public function getPriceVat(float $price, Vat $vat)
    {
        return round($price * ($vat->getPercentage() / 100), 2);
    }

    /**
     * @param object    $object
     * @param bool|null $showVat
     * @param string    $property
     * @return float
     * @throws Exception
     */
    public function getPrice($object, ?bool $showVat = null, $property = 'price')
    {
        $propertyAccessor = new PropertyAccessor();
        $price = $propertyAccessor->getValue($object, $property);

        $vatRate = null;
        if(method_exists($object, 'getVatRate')) {
            $vatRate = $object->getVatRate();
        } elseif(method_exists($object, 'getVatGroups')) {
            if($object instanceof Product) {
                $vatGroup = $this->vatGroupService->determineVatGroupForProduct($object);
                $vatRate = $vatGroup->getRateByDate();
            }
        } else if($object instanceof OrderLineInterface) {
            $vatGroup = $this->vatGroupService->determineVatGroupForLineInterface($object);
            $vatRate = $vatGroup->getRateByDate($object->getOrder()->getDeliveryDate());
        }

        return $this->priceService->getRawDisplayPrice($price, $vatRate, $showVat);
    }

    /**
     * @param \Twig_Environment $env
     * @param Product|ProductInterface $product
     * @param bool|null $withDiscount
     * @param bool|null $showVat
     *
     * @param bool|null $showLabelPriceFrom
     * @param bool|null $inline
     *
     * @return float
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function getProductPriceHtml(
        \Twig_Environment $env,
        ProductInterface $product,
        ?bool $withDiscount = null,
        ?bool $showVat = null,
        ?bool $showLabelPriceFrom = null,
        ?bool $inline = null
    ) {
        $currentPrice = $this->productPriceManager->getPrice($product, $withDiscount, $showVat);
        $displayMode = $this->productPriceManager->getDisplayMode();

        return $env->render('@App/blocks/Product/price.html.twig', [
            'displayMode' => $displayMode,
            'beforePrice' => $this->getPrice($product),
            'currentPrice' => $currentPrice,
            'showLabelPriceFrom' => $showLabelPriceFrom ?: false,
            'inline' => $inline ?: false
        ]);
    }

    /**
     * @param Twig_Environment $env
     * @param OrderLine $orderLine
     *
     * @return float
     */
    public function getOrderLineAmountIncl(\Twig_Environment $env, OrderLine $orderLine)
    {
        return $this->orderLineManager->getAmountIncl($orderLine);
    }
}
