<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Manager\Order\OrderCollectionManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class OrderCollectionManagerExtension
 * @package AppBundle\Twig\Extension
 */
class OrderCollectionManagerExtension extends AbstractExtension
{
    /**
     * @var OrderCollectionManager
     */
    private $orderCollectionManager;

    /**
     * OrderCollectionManagerExtension constructor.
     * @param OrderCollectionManager $orderCollectionManager
     */
    public function __construct(OrderCollectionManager $orderCollectionManager)
    {
        $this->orderCollectionManager = $orderCollectionManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('has_cancellable_invoice_payment',
                [$this->orderCollectionManager, 'hasCancellableInvoicePayment']
            ),
            new TwigFunction('has_draft_orders',
                [$this->orderCollectionManager, 'hasDraftOrders']
            )
        ];
    }
}
