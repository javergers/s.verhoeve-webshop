<?php

namespace AppBundle\Twig\Extension;

use AppBundle\Manager\StockManager;

/**
 * Class StockManagerExtension
 * @package AppBundle\Twig\Extension
 */
class StockManagerExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    /**
     * @var StockManager
     */
    private $stockManager;

    /**
     * Constructor.
     *
     * @param StockManager $stockManager
     */
    public function __construct(StockManager $stockManager)
    {
        $this->stockManager = $stockManager;
    }

    /**
     * Returns a list of global variables to add to the existing list.
     *
     * @return array An array of global variables
     */
    public function getGlobals()
    {
        return [
            'stock_manager' => $this->stockManager,
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_stock_manager';
    }
}
