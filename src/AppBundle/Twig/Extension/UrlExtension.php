<?php

namespace AppBundle\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig_SimpleFilter;

/**
 * Class UrlExtension
 * @package AppBundle\Twig\Extension
 */
class UrlExtension extends AbstractExtension
{
    /**
     * @return array|Twig_SimpleFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('url_build_query', [$this, 'urlBuildQuery']),
        ];
    }

    /**
     *
     * @param string $url
     * @param array  $parameters
     *
     * @return string
     */
    public function urlBuildQuery($url, $parameters)
    {
        $query = [];
        $url = parse_url($url);

        if (isset($url['query'])) {
            parse_str($url['query'], $query);
        }

        foreach ($parameters as $key => $val) {
            $query[$key] = $val;
        }

        $url['query'] = http_build_query($query);

        $url = $this->buildUrl($url);

        return $url;
    }

    /**
     * @param array $parts
     *
     * @return string
     */
    private function buildUrl($parts)
    {
        if (!\is_array($parts)) {
            return false;
        }

        $uri = isset($parts['scheme']) ? $parts['scheme'] . ':' . ((strtolower($parts['scheme']) === 'mailto') ? '' : '//') : '';
        $uri .= isset($parts['user']) ? $parts['user'] . ($parts['pass'] ? ':' . $parts['pass'] : '') . '@' : '';
        $uri .= $parts['host'] ?? '';
        $uri .= isset($parts['port']) ? ':' . $parts['port'] : '';

        if (isset($parts['path'])) {
            $uri .= ($parts['path'][0] === '/') ? $parts['path'] : '/' . $parts['path'];
        }

        $uri .= isset($parts['query']) ? '?' . $parts['query'] : '';
        $uri .= isset($parts['fragment']) ? '#' . $parts['fragment'] : '';

        return $uri;
    }
}
