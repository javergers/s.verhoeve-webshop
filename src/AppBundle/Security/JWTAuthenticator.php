<?php

namespace AppBundle\Security;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Employee\User;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\PreAuthenticationJWTUserToken;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Guard\JWTTokenAuthenticator;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class JWTAuthenticator
 * @package AppBundle\Security
 */
final class JWTAuthenticator extends JWTTokenAuthenticator
{
    use ContainerAwareTrait;

    /**
     * @param Request $request
     * @return PreAuthenticationJWTUserToken
     */
    public function getCredentials(Request $request)
    {
        try {
            return parent::getCredentials($request);
        } catch (AuthenticationException $e) {
            $this->container->get('session')->set('jwt_error', true);
        }

        return null;
    }

    /**
     * @param mixed                 $preAuthToken
     * @param UserProviderInterface $userProvider
     * @return UserInterface
     */
    public function getUser($preAuthToken, UserProviderInterface $userProvider)
    {
        try {
            return parent::getUser($preAuthToken, $userProvider);
        } catch (AuthenticationException $e) {
            $this->container->get('session')->set('jwt_error', true);
        }

        return null;
    }

    /**
     * @param Request        $request
     * @param TokenInterface $token
     * @param string         $providerKey
     * @return null|\Symfony\Component\HttpFoundation\Response|void
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        void($providerKey);

        $employee = null;
        $session = $request->getSession();

        if ($session !== null) {
            if ($session->has('employee_login_as_customer')) {
                $employee = $this->container->get('doctrine')->getRepository(User::class)->find($session->get('employee_id'));
            }

            /** @var Customer $user */
            $user = $token->getUser();

            $this->container->get('app.customer.auth_logger')->success('employee_login', $token->getUsername(),
                $user, null, null, $employee);
        }
    }
}
