<?php

namespace AppBundle\Security\Customer;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Services\Customer\AuthenticationLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;

/**
 * Class AuthenticationLoginSuccessHandler
 * @package AppBundle\Security\Customer
 */
class AuthenticationLoginSuccessHandler extends DefaultAuthenticationSuccessHandler
{
    /**
     * @var AuthenticationLogger
     */
    private $authenticationLogHandler;

    /**
     * AuthenticationSuccessHandler constructor.
     *
     * @param AuthenticationLogger $authenticationLogHandler
     */
    public function setAuthenticationLogger(AuthenticationLogger $authenticationLogHandler)
    {
        $this->authenticationLogHandler = $authenticationLogHandler;
    }

    /**
     *
     * @param Request        $request
     * @param TokenInterface $token
     *
     * @return Response never null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        /** @var Customer $user */
        $user = $token->getUser();

        $this->authenticationLogHandler->success('login', $token->getUsername(), $user);

        return parent::onAuthenticationSuccess($request, $token);
    }
}
