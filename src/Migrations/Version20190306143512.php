<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190306143512 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     * @throws ConnectionException
     * @throws DBALException
     */
    public function preDown(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameterService->deleteByKey('order_evaluator_matching_percentage');
    }

    /**
     * @description Sets the parameters for WICS for use in the WICS service
     * @param Schema $schema
     * @throws ConnectionException
     */
    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');


        $parameterService->createViaArray([
            'key' => 'order_evaluator_matching_percentage',
            'entity' => null,
            'label' => 'Order evalutie vergelijkingspercentage',
            'value' => 75,
        ]);
    }
}