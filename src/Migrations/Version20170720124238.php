<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Order\OrderDeliveryStatus;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170720124238 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE order_delivery_status (id VARCHAR(25) NOT NULL, description VARCHAR(100) NOT NULL, paazl_status VARCHAR(25) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE order_order ADD delivery_status_id VARCHAR(25) DEFAULT NULL');
        $this->addSql('ALTER TABLE order_order ADD CONSTRAINT FK_76B7E7652F924C2F FOREIGN KEY (delivery_status_id) REFERENCES order_delivery_status (id)');
        $this->addSql('CREATE INDEX IDX_76B7E7652F924C2F ON order_order (delivery_status_id)');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        return;
        if ($this->container->get('environment')->is(['prod', 'staging'])) {
            $em = $this->container->get("doctrine")->getManager();

            $orderDeliveryStatusNew = new OrderDeliveryStatus();
            $orderDeliveryStatusNew->setId("unknown");
            $orderDeliveryStatusNew->setDescription("");
            $orderDeliveryStatusNew->setPaazlStatus("LABELS_NOT_CREATED");

            $em->persist($orderDeliveryStatusNew);


            $orderDeliveryStatusReady = new OrderDeliveryStatus();
            $orderDeliveryStatusReady->setId("ready");
            $orderDeliveryStatusReady->setDescription("Voorgemeld bij de distributeur");
            $orderDeliveryStatusReady->setPaazlStatus("LABELS_CREATED");

            $em->persist($orderDeliveryStatusReady);


            $orderDeliveryStatusReadyToPost = new OrderDeliveryStatus();
            $orderDeliveryStatusReadyToPost->setId("ready_to_post");
            $orderDeliveryStatusReadyToPost->setDescription("Klaar voor brievenbusverzending");
            $orderDeliveryStatusReadyToPost->setPaazlStatus("READY_TO_POST");

            $em->persist($orderDeliveryStatusReadyToPost);


            $orderDeliveryStatusScanned = new OrderDeliveryStatus();
            $orderDeliveryStatusScanned->setId("scanned");
            $orderDeliveryStatusScanned->setDescription("Gescanned bij sorteercentrum van distributeur");
            $orderDeliveryStatusScanned->setPaazlStatus("SCANNED");

            $em->persist($orderDeliveryStatusScanned);


            $orderDeliveryStatusDelivered = new OrderDeliveryStatus();
            $orderDeliveryStatusDelivered->setId("delivered");
            $orderDeliveryStatusDelivered->setDescription("Afgeleverd");
            $orderDeliveryStatusDelivered->setPaazlStatus("DELIVERED");

            $em->persist($orderDeliveryStatusDelivered);


            $orderDeliveryStatusDeliveredAtNeighbors = new OrderDeliveryStatus();
            $orderDeliveryStatusDeliveredAtNeighbors->setId("delivered_at_neighbors");
            $orderDeliveryStatusDeliveredAtNeighbors->setDescription("Afgeleverd bij buren");
            $orderDeliveryStatusDeliveredAtNeighbors->setPaazlStatus("DELIVEREDBB");

            $em->persist($orderDeliveryStatusDeliveredAtNeighbors);


            $orderDeliveryStatusAvailableAtPickupPoint = new OrderDeliveryStatus();
            $orderDeliveryStatusAvailableAtPickupPoint->setId("available_at_pickup_point");
            $orderDeliveryStatusAvailableAtPickupPoint->setDescription("Afgeleverd bij ophaalpunt");
            $orderDeliveryStatusAvailableAtPickupPoint->setPaazlStatus("AVAILABLE_AT_PICKUP_POINT");

            $em->persist($orderDeliveryStatusAvailableAtPickupPoint);


            $orderDeliveryStatusAvailableAtPickupPoint = new OrderDeliveryStatus();
            $orderDeliveryStatusAvailableAtPickupPoint->setId("pickedup");
            $orderDeliveryStatusAvailableAtPickupPoint->setDescription("Afgehaald bij ophaalpunt");
            $orderDeliveryStatusAvailableAtPickupPoint->setPaazlStatus("PICKEDUP");

            $em->persist($orderDeliveryStatusAvailableAtPickupPoint);

            $em->flush();
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_order DROP FOREIGN KEY FK_76B7E7652F924C2F');
        $this->addSql('DROP TABLE order_delivery_status');
        $this->addSql('DROP INDEX IDX_76B7E7652F924C2F ON order_order');
        $this->addSql('ALTER TABLE order_order DROP delivery_status_id');
    }
}
