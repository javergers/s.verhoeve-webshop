<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Carrier\Carrier;
use AppBundle\Entity\Carrier\DeliveryMethod;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190319101443 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameterService->createViaArray([
            'key' => 'wics_carrier_id',
            'label' => 'Id van de transporteur in WICS',
            'form_type_class' => null,
            'form_type' => TextType::class,//IntegerType::class, //TODO change to IntegerType when commit 85e3e4d is merged into master
            'multiple' => 0,
            'entity' => Carrier::class,
            'required' => false,
        ]);

        $parameterService->createViaArray([
            'key' => 'wics_carrier_option',
            'label' => 'De code voor de leveringswijze zoals die doorgegeven moet worden aan WICS',
            'form_type_class' => null,
            'form_type' => TextType::class,
            'multiple' => 0,
            'entity' => DeliveryMethod::class,
            'required' => false,
        ]);

        $parameterService->createViaArray([
            'key' => 'wics_pickup_carrier_id',
            'label' => 'Het transporteur id zoals die doorgegeven moet worden aan WICS in het geval van een afhaalorder',
            'form_type_class' => null,
            'form_type' => TextType::class,//IntegerType::class, //TODO change to IntegerType when commit 85e3e4d is merged into master
            'multiple' => 0,
            'entity' => null,
            'required' => false,
        ]);

        $parameterService->createViaArray([
            'key' => 'wics_pickup_carrier_option',
            'label' => 'De code voor de leveringswijze zoals die doorgegeven moet worden aan WICS in het geval van een afhaalorder',
            'form_type_class' => null,
            'form_type' => TextType::class,
            'multiple' => 0,
            'entity' => null,
            'required' => false,
        ]);

        $parameterService->createViaArray([
            'key' => 'delivery_method_by_supplier',
            'label' => 'De DeliveryMethod die standaard gekoppeld moet worden bij leveranciers die een eigen leveringsgebied hebben',
            'form_type_class' => DeliveryMethod::class,
            'form_type' => EntityType::class,
            'multiple' => 0,
            'entity' => null,
            'required' => false,
        ]);
    }

    /**
     * @param Schema $schema
     */
    public function postDown(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameterService->deleteByKey('wics_carrier_id');
        $parameterService->deleteByKey('wics_carrier_option');
        $parameterService->deleteByKey('wics_pickup_carrier_id');
        $parameterService->deleteByKey('wics_pickup_carrier_option');
        $parameterService->deleteByKey('delivery_method_by_supplier');
    }
}
