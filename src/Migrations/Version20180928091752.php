<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180928091752 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE discount CHANGE type type enum(\'amount\', \'percentage\')');
        $this->addSql('ALTER TABLE discount_property CHANGE description description VARCHAR(50) NOT NULL');
        $this->addSql('ALTER TABLE cart_order_line ADD discount_type ENUM(\'line\', \'assortment\') DEFAULT NULL COMMENT \'(DC2Type:DiscountType)\'');
        $this->addSql('ALTER TABLE order_line ADD discount_type ENUM(\'line\', \'assortment\') DEFAULT NULL COMMENT \'(DC2Type:DiscountType)\', CHANGE ledger_id ledger_id INT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE cart_order_line DROP discount_type');
        $this->addSql('ALTER TABLE discount CHANGE type type VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE discount_property CHANGE description description VARCHAR(100) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE order_line DROP FOREIGN KEY FK_9CE58EE1A7B913DD');
        $this->addSql('ALTER TABLE order_line DROP discount_type, CHANGE ledger_id ledger_id INT NOT NULL');
    }
}
