<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190123154353 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_sticker (id INT AUTO_INCREMENT NOT NULL, rule_id INT DEFAULT NULL, description VARCHAR(255) NOT NULL, path VARCHAR(255) DEFAULT NULL, publish TINYINT(1) DEFAULT NULL, publish_start DATETIME DEFAULT NULL, publish_end DATETIME DEFAULT NULL, INDEX IDX_5E34A74D744E0351 (rule_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_sticker ADD CONSTRAINT FK_5E34A74D744E0351 FOREIGN KEY (rule_id) REFERENCES rule (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company_report_customer_history (id INT AUTO_INCREMENT NOT NULL, company_report_customer_id INT DEFAULT NULL, from_date DATETIME NOT NULL, till_date DATETIME NOT NULL, INDEX IDX_41BD177891B78F44 (company_report_customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_report_customer_history ADD CONSTRAINT FK_41BD177891B78F44 FOREIGN KEY (company_report_customer_id) REFERENCES company_report_customer (id)');
        $this->addSql('DROP TABLE product_sticker');
    }
}
