<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\QueryBuilder;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181023063452 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company ADD uuid CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\' AFTER id');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FBF094FD17F50A6 ON company (uuid)');
        $this->addSql('ALTER TABLE customer ADD uuid CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\' AFTER id');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_81398E09D17F50A6 ON customer (uuid)');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        parent::postUp($schema);

        /** @var QueryBuilder $qb */
        $qb = $this->connection->createQueryBuilder();

        $qb->update('company', 'comp')
            ->set('comp.uuid', 'UUID()')
            ->where('comp.uuid IS NULL')
            ->execute();

        $qb = $this->connection->createQueryBuilder();

        $qb->update('customer', 'cust')
            ->set('cust.uuid', 'UUID()')
            ->where('cust.uuid IS NULL')
            ->execute();
    }


    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_4FBF094FD17F50A6 ON company');
        $this->addSql('ALTER TABLE company DROP uuid');
        $this->addSql('DROP INDEX UNIQ_81398E09D17F50A6 ON `customer`');
        $this->addSql('ALTER TABLE `customer` DROP uuid');
    }
}
