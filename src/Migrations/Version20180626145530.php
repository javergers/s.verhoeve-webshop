<?php

namespace DoctrineMigrations;

use AdminBundle\Form\Parameter\ParameterEntityFileType;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180626145530 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $parameters = [
            'cookie_consent_categories_file',
            'cookie_consent_translation_file',
        ];

        $fields = [
            'form_type' => ParameterEntityFileType::class,
        ];

        $this->updateParameterFields($parameters, $fields);
    }

    /**
     * @param Schema $schema
     */
    public function postDown(Schema $schema)
    {
        $parameters = [
            'cookie_consent_categories_file',
            'cookie_consent_translation_file',
        ];

        $fields = [
            'form_type' => TextType::class,
        ];

        $this->updateParameterFields($parameters, $fields);
    }

    /**
     * @param array $parameters
     * @param array $fields
     */
    private function updateParameterFields(array $parameters, array $fields)
    {
        /** @var Connection $conn */
        $conn = $this->container->get('doctrine')->getConnection();

        $qb = $conn->createQueryBuilder();
        $qb->update('parameter');

        $i = 1;
        foreach ($fields as $key => $value) {
            $field = ':field' . $i;

            $qb->set('form_type', $field);
            $qb->setParameter($field, $value);

            $i++;
        }

        $qb->where('`key` IN (:parameters)');
        $qb->setParameter('parameters', $parameters, Connection::PARAM_STR_ARRAY);

        if ($qb->execute()) {
            $this->write('Update fields of parameters');
        }
    }
}
