<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170324095018 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE site ADD order_pickup_enabled TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE supplier_product ADD pickup_enabled TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE productgroup ADD pickup_enabled TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE supplier_product ADD position INT NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE supplier_product DROP pickup_enabled');
        $this->addSql('ALTER TABLE productgroup DROP pickup_enabled');
        $this->addSql('ALTER TABLE site DROP order_pickup_enabled');
        $this->addSql('ALTER TABLE supplier_product DROP position');
    }
}
