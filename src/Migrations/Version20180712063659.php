<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180712063659 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     * @throws DBALException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE productgroup ADD letter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE productgroup ADD CONSTRAINT FK_BD0D167F89FC28C1 FOREIGN KEY (letter_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_BD0D167F89FC28C1 ON productgroup (letter_id)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     * @throws DBALException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE productgroup DROP FOREIGN KEY FK_BD0D167F89FC28C1');
        $this->addSql('DROP INDEX IDX_BD0D167F89FC28C1 ON productgroup');
        $this->addSql('ALTER TABLE productgroup DROP letter_id');
    }
}
