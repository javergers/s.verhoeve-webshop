<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161212104119 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE kiyoh_company ADD page_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE kiyoh_company ADD CONSTRAINT FK_4F58528AC4663E4 FOREIGN KEY (page_id) REFERENCES page (id)');
        $this->addSql('CREATE INDEX IDX_4F58528AC4663E4 ON kiyoh_company (page_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE kiyoh_company DROP FOREIGN KEY FK_4F58528AC4663E4');
        $this->addSql('DROP INDEX IDX_4F58528AC4663E4 ON kiyoh_company');
        $this->addSql('ALTER TABLE kiyoh_company DROP page_id');
    }
}
