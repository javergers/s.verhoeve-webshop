<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170404105421 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cart_order_line DROP FOREIGN KEY FK_FC8FFE138D9F6D38');
        $this->addSql('DROP INDEX IDX_FC8FFE138D9F6D38 ON cart_order_line');
        $this->addSql('ALTER TABLE cart_order_line CHANGE order_id cart_order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cart_order_line ADD CONSTRAINT FK_FC8FFE1313002253 FOREIGN KEY (cart_order_id) REFERENCES cart_order (id)');
        $this->addSql('CREATE INDEX IDX_FC8FFE1313002253 ON cart_order_line (cart_order_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE cart_order_line DROP FOREIGN KEY FK_FC8FFE1313002253');
        $this->addSql('DROP INDEX IDX_FC8FFE1313002253 ON cart_order_line');
        $this->addSql('ALTER TABLE cart_order_line CHANGE cart_order_id order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE cart_order_line ADD CONSTRAINT FK_FC8FFE138D9F6D38 FOREIGN KEY (order_id) REFERENCES cart_order (id)');
        $this->addSql('CREATE INDEX IDX_FC8FFE138D9F6D38 ON cart_order_line (order_id)');
    }
}
