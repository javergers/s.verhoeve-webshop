<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190228091722 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE upsell (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, upsell_product_id INT DEFAULT NULL, position INT DEFAULT NULL, INDEX IDX_A925D27F4584665A (product_id), INDEX IDX_A925D27F6D955428 (upsell_product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE upsell ADD CONSTRAINT FK_A925D27F4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE upsell ADD CONSTRAINT FK_A925D27F6D955428 FOREIGN KEY (upsell_product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE productgroup ADD available_for_upsell TINYINT(1) DEFAULT \'0\' NOT NULL, ADD upsell_quantity_mode ENUM(\'strictly_following\', \'loosely_following\', \'independent\') DEFAULT NULL COMMENT \'(DC2Type:UpsellQuantityType)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE upsell');
        $this->addSql('ALTER TABLE productgroup DROP available_for_upsell, DROP upsell_quantity_mode');
    }
}
