<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170317121210 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE teamleader_contact ADD synced_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE teamleader_product ADD synced_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE teamleader_company ADD synced_at DATETIME NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE teamleader_company DROP synced_at');
        $this->addSql('ALTER TABLE teamleader_contact DROP synced_at');
        $this->addSql('ALTER TABLE teamleader_product DROP synced_at');
    }
}
