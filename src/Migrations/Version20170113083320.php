<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170113083320 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE site ADD adyen_merchant_account VARCHAR(50) DEFAULT NULL, ADD adyen_username VARCHAR(50) DEFAULT NULL, ADD adyen_password VARCHAR(50) DEFAULT NULL, ADD adyen_cse_token VARCHAR(50) DEFAULT NULL, ADD adyen_skin_code VARCHAR(50) DEFAULT NULL, ADD adyen_cse_key LONGTEXT DEFAULT NULL, ADD adyen_hmac_key LONGTEXT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE site DROP adyen_merchant_account, DROP adyen_username, DROP adyen_password, DROP adyen_cse_token, DROP adyen_skin_code, DROP adyen_cse_key, DROP adyen_hmac_key');
    }
}
