<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180314130336 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE productgroup_property CHANGE `key` `key` VARCHAR(40) DEFAULT NULL');
        $this->addSql('UPDATE productgroup_property SET `key` = NULL WHERE `key` = \'\'');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F77F0C7D4E645A7E ON productgroup_property (`key`)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_F77F0C7D4E645A7E ON productgroup_property');
        $this->addSql('UPDATE productgroup_property SET `key` = \'\' WHERE `key` IS NULL');
        $this->addSql('ALTER TABLE productgroup_property CHANGE `key` `key` VARCHAR(40) NOT NULL COLLATE utf8_unicode_ci');
    }
}
