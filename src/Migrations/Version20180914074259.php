<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180914074259 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameters = [
            'google_maps_place_url' => [
                'label' => 'Google maps place url',
                'value' => 'https://www.google.com/maps/place/%s',
            ],
        ];

        foreach ($parameters as $key => $parameter) {
            $parameter = [
                'key' => $key,
                'entity' => null,
                'label' => $parameter['label'],
                'value' => $parameter['value'],
            ];

            $parameterService->createViaArray($parameter);
        }
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function postDown(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameters = [
            'google_maps_place_url',
        ];

        foreach ($parameters as $parameter) {
            $parameterService->deleteByKey($parameter);
        }
    }
}
