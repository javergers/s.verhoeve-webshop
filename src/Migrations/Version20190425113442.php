<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190425113442 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE shipping');
        $this->addSql('ALTER TABLE wics_message DROP entity_id, CHANGE type type VARCHAR(32) DEFAULT NULL');
        $this->addSql('ALTER TABLE csv_order CHANGE status status ENUM(\'registered\', \'error\', \'archive\')');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BAC76500D17F50A6 ON parameter_entity_file (uuid)');
        $this->addSql('ALTER TABLE adiuvo_user DROP trusted');
        $this->addSql('ALTER TABLE designer_template_thumb DROP FOREIGN KEY FK_D572895E5DA0FB8');
        $this->addSql('ALTER TABLE designer_template_thumb ADD CONSTRAINT FK_D572895E5DA0FB8 FOREIGN KEY (template_id) REFERENCES designer_template (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE designer_collage_object DROP FOREIGN KEY FK_DE5FCC8FEC9066A4');
        $this->addSql('ALTER TABLE designer_collage_object RENAME INDEX idx_35bf991b5da0fb8 TO IDX_DE5FCC8FEC9066A4');
        $this->addSql('ALTER TABLE discount CHANGE type type enum(\'amount\', \'percentage\')');
        $this->addSql('ALTER TABLE vat_rate CHANGE group_id group_id INT NOT NULL');
        $this->addSql('ALTER TABLE vat_group CHANGE country_id country_id VARCHAR(2) NOT NULL, CHANGE level_id level_id INT NOT NULL');
        $this->addSql('ALTER TABLE vat_group_product ADD CONSTRAINT FK_3AC74257E8C706E8 FOREIGN KEY (vat_group_id) REFERENCES vat_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE vat_group_product ADD CONSTRAINT FK_3AC742574584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE delivery_method CHANGE name name VARCHAR(20) NOT NULL');
        $this->addSql('ALTER TABLE `order` CHANGE status status VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE order_collection_line_vat DROP FOREIGN KEY FK_493EE72BFC2A1E2F');
        $this->addSql('DROP INDEX IDX_493EE72BFC2A1E2F ON order_collection_line_vat');
        $this->addSql('ALTER TABLE order_collection_line_vat CHANGE order_collection_id order_collection_line_id INT NOT NULL');
        $this->addSql('ALTER TABLE order_collection_line_vat ADD CONSTRAINT FK_493EE72BE5FAECA5 FOREIGN KEY (order_collection_line_id) REFERENCES order_collection_line (id)');
        $this->addSql('CREATE INDEX IDX_493EE72BE5FAECA5 ON order_collection_line_vat (order_collection_line_id)');
        $this->addSql('ALTER TABLE usp_product ADD CONSTRAINT FK_CB0F88AAA6601B70 FOREIGN KEY (usp_id) REFERENCES usp (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE usp_product ADD CONSTRAINT FK_CB0F88AA4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE site_usp RENAME INDEX usp_unique TO UNIQ_DF11980F6BD1646A6601B70');
        $this->addSql('ALTER TABLE site_opening_hours CHANGE day day SMALLINT NOT NULL');
        $this->addSql('ALTER TABLE site_opening_hours RENAME INDEX idx_4d1ea798f6bd1646 TO IDX_74E3A9A0F6BD1646');
        $this->addSql('ALTER TABLE report_chart CHANGE date_filter date_filter VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE company_report_customer CHANGE frequency frequency ENUM(\'P1D\', \'P7D\', \'P14D\', \'P1M\', \'P3M\', \'P1Y\') NOT NULL COMMENT \'(DC2Type:CompanyReportCustomerFrequencyType)\'');
        $this->addSql('ALTER TABLE order_priority_rule CHANGE priority priority INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_priority_rule RENAME INDEX idx_aee8145744e0351 TO IDX_23C349C7744E0351');
        $this->addSql('ALTER TABLE audit_associations CHANGE tbl tbl VARCHAR(128) DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE audit_associations CHANGE tbl tbl VARCHAR(128) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('CREATE TABLE shipping (id INT AUTO_INCREMENT NOT NULL, description VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE adiuvo_user ADD trusted JSON DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:json_array)\'');
        $this->addSql('ALTER TABLE company_report_customer CHANGE frequency frequency VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE csv_order CHANGE status status VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE delivery_method CHANGE name name VARCHAR(50) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE designer_collage_object RENAME INDEX idx_de5fcc8fec9066a4 TO IDX_35BF991B5DA0FB8');
        $this->addSql('ALTER TABLE designer_template_thumb DROP FOREIGN KEY FK_D572895E5DA0FB8');
        $this->addSql('ALTER TABLE designer_template_thumb ADD CONSTRAINT FK_D572895E5DA0FB8 FOREIGN KEY (template_id) REFERENCES designer_template (id)');
        $this->addSql('ALTER TABLE discount CHANGE type type VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE `order` CHANGE status status VARCHAR(25) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE order_collection_line_vat DROP FOREIGN KEY FK_493EE72BE5FAECA5');
        $this->addSql('DROP INDEX IDX_493EE72BE5FAECA5 ON order_collection_line_vat');
        $this->addSql('ALTER TABLE order_collection_line_vat CHANGE order_collection_line_id order_collection_id INT NOT NULL');
        $this->addSql('ALTER TABLE order_collection_line_vat ADD CONSTRAINT FK_493EE72BFC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES order_collection (id)');
        $this->addSql('CREATE INDEX IDX_493EE72BFC2A1E2F ON order_collection_line_vat (order_collection_id)');
        $this->addSql('ALTER TABLE order_priority_rule CHANGE priority priority DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE order_priority_rule RENAME INDEX idx_23c349c7744e0351 TO IDX_AEE8145744E0351');
        $this->addSql('DROP INDEX UNIQ_BAC76500D17F50A6 ON parameter_entity_file');
        $this->addSql('ALTER TABLE report_chart CHANGE date_filter date_filter VARCHAR(15) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE site_opening_hours CHANGE day day INT NOT NULL');
        $this->addSql('ALTER TABLE site_opening_hours RENAME INDEX idx_74e3a9a0f6bd1646 TO IDX_4D1EA798F6BD1646');
        $this->addSql('ALTER TABLE site_usp RENAME INDEX uniq_df11980f6bd1646a6601b70 TO usp_unique');
        $this->addSql('ALTER TABLE usp_product DROP FOREIGN KEY FK_CB0F88AAA6601B70');
        $this->addSql('ALTER TABLE usp_product DROP FOREIGN KEY FK_CB0F88AA4584665A');
        $this->addSql('ALTER TABLE vat_group CHANGE country_id country_id VARCHAR(2) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE level_id level_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE vat_group_product DROP FOREIGN KEY FK_3AC74257E8C706E8');
        $this->addSql('ALTER TABLE vat_group_product DROP FOREIGN KEY FK_3AC742574584665A');
        $this->addSql('ALTER TABLE vat_rate CHANGE group_id group_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE wics_message ADD entity_id INT DEFAULT NULL, CHANGE type type VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
