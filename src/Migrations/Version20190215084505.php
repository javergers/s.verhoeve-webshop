<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20190215084505
 * @package DoctrineMigrations
 */
class Version20190215084505 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }

    /**
     * @param Schema $schema
     * @throws DBALException
     */
    public function postUp(Schema $schema)
    {
        $this->updateTables('`supplier_group`');
        $this->updateTables('`supplier_delivery_area`');
    }

    /**
     * @param null $dbTable
     * @return bool
     * @throws DBALException
     */
    private function updateTables($dbTable = null)
    {
        if($dbTable === null) {
            return false;
        }

        $sql = 'UPDATE ' . $dbTable . ' SET `delivery_interval` = IF (INSTR(`delivery_interval`, \'T\') > 0, delivery_interval, 
          IF (INSTR(`delivery_interval`, \'D\'), REPLACE(delivery_interval, \'D\',\'DT\'), REPLACE(delivery_interval, \'P\',\'PT\'))
        )';

        $stm = $this->connection->query($sql);

        return $stm->execute();
    }
}
