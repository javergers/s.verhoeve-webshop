<?php

namespace DoctrineMigrations;

use AdminBundle\Form\Fields\PriceRange;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class Version20181108085552
 * @package DoctrineMigrations
 */
class Version20181108085552 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE data_set_filter_field (id INT AUTO_INCREMENT NOT NULL, parent_option_id INT DEFAULT NULL, entity_id INT DEFAULT NULL, form_type VARCHAR(255) NOT NULL, path VARCHAR(255) NOT NULL, label VARCHAR(255) NOT NULL, placeholder VARCHAR(255) DEFAULT NULL, multiple TINYINT(1), pinned TINYINT(1), position INT, UNIQUE INDEX UNIQ_3CA22D2A568F3281 (parent_option_id), INDEX IDX_3CA22D2A81257D5D (entity_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('CREATE TABLE data_set_filter_field_option (id INT AUTO_INCREMENT NOT NULL, data_set_filter_field_id INT DEFAULT NULL, position INT NOT NULL, operator VARCHAR(255) NOT NULL, label VARCHAR(255) NOT NULL, value VARCHAR(255), INDEX IDX_C3501CE64B699E11 (data_set_filter_field_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE data_set_filter_field ADD CONSTRAINT FK_3CA22D2A568F3281 FOREIGN KEY (parent_option_id) REFERENCES data_set_filter_field_option (id)');

        $this->addSql('ALTER TABLE data_set_filter_field ADD CONSTRAINT FK_3CA22D2A81257D5D FOREIGN KEY (entity_id) REFERENCES entity (id)');
        $this->addSql('ALTER TABLE data_set_filter_field_option ADD CONSTRAINT FK_C3501CE64B699E11 FOREIGN KEY (data_set_filter_field_id) REFERENCES data_set_filter_field (id)');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        if ($this->container->get('environment')->is(['prod', 'staging'])) {

            $doctrine = $this->container->get('doctrine');

            /** @var Connection $conn */
            $conn = $doctrine->getConnection();

            /** @var QueryBuilder $qb */
            $qb = $conn->createQueryBuilder();

            $entityId = $qb
                ->select('id')
                ->from('entity')
                ->where('fully_qualified_name = :fullyQualifiedName')
                ->setParameter('fullyQualifiedName', GenericProduct::class)
                ->execute()
                ->fetchColumn()
            ;

            $qb = $conn->createQueryBuilder();

            $qb->insert('data_set_filter_field')->values([
                'entity_id' => $conn->quote($entityId),
                'form_type' => $conn->quote(PriceRange::class),
                'path' => $conn->quote('__result.variations.price'),
                'label' => $conn->quote('Prijs Excl. BTW'),
                'multiple' => 1,
                'position' => 1
            ])->execute();

            $dataSetFilterFieldId = $conn->lastInsertId();

            $qb = $conn->createQueryBuilder();
            $qb->insert('data_set_filter_field_option')->values([
                'data_set_filter_field_id' => $dataSetFilterFieldId,
                'label' => $conn->quote('min'),
                'value' => $conn->quote('0'),
                'operator' => $conn->quote('='),
                'position' => 1
            ])->execute();

            $qb = $conn->createQueryBuilder();
            $qb->insert('data_set_filter_field_option')->values([
                'data_set_filter_field_id' => $dataSetFilterFieldId,
                'label' => $conn->quote('max'),
                'value' => $conn->quote('250'),
                'operator' => $conn->quote('='),
                'position' => 2
            ])->execute();

            $qb->insert('data_set_filter_field')->values([
                'entity_id' => $conn->quote($entityId),
                'form_type' => $conn->quote(EntityType::class),
                'path' => $conn->quote('__result.assortmentProducts.assortment.id'),
                'label' => $conn->quote('Assortiment'),
                'multiple' => 1,
                'placeholder' => $conn->quote('Maak een keuze'),
                'position' => 2
            ])->execute();

            $qb->insert('data_set_filter_field')->values([
                'entity_id' => $conn->quote($entityId),
                'form_type' => $conn->quote(EntityType::class),
                'path' => $conn->quote('__result.productgroup.id'),
                'label' => $conn->quote('Productgroep'),
                'multiple' => 1,
                'placeholder' => $conn->quote('Maak een keuze'),
                'position' => 3
            ])->execute();

            $qb->insert('data_set_filter_field')->values([
                'entity_id' => $conn->quote($entityId),
                'form_type' => $conn->quote(ChoiceType::class),
                'path' => $conn->quote('__result.variations.stock.physicalStock'),
                'label' => $conn->quote('Voorraadbeheer'),
                'multiple' => 0,
                'placeholder' => $conn->quote('Maak een keuze'),
                'position' => 4
            ])->execute();

            $dataSetFilterFieldId = $conn->lastInsertId();

            $qb = $conn->createQueryBuilder();
            $qb->insert('data_set_filter_field_option')->values([
                'data_set_filter_field_id' => $dataSetFilterFieldId,
                'label' => $conn->quote('Ja'),
                'value' => $conn->quote('0'),
                'operator' => $conn->quote('>'),
                'position' => 1
            ])->execute();

            $qb = $conn->createQueryBuilder();
            $qb->insert('data_set_filter_field_option')->values([
                'data_set_filter_field_id' => $dataSetFilterFieldId,
                'label' => $conn->quote('Nee'),
                'value' => $conn->quote('0'),
                'operator' => $conn->quote('<='),
                'position' => 2
            ])->execute();

            $qb = $conn->createQueryBuilder();
            $qb->insert('data_set_filter_field_option')->values([
                'data_set_filter_field_id' => $dataSetFilterFieldId,
                'label' => $conn->quote('Uitgeschakeld'),
                'operator' => $conn->quote('= NULL'),
                'position' => 2
            ])->execute();

        }
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE data_set_filter_field_option DROP FOREIGN KEY FK_C3501CE64B699E11');
        $this->addSql('ALTER TABLE data_set_filter_field DROP FOREIGN KEY FK_3CA22D2A568F3281');

        $this->addSql('DROP TABLE data_set_filter_field_option');
        $this->addSql('DROP TABLE data_set_filter_field');
    }
}
