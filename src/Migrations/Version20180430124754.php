<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180430124754 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function postUp(Schema $schema)
    {
        $conn = $this->connection;

        /** @var QueryBuilder $qb */
        $qb = $conn->createQueryBuilder();

        $conn->beginTransaction();

        $productGroups = $conn->createQueryBuilder()->select('*')->from('productgroup')->execute()->fetchAll();

        foreach ($productGroups as $group) {
            $productGroupProperties = $conn->createQueryBuilder()->select('productgroup_property_id')
                ->from('productgroup_property_productgroup')
                ->where('productgroup_id = :productgroupid')
                ->setParameter('productgroupid', $group['id'])
                ->execute()
                ->fetchAll();

            if (!empty($productGroupProperties)) {

                $qb->insert('productgroup_property_set')->values([
                    'name' => $conn->quote($group['title']),
                    'productgroup_id' => $group['id'],
                ])->execute();

                $propertySetId = $conn->lastInsertId();

                foreach ($productGroupProperties as $property) {
                    $qb->insert('productgroup_property_set_productgroup_property')->values([
                        'productgroup_property_set_id' => $propertySetId,
                        'productgroup_property_id' => $property['productgroup_property_id'],
                    ])->execute();
                }

                /** @var QueryBuilder $qb */
                $qb = $conn->createQueryBuilder();
                $qb->update('product')
                    ->set('property_set_id', $propertySetId)
                    ->where('productgroup_id = :productgroupid')
                    ->setParameter('productgroupid', $group['id'])
                    ->execute();
            }
        }

        $conn->commit();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
