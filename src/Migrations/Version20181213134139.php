<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181213134139 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE commission_invoice_line DROP FOREIGN KEY FK_5DB74755B5B63A6B');
        $this->addSql('DROP INDEX IDX_5DB74755B5B63A6B ON commission_invoice_line');
        $this->addSql('ALTER TABLE commission_invoice_line CHANGE vat_id vat_rate_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commission_invoice_line ADD CONSTRAINT FK_5DB7475543897540 FOREIGN KEY (vat_rate_id) REFERENCES vat_rate (id)');
        $this->addSql('CREATE INDEX IDX_5DB7475543897540 ON commission_invoice_line (vat_rate_id)');
        $this->addSql('ALTER TABLE commission_invoice_supplier_order_line DROP FOREIGN KEY FK_BB089323B5B63A6B');
        $this->addSql('DROP INDEX IDX_F445BE65B5B63A6B ON commission_invoice_supplier_order_line');
        $this->addSql('ALTER TABLE commission_invoice_supplier_order_line CHANGE vat_id vat_rate_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commission_invoice_supplier_order_line ADD CONSTRAINT FK_F445BE6543897540 FOREIGN KEY (vat_rate_id) REFERENCES vat_rate (id)');
        $this->addSql('CREATE INDEX IDX_F445BE6543897540 ON commission_invoice_supplier_order_line (vat_rate_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE commission_invoice_line DROP FOREIGN KEY FK_5DB7475543897540');
        $this->addSql('DROP INDEX IDX_5DB7475543897540 ON commission_invoice_line');
        $this->addSql('ALTER TABLE commission_invoice_line CHANGE vat_rate_id vat_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commission_invoice_line ADD CONSTRAINT FK_5DB74755B5B63A6B FOREIGN KEY (vat_id) REFERENCES vat (id)');
        $this->addSql('CREATE INDEX IDX_5DB74755B5B63A6B ON commission_invoice_line (vat_id)');
        $this->addSql('ALTER TABLE commission_invoice_supplier_order_line DROP FOREIGN KEY FK_F445BE6543897540');
        $this->addSql('DROP INDEX IDX_F445BE6543897540 ON commission_invoice_supplier_order_line');
        $this->addSql('ALTER TABLE commission_invoice_supplier_order_line CHANGE vat_rate_id vat_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE commission_invoice_supplier_order_line ADD CONSTRAINT FK_BB089323B5B63A6B FOREIGN KEY (vat_id) REFERENCES vat (id)');
        $this->addSql('CREATE INDEX IDX_F445BE65B5B63A6B ON commission_invoice_supplier_order_line (vat_id)');
    }
}
