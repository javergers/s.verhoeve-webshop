<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20190329110003
 * @package DoctrineMigrations
 */
class Version20190329110003 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function postUp(Schema $schema)
    {
        $sql = "
          UPDATE product p SET price = '0.00'
          WHERE p.parent_id IS NOT NULL
	        AND p.price IS NULL
        ";

        $this->connection->query($sql);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
