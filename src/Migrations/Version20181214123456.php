<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use AppBundle\Entity\Discount\Discount;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181214123456 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    /**
     * @description Sets the parameters for WICS for use in the WICS service
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameters = [
            [
                'key' => 'order_restitution_voucher_values',
                'label' => 'Vergoeding bedragen voor restitution vouchers',
                'form_type_class' => Discount::class,
                'form_type' => EntityType::class,
                'multiple' => 1,
                'entity' => null,
                'required' => false,
            ],
            [
                'key' => 'order_restitution_voucher_relative_values',
                'label' => 'Vergoeding percentages voor restitution vouchers',
                'form_type_class' => Discount::class,
                'form_type' => EntityType::class,
                'multiple' => 1,
                'entity' => null,
                'required' => false,
            ],
            [
                'key' => 'order_restitution_voucher_max_relative_pct',
                'value' => 60,
                'label' => 'Maximale percentage van het orderbedrag (incl) dat een restitution voucher waard mag zijn',
                'entity' => null,
            ],
        ];

        foreach ($parameters as $parameter) {
            $parameterService->createViaArray($parameter);
        }
    }
}
