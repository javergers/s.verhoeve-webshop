<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180327130244 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_collection CHANGE fraud_score fraud_score INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fraud_detection_report DROP FOREIGN KEY FK_9E097ACCFC2A1E2F');
        $this->addSql('ALTER TABLE fraud_detection_report ADD CONSTRAINT FK_9E097ACCFC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE discount CHANGE type type ENUM(\'amount\', \'percentage\')');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE discount CHANGE type type VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fraud_detection_report DROP FOREIGN KEY FK_9E097ACCFC2A1E2F');
        $this->addSql('ALTER TABLE fraud_detection_report ADD CONSTRAINT FK_9E097ACCFC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES order_collection (id)');
        $this->addSql('ALTER TABLE order_collection CHANGE fraud_score fraud_score INT NOT NULL');
    }
}
