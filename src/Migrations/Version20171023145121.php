<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Security\Employee\Group;
use AppBundle\Entity\Security\Employee\Role;
use AppBundle\Entity\Security\Employee\RoleCategory;
use AppBundle\Entity\Security\Employee\User;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171023145121 extends AbstractMigration implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    private $categories;
    private $roles;

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE role (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, column_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, description VARCHAR(255) DEFAULT NULL, INDEX IDX_57698A6A12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(100) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE group_role (group_id INT NOT NULL, role_id INT NOT NULL, INDEX IDX_7E33D11AFE54D947 (group_id), INDEX IDX_7E33D11AD60322AC (role_id), PRIMARY KEY(group_id, role_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_roles_users (adiuvo_user_id INT NOT NULL, role_id INT NOT NULL, INDEX IDX_FD7A3A5FA245953 (adiuvo_user_id), INDEX IDX_FD7A3A5D60322AC (role_id), PRIMARY KEY(adiuvo_user_id, role_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE role ADD CONSTRAINT FK_57698A6A12469DE2 FOREIGN KEY (category_id) REFERENCES role_category (id)');
        $this->addSql('ALTER TABLE group_role ADD CONSTRAINT FK_7E33D11AFE54D947 FOREIGN KEY (group_id) REFERENCES adiuvo_user_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE group_role ADD CONSTRAINT FK_7E33D11AD60322AC FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_roles_users ADD CONSTRAINT FK_FD7A3A5FA245953 FOREIGN KEY (adiuvo_user_id) REFERENCES adiuvo_user (id)');
        $this->addSql('ALTER TABLE role_roles_users ADD CONSTRAINT FK_FD7A3A5D60322AC FOREIGN KEY (role_id) REFERENCES role (id)');
        $this->addSql('CREATE TABLE role_column (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, INDEX IDX_D67566CF12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE role_column ADD CONSTRAINT FK_D67566CF12469DE2 FOREIGN KEY (category_id) REFERENCES role_category (id)');
        $this->addSql('ALTER TABLE role ADD CONSTRAINT FK_57698A6ABE8E8ED5 FOREIGN KEY (column_id) REFERENCES role_column (id)');
        $this->addSql('CREATE INDEX IDX_57698A6ABE8E8ED5 ON role (column_id)');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function postUp(Schema $schema)
    {
        /** @var QueryBuilder $qb */
        $qb =  $this->connection->createQueryBuilder();

        $users = $qb->select('id, roles')->from('adiuvo_user')->execute()->fetchAll();
        foreach($users as $user) {
            $this->connection->beginTransaction();

            echo "Migrating User: " . $user['id'] . PHP_EOL;
            $this->loopRoles($user, 'user');

            $this->connection->commit();
        }

        /** @var QueryBuilder $qb */
        $qb =  $this->connection->createQueryBuilder();

        $groups = $qb->select('id, roles')->from('adiuvo_user_group')->execute()->fetchAll();
        foreach($groups as $group) {
            $this->connection->beginTransaction();

            echo "Migrating Group: " . $group['id'] . PHP_EOL;
            $this->loopRoles($group, 'group');

            $this->connection->commit();
        }
    }

    /**
     * @param        $object
     * @param string $type
     * @return void
     */
    private function loopRoles($object, string $type)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->connection->createQueryBuilder();

        foreach (unserialize($object['roles'], []) as $roleName) {
            $role = null;
            if (!isset($this->roles[$roleName])) {

                $roleArray = [
                    'name' => $this->connection->quote($roleName),
                    'category_id' => $this->getRoleCategory($roleName)
                ];

                $description = '';
                if (strpos($roleName, 'READ')) {
                    $description = 'Lezen';
                } elseif (strpos($roleName, 'CREATE')) {
                    $description = 'Schrijven';
                } elseif (strpos($roleName, 'UPDATE')) {
                    $description = 'Bewerken';
                } elseif (strpos($roleName, 'DELETE')) {
                    $description = 'Verwijderen';
                }

                if($description) {
                    $roleArray['description'] = $this->connection->quote($description);
                }

                $qb->insert('role')->values($roleArray)->execute();
                $role = $this->connection->lastInsertId();
                $this->roles[$roleName] = $role;
            } else {
                $role = $this->roles[$roleName];
            }

            if($role) {
                if($type == 'user') {
                    $column = 'adiuvo_user_id';
                    $table = 'role_roles_users';
                } else {
                    $column = 'group_id';
                    $table = 'group_role';
                }

                $qb->insert($table)->values([$column => $object['id'], 'role_id' => $role])->execute();
            }
        }
    }

    /**
     * @param string $role
     * @return RoleCategory
     */
    private function getRoleCategory(string $role)
    {
        $category = null;
        $forbiddenWords = ['ROLE_', '_READ', '_CREATE', '_UPDATE', '_DELETE'];
        $categoryName = str_replace('_', ' ',
            ucfirst(strtolower(str_replace($forbiddenWords, null, $role))));

        /** @var QueryBuilder $qb */
        $qb = $this->connection->createQueryBuilder();

        if (!isset($this->categories[$categoryName])) {
            $qb->insert('role_category')->values([
                'name' => $this->connection->quote($categoryName)
            ])->execute();

            $category = $this->connection->lastInsertId();
            $this->categories[$categoryName] = $category;

            echo "Added Category: " . $categoryName . " - ". $category .PHP_EOL;
        } else {
            $category = $this->categories[$categoryName];
        }

        return $category;
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE group_role DROP FOREIGN KEY FK_7E33D11AD60322AC');
        $this->addSql('ALTER TABLE role_roles_users DROP FOREIGN KEY FK_FD7A3A5D60322AC');
        $this->addSql('ALTER TABLE role DROP FOREIGN KEY FK_57698A6A12469DE2');
        $this->addSql('ALTER TABLE role DROP FOREIGN KEY FK_57698A6ABE8E8ED5');
        $this->addSql('DROP TABLE role_column');
        $this->addSql('DROP INDEX IDX_57698A6ABE8E8ED5 ON role');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE role_category');
        $this->addSql('DROP TABLE group_role');
        $this->addSql('DROP TABLE role_roles_users');
    }
}
