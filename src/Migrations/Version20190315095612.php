<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190315095612 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE INDEX datadog_idx1 ON audit_logs ( `action`, `tbl`, `logged_at`)');
        $this->addSql('CREATE INDEX datadog_idx2 ON audit_associations ( `fk`, `class`)');
        $this->addSql('CREATE INDEX datadog_idx3 ON audit_associations ( `fk`)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `audit_logs` DROP INDEX datadog_idx1');
        $this->addSql('ALTER TABLE `audit_associations` DROP INDEX datadog_idx2');
        $this->addSql('ALTER TABLE `audit_associations` DROP INDEX datadog_idx3');
    }
}