<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190118080912 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE order_collection_line_vat (id INT AUTO_INCREMENT NOT NULL, order_collection_id INT NOT NULL, vat_rate_id INT NOT NULL, amount DOUBLE PRECISION NOT NULL, INDEX IDX_493EE72BFC2A1E2F (order_collection_id), INDEX IDX_493EE72B43897540 (vat_rate_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE order_collection_line_vat ADD CONSTRAINT FK_493EE72BFC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES order_collection (id)');
        $this->addSql('ALTER TABLE order_collection_line_vat ADD CONSTRAINT FK_493EE72B43897540 FOREIGN KEY (vat_rate_id) REFERENCES vat_rate (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE order_collection_line_vat');
    }
}
