<?php

namespace DoctrineMigrations;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180215084617 extends AbstractMigration implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     * @var array
     */
    private $parameterKeys = ['combination_min_quantity', 'combination_max_quantity'];

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        foreach ($this->parameterKeys as $key) {
            if ($key == 'combination_min_quantity') {
                $label = "Standaard minimale besteleenheid";
                $value = 1;
            } else {
                $label = "Standaard maximale besteleendheid";
                $value = 100;
            }

            $parameter = [
                'key' => $key,
                'entity' => null,
                'label' => $label,
                'value' => $value
            ];

            $this->container->get('app.parameter')->createViaArray($parameter);
        }
    }

    /**
     * @param Schema $schema
     */
    public function postDown(Schema $schema)
    {
        foreach($this->parameterKeys as $key) {
            $this->container->get('app.parameter')->deleteByKey($key);
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
