<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Order\OrderStatus;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170720124237 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        return;
        if ($this->container->get('environment')->is(['prod', 'staging'])) {
            $em = $this->container->get("doctrine")->getManager();

            $orderStatusNew = new OrderStatus();
            $orderStatusNew->setId("new");
            $orderStatusNew->setDescription("Nieuw (nog te verwerken)");
            $orderStatusNew->setClass("orange-300");
            $orderStatusNew->setProcessable(true);
            $orderStatusNew->setCancellable(true);
            $orderStatusNew->setCountable(true);
            $orderStatusNew->setPosition(4);

            $em->persist($orderStatusNew);


            $orderStatusSent = new OrderStatus();
            $orderStatusSent->setId("sent");
            $orderStatusSent->setDescription("Verzonden (naar klant)");
            $orderStatusSent->setClass("success-600");
            $orderStatusSent->setProcessable(false);
            $orderStatusSent->setCancellable(false);
            $orderStatusSent->setCountable(true);
            $orderStatusSent->setPosition(7);

            $em->persist($orderStatusSent);


            $orderStatusComplete = new OrderStatus();
            $orderStatusComplete->setId("complete");
            $orderStatusComplete->setDescription("Voltooid (volledig afgerond)");
            $orderStatusComplete->setClass("success-800");
            $orderStatusComplete->setProcessable(false);
            $orderStatusComplete->setCancellable(false);
            $orderStatusComplete->setCountable(true);
            $orderStatusComplete->setPosition(8);

            $em->persist($orderStatusComplete);


            $orderStatusArchived = new OrderStatus();
            $orderStatusArchived->setId("archived");
            $orderStatusArchived->setDescription("Gearchiveerd");
            $orderStatusArchived->setClass("slate-300");
            $orderStatusArchived->setProcessable(false);
            $orderStatusArchived->setCancellable(false);
            $orderStatusArchived->setCountable(true);
            $orderStatusArchived->setPosition(9);

            $em->persist($orderStatusArchived);

            if ($em->find(OrderStatus::class, "cancelled")) {
                $em->find(OrderStatus::class, "cancelled")->setClass("slate-800");
            }

            if ($em->find(OrderStatus::class, "pending")) {
                $em->find(OrderStatus::class, "pending")->setClass("orange-600");
            }

            $em->flush();
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');
    }
}
