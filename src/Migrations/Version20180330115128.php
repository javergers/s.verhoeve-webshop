<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Assortment\AssortmentProductImage;
use AppBundle\Entity\Catalog\Assortment\AssortmentTranslation;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductBulkPrice;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Catalog\Product\ProductgroupProperty;
use AppBundle\Entity\Catalog\Product\ProductgroupPropertyOption;
use AppBundle\Entity\Catalog\Product\ProductgroupPropertyTranslation;
use AppBundle\Entity\Catalog\Product\ProductImage;
use AppBundle\Entity\Catalog\Product\ProductProperty;
use AppBundle\Entity\Catalog\Product\ProductTranslation;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Common\Image;
use AppBundle\Entity\Common\ImageFormat;
use AppBundle\Entity\Geography\Address;
use AppBundle\Entity\Geography\DeliveryAddressType;
use AppBundle\Entity\Geography\DeliveryAddressTypeField;
use AppBundle\Entity\Geography\DeliveryAddressTypeFieldTranslation;
use AppBundle\Entity\Geography\DeliveryAddressTypeTranslation;
use AppBundle\Entity\Order\FraudDetectionRule;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Entity\Security\Employee\User;
use AppBundle\Entity\Site\Banner;
use AppBundle\Entity\Site\BannerTranslation;
use AppBundle\Entity\Site\Domain;
use AppBundle\Entity\Site\Menu;
use AppBundle\Entity\Site\MenuItem;
use AppBundle\Entity\Site\MenuItemTranslation;
use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Site\PageTranslation;
use AppBundle\Entity\Site\SiteCustomHtml;
use AppBundle\Entity\Site\SiteTranslation;
use AppBundle\Entity\Site\Usp;
use AppBundle\Entity\Site\UspTranslation;
use AppBundle\Entity\Supplier\DeliveryArea;
use AppBundle\Entity\Supplier\SupplierGroupProductTranslation;
use AppBundle\Entity\Supplier\SupplierProductTranslation;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180330115128 extends AbstractMigration
{

    /**
     * @var array
     */
    private $classes = [
        'Adiuvo\\CmsBundle\\Entity\\ImageFormat' => ImageFormat::class,
        'Adiuvo\\CmsBundle\\Entity\\Image' => Image::class,
        'Adiuvo\\CmsBundle\\Entity\\User' => User::class,
        'AdminBundle\\Entity\\ImageFormat' => ImageFormat::class,
        'AppBundle\\Entity\\Page' => Page::class,
        'AppBundle\\Entity\\Address' => Address::class,
        'AppBundle\\Entity\\Assortment' => Assortment::class,
        'AppBundle\\Entity\\AssortmentProductImage' => AssortmentProductImage::class,
        'AppBundle\\Entity\\AssortmentTranslation' => AssortmentTranslation::class,
        'AppBundle\\Entity\\Company' => Company::class,
        'AppBundle\\Entity\\Customer' => Customer::class,
        'AppBundle\\Entity\\Fraud\\FraudDetectionRule' => FraudDetectionRule::class,
        'AppBundle\\Entity\\Order' => OrderCollection::class,
        'AppBundle\\Entity\\OrderLine' => OrderLine::class,
        'AppBundle\\Entity\\OrderOrder' => Order::class,
        'AppBundle\\Entity\\Product' => Product::class,
        'AppBundle\\Entity\\Product\\TransportType' => TransportType::class,
        'AppBundle\\Entity\\ProductCard' => ProductCard::class,
        'AppBundle\\Entity\\Productgroup' => Productgroup::class,
        'AppBundle\\Entity\\ProductgroupProperty' => ProductgroupProperty::class,
        'AppBundle\\Entity\\ProductgroupPropertyOption' => ProductgroupPropertyOption::class,
        'AppBundle\\Entity\\ProductgroupPropertyTranslation' => ProductgroupPropertyTranslation::class,
        'AppBundle\\Entity\\ProductImage' => ProductImage::class,
        'AppBundle\\Entity\\ProductProperty' => ProductProperty::class,
        'AppBundle\\Entity\\ProductTranslation' => ProductTranslation::class,
        'AppBundle\\Entity\\Supplier\\DeliveryArea' => DeliveryArea::class,
        'AppBundle\\Entity\\SupplierGroupProductTranslation' => SupplierGroupProductTranslation::class,
        'AppBundle\\Entity\\SupplierProductTranslation' => SupplierProductTranslation::class,
        'AppBundle\\Entity\\User' => User::class,
        'Topbloemen\\CustomerBundle\\Entity\\Address' => Address::class,
        'Topbloemen\\CustomerBundle\\Entity\\Company' => Company::class,
        'Topbloemen\\CustomerBundle\\Entity\\Customer' => Customer::class,
        'Topbloemen\\CustomerBundle\\Entity\\CustomerGroup' => CustomerGroup::class,
        'Topbloemen\\ShopBundle\\Entity\\Assortment' => Assortment::class,
        'Topbloemen\\ShopBundle\\Entity\\AssortmentTranslation' => AssortmentTranslation::class,
        'Topbloemen\\ShopBundle\\Entity\\Order' => OrderCollection::class,
        'Topbloemen\\ShopBundle\\Entity\\OrderOrder' => Order::class,
        'Topbloemen\\ShopBundle\\Entity\\OrderOrderLine' => OrderLine::class,
        'Topbloemen\\ShopBundle\\Entity\\Product' => Product::class,
        'Topbloemen\\ShopBundle\\Entity\\ProductBulkPrice' => ProductBulkPrice::class,
        'Topbloemen\\ShopBundle\\Entity\\ProductCard' => ProductCard::class,
        'Topbloemen\\ShopBundle\\Entity\\Productgroup' => Productgroup::class,
        'Topbloemen\\ShopBundle\\Entity\\ProductgroupProperty' => ProductgroupProperty::class,
        'Topbloemen\\ShopBundle\\Entity\\ProductgroupPropertyOption' => ProductgroupPropertyOption::class,
        'Topbloemen\\ShopBundle\\Entity\\ProductgroupPropertyTranslation' => ProductgroupPropertyTranslation::class,
        'Topbloemen\\ShopBundle\\Entity\\ProductImage' => ProductImage::class,
        'Topbloemen\\ShopBundle\\Entity\\ProductProperty' => ProductProperty::class,
        'Topbloemen\\ShopBundle\\Entity\\ProductTranslation' => ProductTranslation::class,
        'Topbloemen\\SiteBundle\\Entity\\Banner' => Banner::class,
        'Topbloemen\\SiteBundle\\Entity\\BannerTranslation' => BannerTranslation::class,
        'Topbloemen\\SiteBundle\\Entity\\Menu' => Menu::class,
        'Topbloemen\\SiteBundle\\Entity\\MenuItem' => MenuItem::class,
        'Topbloemen\\SiteBundle\\Entity\\MenuItemTranslation' => MenuItemTranslation::class,
        'Topbloemen\\SiteBundle\\Entity\\Page' => Page::class,
        'Topbloemen\\SiteBundle\\Entity\\PageTranslation' => PageTranslation::class,
        'Topbloemen\\SiteBundle\\Entity\\SiteCustomHtml' => SiteCustomHtml::class,
        'Topbloemen\\SiteBundle\\Entity\\SiteTranslation' => SiteTranslation::class,
        'Topbloemen\\SiteBundle\\Entity\\Usp' => Usp::class,
        'Topbloemen\\SiteBundle\\Entity\\UspTranslation' => UspTranslation::class,
        'Topbloemen\\SiteBundle\\Entity\\Variables\\DeliveryAddressType' => DeliveryAddressType::class,
        'Topbloemen\\SiteBundle\\Entity\\Variables\\DeliveryAddressType\\DeliveryAddressTypeField' => DeliveryAddressTypeField::class,
        'Topbloemen\\SiteBundle\\Entity\\Variables\\DeliveryAddressType\\DeliveryAddressTypeFieldTranslation' => DeliveryAddressTypeFieldTranslation::class,
        'Topbloemen\\SiteBundle\\Entity\\Variables\\DeliveryAddressTypeTranslation' => DeliveryAddressTypeTranslation::class,
        //'Topbloemen\\SupplierBundle\\Entity\\Supplier' => bestaat niet meer,
        'Topbloemen\\SupplierBundle\\Entity\\SupplierGroupProductTranslation' => SupplierGroupProductTranslation::class,
        'Topbloemen\\SupplierBundle\\Entity\\SupplierProductTranslation' => SupplierProductTranslation::class,
        'Proxies\\__CG__\\Topbloemen\\SiteBundle\\Entity\\Banner' => Banner::class,
        'Topbloemen\\SiteBundle\\Entity\\Domain' => Domain::class
    ];

    /**
     * @var array
     */
    protected $tables = [
        'adiuvo_image' => 'entity_name',
        'adiuvo_image_format' => 'entity',
        'ext_log_entries' => 'object_class',
        'jms_job_related_entities' => 'related_class',
        'menu_item' => 'entity_name',
        'seo_redirect' => 'entity_class'
    ];


    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\DBALException
     */
    public function postUp(Schema $schema)
    {
        foreach($this->tables as $table => $column) {
            foreach($this->classes as $oldClass => $newClass) {
                $this->connection->executeQuery('UPDATE `'.$table.'` SET `'.$column.'` = "'.addslashes($newClass).'" WHERE `'.$column.'` = "'.addslashes($oldClass).'"');
            }
        }
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\DBALException
     */
    public function postDown(Schema $schema)
    {
        foreach($this->tables as $table => $column) {
            foreach($this->classes as $oldClass => $newClass) {
                $this->connection->executeQuery('UPDATE `'.$table.'` SET `'.$column.'` = "'.addslashes($oldClass).'" WHERE `'.$column.'` = "'.addslashes($newClass).'"');
            }
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
