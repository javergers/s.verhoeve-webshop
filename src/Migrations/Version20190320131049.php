<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190320131049 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE designer_template_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, title VARCHAR(100) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_CF127A412C2AC5D3 (translatable_id), UNIQUE INDEX designer_template_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE designer_template_translation ADD CONSTRAINT FK_CF127A412C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES designer_template (id) ON DELETE CASCADE');
    }

    public function postUp(Schema $schema)
    {
        $stmt = $this->connection->prepare('INSERT INTO designer_template_translation (`translatable_id`, `title`, `locale`)
SELECT `id` as `translatable_id`, `name` as `title`, \'nl_NL\' as `locale` FROM designer_template');

        $stmt->execute();
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE designer_template_translation');
    }
}
