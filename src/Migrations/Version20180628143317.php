<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180628143317 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company ADD invoice_address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094FC6BDFEB FOREIGN KEY (invoice_address_id) REFERENCES address (id)');
        $this->addSql('CREATE INDEX IDX_4FBF094FC6BDFEB ON company (invoice_address_id)');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->connection->createQueryBuilder();

        $results = $qb->select('c.id, a.id as address_id')
            ->from('company', 'c')
            ->leftJoin('c', 'address', 'a', 'c.id = a.company_id')
            ->andWhere('a.invoice_address = 1')
            ->execute()
            ->fetchAll()
        ;

        /** @var QueryBuilder $qb */
        $qb = $this->connection->createQueryBuilder();
        $updateQuery = $qb->update('company', 'c')->set('invoice_address_id', ':address_id')->where('id = :company_id');
        foreach($results as $result) {
            $updateQuery->setParameters([
                'address_id' => $result['address_id'],
                'company_id' => $result['id']
            ])->execute();
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094FC6BDFEB');
        $this->addSql('DROP INDEX IDX_4FBF094FC6BDFEB ON company');
        $this->addSql('ALTER TABLE company DROP invoice_address_id');
    }
}
