<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161205100936 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE order_activity (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, customer_id INT DEFAULT NULL, order_collection_id INT NOT NULL, order_id INT DEFAULT NULL, activity VARCHAR(100) NOT NULL, text LONGTEXT NOT NULL, datetime DATETIME NOT NULL, INDEX IDX_4D8CEBEBA76ED395 (user_id), INDEX IDX_4D8CEBEB9395C3F3 (customer_id), INDEX IDX_4D8CEBEBFC2A1E2F (order_collection_id), INDEX IDX_4D8CEBEB8D9F6D38 (order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE order_activity ADD CONSTRAINT FK_4D8CEBEBA76ED395 FOREIGN KEY (user_id) REFERENCES adiuvo_user (id)');
        $this->addSql('ALTER TABLE order_activity ADD CONSTRAINT FK_4D8CEBEB9395C3F3 FOREIGN KEY (customer_id) REFERENCES `customer` (id)');
        $this->addSql('ALTER TABLE order_activity ADD CONSTRAINT FK_4D8CEBEBFC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE order_activity ADD CONSTRAINT FK_4D8CEBEB8D9F6D38 FOREIGN KEY (order_id) REFERENCES order_order (id)');
        $this->addSql('ALTER TABLE order_status ADD cancellable TINYINT(1) NOT NULL');
        $this->addSql('UPDATE order_status SET cancellable = 1 WHERE id IN (\'on_hold\', \'payment_pending\', \'pending\')');
        $this->addSql('ALTER TABLE order_status ADD countable TINYINT(1) NOT NULL');
        $this->addSql('UPDATE `order_status` SET countable = 1 WHERE id IN (\'on_hold\', \'payment_pending\', \'pending\', \'processed\')');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE order_activity');
        $this->addSql('ALTER TABLE order_status DROP cancellable');
        $this->addSql('ALTER TABLE order_status DROP countable');
    }
}
