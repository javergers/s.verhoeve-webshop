<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180118133213 extends AbstractMigration implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE supplier_group ADD delivery_vat_id INT DEFAULT NULL, ADD delivery_interval VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE supplier_group ADD CONSTRAINT FK_B4DB46EFABA7C3E5 FOREIGN KEY (delivery_vat_id) REFERENCES vat (id)');
        $this->addSql('CREATE INDEX IDX_B4DB46EFABA7C3E5 ON supplier_group (delivery_vat_id)');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $vat = $this->connection->createQueryBuilder()->select('id')->from('vat')->where('description = "Laag"')->setMaxResults(1)->execute()->fetchAll();

        $this->connection->beginTransaction();

        /** @var QueryBuilder $qb */
        $qb = $this->connection->createQueryBuilder();

        if ($vat) {
            $this->connection->createQueryBuilder()->update('supplier_group')->values([
                'delivery_interval' => 'P1D7H',
                'delivery_vat_id' => $vat[0]['id']
            ])->execute();
        }

        $this->connection->commit();
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE supplier_group DROP FOREIGN KEY FK_B4DB46EFABA7C3E5');
        $this->addSql('DROP INDEX IDX_B4DB46EFABA7C3E5 ON supplier_group');
        $this->addSql('ALTER TABLE supplier_group DROP delivery_vat_id, DROP delivery_interval');
    }
}
