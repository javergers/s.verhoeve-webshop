<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180125131724 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function preUp(Schema $schema)
    {
        // Unfortunately, this query cannot be built with query builder
        $sql = "
            UPDATE productgroup_property pgp
            JOIN productgroup_property_translation pgpt ON pgp.id = pgpt.translatable_id AND locale = 'nl_NL'
            SET pgp.key = REPLACE(LOWER(pgpt.description), ' ', '-')
            WHERE pgp.key IS NULL
        ";

        $this->connection->query($sql);
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE productgroup_property CHANGE `key` `key` VARCHAR(40) NOT NULL, CHANGE is_unique is_unique TINYINT(1) DEFAULT NULL, CHANGE on_frontend on_frontend TINYINT(1) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_F77F0C7D4E645A7E ON productgroup_property');
        $this->addSql('ALTER TABLE productgroup_property CHANGE `key` `key` VARCHAR(40) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE is_unique is_unique TINYINT(1) NOT NULL, CHANGE on_frontend on_frontend TINYINT(1) NOT NULL');
    }
}
