<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Order\OrderDeliveryStatus;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170724145506 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_delivery_status CHANGE paazl_status paazl_status VARCHAR(25) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        return;
        if ($this->container->get('environment')->is(['prod', 'staging'])) {
            $em = $this->container->get("doctrine")->getManager();

            $orderDeliveryStatusNew = new OrderDeliveryStatus();
            $orderDeliveryStatusNew->setId("failed");
            $orderDeliveryStatusNew->setDescription("Levering mislukt");

            $em->persist($orderDeliveryStatusNew);

            $em->flush();
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE order_delivery_status CHANGE paazl_status paazl_status VARCHAR(25) NOT NULL COLLATE utf8_unicode_ci');
    }
}
