<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Payment\PaymentStatus;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171031163512 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        return;
        $paymentStatus = (new PaymentStatus())
            ->setId("reversed")
            ->setDescription("Teruggevorderd")
            ->setClass("danger-300")
            ->setCountable(true);

        $this->container->get("doctrine")->getManager()->persist($paymentStatus);
        $this->container->get("doctrine")->getManager()->flush();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
