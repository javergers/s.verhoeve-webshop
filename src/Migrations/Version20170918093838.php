<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Payment\Paymentmethod;
use AppBundle\Entity\Site\Site;
use AppBundle\Entity\Site\SitePaymentMethodSettings;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170918093838 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE site_payment_method_settings (id INT AUTO_INCREMENT NOT NULL, site_id INT DEFAULT NULL, payment_method_id INT DEFAULT NULL, enabled TINYINT(1) NOT NULL, INDEX IDX_2221C89FF6BD1646 (site_id), INDEX IDX_2221C89F5AA1164F (payment_method_id), UNIQUE INDEX UNIQ_2221C89FF6BD16465AA1164F (site_id, payment_method_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE site_payment_method_settings ADD CONSTRAINT FK_2221C89FF6BD1646 FOREIGN KEY (site_id) REFERENCES site (id)');
        $this->addSql('ALTER TABLE site_payment_method_settings ADD CONSTRAINT FK_2221C89F5AA1164F FOREIGN KEY (payment_method_id) REFERENCES paymentmethod (id)');
    }

    public function postUp(Schema $schema)
    {
        return;
        if ($this->container->get('environment')->is(['prod', 'staging'])) {
            $this->connection->beginTransaction();

            /** @var Site[] $sites */
            $sites = $this->container->get("doctrine")->getRepository(Site::class)->findAll();

            /** @var Paymentmethod[] $paymentMethods */
            $paymentMethods = $this->container->get("doctrine")->getRepository(Paymentmethod::class)->findAll();

            foreach ($sites as $site) {
                foreach ($paymentMethods as $paymentMethod) {
                    $paymentMethodSettings = new SitePaymentMethodSettings();
                    $paymentMethodSettings->setSite($site);
                    $paymentMethodSettings->setPaymentMethod($paymentMethod);
                    $paymentMethodSettings->setEnabled(true);

                    $site->addPaymentMethodSetting($paymentMethodSettings);
                }
            }

            $this->container->get("doctrine")->getManager()->flush();

            $this->connection->commit();
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE site_payment_method_settings');
    }
}
