<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyConnectorParameter;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170623133533 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company_connector_parameter (company_id INT NOT NULL, connector VARCHAR(20) NOT NULL, name VARCHAR(100) NOT NULL, value LONGTEXT DEFAULT NULL, INDEX IDX_F048FE34979B1AD6 (company_id), PRIMARY KEY(company_id, connector, name)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_connector_parameter ADD CONSTRAINT FK_F048FE34979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        return;
        if ($this->container->get('environment')->is(['prod', 'staging'])) {
            $this->connection->beginTransaction();

            /** @var Company[] $companies */
            $companies = $this->container->get("doctrine")->getRepository(Company::class)->findBy([
                "supplierConnector" => "BakkerMail",
            ]);

            foreach ($companies as $company) {
                /** @var CompanyConnectorParameter $companyConnectorParameter */
                $companyConnectorParameter = $this->container->get("doctrine")->getRepository(CompanyConnectorParameter::class)->findOneBy([
                    "company" => $company,
                    "connector" => "BakkerMail",
                    "name" => "email",
                ]);

                if ($companyConnectorParameter && $companyConnectorParameter->getValue()) {
                    continue;
                }

                $companyConnectorParameter = new CompanyConnectorParameter();
                $companyConnectorParameter->setCompany($company);
                $companyConnectorParameter->setConnector("BakkerMail");
                $companyConnectorParameter->setName("email");
                $companyConnectorParameter->setValue($company->getEmail());

                $this->container->get("doctrine")->getManager()->persist($companyConnectorParameter);
                $this->container->get("doctrine")->getManager()->flush();
            }

            $this->connection->commit();
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE company_connector_parameter');
    }
}
