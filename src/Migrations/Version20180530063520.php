<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class Version20180530063520
 * @package Application\Migrations
 */
class Version20180530063520 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema): void
    {
        /** @var Connection $conn */
        $conn = $this->container->get('doctrine')->getConnection();

        $qb = $conn->createQueryBuilder();

        $qb
            ->update('product')
            ->set('product.entity_type', ':entityType')
            ->where('product.sku LIKE :sku')
            ->setParameter('entityType', 'product_personalization')
            ->setParameter('sku', 'PER%')
        ;

        if($qb->execute()) {
            $this->write('Update field `entity_type` to `product_personalization` for all personalization products');
        }
    }

    /**
     * @param Schema $schema
     */
    public function preDown(Schema $schema): void
    {
        /** @var Connection $conn */
        $conn = $this->container->get('doctrine')->getConnection();

        $qb = $conn->createQueryBuilder();

        $qb
            ->update('product')
            ->set('product.entity_type', ':entityType')
            ->where('product.sku LIKE :sku')
            ->setParameter('entityType', 'product')
            ->setParameter('sku', 'PER%')
        ;

        if($qb->execute()) {
            $this->write('Update field `entity_type` back to `product` for all personalization products');
        }
    }

}
