<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190201130300 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE designer_template ADD company_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE designer_template ADD CONSTRAINT FK_81C553D3979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('CREATE INDEX IDX_81C553D3979B1AD6 ON designer_template (company_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE designer_template DROP FOREIGN KEY FK_81C553D3979B1AD6');
        $this->addSql('DROP INDEX IDX_81C553D3979B1AD6 ON designer_template');
        $this->addSql('ALTER TABLE designer_template DROP company_id');
    }
}
