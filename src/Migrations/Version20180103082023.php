<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180103082023 extends AbstractMigration implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    private $parameterKeys = ['fraud_warning_score', 'fraud_blocked_score'];

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        foreach ($this->parameterKeys as $key) {
            if ($key == 'fraud_warning_score') {
                $label = "Fraudescore waarschuwingsscore";
                $score = 30;
            } else {
                $label = "Fraudescore blokkadescore";
                $score = 50;
            }

            $parameter = [
                'key' => $key,
                'entity' => null,
                'label' => $label,
                'value' => $score
            ];

            $this->container->get('app.parameter')->createViaArray($parameter);
        }
    }

    /**
     * @param Schema $schema
     */
    public function preDown(Schema $schema)
    {
        foreach($this->parameterKeys as $key) {
            $this->container->get('app.parameter')->deleteByKey($key);
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
