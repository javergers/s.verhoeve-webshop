<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Entity\Site\Site;
use AppBundle\Entity\Catalog\Product\Vat;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171025094712 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE product SET entity_type = \'product_transport_type\' WHERE sku LIKE \'DLV%\' ');
    }

    /**
     * @param Schema $schema
     * @throws \Exception
     */
    public function postUp(Schema $schema)
    {
        if (in_array($this->container->get('kernel')->getEnvironment(), ['prod', 'staging'])) {
            $this->container->get("doctrine")->getConnection()->beginTransaction();

            $em = $this->container->get("doctrine")->getManager();

            /** @var Productgroup $productGroup */
            $productGroup = $em->getRepository(Productgroup::class)->findOneBy([
                "skuPrefix" => "DLV",
            ]);

            if (!$productGroup) {
                throw new \Exception("Productgroup for delivery not found");
            }

            $transportTypePakket = $productGroup->getProducts()->filter(function (Product $product) {
                return $product->getVat()->getPercentage() == 6;
            })->current();

            $transportTypePallet = $productGroup->getProducts()->filter(function (Product $product) {
                return $product->getVat()->getPercentage() == 21;
            })->current();

            if (!$transportTypePakket || !$transportTypePallet) {
                throw new \Exception("Pakket and Pallet delivery costs products not found");
            }

            /** @var Site[] $sites */
            $sites = $em->getRepository(Site::class)->findAll();

            foreach ($sites as $site) {
                switch ($site->getTheme()) {
                    case "bloemisten";
                        $site->setDefaultProductTransportType($transportTypePallet);

                        break;
                    default:
                        $site->setDefaultProductTransportType($transportTypePakket);

                        break;
                }
            }

            $post = new TransportType();
            $post->translate("nl_NL")->setTitle("Post");
            $post->setPrice(2.95);
            $post->setVat($em->getRepository(Vat::class)->find(1));
            $post->setProductgroup($productGroup);
            $post->setPurchasable(false);
            $post->mergeNewTranslations();

            $em->persist($post);

            $pakket = new TransportType();
            $pakket->translate("nl_NL")->setTitle("Pakket");
            $pakket->setPrice(round(6.95 / 1.21, 3));
            $pakket->setVat($em->getRepository(Vat::class)->find(3));
            $pakket->setProductgroup($productGroup);
            $pakket->setPurchasable(false);
            $pakket->mergeNewTranslations();

            $em->persist($pakket);

            $pallet = new TransportType();
            $pallet->translate("nl_NL")->setTitle("Pallet");
            $pallet->setPrice(round(29.50 / 1.21, 3));
            $pallet->setVat($em->getRepository(Vat::class)->find(3));
            $pallet->setProductgroup($productGroup);
            $pallet->setPurchasable(false);
            $pallet->mergeNewTranslations();

            $em->persist($pallet);

            $jodeco = new TransportType();
            $jodeco->translate("nl_NL")->setTitle("Jodeco");
            $jodeco->setPrice(round(29.50 / 1.21, 3));
            $jodeco->setVat($em->getRepository(Vat::class)->find(3));
            $jodeco->setProductgroup($productGroup);
            $jodeco->setPurchasable(false);
            $jodeco->mergeNewTranslations();

            $em->persist($jodeco);

            $em->flush();

            $this->container->get("doctrine")->getConnection()->commit();
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM product WHERE entity_type = \'product_transport_type\' AND DATE(created_at) > \'2017-10-01\' ');
        $this->addSql('UPDATE product SET entity_type = \'product\' WHERE sku LIKE \'DLV%\' ');
    }
}
