<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180828085020 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fraud_detection_report DROP FOREIGN KEY FK_9E097ACCFC2A1E2F');
        $this->addSql('ALTER TABLE fraud_detection_report ADD CONSTRAINT FK_9E097ACCFC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES order_collection (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fraud_detection_report DROP FOREIGN KEY FK_9E097ACCFC2A1E2F');
        $this->addSql('ALTER TABLE fraud_detection_report ADD CONSTRAINT FK_9E097ACCFC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES `order` (id)');
    }
}
