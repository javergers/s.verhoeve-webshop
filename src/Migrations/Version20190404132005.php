<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190404132005 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company CHANGE price_display_mode price_display_mode ENUM(\'original\', \'discounted\', \'both\') DEFAULT \'original\' COMMENT \'(DC2Type:CompanyPriceDisplayModeType)\' NOT NULL');

    }

    /**
     * @param Schema $schema
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function preUp(Schema $schema)
    {
        $this->connection->query('UPDATE company SET price_display_mode = "original" WHERE price_display_mode IS NULL')->execute();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company CHANGE price_display_mode price_display_mode ENUM(\'original\', \'discounted\', \'both\') DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:CompanyPriceDisplayModeType)\'');
    }
}
