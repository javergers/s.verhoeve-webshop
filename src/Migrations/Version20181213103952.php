<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181213103952 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company ADD company_product_sku_prefix VARCHAR(5) DEFAULT NULL, CHANGE parent_id parent_id INT DEFAULT NULL, CHANGE default_delivery_address_id default_delivery_address_id INT DEFAULT NULL, CHANGE company_registration_id company_registration_id INT DEFAULT NULL, CHANGE menu_id menu_id INT DEFAULT NULL, CHANGE invoice_address_id invoice_address_id INT DEFAULT NULL, CHANGE name name VARCHAR(255) DEFAULT NULL, CHANGE chamber_of_commerce_number chamber_of_commerce_number VARCHAR(100) DEFAULT NULL, CHANGE vat_number vat_number VARCHAR(100) DEFAULT NULL, CHANGE deleted_at deleted_at DATETIME DEFAULT NULL, CHANGE twinfield_relation_number twinfield_relation_number VARCHAR(9) DEFAULT NULL, CHANGE is_customer is_customer TINYINT(1) DEFAULT NULL, CHANGE is_supplier is_supplier TINYINT(1) DEFAULT NULL, CHANGE supplier_connector supplier_connector VARCHAR(20) DEFAULT NULL, CHANGE phone_number phone_number VARCHAR(35) DEFAULT NULL, CHANGE email email VARCHAR(200) DEFAULT NULL, CHANGE chamber_of_commerce_establishment_number chamber_of_commerce_establishment_number VARCHAR(100) DEFAULT NULL, CHANGE handle_child_orders handle_child_orders TINYINT(1) DEFAULT NULL, CHANGE bill_on_parent bill_on_parent TINYINT(1) DEFAULT NULL, CHANGE iban iban VARCHAR(30) DEFAULT NULL, CHANGE bic bic VARCHAR(10) DEFAULT NULL, CHANGE bank_account_holder bank_account_holder VARCHAR(100) DEFAULT NULL, CHANGE metadata metadata LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', CHANGE invoice_email invoice_email VARCHAR(200) DEFAULT NULL, CHANGE price_display_mode price_display_mode ENUM(\'original\', \'discounted\', \'both\') DEFAULT NULL COMMENT \'(DC2Type:CompanyPriceDisplayModeType)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company DROP company_product_sku_prefix, CHANGE company_registration_id company_registration_id INT DEFAULT NULL, CHANGE invoice_address_id invoice_address_id INT DEFAULT NULL, CHANGE menu_id menu_id INT DEFAULT NULL, CHANGE parent_id parent_id INT DEFAULT NULL, CHANGE default_delivery_address_id default_delivery_address_id INT DEFAULT NULL, CHANGE twinfield_relation_number twinfield_relation_number VARCHAR(9) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE name name VARCHAR(255) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE phone_number phone_number VARCHAR(35) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE email email VARCHAR(200) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE invoice_email invoice_email VARCHAR(200) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE is_customer is_customer TINYINT(1) DEFAULT \'NULL\', CHANGE is_supplier is_supplier TINYINT(1) DEFAULT \'NULL\', CHANGE handle_child_orders handle_child_orders TINYINT(1) DEFAULT \'NULL\', CHANGE bill_on_parent bill_on_parent TINYINT(1) DEFAULT \'NULL\', CHANGE chamber_of_commerce_number chamber_of_commerce_number VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE chamber_of_commerce_establishment_number chamber_of_commerce_establishment_number VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE vat_number vat_number VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE supplier_connector supplier_connector VARCHAR(20) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE iban iban VARCHAR(30) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE bic bic VARCHAR(10) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE bank_account_holder bank_account_holder VARCHAR(100) DEFAULT \'NULL\' COLLATE utf8_unicode_ci, CHANGE metadata metadata LONGTEXT DEFAULT \'NULL\' COLLATE utf8_unicode_ci COMMENT \'(DC2Type:json_array)\', CHANGE price_display_mode price_display_mode ENUM(\'original\', \'discounted\', \'both\') DEFAULT \'NULL\' COLLATE utf8_unicode_ci COMMENT \'(DC2Type:CompanyPriceDisplayModeType)\', CHANGE deleted_at deleted_at DATETIME DEFAULT \'NULL\'');

    }
}
