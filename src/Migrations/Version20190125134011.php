<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190125134011 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE commission_batch_company (commission_batch_id INT NOT NULL, company_id INT NOT NULL, INDEX IDX_6DE0820175E4B948 (commission_batch_id), INDEX IDX_6DE08201979B1AD6 (company_id), PRIMARY KEY(commission_batch_id, company_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE commission_batch_company ADD CONSTRAINT FK_6DE0820175E4B948 FOREIGN KEY (commission_batch_id) REFERENCES commission_batch (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE commission_batch_company ADD CONSTRAINT FK_6DE08201979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE commission_batch ADD invoice_date DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company_report_customer_history (id INT AUTO_INCREMENT NOT NULL, company_report_customer_id INT DEFAULT NULL, from_date DATETIME NOT NULL, till_date DATETIME NOT NULL, INDEX IDX_41BD177891B78F44 (company_report_customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_report_customer_history ADD CONSTRAINT FK_41BD177891B78F44 FOREIGN KEY (company_report_customer_id) REFERENCES company_report_customer (id)');
        $this->addSql('DROP TABLE commission_batch_company');
        $this->addSql('ALTER TABLE commission_batch DROP invoice_date');
    }
}
