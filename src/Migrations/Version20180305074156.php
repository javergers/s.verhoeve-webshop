<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180305074156 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE subscription_product DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE subscription_product ADD id INT NOT NULL, CHANGE subscription_id subscription_id INT DEFAULT NULL, CHANGE product_id product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE subscription_product ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE subscription_product CHANGE id id INT NOT NULL AUTO_INCREMENT');
        $this->addSql('ALTER TABLE subscription_address DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE subscription_address ADD id INT NOT NULL, CHANGE subscription_id subscription_id INT DEFAULT NULL, CHANGE address_id address_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE subscription_address ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE subscription_address CHANGE id id INT NOT NULL AUTO_INCREMENT');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE subscription_address MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE subscription_address DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE subscription_address DROP id, CHANGE subscription_id subscription_id INT NOT NULL, CHANGE address_id address_id INT NOT NULL');
        $this->addSql('ALTER TABLE subscription_address ADD PRIMARY KEY (subscription_id, address_id)');
        $this->addSql('ALTER TABLE subscription_product MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE subscription_product DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE subscription_product DROP id, CHANGE subscription_id subscription_id INT NOT NULL, CHANGE product_id product_id INT NOT NULL');
        $this->addSql('ALTER TABLE subscription_product ADD PRIMARY KEY (subscription_id, product_id)');
    }
}
