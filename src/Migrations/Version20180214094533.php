<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180214094533 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE report_column DROP FOREIGN KEY FK_54A052E1B06CCB88');
        $this->addSql('DROP INDEX IDX_54A052E1B06CCB88 ON report_column');
        $this->addSql('ALTER TABLE report_column ADD name VARCHAR(255) NOT NULL, ADD path VARCHAR(255) NOT NULL, DROP entity_relation_id');
        $this->addSql('ALTER TABLE report_column CHANGE `order` position INT NOT NULL');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE report_column CHANGE position `order` INT NOT NULL');
        $this->addSql('ALTER TABLE report_column ADD entity_relation_id INT DEFAULT NULL, DROP name, DROP path');
        $this->addSql('ALTER TABLE report_column ADD CONSTRAINT FK_54A052E1B06CCB88 FOREIGN KEY (entity_relation_id) REFERENCES entity_relation (id)');
        $this->addSql('CREATE INDEX IDX_54A052E1B06CCB88 ON report_column (entity_relation_id)');
    }
}
