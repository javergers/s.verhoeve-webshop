<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180213153131 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company_report_customer (id INT AUTO_INCREMENT NOT NULL, company_report_id INT DEFAULT NULL, customer_id INT DEFAULT NULL, frequency ENUM(\'daily\', \'weekly\', \'twoweekly\', \'monthly\', \'quarterly\', \'yearly\') NOT NULL COMMENT \'(DC2Type:CompanyReportCustomerFrequencyType)\', INDEX IDX_CEA82D0978C92E30 (company_report_id), INDEX IDX_CEA82D099395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_report (id INT AUTO_INCREMENT NOT NULL, report_id INT DEFAULT NULL, company_id INT DEFAULT NULL, INDEX IDX_F9B013F14BD2A4C0 (report_id), INDEX IDX_F9B013F1979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_report_customer ADD CONSTRAINT FK_CEA82D0978C92E30 FOREIGN KEY (company_report_id) REFERENCES company_report (id)');
        $this->addSql('ALTER TABLE company_report_customer ADD CONSTRAINT FK_CEA82D099395C3F3 FOREIGN KEY (customer_id) REFERENCES `customer` (id)');
        $this->addSql('ALTER TABLE company_report ADD CONSTRAINT FK_F9B013F14BD2A4C0 FOREIGN KEY (report_id) REFERENCES report (id)');
        $this->addSql('ALTER TABLE company_report ADD CONSTRAINT FK_F9B013F1979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company_report_customer DROP FOREIGN KEY FK_CEA82D0978C92E30');
        $this->addSql('DROP TABLE company_report_customer');
        $this->addSql('DROP TABLE company_report');
    }
}
