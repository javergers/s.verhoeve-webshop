<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170206104306 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product ADD google_shopping_taxonomy_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADF55DC6B7 FOREIGN KEY (google_shopping_taxonomy_id) REFERENCES google_shopping_taxonomy (id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADF55DC6B7 ON product (google_shopping_taxonomy_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADF55DC6B7');
        $this->addSql('DROP INDEX IDX_D34A04ADF55DC6B7 ON product');
        $this->addSql('ALTER TABLE product DROP google_shopping_taxonomy_id');
    }
}
