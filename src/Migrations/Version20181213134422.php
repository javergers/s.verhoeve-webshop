<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Finance\Tax\VatGroup;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181213134422 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameters = [
            'commission_fee_vatgroup' => 'Commissie fee BTW group',
        ];

        foreach ($parameters as $key => $label) {
            $parameter = [
                'key' => $key,
                'entity' => null,
                'label' => $label,
                'value' => null,
                'form_type' => EntityType::class,
                'form_type_class' => VatGroup::class,
                'required' => true,
            ];

            $parameterService->createViaArray($parameter);
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
