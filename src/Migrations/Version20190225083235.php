<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Relation\Company;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190225083235 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company ADD credit_limit DOUBLE PRECISION DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company DROP credit_limit');
    }

    /**
     * @param Schema $schema
     * @throws ConnectionException
     */
    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameter = [
            'key' => 'company_credit_limit',
            'entity' => null,
            'label' => 'Krediet limiet bedrijven',
            'value' => Company::CREDIT_LIMIT,
        ];

        $parameterService->createViaArray($parameter);
    }
}
