<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Order\OrderLine;
use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class Version20180516075900
 * @package Application\Migrations
 */
class Version20180516075900 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `order_line` ADD `uuid` CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\' AFTER `id`');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9CE58EE1D17F50A6 ON `order_line` (`uuid`)');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        parent::postUp($schema);

        /** @var QueryBuilder $qb */
        $qb = $this->connection->createQueryBuilder();

        $qb->update('order_line', 'ol')
            ->set('ol.uuid', 'UUID()')
            ->where('ol.uuid IS NULL')
            ->execute();
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE order_line DROP FOREIGN KEY FK_9CE58EE1A7B913DD');
        $this->addSql('DROP INDEX UNIQ_9CE58EE1D17F50A6 ON order_line');

    }
}
