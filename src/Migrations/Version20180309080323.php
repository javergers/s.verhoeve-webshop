<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180309080323 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');


        $this->addSql('CREATE TABLE discount (id INT AUTO_INCREMENT NOT NULL, rule_id INT DEFAULT NULL, type enum(\'amount\', \'percentage\'), value NUMERIC(6, 3) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_E1E0B40E744E0351 (rule_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE discount_company (discount_id INT NOT NULL, company_id INT NOT NULL, INDEX IDX_F9B7645E4C7C611F (discount_id), INDEX IDX_F9B7645E979B1AD6 (company_id), PRIMARY KEY(discount_id, company_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE voucher (id INT AUTO_INCREMENT NOT NULL, discount_id INT DEFAULT NULL, description VARCHAR(64) NOT NULL, code VARCHAR(32) NOT NULL, valid_from DATETIME NOT NULL, valid_until DATETIME NOT NULL, `usage` ENUM(\'once\', \'limited\', \'unlimited\') NOT NULL COMMENT \'(DC2Type:VoucherUsageType)\', `limit` INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_1392A5D84C7C611F (discount_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE discount ADD CONSTRAINT FK_E1E0B40E744E0351 FOREIGN KEY (rule_id) REFERENCES rule (id)');
        $this->addSql('ALTER TABLE discount_company ADD CONSTRAINT FK_F9B7645E4C7C611F FOREIGN KEY (discount_id) REFERENCES discount (id)');
        $this->addSql('ALTER TABLE discount_company ADD CONSTRAINT FK_F9B7645E979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE voucher ADD CONSTRAINT FK_1392A5D84C7C611F FOREIGN KEY (discount_id) REFERENCES discount (id)');
        $this->addSql('CREATE TABLE voucher_batch (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, prefix VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE discount CHANGE type type enum(\'amount\', \'percentage\')');
        $this->addSql('ALTER TABLE voucher ADD batch_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE voucher ADD CONSTRAINT FK_1392A5D8F39EBE7A FOREIGN KEY (batch_id) REFERENCES voucher_batch (id)');
        $this->addSql('CREATE INDEX IDX_1392A5D8F39EBE7A ON voucher (batch_id)');        $this->addSql('ALTER TABLE voucher_batch ADD user_id INT DEFAULT NULL, ADD valid_from DATETIME NOT NULL, ADD valid_until DATETIME NOT NULL, ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE voucher_batch ADD CONSTRAINT FK_3DB004EAA76ED395 FOREIGN KEY (user_id) REFERENCES adiuvo_user (id)');
        $this->addSql('CREATE INDEX IDX_3DB004EAA76ED395 ON voucher_batch (user_id)');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE voucher_batch DROP FOREIGN KEY FK_3DB004EAA76ED395');
        $this->addSql('DROP INDEX IDX_3DB004EAA76ED395 ON voucher_batch');
        $this->addSql('ALTER TABLE discount CHANGE type type VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('DROP INDEX IDX_1392A5D8F39EBE7A ON voucher');
        $this->addSql('ALTER TABLE discount_company DROP FOREIGN KEY FK_F9B7645E4C7C611F');
        $this->addSql('ALTER TABLE voucher DROP FOREIGN KEY FK_1392A5D84C7C611F');
        $this->addSql('DROP TABLE discount');
        $this->addSql('DROP TABLE discount_company');
        $this->addSql('DROP TABLE voucher');
        $this->addSql('DROP TABLE voucher_batch');
    }
}
