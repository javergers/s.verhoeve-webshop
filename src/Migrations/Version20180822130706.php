<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180822130706 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company_site_custom_field_translation DROP FOREIGN KEY FK_A3FCA66A2C2AC5D3');
        $this->addSql('CREATE TABLE company_custom_field (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, field_key VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, required TINYINT(1) NOT NULL, multiple TINYINT(1) DEFAULT NULL, position INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_663577FF979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_custom_field_option (id INT AUTO_INCREMENT NOT NULL, company_custom_field_id INT DEFAULT NULL, option_key VARCHAR(255) NOT NULL, option_value VARCHAR(255) NOT NULL, position INT NOT NULL, INDEX IDX_50280F53EE14DAA9 (company_custom_field_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_custom_field_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, label VARCHAR(60) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_AA7770D62C2AC5D3 (translatable_id), UNIQUE INDEX company_custom_field_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_custom_field ADD CONSTRAINT FK_663577FF979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE company_custom_field_option ADD CONSTRAINT FK_50280F53EE14DAA9 FOREIGN KEY (company_custom_field_id) REFERENCES company_custom_field (id)');
        $this->addSql('ALTER TABLE company_custom_field_translation ADD CONSTRAINT FK_AA7770D62C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES company_custom_field (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE company_site_custom_field');
        $this->addSql('DROP TABLE company_site_custom_field_translation');
        $this->addSql('ALTER TABLE order_collection ADD metadata LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\'');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company_custom_field_option DROP FOREIGN KEY FK_50280F53EE14DAA9');
        $this->addSql('ALTER TABLE company_custom_field_translation DROP FOREIGN KEY FK_AA7770D62C2AC5D3');
        $this->addSql('CREATE TABLE company_site_custom_field (id INT AUTO_INCREMENT NOT NULL, company_site_id INT NOT NULL, type VARCHAR(30) NOT NULL COLLATE utf8_unicode_ci, required TINYINT(1) NOT NULL, multiple TINYINT(1) NOT NULL, position INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_FEFB17AD6EF0C91 (company_site_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE company_site_custom_field_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, label VARCHAR(60) DEFAULT NULL COLLATE utf8_unicode_ci, `values` LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, locale VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, UNIQUE INDEX company_site_custom_field_translation_unique_translation (translatable_id, locale), INDEX IDX_A3FCA66A2C2AC5D3 (translatable_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_site_custom_field ADD CONSTRAINT FK_FEFB17AD6EF0C91 FOREIGN KEY (company_site_id) REFERENCES company_site (id)');
        $this->addSql('ALTER TABLE company_site_custom_field_translation ADD CONSTRAINT FK_A3FCA66A2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES company_site_custom_field (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE company_custom_field');
        $this->addSql('DROP TABLE company_custom_field_option');
        $this->addSql('DROP TABLE company_custom_field_translation');
        $this->addSql('ALTER TABLE order_collection DROP metadata');
    }
}
