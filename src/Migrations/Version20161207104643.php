<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161207104643 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product ADD image_path LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE `order` CHANGE invoice_address_phone_number invoice_address_phone_number VARCHAR(35) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `order` CHANGE invoice_address_phone_number invoice_address_phone_number VARCHAR(35) DEFAULT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:phone_number)\'');
        $this->addSql('ALTER TABLE product DROP image_path');
    }
}
