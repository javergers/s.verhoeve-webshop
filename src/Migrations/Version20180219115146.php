<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180219115146 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE assortment ADD content_id INT DEFAULT NULL, ADD content_position INT DEFAULT NULL, ADD content_size VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE assortment ADD CONSTRAINT FK_2A5E6D8B84A0A3ED FOREIGN KEY (content_id) REFERENCES page (id)');
        $this->addSql('CREATE INDEX IDX_2A5E6D8B84A0A3ED ON assortment (content_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE assortment DROP FOREIGN KEY FK_2A5E6D8B84A0A3ED');
        $this->addSql('DROP INDEX IDX_2A5E6D8B84A0A3ED ON assortment');
        $this->addSql('ALTER TABLE assortment DROP content_id, DROP content_position, DROP content_size');
    }
}
