<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170524101852 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE site ADD order_success_page_id INT DEFAULT NULL, ADD customer_service_menu_disabled TINYINT(1) DEFAULT NULL, ADD payment_method_logos_hidden TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE site ADD CONSTRAINT FK_694309E4613D032F FOREIGN KEY (order_success_page_id) REFERENCES page (id)');
        $this->addSql('CREATE INDEX IDX_694309E4613D032F ON site (order_success_page_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE site DROP FOREIGN KEY FK_694309E4613D032F');
        $this->addSql('DROP INDEX IDX_694309E4613D032F ON site');
        $this->addSql('ALTER TABLE site DROP order_success_page_id, DROP customer_service_menu_disabled, DROP payment_method_logos_hidden');

    }
}
