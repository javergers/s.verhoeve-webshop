<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161116151423 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE delivery_address_type_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, name VARCHAR(60) DEFAULT NULL, company_label VARCHAR(60) DEFAULT NULL, recipient_label VARCHAR(60) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_A5D443742C2AC5D3 (translatable_id), UNIQUE INDEX delivery_address_type_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE delivery_address_type_field_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, `label` VARCHAR(60) DEFAULT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_6573AE402C2AC5D3 (translatable_id), UNIQUE INDEX delivery_address_type_field_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE delivery_address_type_field (id INT AUTO_INCREMENT NOT NULL, delivery_address_type_id INT NOT NULL, type VARCHAR(30) NOT NULL, required TINYINT(1) NOT NULL, position INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_FA72DC13286F5B26 (delivery_address_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE delivery_address_type (id INT AUTO_INCREMENT NOT NULL, hide_company_field TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE delivery_address_type_site (delivery_address_type_id INT NOT NULL, site_id INT NOT NULL, INDEX IDX_3F493C45286F5B26 (delivery_address_type_id), INDEX IDX_3F493C45F6BD1646 (site_id), PRIMARY KEY(delivery_address_type_id, site_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE delivery_address_type_excluded_assortment (delivery_address_type_id INT NOT NULL, assortment_id INT NOT NULL, INDEX IDX_EC4E6686286F5B26 (delivery_address_type_id), INDEX IDX_EC4E6686D7A15862 (assortment_id), PRIMARY KEY(delivery_address_type_id, assortment_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE delivery_address_type_translation ADD CONSTRAINT FK_A5D443742C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES delivery_address_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE delivery_address_type_field_translation ADD CONSTRAINT FK_6573AE402C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES delivery_address_type_field (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE delivery_address_type_field ADD CONSTRAINT FK_FA72DC13286F5B26 FOREIGN KEY (delivery_address_type_id) REFERENCES delivery_address_type (id)');
        $this->addSql('ALTER TABLE delivery_address_type_site ADD CONSTRAINT FK_3F493C45286F5B26 FOREIGN KEY (delivery_address_type_id) REFERENCES delivery_address_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE delivery_address_type_site ADD CONSTRAINT FK_3F493C45F6BD1646 FOREIGN KEY (site_id) REFERENCES site (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE delivery_address_type_excluded_assortment ADD CONSTRAINT FK_EC4E6686286F5B26 FOREIGN KEY (delivery_address_type_id) REFERENCES delivery_address_type (id)');
        $this->addSql('ALTER TABLE delivery_address_type_excluded_assortment ADD CONSTRAINT FK_EC4E6686D7A15862 FOREIGN KEY (assortment_id) REFERENCES assortment (id)');
        $this->addSql('ALTER TABLE cart_order ADD delivery_address_type_id INT DEFAULT NULL, ADD metadata LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\'');
        $this->addSql('ALTER TABLE cart_order ADD CONSTRAINT FK_AAC3FE90286F5B26 FOREIGN KEY (delivery_address_type_id) REFERENCES delivery_address_type (id)');
        $this->addSql('CREATE INDEX IDX_AAC3FE90286F5B26 ON cart_order (delivery_address_type_id)');
        $this->addSql('ALTER TABLE order_order ADD delivery_address_type_id INT DEFAULT NULL, ADD metadata LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\'');
        $this->addSql('ALTER TABLE order_order ADD CONSTRAINT FK_76B7E765286F5B26 FOREIGN KEY (delivery_address_type_id) REFERENCES delivery_address_type (id)');
        $this->addSql('CREATE INDEX IDX_76B7E765286F5B26 ON order_order (delivery_address_type_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE delivery_address_type_field_translation DROP FOREIGN KEY FK_6573AE402C2AC5D3');
        $this->addSql('ALTER TABLE delivery_address_type_translation DROP FOREIGN KEY FK_A5D443742C2AC5D3');
        $this->addSql('ALTER TABLE delivery_address_type_field DROP FOREIGN KEY FK_FA72DC13286F5B26');
        $this->addSql('ALTER TABLE delivery_address_type_site DROP FOREIGN KEY FK_3F493C45286F5B26');
        $this->addSql('ALTER TABLE delivery_address_type_excluded_assortment DROP FOREIGN KEY FK_EC4E6686286F5B26');
        $this->addSql('ALTER TABLE cart_order DROP FOREIGN KEY FK_AAC3FE90286F5B26');
        $this->addSql('ALTER TABLE order_order DROP FOREIGN KEY FK_76B7E765286F5B26');
        $this->addSql('DROP TABLE delivery_address_type_translation');
        $this->addSql('DROP TABLE delivery_address_type_field_translation');
        $this->addSql('DROP TABLE delivery_address_type_field');
        $this->addSql('DROP TABLE delivery_address_type');
        $this->addSql('DROP TABLE delivery_address_type_site');
        $this->addSql('DROP TABLE delivery_address_type_excluded_assortment');
        $this->addSql('DROP INDEX IDX_AAC3FE90286F5B26 ON cart_order');
        $this->addSql('ALTER TABLE cart_order DROP delivery_address_type_id, DROP metadata');
        $this->addSql('DROP INDEX IDX_76B7E765286F5B26 ON order_order');
        $this->addSql('ALTER TABLE order_order DROP delivery_address_type_id, DROP metadata');
    }
}
