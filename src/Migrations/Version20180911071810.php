<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180911071810 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $qb = $this->connection->createQueryBuilder();
        $qb->select('p.id, p.parent_id')
            ->from('product', 'p')
            ->join('p', 'product_additional_product_assortment', 'papa', 'papa.product_id = p.id')
            ->andWhere('p.parent_id IS NOT NULL');

        $products = $qb->execute()->fetchAll();

        $qb = $this->connection->createQueryBuilder();
        $updateQuery = $qb->update('product_additional_product_assortment')
            ->set('product_id', ':productid')
            ->andWhere('product_id = :childId');

        foreach ($products as $product) {
            $updateQuery->setParameters([
                'productid' => $product['parent_id'],
                'childId' => $product['id'],
            ])->execute();

            $this->write(sprintf('Product %s migrated', $product['parent_id']));
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
