<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Relation\Company;
use AppBundle\Form\Type\YesNoType;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190213082056 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function up(Schema $schema) : void
    {
        $parameterService = $this->container->get('app.parameter');

        $parameterService->createViaArray([
            'key' => 'show_product_stock',
            'entity' => Company::class,
            'label' => 'Voorraad tonen bij product',
            'value' => '',
            'form_type' => YesNoType::class,
            'required' => false,
        ]);
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
