<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190225092626 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_collection_line DROP FOREIGN KEY FK_506A5B44B5B63A6B');
        $this->addSql('ALTER TABLE order_collection_line DROP FOREIGN KEY FK_506A5B44FC2A1E2F');
        $this->addSql('DROP INDEX IDX_506A5B44B5B63A6B ON order_collection_line');
        $this->addSql('ALTER TABLE order_collection_line DROP vat_id');
        $this->addSql('ALTER TABLE order_collection_line ADD CONSTRAINT FK_506A5B44FC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES order_collection (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_collection_line DROP FOREIGN KEY FK_506A5B44FC2A1E2F');
        $this->addSql('ALTER TABLE order_collection_line ADD vat_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_collection_line ADD CONSTRAINT FK_506A5B44B5B63A6B FOREIGN KEY (vat_id) REFERENCES vat (id)');
        $this->addSql('ALTER TABLE order_collection_line ADD CONSTRAINT FK_506A5B44FC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES `order` (id)');
        $this->addSql('CREATE INDEX IDX_506A5B44B5B63A6B ON order_collection_line (vat_id)');
    }
}
