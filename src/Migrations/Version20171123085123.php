<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171123085123 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company_internal_remark (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, company_id INT DEFAULT NULL, remark LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_13DA393A76ED395 (user_id), INDEX IDX_13DA393979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_internal_remark ADD CONSTRAINT FK_13DA393A76ED395 FOREIGN KEY (user_id) REFERENCES adiuvo_user (id)');
        $this->addSql('ALTER TABLE company_internal_remark ADD CONSTRAINT FK_13DA393979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        /** @var QueryBuilder $qb */
        $qb = $this->connection->createQueryBuilder();

        $companies = $qb->select('id, internal_remark')->from('company')->where('internal_remark IS NOT NULL')->execute()->fetchAll();

        $now = date('Y-m-d H:i:s');

        foreach ($companies as $company) {
            $qb->insert('company_internal_remark')->values([
                'company_id' => $company['id'],
                'remark' => $this->connection->quote($company['internal_remark']),
                'created_at' => $this->connection->quote($now),
                'updated_at' => $this->connection->quote($now),
            ])->execute();
        }

        return;
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE company_internal_remark');
    }
}
