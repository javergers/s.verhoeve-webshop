<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170317110819 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE robin_customer (customer_id INT NOT NULL, data LONGTEXT NOT NULL COMMENT \'(DC2Type:json_array)\', synced_at DATETIME NOT NULL, PRIMARY KEY(customer_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE robin_order (order_id INT NOT NULL, data LONGTEXT NOT NULL COMMENT \'(DC2Type:json_array)\', synced_at DATETIME NOT NULL, PRIMARY KEY(order_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE robin_customer ADD CONSTRAINT FK_9A17B06A9395C3F3 FOREIGN KEY (customer_id) REFERENCES `customer` (id)');
        $this->addSql('ALTER TABLE robin_order ADD CONSTRAINT FK_454EFE8E8D9F6D38 FOREIGN KEY (order_id) REFERENCES order_order (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE robin_customer');
        $this->addSql('DROP TABLE robin_order');
    }
}
