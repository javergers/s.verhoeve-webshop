<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171121080228 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE productgroup_property DROP target');
        $this->addSql('ALTER TABLE productgroup_property ADD filterable TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE productgroup_property CHANGE form_type form_type ENUM(\'text\', \'textarea\', \'date\', \'choice\', \'price\', \'entity\', \'assortment\') NOT NULL COMMENT \'(DC2Type:ProductgroupPropertyTypeType)\'');
        $this->addSql('ALTER TABLE productgroup_property ADD `is_unique` TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE productgroup_property ADD on_frontend TINYINT(1) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE productgroup_property DROP on_frontend');
        $this->addSql('ALTER TABLE productgroup_property DROP `is_unique`');
        $this->addSql('ALTER TABLE productgroup_property CHANGE form_type form_type ENUM(\'text\', \'choice\', \'entity\', \'assortment\') NOT NULL COMMENT \'(DC2Type:ProductgroupPropertyTypeType)\'');
        $this->addSql('ALTER TABLE productgroup_property DROP filterable');
        $this->addSql('ALTER TABLE productgroup_property ADD target ENUM(\'product\', \'product_variation\') NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:ProductgroupPropertyTargetType)\'');
    }
}
