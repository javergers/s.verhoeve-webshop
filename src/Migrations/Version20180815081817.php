<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180815081817 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product ADD letter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD4525FF26 FOREIGN KEY (letter_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD4525FF26 ON product (letter_id)');
        $this->addSql('ALTER TABLE productgroup DROP FOREIGN KEY FK_BD0D167F89FC28C1');
        $this->addSql('DROP INDEX IDX_BD0D167F89FC28C1 ON productgroup');
        $this->addSql('ALTER TABLE productgroup DROP letter_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD4525FF26');
        $this->addSql('DROP INDEX IDX_D34A04AD4525FF26 ON product');
        $this->addSql('ALTER TABLE product DROP letter_id');
        $this->addSql('ALTER TABLE productgroup ADD letter_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE productgroup ADD CONSTRAINT FK_BD0D167F89FC28C1 FOREIGN KEY (letter_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_BD0D167F89FC28C1 ON productgroup (letter_id)');
    }
}
