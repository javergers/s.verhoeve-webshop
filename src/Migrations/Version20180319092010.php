<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Designer\DesignerCollage;
use AppBundle\Entity\Designer\DesignerFont;
use AppBundle\Entity\Designer\DesignerTemplate;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\YesNoType;
use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180319092010 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE parameter_entity ADD parent_id INT DEFAULT NULL AFTER `id`');

        $this->addSql('ALTER TABLE parameter_entity ADD CONSTRAINT FK_EA564E993D8E604F FOREIGN KEY (parent_id) REFERENCES parameter_entity (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_EA564E993D8E604F ON parameter_entity (parent_id)');

        $this->addSql('ALTER TABLE parameter_entity CHANGE parameter_id parameter_id VARCHAR(64) DEFAULT NULL');
        $this->addSql('ALTER TABLE parameter CHANGE `key` `key` VARCHAR(64) NOT NULL');

        $this->addSql('ALTER TABLE parameter ADD COLUMN form_type_class VARCHAR(255) DEFAULT NULL AFTER form_type, CHANGE `key` `key` VARCHAR(64) NOT NULL ');

        $this->addSql('CREATE TABLE designer_template_thumb (id INT AUTO_INCREMENT NOT NULL, template_id INT DEFAULT NULL, name VARCHAR(40) NOT NULL, image LONGTEXT NOT NULL, path LONGTEXT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_D572895E5DA0FB8 (template_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');

        $this->addSql('ALTER TABLE designer_template_thumb ADD CONSTRAINT FK_D572895E5DA0FB8 FOREIGN KEY (template_id) REFERENCES designer_template (id)');

        $this->addSql('ALTER TABLE designer_template ADD image_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE designer_template ADD CONSTRAINT FK_81C553D33DA5256D FOREIGN KEY (image_id) REFERENCES designer_template_thumb (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_81C553D33DA5256D ON designer_template (image_id)');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\DBALException
     */
    public function postUp(Schema $schema)
    {
        $columnType = str_replace('\\', '\\\\', ColumnType::class);
        $yesNoType = str_replace('\\', '\\\\', YesNoType::class);
        $entityType = str_replace('\\', '\\\\', EntityType::class);
        $designerCollage = str_replace('\\', '\\\\', DesignerCollage::class);
        $designerFont = str_replace('\\', '\\\\', DesignerFont::class);

        $sql = "
            INSERT INTO `parameter` (`key`, `form_type`, `multiple`, `form_type_class`)
            VALUES
                ('designer_ext_background','" . $columnType . "',0,''),
                ('designer_ext_background_image_enabled','" . $yesNoType . "',0,''),
                ('designer_ext_background_pattern_enabled','" . $yesNoType . "',0,''),
                ('designer_ext_collage','" . $columnType . "',0,''),
                ('designer_ext_collage_enabled','" . $yesNoType . "',0,''),
                ('designer_ext_collage_items','" . $entityType . "',0,'" . $designerCollage . "'),
                ('designer_ext_emoji','" . $columnType . "',0,''),
                ('designer_ext_emoji_enabled','" . $yesNoType . "',0,''),
                ('designer_ext_image','" . $columnType . "',0,''),
                ('designer_ext_image_enabled','" . $yesNoType . "',0,''),
                ('designer_ext_text','" . $columnType . "',0,''),
                ('designer_ext_text_enabled','" . $yesNoType . "',0,''),
                ('designer_ext_text_fonts','" . $entityType . "',0,'" . $designerFont . "')
        ";

        $this->connection->executeQuery($sql);
        $this->write("Add `parameters` into database.");

        $designerTemplateClass = str_replace('\\', '\\\\', DesignerTemplate::class);

        $sql = "
            INSERT INTO `parameter_entity` (`parameter_id`, `entity`, `required`, `label`, `parent_id`)
            VALUES
                ('designer_ext_background','" . $designerTemplateClass . "',1,'Ext. achtergrond',NULL),
                ('designer_ext_background_image_enabled','" . $designerTemplateClass . "',1,'Afbeelding actief',NULL),
                ('designer_ext_background_pattern_enabled','" . $designerTemplateClass . "',1,'Patronen actief',NULL),
                ('designer_ext_collage','" . $designerTemplateClass . "',1,'Ext. Collage',NULL),
                ('designer_ext_collage_enabled','" . $designerTemplateClass . "',1,'Actief',NULL),
                ('designer_ext_collage_items','" . $designerTemplateClass . "',0,'Collages',NULL),
                ('designer_ext_emoji','" . $designerTemplateClass . "',1,'Ext. Emoji',NULL),
                ('designer_ext_emoji_enabled','" . $designerTemplateClass . "',1,'Actief',NULL),
                ('designer_ext_image','" . $designerTemplateClass . "',1,'Ext. Image',NULL),
                ('designer_ext_image_enabled','" . $designerTemplateClass . "',1,'Actief',NULL),
                ('designer_ext_text','" . $designerTemplateClass . "',1,'Ext. Text',NULL),
                ('designer_ext_text_enabled','" . $designerTemplateClass . "',1,'Actief',NULL),
                ('designer_ext_text_fonts','" . $designerTemplateClass . "',1,'Lettertypen',NULL)
        ";

        $this->connection->executeQuery($sql);
        $this->write("Add `parameters entities` into database.");

        $sql = "
            UPDATE `parameter_entity` AS `pe` SET `pe`.`parent_id` = (SELECT `d`.`id` FROM (SELECT * FROM `parameter_entity` AS `e` WHERE `e`.`parameter_id` = 'designer_ext_background' LIMIT 1) AS `d`)  WHERE `pe`.`parameter_id` LIKE 'designer_ext_background_%';            
            UPDATE `parameter_entity` AS `pe` SET `pe`.`parent_id` = (SELECT `d`.`id` FROM (SELECT * FROM `parameter_entity` AS `e` WHERE `e`.`parameter_id` = 'designer_ext_collage' LIMIT 1) AS `d`)  WHERE `pe`.`parameter_id` LIKE 'designer_ext_collage_%';            
            UPDATE `parameter_entity` AS `pe` SET `pe`.`parent_id` = (SELECT `d`.`id` FROM (SELECT * FROM `parameter_entity` AS `e` WHERE `e`.`parameter_id` = 'designer_ext_emoji' LIMIT 1) AS `d`)  WHERE `pe`.`parameter_id` LIKE 'designer_ext_emoji_%';            
            UPDATE `parameter_entity` AS `pe` SET `pe`.`parent_id` = (SELECT `d`.`id` FROM (SELECT * FROM `parameter_entity` AS `e` WHERE `e`.`parameter_id` = 'designer_ext_image' LIMIT 1) AS `d`)  WHERE `pe`.`parameter_id` LIKE 'designer_ext_image_%';            
            UPDATE `parameter_entity` AS `pe` SET `pe`.`parent_id` = (SELECT `d`.`id` FROM (SELECT * FROM `parameter_entity` AS `e` WHERE `e`.`parameter_id` = 'designer_ext_text' LIMIT 1) AS `d`)  WHERE `pe`.`parameter_id` LIKE 'designer_ext_text_%';
        ";
        $this->connection->executeQuery($sql);
        $this->write("Add `parent to parameter entity` into database.");
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\DBALException
     */
    public function preDown(Schema $schema)
    {
        $this->connection->executeQuery("UPDATE parameter_entity_value SET `parameter_entity_id` = NULL WHERE parameter_entity_id IN (SELECT id FROM parameter_entity WHERE parameter_id LIKE 'designer_%')");

        $this->connection->executeQuery("DELETE FROM parameter_entity_value WHERE `parameter_entity_id` IN (SELECT id FROM parameter_entity WHERE parameter_id LIKE 'designer_%')");
        $this->write("Remove `designer parameters entity values` from database.");

        $this->connection->executeQuery("DELETE FROM parameter_entity WHERE `parameter_id` LIKE 'designer_%'");
        $this->write("Remove `designer parameters entities` from database.");

        $this->connection->executeQuery("DELETE FROM parameter WHERE `key` LIKE 'designer_%'");
        $this->write("Remove `designer parameters` from database.");
    }


    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE designer_template DROP FOREIGN KEY FK_81C553D33DA5256D');
        $this->addSql('ALTER TABLE parameter_entity DROP FOREIGN KEY FK_EA564E993D8E604F');

        $this->addSql('DROP INDEX IDX_EA564E993D8E604F ON parameter_entity');
        $this->addSql('ALTER TABLE parameter_entity DROP parent_id');
        $this->addSql('ALTER TABLE parameter DROP form_type_class');

        $this->addSql('ALTER TABLE parameter_entity DROP FOREIGN KEY FK_EA564E997C56DBD6');
        $this->addSql('ALTER TABLE parameter CHANGE `key` `key` VARCHAR(32) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE parameter_entity CHANGE parameter_id parameter_id VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE parameter_entity ADD CONSTRAINT FK_EA564E997C56DBD6 FOREIGN KEY (parameter_id) REFERENCES parameter (`key`)');

        $this->addSql('DROP INDEX UNIQ_81C553D33DA5256D ON designer_template');
        $this->addSql('DROP TABLE designer_template_thumb');
        $this->addSql('ALTER TABLE designer_template DROP image_id');

    }
}
