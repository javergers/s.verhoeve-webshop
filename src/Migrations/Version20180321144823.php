<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180321144823 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE designer_tag (id INT AUTO_INCREMENT NOT NULL, name LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE designer_template_tag (designer_template_id INT NOT NULL, tag_id INT NOT NULL, INDEX IDX_38CBB85A93167670 (designer_template_id), INDEX IDX_38CBB85ABAD26311 (tag_id), PRIMARY KEY(designer_template_id, tag_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE designer_template_tag ADD CONSTRAINT FK_38CBB85A93167670 FOREIGN KEY (designer_template_id) REFERENCES designer_template (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE designer_template_tag ADD CONSTRAINT FK_38CBB85ABAD26311 FOREIGN KEY (tag_id) REFERENCES designer_tag (id) ON DELETE CASCADE');
        $this->addSql('CREATE TABLE tag_designer_template (tag_id INT NOT NULL, designer_template_id INT NOT NULL, INDEX IDX_3A9A4BA7BAD26311 (tag_id), INDEX IDX_3A9A4BA793167670 (designer_template_id), PRIMARY KEY(tag_id, designer_template_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tag_designer_template ADD CONSTRAINT FK_3A9A4BA7BAD26311 FOREIGN KEY (tag_id) REFERENCES designer_tag (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tag_designer_template ADD CONSTRAINT FK_3A9A4BA793167670 FOREIGN KEY (designer_template_id) REFERENCES designer_template (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE designer_template DROP FOREIGN KEY FK_81C553D33DA5256D');
        $this->addSql('DROP INDEX UNIQ_81C553D33DA5256D ON designer_template');
        $this->addSql('ALTER TABLE designer_template ADD image LONGTEXT DEFAULT NULL, DROP image_id');
        $this->addSql('ALTER TABLE designer_template ADD image_path LONGTEXT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE designer_template ADD image_id INT DEFAULT NULL, DROP image');
        $this->addSql('ALTER TABLE designer_template ADD CONSTRAINT FK_81C553D33DA5256D FOREIGN KEY (image_id) REFERENCES designer_template_thumb (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_81C553D33DA5256D ON designer_template (image_id)');
        $this->addSql('ALTER TABLE designer_template DROP image_path');
        $this->addSql('DROP TABLE designer_tag');
        $this->addSql('DROP TABLE designer_template_tag');
        $this->addSql('DROP TABLE tag_designer_template');
    }
}
