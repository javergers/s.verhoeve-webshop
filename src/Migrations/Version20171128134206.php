<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171128134206 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
    }

    /**
     * @param Schema $schema
     * @throws DBALException
     */
    public function postUp(Schema $schema)
    {
        /** @var Connection $conn */
        $conn = $this->connection;
        $qb = $conn->createQueryBuilder();

        $products = $qb->select('*')
            ->from('product')
            ->where('type = :type')
            ->andWhere('parent_id IS NULL')
            ->setParameters([
                'type' => 'simple',
            ])
            ->execute()
            ->fetchAll();

        $variationSkuUpdateQuery = $conn->createQueryBuilder()->update('product', 'p')
            ->set('p.type', ':type')
            ->set('p.parent_id', ':parent_id')
            ->where('p.id = :id');

        $translationUpdateQuery = $conn->createQueryBuilder()
            ->update('product_translation')
            ->set('slug', 'NULL')
            ->where('translatable_id = :product');

        foreach ($products as $product) {
            $conn->beginTransaction();

            $parent = $product;

            unset($parent['id'], $parent['sku']);
            $parent['type'] = 'variable';

            $parent = array_filter($parent, function ($f) {
                return null !== $f;
            });

            $parent = array_map(function ($value) {
                return $this->connection->quote($value);
            }, $parent);

            $qb->insert('product')->values($parent)->execute();
            $parentId = $conn->lastInsertId();

            //set parentId on variation
            //first remove sku from variation
            $variationSkuUpdateQuery->setParameters([
                'type' => null,
                'parent_id' => $parentId,
                'id' => (int)$product['id'],
            ])->execute();

            $qb = $conn->createQueryBuilder();

            //set translation
            $translations = $qb->select('*')
                ->from('product_translation')
                ->where('translatable_id = :transid')
                ->setParameter('transid', $product['id'])
                ->execute()
                ->fetchAll();

            foreach ($translations as $key => $translation) {
                $translation = array_filter($translation, function ($f) {
                    return null !== $f;
                });

                $translation = array_map(function ($value) {
                    return $this->connection->quote($value);
                }, $translation);

                $translation['translatable_id'] = $parentId;

                unset($translation['id']);

                $translationUpdateQuery->setParameter('product', $product['id'])->execute();
                $qb->insert('product_translation')->values($translation)->execute();
            }

            $conn->commit();
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
