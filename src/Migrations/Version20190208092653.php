<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190208092653 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE timeslot (id INT AUTO_INCREMENT NOT NULL, `from` TIME NOT NULL, till TIME NOT NULL, rrule LONGTEXT NOT NULL, exclude_company_closing_dates TINYINT(1) NOT NULL, exclude_holidays TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pickup_location_timeslot (pickup_location_id INT NOT NULL, timeslot_id INT NOT NULL, INDEX IDX_3A0D1D9C77EA60D (pickup_location_id), INDEX IDX_3A0D1D9F920B9E9 (timeslot_id), PRIMARY KEY(pickup_location_id, timeslot_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE pickup_location_timeslot ADD CONSTRAINT FK_3A0D1D9C77EA60D FOREIGN KEY (pickup_location_id) REFERENCES pickup_location (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pickup_location_timeslot ADD CONSTRAINT FK_3A0D1D9F920B9E9 FOREIGN KEY (timeslot_id) REFERENCES timeslot (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE pickup_location ADD preparation_time INT DEFAULT 1 NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pickup_location_timeslot DROP FOREIGN KEY FK_3A0D1D9F920B9E9');
        $this->addSql('DROP TABLE timeslot');
        $this->addSql('DROP TABLE pickup_location_timeslot');
        $this->addSql('ALTER TABLE pickup_location DROP preparation_time');
    }
}
