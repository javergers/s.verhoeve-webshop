<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190405074348 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_collection_line ADD related_order_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE order_collection_line ADD CONSTRAINT FK_506A5B442B1C2395 FOREIGN KEY (related_order_id) REFERENCES `order` (id)');
        $this->addSql('CREATE INDEX IDX_506A5B442B1C2395 ON order_collection_line (related_order_id)');
        $this->addSql('ALTER TABLE order_collection_line CHANGE discount_amount discount_amount DOUBLE PRECISION DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_collection_line DROP FOREIGN KEY FK_506A5B442B1C2395');
        $this->addSql('DROP INDEX IDX_506A5B442B1C2395 ON order_collection_line');
        $this->addSql('ALTER TABLE order_collection_line DROP related_order_id');
        $this->addSql('ALTER TABLE order_collection_line CHANGE discount_amount discount_amount DOUBLE PRECISION NOT NULL');
    }
}
