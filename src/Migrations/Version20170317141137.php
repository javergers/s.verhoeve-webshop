<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170317141137 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE customer DROP robin_hash, DROP robin_synced_at');
        $this->addSql('ALTER TABLE order_order DROP robin_hash, DROP robin_synced_at');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `customer` ADD robin_hash VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci, ADD robin_synced_at DATETIME DEFAULT NULL');
        $this->addSql('ALTER TABLE order_order ADD robin_hash VARCHAR(32) DEFAULT NULL COLLATE utf8_unicode_ci, ADD robin_synced_at DATETIME DEFAULT NULL');
    }
}
