<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170724125000 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        return;
        if ($this->container->get('environment')->is(['prod', 'staging'])) {
            $this->connection->beginTransaction();

            /** @var QueryBuilder $qb */
            $qb = $this->container->get("doctrine")->getRepository(Customer::class)->createQueryBuilder('c');

            $customerGroup = $this->container->get('doctrine')->getRepository(CustomerGroup::class)->find(3);

            $customers = $qb
                ->leftJoin('c.company', 'com')
                ->where('c.company IS NOT NULL')
                ->andWhere('c.username IS NULL')
                ->andWhere('c.email IS NOT NULL')
                ->andWhere('c.deletedAt IS NULL')
                ->andWhere('com.deletedAt IS NULL')
                ->getQuery()
                ->getResult();

            $customers = new ArrayCollection($customers);

            $customers = $customers->filter(function (Customer $customer) use ($customerGroup) {
                try {
                    if ($customer->getCompany() && $customer->getCompany()->getCustomerGroups()->exists(function (
                            $key,
                            CustomerGroup $cg
                        ) use ($customerGroup) {
                            return $cg == $customerGroup;
                        })
                    ) {
                        return false;
                    }
                } catch (EntityNotFoundException $e) {
                    return false;
                }

                return true;
            });

            /** @var QueryBuilder $updater */
            $updater = $this->container->get("doctrine")->getManager()
                ->createQueryBuilder()
                ->update(Customer::class, 'customer')
                ->set('customer.username', ":username")
                ->set('customer.usernameCanonical', ":username")
                ->set('customer.updatedAt', "CURRENT_TIME()")
                ->andWhere("customer.id = :id");

            $sql = $updater->getQuery()->getSQL();
            $stmt = $this->container->get('doctrine')->getConnection()->prepare($sql);

            /** @var Customer $customer */
            foreach ($customers as $customer) {
                $domain = substr($customer->getCompany()->getEmail(), strpos($customer->getCompany()->getEmail(), '@'));
                $username = implode("", ["b", $customer->getId(), $domain]);

                $stmt->execute([
                    $username,
                    $username,
                    $customer->getId(),
                ]);
            }

            $this->connection->commit();
        }
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');
    }
}
