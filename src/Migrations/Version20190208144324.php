<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190208144324 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company_transport_type (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, product_group_id INT DEFAULT NULL, transport_type_id INT DEFAULT NULL, price DOUBLE PRECISION NOT NULL, INDEX IDX_7FC62F0F979B1AD6 (company_id), INDEX IDX_7FC62F0F35E4B3D0 (product_group_id), INDEX IDX_7FC62F0F519B4C62 (transport_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_transport_type ADD CONSTRAINT FK_7FC62F0F979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE company_transport_type ADD CONSTRAINT FK_7FC62F0F35E4B3D0 FOREIGN KEY (product_group_id) REFERENCES productgroup (id)');
        $this->addSql('ALTER TABLE company_transport_type ADD CONSTRAINT FK_7FC62F0F519B4C62 FOREIGN KEY (transport_type_id) REFERENCES product (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE company_transport_type');
    }
}
