<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190624150028 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE exact_invoice_export (id INT AUTO_INCREMENT NOT NULL, created_by_id INT NOT NULL, delivery_date DATE NOT NULL, invoice_date DATE NOT NULL, export_status ENUM(\'new\', \'in_progress\', \'exported\', \'failed\') DEFAULT \'new\' NOT NULL COMMENT \'(DC2Type:ExactInvoiceExportStatusType)\', processed_at DATE DEFAULT NULL, companies_to_process JSON DEFAULT NULL COMMENT \'(DC2Type:json_array)\', created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_AB955F36B03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE exact_invoice (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, invoice_export_id INT NOT NULL, invoice_date DATE NOT NULL, external_ref VARCHAR(50) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_B92495C7979B1AD6 (company_id), INDEX IDX_B92495C71625E1E (invoice_export_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE exact_invoice_export ADD CONSTRAINT FK_AB955F36B03A8386 FOREIGN KEY (created_by_id) REFERENCES adiuvo_user (id)');
        $this->addSql('ALTER TABLE exact_invoice ADD CONSTRAINT FK_B92495C7979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE exact_invoice ADD CONSTRAINT FK_B92495C71625E1E FOREIGN KEY (invoice_export_id) REFERENCES exact_invoice_export (id)');
        $this->addSql('ALTER TABLE csv_order CHANGE status status ENUM(\'registered\', \'error\', \'archive\')');
        $this->addSql('ALTER TABLE billing_item_group ADD invoice_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE billing_item_group ADD CONSTRAINT FK_CB0030842989F1FD FOREIGN KEY (invoice_id) REFERENCES exact_invoice (id)');
        $this->addSql('CREATE INDEX IDX_CB0030842989F1FD ON billing_item_group (invoice_id)');
        $this->addSql('ALTER TABLE company_custom_field ADD group_invoices TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE company_custom_order_field_value DROP INDEX UNIQ_F6CB3727EE14DAA9, ADD INDEX IDX_F6CB3727EE14DAA9 (company_custom_field_id)');
        $this->addSql('ALTER TABLE company_custom_order_field_value DROP FOREIGN KEY FK_F6CB37279395C3F3');
        $this->addSql('DROP INDEX IDX_F6CB37279395C3F3 ON company_custom_order_field_value');
        $this->addSql('ALTER TABLE company_custom_order_field_value DROP customer_id');
        $this->addSql('ALTER TABLE discount CHANGE type type enum(\'amount\', \'percentage\')');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE exact_invoice DROP FOREIGN KEY FK_B92495C71625E1E');
        $this->addSql('ALTER TABLE billing_item_group DROP FOREIGN KEY FK_CB0030842989F1FD');
        $this->addSql('DROP TABLE exact_invoice_export');
        $this->addSql('DROP TABLE exact_invoice');
        $this->addSql('DROP INDEX IDX_CB0030842989F1FD ON billing_item_group');
        $this->addSql('ALTER TABLE billing_item_group DROP invoice_id');
        $this->addSql('ALTER TABLE company_custom_field DROP group_invoices');
        $this->addSql('ALTER TABLE company_custom_order_field_value DROP INDEX IDX_F6CB3727EE14DAA9, ADD UNIQUE INDEX UNIQ_F6CB3727EE14DAA9 (company_custom_field_id)');
        $this->addSql('ALTER TABLE company_custom_order_field_value ADD customer_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE company_custom_order_field_value ADD CONSTRAINT FK_F6CB37279395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id)');
        $this->addSql('CREATE INDEX IDX_F6CB37279395C3F3 ON company_custom_order_field_value (customer_id)');
        $this->addSql('ALTER TABLE csv_order CHANGE status status VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE discount CHANGE type type VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
