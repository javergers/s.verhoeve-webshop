<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181220092237 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE supplier_order_line DROP FOREIGN KEY FK_FD144308B5B63A6B');
        $this->addSql('DROP INDEX IDX_FD144308B5B63A6B ON supplier_order_line');
        $this->addSql('ALTER TABLE supplier_order_line CHANGE vat_id vat_rate_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE supplier_order_line ADD CONSTRAINT FK_FD14430843897540 FOREIGN KEY (vat_rate_id) REFERENCES vat_rate (id)');
        $this->addSql('CREATE INDEX IDX_FD14430843897540 ON supplier_order_line (vat_rate_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE supplier_order_line DROP FOREIGN KEY FK_FD14430843897540');
        $this->addSql('DROP INDEX IDX_FD14430843897540 ON supplier_order_line');
        $this->addSql('ALTER TABLE supplier_order_line CHANGE vat_rate_id vat_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE supplier_order_line ADD CONSTRAINT FK_FD144308B5B63A6B FOREIGN KEY (vat_id) REFERENCES vat (id)');
        $this->addSql('CREATE INDEX IDX_FD144308B5B63A6B ON supplier_order_line (vat_id)');
    }
}
