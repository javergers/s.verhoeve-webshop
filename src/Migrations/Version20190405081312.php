<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190405081312 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE company_invoice_settings (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, invoice_frequency ENUM(\'collective\', \'order\', \'order_collection\', \'weekly\', \'biweekly\', \'monthly\', \'quarterly\') DEFAULT \'collective\' NOT NULL COMMENT \'(DC2Type:company_invoice_frequency_type)\', payment_term INT DEFAULT 14 NOT NULL, invoice_discount ENUM(\'separate\', \'applied\') DEFAULT \'separate\' NOT NULL COMMENT \'(DC2Type:company_invoice_discount_type)\', invoice_incl_personal_data ENUM(\'none\', \'customer\', \'recipient\', \'all\') DEFAULT \'none\' NOT NULL COMMENT \'(DC2Type:company_invoice_incl_personal_data_type)\', invoice_zero_value TINYINT(1) NOT NULL, invoice_type ENUM(\'pdf_mail\', \'ubl_mail\', \'ubl_gov_nl\') DEFAULT \'pdf_mail\' NOT NULL COMMENT \'(DC2Type:company_invoice_type_type)\', UNIQUE INDEX UNIQ_D4960626979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE company_invoice_settings ADD CONSTRAINT FK_D4960626979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE company ADD company_invoice_settings_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE company ADD CONSTRAINT FK_4FBF094F15B76E41 FOREIGN KEY (company_invoice_settings_id) REFERENCES company_invoice_settings (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4FBF094F15B76E41 ON company (company_invoice_settings_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company DROP FOREIGN KEY FK_4FBF094F15B76E41');
        $this->addSql('DROP TABLE company_invoice_settings');
        $this->addSql('ALTER TABLE company DROP company_invoice_settings_id');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function postUp(Schema $schema)
    {
        $conn = $this->connection;

        $conn->beginTransaction();

        $stmt = $conn->prepare("SELECT `id` FROM `company`");
        $stmt->execute();
        $companies = $stmt->fetchAll();

        $count = 1;

        foreach($companies as $company){

            $stmt1 = $conn->prepare("
              INSERT INTO `company_invoice_settings`
              (`id`, `invoice_frequency`, `payment_term`, `invoice_discount`, `invoice_incl_personal_data`, `invoice_zero_value`, `invoice_type`)
              VALUES ($count, 'collective', 14, 'separate', 'none', 1, 'pdf_mail')");
            $stmt1->execute();

            $stmt2 = $conn->prepare("UPDATE `company` SET `company_invoice_settings_id` = $count WHERE `id` = $count");
            $stmt2->execute();

            $count++;
        }

        $conn->commit();
    }
}
