<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170501092436 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE supplier_delivery_area (id INT AUTO_INCREMENT NOT NULL, postcode_id INT DEFAULT NULL, company_id INT DEFAULT NULL, delivery_price DOUBLE PRECISION DEFAULT NULL, delivery_interval VARCHAR(20) NOT NULL, INDEX IDX_14D57535EECBFDF1 (postcode_id), INDEX IDX_14D57535979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE supplier_delivery_area ADD CONSTRAINT FK_14D57535EECBFDF1 FOREIGN KEY (postcode_id) REFERENCES postcode (id)');
        $this->addSql('ALTER TABLE supplier_delivery_area ADD CONSTRAINT FK_14D57535979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE supplier_delivery_area');
    }
}
