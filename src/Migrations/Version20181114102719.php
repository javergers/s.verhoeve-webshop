<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbortMigrationException;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20181114102719
 * @package DoctrineMigrations
 */
class Version20181114102719 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE data_set_filter_field_option DROP FOREIGN KEY FK_C3501CE64B699E11');
        $this->addSql('ALTER TABLE data_set_filter_field_option ADD CONSTRAINT FK_C3501CE64B699E11 FOREIGN KEY (data_set_filter_field_id) REFERENCES data_set_filter_field (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     * @throws AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE data_set_filter_field_option DROP FOREIGN KEY FK_C3501CE64B699E11');
        $this->addSql('ALTER TABLE data_set_filter_field_option ADD CONSTRAINT FK_C3501CE64B699E11 FOREIGN KEY (data_set_filter_field_id) REFERENCES data_set_filter_field (id)');
    }
}
