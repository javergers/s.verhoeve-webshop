<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180416133250 extends AbstractMigration
{
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE csv_order (id INT AUTO_INCREMENT NOT NULL, company_id INT DEFAULT NULL, file_name VARCHAR(64) NOT NULL, status ENUM(\'registered\', \'error\', \'archive\'), created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_E053040A979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE csv_order_cart (csv_order_id INT NOT NULL, cart_id INT NOT NULL, INDEX IDX_A3D0145FFDD8854C (csv_order_id), INDEX IDX_A3D0145F1AD5CDBF (cart_id), PRIMARY KEY(csv_order_id, cart_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE csv_order ADD CONSTRAINT FK_E053040A979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE csv_order_cart ADD CONSTRAINT FK_A3D0145FFDD8854C FOREIGN KEY (csv_order_id) REFERENCES csv_order (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE csv_order_cart ADD CONSTRAINT FK_A3D0145F1AD5CDBF FOREIGN KEY (cart_id) REFERENCES cart (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE csv_order_cart DROP FOREIGN KEY FK_A3D0145FFDD8854C');
        $this->addSql('DROP TABLE csv_order');
        $this->addSql('DROP TABLE csv_order_cart');
    }
}
