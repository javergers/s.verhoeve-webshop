<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180213115019 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE report_column (id INT AUTO_INCREMENT NOT NULL, report_id INT DEFAULT NULL, entity_relation_id INT DEFAULT NULL, `order` INT NOT NULL, INDEX IDX_54A052E14BD2A4C0 (report_id), INDEX IDX_54A052E1B06CCB88 (entity_relation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE report (id INT AUTO_INCREMENT NOT NULL, rule_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_C42F7784744E0351 (rule_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE report_column ADD CONSTRAINT FK_54A052E14BD2A4C0 FOREIGN KEY (report_id) REFERENCES report (id)');
        $this->addSql('ALTER TABLE report_column ADD CONSTRAINT FK_54A052E1B06CCB88 FOREIGN KEY (entity_relation_id) REFERENCES entity_relation (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F7784744E0351 FOREIGN KEY (rule_id) REFERENCES rule (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE report_column DROP FOREIGN KEY FK_54A052E14BD2A4C0');
        $this->addSql('DROP TABLE report_column');
        $this->addSql('DROP TABLE report');
    }
}
