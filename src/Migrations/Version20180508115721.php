<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180508115721 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company_connector_parameter DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE company_connector_parameter ADD id INT NOT NULL FIRST');
        $this->addSql('ALTER TABLE company_connector_parameter ADD PRIMARY KEY (id), MODIFY COLUMN id INT auto_increment');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company_connector_parameter MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE company_connector_parameter DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE company_connector_parameter DROP id');
        $this->addSql('ALTER TABLE company_connector_parameter ADD PRIMARY KEY (company_id, connector, name)');
    }
}
