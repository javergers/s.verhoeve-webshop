<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181204141538 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE vat_rule (id INT AUTO_INCREMENT NOT NULL, rule_id INT DEFAULT NULL, position INT NOT NULL, allow_split TINYINT(1) NOT NULL, INDEX IDX_6FB06432744E0351 (rule_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_line_vat (id INT AUTO_INCREMENT NOT NULL, order_line_id INT DEFAULT NULL, vat_rate_id INT DEFAULT NULL, amount DOUBLE PRECISION NOT NULL, INDEX IDX_9600F3D8BB01DC09 (order_line_id), INDEX IDX_9600F3D843897540 (vat_rate_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE vat_rule ADD CONSTRAINT FK_6FB06432744E0351 FOREIGN KEY (rule_id) REFERENCES rule (id)');
        $this->addSql('ALTER TABLE order_line_vat ADD CONSTRAINT FK_9600F3D8BB01DC09 FOREIGN KEY (order_line_id) REFERENCES order_line (id)');
        $this->addSql('ALTER TABLE order_line_vat ADD CONSTRAINT FK_9600F3D843897540 FOREIGN KEY (vat_rate_id) REFERENCES vat_rate (id)');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function postUp(Schema $schema)
    {
        $conn = $this->connection;

        $conn->beginTransaction();

        $stmt = $conn->prepare('
            INSERT INTO order_line_vat (order_line_id, vat_rate_id, amount)
            SELECT order_line.id, IFNULL(IFNULL(order_line.vat_id, product.vat_id), 1), IFNULL(order_line.price * order_line.quantity, 0.00) AS amount
            FROM `order_line`
            INNER JOIN `product` ON `order_line`.`product_id` = `product`.`id`
        ');

        $stmt->execute();

        $conn->commit();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE vat_rule');
        $this->addSql('DROP TABLE order_line_vat');
    }
}
