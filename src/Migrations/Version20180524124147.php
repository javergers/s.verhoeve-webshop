<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180524124147 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;
    
    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE wms_order_type (id INT AUTO_INCREMENT NOT NULL, rule_id INT DEFAULT NULL, code VARCHAR(8) DEFAULT NULL, priority INT DEFAULT NULL, INDEX IDX_8DF1A5CC744E0351 (rule_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE wms_order_type ADD CONSTRAINT FK_8DF1A5CC744E0351 FOREIGN KEY (rule_id) REFERENCES rule (id)');
    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\DBAL\Migrations\AbortMigrationException
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE wms_order_type');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameter = [
            'key' => 'wms_order_type_fallback',
            'entity' => null,
            'label' => 'WMS order type fallback',
            'value' => 'STDRF',
        ];

        $parameterService->createViaArray($parameter);
    }

    /**
     * @param Schema $schema
     */
    public function postDown(Schema $schema)
    {
        $parameterService = $this->container->get('app.parameter');

        $parameterService->deleteByKey('wms_order_type_fallback');
    }
}
