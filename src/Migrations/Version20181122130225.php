<?php

namespace DoctrineMigrations;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Vat;
use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Entity\Finance\Tax\VatLevel;
use AppBundle\Entity\Finance\Tax\VatRate;
use AppBundle\Entity\Geography\Country;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181122130225 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function postUp(Schema $schema)
    {
        /** @var EntityManagerInterface $em */
        $em = $this->container->get('doctrine.orm.default_entity_manager');

        $vats = $em->getRepository(Vat::class)->findAll();

        $country = $em->getRepository(Country::class)->find('NL');
        foreach($vats as $vat) {
            $factor = $vat->getPercentage() / 100;

            $vatLevel = new VatLevel();
            $vatLevel->setTitle($vat->getDescription());
            $vatLevel->setSpecial(false);

            $vatGroup = new VatGroup();
            $vatGroup->setCountry($country);
            $vatGroup->setLevel($vatLevel);

            $vatRate = new VatRate();
            $vatRate->setFactor($factor);
            $vatRate->setValidFrom(new \DateTime());
            $vatRate->setValidUntil(new \DateTime('31-12-2018 23:59:59'));
            $vatRate->setPurchase(false);
            $vatRate->setTitle($vat->getDescription());
            $vatRate->setGroup($vatGroup);
            $vatRate->setCode('1');

            $em->persist($vatGroup);
            $em->persist($vatRate);
        }

        $em->flush();

        $stmt = $this->connection->prepare('INSERT INTO product_vat_group (product_id, vat_group_id) SELECT id, vat_id FROM product WHERE vat_id IS NOT NULL');
        $stmt->execute();
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
