<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181023083555 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE billing_item (id INT AUTO_INCREMENT NOT NULL, order_id INT DEFAULT NULL, order_collection_line_id INT DEFAULT NULL, billing_item_group_id INT NOT NULL, transaction_type ENUM(\'debit\', \'credit\') NOT NULL COMMENT \'(DC2Type:BillingItemTransactionType)\', INDEX IDX_60691BD98D9F6D38 (order_id), INDEX IDX_60691BD9E5FAECA5 (order_collection_line_id), INDEX IDX_60691BD95F3B4054 (billing_item_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE billing_item_group (id INT AUTO_INCREMENT NOT NULL, order_collection_id INT DEFAULT NULL, sequence INT NOT NULL, INDEX IDX_CB003084FC2A1E2F (order_collection_id), UNIQUE INDEX colection_sequence_unique (order_collection_id, sequence), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE billing_item ADD CONSTRAINT FK_60691BD98D9F6D38 FOREIGN KEY (order_id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE billing_item ADD CONSTRAINT FK_60691BD9E5FAECA5 FOREIGN KEY (order_collection_line_id) REFERENCES order_collection_line (id)');
        $this->addSql('ALTER TABLE billing_item ADD CONSTRAINT FK_60691BD95F3B4054 FOREIGN KEY (billing_item_group_id) REFERENCES billing_item_group (id)');
        $this->addSql('ALTER TABLE billing_item_group ADD CONSTRAINT FK_CB003084FC2A1E2F FOREIGN KEY (order_collection_id) REFERENCES order_collection (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE billing_item DROP FOREIGN KEY FK_60691BD95F3B4054');
        $this->addSql('DROP TABLE billing_item');
        $this->addSql('DROP TABLE billing_item_group');
    }
}
