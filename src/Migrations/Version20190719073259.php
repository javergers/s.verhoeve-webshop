<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190719073259 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company_invoice_settings ADD invoice_reminder_email VARCHAR(200) DEFAULT NULL, CHANGE invoice_incl_personal_data invoice_incl_personal_data ENUM(\'none\', \'customer\', \'recipient\', \'all\') DEFAULT \'none\' NOT NULL COMMENT \'(DC2Type:company_invoice_personal_data)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE company_invoice_settings DROP invoice_reminder_email, CHANGE invoice_incl_personal_data invoice_incl_personal_data ENUM(\'none\', \'customer\', \'recipient\', \'all\') DEFAULT \'none\' NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:company_invoice_personal_data)\'');
    }
}
