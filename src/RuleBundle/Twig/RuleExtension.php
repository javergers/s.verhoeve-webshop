<?php

namespace RuleBundle\Twig;

use RuleBundle\Service\RuleTransformerService;

/**
 * Class RuleExtension
 * @package RuleBundle\Twig
 */
class RuleExtension extends \Twig_Extension
{
    /** @var RuleTransformerService */
    private $ruleTransformer;

    /**
     * @return array|\Twig_SimpleFilter[]
     */
    public function getFilters()
    {
        $options = [
            'is_safe' => ['html'],
        ];

        return [
            new \Twig_SimpleFilter('rule_filters', [$this, 'generateRuleFilters'], $options),
        ];
    }

    /**
     * RuleExtension constructor.
     * @param RuleTransformerService $ruleTransformer
     */
    public function __construct(RuleTransformerService $ruleTransformer)
    {
        $this->ruleTransformer = $ruleTransformer;
    }

    /**
     * @param array $filters
     * @return array
     */
    public function generateRuleFilters(array $filters)
    {
        return $this->ruleTransformer->transformFilter($filters);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rule_filters_extension';
    }
}