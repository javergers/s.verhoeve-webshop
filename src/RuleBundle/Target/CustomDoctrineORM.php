<?php

namespace RuleBundle\Target;

use Doctrine\ORM\QueryBuilder;

use RulerZ\Compiler\Context;
use RulerZ\Target\AbstractSqlTarget;
use RulerZ\Target\DoctrineORM\DoctrineORM;
use RulerZ\Target\DoctrineORM\DoctrineORMVisitor;

/**
 * Class CustomDoctrineORM
 * @package RuleBundle\Target
 */
class CustomDoctrineORM extends DoctrineORM
{
    /**
     * {@inheritdoc}
     */
    public function getExecutorTraits()
    {
        return [
            '\RuleBundle\Target\CustomFilterTrait',
            '\RulerZ\Executor\Polyfill\FilterBasedSatisfaction',
        ];
    }
}
