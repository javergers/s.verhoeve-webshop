<?php

namespace RuleBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class RuleBundle
 * @package RuleBundle
 */
class RuleBundle extends Bundle
{
}
