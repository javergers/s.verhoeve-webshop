<?php

namespace RuleBundle\Command;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use RuleBundle\Annotation as Rule;
use RuleBundle\Entity\Entity;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class EntitiesCommand
 * @package RuleBundle\Command
 */
class EntitiesCommand extends ContainerAwareCommand
{
    /**
     * @var EntityManagerInterface $em
     */
    protected $em;

    /**
     * @var AnnotationReader $annotationReader
     */
    protected $annotationReader;

    protected function configure()
    {
        $this->setName('topgeschenken:entities:generate');
    }

    /**
     * EntitiesCommand constructor.
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function __construct()
    {
        $this->annotationReader = new AnnotationReader();

        parent::__construct();
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $entities = $this->em->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();

        $processedClasses = [];
        $newEntities = new ArrayCollection();

        $output->writeln('');
        $output->writeln('   <success>   Generating Entities   </success>');
        $output->writeln('');
        foreach ($entities as $key => $entityName) {
            $output->writeln($key . '. ' . $entityName);

            $reflection = new \ReflectionClass($entityName);
            $annotation = $this->annotationReader->getClassAnnotation($reflection, Rule\Entity::class);

            //check interfaces and index properties if neccesary
            foreach ($reflection->getInterfaces() as $interface) {
                /** @var Rule\EntityInterface $interfaceAnnotation */
                $interfaceAnnotation = $this->annotationReader->getClassAnnotation($interface,
                    Rule\EntityInterface::class);

                if (null !== $interfaceAnnotation && !\in_array($interface->getName(), $processedClasses, true)) {
                    /** @var Entity $entity */
                    $entity = $this->em->getRepository(Entity::class)->findOneOrCreate(['fullyQualifiedName' => $interface->getName()]);
                    $entity->setLabel($interfaceAnnotation->getDescription());
                    $entity->setRuleStartPoint(true);

                    $processedClasses[] = $interface->getName();
                    $newEntities->add($entity);
                }
            }

            if (null !== $annotation && !\in_array($entityName, $processedClasses, true)) {
                $label = $annotation->getDescription() ?? $reflection->getShortName();
                /** @var Entity $entity */
                $entity = $this->em->getRepository(Entity::class)->findOneOrCreate(['fullyQualifiedName' => $entityName]);
                $entity->setLabel($label);
                $entity->setRuleStartPoint(true);

                $processedClasses[] = $entityName;
                $newEntities->add($entity);
            }
        }

        $this->em->flush();

        foreach ($newEntities as $entity) {
            $this->runCommand('topgeschenken:entities:properties', ['id' => $entity->getId()], $output);
        }

        return 0;
    }

    /**
     * Runs a specified command with given arguments
     *
     * @param                 $cmd
     * @param array|null      $arguments
     * @param OutputInterface $output
     * @return int
     * @throws \Exception
     */
    private function runCommand($cmd, array $arguments, OutputInterface $output)
    {
        $app = $this->getApplication();
        $app->setAutoExit(false);

        $command = $app->find($cmd);

        $commandInput = null;

        if (null !== $arguments) {
            $commandInput = new ArrayInput($arguments);
        }

        return $command->run($commandInput, $output);
    }
}
