<?php

namespace RuleBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\QueryBuilder;
use RuleBundle\Entity\EntityAssociation;
use RuleBundle\Entity\Expression;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class AssociationsCommand
 * @package RuleBundle\Command
 */
class ExpressionCommand extends ContainerAwareCommand
{
    /** @var  EntityManagerInterface $em */
    protected $em;

    protected function configure()
    {
        $this->setName('topgeschenken:rules:expressions:clean');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();

        $existingExpressions = [];

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('expression_id', 'expression_id');
        $query = "SELECT DISTINCT `expression_id` FROM `rule_operand` WHERE `expression_id` IS NOT NULL UNION SELECT DISTINCT `expression_id` FROM `rule` WHERE `expression_id` IS NOT NULL;";
        $result = $this->em->createNativeQuery($query, $rsm)->getResult();

        foreach ($result as $exprId) {
            $existingExpressions[] = $exprId['expression_id'];
        }

        /** @var QueryBuilder $qb */
        $qb = $this->em->getRepository(Expression::class)->createQueryBuilder('e');
        $qb->andWhere('e.id NOT IN(:existingExpressions)');
        $qb->setParameter('existingExpressions', $existingExpressions);

        $unusedExpressions = $qb->getQuery()->getResult();

        foreach ($unusedExpressions as $unusedExpression) {
            $this->em->remove($unusedExpression);
        }

        $this->em->flush();
    }
}
