<?php

namespace RuleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Rule
 * @package RuleBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class Rule
{
    /**
     * @var int $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Expression $expression
     *
     * @ORM\OneToOne(targetEntity="RuleBundle\Entity\Expression", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    protected $expression;

    /**
     * @var string $start
     * @ORM\Column(type="string", length=255)
     */
    protected $start;

    /**
     * @var string $description
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $description;

    /**
     * @var string $definition
     * @ORM\Column(type="text", nullable=true)
     */
    protected $definition;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $satisfyAll;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getDescription();
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Expression $expression
     * @return $this
     */
    public function setExpression(Expression $expression)
    {
        $this->expression = $expression;

        return $this;
    }

    /**
     * @return Expression
     */
    public function getExpression()
    {
        return $this->expression;
    }

    /**
     * @param $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param $start
     * @return $this
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * @return string
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param $definition
     * @return Rule
     */
    public function setDefinition($definition): Rule
    {
        $this->definition = $definition;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getDefinition(): ?string
    {
        return $this->definition;
    }

    /**
     * @param $satisfyAll
     * @return Rule
     */
    public function setSatisfyAll($satisfyAll): Rule
    {
        $this->satisfyAll = $satisfyAll;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getSatisfyAll(): ?bool
    {
        return $this->satisfyAll;
    }
}
