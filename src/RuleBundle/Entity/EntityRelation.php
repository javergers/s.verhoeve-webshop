<?php

namespace RuleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Class EntityRelation
 * @package RuleBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class EntityRelation
{

    use ORMBehaviors\Translatable\Translatable;

    /**
     * @var integer $id
     *
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Entity $startEntity
     *
     * @ORM\ManyToOne(targetEntity="RuleBundle\Entity\Entity", inversedBy="entityRelations")
     */
    protected $startEntity;

    /**
     * @var Entity $endEntity
     *
     * @ORM\ManyToOne(targetEntity="RuleBundle\Entity\Entity")
     */
    protected $endEntity;

    /**
     * @var string $path
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $path;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return EntityRelation
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set startEntity
     *
     * @param Entity $startEntity
     *
     * @return EntityRelation
     */
    public function setStartEntity(Entity $startEntity = null)
    {
        $this->startEntity = $startEntity;

        return $this;
    }

    /**
     * Get startEntity
     *
     * @return Entity
     */
    public function getStartEntity()
    {
        return $this->startEntity;
    }

    /**
     * Set endEntity
     *
     * @param Entity $endEntity
     *
     * @return EntityRelation
     */
    public function setEndEntity(Entity $endEntity = null)
    {
        $this->endEntity = $endEntity;

        return $this;
    }

    /**
     * Get endEntity
     *
     * @return Entity
     */
    public function getEndEntity()
    {
        return $this->endEntity;
    }
}
