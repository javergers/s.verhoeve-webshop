<?php

namespace RuleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Expression
 * @package RuleBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 * @ORM\Table("rule_expression")
 */
class Expression
{
    /**
     * @var int $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Operand $leftOperand
     *
     * @ORM\ManyToOne(targetEntity="RuleBundle\Entity\Operand", inversedBy="expression", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    protected $leftOperand;

    /**
     * @var Operand $rightOperand
     *
     * @ORM\ManyToOne(targetEntity="RuleBundle\Entity\Operand", inversedBy="expression", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $rightOperand;

    /**
     * @var string $operator
     *
     * @ORM\Column(type="string", length=16)
     */
    protected $operator;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Operand $leftOperand
     * @return $this
     */
    public function setLeftOperand(Operand $leftOperand)
    {
        $this->leftOperand = $leftOperand;

        return $this;
    }

    /**
     * @return Operand
     */
    public function getLeftOperand()
    {
        return $this->leftOperand;
    }

    /**
     * @param Operand $rightOperand
     * @return $this
     */
    public function setRightOperand(Operand $rightOperand)
    {
        $this->rightOperand = $rightOperand;

        return $this;
    }

    /**
     * @return Operand
     */
    public function getRightOperand()
    {
        return $this->rightOperand;
    }

    /**
     * @param $operator
     * @return $this
     */
    public function setOperator($operator)
    {
        $this->operator = $operator;

        return $this;
    }

    /**
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }
}
