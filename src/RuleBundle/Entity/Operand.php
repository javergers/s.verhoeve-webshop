<?php

namespace RuleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Operand
 * @package RuleBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 * @ORM\Table("rule_operand")
 */
class Operand
{
    /**
     * @var int $id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Expression $expression
     *
     * @ORM\ManyToOne(targetEntity="RuleBundle\Entity\Expression", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $expression;

    /**
     * @var EntityRelation $entityRelation
     *
     * @ORM\ManyToOne(targetEntity="RuleBundle\Entity\EntityRelation")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $entityRelation;

    /**
     * @var string $variable
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $variable;

    /**
     * @var string $path
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $field;

    /**
     * @var string $value
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $value;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param Expression $expression
     * @return $this
     */
    public function setExpression(Expression $expression)
    {
        $this->expression = $expression;

        return $this;
    }

    /**
     * @return Expression
     */
    public function getExpression()
    {
        return $this->expression;
    }

    /**
     * @param string $variable
     * @return $this
     */
    public function setVariable(string $variable)
    {
        $this->variable = $variable;

        return $this;
    }

    /**
     * @return string
     */
    public function getVariable()
    {
        return $this->variable;
    }

    /**
     * @param $entityRelation
     * @return $this
     */
    public function setEntityRelation($entityRelation)
    {
        $this->entityRelation = $entityRelation;

        return $this;
    }

    /**
     * @return EntityRelation
     */
    public function getEntityRelation()
    {
        return $this->entityRelation;
    }

    /**
     * @param string $field
     * @return $this
     */
    public function setField(string $field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue(string $value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }
}
