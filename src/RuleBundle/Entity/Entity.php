<?php

namespace RuleBundle\Entity;

use AppBundle\Entity\Common\DataSet\DataSetFilterField;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Entity
 * @package RuleBundle\Entities
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BaseRepository")
 */
class Entity
{
    /**
     * @var integer $id
     *
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Property[] $properties
     *
     * @ORM\OneToMany(targetEntity="RuleBundle\Entity\Property", mappedBy="entity")
     */
    protected $properties;

    /**
     * @var $label
     *
     * @ORM\Column(type="string", length=64)
     */
    protected $label;

    /**
     * @var bool $ruleStartPoint
     *
     * @ORM\Column(type="boolean")
     */
    protected $ruleStartPoint = false;

    /**
     * @var EntityRelation[] $entityRelations
     *
     * @ORM\OneToMany(targetEntity="RuleBundle\Entity\EntityRelation", mappedBy="startEntity")
     */
    protected $entityRelations;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Common\DataSet\DataSetFilterField", mappedBy="entity",
     *                                                                                   cascade={"persist","remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     * @Assert\Valid(traverse=true)
     */
    protected $dataSetFilterFields;

    /**
     * @var string $path
     *
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $fullyQualifiedName;

    /**
     * Entity constructor.
     */
    public function __construct()
    {
        $this->properties = new ArrayCollection();
        $this->entityRelations = new ArrayCollection();
        $this->dataSetFilterFields = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return Entity
     */
    public function setLabel(string $label): Entity
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * Set fullyQualifiedName
     *
     * @param string $fullyQualifiedName
     *
     * @return Entity
     */
    public function setFullyQualifiedName($fullyQualifiedName): Entity
    {
        $this->fullyQualifiedName = $fullyQualifiedName;

        return $this;
    }

    /**
     * Get fullyQualifiedName
     *
     * @return string
     */
    public function getFullyQualifiedName(): string
    {
        return $this->fullyQualifiedName;
    }

    /**
     * @return Collection|Property[]
     */
    public function getProperties(): Collection
    {
        return $this->properties;
    }

    /**
     * @return bool
     */
    public function isRuleStartPoint(): bool
    {
        return (bool)$this->ruleStartPoint;
    }

    /**
     * @param bool $ruleStartPoint
     *
     * @return $this
     */
    public function setRuleStartPoint(bool $ruleStartPoint): Entity
    {
        $this->ruleStartPoint = $ruleStartPoint;

        return $this;
    }

    /**
     * @return Collection|EntityRelation[]
     */
    public function getEntityRelations(): Collection
    {
        return $this->entityRelations;
    }

    /**
     * @param DataSetFilterField $dataSetFilterField
     *
     * @return Entity
     */
    public function addDataSetFilterField(DataSetFilterField $dataSetFilterField): Entity
    {
        if (!$this->dataSetFilterFields->contains($dataSetFilterField)) {
            $dataSetFilterField->setEntity($this);

            $this->dataSetFilterFields->add($dataSetFilterField);
        }

        return $this;
    }

    /**
     * @param DataSetFilterField $dataSetFilterField
     *
     * @return Entity
     */
    public function removeDataSetFilterField(DataSetFilterField $dataSetFilterField): Entity
    {
        if (!$this->dataSetFilterFields->contains($dataSetFilterField)) {
            $this->dataSetFilterFields->removeElement($dataSetFilterField);
        }

        return $this;
    }

    /**
     * @return Collection|DataSetFilterField[]
     */
    public function getDataSetFilterFields(): Collection
    {
        return $this->dataSetFilterFields;
    }

}
