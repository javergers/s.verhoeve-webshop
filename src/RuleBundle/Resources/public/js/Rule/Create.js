function RuleCreate(url, callback, cancelCallback)
{
    this.contentWindow = new ContentWindow({
        width: '1200px',
        zIndex: 2000,
        toolbarButtons: [
            {
                position: 'right',
                label: 'Aanmaken',
                disabled: true,
                order: 1,
                classNames: 'btn btn-primary',
                action: 'rule_create'
            }
        ],
        hideCallbackOnSubmit: function() {
            location.reload();
        }
    });

    this.open(url, callback, cancelCallback);
}

RuleCreate.prototype.open = function (url, callback, cancelCallback)
{
    var self = this;

    this.url = url;
    this.contentWindow.empty().show();
    this.contentWindow.setOption('refreshUrl', this.url);
    this.contentWindow.setOption('refreshCallback', function () {
        self.initialize(callback, cancelCallback);
    });

    $.get(this.url).done(function (data) {
        self.contentWindow.setHtml(data);

        self.initialize(callback, cancelCallback);
    }).fail(function () {
        console.error('Something went wrong, please reload the page or try again');
    });
};

RuleCreate.prototype.initialize = function (callback, cancelCallback)
{
    this.bindEvents(callback, cancelCallback);
};

RuleCreate.prototype.bindEvents = function (callback, cancelCallback)
{
    var self = this;
    var contentWindow = self.contentWindow.elm;

    contentWindow.unbind();

    if(cancelCallback && cancelCallback instanceof Function) {
        contentWindow.on('click', 'a[data-action="cancel"]', function(e) {
            e.preventDefault();

            cancelCallback();
        });
    }

    contentWindow.on('change', '#rule_start_point', function () {
        contentWindow.find('.select2-hidden-accessible').select2('close');
        contentWindow.find('#rule_builder').addClass('loading');

        var state = $(this).val();

        $(this).parent().toggleClass('has-error', !state);
        $(this).toggleClass('error', !state);

        $.ajax({
            url: ruleBuilder.getOption('filtersUrl').replace('%%IDENTIFIER%%', encodeURI($(this).val())),
            method: 'GET'
        }).done(function (data) {
            ruleBuilder.setFilters(data, true);
        }).always(function() {
            contentWindow.find('#rule_builder').removeClass('loading');
        });
    });

    contentWindow.on('change', '.rule-operator-container select', function() {
        var val = $(this).val();
        if(val.indexOf(['is_null', 'is_not_null'])) {
            $('a[data-action="rule_create"]').removeAttr('disabled');
        }
    });

    contentWindow.on('keyup change', '.rule-value-container :input', function () {
        $('a[data-action="rule_create"]').removeAttr('disabled');
    });

    contentWindow.on('click', '[data-action="rule_create"]', function() {
        var rules = ruleBuilder.getRules();
        var startPoint = $('#rule_start_point');
        var startPointState = startPoint.val();

        startPoint.parent().toggleClass('has-error', !startPointState);
        startPoint.toggleClass('error', !startPointState);

        if(!$('#rule_description').val()) {
            swal('Foutieve invoer', 'Geef een regelnaam op', 'error');
            return;
        }

        if (rules !== null && startPointState) {
            $.ajax({
                url: ruleBuilder.getOption('createUrl'),
                method: 'POST',
                data: {
                    start: encodeURI(startPoint.val()),
                    description: $('#rule_description').val(),
                    rules: JSON.stringify(rules),
                    satisfy_all: $('#rule_satisfy_all:checked').is(':checked') ? 1 : 0
                }
            }).done(function (data) {
                var success = false;

                if (data && data.hasOwnProperty('message')) {
                    if (data.message === 'ok') {
                        success = true;
                    }
                }

                if(callback) {
                    callback();
                }

                if (success) {
                    setTimeout(function () {
                        self.contentWindow.hide();
                    }, 1500);
                }
            });
        }
    });
};
