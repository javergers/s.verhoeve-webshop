function RuleEdit(url) {
    this.contentWindow = new ContentWindow({
        width: '1200px',
        zIndex: 2000,
        toolbarButtons: []
    });

    this.open(url);
}

RuleEdit.prototype.open = function (url) {
    var self = this;

    this.url = url;
    this.contentWindow.empty().show();
    this.contentWindow.setOption('refreshUrl', this.url);
    this.contentWindow.setOption('refreshCallback', function () {
        self.initialize();
    });

    $.get(this.url).done(function (data) {
        self.contentWindow.setHtml(data);

        self.initialize();
    }).fail(function () {
        console.error('Something went wrong, please reload the page or try again');
    });
};

RuleEdit.prototype.initialize = function () {
    this.bindEvents();
};

RuleEdit.prototype.bindEvents = function () {
    var self = this;
    var contentWindow = self.contentWindow.elm;

    contentWindow.on('change', '#rule_start_point', function () {
        contentWindow.find('.select2-hidden-accessible').select2('close');
        contentWindow.find('#rule_builder').addClass('loading');

        var state = $(this).val();

        $(this).parent().toggleClass('has-error', !state);
        $(this).toggleClass('error', !state);

        $.ajax({
            url: ruleBuilder.getOption('filtersUrl').replace('%%IDENTIFIER%%', encodeURI($(this).val())),
            method: 'GET'
        }).done(function (data) {
            ruleBuilder.setFilters(data, true);
        }).always(function () {
            contentWindow.find('#rule_builder').removeClass('loading');
        });
    });

    contentWindow.on('keyup change', '.rule-value-container :input', function () {
        $('a[data-action="rule_create"]').removeAttr('disabled');
    });

    contentWindow.on('click', '[data-action="rule_edit_save"]', function () {
        var rules = ruleBuilder.getRules();

        if (rules !== null) {
            $.ajax({
                url: ruleBuilder.getOption('saveUrl'),
                method: 'POST',
                data: {
                    start: encodeURI($('#rule_start_point').val()),
                    description: $('#rule_description').val(),
                    rules: JSON.stringify(rules)
                }
            }).done(function (data) {
                var success = false;

                if (data && data.hasOwnProperty('message')) {
                    if (data.message === 'ok') {
                        success = true;
                    }
                }

                if (success) {
                    setTimeout(function () {
                        self.contentWindow.hide();
                    }, 1500);
                }
            });
        }
    });
};
