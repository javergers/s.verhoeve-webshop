/**
 * @class TsDaterangePicker
 * @description Applies Bootstrap Daterange pickers on input combo-boxes.
 * @param {object} [options]
 * @param {string} [options.showWeekNumbers=true]
 * @throws MissingLibraryError
 */
$.fn.queryBuilder.define('ts-daterange-picker', function (options) {
    if (!$.fn.daterangepicker) {
        throw Error('Bootstrap Daterange picker is required to use "ts-daterange-picker" plugin.');
    }

    var format = {
        date: 'YYYY-MM-DD',
        datetime: 'YYYY-MM-DD hh:mm'
    };

    var defaultOptions = {
        date: {
            locale: {
                format: 'DD-MM-YYYY'
            }
        },
        datetime: {
            timePicker: true,
            timePicker24Hour: true,
            timePickerSeconds: false,
            locale: {
                format: 'DD-MM-YYYY hh:mm'
            }
        }
    };

    this.on('afterCreateRuleInput', function (e, rule) {
        var filter = rule.filter;
        var operator = rule.operator;

        if (filter.type === 'date' || filter.type === 'datetime') {
            var input = $(rule.$el, '.rule-input-container').find('input.form-control:first');
            var input_2 = null;

            var inContentWindow = (input.parents('.content-window-body').length > 0);

            var newOptions = $.extend(true, defaultOptions[filter.type], options);
            var betweenOperator = (operator.type === 'date_between' || operator.type === 'date_not_between');

            if (betweenOperator) {
                input_2 = $(rule.$el, '.rule-input-container').find('input.form-control:last');
                input_2.attr('data-between-end-date', true);
            }

            var newInput = input.clone(false);

            newInput.attr('data-original-name', newInput.attr('name'));
            newInput.attr('name', newInput.attr('name') + '_clone');
            newInput.attr('placeholder', 'Dit is de clone');
            newInput.addClass('daterangepicker');

            $(newInput).insertAfter($(rule.$el, '.rule-input-container').find('input.form-control:last'));

            input.on('focus', function () {
                newInput.data('daterangepicker').show();
            });

            if (input_2) {
                input_2.on('focus', function () {
                    newInput.data('daterangepicker').show();
                });
            }

            if (betweenOperator) {
                newOptions.linkedCalendars = true;
                newOptions.singleDatePicker = false;
            }

            if (inContentWindow) {
                newOptions.parentEl = '.content-window';
                newOptions.opens = 'left';
            }

            newInput.daterangepicker(newOptions);

            newInput.on('apply.daterangepicker, hide.daterangepicker', function (ev, picker) {
                if (betweenOperator && input_2) {
                    input.val(picker.startDate.format(format[filter.type])).trigger('change');
                    input_2.val(picker.endDate.format(format[filter.type])).trigger('change');
                } else {
                    input.val(picker.startDate.format(format[filter.type])).trigger('change');
                }
            });

            newInput.on('cancel.daterangepicker', function (ev, picker) {
                input.val('').trigger('change');
            });
        }
    });

    this.on('afterUpdateRuleValue', function (e, rule) {
        var filter = rule.filter;
        var operator = rule.operator;

        if (rule._updating_value && (filter.type === 'date' || filter.type === 'datetime')) {
            var input = $(rule.$el, '.rule-input-container').find('input.form-control');
            var input_2 = null;

            var betweenOperator = (operator.type === 'date_between' || operator.type === 'date_not_between');

            if (betweenOperator) {
                input_2 = $(rule.$el, '.rule-input-container').find('input.form-control[data-between-end-date]');
            }

            var cloneName = input.attr('name') + '_clone';
            var newInput = $(rule.$el, '.rule-input-container').find('input[name="' + cloneName + '"]');

            var dp = newInput.data('daterangepicker');
            var date = new Date();
            var date_2 = new Date();

            if (dp) {
                if (betweenOperator) {
                    if (input.val()) {
                        date = moment(input.val());
                    }

                    if (input_2.val()) {
                        date_2 = moment(input_2.val());
                    }
                } else {
                    if (input.val()) {
                        date = moment(input.val());
                        date_2 = date;
                    }
                }

                dp.setStartDate(date);
                dp.setEndDate(date_2);
            }
        }
    });
}, {
    autoUpdateInput: true,
    showWeekNumbers: true,
    singleDatePicker: true,
    linkedCalendars: false
});