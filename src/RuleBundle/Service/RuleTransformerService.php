<?php

namespace RuleBundle\Service;

use AppBundle\Entity\Catalog\Product\Product;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use RuleBundle\Annotation as AnnotationRule;
use RuleBundle\Annotation\Method;
use RuleBundle\Annotation\Property;
use RuleBundle\Entity\Entity;
use RuleBundle\Entity\EntityRelation;
use RuleBundle\Entity\Expression;
use RuleBundle\Entity\Operand;
use RuleBundle\Entity\Rule;

/**
 * Class RuleTransformerService
 * @package RuleBundle\Service
 */
class RuleTransformerService
{
    /** @var EntityManagerInterface $em */
    private $em;

    /** @var Entity $entity */
    private $entity;

    /** @var array $array */
    private $array = [];

    /** @var AnnotationReader $annotationReader */
    protected $annotationReader;

    protected $types = ['string', 'integer', 'decimal', 'float', 'boolean'];

    private $operatorMap = [
        '<' => 'less',
        '>' => 'greater',
        '=' => 'equal',
        '!=' => 'not_equal',
        '= NULL' => 'is_null',
        '!= NULL' => 'is_not_null',
        '<=' => 'less_or_equal',
        '>=' => 'greater_or_equal',
    ];

    public const AND_CONDITION = 'AND';
    public const OR_CONDITION = 'OR';

    /**
     * RuleTransformerService constructor.
     *
     * @param EntityManagerInterface $em
     * @throws AnnotationException
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->annotationReader = new AnnotationReader();
    }

    /**
     * @param string $fqn
     *
     * @throws \RuntimeException
     */
    public function setEntity(string $fqn)
    {
        $entity = $this->em->getRepository(Entity::class)->findOneBy(['fullyQualifiedName' => $fqn]);

        if (!$entity->isRuleStartPoint()) {
            throw new \RuntimeException('rule.not_startpoint');
        }

        $this->entity = $entity;
    }

    /**
     * @return Entity
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param array $inputs
     *
     * @return array
     * @throws AnnotationException
     * @throws \ReflectionException
     */
    public function getFilters(array $inputs)
    {
        if (!isset($inputs[ResolverService::QB_ALIAS])) {
            throw new \RuntimeException("Key '" . ResolverService::QB_ALIAS . "' does not exist in inputs");
        }

        foreach ($inputs as $key => $entity) {
            $this->setEntity($entity->getFullyQualifiedName());
            $this->singleton($entity, [], null, true);
        }

        $output = [];

        foreach ($inputs as $inputName => $inputEntity) {
            $filter = $this->array[$inputEntity->getFullyQualifiedName()];
            $output[] = $this->getFilterObject($filter, $filter['description'], $inputName);
        }

        $output = $this->flattenOutput($output);

        usort($output, [$this, 'sortOutput']);

        return $output;
    }

    /**
     * @param Entity $entity
     * @param array  $breadcrumbs
     * @param mixed  $relation
     * @throws AnnotationException
     * @throws \ReflectionException
     */
    private function singleton(Entity $entity, array $breadcrumbs = [], $relation = null, $root = false)
    {
        $reflection = new \ReflectionClass($entity->getFullyQualifiedName());
        if ($reflection->isInterface()) {
            $annotationClass = AnnotationRule\EntityInterface::class;
        } else {
            $annotationClass = AnnotationRule\Entity::class;
        }

        $annotation = $this->annotationReader->getClassAnnotation($reflection, $annotationClass);

        if (null !== $annotation && !\in_array($entity->getId(), $breadcrumbs, true)) {
            $breadcrumbs[] = $entity->getId();

            $properties = [];
            if (!($entity instanceof \DateTime)) {
                $properties = $this->loopProperties($entity, $breadcrumbs, $relation, $root);
            }

            $this->array[$entity->getFullyQualifiedName()] = [
                'label' => $entity->getLabel(),
                'description' => $annotation->getDescription(),
                'properties' => $properties,
            ];
        }
    }

    /**
     * @param Entity $entity
     *
     * @param array  $breadcrumbs
     * @param mixed  $relation
     * @param bool   $root
     * @return array
     * @throws AnnotationException
     * @throws \ReflectionException
     */
    private function loopProperties(Entity $entity, array $breadcrumbs = [], $relation = null, $root = false)
    {
        $reflection = new \ReflectionClass($entity->getFullyQualifiedName());
        $properties = $entity->getProperties();
        $propertyArray = [];

        foreach ($properties as $property) {
            if ($root) {
                $relation = null;
            }

            $annotationReader = new AnnotationReader();
            if ($reflection->isInterface()) {
                $annotation = $annotationReader->getMethodAnnotation(new \ReflectionMethod($property->getEntity()->getFullyQualifiedName(),
                    'get' . ucfirst($property->getName())), Method::class);
            } else {
                $annotation = $annotationReader->getPropertyAnnotation(new \ReflectionProperty($property->getEntity()->getFullyQualifiedName(),
                    $property->getName()), Property::class);
            }

            $propertyInfo = [
                'path' => $property->getName(),
                'type' => $property->getType(),
                'description' => $property->getDescription(),
            ];

            if (null !== $annotation && null !== $annotation->getAutocomplete()) {
                $propertyInfo['xhr'] = [
                    'id' => $annotation->getAutocomplete()['id'],
                    'label' => $annotation->getAutocomplete()['label'],
                ];
            }

            if ($relation && $annotation && $annotation->getRelation() !== null && $relation !== $annotation->getRelation()) {
                continue;
            }

            $propertyArray[] = $propertyInfo;

            if (!array_key_exists($property->getType(), $this->array) && !\in_array($property->getType(), $this->types,
                    true)) {

                if (!$relation && $annotation && $annotation->getRelation()) {
                    $relation = $annotation->getRelation();
                }

                $entityRelation = $this->em->getRepository(Entity::class)->findOneBy(['fullyQualifiedName' => $property->getType()]);

                if (null !== $entityRelation) {
                    $this->singleton($entityRelation, $breadcrumbs, $relation);
                }
            }
        }

        return $propertyArray;
    }

    /**
     * @param string|array      $conditionSet
     * @param array             $inputs
     * @param string|null       $description
     *
     * @param Rule|null|\object $rule
     * @return Rule
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \ReflectionException
     */
    public function reverseTransformData($conditionSet, array $inputs, string $description = null, Rule $rule = null)
    {
        if (!\is_array($conditionSet)) {
            $conditionSet = json_decode($conditionSet, true);
        }

        if (!$rule) {
            /** @var Rule $rule */
            $rule = $this->em->getRepository(Rule::class)->findOneOrCreate([
                'expression' => $this->reverseTransformExpression($inputs, $conditionSet),
            ]);
        } else {
            $rule->setExpression($this->reverseTransformExpression($inputs, $conditionSet));
        }

        $rule->setDescription($description); // @todo: generate if null

        return $rule;
    }

    /**
     * @param array $data
     * @return \object|Rule
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     * @throws \ReflectionException
     */
    public function importData(array $data)
    {

        $conditionSet = json_encode($data['conditionSet']);

        $inputs = [
            ResolverService::QB_ALIAS => $this->em->getRepository(Entity::class)->findOneBy([
                'fullyQualifiedName' => urldecode($data['rule']['start']),
            ]),
        ];

        $rule = $this->reverseTransformData($conditionSet, $inputs, $data['rule']['description']);
        $rule->setStart($data['rule']['start']);

        $this->em->persist($rule);
        $this->em->flush();

        return $rule;
    }

    /**
     * @param array $inputs
     * @param array $conditionSet
     *
     * @return null|\object|Expression
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     * @throws \ReflectionException
     * @throws \TypeError
     */
    private function reverseTransformExpression(array $inputs, array $conditionSet)
    {
        $expression = null;

        if (isset($conditionSet['condition'])) {
            foreach ($conditionSet['rules'] as $subConditionSet) {
                $subExpression = $this->reverseTransformExpression($inputs, $subConditionSet);

                if (null === $expression) {
                    $expression = $subExpression;
                } else {
                    $expression = $this->em->getRepository(Expression::class)->findOneOrCreate([
                        'leftOperand' => $this->em->getRepository(Operand::class)->findOneOrCreate([
                            'expression' => $expression,
                            'entityRelation' => null,
                            'variable' => null,
                            'field' => null,
                            'value' => null,
                        ]),
                        'rightOperand' => $this->em->getRepository(Operand::class)->findOneOrCreate([
                            'expression' => $subExpression,
                            'entityRelation' => null,
                            'variable' => null,
                            'field' => null,
                            'value' => null,
                        ]),
                        'operator' => $conditionSet['condition'],
                    ]);
                }
            }
        } else {
            $leftOperand = $this->reverseTransformField($inputs, $conditionSet['id']);
            $rightOperand = $this->em->getRepository(Operand::class)->findOneOrCreate([
                'expression' => null,
                'entityRelation' => null,
                'variable' => null,
                'field' => null,
                'value' => \is_array($conditionSet['value']) ? implode(',',
                    $conditionSet['value']) : $conditionSet['value'],
            ]);

            $operator = $this->reverseTransformOperator($conditionSet['operator']);

            $expression = $this->em->getRepository(Expression::class)->findOneOrCreate([
                'leftOperand' => $leftOperand,
                'rightOperand' => $rightOperand,
                'operator' => $operator,
            ]);
        }

        $this->em->flush();

        return $expression;

    }

    /**
     * @param string $operator
     *
     * @return string
     */
    private function reverseTransformOperator(string $operator)
    {
        $reverseOperatorMap = array_flip($this->operatorMap);

        if (isset($reverseOperatorMap[$operator])) {
            return $reverseOperatorMap[$operator];
        }

        return $operator;
    }

    /**
     * @param array  $inputs
     * @param string $field
     *
     * @return \object
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     * @throws \ReflectionException
     * @throws \TypeError
     * @throws \Exception
     */
    private function reverseTransformField(array $inputs, string $field)
    {
        $parts = explode('.', $field);
        $entityRelation = null;

        $inputName = array_shift($parts);
        $propertyName = array_pop($parts);

        $path = null;
        if (!empty($parts)) {
            $endEntity = null;
            $path = implode('.', $parts);

            /** @var Entity[] $inputs */

            $reflection = new \ReflectionClass($inputs[$inputName]->getFullyQualifiedName());
            $annotationReader = new AnnotationReader();
            if ($reflection->isInterface()) {
                $targetClass = $inputs[$inputName]->getFullyQualifiedName();

                foreach ($parts as $associationName) {
                    if (null !== $annotationReader->getMethodAnnotation($reflection->getMethod('get' . ucfirst($associationName)),
                            Method::class)) {
                        $targetClass = $annotationReader->getMethodAnnotation($reflection->getMethod('get' . ucfirst($associationName)),
                            Method::class)->getRelatedEntity();
                        $reflection = new \ReflectionClass($targetClass);
                    }
                }
            } else {
                $targetClass = $this->em->getMetadataFactory()->getMetadataFor($inputs[$inputName]->getFullyQualifiedName());

                foreach ($parts as $associationName) {
                    $targetClass = $this->em->getMetadataFactory()->getMetadataFor($targetClass->getAssociationTargetClass($associationName));
                }

                $targetClass = $targetClass->getName();
            }

            $endEntity = $this->em->getRepository(Entity::class)->findOneBy([
                'fullyQualifiedName' => $targetClass,
            ]);

            if (null === $endEntity) {
                throw new \RuntimeException(sprintf('Entity not found for path "%s"', $path));
            }

            /** @var EntityRelation $entityRelation */
            $entityRelation = $this->em->getRepository(EntityRelation::class)->findOneOrCreate([
                'startEntity' => $inputs[$inputName],
                'path' => $path,
            ]);

            if ($entityRelation->getId() && $entityRelation->getEndEntity() !== $endEntity) {
                throw new \RuntimeException(sprintf('The target obtained by reflection ("%s") does not match the target ("%s") in the database',
                    $endEntity->getFullyQualifiedName(), $entityRelation->getEndEntity()->getFullyQualifiedName()));
            }

            $entityRelation->setEndEntity($endEntity);
        }

        return $this->em->getRepository(Operand::class)->findOneOrCreate([
            'expression' => null,
            'entityRelation' => $entityRelation,
            'variable' => $inputName === ResolverService::QB_ALIAS ? null : $inputName,
            'field' => $propertyName,
            'value' => null,
        ]);
    }

    /**
     * @param Rule $rule
     *
     * @return mixed
     * @throws \Exception
     */
    public function transformData(Rule $rule)
    {
        $conditionSet = $this->transformExpression($rule->getExpression());

        if (!isset($conditionSet['condition'])) {
            $conditionSet = [
                'condition' => 'AND',
                'rules' => [$conditionSet],
            ];
        }

        return $conditionSet;
    }

    /**
     * @param Expression $expression
     *
     * @return array
     * @throws \Exception
     */
    private function transformExpression(Expression $expression)
    {
        if (\in_array($expression->getOperator(), ['AND', 'OR'])) {
            $array['condition'] = $expression->getOperator();
            $array['rules'] = [];

            $subExpressions = [
                $expression->getLeftOperand()->getExpression(),
                $expression->getRightOperand()->getExpression(),
            ];

            foreach ($subExpressions as $subExpression) {
                $subStructure = $this->transformExpression($subExpression);

                if (isset($subStructure['condition']) && $subStructure['condition'] === $array['condition']) {
                    foreach ($subStructure['rules'] as $key => $value) {
                        $array['rules'][$key] = $value;
                    }
                } else {
                    $array['rules'][] = $subStructure;
                }
            }

            return $array;
        }

        $field = $this->transformField($expression->getLeftOperand());

        $operator = $this->transformOperator($expression->getOperator());
        $value = $expression->getRightOperand()->getValue();

        $array = [
            'operator' => $operator,
            'id' => $field,
            'field' => $field,
            'value' => ((in_array($operator,
                    ['between', 'not_between', 'date_between', 'date_not_between']) && strpos($value,
                    ',') !== false) ? explode(',', $value) : $value),
            'input' => 'text',
            'type' => 'string',
        ];

        return $array;
    }

    /**
     * @param string $operator
     *
     * @return mixed|string
     */
    private function transformOperator(string $operator)
    {
        if (isset($this->operatorMap[$operator])) {
            return $this->operatorMap[$operator];
        }

        return $operator;
    }

    /**
     * @param Operand $operand
     *
     * @return string
     * @throws \Exception
     */
    private function transformField(Operand $operand)
    {
        if (!$operand->getField()) {
            throw new \Exception("PARSER BUG");
        }

        $result = ResolverService::QB_ALIAS;
        if ($operand->getVariable()) {
            $result = $operand->getVariable();
        }

        if ($operand->getEntityRelation()) {
            $result .= '.' . $operand->getEntityRelation()->getPath();
        }

        return $result . '.' . $operand->getField();
    }

    /**
     * @param       $filter
     * @param null  $parentDescription
     * @param null  $propertyPath
     * @param array $output
     * @param null  $originalField
     *
     * @return array
     */
    private function getFilterObject(
        $filter,
        $parentDescription = null,
        $propertyPath = null,
        $output = [],
        $originalField = null
    ) {
        foreach ($filter['properties'] as $field) {
            if (strpos($field['type'], "\\") === false) {
                if (empty($field['values'])) {
                    $field['values'] = null;
                }

                if (empty($field['data'])) {
                    $field['data'] = [];
                }

                $filterObject = [
                    'optgroup' => $parentDescription,
                    'id' => $propertyPath . '.' . $field['path'],
                    'label' => $field['description'],
                    'type' => $this->getFilterType($field),
                    'input' => $this->getFilterInputElement($field),
                    'values' => $this->getFilterInputValues($field),
                    'operators' => $this->getFilterOperators($field),
                    'data' => $field['data'],
                    'value_separator' => ',',
                ];

                if ($filterObject['type'] === 'boolean') {
                    $filterObject['validation'] = [
                        'allow_empty_value' => true,
                    ];
                }

                $output[] = $filterObject;
            } elseif (isset($this->array[$field['type']], $field['xhr'])) {

                if (empty($field['values'])) {
                    $field['values'] = null;
                }

                if (empty($field['data'])) {
                    $field['data'] = [];
                }

                $filterObject = [
                    'optgroup' => $parentDescription,
                    'id' => $propertyPath . '.' . $field['path'] . '.id',
                    'label' => $field['description'],
                    'type' => $this->getFilterType($field),
                    'input' => $this->getFilterInputElement($field),
                    'values' => $this->getFilterInputValues($field),
                    'operators' => $this->getFilterOperators($field),
                    'data' => $field['data'],
                    'value_separator' => ',',
                ];

                $filterObject['data']['ajax'] = $field['xhr'];
                $filterObject['data']['ajax']['entity'] = $field['type'];
                $filterObject['valueSetter'] = true;

                $filterObject['type'] = 'integer';
                $filterObject['input'] = 'select';

                $output[] = $filterObject;

                if (strpos($parentDescription, '> '.$field['description']) === false) {
                    $output = $this->getFilterObject($this->array[$field['type']],
                        $parentDescription . ' > ' . $field['description'], $propertyPath . '.' . $field['path'],
                        $output, $field);
                }
            }
        }

        return $output;
    }

    /**
     * @param $output
     *
     * @return array
     */
    private function flattenOutput($output)
    {
        $result = [];

        foreach ($output as $items) {
            usort($items, [$this, 'sortOutput']);

            foreach ($items as $item) {
                $result[] = $item;
            }
        }

        return $result;
    }

    /**
     * @param $a
     * @param $b
     *
     * @return int
     */
    private function sortOutput($a, $b)
    {
        return strcasecmp($a['optgroup'], $b['optgroup']);
    }

    /**
     * @param $field
     *
     * @return string
     */
    private function getFilterType($field)
    {
        switch ($field['type']) {
            case 'string':
            case 'date':
            case 'datetime':
            case 'boolean':
                return $field['type'];
            case 'float':
            case 'decimal':
                return 'double';
            case 'collection':
                return 'integer';
            default:
                return 'string';
        }
    }

    /**
     * @param $field
     *
     * @return string
     */
    private function getFilterInputElement($field)
    {
        switch ($field['type']) {
            case 'boolean':
                return 'checkbox';

            case 'collection':
                return 'select';

            default:
                return 'text';
        }
    }

    /**
     * @param $field
     *
     * @return array|null
     */
    private function getFilterInputValues($field)
    {
        $output = [];
        $values = $field['values'];

        if ($field['type'] === 'boolean') {
            return [
                1 => ' ',
            ];
        }

        if (null === $values) {
            return null;
        }

        foreach ($values as $value) {
            $output[$value['id']] = $value['description'];
        }

        return $output;
    }

    /**
     * @param $field
     *
     * @return array|null
     */
    private function getFilterOperators($field)
    {
        $output = [];

        if (strpos($field['type'], '\\') !== false || $field['type'] === 'string') {
            return [
                'equal',
                'not_equal',
                'begins_with',
                'ends_with',
                'is_null',
                'is_not_null',
            ];
        }

        switch ($field['type']) {
            case 'boolean':
                $output[] = 'equal';

                break;
            case 'date':
            case 'datetime':
                $output = [
                    'equal',
                    'not_equal',
                    'less',
                    'less_or_equal',
                    'greater',
                    'greater_or_equal',
                    'date_between',
                    'date_not_between',
                ];

                break;
            case 'integer':
            case 'decimal':
            case 'float':
                $output = [
                    'equal',
                    'not_equal',
                    'less',
                    'less_or_equal',
                    'greater',
                    'greater_or_equal',
                    'between',
                    'not_between',
                    'is_null',
                    'is_not_null',
                ];

                break;
            case 'collection':
                $output = [
                    'equal',
                    'not_equal',
                    'is_null',
                    'is_not_null',
                ];

                break;

        }

        if (\count($output) === 0) {
            return null;
        }

        return $output;
    }

    /**
     * @return array
     */
    public function getOperators(): array
    {
        return $this->operatorMap;
    }
}
