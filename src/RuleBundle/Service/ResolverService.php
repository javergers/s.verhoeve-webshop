<?php

namespace RuleBundle\Service;

use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Hoa\Ruler\Model\Bag\Context;
use Hoa\Ruler\Model\Bag\Scalar;
use Hoa\Ruler\Model\Operator;
use RuleBundle\Annotation\MappedProperty;
use RuleBundle\Entity\Expression;
use RuleBundle\Entity\Operand;
use RuleBundle\Entity\Rule;
use RuleBundle\Target\CustomDoctrineORM;
use RulerZ\Compiler\Compiler;
use RulerZ\Parser\Parser;
use RulerZ\RulerZ;
use RulerZ\Target;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Traversable;

/**
 * Class ResolverService
 * @package RuleBundle\Service
 */
class ResolverService
{
    public const QB_ALIAS = '__result';

    /** @var array LIKE_OPERATORS */
    public const LIKE_OPERATORS = [
        'begins_with',
        'ends_with',
        'contains',
    ];

    /** @var EntityManagerInterface $em */
    private $em;

    /** @var RulerZ $executor */
    private $executor;

    /** @var string $class */
    private $class;

    /** @var AnnotationReader $annotationReader */
    private $annotationReader;

    /**
     * @param EntityManagerInterface $em
     * @throws AnnotationException
     */
    public function setEntityManager(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->annotationReader = new AnnotationReader();
    }

    /**
     * Set the executor class from RulerZ
     */
    public function setExecutor()
    {
        $compiler = Compiler::create();
        $this->executor = new RulerZ($compiler, [
            new CustomDoctrineORM(),
            new Target\Native\Native(),
        ]);
    }

    /**
     * @param string $class
     * @param Rule   $rule
     * @param array  $parameters
     * @return Traversable
     * @throws \Exception
     */
    public function filter(string $class, Rule $rule, array $parameters = [])
    {
        /** @var QueryBuilder $qb */
        $qb = $this->em->createQueryBuilder()
            ->select(self::QB_ALIAS)
            ->from($class, self::QB_ALIAS);

        $this->class = $class;

        $ruleText = $this->expressionToString($rule->getExpression(), $parameters);

        return $this->executor->filter($qb,
            $ruleText, $parameters);
    }

    /**
     * @param QueryBuilder $qb
     * @param string       $ruleText
     * @param              $parameters
     *
     * @return mixed
     */
    public function applyFilter(QueryBuilder $qb, string $ruleText, array $parameters = [])
    {
        return $this->executor->applyFilter($qb, $ruleText, $parameters);
    }

    /**
     * @param      $object
     * @param Rule $rule
     * @return bool
     * @throws \Exception
     */
    public function satisfies($object, Rule $rule)
    {
        $this->class = \get_class($object);

        if (null === $rule->getDefinition()) {
            $ruleText = $this->expressionToString($rule->getExpression(), []);
            $ruleText = str_replace(self::QB_ALIAS . '.', null, $ruleText);

            $rule->setDefinition($ruleText);
        }

        $parser = new Parser();
        $operators = $parser->parse($rule->getDefinition())->getOperators();
        $rootOperator = array_pop($operators);
        $result = $this->satisfyOperator($rootOperator, $object, $rule->getSatisfyAll());

        if (\is_array($result)) {
            return $this->satisfyResult($result);
        }

        return $result;
    }

    /**
     * @param array     $result
     * @param null|bool $satisfyAll
     * @return bool
     */
    private function satisfyResult(array $result, $satisfyAll = null): bool
    {
        foreach ($result as $key => $res) {
            if ($key === 'and') {
                $satisfyAll = true;
            } elseif ($key === 'or') {
                $satisfyAll = false;
            }

            foreach ($res as $bool) {
                if (\is_array($bool)) {
                    $bool = $this->satisfyResult($bool, $satisfyAll);
                }

                if ($bool xor $satisfyAll) {
                    return $bool;
                }
            }

            if ($satisfyAll) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Operator $operator
     * @param          $object
     * @param bool     $satisfyAll
     * @return mixed
     */
    private function satisfyOperator(Operator $operator, $object, $satisfyAll = false)
    {
        $operatorName = $operator->getName();
        $path = [];
        $result = [];
        $value = null;

        foreach ($operator->getArguments() as $argument) {
            if ($argument instanceof Operator) {
                $result[$operator->getName()][] = $this->satisfyOperator($argument, $object, $satisfyAll);
            } else {
                if ($argument instanceof Context) {
                    $path[] = $argument->getId();
                    foreach ($argument->getDimensions() as $dimension) {
                        $path[] = array_pop($dimension);
                    }
                }

                if ($argument instanceof Scalar) {
                    $value = $argument->getValue();
                }
            }
        }

        if (!empty($path)) {
            return $this->satisfySingleStatement($object, $path, $value, $operatorName, $satisfyAll);
        }

        return $result;
    }

    /**
     * @param       $object
     * @param array $path
     * @param       $statementValue
     * @param       $operator
     * @param bool  $satisfyAll
     * @return bool
     */
    private function satisfySingleStatement($object, array $path, $statementValue, $operator, $satisfyAll = false)
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()
            ->enableMagicCall()
            ->getPropertyAccessor();

        $value = $propertyAccessor->getValue($object, current($path));

        if (null !== $value && \count($path) > 1) {
            $path = \array_slice($path, 1);
            if ($value instanceof Collection) {
                foreach ($value as $o) {
                    $value = $this->satisfySingleStatement($o, $path, $statementValue, $operator, $satisfyAll);

                    if ($satisfyAll xor $value) {
                        return $value;
                    }
                }
            } else {
                if (\is_object($value)) {
                    $object = $value;
                }

                $value = $this->satisfySingleStatement($object, $path, $statementValue, $operator, $satisfyAll);
            }

            return $value;
        }

        switch ($operator) {
            case 'like':
                if (strpos($statementValue, '%') > 0) {
                    return $this->startsWith($value, str_replace('%', null, $statementValue));
                }

                return $this->endsWith($value, str_replace('%', null, $statementValue));
            case '= NULL':
                return null === $value;
            case '!= NULL':
                return null !== $value;
            case '<':
                return $value < $statementValue;
            case '<=':
                return $value <= $statementValue;
            case '>':
                return $value > $statementValue;
            case '>=':
                return $value >= $statementValue;
            case '=':
                return $value == $statementValue;
            case '!=':
                return $value != $statementValue;
        }

        return $value;
    }

    /**
     * @param Expression $expression
     * @param array      $parameters
     * @return string
     * @throws \Exception
     */
    public function expressionToString(Expression $expression, array $parameters)
    {
        $left = $this->operandToString($expression->getLeftOperand(), $parameters);
        $operator = $expression->getOperator();

        if (null !== $expression->getRightOperand()) {
            $right = $this->operandToString($expression->getRightOperand(), $parameters);

            if ($this->isLikeOperator($operator)) {
                $rule = $this->getLikeRuleText($left, $operator, $right);
            } else {
                $rule = "{$left} {$operator} {$right}";
            }
        } else {
            $rule = "{$operator} {$left}";
        }

        return $rule;
    }

    /**
     * @param $operator
     * @return bool
     */
    private function isLikeOperator($operator)
    {
        if (\in_array($operator, self::LIKE_OPERATORS, true)) {
            return true;
        }

        return false;
    }

    /**
     * @param $leftOperand
     * @param $operator
     * @param $rightOperand
     * @return string
     */
    private function getLikeRuleText($leftOperand, $operator, $rightOperand)
    {
        $format = null;
        switch ($operator) {
            case 'begins_with':
                $format = "%s LIKE '%s%%'";
                break;

            case 'ends_with':
                $format = "%s LIKE '%%%s'";
                break;

            case 'contains':
            default:
                $format = "%s LIKE '%%%s%%'";
                break;
        }

        return sprintf($format, $leftOperand, str_replace("'", null, $rightOperand));
    }

    /**
     * @param Operand $operand
     * @param array   $parameters
     * @return null|string
     * @throws \RuntimeException
     * @throws \Exception
     */
    private function operandToString(Operand $operand, array $parameters): ?string
    {
        if ($operand->getExpression()) {
            return '(' . $this->expressionToString($operand->getExpression(), $parameters) . ')';
        }

        $path = null;
        if (null !== $operand->getEntityRelation()) {
            $path = $operand->getEntityRelation()->getPath();
        }

        $field = $operand->getField();
        $value = $operand->getValue();
        $variable = $operand->getVariable();

        if (null !== $value) {
            if (!is_numeric($value)) {
                $value = "'$value'";
            }

            return (string)$value;
        }

        if ($path && null === $field) {
            throw new \RuntimeException('Missing field name');
        }

        if ($variable) {
            if ($path && null === $field) {
                return ':' . $variable;
            }

            if (!\is_object($parameters[$variable])) {
                throw new \RuntimeException(sprintf("Expected parameter '%s' to be an object.", $variable));
            }

            $relatedEntity = $parameters[$variable];

            if (null !== $operand->getEntityRelation() &&
                \get_class($relatedEntity) !== $operand->getEntityRelation()->getStartEntity()->getFullyQualifiedName()) {
                throw new \RuntimeException(sprintf("Wrong type of object passed for parameter '%s'", $variable));
            }

            $accessor = PropertyAccess::createPropertyAccessorBuilder()
                ->enableMagicCall()
                ->getPropertyAccessor();

            if (!$path) {
                $pathElements = explode('.', $path);

                foreach ($pathElements as $pathElement) {
                    $relatedEntity = $accessor->getValue($relatedEntity, $pathElement);
                }
            }

            return (string)$accessor->getValue($relatedEntity, $field);
        }

        if ($path) {
            $classMeta = $this->em->getClassMetadata($this->class);
            $pathElements = explode('.', $path);

            foreach ($pathElements as $key => $pathElement) {
                $property = $pathElement;
                //check if property exists
                if (!$classMeta->hasField($pathElement)) {
                    $method = 'get' . ucfirst($pathElement);
                    if ($classMeta->getReflectionClass()->hasMethod($method)) {
                        /** @var \ReflectionMethod $refMethod */
                        $refMethod = $classMeta->getReflectionClass()->getMethod($method);

                        /** @var MappedProperty $mappedPropAnnotation */
                        $mappedPropAnnotation = $this->annotationReader->getMethodAnnotation($refMethod,
                            MappedProperty::class);
                        if (null !== $mappedPropAnnotation) {
                            $property = $mappedPropAnnotation->getProperty();
                            $pathElements[$key] = $property;
                        } elseif (!$classMeta->getReflectionClass()->hasProperty($property)) {
                            throw new \RuntimeException(
                                sprintf(
                                    "The class '%s' has no property named '%s' but found method '%s()', use the @MappedProperty annotation in '%s' to map the method to the correct property.",
                                    $classMeta->getName(), $property, $method, $classMeta->getName()
                                )
                            );
                        }

                        $propertyAnnotations = $this->annotationReader->getPropertyAnnotations($classMeta->getReflectionProperty($property));

                        foreach ($propertyAnnotations as $propertyAnnotation) {
                            if (property_exists($propertyAnnotation, 'targetEntity')) {
                                $classMeta = $this->em->getClassMetadata($propertyAnnotation->targetEntity);
                                break;
                            }
                        }
                    }
                }
            }

            $path = implode('.', $pathElements);

            return self::QB_ALIAS . '.' . $path . '.' . $field;
        }

        if ($field) {
            return self::QB_ALIAS . '.' . $field;
        }

        return null;
    }

    /**
     * @param $haystack
     * @param $needle
     * @return bool
     */
    private function startsWith($haystack, $needle): bool
    {
        return (strpos($haystack, $needle) === 0);
    }

    /**
     * @param $haystack
     * @param $needle
     * @return bool
     */
    private function endsWith($haystack, $needle): bool
    {
        $length = \strlen($needle);

        if ($length === 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }
}
