<?php

namespace RuleBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use RuleBundle\Entity\Entity;
use RuleBundle\Entity\Rule;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RuleService
 * @package AppBundle\Services
 */
class RuleService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var RuleTransformerService */
    private $ruleTransformer;

    /**
     * RuleService constructor.
     * @param EntityManagerInterface          $entityManager
     * @param RuleTransformerService $ruleTransformer
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        RuleTransformerService $ruleTransformer
    ) {
        $this->entityManager = $entityManager;
        $this->ruleTransformer = $ruleTransformer;
    }

    /**
     * @param Request $request
     * @param boolean $returnRule
     *
     * @return bool|Rule
     *
     * @throws \Doctrine\Common\Persistence\Mapping\MappingException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     * @throws \ReflectionException
     */
    public function store(Request $request, $returnRule = false)
    {
        $inputs = [
            ResolverService::QB_ALIAS => $this->entityManager->getRepository(Entity::class)->findOneBy([
                'fullyQualifiedName' => urldecode($request->request->get('start')),
            ]),
        ];

        $rules = $request->request->get('rules');

        if ($rules) {
            $rule = $this->ruleTransformer->reverseTransformData($rules, $inputs,
                $request->request->get('description'), $request->get('rule'));

            $rule->setStart(urldecode($request->request->get('start')));
            $rule->setSatisfyAll((bool)$request->request->get('satisfy_all'));

            if (!$rule->getId()) {
                $this->entityManager->persist($rule);
            }

            $this->entityManager->flush();

            if ($returnRule) {
                return $rule;
            }

            return true;
        }

        return false;
    }
}
