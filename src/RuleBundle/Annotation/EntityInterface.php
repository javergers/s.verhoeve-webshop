<?php

namespace RuleBundle\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target({"CLASS"})
 * Class EntityInterface
 * @package RuleBundle\Annotation
 */
class EntityInterface
{
    /**
     * @var string $description
     */
    public $description;

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}