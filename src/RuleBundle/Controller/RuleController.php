<?php

namespace RuleBundle\Controller;

use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Geography\CountryTranslation;
use AppBundle\Entity\Order\OrderLine;
use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use JMS\JobQueueBundle\Entity\Job;
use Knp\DoctrineBehaviors\Model\Translatable\Translatable;
use RuleBundle\Entity\Entity;
use RuleBundle\Entity\Rule;
use RuleBundle\Service\ResolverService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class RuleController
 * @package RuleBundle\Controller
 * @Route("/rules")
 */
class RuleController extends BaseController
{
    public const TABLE_PREFIX = 'tbl_';

    /** @var EntityManagerInterface $em */
    protected $em;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->em = $container->get('doctrine.orm.entity_manager');
    }

    /**
     * @Route("/")
     * @Method("GET")
     * @Template()
     *
     * @return array
     */
    public function indexAction()
    {
        return [
            'rules' => $this->em->getRepository(Rule::class)->findAll(),
        ];
    }

    /**
     * @Route("/create")
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @param Request $request
     *
     * @return array | Response
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        $entityRepository = $this->getDoctrine()->getRepository(Entity::class);

        $entities = $entityRepository->findBy([
            'ruleStartPoint' => 1,
        ]);

        //generate entities when entities do not exist yet
        if (empty($entities)) {
            $jobManager = $this->container->get('job.manager');

            $job = new Job('topgeschenken:entities:generate');
            $jobManager->addJob($job);
            $this->container->get('doctrine')->getManager()->flush();

            return $this->render('@Rule/Rule/no-entities-message.html.twig');
        }

        if ($request->isMethod(Request::METHOD_POST)) {
            return $this->store($request);
        }

        return [
            'filters' => $this->get('rule.transformer')->getFilters([
                ResolverService::QB_ALIAS => $entityRepository->findOneBy([
                    'fullyQualifiedName' => OrderLine::class,
                ]),
            ]),
            'entities' => $entities,
            'checkEntityName' => OrderLine::class,
        ];
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws OptimisticLockException
     * @throws MappingException
     * @throws \Exception
     * @throws \ReflectionException
     * @throws \TypeError
     */
    private function store(Request $request)
    {
        if ($this->get('rule')->store($request)) {
            return new JsonResponse(['message' => 'ok'], 200);
        }

        return new JsonResponse(['message' => 'store.failed'], 422);
    }

    /**
     * @Route("/edit/{id}")
     * @Method({"GET", "POST"})
     * @Template()
     *
     * @param Request $request
     * @param Rule    $id
     * @return array|JsonResponse
     * @throws OptimisticLockException
     * @throws MappingException
     * @throws \Exception
     * @throws \ReflectionException
     * @throws \TypeError
     */
    public function editAction(Request $request, Rule $id)
    {
        $rule = $id;

        $entities = $this->getDoctrine()->getRepository(Entity::class)->findBy(['ruleStartPoint' => 1]);

        $ruleTransformer = $this->get('rule.transformer');

        if ($request->isMethod(Request::METHOD_POST)) {
            return $this->store($request);
        }

        $inputs = [
            ResolverService::QB_ALIAS => $this->em->getRepository(Entity::class)->findOneBy([
                'fullyQualifiedName' => urldecode($rule->getStart()),
            ]),
        ];

        return [
            'filters' => $ruleTransformer->getFilters($inputs),
            'entities' => $entities,
            'data' => $ruleTransformer->transformData($rule),
            'rule' => $rule,
        ];
    }

    /**
     * @Route("/{rule}/remove")
     * @Method("GET")
     *
     * @param Rule $rule
     *
     * @return JsonResponse
     * @throws OptimisticLockException
     */
    public function deleteAction(Rule $rule)
    {
        $this->em->remove($rule);
        $this->em->flush();

        return new JsonResponse(['message' => 'ok'], 200);
    }

    /**
     * @Route("/show/{rule}")
     * @Template()
     * @Method("GET")
     *
     * @param Rule $rule
     *
     * @return array
     */
    public function showAction(Rule $rule)
    {
        return [
            'rule' => $rule,
        ];
    }

    /**
     * @Route("/get-filters/{start}")
     * @Method("GET")
     *
     * @param string|null $start
     * @param Request     $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function getFiltersAction(string $start = null, Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException();
        }

        $ruleTransformer = $this->container->get('rule.transformer');

        $inputs = [
            ResolverService::QB_ALIAS => $this->em->getRepository(Entity::class)->findOneBy(['fullyQualifiedName' => $start]),
        ];

        return new JsonResponse($ruleTransformer->getFilters($inputs));
    }

    /**
     * @Route("/test-xhr-country/{country}")
     * @Method("GET")
     *
     * @param null    $country
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function testCountryXhrAction($country = null, Request $request)
    {
        // Fetch one result for initializing a select2 dropdown
        if (null !== $country) {
            /** @var Country $country */
            $country = $this->getDoctrine()->getRepository(Country::class)->find($country);

            return new JsonResponse([
                'id' => $country->getId(),
                'text' => $country->translate()->getName(),
            ]);
        }

        // Auto-complete search
        $term = $request->query->get('term', null);
        $results = [];

        if (null === $term || \strlen($term) < 3) {
            return new JsonResponse($results);
        }

        $qb = $this->em->createQueryBuilder()
            ->select('c')
            ->from(Country::class, 'c')
            ->leftJoin(CountryTranslation::class, 'ct', 'WITH', 'c.id = ct.translatable AND ct.locale = :locale ')
            ->where('LOWER(ct.name) LIKE :search')
            ->setParameters([
                'search' => '%' . $term . '%',
                'locale' => 'nl_NL',
            ])
            ->getQuery();

        $countries = $qb->getResult();

        /** @var Country $country */
        foreach ($countries as $country) {
            $results[] = [
                'id' => $country->getId(),
                'text' => $country->translate()->getName(),
            ];
        }


        return new JsonResponse($results);
    }

    /**
     * @Route("/filter/autocomplete/{lookup}")
     * @Method("POST")
     *
     * @param int|null $lookup
     * @param Request  $request
     *
     * @return JsonResponse
     * @throws \ReflectionException
     */
    public function filterAutocompleteAction($lookup = null, Request $request)
    {
        $term = $request->request->get('term', null);
        $entity = $request->request->get('entity', null);
        $idColumn = self::TABLE_PREFIX . $request->request->get('id', null);
        $labelColumn = self::TABLE_PREFIX . $request->request->get('label', null);

        $results = [];
        $translationEntity = null;
        $translationActive = false;

        if (null === $lookup && (null === $term || \strlen($term) < 3 || !class_exists($entity) || null === $idColumn || null === $labelColumn)) {
            return new JsonResponse($results);
        }

        $reflectionClass = new \ReflectionClass($entity);

        $entityAlias = self::TABLE_PREFIX . strtolower($reflectionClass->getShortName());
        $idProperty = str_replace($entityAlias . '.', null, $idColumn);
        $labelProperty = str_replace($entityAlias . '.', null, $labelColumn);

        // Fetch one result for initializing a select2 dropdown
        if (null !== $lookup) {
            $item = $this->getDoctrine()->getRepository($entity)->find($lookup);

            return new JsonResponse([
                'id' => $this->get('property_accessor')->getValue($item, $idProperty),
                'text' => $this->get('property_accessor')->getValue($item, $labelProperty),
            ]);
        }

        // Auto-complete search
        if (\in_array(Translatable::class, $reflectionClass->getTraitNames(), true)) {
            $translationEntity = $reflectionClass->getName() . 'Translation';

            if (class_exists($translationEntity)) {
                $translationActive = true;
            }
        }

        $parameters = [
            'search' => '%' . $term . '%',
        ];

        $qb = $this->em->createQueryBuilder()
            ->select($entityAlias)
            ->from($entity, $entityAlias);

        if ($translationActive && null !== $translationEntity) {
            $qb
                ->leftJoin($translationEntity, 'translate', 'WITH',
                    $idColumn . ' = translate.translatable AND translate.locale = :locale ');

            $parameters['locale'] = 'nl_NL';

            $labelColumn = $labelProperty;
        }

        $query = $qb
            ->where('LOWER(' . $labelColumn . ') LIKE :search')
            ->setParameters($parameters)
            ->getQuery();

        $items = $query->getResult();

        foreach ($items as $item) {
            $results[] = [
                'id' => $this->get('property_accessor')->getValue($item, $idProperty),
                'text' => $this->get('property_accessor')->getValue($item, $labelProperty),
            ];
        }

        return new JsonResponse($results);
    }
}
