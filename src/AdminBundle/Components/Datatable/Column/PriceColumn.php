<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Entity\Finance\Tax\VatRate;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class PriceColumn
 * @package AdminBundle\Components\Datatable\Column
 */
class PriceColumn extends AbstractColumn
{
    /**
     * @param Product $entity
     * @return string|null
     * @throws \Exception
     */
    public function value($entity): ?string
    {
        $withVat = false;

        if ($this->hasOption('withVat')) {
            $withVat = (bool)$this->getOption('withVat');
        }

        $accessor = PropertyAccess::createPropertyAccessor();

        // Define database column to handle.
        $column = $this->name;
        if ($this->hasOption('column') && null !== $this->getOption('column')) {
            $column = $this->getOption('column');
        }

        if ($accessor->isReadable($entity, $column)) {
            $prices = [];

            $variations = $entity->getVariations();

            if (!\count($variations)) {
                $price = $accessor->getValue($entity, $column);

                if (!$price && ($accessor->getValue($entity, 'minPrice') && $accessor->getValue($entity, 'maxPrice'))) {
                    $prices[] = $accessor->getValue($entity, 'minPrice');
                    $prices[] = $accessor->getValue($entity, 'maxPrice');
                } else {
                    if(!$price && method_exists($entity, 'getRelatedProduct')) {
                        $price = $accessor->getValue($entity->getRelatedProduct(), $column);
                    }

                    $prices[] = $price;
                }
            } else {
                $prices = [];

                foreach ($variations as $variation) {
                    $price = $accessor->getValue($variation, $column);

                    if ($price === null) {
                        $price = $accessor->getValue($entity, $column);
                    }

                    if (!\in_array($price, $prices, true)) {
                        $prices[] = $price;
                    }
                }
            }

            $html = '<div class="text-right" style="white-space: nowrap;">';

            sort($prices);

            if ((float)$prices[0] === 0.00) {
                $html .= '<em>gratis</em>, ';

                array_shift($prices);
            }

            if (!$prices) {
                $html = trim($html, ', ');
            }

            if ($withVat) {
                /** @var VatGroup $vatGroup */
                $vatGroup = $entity->getCheapestVariation()->getVatGroups()->filter(function(VatGroup $vatGroup) {
                    return $vatGroup->getCountry()->getId() === 'NL';
                })->current();

                if ($vatGroup) {
                    $vatRate = $vatGroup->getRateByDate();

                    if ($vatRate !== null) {
                        foreach ($prices as &$price) {
                            $price *= (1 + $vatRate->getFactor());
                        }
                    }
                }

                unset($price);
            }

            sort($prices);

            array_walk($prices, function (&$price) use ($withVat) {
                $price = number_format($price, $withVat ? 2 : 3, ',', '.');
            });

            $count = \count($prices);

            if ($count === 1) {
                $html .= '€ ' . $prices[0];
            } elseif ($count > 1) {
                $html .= '€ ' . $prices[0] . ' - ' . '€ ' . end($prices);
            }

            $html .= '</div>';

            return $html;
        }

        return false;
    }

    /**
     * @param OptionsResolver $resolver
     * @return OptionsResolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'orderable' => false,
            'searchable' => true,
            'withVat' => true,
            'column' => null,
        ]);

        $resolver->setDefined([
            'value',
        ]);

        $resolver->setDefault('attr', [
            'class' => 'text-right',
            'style' => 'width: 180px',
        ]);

        $resolver->setAllowedTypes('withVat', ['bool', 'null']);
        $resolver->setAllowedTypes('column', ['string', 'null']);

        return $resolver;
    }
}
