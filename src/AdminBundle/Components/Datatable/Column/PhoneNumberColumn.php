<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PhoneNumberColumn
 * @package AdminBundle\Components\Datatable\Column
 */
class PhoneNumberColumn extends AbstractColumn
{
    /**
     * @param $entity
     * @return null|string
     * @throws \Exception
     */
    public function value($entity): ?string
    {
        $value = parent::value($entity);

        if ($value) {
            return '<a href="tel:'. $value .'">' . $value . '</a>';
        }

        return null;
    }

    /**
     * @param OptionsResolver $resolver
     * @return OptionsResolver
     */
    public function configureOptions(OptionsResolver $resolver): OptionsResolver
    {
        return $resolver;
    }
}