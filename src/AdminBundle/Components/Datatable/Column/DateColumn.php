<?php

namespace AdminBundle\Components\Datatable\Column;

use Symfony\Component\OptionsResolver\OptionsResolver;

class DateColumn extends DateTimeColumn
{
    public function value($entity)
    {
        $this->options['timetype'] = \IntlDateFormatter::NONE;

        return parent::value($entity);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                "orderable" => true,
                "searchable" => true,
                "datetype" => \IntlDateFormatter::FULL,
            ]
        );

        return $resolver;
    }
}
