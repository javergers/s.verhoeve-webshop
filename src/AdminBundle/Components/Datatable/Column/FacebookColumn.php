<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;

class FacebookColumn extends AbstractColumn
{
    public function value($entity)
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        if ($accessor->isReadable($entity, $this->name) && $accessor->getValue($entity, $this->name)) {
            return '<a href="https://www.facebook.com/' . $accessor->getValue($entity,
                    $this->name) . '" target="_blank"><i class="icon-facebook2"></i></a>';
        }

        return false;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                "orderable" => false,
                "searchable" => true,
            ]
        );

        $resolver->setDefined([
            "value",
        ]);

        $resolver->setDefault("attr", [
            'class' => 'text-right',
            'style' => 'width: 180px',
        ]);

        return $resolver;
    }
}
