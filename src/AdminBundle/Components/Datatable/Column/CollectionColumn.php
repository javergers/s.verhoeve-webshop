<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;

class CollectionColumn extends AbstractColumn
{
    public function value($entity)
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        if ($accessor->isReadable($entity, $this->name)) {
            $a = [];

            foreach ($accessor->getValue($entity, $this->name) as $result) {
                if ($this->hasOption('value')) {
                    $a[] = call_user_func_array($this->getOption('value'), [$result]);
                } else {
                    $a[] = (string)$result;
                }
            }

            $a = array_filter($a, function ($value) {
                return !empty($value);
            });


            return implode(", ", $a);
        }

        return false;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                "orderable" => false,
                "searchable" => true,
            ]
        );

        $resolver->SetRequired([
            "property",
        ]);

        $resolver->setDefined([
            "value",
        ]);

//        $resolver->setRequired(
//            array(
//                "value",
//            )
//        );

        return $resolver;
    }
}
