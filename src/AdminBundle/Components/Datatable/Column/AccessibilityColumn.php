<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use AppBundle\Traits\AccessibleEntity;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccessibilityColumn extends AbstractColumn
{
    public function __construct($name, array $options)
    {
        parent::__construct($name, $options);
    }

    /**
     * @param $entity AccessibleEntity
     *
     * @return mixed $html;
     */
    public function value($entity)
    {
        $html = null;
        $class = null;
        $title = 'Zichtbaar ';

        if ($entity->isLoginRequired() && $entity->getAccessibleCustomerGroups()->isEmpty()) {
            $class = 'icon-primitive-dot column-accessible text-danger';
            $title .= 'voor ingelogde gebruikers';
        } else {
            if ($entity->isLoginRequired() && !$entity->getAccessibleCustomerGroups()->isEmpty()) {
                $class = 'icon-primitive-dot column-accessible text-warning';
                $title .= 'voor klantgroepen:<br />';

                foreach ($entity->getAccessibleCustomerGroups() as $customerGroup) {
                    $title .= $customerGroup->getDescription() . '<br />';
                }
            } else {
                $class = 'icon-primitive-dot column-accessible text-success';
                $title .= 'voor iedereen';
            }
        }

        $html .= '<span class="' . $class . '" data-popup="tooltip" data-html="true" title="' . $title . '" data-placement="bottom"></span>';

        return $html;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "label" => '<span class="icon-lock2"></span>',
            "orderable" => false,
        ]);

        return $resolver;
    }
}
