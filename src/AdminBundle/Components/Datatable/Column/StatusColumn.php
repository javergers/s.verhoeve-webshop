<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class StatusColumn
 * @package AdminBundle\Components\Datatable\Column
 */
class StatusColumn extends AbstractColumn
{

    /**
     * AbstractColumn constructor.
     * @param       $name
     * @param array $options
     */
    public function __construct($name, array $options)
    {
        parent::__construct($name, $options);
    }

    /**
     * @param $entity
     * @return mixed|null
     * @throws \Exception
     */
    public function value($entity)
    {
        $value = parent::value($entity);

        $activeStatusLabel = $value ? 'Actief' : 'Inactief';
        $activeStatusClass = $value ? 'success' : 'danger';
        $class = "label label-flat border-{$activeStatusClass} text-{$activeStatusClass}-600";

        return '<span class="' . $class . '">' . $activeStatusLabel . '</span>';
    }

    /**
     * @param OptionsResolver $resolver
     * @return mixed
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver;
    }
}
