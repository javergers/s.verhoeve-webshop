<?php

namespace AdminBundle\Components\Datatable\Column;

use function strtolower;

/**
 * Class FontColumn
 * @package AdminBundle\Components\Datatable\Column
 */
class FontColumn extends TextColumn
{
    /**
     * @param $entity
     *
     * @return mixed|string|null
     */
    public function value($entity)
    {
        $html = '';

        if (method_exists($entity, 'getType') && strtolower($entity->getType()) === 'google') {
            $html .= '<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=' . $entity->getFamily() . '">';
        }

        $html .= '<span style="font-family: \'' . $entity->getFamily() . '\'">' . $entity->getFamily() . '</span>';

        return $html;
    }
}