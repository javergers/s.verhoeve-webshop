<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressColumn extends AbstractColumn
{
    public function __construct($name, array $options)
    {
        parent::__construct($name, $options);
    }

    public function value($entity)
    {
        $address = $entity->getAddress();
        $address .= ", " . $entity->getPostcode() . " " . $entity->getCity();
        $address .= ", " . $entity->getCountry();

        return $address;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                "orderable" => false,
                "searchable" => true,
            ]
        );

        return $resolver;
    }
}
