<?php

namespace AdminBundle\Components\Datatable\Column;

use Symfony\Component\PropertyAccess\PropertyAccess;

class IpColumn extends TextColumn
{
    public function __construct($name, array $options)
    {
        parent::__construct($name, $options);
    }

    public function value($entity)
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        $value = null;

        if ($accessor->isReadable($entity, $this->name)) {
            $ip = $accessor->getValue($entity, $this->name);

            if ($ip) {
                $output = '<form action="https://ipaddressguide.com/ip2location" method="post" target="_blank">';
                $output .= '<input type="hidden" name="ip" value="' . $ip . '">';
                $output .= '<input type="submit" value="' . $ip . '" style="background: none; border: none; padding: 0; margin: 0; text-align: center; display: inline;">';
                $output .= '</form>';

                $value = $output;
            }
        }

        return $value;
    }
}