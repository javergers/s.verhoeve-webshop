<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TextColumn extends AbstractColumn
{
    public function __construct($name, array $options)
    {
        parent::__construct($name, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                "orderable" => true,
                "searchable" => true,
            ]
        );

        return $resolver;
    }
}
