<?php

namespace AdminBundle\Components\Datatable\Column;

use Symfony\Component\OptionsResolver\OptionsResolver;

class CallbackColumn extends TextColumn
{
    public function value($entity)
    {
        $value = call_user_func_array($this->getOption('callback'), [
            parent::value($entity),
            $entity,
        ]);

        return $value;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined([
            "callback",
        ]);

        $resolver->setAllowedTypes('callback', ['closure']);

        return $resolver;
    }
}
