<?php

namespace AdminBundle\Components\Datatable\Column;

use Symfony\Component\OptionsResolver\OptionsResolver;

class TextareaColumn extends TextColumn
{
    public function __construct($name, array $options)
    {
        parent::__construct($name, $options);
    }

    public function value($entity)
    {
        $value = parent::value($entity);
        $wrappedValue = explode(PHP_EOL, wordwrap($value, 75, PHP_EOL))[0];

        if ($wrappedValue != $value) {
            $wrappedValue .= "&hellip;";
        }

        return '<div title="' . $value . '">' . $wrappedValue . '</div>';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                "orderable" => true,
                "searchable" => true,
            ]
        );

        return $resolver;
    }
}
