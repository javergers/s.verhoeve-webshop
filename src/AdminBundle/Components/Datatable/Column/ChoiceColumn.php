<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChoiceColumn extends AbstractColumn
{
    public function __construct($name, array $options)
    {
        parent::__construct($name, $options);
    }

    public function value($entity)
    {
        $value = parent::value($entity);

        if (!array_key_exists($value, $this->getOption("choices"))) {
            return '<i>' . $value . '</i>';
        }

        return $this->getOption("choices")[$value];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                "orderable" => true,
                "searchable" => true,
            ]
        );

        $resolver->setRequired([
            "choices",
        ]);

        return $resolver;
    }
}
