<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use AdminBundle\Traits\PublishableEntity;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PublishColumn extends AbstractColumn
{
    public function __construct($name, array $options)
    {
        parent::__construct($name, $options);
    }

    /**
     * @param $entity PublishableEntity
     *
     * @return mixed $html;
     */
    public function value($entity)
    {
        $html = null;

        if ($entity->isScheduled()) {
            $class = 'icon-alarm column-publish text-warning planned';
            $title = 'Ingepland:<br />';

            $title .= $this->getFormattedPublishDates($entity);
        } else {
            if ($entity->isPublished()) {
                $class = 'icon-primitive-dot text-success column-publish published';
                $title = 'Gepubliceerd';

                if ($entity->hasSchedule(false)) {
                    $title = 'Gepubliceerd (van/tot):<br />';
                    $title .= $this->getFormattedPublishDates($entity);
                }
            } else {
                $class = 'icon-primitive-dot text-danger column-publish not-published';
                $title = 'Niet gepubliceerd';
            }
        }

        $html .= '<span class="' . $class . '" data-popup="tooltip" data-html="true" title="' . $title . '" data-placement="bottom"></span>';

        return $html;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "label" => '<span class="icon-traffic-lights"></span>',
            "orderable" => false,
        ]);

        return $resolver;
    }

    /**
     * @param $entity PublishableEntity
     *
     * @return string $formatted
     */
    private function getFormattedPublishDates($entity)
    {
        $formatted = null;

        if ($entity->getPublishStart()) {
            $formatted .= 'Start: ' . $entity->getPublishStart()->format('d-m-Y') . ' om ' . $entity->getPublishStart()->format('H:i') . '<br />';
        }

        if ($entity->getPublishEnd()) {
            $formatted .= 'Eind: ' . $entity->getPublishEnd()->format('d-m-Y') . ' om ' . $entity->getPublishEnd()->format('H:i');
        }

        return $formatted;
    }
}
