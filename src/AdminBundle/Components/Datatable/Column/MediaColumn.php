<?php

namespace AdminBundle\Components\Datatable\Column;

use AdminBundle\Components\Datatable\AbstractColumn;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MediaColumn extends AbstractColumn
{

    public function value($entity)
    {
        if ($this->hasOption('value')) {
            $value = call_user_func_array($this->getOption('value'), [$entity]);
        }

        if (!$value) {
            return '<em>Geen afbeelding</em>';
        }

        $output = null;

        if ($this->hasOption('lightbox') && $this->hasOption('lightbox') == true) {
            $output .= '<a href="' . $value . '" data-popup="lightbox">';
        }

        $output .= '<img src="' . $value . '" class="img-rounded img-preview">';

        if ($this->hasOption('lightbox') && $this->hasOption('lightbox') == true) {
            $output .= '</a>';
        }

        return $output;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired([
            "value",
        ]);

        $resolver->setDefaults([
            'lightbox' => true,
            'orderable' => false,
        ]);

        return $resolver;
    }
}
