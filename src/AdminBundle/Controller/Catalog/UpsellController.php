<?php

namespace AdminBundle\Controller\Catalog;

use AdminBundle\Controller\Controller;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Product;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/upsell-producten")
 * Class UpsellController
 * @package AdminBundle\Controller\Catalog
 */
class UpsellController extends Controller
{
    /**
     * @Route("/zoeken", methods={"GET"})
     * @Template()
     *
     * @param Request $request
     *
     * @return array
     * @throws \Exception
     */
    public function searchAction(Request $request): array
    {
        /** @var QueryBuilder $qb */
        $qb = $this->getDoctrine()->getRepository(Product::class)->createQueryBuilder('product');
        $qb->leftJoin('product.variations', 'variation');
        $qb->addSelect('variation');

        $qb->andWhere($qb->expr()->orX(
            $qb->expr()->like('product.name', ':title'),
            $qb->expr()->like('variation.name', ':title')
        ));

        $qb->setParameter('title', '%' . $request->query->get('query') . '%');
        $qb->andWhere('product.parent IS NULL');

        $results = $qb->getQuery()->getResult();

        return [
            'results' => $results,
        ];
    }
}