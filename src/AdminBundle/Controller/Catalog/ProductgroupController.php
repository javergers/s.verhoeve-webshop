<?php

namespace AdminBundle\Controller\Catalog;

use AdminBundle\Controller\CrudController;
use AdminBundle\Form\Catalog\ProductgroupType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Catalog\Product\Productgroup;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @Route("/catalogus/productgroepen")
 */
class ProductgroupController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null $n
     * @return mixed|null|string
     */
    public function getLabel($n = null)
    {
        return 'Productgroep';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => Productgroup::class,
            'form_type' => ProductgroupType::class,
            'datatable' => [
                'entity_class' => Productgroup::class,
                'form_type' => ProductgroupType::class,
                'datatable_type' => 'AdminBundle\Datatable\Catalog\ProductgroupType',
            ],
        ];
    }
}
