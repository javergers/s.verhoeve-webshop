<?php

namespace AdminBundle\Controller\Catalog;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Catalog\ProductDatatableType;
use AdminBundle\Datatable\Catalog\ProductVariationDatatableType;
use AppBundle\Exceptions\NonDeletableException;
use AdminBundle\Form\Catalog\Product\ProductVariationFormType;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Product;
use Doctrine\ORM\EntityNotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/catalogus/product-variaties")
 * Class ProductVariationController
 * @package AdminBundle\Controller\Catalog
 */
class ProductVariationController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'label' => 'Productvariaties',
            'entity_class' => GenericProduct::class,
            'form_type' => ProductVariationFormType::class,
            'xhr_assets' => [
                'admin/js/form/product-variation.js',
            ],
            'assets' => [
                'admin/assets/sortable/Sortable.min.js',
                'admin/js/form/product.js',
                'admin/css/form/product.css',
                'admin/js/form/assortment.js',
                'admin/css/form/assortment.css',
            ],
            'datatable' => [
                'datatable_type' => ProductDatatableType::class,
                'entity_class' => GenericProduct::class,
            ],
            'view' => [
                'panels' => [
                    [
                        'id' => 'description_panel',
                        'type' => 'template',
                        'template' => '@Admin/Product/description-panel.html.twig',
                    ],
                    [
                        'id' => 'variation-panel',
                        'title' => 'Variaties',
                        'type' => 'datatable',
                        'datatable' => [
                            'datatable_type' => ProductVariationDatatableType::class,
                            'entity_class' => GenericProduct::class,
                            'form_type' => ProductVariationFormType::class,
                            'results_per_page' => 5,
                            'parent_column' => 'parent',
                            'base_route' => 'admin_catalog_productvariation',
                            'allow_read' => false,
                            'allow_reorder' => true,
                            'item_actions' => [
                                [
                                    'icon' => 'mdi mdi-truck-fast',
                                    'route' => 'admin_supplier_supplierproduct_supplier',
                                    'type' => 'open-content',
                                    'method' => 'GET',
                                ],
                            ],
                            'refresh_panel' => 'variation-panel',
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @Route("/{id}/bewerken", methods={"GET"})
     * @Template()
     * @param $id
     *
     * @return array|Response
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function editAction($id)
    {
        $this->denyAccessUnlessGranted($this->getRolesFor('create'));

        $entity = $this->getRepository()->find($id);

        $form = $this->getForm($this->getFormType(), $entity);

        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/{id}/bewerken", methods={"POST"})
     *
     * @param Request $request
     * @param int     $id
     * @return Response
     * @throws EntityNotFoundException
     * @throws \Exception
     */
    public function forwardEditAction(Request $request, int $id)
    {
        /** @var Product $product */
        $product = $this->getDoctrine()->getRepository(GenericProduct::class)->find($id);

        if (null === $product) {
            throw new EntityNotFoundException('Product cannot be found');
        }

        return $this->editPersistAction($id);
    }

    /**
     * @Route("/{id}/verwijderen", methods={"DELETE"})
     *
     * @param int $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteAction($id): JsonResponse
    {
        $this->denyAccessUnlessGranted($this->getRolesFor('delete'));
        $parent = null;

        /** @var Product $product */
        $product = $this->getDoctrine()->getRepository(GenericProduct::class)->find($id);

        if (null === $product) {
            throw new \Exception('Product cannot be found');
        }

        if ($product->getParent()) {
            $parent = $product->getParent();
        }

        if (null !== $parent && $parent->getVariations()->count() < 2) {
            return new JsonResponse(['response' => 'last_variation', 'type' => 'error']);
        }

        try {
            $em = parent::getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();

            return new JsonResponse(['response' => 'variation_removed', 'type' => 'success']);
        } catch (NonDeletableException $e) {
            return new JsonResponse(['response' => 'cannot_remove_variation', 'type' => 'error']);
        } catch (\Exception $e) {
            return new JsonResponse(['response' => 'unknown_error_occured', 'type' => 'error']);
        }
    }
}
