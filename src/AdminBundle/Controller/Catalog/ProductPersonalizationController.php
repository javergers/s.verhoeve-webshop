<?php

namespace AdminBundle\Controller\Catalog;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Catalog\ProductDatatableType;
use AdminBundle\Form\Catalog\PersonalizationType;
use AdminBundle\Form\Catalog\Product\PersonalizationVariationFormType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Catalog\Product\Personalization;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/catalogus/producten/personalisaties")
 *
 */
class ProductPersonalizationController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Personalisaties';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'label' => 'Product',
            'entity_class' => Personalization::class,
            'form_type' => PersonalizationType::class,
            'assets' => [
                'admin/js/form/product.js',
                'admin/js/designer/DesignSelectModal.js',
                'admin/css/form/product.css',
                'admin/css/product/product-item.css',
                'admin/css/designer/DesignSelectModal.css',
            ],
            'title' => function (Personalization $product) {
                return $product->getName();
            },
            'datatable' => [
                'datatable_type' => ProductDatatableType::class,
                'entity_class' => Personalization::class,
                'form_type' => PersonalizationType::class,
                'query_builder' => function (QueryBuilder $qb) {
                    $qb->andWhere('product.parent IS NULL');
                },
                'paginator' => [
                    'options' => [
                        'wrap-queries' => true,
                    ],
                ],
                'allow_edit' => false,
                'allow_read' => true
            ],
            'view' => [
                'panels' => [
                    [
                        'id' => 'description-panel',
                        'type' => 'template',
                        'template' => '@Admin/Product/personalization-panel.html.twig',
                    ],
                    [
                        'id' => 'variation-panel',
                        'title' => 'Variaties',
                        'type' => 'datatable',
                        'datatable' => [
                            'datatable_type' => ProductDatatableType::class,
                            'entity_class' => Personalization::class,
                            'form_type' => PersonalizationVariationFormType::class,
                            'results_per_page' => 5,
                            'parent_column' => 'parent',
                            'base_route' => 'admin_catalog_personalizationvariation',
                            'item_actions' => [
                                [
                                    'icon' => 'mdi mdi-truck-fast',
                                    'route' => 'admin_supplier_supplierproduct_supplier',
                                    'type' => 'open-content',
                                    'method' => 'GET',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
