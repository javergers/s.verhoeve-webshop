<?php

namespace AdminBundle\Controller\Catalog\Product;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Catalog\ProductCardType;
use AdminBundle\Form\Catalog\CompanyCardFormType;
use AppBundle\Entity\Catalog\Product\CompanyCard;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Relation\Company;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CompanyCardController
 * @package AdminBundle\Controller\Catalog
 * @Route("/catalogus/producten/bedrijfskaartjes")
 */
class CompanyCardController extends CrudController
{

    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Bedrijfskaartjes';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'label' => 'Bedrijfskaartjes',
            'entity_class' => CompanyCard::class,
            'form_type' => CompanyCardFormType::class,
            'assets' => [
                'admin/js/form/product.js',
                'admin/css/form/product.css',
                'admin/css/product/product-item.css',
            ],
            'datatable' => [
                'entity_class' => CompanyCard::class,
                'form_type' => CompanyCardFormType::class,
                'datatable_type' => ProductCardType::class,
                'paginator' => [
                    'options' => [
                        'wrap-queries' => true,
                    ],
                ],
                'allow_edit' => true,
            ],
            'title' => function (CompanyCard $product) {
                return $product->getName();
            },
            'view' => [
                'panels' => [
                    [
                        'id' => 'description-panel',
                        'type' => 'template',
                        'template' => '@Admin/Product/company-card-description-panel.html.twig',
                    ],
                ],
            ],
        ];
    }

    /**
     * @Route("/{company}/standaard-kaartje/toevoegen", methods={"POST"})
     * @param Company $company
     * @param Request $request
     * @return JsonResponse
     */
    public function addDefaultAction(Company $company, Request $request)
    {
        $cardId = $request->get('card');

        if (null !== $cardId) {
            /** @var ProductCard $card */
            $card = $this->getDoctrine()->getRepository(ProductCard::class)->find($cardId);

            $company->addCard($card);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse('ok');
        }

        return new JsonResponse('error', 400);
    }

    /**
     * @Route("/{company}/{card}/standaard-kaartje/verwijderen", methods={"DELETE"})
     * @param Company     $company
     * @param ProductCard $card
     * @return JsonResponse
     */
    public function removeDefaultAction(Company $company, ProductCard $card)
    {
        $company->removeCard($card);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse('ok');
    }
}