<?php

namespace AdminBundle\Controller\Catalog\Product;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Catalog\ProductPackagingUnitDatatableType;
use AdminBundle\Form\Product\PackagingUnitFormType;
use AppBundle\Entity\Catalog\Product\PackagingUnit;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/catalogus/producten/verpakkings-eenheden")
 * Class PackagingUnitController
 * @package AdminBundle\Controller\Product
 */
class PackagingUnitController extends CrudController
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Verpakkingseenheden';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => PackagingUnit::class,
            'form_type' => PackagingUnitFormType::class,
            'datatable' => [
                'entity_class' => PackagingUnit::class,
                'datatable_type' => ProductPackagingUnitDatatableType::class,
                'form_type' => PackagingUnitFormType::class,
            ],
        ];
    }
}
