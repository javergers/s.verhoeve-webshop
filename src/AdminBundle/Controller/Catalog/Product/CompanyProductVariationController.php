<?php

namespace AdminBundle\Controller\Catalog\Product;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Catalog\ProductDatatableType;
use AdminBundle\Form\Catalog\Product\CompanyProductFormType;
use AdminBundle\Form\Catalog\Product\CompanyProductVariationFormType;
use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Catalog\Product\ProductTranslation;
use AppBundle\Entity\Relation\Company;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/catalogus/bedrijfsproducten-variaties")
 * Class CompanyProductController
 * @package AdminBundle\Controller\Catalog\Product
 */
class CompanyProductVariationController extends CrudController
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Bedrijfsproducten';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => CompanyProduct::class,
            'form_type' => CompanyProductVariationFormType::class,
            'datatable' => [
                'datatable_type' => ProductDatatableType::class,
                'entity_class' => CompanyProduct::class,
                'form_type' => CompanyProductFormType::class,
                'actions_processing' => 'override',
            ],
        ];
    }
}
