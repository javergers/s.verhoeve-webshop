<?php

namespace AdminBundle\Controller\Catalog\Product;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Catalog\Product\ProductStickerDatatableType;
use AdminBundle\Form\Catalog\Product\ProductStickerFormType;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductSticker;
use Doctrine\ORM\EntityManagerInterface;
use RuleBundle\Entity\Entity;
use RuleBundle\Entity\Rule;
use RuleBundle\Service\ResolverService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/product-stickers")
 * Class ProductStickerController
 * @package AdminBundle\Controller\Catalog\Product
 */
class ProductStickerController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'assets' => [
                'admin/js/catalog/product/product-sticker.js',
            ],
            'entity_class' => ProductSticker::class,
            'form_type' => ProductStickerFormType::class,
            'datatable' => [
                'datatable_type' => ProductStickerDatatableType::class,
                'entity_class' => ProductSticker::class,
                'form_type' => ProductStickerFormType::class,
                'actions_processing' => 'override',
                'actions' => [
                    [
                        'icon' => 'mdi mdi-plus',
                        'route' => 'admin_catalog_product_productsticker_addrule',
                        'label' => 'Toevoegen',
                        'className' => 'btn-success',
                        'action' => 'create-rule',
                    ],
                ],
                'item_actions' => [
                    [
                        'icon' => 'mdi mdi-eye',
                        'route' => 'rule_rule_edit',
                        'type' => 'rule_edit',
                        'method' => 'GET',
                        'override_propagation' => true,
                        'identifier' => 'rule.id',
                    ],
                ]
            ]
        ];
    }

    /**
     * @Route("/regel-aanmaken", methods={"GET", "POST"})
     * @Template("@Admin/Rule/create-modal.html.twig")
     * @param Request $request
     * @return array|JsonResponse
     */
    public function addRule(Request $request) {
        $ruleTransformer = $this->get('rule.transformer');

        if($request->getMethod() === 'POST') {
            /** @var EntityManagerInterface $em */
            $em = $this->container->get('doctrine')->getManager();
            /** @var Rule $rule */
            $rule = $this->container->get('rule')->store($this->getRequest(), true);

            $productSticker = new ProductSticker();
            $productSticker->setDescription($rule->getDescription());
            $productSticker->setRule($rule);

            $em->persist($productSticker);
            $em->flush();

            return new JsonResponse(['status' => 'ok']);
        }

        $entity = $this->getDoctrine()->getRepository(Entity::class)->findOneBy([
            'fullyQualifiedName' => Product::class,
        ]);

        $inputs = [
            ResolverService::QB_ALIAS => $entity,
        ];

        return [
            'filters' => $ruleTransformer->getFilters($inputs),
            'entities' => [$entity],
            'persistUrl' => $this->container->get('router')->generate('admin_catalog_product_productsticker_addrule'),
        ];
    }
}