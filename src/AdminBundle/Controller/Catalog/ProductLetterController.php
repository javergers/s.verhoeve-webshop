<?php

namespace AdminBundle\Controller\Catalog;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Catalog\LetterType as ProductDatatableType;
use AdminBundle\Form\Catalog\LetterType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Catalog\Product\Letter;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/catalogus/producten/brief")
 */
class ProductLetterController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Brief toevoegen';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'label' => 'Brief',
            'entity_class' => Letter::class,
            'form_type' => LetterType::class,
            'assets' => [
                'admin/js/form/product.js',
                'admin/js/designer/DesignSelectModal.js',
                'admin/css/form/product.css',
                'admin/css/product/product-item.css',
                'admin/css/designer/DesignSelectModal.css',
            ],
            'title' => function (Letter $product) {
                return $product->getName();
            },
            'datatable' => [
                'datatable_type' => ProductDatatableType::class,
                'entity_class' => Letter::class,
                'form_type' => LetterType::class,
                'query_builder' => function (QueryBuilder $qb) {
                    $qb->andWhere('product.parent IS NULL');
                },
                'paginator' => [
                    'options' => [
                        'wrap-queries' => true,
                    ],
                ],
                'allow_edit' => false,
                'allow_read' => true,
            ],
            'view' => [
                'panels' => [
                    [
                        'id' => 'description-panel',
                        'type' => 'template',
                        'template' => '@Admin/Product/letter-panel.html.twig',
                    ],
                ],
            ],
        ];
    }
}
