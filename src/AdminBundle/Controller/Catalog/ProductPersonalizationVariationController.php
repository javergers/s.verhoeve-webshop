<?php

namespace AdminBundle\Controller\Catalog;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Catalog\ProductDatatableType;
use AdminBundle\Form\Catalog\Product\ProductVariationFormType;
use AppBundle\Entity\Catalog\Product\Personalization;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/catalogus/product-personalization-variaties")
 * Class ProductPersonalizationVariationController
 * @package AdminBundle\Controller\Catalog
 */
class ProductPersonalizationVariationController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'label' => 'Productvariaties',
            'entity_class' => Personalization::class,
            'form_type' => ProductVariationFormType::class,
            'assets' => [
                'admin/js/form/product.js',
                'admin/css/form/product.css',
                'admin/css/',
            ],
            'datatable' => [
                'datatable_type' => ProductDatatableType::class,
                'entity_class' => Personalization::class,
            ],
        ];
    }

}
