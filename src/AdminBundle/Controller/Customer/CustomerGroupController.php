<?php

namespace AdminBundle\Controller\Customer;

use AdminBundle\Controller\CrudController;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/klantgroepen")
 */
class CustomerGroupController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Klantgroepen';
    }

    public function getOptions()
    {
        return [
            'entity_class' => CustomerGroup::class,
        ];
    }
}
