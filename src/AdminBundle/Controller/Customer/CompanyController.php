<?php

namespace AdminBundle\Controller\Customer;

use AdminBundle\Annotation\Breadcrumb;
use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Address\AddressDatatableType;
use AdminBundle\Datatable\App\CompanyType;
use AdminBundle\Datatable\Company\CompanyCustomerType;
use AdminBundle\Datatable\App\CustomerType;
use AdminBundle\Datatable\Company\CompanyCustomFieldDatatableType;
use AdminBundle\Datatable\Company\CompanyEstablishmentType as CompanyEsablishmentDatatableType;
use AdminBundle\Datatable\Discount\CompanyDiscountDatatableType;
use AdminBundle\Datatable\Discount\DiscountDatatableType;
use AdminBundle\Datatable\Company\CompanyReportDatatableType;
use AdminBundle\Datatable\Order\CompanyOrderDatatableType;
use AdminBundle\Form\Company\CompanyReportFormType;
use AdminBundle\Form\App\CustomerType as FormCustomerType;
use AdminBundle\Form\Company\CompanySupplierType;
use AdminBundle\Form\Discount\DiscountFormType;
use AdminBundle\Form\Company\UploadUsersImportType;
use AdminBundle\Form\Company\CompanyCustomFieldFormType;
use AdminBundle\Form\Relation\AddressFormType;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Discount\Voucher;
use AppBundle\Entity\Relation\CompanyConnectorParameter;
use AppBundle\Entity\Relation\CompanyCustomField;
use AppBundle\Form\Company\CompanyEstablishmentType;
use AppBundle\Connector\Bakker;
use AppBundle\Entity\Discount\Discount;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyEstablishment;
use AppBundle\Entity\Relation\CompanyInternalRemark;
use AppBundle\Entity\Relation\CompanyRegistration;
use AppBundle\Entity\Report\CompanyReport;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Entity\Site\Site;
use AppBundle\Entity\Supplier\SupplierGroup;
use AppBundle\Interfaces\PageView\PageViewControllerInterface;
use AppBundle\Manager\Relation\CompanyManager;
use AppBundle\Services\Company\CompanyRegistrationService;
use AppBundle\Services\Discount\VoucherService;
use AppBundle\Services\Relation\CustomerImportService;
use AppBundle\Services\Relation\CustomerImportTemplateService;
use AppBundle\ThirdParty\Exact\Entity\Invoice;
use DateTime;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use PhpOffice\PhpSpreadsheet\Writer\Exception as WriterException;
use ReflectionClass;
use RuleBundle\Entity\Entity;
use RuleBundle\Service\ResolverService;
use RuntimeException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\Validator\Constraints\Email;
use AppBundle\Services\ConnectorHelper;

/**
 * @Route("/klanten/bedrijven")
 */
class CompanyController extends CrudController implements PageViewControllerInterface
{
    /**
     * @param int $n
     * @return string
     */
    public function getLabel($n = null): string
    {
        return 'Bedrijven';
    }

    /**
     * @Route("/login")
     *
     * @param Request $request
     *
     * @return mixed
     * @throws Exception
     */
    public function loginAction(Request $request)
    {
        $company = $this->getDoctrine()->getRepository(Company::class)->find($request->get('id'));

        /** @var Bakker $connector */
        $connector = $this->container->get(ConnectorHelper::class)->getConnector($company);

        $url = $connector->login($company);

        if (!$url) {
            throw new BadRequestHttpException();
        }

        return new RedirectResponse($url);
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'entity_class' => Company::class,
            'datatable' => [
                'entity_class' => Company::class,
                'datatable_type' => CompanyType::class,
                'actions_processing' => 'override',
                'actions' => [
                    [
                        'icon' => 'mdi mdi-plus',
                        'route' => 'admin_customer_company_wizard',
                        'label' => 'Bedrijf toevoegen',
                        'className' => 'btn-success',
                    ],
                ],
                'allow_read' => true,
                'allow_edit' => false,
            ],
            'assets' => [
                'admin/js/form/company.js',
                'admin/js/form/customer.js',
                'admin/css/form/company.css',
                'admin/js/form/product.js',
                'admin/js/company/company-assortment.js',
                'admin/assets/sortable/Sortable.min.js',
                'admin/js/form/assortment.js',
                'admin/js/form/company-establishment.js',
                'admin/css/form/assortment.css',
                'admin/css/internal-remark.css',
                'admin/js/modals/customer_import_modal.js',
                'admin/js/designer/DesignSelectModal.js',
                'admin/css/designer/DesignSelectModal.css',
            ],
            'view' => [
                'label_subject' => '%%name%%',
                'panels' => [
                    [
                        'id' => 'information_panel',
                        'type' => 'template',
                        'template' => '@Admin/Company/information-panel.html.twig',
                    ],
                    [
                        'id' => 'tab_panel',
                        'type' => 'tabs',
                        'tabs' => [
                            [
                                'action' => 'companyDetails',
                                'label' => 'Dashboard',
                                'type' => 'template',
                                'template' => [
                                    'template' => '@Admin/Company/companyDetailsTab.html.twig',
                                ],
                            ],
                            [
                                'action' => 'companyDepartments',
                                'label' => 'Vestigingen',
                                'datatable' => [
                                    'datatable_type' => CompanyEsablishmentDatatableType::class,
                                    'form_type' => CompanyEstablishmentType::class,
                                    'entity_class' => CompanyEstablishment::class,
                                    'base_route' => 'admin_company_companyestablishment',
                                    'refresh_panel' => 'tab_panel',
                                ],
                            ],
                            [
                                'action' => 'orders',
                                'label' => 'Bestellingen',
                                'datatable' => [
                                    'alias' => 'o',
                                    'datatable_type' => CompanyOrderDatatableType::class,
                                    'entity_class' => Order::class,
                                    'allow_add' => false,
                                    'allow_delete' => false,
                                    'allow_edit' => false,
                                    'allow_read' => false,
                                    'query_builder' => function (QueryBuilder $qb) {
                                        return $qb->orderBy('o.id', 'DESC');
                                    },
                                    'parent_column' => 'orderCollection.company',
                                    'item_actions' => [
                                        [
                                            'icon' => 'mdi mdi-eye',
                                            'route' => 'admin_sales_order_order',
                                            'type' => 'get-order',
                                            'method' => 'GET',
                                            'identifier' => [
                                                'field' => 'orderCollection',
                                                'id' => 'orderCollection.id',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                'action' => 'users',
                                'label' => 'Gebruikers',
                                'datatable' => [
                                    'datatable_type' => CustomerType::class,
                                    'form_type' => FormCustomerType::class,
                                    'entity_class' => Customer::class,
                                    'base_route' => 'admin_customer_customer',
                                    'allow_read' => true,
                                    'allow_edit' => false,
                                    'refresh_panel' => 'users',
                                    'actions' => [
                                        [
                                            'url' => function (Company $company) {
                                                return $this->generateUrl('admin_customer_company_downloadcustomerimporttemplate',
                                                    ['id' => $company->getId()]);
                                            },
                                            'label' => 'Download template',
                                            'className' => 'btn-success',
                                            'type' => 'link-user-import',
                                        ],

                                        [
                                            'url' => function (Company $company) {
                                                return $this->generateUrl('admin_customer_company_importcustomers',
                                                    ['id' => $company->getId()]
                                                );
                                            },
                                            'label' => 'Importeer contacten',
                                            'className' => 'btn-success',
                                            'type' => 'import-users',
                                        ],
                                    ],
                                ],
                            ],
                            [
                                'action' => 'addresses',
                                'label' => 'Bezorgadressen',
                                'datatable' => [
                                    'alias' => 'addr',
                                    'parent_column' => 'company.id',
                                    'datatable_type' => AddressDatatableType::class,
                                    'form_type' => AddressFormType::class,
                                    'entity_class' => Address::class,
                                    'base_route' => 'admin_relation_address',
                                    'refresh_panel' => 'tab_panel',
                                    'query_builder' => function (QueryBuilder $qb, Request $request) {
                                        return $qb->andWhere('addr.delivery_address = 1');
                                    },
                                ],
                            ],
                            [
                                'action' => 'financialDetails',
                                'label' => 'Financieel',
                                'type' => 'template',
                                'template' => [
                                    'template' => '@Admin/Company/companyFinancialTab.html.twig',
                                ],
                            ],
                            [
                                'action' => 'catalog',
                                'label' => 'Catalogus',
                                'type' => 'template',
                                'template' => [
                                    'template' => '@Admin/Company/CompanyAssortment/index.html.twig',
                                ],
                            ],
                            [
                                'action' => 'customFieldsDetails',
                                'label' => 'Eigen velden',
                                'datatable' => [
                                    'datatable_type' => CompanyCustomFieldDatatableType::class,
                                    'form_type' => CompanyCustomFieldFormType::class,
                                    'entity_class' => CompanyCustomField::class,
                                    'base_route' => 'admin_company_companycustomfield',
                                    'refresh_panel' => 'tab_panel',
                                    'paginator' => [
                                        'options' => [
                                            'wrap-queries' => true,
                                        ],
                                    ],
                                ],
                            ],
                            [
                                'action' => 'discounts',
                                'label' => 'Kortingen',
                                'datatable' => [
                                    'alias' => 'addr',
                                    'parent_column' => 'companies.id',
                                    'datatable_type' => CompanyDiscountDatatableType::class,
                                    'form_type' => DiscountFormType::class,
                                    'entity_class' => Discount::class,
                                    'base_route' => 'admin_discount_discount',
                                    'refresh_panel' => 'tab_panel',
                                    'actions_processing' => 'override',
                                    'disable_search' => true,
                                    'actions' => [
                                        [
                                            'icon' => 'mdi mdi-plus',
                                            'route' => 'admin_customer_company_adddiscount',
                                            'label' => 'Korting toevoegen',
                                            'className' => 'btn-success',
                                            'action' => 'create-discount-rule',
                                        ],
                                        [
                                            'icon' => 'mdi mdi-plus',
                                            'route' => 'admin_customer_company_existingruleadddiscount',
                                            'label' => 'Bestaande voorwaarden gebruiken',
                                            'className' => 'btn-success',
                                            'action' => 'create-discount-existing-rule'
                                        ],
                                    ],
                                    'allow_edit' => false,
                                    'item_actions' => [
                                        [
                                            'icon' => 'mdi mdi-pencil',
                                            'route' => 'admin_discount_discount_edit',
                                            'type' => 'open-content',
                                            'method' => 'GET',
                                            'identifier' => [
                                                'field' => 'id'
                                            ]
                                        ],
                                        [
                                            'icon' => 'mdi mdi-eye',
                                            'route' => 'rule_rule_edit',
                                            'type' => 'rule_edit',
                                            'identifier' => 'rule.id',
                                        ],
                                    ]
                                ],
                            ],
                            [
                                'action' => 'reports',
                                'label' => 'Rapportages',
                                'datatable' => [
                                    'datatable_type' => CompanyReportDatatableType::class,
                                    'form_type' => CompanyReportFormType::class,
                                    'entity_class' => CompanyReport::class,
                                    'base_route' => 'admin_company_companyreport',
                                    'actions_processing' => 'override',
                                    'allow_edit' => false,
                                    'actions' => [
                                        [
                                            'route' => 'admin_company_companyreport_link',
                                            'label' => 'Bestaand profiel koppelen',
                                            'className' => 'btn-success',
                                            'type' => 'link-business-report',
                                        ],
                                        [
                                            'icon' => 'mdi mdi-plus',
                                            'route' => 'admin_company_companyreport_create',
                                            'label' => 'Profiel toevoegen',
                                            'className' => 'btn-success',
                                            'type' => 'create-business-report',
                                        ],
                                    ],
                                    'item_actions' => [
                                        [
                                            'icon' => 'mdi mdi-download',
                                            'route' => 'admin_company_companyreport_download',
                                            'type' => 'download',
                                            'method' => 'GET',
                                            'identifier' => [
                                                'field' => 'companyReport'
                                            ]
                                        ],
                                        [
                                            'icon' => 'fa fa-users',
                                            'route' => 'admin_company_companyreport_customer',
                                            'type' => 'open-content',
                                            'method' => 'GET',
                                            'identifier' => [
                                                'field' => 'companyReport',
                                            ]
                                        ],
                                        [
                                            'icon' => 'mdi mdi-pencil',
                                            'route' => 'admin_company_companyreport_edit',
                                            'type' => 'open-content',
                                            'method' => 'GET',
                                        ],
                                    ],
                                    'paginator' => [
                                        'options' => [
                                            'wrap-queries' => true,
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param Company $company
     * @param array   $options
     * @return Response
     */
    public function catalog(Company $company, array $options)
    {
        /** @var EntityRepository $repo */
        $repo = $this->container->get('doctrine')->getManager()->getRepository(ProductCard::class);
        /** @var QueryBuilder $qb */
        $qb = $repo->createQueryBuilder('card');
        $qb->andWhere('card.parent is null');

        $cards = [];
        /** @var ProductCard $card */
        foreach ($qb->getQuery()->getResult() as $card) {
            $cards[] = [
                'id' => $card->getId(),
                'title' => $card->translate()->getTitle(),
            ];
        }

        return $this->render($options['template']['template'], [
            'company' => $company,
            'cards' => $cards
        ]);
    }

    /**
     * @param Company $company
     * @param array   $options
     *
     * @return Response
     */
    public function companyDetails(Company $company, array $options): Response
    {
        return $this->render($options['template']['template'], [
            'company' => $company,
            'totalRevenue' => $this->get(CompanyManager::class)->getTotalRevenue($company),
        ]);
    }

    /**
     * @param Company $company
     * @param array   $options
     *
     * @return Response
     */
    public function financialDetails(Company $company, array $options): Response
    {
        $invoiceRepository = $this->get('doctrine')->getRepository(Invoice::class);
        return $this->render($options['template']['template'], [
            'company' => $company,
            'exportedInvoices' => $invoiceRepository->getExportedInvoiceCountForCompany($company),
        ]);
    }

    /**
     * @param Company $company
     * @param array   $options
     *
     * @return Response
     */
    public function getPrivacyDetails(Company $company, array $options): Response
    {
        return $this->render($options['template']['template'], [
            'company' => $company,
        ]);
    }

    /**
     * @param Company $company
     * @param array   $options
     *
     * @return Response
     */
    public function getOciDetails(Company $company, array $options): Response
    {
        return $this->render($options['template']['template'], [
            'company' => $company,
        ]);
    }

    /**
     * @Route("/wizard", methods={"GET", "POST"})
     * @Breadcrumb()
     * @Template()
     *
     * @param Request $request
     *
     * @throws \Exception
     * @return array|Response
     */
    public function wizardAction(Request $request)
    {
        $countries = Intl::getRegionBundle()->getCountryNames('nl');
        $customerGroups = $this->getDoctrine()->getRepository(CustomerGroup::class)->findAll();
        $supplierGroups = $this->getDoctrine()->getRepository(SupplierGroup::class)->findAll();

        unset ($countries['NL'], $countries['BE']);

        $sites = $this->getDoctrine()->getManager()->getRepository(Site::class)->findAll();

        if ($request->isMethod('POST')) {
            $errors = [];
            $data = null;

            $emailConstraint = new Email();

            $missingOrInvalidDate = (!$request->request->get('country') || (!$request->request->get('kvk') && !$request->request->get('btw')) || !$request->request->get('customer') || (empty($request->request->get('customer')['gender']) || empty($request->request->get('customer')['firstname']) || empty($request->request->get('customer')['lastname']) || (empty($request->request->get('customer')['email']) || \count($this->get('validator')->validate($request->request->get('customer')['email'],
                            $emailConstraint)) > 0)));

            $usernameAlreadyExists = false;

            if (!$missingOrInvalidDate) {
                $usernameAlreadyExists = $this->getDoctrine()->getRepository(Customer::class)->findOneBy([
                    'username' => $request->request->get('customer')['email'],
                ]);
            }

            if ($missingOrInvalidDate || $usernameAlreadyExists) {
                return [
                    'error' => ['Er is een fout opgetreden, controleer de invoer op mogelijke fouten.'],
                    'countries' => $countries,
                    'sites' => $sites,
                    'customerGroups' => $customerGroups,
                    'supplierGroups' => $supplierGroups,
                    'page_title' => 'Toevoegen',
                ];
            }

            if (strtolower($request->request->get('country')) === 'nl') {
                if (!$this->get('webservices_nl')->isValidChamberOfCommerceNumber($request->request->get('kvk'),
                    $request->request->get('kvk_establishment'))) {
                    $errors[] = 'Ongeldig KvK-nummer';
                } else {
                    $data = $this->get('webservices_nl')->getCompanyDataByChamberOfCommerceNumber($request->request->get('kvk'),
                        $request->request->get('kvk_establishment'));
                }

                //set Company registration to company object
                $companyRegistration = $this->get(CompanyRegistrationService::class)->createCompanyRegistration($request->request->get('country'),
                    $data);
                $company = $this->container->get(CompanyManager::class)->create($companyRegistration);

                $company->setChamberOfCommerceNumber($request->request->get('kvk'));

                $company->setPhoneNumber($data->phonenumber);

                if (!$request->request->get('unregistered_establishment')) {
                    $company->setChamberOfCommerceEstablishmentNumber($data->chamberOfCommerceEstablishment);
                }

                $unregisterEstablisment = $request->request->get('unregistered_establishment');

                if ($unregisterEstablisment || ($data->chamberOfCommerceEstablishment !== $data->chamberOfCommerceMainEstablishment)) {
                    $arguments = [
                        'chamberOfCommerceNumber' => $request->request->get('kvk'),
                    ];

                    if (!$unregisterEstablisment) {
                        $arguments['chamberOfCommerceEstablishmentNumber'] = $data->chamberOfCommerceMainEstablishment;
                    }

                    $parentCompany = $this->getDoctrine()->getRepository(Company::class)->findOneBy($arguments);

                    if (!$parentCompany) {
                        throw new RuntimeException('Maak eerst de hoofdvestiging aan');
                    }

                    $company->setParent($parentCompany);
                }
            } else {
                if (!$this->get('ec_europa_eu.vies')->isValidVatNumber($request->request->get('btw'))) {
                    $errors[] = 'Ongeldig BTW-nummer';
                } else {
                    $data = $this->get('ec_europa_eu.vies')->getCompanyDataByVatNumber($request->request->get('btw'));
                }

                //set Company registration to company object
                $companyRegistration = $this->get(CompanyRegistrationService::class)->createCompanyRegistration($request->request->get('country'),
                    $data);
                $company = $this->container->get(CompanyManager::class)->create($companyRegistration);
                $company->setVatNumber($request->request->get('btw'));
            }

            if (!$request->request->get('is_customer') && !$request->request->get('is_supplier')) {
                $errors[] = 'Geef aan of de organisatie een klant en/of leverancier is.';
            } else {
                if ($request->request->get('is_customer') && !$request->request->get('customer_group')) {
                    $errors[] = 'Geef aan of de organisatie een klant en/of leverancier is.';
                }

                if ($request->request->get('is_supplier') && !$request->request->get('supplier_group')) {
                    $errors[] = 'Geef aan of de organisatie een klant en/of leverancier is.';
                }
            }

            if ($errors) {
                return [
                    'errors' => $errors,
                    'countries' => $countries,
                    'sites' => $sites,
                    'customerGroups' => $customerGroups,
                    'supplierGroups' => $supplierGroups,
                    'page_title' => 'Toevoegen',
                ];
            }

            $countryCode = $data->address->countryCode;
            $country = $this->getDoctrine()->getRepository(Country::class)->find($countryCode);

            if ($country === null) {
                throw new RuntimeException("Country can't be empty");
            }

            if ($request->request->get('unregistered_establishment')) {
                $company->setName($request->request->get('main')['name']);
            } else {
                $company->setName($data->name);
            }

            $company->setTwinfieldRelationNumber($request->request->get('twinfield_relation_number'));
            $company->setVerified((bool)$request->request->get('verified'));
            $company->setEmail($request->request->get('customer')['email']);

            $address = new Address();
            $address->setCompany($company);

            if ($request->request->get('unregistered_establishment')) {
                $address->setStreet($request->request->get('main')['street']);
                $address->setNumber(implode(' ', [
                    $request->request->get('main')['housenumber'],
                    $request->request->get('main')['housenumber_addition'],
                ]));
                $address->setPostcode($request->request->get('main')['postcode']);
                $address->setCity($request->request->get('main')['city']);

            } else {
                $address->setStreet($data->address->street);
                $address->setNumber(implode(' ', [
                    $data->address->houseNumber,
                    $data->address->houseNumberAddition,
                ]));
                $address->setPostcode($data->address->postcode);
                $address->setCity($data->address->city);
            }

            $address->setCountry($country);
            $address->setMainAddress(true);
            $address->setCompanyName($company->getName());
            $address->setDescription('Vestigingsadres');

            if (!empty($data->phoneNumber)) {
                $address->setPhoneNumber($data->phoneNumber);
            }

            if ($request->request->get('unregistered_establishment')) {
                if ($request->request->get('invoice_equals_main')) {
                    $invoiceAddress = clone $address;
                    $invoiceAddress->setMainAddress(false);
                } else {
                    $invoiceAddress = new Address();
                    $invoiceAddress->setStreet($request->request->get('invoice')['street']);
                    $invoiceAddress->setNumber(implode(' ', [
                        $request->request->get('invoice')['housenumber'],
                        $request->request->get('invoice')['housenumber_addition'],
                    ]));
                    $invoiceAddress->setPostcode($request->request->get('invoice')['postcode']);
                    $invoiceAddress->setCity($request->request->get('invoice')['city']);
                }
            } elseif (empty($data->postaddress)) {
                $invoiceAddress = clone $address;
                $invoiceAddress->setMainAddress(false);
            } else {
                $invoiceAddress = new Address();
                $invoiceAddress->setCompany($company);
                $invoiceAddress->setStreet($data->postaddress->street);
                $invoiceAddress->setNumber($data->postaddress->houseNumber);
                $invoiceAddress->setNumberAddition($data->postaddress->houseNumberAddition);

                $invoiceAddress->setPostcode($data->postaddress->postcode);
                $invoiceAddress->setCity($data->postaddress->city);

                if ($data->postaddress->countryCode !== $data->address->countryCode) {
                    $countryCode = $data->postaddress->countryCode;
                    $country = $this->getDoctrine()->getRepository(Country::class)->find($countryCode);
                }

                $invoiceAddress->setCountry($country);

                if (!empty($data->phoneNumber)) {
                    $invoiceAddress->setPhoneNumber($data->phoneNumber);
                }
            }

            $invoiceAddress->setCompanyName($company->getName());
            $invoiceAddress->setInvoiceAddress(true);
            $invoiceAddress->setDescription('Factuuradres');

            $company->setInvoiceAddress($invoiceAddress);

            $customerGroup = $this->getDoctrine()->getRepository(CustomerGroup::class)->findOneBy([
                'description' => 'Zakelijk',
            ]);

            $customer = new Customer();
            $customer->setCustomerGroup($customerGroup);
            $customer->setCompany($company);
            $customer->setEnabled(true);
            $customer->setGender($request->request->get('customer')['gender']);
            $customer->setFirstname($request->request->get('customer')['firstname']);
            $customer->setLastname($request->request->get('customer')['lastname']);
            $customer->setUsername($request->request->get('customer')['email']);
            $customer->setEmail($request->request->get('customer')['email']);
            $customer->setLocale($request->request->get('customer')['locale']);

            if ($request->request->get('customer')['site']) {
                $site = $this->getDoctrine()->getManager()->find(Site::class,
                    $request->request->get('customer')['site']);

                $customer->setRegisteredOnSite($site);
            }

            $customer->setDefaultInvoiceAddress($invoiceAddress);

            $company->addCustomer($customer);
            $company->addAddress($address);
            $company->addAddress($invoiceAddress);
            $company->setDefaultDeliveryAddress($address);
            $company->setIsCustomer(false);
            $company->setIsSupplier(false);

            if (null !== $companyRegistration) {
                $company->setCompanyRegistration($companyRegistration);
                $company->setName($companyRegistration->getName());
            }

            if ($request->request->get('is_customer') && \is_array($request->request->get('customer_group'))) {
                $company->setIsCustomer(true);

                $customerGroupsSelected = $this->getDoctrine()->getRepository(CustomerGroup::class)->findBy([
                    'id' => $request->request->get('customer_group'),
                ]);

                foreach ($customerGroupsSelected as $cgItem) {
                    $company->addCustomerGroup($cgItem);
                }
            }

            if ($request->request->get('is_supplier') && \is_array($request->request->get('supplier_group'))) {
                $company->setIsSupplier(true);

                $supplierGroupsSelected = $this->getDoctrine()->getRepository(SupplierGroup::class)->findBy([
                    'id' => $request->request->get('supplier_group'),
                ]);

                foreach ($supplierGroupsSelected as $sgItem) {
                    $company->addSupplierGroup($sgItem);
                }
            }

            try {
                $this->getDoctrine()->getManager()->persist($company);
                $this->getDoctrine()->getManager()->flush();

                $this->get('session')->getFlashBag()->add('backend.success',
                    'Bedrijf is aangemaakt, maak het account nu compleet.');

                return $this->redirectToRoute('admin_customer_company_view', [
                    'id' => $company->getId(),
                ]);
            } catch (\Exception $e) {
                return [
                    'error' => 'Er is een fout opgetreden bij het aanmaken van het bedrijf',
                    'countries' => $countries,
                    'sites' => $sites,
                    'customerGroups' => $customerGroups,
                    'supplierGroups' => $supplierGroups,
                    'page_title' => 'Toevoegen',
                ];
            }
        }

        return [
            'countries' => $countries,
            'sites' => $sites,
            'customerGroups' => $customerGroups,
            'supplierGroups' => $supplierGroups,
            'page_title' => 'Toevoegen',
        ];
    }

    /**
     * @Route("/wizard/identification", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function identificationAction(Request $request): JsonResponse
    {
        if (!$request->query->has('value') || !$request->query->get('value') || !$request->query->has('type') || (!$request->query->get('type') || ($request->query->get('type') && !\in_array($request->query->get('type'),
                        ['kvk', 'btw'])))) {
            throw new BadRequestHttpException();
        }

        $data = null;

        $value = $request->query->get('value');

        try {
            switch ($request->query->get('type')) {
                case 'kvk':
                    $establishment = $request->get('establishment');

                    /** @var Company $company */
                    if ($establishment) {
                        $companyRegistration = $this->getDoctrine()->getRepository(CompanyRegistration::class)->findOneBy([
                            'registrationNumber' => $value,
                            'establishmentNumber' => $establishment,
                        ]);
                    } else {
                        $companyRegistration = $this->getDoctrine()->getRepository(CompanyRegistration::class)->findOneBy([
                            'vatNumber' => $value,
                        ]);
                    }

                    if (!$this->get('webservices_nl')->isValidChamberOfCommerceNumber($value, $establishment)) {
                        throw new BadRequestHttpException('Er is een ongeldig KVK nummer ingevoerd');
                    }

                    $data = $this->get('webservices_nl')->getCompanyDataByChamberOfCommerceNumber($value,
                        $establishment);

                    if ($companyRegistration) {
                        return new JsonResponse([
                            'error' => true,
                            'exists' => true,
                            'company' => [
                                'name' => $companyRegistration->getName(),
                                'url' => $this->generateUrl('admin_customer_company_view'),
                            ],
                            'identification' => $data,
                        ]);
                    }

                    if ($data->chamberOfCommerceMainEstablishment && $data->chamberOfCommerceEstablishment !== $data->chamberOfCommerceMainEstablishment) {
                        $parentCompanyRegistration = $this->getDoctrine()->getRepository(CompanyRegistration::class)->findOneBy([
                            'registrationNumber' => $value,
                            'establishmentNumber' => $data->chamberOfCommerceMainEstablishment,
                        ]);

                        if (!$parentCompanyRegistration) {
                            return new JsonResponse([
                                'error' => true,
                                'exists' => false,
                                'parentExists' => false,
                            ]);
                        }
                    }

                    break;
                case 'btw':
                    /** @var Company $company */
                    $companyRegistration = $this->getDoctrine()->getRepository(CompanyRegistration::class)->findOneBy([
                        'vatNumber' => $value,
                    ]);

                    if (!$this->get('ec_europa_eu.vies')->isValidVatNumber($value)) {
                        throw new BadRequestHttpException('Er is een ongeldig BTW nummer ingevoerd');
                    }

                    $data = $this->get('ec_europa_eu.vies')->getCompanyDataByVatNumber($value);

                    if ($companyRegistration) {
                        return new JsonResponse([
                            'error' => true,
                            'exists' => true,
                            'company' => [
                                'name' => $companyRegistration->getName(),
                                'url' => $this->generateUrl('admin_customer_company_view'),
                            ],
                            'identification' => $data,
                        ]);
                    }

                    break;
            }

        } catch (HttpExceptionInterface $e) {
            return new JsonResponse($e->getMessage(), $e->getStatusCode());
        } catch (Exception $e) {
            return new JsonResponse($e->getMessage(), 500);
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/wizard/get-address", methods={"GET"})
     * @param Request $request
     *
     * @return JsonResponse
     * @throws GuzzleException
     */
    public function getAddressAction(Request $request): JsonResponse
    {
        if (!$request->get('postcode')) {
            throw new RuntimeException("Missing parameter 'postcode'");
        }

        if (!$request->get('housenumber')) {
            throw new RuntimeException("Missing parameter 'number'");
        }

        if (!$request->get('country')) {
            throw new RuntimeException("Missing parameter 'country'");
        }

        if (strtolower($request->get('country')) !== 'nl') {
            throw new RuntimeException('Invalid request');
        }

        try {
            $searchAddress = 'Placeholder ' . $request->get('housenumber');

            $address = $this->get('address');
            $address->parseAddress($searchAddress);

            $result = $this->get('postcode_nl')->getAddress($request->get('postcode'), $address->getHouseNumber(),
                $request->get('housenumberAddition'));

            if ($address->getHouseNumberAddition()) {
                $result->houseNumber .= $address->getHouseNumberAddition();
            }
        } catch (Exception $e) {
            $result = [];
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/wizard/check-username", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws BadRequestHttpException
     */
    public function checkUsernameAction(Request $request): JsonResponse
    {
        if (!$request->query->get('email')) {
            throw new BadRequestHttpException();
        }

        $return = [];

        $usernameAlreadyExists = $this->getDoctrine()->getRepository(Customer::class)->findOneBy([
            'username' => $request->query->get('email'),
        ]);

        if ($usernameAlreadyExists) {
            $return['error'] = true;
        }

        return new JsonResponse($return);
    }

    /**
     * @Route("/migreer", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws \Exception
     * @return JsonResponse|bool
     */
    public function companyRegistrationRequest(Request $request)
    {
        $doctrine = $this->container->get('doctrine');
        $manager = $doctrine->getManager();
        $company = $manager->find(Company::class, $request->get('company'));
        $companyRegistrationService = $this->container->get(CompanyRegistrationService::class);

        /** @var Address $mainAddress */
        if (null !== ($mainAddress = $company->getMainAddress()) && strtoupper($mainAddress->getCountry()->getCode()) === 'NL') {
            $companyRegistration = $companyRegistrationService->createByRegistrationNumber($company->getChamberOfCommerceNumber());
        } else {
            $companyRegistration = $companyRegistrationService->createByVatNumber($company->getVatNumber());
        }

        if ($companyRegistration) {
            $manager->persist($companyRegistration);
            $manager->flush();

            return new JsonResponse([
                'vat_number' => $company->getVatNumber(),
                'chamber_of_commerce_number' => $company->getChamberOfCommerceNumber(),
                'chamber_of_commerce_establishment_number' => $company->getChamberOfCommerceEstablishmentNumber(),
            ]);
        }

        return false;
    }

    /**
     * @Route("/{company}/leveranciers-instellingen", methods={"GET"})
     * @Template("@Admin/Customer/Company/editSupplierSettings.html.twig")
     * @param Company $company
     *
     * @return array
     */
    public function supplierDetails(Company $company): array
    {
        $form = $this->createForm(CompanySupplierType::class, $company);

        return [
            'company' => $company,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{company}/leveranciers-instellingen", methods={"POST"})
     * @Template("@Admin/Customer/Company/editSupplierSettings.html.twig")
     * @param Company $company
     * @param Request $request
     * @return array
     * @throws Exception
     */
    public function persistGetSupplierDetailsAction(Company $company, Request $request): array
    {
        $form = $this->createForm(CompanySupplierType::class, $company);

        $form->handleRequest($request);
        $company = $form->getData();

        $this->persistSupplierParameters($company);

        $this->container->get('doctrine')->getManager()->flush();

        return [
            'company' => $company,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{company}/interne-opmerking", methods={"GET"})
     * @Template()
     * @param Company $company
     *
     * @return array
     */
    public function addInternalRemarkAction(Company $company): array
    {
        return [
            'company' => $company,
        ];
    }

    /**
     * @Route("/{company}/interne-opmerking", methods={"POST"})
     * @Template("@Admin/Customer/Company/addInternalRemark.html.twig")
     * @param Company $company
     * @param Request $request
     *
     * @return Response|array
     */
    public function persistAddInternalRemarkAction(Company $company, Request $request)
    {
        if ($request->get('internal-remark')) {
            if ($this->container->get('security.token_storage')->getToken() === null) {
                throw new RuntimeException('Security token is empty');
            }

            $companyInternalRemark = new CompanyInternalRemark();
            $companyInternalRemark->setCompany($company);
            $companyInternalRemark->setRemark($request->get('internal-remark'));
            $companyInternalRemark->setUser($this->container->get('security.token_storage')->getToken()->getUser());

            $em = $this->container->get('doctrine')->getManager();

            $em->persist($companyInternalRemark);
            $em->flush();

            return new Response('');
        }

        return [
            'company' => $company,
        ];
    }

    /**
     * @Route("/opmerkingen/{companyInternalRemark}/verwijderen", methods={"DELETE"})
     * @param CompanyInternalRemark $companyInternalRemark
     * @return JsonResponse
     * @throws \ReflectionException
     */
    public function deleteInternalRemark(CompanyInternalRemark $companyInternalRemark): JsonResponse
    {
        $roleManager = $this->container->get('cms.role_manager');

        if (!$roleManager->userIsGrantedTo($roleManager->getRolesFor($companyInternalRemark->getCompany(), 'update'))) {
            throw new AccessDeniedException('Je bent niet bevoegd om deze actie uit te voeren');
        }

        $em = $this->container->get('doctrine')->getManager();

        $em->remove($companyInternalRemark);
        $em->flush();

        return new JsonResponse('ok');
    }

    /**
     * @return array
     */
    public function getPageviewSettings(): array
    {
        return [
            'route' => 'admin_customer_company_view',
            'entity' => Company::class,
            'param' => 'id',
        ];
    }

    /**
     * @param $entity
     * @throws Exception
     */
    protected function persistSupplierParameters($entity)
    {
        /** @var Company $company */
        $company = $entity;

        $request = $this->container->get('request_stack')->getCurrentRequest();

        if ($request === null) {
            throw new RuntimeException('Request is null');
        }

        /** @var Form $form */
        $form = $this->createForm(CompanySupplierType::class, $company);

        /** @var FormInterface $connectorForms */
        $connectorForms = $form->get('company_suppliers_container');

        $parameters = new ArrayCollection($this->getDoctrine()->getRepository(CompanyConnectorParameter::class)->findBy([
            'company' => $entity,
        ]));

        $connectors = $this->container->get(ConnectorHelper::class)->getConnectors();

        foreach ($connectors as $connector) {
            $reflectionClass = new ReflectionClass($connector);

            if ($company->getSupplierConnector() !== $reflectionClass->getShortName()) {
                continue;
            }

            $connectorFormName = 'company_suppliers_connector_' . strtolower($reflectionClass->getShortName()) . '_column';

            if (!$connectorForms->has($connectorFormName)) {
                continue;
            }

            foreach ($connectorForms->get($connectorFormName)->all() as $forms) {
                /** @var FormInterface $forms */
                foreach ($forms->all() as $form) {
                    $parameter = $parameters->filter(function (CompanyConnectorParameter $companyConnectorParameter) use
                    (
                        $reflectionClass,
                        $form
                    ) {
                        if ($companyConnectorParameter->getConnector() !== $reflectionClass->getShortName()) {
                            return false;
                        }

                        if ($companyConnectorParameter->getName() !== $form->getName()) {
                            return false;
                        }

                        return true;
                    })->current();

                    $data = null;

                    $connectorForm = $request->get('company_supplier')['company_suppliers_container'][$connectorFormName];

                    if (!empty($connectorForm[$reflectionClass->getShortName()][$form->getName()])) {
                        $data = $connectorForm[$reflectionClass->getShortName()][$form->getName()];

                        switch (\get_class($form->getConfig()->getType()->getInnerType())) {
                            case CheckboxType::class:
                                $data = (bool)$data;
                                break;
                        }
                    }

                    $form->submit($data);

//                    if (!$form->isValid()) {
//                        foreach ($form->getErrors(true, true) as $formError) {
//                            print $formError->getMessage() ."<br />";
//                        }
//                    }

                    if (!$parameter) {
                        $parameter = new CompanyConnectorParameter();
                        $parameter->setCompany($company);
                        $parameter->setConnector($reflectionClass->getShortName());
                        $parameter->setName($form->getName());

                        $this->getDoctrine()->getManager()->persist($parameter);
                    }

                    $parameter->setValue($form->getData());
                }
            }
        }
    }

    /**
     * @Route("/{id}/contacten-import", requirements={"id"="\d+"}), methods={"GET", "POST"})
     * @Template()
     * @Breadcrumb()
     *
     * @param Request $request
     * @param Company $company
     * @return Response|array
     */
    public function importCustomers(Request $request, Company $company)
    {
        $errors = [];

        $form = $this->createForm(UploadUsersImportType::class, $this->getEntity(), [
            'action' => $this->generateUrl('admin_customer_company_importcustomers', [
                'id' => $company->getId(),
            ]),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $file = $form
                ->get('upload_users_container')
                ->get('upload_users_column')
                ->get('userTemplate')
                ->getData();

            try {
                $customerImport = $this->get(CustomerImportService::class);
                $customerImport->processFile($file, $company);

                return new Response();
            } catch (Exception $exception) {
                $errors[] = $exception->getMessage();
            }
        } else {
            $error[] = 'Fout tijdens het verwerken van het formulier.';
        }

        return [
            'form' => $form->createView(),
            'errors' => $errors,
        ];
    }

    /**
     * @Route("/{id}/contacten-import/download-template", requirements={"id"="\d+"}, methods={"GET"})
     * @Breadcrumb()
     *
     * @return BinaryFileResponse
     *
     * @throws Exception
     * @throws WriterException
     */
    public function downloadCustomerImportTemplate(): BinaryFileResponse
    {
        return $this->get(CustomerImportTemplateService::class)->downloadCustomerImportTemplate();
    }

    /**
     * @Route("/korting-toevoegen", methods={"POST"})
     * @Template()
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function addDiscount(Request $request)
    {
        $start = $request->get('start');

        $discount = new Discount();
        $discount->setType($request->get('type'));
        $discount->setValue($request->get('value'));

        $company = $this->getDoctrine()->getRepository(Company::class)->find($request->get('company'));
        $discount->addCompany($company);

        $voucher = new Voucher();
        $voucher->setValidFrom(new DateTime());

        if ($request->get('date')) {
            $voucher->setValidUntil(new DateTime($request->get('date')));
        }

        $voucher->setCode($this->get(VoucherService::class)->generateUniqueVoucherCode());
        $voucher->setDiscount($discount);
        $voucher->setDescription($request->get('name'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($voucher);
        $em->persist($discount);
        $em->flush();

        return new JsonResponse([
            'url' => $this->generateUrl('admin_discount_discount_addrule',
                ['discount' => $discount->getId(), 'start' => $start]),
            'cancelUrl' => $this->generateUrl('admin_discount_discount_delete', ['id' => $discount->getId()])
        ]);
    }

    /**
     * @Route("/korting-toevoegen/bestaande-voorwaarden", methods={"GET", "POST"})
     * @Template("@Admin/Crud/form_xhr.html.twig")
     * @param Request $request
     *
     * @return array
     * @throws Exception
     */
    public function existingRuleAddDiscount(Request $request): array
    {
        /** @var Company $company */
        $company = $this->getDoctrine()->getRepository(Company::class)->find($request->get('company'));
        $form = $this->createForm(DiscountFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Discount $discount */
            $discount = $form->getData();
            $discount->addCompany($company);

            $voucher = new Voucher();
            $voucher->setValidFrom(new DateTime());

            $voucher->setCode($this->get(VoucherService::class)->generateUniqueVoucherCode());
            $voucher->setDiscount($discount);
            $voucher->setDescription($discount->getDescription());

            $em = $this->getDoctrine()->getManager();
            $em->persist($discount);
            $em->persist($voucher);
            $em->flush();
        }

        return [
            'form' => $form->createView()
        ];
    }
}
