<?php

namespace AdminBundle\Controller\Customer;

use AdminBundle\Components\Crud\CrudActionResponse;
use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Address\AddressDatatableType;
use AdminBundle\Datatable\App\CustomerType;
use AdminBundle\Datatable\Order\CustomerOrderDatatableType;
use AdminBundle\Form\App\CustomerType as CustomerFormType;
use AdminBundle\Form\Relation\AddressFormType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Entity\Security\Customer\CustomerInternalRemark;
use AppBundle\Entity\Security\Employee\User;
use AppBundle\Entity\Site\Site;
use AppBundle\Interfaces\PageView\PageViewControllerInterface;
use AppBundle\Manager\CustomerManager;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use JMS\JobQueueBundle\Entity\Job;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;

/**
 * @Route("/klanten")
 */
class CustomerController extends CrudController implements CrudControllerInterface, PageViewControllerInterface
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null): string
    {
        return 'Contacten';
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'entity_class' => Customer::class,
            'form_type' => CustomerFormType::class,
            'assets' => [
                'admin/js/form/company.js',
                'admin/js/form/customer.js',
                'admin/css/form/company.css',
                'admin/js/form/product.js',
                'admin/js/company/company-assortment.js',
                'admin/assets/sortable/Sortable.min.js',
                'admin/js/form/assortment.js',
                'admin/js/form/company-establishment.js',
                'admin/css/form/assortment.css',
                'admin/css/internal-remark.css',
            ],
            'datatable' => [
                'entity_class' => Customer::class,
                'datatable_type' => CustomerType::class,
                'form_type' => CustomerFormType::class,
                'bulk_actions' => [
                    [
                        'route' => 'admin_customer_customer_converttocustomer',
                        'label' => 'Converteer naar klant-account',
                        'icon' => 'icon-users2',
                    ],
                    [
                        'route' => 'admin_customer_customer_sendpasswordresetmail',
                        'label' => 'Stuur wachtwoord vergeten e-mail',
                        'icon' => 'icon-users2',
                    ],
                ],
                'allow_read' => true,
                'allow_edit' => false,
            ],
            'view' => [
                'label_subject' => '%%fullname%%',
                'panels' => [
                    [
                        'id' => 'information_panel',
                        'type' => 'template',
                        'template' => '@Admin/Customer/information-panel.html.twig',
                    ],
                    [
                        'id' => 'customer_tabs',
                        'type' => 'tabs',
                        'tabs' => [
                            [
                                'action' => 'customerDetails',
                                'label' => 'Klantgegevens',
                                'type' => 'template',
                                'template' => [
                                    'template' => '@Admin/Customer/Customer/customerDetailsTab.html.twig',
                                ],
                            ],
                            [
                                'action' => 'orders',
                                'label' => 'Bestellingen',
                                'datatable' => [
                                    'alias' => 'o',
                                    'datatable_type' => CustomerOrderDatatableType::class,
                                    'entity_class' => Order::class,
                                    'allow_add' => false,
                                    'allow_delete' => false,
                                    'allow_edit' => false,
                                    'allow_read' => false,
                                    'query_builder' => function (QueryBuilder $qb) {
                                        return $qb->orderBy('o.id', 'DESC');
                                    },
                                    'parent_column' => 'orderCollection.customer',
                                    'item_actions' => [
                                        [
                                            'icon' => 'mdi mdi-eye',
                                            'route' => 'admin_sales_order_order',
                                            'type' => 'get-order',
                                            'method' => 'GET',
                                            'identifier' => [
                                                'field' => 'orderCollection',
                                                'id' => 'orderCollection.id',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                'action' => 'addresses',
                                'label' => 'Adresboek',
                                'datatable' => [
                                    'datatable_type' => AddressDatatableType::class,
                                    'form_type' => AddressFormType::class,
                                    'entity_class' => Address::class,
                                    'base_route' => 'admin_relation_address',
                                    'refresh_panel' => 'customer_tabs',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @Route("/convert-to-customer", methods={"POST"})
     *
     * @param Request $request
     *
     * @return CrudActionResponse
     */
    public function convertToCustomerAction(Request $request)
    {
        $ids = $request->request->get('ids');

        try {
            $skippedCustomers = [];

            if (!$ids) {
                throw new MissingMandatoryParametersException('Missing ids in POST request');
            }

            $customers = $this->get('doctrine')->getManager()->getRepository(Customer::class)->findBy([
                'id' => $ids,
            ]);

            /** @var Customer $customer */
            $jobManager = $this->container->get('job.manager');
            foreach ($customers as $customer) {
                if ($customer->getUsername()) {
                    $skippedCustomers[$customer->getId()] = $customer->getFullname();

                    continue;
                }

                $customer->setUsername($customer->getEmail());
                $customer->setUsernameCanonical($customer->getEmailCanonical());

                /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
                $tokenGenerator = $this->get('fos_user.util.token_generator');

                $customer->setConfirmationToken($tokenGenerator->generateToken());

                $primaryDomain = $customer->getRegisteredOnSite()->getPrimaryDomain();

                $job = new Job('topbloemen:site:send-password-reset-mail', [
                    $customer->getId(),
                    $primaryDomain->getId(),
                ], true, 'reset_password');

                $job->addRelatedEntity($customer);
                $job->addRelatedEntity($primaryDomain);

                $jobManager->addJob($job);

                $customer->setPasswordRequestedAt(new \DateTime());

                $this->getDoctrine()->getManager()->persist($customer);
            }
            $this->getDoctrine()->getManager()->flush();

            $disable_timer = null;
            $message_type = 'flash';
            $type = 'success';
            $title = 'Actie is succesvol uitgevoerd';
            $text = 'Klant(en) succesvol geconverteerd naar account.';

            if (!empty($skippedCustomers)) {
                $disable_timer = true;
                $message_type = 'modal';
                $type = 'warning';

                if (\count($skippedCustomers) !== \count($customers)) {
                    $text .= '<br /><br />De volgende klanten hebben reeds een account en zijn overgeslagen:<br /><br />';

                    foreach ($skippedCustomers as $skippedCustomer) {
                        $text .= $skippedCustomer . '<br />';
                    }
                } else {
                    $type = 'error';
                    $title = 'Er is geen actie ondernomen';
                    $text = 'Alle geselecteerde klanten hebben reeds een account.';
                }
            }
        } catch (\Exception $e) {
            return new CrudActionResponse([
                'message' => [
                    'type' => 'error',
                    'text' => 'Er is een fout opgetreden tijdens het verwerken van het verzoek',
                ],
            ]);
        }

        return new CrudActionResponse([
            'message' => [
                'disable_timer' => $disable_timer,
                'message_type' => $message_type,
                'title' => $title,
                'type' => $type,
                'text' => $text,
            ],
        ]);
    }

    /**
     * @Route("/send-password-reset-mail", methods={"POST"})
     *
     * @param Request $request
     *
     * @return CrudActionResponse
     */
    public function sendPasswordResetMailAction(Request $request)
    {
        $ids = $request->request->get('ids');

        try {
            if (!$ids) {
                throw new MissingMandatoryParametersException('Missing ids in POST request');
            }

            $customers = $this->get('doctrine')->getManager()->getRepository(Customer::class)->findBy([
                'id' => $ids,
            ]);

            /** @var Customer $customer */
            $jobManager = $this->container->get('job.manager');
            foreach ($customers as $customer) {
                /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
                $tokenGenerator = $this->get('fos_user.util.token_generator');

                $customer->setConfirmationToken($tokenGenerator->generateToken());

                $primaryDomain = $customer->getRegisteredOnSite()->getPrimaryDomain();

                $job = new Job('topbloemen:site:send-password-reset-mail', [
                    $customer->getId(),
                    $primaryDomain->getId(),
                ], true, "reset_password");

                $job->addRelatedEntity($customer);
                $job->addRelatedEntity($primaryDomain);

                $jobManager->addJob($job);

                $customer->setPasswordRequestedAt(new \DateTime());

                $this->getDoctrine()->getManager()->persist($customer);
            }
            $this->getDoctrine()->getManager()->flush();

            $disable_timer = null;
            $message_type = 'flash';
            $type = 'success';
            $title = 'Actie is succesvol uitgevoerd';
            $text = 'Klant(en) ontvangen de wachtwoord vergeten e-mail.';
        } catch (\Exception $e) {
            return new CrudActionResponse([
                'message' => [
                    'type' => 'error',
                    'text' => 'Er is een fout opgetreden tijdens het verwerken van het verzoek',
                ],
            ]);
        }

        return new CrudActionResponse([
            'message' => [
                'disable_timer' => $disable_timer,
                'message_type' => $message_type,
                'title' => $title,
                'type' => $type,
                'text' => $text,
            ],
        ]);
    }

    /**
     * @Route("/login-as-customer/{customer}/{site}", methods={"GET", "POST"})
     * @param Customer $customer
     * @param Site     $site
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function loginAsCustomerAction(Customer $customer, Site $site)
    {
        /* todo: assign on user rights */
        /** @var User $user */
//        $user = $this->getUser();
//        $user->hasGroup("Beheerders");

        return $this->redirect($this->container->get(CustomerManager::class)->login($customer, $site));

    }

    /**
     * @Route("/login-as-customer-sites/{customer}", methods={"GET"})
     *
     * @param Request  $request
     * @param Customer $customer
     *
     * @return JsonResponse
     * @throws BadRequestHttpException
     */
    public function getSitesAction(Request $request, Customer $customer)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException();
        }

        $groups = new ArrayCollection();

        if ($customer->getCustomerGroup()) {
            $groups->add($customer->getCustomerGroup());
        }

        if ($customer->getCompany()) {
            foreach ($customer->getCompany()->getCustomerGroups() as $customerGroup) {
                if ($groups->contains($customerGroup)) {
                    continue;
                }

                $groups->add($customerGroup);
            }
        }

        $sites = $this->get('doctrine')->getRepository(Site::class)->findBy(['parent' => null]);

        /** @var array|Site[] $sites */
        $sites = array_filter($sites, function (Site $site) use ($groups) {
            return $site->getCustomerGroups()->isEmpty() || !$site->getCustomerGroups()->filter(function (
                    CustomerGroup $customerGroup
                ) use ($groups) {
                    return $groups->contains($customerGroup);
                })->isEmpty();
        });


        $result = [];

        foreach ($sites as $site) {
            $result[$site->getId()] = $site->translate(true)->getDescription();
        }

        return new JsonResponse($result);
    }

    /**
     * @Route("/{customer}/interne-opmerking", methods={"GET"})
     * @Template()
     * @param Customer $customer
     *
     * @return array
     */
    public function addInternalRemarkAction(Customer $customer)
    {
        return [
            'customer' => $customer,
        ];
    }

    /**
     * @Route("/{customer}/interne-opmerking", methods={"POST"})
     * @Template("@Admin/Customer/Customer/addInternalRemark.html.twig")
     * @param Customer $customer
     * @param Request  $request
     *
     * @return Response|array
     */
    public function persistAddInternalRemarkAction(Customer $customer, Request $request)
    {
        if ($request->get('internal-remark')) {
            $customerInternalRemark = new CustomerInternalRemark();
            $customerInternalRemark->setCustomer($customer);
            $customerInternalRemark->setRemark($request->get('internal-remark'));
            $customerInternalRemark->setUser($this->container->get('security.token_storage')->getToken()->getUser());
            $em = $this->container->get('doctrine')->getManager();

            $em->persist($customerInternalRemark);
            $em->flush();

            return new Response('');
        }

        return [
            'customer' => $customer,
        ];
    }

    /**
     * @Route("/opmerking/{customerInternalRemark}/verwijderen", methods={"DELETE"})
     * @param CustomerInternalRemark $customerInternalRemark
     * @param Request                $request
     * @return JsonResponse
     */
    public function deleteInternalRemark(CustomerInternalRemark $customerInternalRemark, Request $request): JsonResponse
    {
        $em = $this->container->get('doctrine')->getManager();

        $em->remove($customerInternalRemark);
        $em->flush();

        return new JsonResponse('ok');
    }

    /**
     *
     * @param Customer $customer
     * @param array    $options
     *
     * @return Response
     */
    public function getCustomerDetails(Customer $customer, array $options)
    {
        return $this->render($options['template']['template'], [
            'customer' => $customer,
        ]);
    }

    /**
     * @return array
     */
    public function getPageviewSettings()
    {
        return [
            'route' => 'admin_customer_customer_view',
            'entity' => Customer::class,
            'param' => 'id',
        ];
    }
}
