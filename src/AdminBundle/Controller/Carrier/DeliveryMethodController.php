<?php

namespace AdminBundle\Controller\Carrier;

use AdminBundle\Annotation\Breadcrumb;
use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Carrier\DeliveryMethodDatatableType;
use AdminBundle\Form\Carrier\DeliveryMethodFormType;
use AppBundle\Entity\Carrier\Carrier;
use AppBundle\Entity\Carrier\DeliveryMethod;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * @Route("/leveringswijzen")
 * Class DeliveryMethodController
 * @package AdminBundle\Controller\Carrier
 */
class DeliveryMethodController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'entity_class' => DeliveryMethod::class,
            'form_type' => DeliveryMethodFormType::class,
            'datatable' => [
                'entity_class' => DeliveryMethod::class,
                'form_type' => DeliveryMethodFormType::class,
                'datatable_type' => DeliveryMethodDatatableType::class,
                'allow_read' => false,
                'allow_edit' => true,
                'allow_delete' => true,
            ],
        ];
    }

    /**
     * @Route("/toevoegen-aan-transporteur/{carrier}", methods={"GET", "POST"})
     * @Breadcrumb()
     *
     * @param Carrier         $carrier
     * @param Request         $request
     * @param RouterInterface $router
     * @return Response
     * @throws \Exception
     */
    public function addToCarrier(Carrier $carrier, Request $request, RouterInterface $router)
    {
        $this->denyAccessUnlessGranted($this->getRolesFor('create'));
        if($request->getMethod() === 'GET'){
            $deliveryMethod = $this->getEntity();
            $deliveryMethod->addCarrier($carrier);
            return $this->form($deliveryMethod);
        }

        $response = $this->newPersistAction();
        if($response instanceof RedirectResponse && $response->getTargetUrl() !== $this->getRedirectUrl()) {
            return $response;
        }
        return new RedirectResponse($router->generate('admin_carrier_carrier_view', [
            'id' => $carrier->getId(),
        ]));
    }

}
