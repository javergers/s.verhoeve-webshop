<?php

namespace AdminBundle\Controller\Authentication;

use AdminBundle\Controller\CrudController;
use AdminBundle\Interfaces\CrudControllerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/authenticatie/ip-whitelist")
 * @author Robbert van Mourik
 *
 */
class IpWhitelistController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'IP-whitelist';
    }

    public function getOptions()
    {
        return [
            'entity_class' => 'AdminBundle\Entity\IpWhitelist',
            'form_type' => 'AdminBundle\Form\Authentication\IpWhitelistType',
            'results_per_page' => 50,
            'roles' => [
                'list' => ['ROLE_VIEW'],
                'form' => ['ROLE_MODIFY'],
                'delete' => ['ROLE_MODIFY'],
            ],

            'datatable' => [
                'entity_class' => 'AdminBundle\Entity\IpWhitelist',
                'form_type' => 'AdminBundle\Form\Authentication\IpWhitelistType',
                'datatable_type' => 'AdminBundle\Datatable\Authentication\IpWhitelistType',
            ],
        ];
    }
}
