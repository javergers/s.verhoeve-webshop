<?php

namespace AdminBundle\Controller\Authentication;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Authentication\GroupType as GroupTypeDatatable;
use AdminBundle\Form\Authentication\GroupType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Security\Employee\Group;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/authenticatie/groepen")
 * @author Robbert van Mourik
 *
 */
class GroupController extends CrudController implements CrudControllerInterface
{
    /**
     * @param string $n
     * @return string
     */
    public function getLabel($n = null): string
    {
        return 'Groepen';
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'entity_class' => Group::class,
            'form_type' => GroupType::class,
            'roles' => [
                'list' => ['ROLE_VIEW'],
                'form' => ['ROLE_MODIFY'],
                'delete' => ['ROLE_MODIFY'],
            ],
            'datatable' => [
                'entity_class' => Group::class,
                'datatable_type' => GroupTypeDatatable::class,
                'form_type' => GroupType::class,
                'results_per_page' => 50,
            ],
        ];
    }

    /**
     * @Route("/privileges", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getRolesForGroupAction(Request $request): JsonResponse
    {
        if ($request->request->has('groups') && \count($request->request->get('groups')) === 0) {
            return new JsonResponse();
        }

        /** @var EntityRepository $groupRepository */
        $groupRepository = $this->getDoctrine()->getRepository(Group::class);

        /** @var Group $groups */
        $groups = $groupRepository->findById($request->request->get('groups'));

        $results = [];

        /** @var Group $group */
        foreach ($groups as $group) {
            foreach ($group->getRoles() as $role) {
                $results[$group->getName()][] = $role;
            }
        }


        return new JsonResponse($results);
    }
}
