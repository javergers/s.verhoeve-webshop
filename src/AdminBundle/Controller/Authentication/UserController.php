<?php

namespace AdminBundle\Controller\Authentication;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Authentication\UserType as UserDatatableType;
use AdminBundle\Form\Authentication\UserType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Security\Employee\User;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/authenticatie/gebruikers")
 * @author Robbert van Mourik
 *
 */
class UserController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Gebruikers';
    }

    public function getOptions()
    {

        return [
            'label' => 'Gebruikers',
            'entity_class' => User::class,
            'form_type' => UserType::class,
            'assets' => [
                'admin/css/form/user.css',
                'admin/js/form/user.js',
            ],
            'roles' => [
                'list' => ['ROLE_VIEW'],
                'form' => ['ROLE_MODIFY'],
                'delete' => ['ROLE_MODIFY'],
            ],
            'datatable' => [
                'entity_class' => User::class,
                'datatable_type' => UserDatatableType::class,
                'form_type' => UserType::class,
                'results_per_page' => 50,
            ],
        ];
    }
}
