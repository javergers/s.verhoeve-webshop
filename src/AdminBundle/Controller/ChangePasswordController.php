<?php

namespace AdminBundle\Controller;

/**
 * {@inheritdoc}
 */
class ChangePasswordController extends Controller
{

    /**
     * {@inheritdoc}
     */
    protected function getRedirectionUrl()
    {
        return $this->container->get('router')->generate('admin_dashboard_index');
    }

}