<?php

namespace AdminBundle\Controller\DataSet;

use AdminBundle\Controller\Controller;
use AdminBundle\Form\DataSet\Settings\DataSetFilterFormType;
use AdminBundle\Interfaces\CrudControllerInterface;
use ReflectionException;
use RuleBundle\Entity\Entity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DataSetFilterController
 * @package AdminBundle\Controller\DataSet
 *
 * @Route("/dataset/filter-instellingen")
 */
class DataSetFilterSettingsController extends Controller implements CrudControllerInterface
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Dataset filter instellingen';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [];
    }

    /**
     * @Route("/{entity}", methods={"GET", "POST"})
     *
     * @param Request $request
     * @param Entity  $entity
     *
     * @return Response
     * @throws ReflectionException
     */
    public function index(Request $request, Entity $entity): Response
    {
        $this->denyAccessUnlessGranted($this->getRolesFor('create'));

        $form = $this->createForm(DataSetFilterFormType::class, $entity, [
            'action' => $this->generateUrl('admin_dataset_datasetfiltersettings_index', [
                'entity' => $entity->getId(),
            ]),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return new Response('');
        }

        return $this->render('AdminBundle:DataSet:settings_form.html.twig', [
            'form' => $form->createView(),
            'entityName' => $entity->getLabel(),
        ]);
    }
}