<?php

namespace AdminBundle\Controller\Assortment;

use AdminBundle\Controller\Controller;
use AdminBundle\Form\Assortment\EditAssortmentProductFormType;
use AppBundle\Entity\Catalog\Assortment\AssortmentProduct;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AssortmentProductController
 * @package AdminBundle\Controller\Assortment
 * @Route("/assortiment-producten")
 */
class AssortmentProductController extends Controller
{
    /**
     * @Route("/{assortmentProduct}/bewerken", methods={"GET"})
     * @Template()
     * @param AssortmentProduct $assortmentProduct
     *
     * @return array
     */
    public function editAction(AssortmentProduct $assortmentProduct)
    {
        $form = $this->createForm(EditAssortmentProductFormType::class, $assortmentProduct);

        return [
            'assortmentProduct' => $assortmentProduct,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{assortmentProduct}/bewerken", methods={"POST"})
     * @Template()
     * @param AssortmentProduct $assortmentProduct
     * @param Request           $request
     *
     * @return array
     */
    public function persistEditAction(AssortmentProduct $assortmentProduct, Request $request)
    {
        $form = $this->createForm(EditAssortmentProductFormType::class, $assortmentProduct);
        $accessor = PropertyAccess::createPropertyAccessorBuilder()
            ->enableMagicCall()
            ->getPropertyAccessor();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $assortmentProduct = $form->getData();

            foreach ($assortmentProduct->getAssortmentProductImages() as $productImage) {
                $productImage->setAssortmentProduct($assortmentProduct);
            }

            foreach ($form->all() as $field) {
                if ($field->getName() == 'assortmentProductImages') {
                    continue;
                }

                $accessor->setValue($assortmentProduct, $field->getName(), $field->getData());
            }

            $this->getDoctrine()->getManager()->flush();
        }

        return [
            'assortmentProduct' => $assortmentProduct,
        ];
    }

}
