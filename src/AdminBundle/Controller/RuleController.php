<?php

namespace AdminBundle\Controller;

use AdminBundle\Datatable\Rule\RuleDatatableType;
use AppBundle\Entity\Relation\CompanyRule;
use AppBundle\Entity\Report\CompanyReport;
use AppBundle\Entity\Report\Report;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use RuleBundle\Entity\Rule;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RuleController
 * @package AppBundle\Controller
 * @Route("/regels")
 */
class RuleController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => Rule::class,
            'datatable' => [
                'entity_class' => Rule::class,
                'datatable_type' => RuleDatatableType::class,
                'actions' => [
                    [
                        'icon' => 'mdi mdi-plus',
                        'route' => 'rule_rule_create',
                        'label' => 'Regel toevoegen',
                        'className' => 'btn-success',
                        'type' => 'add-rule',
                    ]
                ],
                'item_actions' => [
                    [
                        'icon' => 'mdi mdi-eye',
                        'route' => 'rule_rule_edit',
                        'type' => 'rule_edit',
                        'method' => 'GET',
                        'override_propagation' => true,
                    ]
                ],
                'query_builder' => function (QueryBuilder $qb) {
                    $qb->leftJoin(Report::class, 'r', Join::WITH, 'r.rule = rule.id');
                    $qb->leftJoin(CompanyRule::class, 'cr', Join::WITH, 'cr.rule = rule.id');
                    $qb->where('r.rule IS NULL');
                    $qb->andWhere('cr.rule IS NULL');
                },
                'disable_filter' => true,
            ],
            'assets' => [
                'admin/js/rule/rule.js',
                'rule/js/Rule/Edit.js'
            ],
        ];
    }
}
