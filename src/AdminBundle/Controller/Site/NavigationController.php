<?php

namespace AdminBundle\Controller\Site;

use AdminBundle\Controller\Controller;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Entity\Site\Menu;
use AppBundle\Entity\Site\MenuItem;
use AppBundle\Entity\Site\Site;
use AppBundle\Interfaces\MenuSourceInterface;
use AppBundle\Traits\AccessibleEntity;
use AppBundle\Utils\MenuItem as MenuItemUtil;
use AppBundle\Utils\MenuStructure;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\InvalidArgumentException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;

/**
 * Class NavigationController
 * @package AdminBundle\Controller\Site
 *
 * @Route("/site/navigatie")
 */
class NavigationController extends Controller
{
    /** @var int */
    private $position;

    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null): string
    {
        return 'Navigatie';
    }

    /**
     * @Template()
     * @Route("/")
     *
     * @return array
     */
    public function indexAction(): array
    {
        $sites = $this->get('doctrine')->getRepository(Site::class)->findAll();

        return [
            'sites' => $sites,
        ];
    }

    /**
     * @Template("@Admin/Site/Navigation/index.html.twig")
     * @Route("/{id}")
     *
     * @param Menu    $menu
     * @param Request $request
     *
     * @return JsonResponse|array
     * @throws InvalidArgumentException
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function menuAction(Menu $menu, Request $request)
    {
        if ($request->isXmlHttpRequest() && $request->query->has('entityId') && $request->query->has('entityName')) {
            $result = [];
            $hasAccessibility = false;
            $entityId = $request->query->get('entityId');
            $entityName = $request->query->get('entityName');

            if (class_exists($entityName)) {
                $entity = $this->get('doctrine')->getManager()->find($entityName, $entityId);
                $result['displayTitle'] = $entity->getMenuTitle();

                $hasAccessibility = \in_array(AccessibleEntity::class, class_uses($entityName), true);

                if ($hasAccessibility) {
                    $result['loginRequired'] = $entity->getLoginRequired();
                    $result['customerGroups'] = [];

                    /** @var CustomerGroup $customerGroup */
                    foreach ($entity->getAccessibleCustomerGroups() as $customerGroup) {
                        $result['customerGroups'][$customerGroup->getId()] = $customerGroup->getDescription();
                    }
                }
            }

            $result['hasAccessibility'] = $hasAccessibility;

            return new JsonResponse($result);
        }

        $sites = $this->get('doctrine')->getRepository(Site::class)->findAll();
        $customerGroups = $this->get('doctrine')->getRepository(CustomerGroup::class)->findAll();

        $menuStructure = $this->get('app.menu_factory')->disableCache()->get($menu);

        return [
            'sites' => $sites,
            'customerGroups' => $customerGroups,
            'active_menu' => $menuStructure,
            'dummy_item' => $this->getDummyMenuStructure($menu),
            'locales' => $this->getParameter('a2lix_translation_form.locales'),
            'default_locale' => $this->getParameter('a2lix_translation_form.default_locale'),
            'max_levels' => $menu->getMaxLevels(),
            'max_root_items' => $menu->getMaxRootItems(),
            'max_child_items' => $menu->getMaxChildItems(),
        ];
    }

    /**
     * @Route("/{id}/zoeken", methods={"GET"})
     * @Template()
     *
     * @param Menu    $menu
     * @param Request $request
     *
     * @return array
     * @throws \Exception
     */
    public function searchAction(Menu $menu, Request $request): array
    {
        if (!$request->query->get('keyword')) {
            throw new MissingMandatoryParametersException('Some mandatory parameters are missing');
        }

        $results = [];

        $availableMenuSources = $this->getMenuSources();

        foreach ($availableMenuSources as $key => $source) {
            if (!method_exists($this->getDoctrine()->getRepository($source), 'searchMenu')) {
                continue;
            }

            $reflection = new \ReflectionClass($source);

            if(false === $menu->getCompanies()->isEmpty()) {
                if (!method_exists($this->getDoctrine()->getRepository($source), 'searchMenuByCompany')) {
                    continue;
                }

                $company = $menu->getCompanies()->current();

                $items = $this->getDoctrine()->getRepository($source)->searchMenuByCompany($company,
                    $request->query->get('keyword'));
            } else {
                $items = $this->getDoctrine()->getRepository($source)->searchMenu($menu->getSite(),
                    $request->query->get('keyword'));
            }

            if ($items) {
                $results[$key] = [
                    'config' => $source::getMenuSourceConfig(),
                    'items' => $items,
                    'slug' => strtolower($reflection->getShortName()),
                    'entity' => $source,
                ];
            }
        }

        return [
            'results' => $results,
        ];
    }

    /**
     * @Route("/{id}/opslaan", methods={"POST"})
     * @Template("@Admin/Site/Navigation/index.html.twig")
     *
     * @param Menu    $menu
     * @param Request $request
     *
     * @return array|RedirectResponse
     * @throws InvalidArgumentException
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function saveAction(Menu $menu, Request $request)
    {
        $itemsMarkedForRemoval = json_decode($request->request->get('marked_for_removal'));
        $postedItems = json_decode($request->request->get('tree_source'));

        $currentItems = $menu->getItems();

        $formattedCurrentItems = $this->formatCurrentItems($currentItems);

        try {
            $this->get('doctrine')->getConnection()->beginTransaction();

            $this->position = -1;

            $this->processCreateAndUpdates($menu, $formattedCurrentItems, $postedItems);
            $this->processRemovals($itemsMarkedForRemoval, $formattedCurrentItems);

            $this->get('doctrine')->getManager()->flush();

            $this->get('doctrine')->getConnection()->commit();

            return $this->redirectToRoute('admin_site_navigation_menu', [
                'id' => $menu->getId(),
            ]);
        } catch (\Exception $e) {
            $this->get('doctrine')->getConnection()->rollback();
        }

        $sites = $this->get('doctrine')->getRepository(Site::class)->findAll();
        $customerGroups = $this->get('doctrine')->getRepository(CustomerGroup::class)->findAll();

        $menuStructure = $this->get('app.menu_factory')->disableCache()->get($menu);

        return [
            'sites' => $sites,
            'customerGroups' => $customerGroups,
            'active_menu' => $menuStructure,
            'dummy_item' => $this->getDummyMenuStructure($menu),
            'locales' => $this->getParameter('a2lix_translation_form.locales'),
            'default_locale' => $this->getParameter('a2lix_translation_form.default_locale'),
            'max_levels' => $menu->getMaxLevels(),
            'max_root_items' => $menu->getMaxRootItems(),
        ];
    }

    /**
     * @param       $items
     * @param array $results
     *
     * @return array
     */
    private function formatCurrentItems($items, array $results = []): array
    {
        /**
         * @var MenuItem $item
         * @return mixed
         */
        foreach ($items as $item) {
            $results[$item->getId()] = $item;
        }

        return $results;
    }

    /**
     * @param Menu    $menu
     * @param array   $currentItems
     * @param array   $items
     * @param null    $parent
     * @param integer $position
     *
     * @todo: Add image processing
     */
    private function processCreateAndUpdates($menu, $currentItems, $items, $parent = null, $position = -1): void
    {
        foreach ($items as $item) {
            $menuItem = new MenuItem();

            if (!empty($item->new)) {
                $menuItem->setMenu($menu);

                $menu->addItem($menuItem);
            } elseif (array_key_exists($item->id, $currentItems)) {
                $menuItem = $currentItems[$item->id];
            }

            $menuItem->setBlank($item->blank);
            $menuItem->setPosition(++$position);
            $menuItem->setPublish($item->publish);

            if (!empty($item->publish_start)) {
                $menuItem->setPublishStart(new \DateTime($item->publish_start));
            } else {
                $menuItem->setPublishStart(null);
            }

            if (!empty($item->publish_end)) {
                $menuItem->setPublishEnd(new \DateTime($item->publish_end));
            } else {
                $menuItem->setPublishEnd(null);
            }

            if (!empty($item->entity_id) && !empty($item->entity_name)) {
                $menuItem->setEntityId($item->entity_id);
                $menuItem->setEntityName($item->entity_name);
            } else {
                $menuItem->setLoginRequired($item->login_required);

                if ($menuItem->getAccessibleCustomerGroups()) {
                    foreach ($menuItem->getAccessibleCustomerGroups() as $customerGroup) {
                        $menuItem->removeAccessibleCustomerGroup($customerGroup);
                    }

                    if (!empty($item->customer_groups) && \is_array($item->customer_groups)) {
                        $customerGroups = $this->get('doctrine')->getRepository(CustomerGroup::class)->findBy([
                            'id' => $item->customer_groups,
                        ]);

                        foreach ($customerGroups as $customerGroup) {
                            $menuItem->addAccessibleCustomerGroup($customerGroup);
                        }
                    }
                }
            }

            foreach ($item->url as $url) {
                foreach (array_keys((array)$url) as $locale) {
                    if (!empty($url->{$locale})) {
                        $menuItem->translate($locale, false)->setUrl($url->{$locale});
                    } else {
                        $menuItem->translate($locale, false)->setUrl(null);
                    }
                }
            }

            foreach ($item->title as $title) {
                foreach (array_keys((array)$title) as $locale) {
                    if (!empty($title->{$locale})) {
                        $menuItem->translate($locale, false)->setTitle($title->{$locale});
                    } else {
                        $menuItem->translate($locale, false)->setTitle(null);
                    }
                }
            }

            foreach ($item->content as $content) {
                foreach (array_keys((array)$content) as $locale) {
                    if (!empty($content->{$locale})) {
                        $menuItem->translate($locale, false)->setContent($content->{$locale});
                    } else {
                        $menuItem->translate($locale, false)->setContent(null);
                    }
                }
            }

            if ($parent && $parent instanceof MenuItem) {
                $menuItem->setParent($parent);

                $parent->addChild($menuItem);
            } else {
                $menuItem->setParent(null);
            }

            $this->get('doctrine')->getManager()->persist($menuItem);

            $menuItem->mergeNewTranslations();

            if (!empty($item->children)) {
                $this->processCreateAndUpdates($menu, $currentItems, $item->children, $menuItem, -1);
            }
        }
    }

    /**
     * @param array $markedForRemoval
     * @param array $currentItems
     */
    private function processRemovals($markedForRemoval, $currentItems): void
    {
        foreach ($markedForRemoval as $id) {
            $menuItem = $currentItems[$id];

            $this->get('doctrine')->getManager()->remove($menuItem);
        }
    }

    /**
     * @return array
     */
    private function getMenuSources(): array
    {
        return $this->getMenuSourceEntities();
    }

    /**
     * @return array
     */
    private function getMenuSourceEntities(): array
    {
        $classes = $this->container->get('doctrine.orm.entity_manager')->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();

        $entities = [];

        foreach ($classes as $class) {
            if (!\in_array(MenuSourceInterface::class, class_implements($class), true)) {
                continue;
            }

            $entities[] = $class;
        }

        return $entities;
    }

    /**
     * Method to create a dummy structure for the prototype
     *
     * @param Menu $menu
     *
     * @return MenuStructure
     * @throws \Exception
     */
    private function getDummyMenuStructure(Menu $menu): MenuStructure
    {
        $slugService = $this->get('app.slug');
        $tokenStorage = $this->get('security.token_storage');
        $translator = $this->get('translator');

        /** @var EntityManagerInterface $em */
        $em = $this->getDoctrine()->getManager();

        $dummyMenuItem = new MenuItem();
        $dummyMenuItem->setId('__INDEX__');
        $dummyMenuItem->setBlank(false);
        $dummyMenuItem->setPublish(true);

        $dummyMenuItemUtil = new MenuItemUtil($em, $slugService,
            $this->get('security.token_storage'), $menu, $translator, $dummyMenuItem);

        $dummyMenuStructure = new MenuStructure(
            $em,
            $slugService,
            $tokenStorage,
            $menu,
            $translator,
            $dummyMenuItem,
            false
        );

        $dummyMenuStructure->setItems([$dummyMenuItemUtil]);

        return $dummyMenuStructure;
    }
}
