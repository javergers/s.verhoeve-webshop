<?php

namespace AdminBundle\Controller\Site;

use AdminBundle\Controller\CrudController;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Site\Site;
use AppBundle\Interfaces\Preview\PreviewableControllerInterface;
use AppBundle\Interfaces\Preview\PreviewableEntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/site/paginas")
 */
class PageController extends CrudController implements CrudControllerInterface, PreviewableControllerInterface
{
    /**
     * @param null $n
     *
     * @return mixed|null|string
     */
    public function getLabel($n = null)
    {
        return "Pagina's";
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => Page::class,
            'datatable' => [
                'entity_class' => Page::class,
                'item_actions' => [
                    [
                        'icon' => 'mdi mdi-table-search',
                        'type' => 'preview',
                        'label' => 'Voorbeeld',
                        'url' => function (Page $page) {
                            return $this->generateUrl('admin_site_page_preview', [
                                'entity' => $page->getId()
                            ]);
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Method for retreiving the possible sites which this item can be previewed on
     *
     * @Route("/preview/{entity}/sites", methods={"GET"})
     * @ParamConverter("entity", class="AppBundle:Site\Page")
     *
     * @param PreviewableEntityInterface|Page $entity
     * @param Request                         $request
     *
     * @return JsonResponse
     */
    public function previewableSitesAction(PreviewableEntityInterface $entity, Request $request)
    {
        $sites = new ArrayCollection();
        $results = [];

        $site = $entity->getSite();

        if (!$sites->contains($site)) {
            $sites->add($site);
        }

        foreach ($sites as $site) {
            $results[$site->getId()] = $site->translate()->getDescription();
        }

        return new JsonResponse([
            'sites' => $results,
        ]);
    }

    /**
     * Redirect to the proper url of the item that needs to be previewed
     *
     * @Route("/preview/{entity}/{site}", defaults={"site" = null}, methods={"GET"})
     * @ParamConverter("entity", class="AppBundle:Site\Page")
     *
     * @param PreviewableEntityInterface|Page $entity
     * @param Site|null                       $site
     * @param Request                         $request
     *
     * @return RedirectResponse
     */
    public function previewAction(PreviewableEntityInterface $entity, Site $site = null, Request $request)
    {
        $values = [
            'class' => get_class($entity),
            'identifier' => $entity->getId(),
            'siteId' => $site->getId(),
            'admin_url' => $request->query->get('return_uri', null),
        ];

        $url = $this->get('preview')->getRequestUrl($site->getPrimaryDomain()->getUri(), $values);

        return new RedirectResponse($url);
    }
}
