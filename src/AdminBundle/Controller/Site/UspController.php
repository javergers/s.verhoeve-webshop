<?php

namespace AdminBundle\Controller\Site;

use AdminBundle\Controller\CrudController;
use AdminBundle\Interfaces\CrudControllerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @Route("/site/ups")
 */
class UspController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return "USP's";
    }

    public function getOptions()
    {
        return [
            'entity_class' => 'AppBundle\Entity\Site\Usp',
        ];
    }
}
