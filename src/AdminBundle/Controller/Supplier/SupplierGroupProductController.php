<?php

namespace AdminBundle\Controller\Supplier;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Supplier\SupplierGroupProductDatatableType;
use AdminBundle\Form\Catalog\SupplierGroupProductType;
use AppBundle\Entity\Supplier\SupplierGroupProduct;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SupplierProductController
 * @package AdminBundle\Controller\Supplier
 * @Route("/leveranciers-groep-producten")
 */
class SupplierGroupProductController extends CrudController {

    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null): string
    {
        return 'Leveranciersproducten';
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'entity_class' => SupplierGroupProduct::class,
            'form_type' => SupplierGroupProductType::class,
            'datatable' => [
                'entity_class' => SupplierGroupProduct::class,
                'datatable_type' => SupplierGroupProductDatatableType::class,
                'form_type' => SupplierGroupProductType::class,
            ]
        ];
    }

}
