<?php

namespace AdminBundle\Controller\Supplier;

use AdminBundle\Annotation\Breadcrumb;
use AdminBundle\Controller\CrudController;
use AdminBundle\Form\Supplier\PickupLocationType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Relation\CompanyEstablishment;
use AppBundle\Entity\Relation\PickupLocation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PickupLocationController
 * @package AdminBundle\Controller\Supplier
 * @Route("/afhaal-locaties")
 */
class PickupLocationController extends CrudController implements CrudControllerInterface
{
    /**
     * @param int|null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Afhaallocatie';
    }

    /**
     * @Route("/vestiging/{companyEstablishment}/bewerken", methods={"GET"})
     * @Breadcrumb()
     *
     * @param CompanyEstablishment $companyEstablishment
     * @return Response
     * @throws \Exception
     */
    public function editFromCompanyEstablishment(CompanyEstablishment $companyEstablishment){
        /** @var PickupLocation $pickupLocation */
        $pickupLocation = $this->get('doctrine')->getRepository(PickupLocation::class)->findByCompany($companyEstablishment->getCompany());
        return $this->editPickupLocation($pickupLocation);
    }

    /**
     * @Route("/vestiging/{companyEstablishment}/bewerken", methods={"POST"})
     * @Breadcrumb()
     *
     * @param CompanyEstablishment $companyEstablishment
     * @return Response
     * @throws \Exception
     */
    public function editPersistFromCompanyEstablishment(CompanyEstablishment $companyEstablishment){
        /** @var PickupLocation $pickupLocation */
        $pickupLocation = $this->get('doctrine')->getRepository(PickupLocation::class)->findByCompany($companyEstablishment->getCompany());
        return $this->editPersistPickupLocation($pickupLocation);
    }

    /**
     * @Route("/{id}/bewerken", methods={"GET"})
     * @ParamConverter("id", options={"mapping": { "pickupLocation" }})
     * @Breadcrumb()
     *
     * @param PickupLocation $pickupLocation
     * @return Response
     * @throws \Exception
     */
    public function editPickupLocation(PickupLocation $pickupLocation){
        $formData = $this->form($pickupLocation, false);
        return $this->render('@Admin/Crud/div_collection_form.html.twig', $formData);
    }

    /**
     * @Route("/{id}/bewerken", methods={"POST"})
     * @ParamConverter("id", options={"mapping": { "pickupLocation" }})
     * @Breadcrumb()
     *
     * @param PickupLocation $pickupLocation
     * @return Response
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function editPersistPickupLocation(PickupLocation $pickupLocation){
        $this->denyAccessUnlessGranted($this->getRolesFor('update'));

        $form = $this->getForm($this->getFormType(), $pickupLocation);

        $form->handleRequest($this->getRequest());
        $this->get('doctrine')->getManager()->flush();

        return $this->editPickupLocation($pickupLocation);
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => PickupLocation::class,
            'form_type' => PickupLocationType::class,
        ];
    }
}
