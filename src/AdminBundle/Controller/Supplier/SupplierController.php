<?php

namespace AdminBundle\Controller\Supplier;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\App\CompanyType;
use AdminBundle\Datatable\Company\CompanyEstablishmentType;
use AdminBundle\Datatable\Order\SupplierOrderDatatableType;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyEstablishment;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Manager\Relation\CompanyManager;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/klanten/leveranciers")
 * Class SupplierController
 * @package AdminBundle\Controller\Supplier
 */
class SupplierController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'entity_class' => Company::class,
            'datatable' => [
                'entity_class' => Company::class,
                'datatable_type' => CompanyType::class,
                'query_builder' => function (QueryBuilder $qb) {
                    return $qb->andWhere('company.isSupplier = true');
                },
                'actions_processing' => 'override',
                'actions' => [
                    [
                        'icon' => 'mdi mdi-plus',
                        'route' => 'admin_customer_company_wizard',
                        'label' => 'Leverancier toevoegen',
                        'className' => 'btn-success',
                    ],
                ],
                'allow_read' => true,
                'allow_edit' => false,
            ],
            'assets' => [
                'admin/js/form/company.js',
                'admin/css/form/company.css',
                'admin/js/form/product.js',
                'admin/js/company/company-assortment.js',
                'admin/assets/sortable/Sortable.min.js',
                'admin/js/form/assortment.js',
                'admin/js/form/company-establishment.js',
                'admin/css/form/assortment.css',
            ],
            'view' => [
                'label_subject' => '%%name%%',
                'panels' => [
                    [
                        'id' => 'information_panel',
                        'type' => 'template',
                        'template' => '@Admin/Company/information-panel.html.twig',
                        'template_parameters' => [
                            'showConnector' => true,
                        ],
                    ],
                    [
                        'id' => 'tab_panel',
                        'type' => 'tabs',
                        'tabs' => [
                            [
                                'action' => 'orders',
                                'label' => 'Uitgevoerde orders',
                                'datatable' => [
                                    'alias' => 'ordr',
                                    'datatable_type' => SupplierOrderDatatableType::class,
                                    'entity_class' => SupplierOrder::class,
                                    'allow_add' => false,
                                    'allow_delete' => false,
                                    'allow_edit' => false,
                                    'allow_read' => false,
                                    'parent_column' => 'supplier',
                                    'query_builder' => function (QueryBuilder $qb, Request $request = null) {
                                        if (null !== $request) {
                                            return $qb->where('ordr.supplier = :value')->orderBy('ordr.deliveryDate', 'DESC');
                                        }

                                        return $qb;
                                    },
                                    'item_actions' => [
                                        [
                                            'icon' => 'mdi mdi-eye',
                                            'route' => 'admin_sales_order_order',
                                            'type' => 'get-order',
                                            'method' => 'GET',
                                            'identifier' => [
                                                'field' => 'orderCollection',
                                                'id' => 'order.orderCollection.id',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                'action' => 'vestigingen',
                                'label' => 'Vestigingen',
                                'datatable' => [
                                    'alias' => 'coes',
                                    'datatable_type' => CompanyEstablishmentType::class,
                                    'entity_class' => CompanyEstablishment::class,
                                    'allow_add' => true,
                                    'allow_delete' => false,
                                    'allow_edit' => true,
                                    'allow_read' => true,
                                    'parent_column' => 'company',
                                    'item_actions' => [
                                        [
                                            'icon' => 'mdi mdi-pencil',
                                            'route' => 'admin_company_companyestablishment_edit',
                                            'type' => 'get-company-establishment',
                                            'method' => 'GET',
                                            'identifier' => [
                                                'field' => 'id',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
