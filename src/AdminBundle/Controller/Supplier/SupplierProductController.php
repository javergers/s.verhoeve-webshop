<?php

namespace AdminBundle\Controller\Supplier;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Supplier\SupplierGroupProductDatatableType;
use AdminBundle\Datatable\Supplier\SupplierProductDatatableType;
use AdminBundle\Form\App\SupplierGroupType;
use AdminBundle\Form\Catalog\SupplierGroupProductType;
use AdminBundle\Form\Catalog\SupplierProductType;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Supplier\SupplierGroup;
use AppBundle\Entity\Supplier\SupplierGroupProduct;
use AppBundle\Entity\Supplier\SupplierProduct;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Router;

/**
 * Class SupplierProductController
 * @package AdminBundle\Controller\Supplier
 * @Route("/leveranciers-producten")
 */
class SupplierProductController extends CrudController
{

    /**
     * @var Router
     */
    private $router;

    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null): string
    {
        return 'Leveranciersproducten';
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'entity_class' => SupplierProduct::class,
            'form_type' => SupplierProductType::class,
            'datatable' => [
                'entity_class' => SupplierProduct::class,
                'datatable_type' => SupplierProductDatatableType::class,
                'form_type' => SupplierProductType::class,
            ],
        ];
    }

    /**
     * @Route("/{id}/leveranciers", methods={"GET"})
     * @Template()
     *
     * @param         $id
     * @param Request $request
     *
     * @return array
     *
     * @throws \Exception
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function supplierAction($id, Request $request): array
    {
        $this->router = $this->container->get('router');
        /** @var Product $product */
        $product = $this->container->get('doctrine')->getManager()->getRepository(Product::class)->find($id);

        $supplierProductDatatable = $this->getSupplierProductDatatable($product);
        $supplierGroupProductDatatable = $this->getSupplierGroupProductDatatable($product);

        return [
            'supplierProductDatatable' => $supplierProductDatatable,
            'supplierGroupProductDatatable' => $supplierGroupProductDatatable,
            'product' => $product,
        ];
    }



    /**
     * @Route("/{product}/leveranciers-data", methods={"GET", "POST"})
     *
     * @param Product $product
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function supplierDataAction(Product $product): JsonResponse
    {
        return $this->container->get('admin.datatable.service')->getData([
            'parent' => [
                'entity' => 'product',
                'parentId' => $product->getId(),
            ],
            'entity_class' => SupplierProduct::class,
            'datatable_type' => SupplierProductDatatableType::class,
            'form_type' => SupplierProductType::class,
            'base_route' => 'admin_supplier_supplierproduct',
            'allow_reorder' => true
        ]);
    }

    /**
     * @Route("/{product}/leveranciers-groep-data", methods={"GET", "POST"})
     *
     * @param Product $product
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function supplierGroupDataAction(Product $product): JsonResponse
    {
        return $this->container->get('admin.datatable.service')->getData([
            'parent' => [
                'entity' => 'product',
                'parentId' => $product->getId(),
            ],
            'entity_class' => SupplierGroupProduct::class,
            'datatable_type' => SupplierGroupProductDatatableType::class,
            'form_type' => SupplierGroupProductType::class,
            'base_route' => 'admin_supplier_suppliergroupproduct',
            'allow_reorder' => true
        ]);
    }

    /**
     * @param Product $product
     *
     * @return string
     *
     * @throws \Exception
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function getSupplierProductDatatable(Product $product): string
    {
        return $this->container->get('admin.datatable.service')->getTable([
            'entity_class' => SupplierProduct::class,
            'datatable_type' => SupplierProductDatatableType::class,
            'form_type' => SupplierProductType::class,
            'data_url' => $this->router->generate('admin_supplier_supplierproduct_supplierdata',
                ['product' => $product->getId()]),
            'disable_search' => true,
            'base_route' => 'admin_supplier_supplierproduct',
            'allow_reorder' => true,
            'reorder_url' => $this->generateUrl('admin_supplier_supplierproduct_reordersupplierproduct', [
                'product' => $product->getId()
            ])
        ]);
    }

    /**
     * @param Product $product
     *
     * @return string
     *
     * @throws \Exception
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function getSupplierGroupProductDatatable(Product $product): string
    {
        return $this->container->get('admin.datatable.service')->getTable([
            'entity_class' => SupplierGroup::class,
            'datatable_type' => SupplierGroupProductDatatableType::class,
            'form_type' => SupplierGroupType::class,
            'data_url' => $this->router->generate('admin_supplier_supplierproduct_suppliergroupdata',
                ['product' => $product->getId()]),
            'disable_search' => true,
            'base_route' => 'admin_supplier_suppliergroupproduct',
            'allow_reorder' => true,
            'reorder_url' => $this->generateUrl('admin_supplier_supplierproduct_reordersuppliergroupproduct', [
                'product' => $product->getId()
            ])
        ]);
    }

    /**
     * @Route("/{product}/leveranciers-product-reorder", methods={"POST"})
     * @param Request $request
     * @param Product $product
     *
     * @return JsonResponse
     */
    public function reorderSupplierProduct(Request $request, Product $product): JsonResponse
    {
        $reorder = $request->get('reorder');

        return $this->container->get('admin.datatable.service')->reorder([
            'entity_class' => SupplierProduct::class,
        ], $reorder);
    }

    /**
     * @Route("/{product}/leveranciers-groep-product-reorder", methods={"POST"})
     * @param Request $request
     * @param Product $product
     *
     * @return JsonResponse
     */
    public function reorderSupplierGroupProduct(Request $request, Product $product): JsonResponse
    {
        $reorder = $request->get('reorder');

        return $this->container->get('admin.datatable.service')->reorder([
            'entity_class' => SupplierGroupProduct::class,
        ], $reorder);
    }
}
