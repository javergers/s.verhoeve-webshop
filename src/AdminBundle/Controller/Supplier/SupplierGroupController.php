<?php

namespace AdminBundle\Controller\Supplier;

use AdminBundle\Controller\CrudController;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Supplier\SupplierGroup;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * @Route("/leveranciergroepen")
 */
class SupplierGroupController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Leveranciergroepen';
    }

    public function getOptions()
    {
        return [
            'entity_class' => SupplierGroup::class,
        ];
    }
}
