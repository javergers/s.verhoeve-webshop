<?php

namespace AdminBundle\Controller\Examples\Crud;

use AdminBundle\Controller\CrudController;
use AdminBundle\Interfaces\CrudControllerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/examples/crud/array-collection")
 */
class ArrayCollectionController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Test Array Collection';
    }

    public function getOptions()
    {
        return [
            'label' => 'Example',
            'datatable' => [
                'datatable_type' => 'AdminBundle\Datatable\Catalog\ProductDatatableType',
            ],
        ];
    }

    /**
     * @return ArrayCollection
     */
    public function getCollection()
    {
        $collection = new ArrayCollection();


        foreach ($this->container->get('router')->getRouteCollection()->all() as $route) {
            // ignore all service routes like assetic, web_profiler, twig
            if (strpos($route->getDefault('_controller'), ".") !== false) {
                continue;
            }

            $object = new \stdClass();
            $object->title = $route->getPath();
            $object->description = $route->getDefault('_controller');
            $object->price = implode(",", $route->getMethods());

            $collection[] = $object;
        }

        return $collection;
    }
}
