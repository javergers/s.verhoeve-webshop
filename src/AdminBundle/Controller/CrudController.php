<?php

namespace AdminBundle\Controller;

use AdminBundle\Annotation\Breadcrumb;
use AdminBundle\Components\Crud\CrudActionResponse;
use AppBundle\Exceptions\ImageUploadException;
use AppBundle\Exceptions\NonDeletableException;
use AppBundle\Exceptions\NonEditableException;
use AdminBundle\Services\AutoDetect\AutoDetectTypeService;
use AdminBundle\Services\Router\BaseRouteService;
use AdminBundle\Traits\SidebarMenu;
use AppBundle\Form\Type\EntityChoiceType;
use AppBundle\Interfaces\EntityInterface;
use AppBundle\Traits\NonEditableEntity;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;

/**
 * Class CrudController
 * @package AdminBundle\Controller
 * @link    http://symfonybricks.com/en/brick/abstract-controller-for-a-rest-based-crud-server
 */
abstract class CrudController extends Controller
{
    use SidebarMenu;

    const SCREEN_INDEX = 'screen';
    const SCREEN_VIEW = 'view';
    const SCREEN_ADD = 'add';
    const SCREEN_EDIT = 'edit';
    const SCREEN_DELETE = 'delete';

    /**
     * @var array
     */
    private $options = [];

    /**
     * @var string
     */
    private $formType;

    /**
     * @var array
     */
    private $assets = [];

    /**
     * @var string
     */
    private $breadcrumb_menu;

    /**
     * @var string
     */
    private $screen;

    /**
     * @var array
     */
    private $selectedTab = [];

    /**
     * @var string
     */
    protected $redirectRoute;

    /**
     * @var null | array
     */
    protected $redirectParameters;

    /**
     * @return string
     */
    protected function getRedirectUrl()
    {
        return $this->generateUrl($this->redirectRoute ?? $this->container->get(BaseRouteService::class)->getBaseRouteForAction('index'),
            $this->redirectParameters ?? []);
    }

    /**
     * @Route("/", methods={"GET"})
     */
    public function indexAction()
    {
        return $this->forward(\get_class($this) . '::datatableAction');
    }

    /**
     * @Route("/datatable", methods={"GET"})
     * @throws \Exception
     */
    public function datatableAction()
    {
        $this->denyAccessUnlessGranted($this->getRolesFor());

        $this->options = $this->getOptions();

        if ($this->isResolved()) {
            $this->resolve();
        }

        $this->screen = self::SCREEN_INDEX;

        $dataTable = $this->container->get('admin.datatable.service')->getTable($this->options['datatable']);

        return $this->render(
            'AdminBundle:Crud:datatable.html.twig',
            [
                'datatable' => $dataTable,
                'crud' => $this,
                'page_title' => $this->getLabel() ? $this->getLabel() . ' overzicht' : 'Overzicht',
                'breadcrumb_menu' => $this->getBreadcrumbMenu(),
                'assets' => $this->getAssets(),
            ]
        );
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @return bool
     */
    protected function isResolved()
    {
        return !empty($this->options);
    }

    protected function resolve()
    {
        $resolver = new OptionsResolver();

        if (!method_exists($this, 'getCollection')) {
            $resolver->setRequired(
                [
                    'entity_class',
                ]
            );
        }

        $resolver->setDefined([
            'sidebar_menu',
            'paginator',
        ]);

        $resolver->setDefaults(
            [
                'form' => [],
                'form_type' => null,
                'results_per_page' => 50,
                'query_builder' => null,
                'query' => '',
                'roles' => null,
                'assets' => [],
                'xhr_assets' => [],
                'breadcrumb_menu' => null,
                'datatable' => [
                    'button_label' => $this->getLabel(),
                    'entity_class' => $this->getOption('entity_class'),
                ],
                'view' => null,
            ]
        );

        $resolver->setDefined([
            'label',
            'title',
        ]);

        $resolver->setAllowedTypes('title', ['null', 'closure']);
        $resolver->setAllowedTypes('form_type', ['null', 'boolean', 'string', 'closure']);
        $resolver->setAllowedTypes('query_builder', ['null', 'closure']);
        $resolver->setAllowedTypes('query', ['null', 'string']);
        $resolver->setAllowedTypes('roles', ['null', 'array']);
        $resolver->setAllowedTypes('label', ['null', 'string', 'array']);
        $resolver->setAllowedTypes('assets', ['null', 'string', 'array']);
        $resolver->setAllowedTypes('xhr_assets', ['null', 'string', 'array']);
        $resolver->setAllowedTypes('breadcrumb_menu', ['null', 'string']);
        $resolver->setAllowedTypes('view', ['null', 'array']);
        $resolver->setAllowedTypes('form', ['array']);

        if (!method_exists($this, 'getCollection')) {
            $resolver->setAllowedTypes('entity_class', ['string']);
        }

        $this->options = $resolver->resolve($this->getOptions());

        if (null !== $this->options['view']) {
            $this->options['view'] = $this->resolveView();
        }

        // Resolve properties for crud form
        if (null !== $this->options['form']) {
            $this->options['form'] = $this->resolveCrudForm();
        }

        if (!$this->options['form_type'] && $this->options['form_type'] !== false) {
            $this->options['form_type'] = $this->container->get(AutoDetectTypeService::class)->autoDetectClassType($this->options['entity_class'],
                'form');
        }

        if (is_callable($this->options['form_type'])) {
            $entity = null;

            if (parent::getRequest()->get('id')) {
                $entity = $this->getRepository()->find(parent::getRequest()->get('id'));
            }

            $this->options['form_type'] = $this->options['form_type']($entity);
        }
    }

    /**
     * @param int $n
     *
     * @return mixed|null
     */
    public function getLabel($n = null)
    {
        if (!isset($this)) {
            return null;
        }

        if (!$this->getOption('label')) {
            return null;
        }

        $label = $this->getOption('label');

        if (is_string($label)) {
            return $label;
        }

        if ($n == 1) {
            return $label[0];
        } else {
            return $label[1];
        }
    }

    /**
     * @param $key
     *
     * @return mixed|null
     */
    public function getOption($key)
    {
        if (!$this->hasOption($key)) {
            return null;
        }

        return $this->options[$key];
    }

    /**
     * @param $key
     *
     * @return bool
     */
    public function hasOption($key)
    {
        return array_key_exists($key, $this->options);
    }

    /**
     * @param array $buttons
     * @return array
     */
    private function resolveCrudFormButtons(array $buttons): array
    {
        $resolver = new OptionsResolver();

        $resolver->setRequired([
            'route',
            'label'
        ]);

        $resolver->setDefaults([
            'route_parameter' => 'id',
            'icon' => 'checkmark-circle'
        ]);

        $resolver->setAllowedTypes('route', ['string']);
        $resolver->setAllowedTypes('route_parameter', ['string']);
        $resolver->setAllowedTypes('label', ['string']);
        $resolver->setAllowedTypes('icon', ['string']);

        $buttons = array_map(function($button) use ($resolver) {
            return $resolver->resolve($button);
        }, $buttons);

        return $buttons;
    }

    /**
     * @return array|mixed|null
     */
    private function resolveCrudForm()
    {
        $resolver = new OptionsResolver();

        $resolver
            ->setDefaults([
                'buttons' => [],
                'allow_submit_close' => true,
                'allow_submit' => true,
            ])
            ->setRequired('buttons')
            ->setAllowedTypes('buttons', ['array'])
        ;

        $formProperties = $resolver->resolve($this->options['form']);

        $formProperties['buttons'] = $this->resolveCrudFormButtons($formProperties['buttons']);

        return $formProperties;
    }

    /**
     * @return array
     */
    private function resolveView()
    {
        $resolver = new OptionsResolver();

        $resolver
            ->setDefined([
                'label',
                'label_subject',
                'label_show_subject',
                'panels',
            ])
            ->setAllowedTypes('label', ['string'])
            ->setAllowedTypes('label_subject', ['string'])
            ->setAllowedTypes('label_show_subject', ['bool'])
            ->setAllowedTypes('panels', ['array'])
            ->setDefaults([
                'label' => 'Bekijk',
                'label_show_subject' => true,
            ]);

        $options = $resolver->resolve($this->getOption('view'));

        $panelsResolver = new OptionsResolver();
        $panelsResolver
            ->setDefined([
                'id',
                'type',
                'template',
                'template_parameters',
                'tabs',
                'panel_actions',
                'datatable',
                'title',
            ])
            ->setRequired([
                'id',
            ])
            ->setAllowedTypes('id', ['string'])
            ->setAllowedTypes('type', ['string'])
            ->setAllowedValues('type', ['tabs', 'template', 'datatable'])
            ->setAllowedTypes('template', ['string'])
            ->setAllowedTypes('template_parameters', ['array'])
            ->setAllowedTypes('tabs', ['array'])
            ->setAllowedTypes('panel_actions', ['array'])
            ->setAllowedTypes('datatable', ['array'])
            ->setAllowedTypes('title', ['string']);

        $panelTabsResolver = new OptionsResolver();
        $panelTabsResolver
            ->setDefined([
                'action',
                'icon',
                'label',
                'type',
                'datatable',
                'template',
                'href',
            ])
            ->setDefaults([
                'type' => 'datatable',
                'href' => $this->container->get(BaseRouteService::class)->getBaseRouteForAction('childview'),
            ])
            ->setAllowedTypes('action', ['string'])
            ->setAllowedTypes('label', ['string'])
            ->setAllowedTypes('icon', ['string'])
            ->setAllowedTypes('type', ['string'])
            ->setAllowedValues('type', ['template', 'datatable'])
            ->setRequired(['action', 'label', 'href']);

        $panelTabTemplateResolver = new OptionsResolver();
        $panelTabTemplateResolver->setDefined([
            'template',
            'template_parameters',
        ]);

        $panelTabTemplateResolver->setAllowedTypes('template', ['string']);
        $panelTabTemplateResolver->setAllowedTypes('template_parameters', ['array']);
        $panelTabTemplateResolver->setDefaults([
            'template_parameters' => [],
        ]);

        $options['panels'] = array_map(function ($panel) use (
            $panelsResolver,
            $panelTabsResolver,
            $panelTabTemplateResolver
        ) {
            $panel = $panelsResolver->resolve($panel);

            if ($panel['type'] == "tabs") {
                $panel['tabs'] = array_map(function ($tab) use ($panelTabsResolver, $panelTabTemplateResolver) {
                    if (isset($tab['type']) && $tab['type'] == 'template') {
                        $tab['template'] = $panelTabTemplateResolver->resolve($tab['template']);
                    }

                    return $panelTabsResolver->resolve($tab);
                }, $panel['tabs']);
            }

            return $panel;
        }, $options['panels']);

        return $options;
    }

    /**
     * @return ObjectRepository
     */
    protected function getRepository()
    {
        if (!$this->isResolved()) {
            $this->resolve();
        }

        return parent::getDoctrine()->getRepository($this->options['entity_class']);
    }

    /**
     * @return mixed
     */
    protected function getBreadcrumbMenu()
    {
        if (!$this->isResolved()) {
            $this->resolve();
        }

        if ($this->hasOption('breadcrumb_menu')) {
            $this->breadcrumb_menu = $this->options['breadcrumb_menu'];
        }

        return $this->breadcrumb_menu;
    }

    /**
     * @return array|mixed
     */
    protected function getAssets($type = 'assets')
    {
        if (!$this->isResolved()) {
            $this->resolve();
        }

        if ($this->hasOption($type)) {
            $assets = [];

            if (!is_array($this->options[$type])) {
                $assets[] = $this->options[$type];
            } else {
                $assets = $this->options[$type];
            }

            foreach ($assets as &$asset) {
                $asset = '/bundles/' . $asset;
            }

            $this->assets = $assets;
        }

        $this->assets[] = '/bundles/admin/css/ContentWindow.css';
        $this->assets[] = '/assets/vendor/jquery-form/dist/jquery.form.min.js';

        return $this->assets;
    }


    /**
     * @Route("/data", methods={"GET", "POST"})
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function dataTableDataAction()
    {
        $this->denyAccessUnlessGranted($this->getRolesFor());

        $this->options = $this->getOptions();
        if ($this->isResolved()) {
            $this->resolve();
        }

        $dataTable = $this->container->get('admin.datatable.service')->getData($this->options['datatable']);

        return $dataTable;
    }

    /**
     * @Route("/nieuw", methods={"GET"})
     * @Breadcrumb()
     *
     * @return array|Response
     * @throws Exception
     */
    public function addAction()
    {
        $this->denyAccessUnlessGranted($this->getRolesFor('create'));

        $this->screen = self::SCREEN_ADD;

        $entity = $this->getEntity();

        return $this->form($entity);
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        if (!$this->isResolved()) {
            $this->resolve();
        }

        return new $this->options['entity_class'];
    }

    /**
     * @param      $entity
     * @param bool $render
     *
     * @return Response|array
     * @throws \Exception
     */
    protected function form($entity, $render = true)
    {
        /** @var EntityInterface $entity */
        $form = $this->getForm($this->getFormType(), $entity);

        if (parent::getRequest()->get('name') && parent::getRequest()->get('search')) {
            return $this->autoComplete($form);
        }

        if (!$this->getLabel()) {
            $page_title = ((self::getScreen() != self::SCREEN_EDIT) ? 'Toevoegen' : 'Bewerken');
        } else {

            $page_title = $this->getLabel() . ' ' . ((self::getScreen() != self::SCREEN_EDIT) ? 'Toevoegen' : 'bewerken');
        }

        if ($entity->getId() && array_key_exists('title', $this->options) && is_callable($this->options['title'])) {
            $page_title .= ' - ' . $this->options['title']($entity);
        }

        $data = [
            'entity' => $entity,
            'form' => $form->createView(),
            'url' => parent::getUrl('datatable'),
            'assets' => $this->getAssets(),
            'page_title' => $page_title,
            'breadcrumb_menu' => $this->getBreadcrumbMenu(),
            'xhr' => false,
        ];

        if ($this->getRequest()->isXmlHttpRequest()) {
            if(!$render){
                return [
                    'form' => $form->createView(),
                    'assets' => $this->getAssets('xhr_assets'),
                    'xhr' => true,
                ];
            }
            return $this->render(
                '@Admin/Crud/form_xhr.html.twig',
                [
                    'form' => $form->createView(),
                    'assets' => $this->getAssets('xhr_assets'),
                ]
            );
        }

        if (!$render) {
            return $data;
        }

        return $this->render(
            'AdminBundle:Crud:form.html.twig',
            $data
        );


    }

    /**
     * @param       $formType
     * @param       $entity
     * @param array $data
     *
     * @return Form|FormInterface
     */
    protected function getForm($formType, $entity, array $data = [])
    {
        if (!$this->isResolved()) {
            $this->resolve();
        }

        if (!array_key_exists('method', $data)) {
            $data['method'] = 'POST';
        }

        $form = $this->createForm($formType, $entity);
        $form = $this->applySecurity($form);

        if (!$this->getRequest()->isXmlHttpRequest()) {
            $form->add('close', SubmitType::class, [
                'label' => 'Sluiten',
                'attr' => [
                    'title' => 'Wijzigingen worden niet opgeslagen',
                    'swal' => 'confirm',
                    'data-title' => 'Uw wijzigingen worden niet opgeslagen!',
                    'data-type' => 'info',
                    'data-cancelText' => 'Annuleren',
                    'data-confirmText' => 'Bevestig',
                    'data-callback' => 'function () { window.location.href=\'' . $this->getRedirectUrl() . '\'; }',
                    'data-group' => 'actions',
                    'class' => 'pull-left'
                ],
                'icon' => 'cancel-circle2',
            ]);

            $formOptions = $this->getOption('form');

            if ($formOptions['allow_submit']) {
                $form->add('submit', SubmitType::class, [
                    'label' => 'Opslaan',
                    'icon' => 'reload-alt',
                    'attr' => [
                        'data-group' => 'actions',
                    ],
                ]);
            }

            if(!empty($formOptions)) {
                foreach($formOptions['buttons'] as $key => $buttonArray) {
                    $form->add($key, SubmitType::class, [
                        'label' => $buttonArray['label'],
                        'icon' => $buttonArray['icon'],
                        'attr' => [
                            'class' => 'btn-primary',
                            'data-group' => 'actions'
                        ]
                    ]);
                }
            }

            if ($formOptions['allow_submit_close']) {
                $form->add('submit_close', SubmitType::class, [
                    'label' => 'Opslaan en sluiten',
                    'attr' => [
                        'class' => 'btn-primary',
                        'data-group' => 'actions',
                    ],
                    'icon' => 'checkmark-circle',
                ]);
            }
        }

        return $form;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    protected function getFormType()
    {
        if (!$this->isResolved()) {
            $this->resolve();
        }

        if (!$this->formType && isset($this->options['form_type'])) {
            if (!class_exists($this->options['form_type'])) {
                throw new Exception('Form "' . $this->options['form_type'] . '" does not exist');
            }

            $this->formType = $this->options['form_type'];
        }

        return $this->formType;
    }

    /**
     * @param FormInterface $form
     *
     * @return JsonResponse
     */
    protected function autoComplete(FormInterface $form): JsonResponse
    {
        $searchString = parent::getRequest()->get('search');

        $response = (object)[
            'items' => [],
        ];

        if (null === $searchString || \strlen($searchString) < 3 || \in_array($searchString,
                $this->container->getParameter('search_ignored_search_strings'), true)) {
            return new JsonResponse($response);
        }

        $element = $this->getFormElement($form, parent::getRequest()->get('name'));

        if (null === $element) {
            return new JsonResponse($response);
        }

        $class = $element->getConfig()->getOption('class');
        $choiceLabel = $element->getConfig()->getOption('choice_label');

        if ($element->getConfig()->hasOption('query_builder') && null !== $element->getConfig()->getOption('query_builder')) {
            $qb = $element->getConfig()->getOption('query_builder');

            $alias = current($qb->getDQLPart('from'))->getAlias();
        } else {
            $alias = 'results';

            /** @var EntityManagerInterface $em */
            $em = parent::getDoctrine()->getManager();

            /** @var QueryBuilder $qb */
            $qb = $em->createQueryBuilder();

            $qb->select($alias);
            $qb->from($class, $alias);
        }

        $criteria = [];

        foreach ((array)$element->getConfig()->getOption('select2')['findBy'] as $reference) {
            $criteria[] = $qb->expr()->like($alias . '.' . $reference, ':search');
        }

        $qb->andWhere(
            \call_user_func_array([$qb->expr(), 'orX'], $criteria)
        );

        $query = $qb->getQuery();
        $query->setParameter('search', '%' . $searchString . '%');

        $items = [];
        $type = get_class($element->getConfig()->getType()->getInnerType());

        $propertyAccessor = new PropertyAccessor();
        foreach ($query->getResult() as $entity) {
            /** @var EntityInterface $entity */
            if ($choiceLabel) {
                if($type === EntityChoiceType::class) {
                    $label = $choiceLabel($entity->getId(), $propertyAccessor->getValue($entity, $element->getConfig()->getOption('field')));
                } else {
                    $label = $choiceLabel($entity);
                }
            } else {
                $label = (string)$entity;
            }

            $items[] = ['id' => $entity->getId(), 'text' => $label];
        }

        $response->items = $items;

        return new JsonResponse($response);
    }

    /**
     * @param FormInterface $form
     * @param               $name
     *
     * @return FormInterface|null
     */
    private function getFormElement(FormInterface $form, $name)
    {
        foreach ($form as $element) {
            if ($element->getConfig()->getOption('inherit_data')) {
                $element = $this->getFormElement($element, $name);

                if ($element) {
                    return $element;
                }
            } else {
                if ($element->getName() == $name) {
                    return $element;
                }
            }
        }

        return null;
    }

    /**
     *
     * @return string
     */
    public function getScreen()
    {
        return $this->screen;
    }

    /**
     * @Route("/{id}/bekijken", methods={"GET"})
     * @Breadcrumb()
     *
     * @param $id
     *
     * @return Response
     * @throws \Exception
     * @throws \RuntimeException
     * @throws \LogicException
     */
    public function viewAction($id)
    {
        $this->denyAccessUnlessGranted($this->getRolesFor('read'));

        $this->screen = self::SCREEN_VIEW;

        if (!$this->isResolved()) {
            $this->resolve();
        }

        if (null === $this->getOption('view')) {
            return $this->redirect($this->generateUrl($this->container->get(BaseRouteService::class)->getBaseRouteForAction('index')));
        }

        $entity = $this->getRepository()->find($id);

        if (!$entity) {
            throw new \RuntimeException('This id doesnt exist');
        }

        if (parent::getRequest()->get('name') && parent::getRequest()->get('search')) {

            /** @var EntityInterface $entity */
            $form = $this->getForm($this->getFormType(), $entity);

            return $this->autoComplete($form);
        }

        $className = \get_class($entity);
        $view = $this->getOption('view');

        $panels = [];
        foreach ($view['panels'] as $panel) {
            if ($panel['type'] === 'template') {
                $parameters = $panel['template_parameters'] ?? [];

                //parent entity gets always passed to panel
                $parameters[lcfirst($this->getEntityName($className))] = $entity;
                $panel['template'] = $this->render($panel['template'], $parameters);
            }

            $panels[] = $panel;
        }

        if (!empty($view['label'])) {
            $view['label'] .= ' ';
        }

        if ($view['label_show_subject'] && !empty($view['label_subject'])) {
            $view['label'] .= $this->generateViewLabelSubject($entity, $view['label_subject']);
        }

        $entityClass = explode('\\', strtolower($className));
        $entityClass = array_pop($entityClass);

        $template = 'AdminBundle:Crud:view.html.twig';
        if ($this->getRequest()->isXmlHttpRequest()) {
            $template = 'AdminBundle:Crud:view_xhr.html.twig';
        }


        return $this->render(
            $template,
            [
                'page_title' => $view['label'],
                'panels' => $panels,
                'entityId' => $entity->getId(),
                'entityClass' => $entityClass,
                'assets' => $this->getAssets(),
                'base_route' => $this->container->get(BaseRouteService::class)->getBaseRouteForAction('panelview'),
            ]
        );
    }

    /**
     * @param $entityClass
     *
     * @return array|mixed
     */
    public function getEntityName($entityClass)
    {
        $entityName = explode('\\', $entityClass);
        $entityName = end($entityName);

        return $entityName;
    }

    /**
     * @param $entity
     * @param $subjectLabel
     *
     * @return string
     */
    protected function generateViewLabelSubject($entity, $subjectLabel)
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        // If string contains %% we need to parse the keys with the PropertyAccessor
        if (strpos($subjectLabel, '%%') !== false) {
            $subjectLabel = preg_replace_callback('/%%([a-zA-Z]*)%%/',
                function ($matches) use ($accessor, $entity) {
                    return $accessor->getValue($entity, $matches[1]);
                }, $subjectLabel);
        }

        return $subjectLabel;
    }

    /**
     * @Route("/{id}/bekijken/panel/{panelId}", methods={"GET"})
     * @param $id
     * @param $panelId
     *
     * @return JsonResponse|Response
     * @throws Exception
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function panelViewAction($id, $panelId)
    {
        if (!$this->isResolved()) {
            $this->resolve();
        }

        $currentPanel = $this->getCurrentPanel($panelId);
        $entity = $this->getRepository()->find($id);

        if (null !== $entity && $currentPanel['type'] === 'template') {
            $parameters = isset($currentPanel['template_parameters']) ? $currentPanel['template_parameters'] : [];

            //parent entity gets always passed to panel
            $parameters[lcfirst($this->getEntityName(\get_class($entity)))] = $entity;
            $currentPanel['template'] = $this->renderView($currentPanel['template'], $parameters);
        } elseif ($currentPanel['type'] === 'datatable') {
            $currentPanel['datatable']['data_url'] = $this->getPanelActionUrl($id, $panelId, 'panelviewdata');
            $currentPanel['datatable']['reorder_url'] = $this->getPanelActionUrl($id, $panelId, 'reorder');

            $options = null;

            $dataTable = $this->container->get('admin.datatable.service')->getTable($currentPanel['datatable']);

            $currentPanel['template'] = $this->renderView('AdminBundle:Crud:datatable_xhr.html.twig', [
                    'datatable' => $dataTable,
                ]
            );
        }

        return $this->render(
            'AdminBundle:Crud:panel-body.html.twig',
            [
                'panel' => $currentPanel,
                'entity_id' => $entity->getId(),
            ]
        );
    }

    /**
     * @param $panelId
     *
     * @return mixed
     * @throws Exception
     */
    private function getCurrentPanel($panelId)
    {
        $view = $this->getOption('view');
        $currentPanel = array_filter($view['panels'], function ($panel) use ($panelId) {
            return $panel['id'] == $panelId;
        });

        if (!$currentPanel) {
            throw new Exception(sprintf("Can't find panel '%s'", $panelId));
        }

        return array_shift($currentPanel);
    }

    /**
     * @Route("/{id}/bekijken/panel/{panelId}/data", methods={"POST"})
     * @param $id
     * @param $panelId
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function panelViewDataAction($id, $panelId)
    {
        if (!$this->isResolved()) {
            $this->resolve();
        }

        $currentPanel = $this->getCurrentPanel($panelId);

        $currentPanel['datatable']['parent']['entity'] = $this->getEntityName($this->options['entity_class']);
        $parentEntity = $this->getRepository()->find($id);
        $currentPanel['datatable']['parent']['parentId'] = $parentEntity->getId();

        return $this->container->get('admin.datatable.service')->getData($currentPanel['datatable']);
    }

    /**
     * @Route("/{id}/bekijken/panel/{panelId}/herschikken", methods={"POST"})
     * @param Request $request
     * @param         $id
     * @param         $panelId
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function reorderAction(Request $request, $id, $panelId)
    {
        if (!$this->isResolved()) {
            $this->resolve();
        }

        $reorder = $request->get('reorder');
        $currentPanel = $this->getCurrentPanel($panelId);

        $currentPanel['datatable']['parent']['entity'] = $this->getEntityName($this->options['entity_class']);
        $parentEntity = $this->getRepository()->find($id);

        if (null === $parentEntity) {
            throw new \RuntimeException('Parent entity not found.');
        }

        $currentPanel['datatable']['parent']['parentId'] = $parentEntity->getId();

        return $this->container->get('admin.datatable.service')->reorder($currentPanel['datatable'], $reorder);
    }


    /**
     * @Route("/{id}/bekijken/child/{child}", methods={"GET"})
     * @param $id
     * @param $child
     *
     * @return null|JsonResponse|Response
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function childViewAction($id, $child)
    {
        if (!$this->isResolved()) {
            $this->resolve();
        }

        $this->getSelectedTab($child);

        if ($this->selectedTab['type'] === 'datatable') {
            $baseRoute = $this->container->get(BaseRouteService::class)->getBaseRouteForAction('childviewdata');
            $generatedUrl = $this->container->get('router')->generate($baseRoute, [
                'id' => $id,
                'child' => $child,
            ]);

            $this->selectedTab['datatable']['data_url'] = $generatedUrl;
            $this->selectedTab['datatable']['parent']['entity'] = $this->getEntityName($this->options['entity_class']);
            $parentEntity = $this->getRepository()->find($id);

            if (null === $parentEntity) {
                throw new \RuntimeException('Entity not found');
            }

            $this->selectedTab['datatable']['parent']['parentId'] = $parentEntity->getId();

            return new JsonResponse($this->container->get('admin.datatable.service')->getTable($this->selectedTab['datatable'],
                $parentEntity));
        }

        if ($this->selectedTab['type'] === 'template') {
            $entity = $this->getRepository()->find($id);

            if (null === $entity) {
                throw new \RuntimeException('Entity not found');
            }

            $template = $this->selectedTab['template'];

            if ($this->selectedTab['action'] !== null && method_exists($this, $this->selectedTab['action'])) {
                return $this->{$this->selectedTab['action']}($entity, $this->selectedTab);
            }

            //parent entity gets always passed to panel
            $template['template_parameters'][lcfirst($this->getEntityName(\get_class($entity)))] = $entity;

            return $this->render($template['template'], $template['template_parameters']);
        }

        return null;
    }

    /**
     * @param $child
     *
     * @return mixed
     */
    private function getSelectedTab($child)
    {
        foreach ($this->options['view']['panels'] as $panel) {
            if (isset($panel['tabs'])) {
                foreach ($panel['tabs'] as $tab) {
                    if ($tab['action'] == $child) {
                        $this->selectedTab = $tab;

                        return $this->selectedTab;
                    }
                }
            }
        }

        return null;
    }

    /**
     * @Route("/{id}/bekijken/{child}/data", methods={"POST"})
     * @param $id
     * @param $child
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function childViewDataAction($id, $child)
    {
        if (!$this->isResolved()) {
            $this->resolve();
        }

        $this->getSelectedTab($child);

        $this->selectedTab['datatable']['parent']['entity'] = $this->getEntityName($this->options['entity_class']);
        $parentEntity = $this->getRepository()->find($id);
        $this->selectedTab['datatable']['parent']['parentId'] = $parentEntity->getId();

        return $this->container->get('admin.datatable.service')->getData($this->selectedTab['datatable']);
    }

    /**
     * @Route("/nieuw", methods={"POST"})
     * @throws \Exception
     */
    public function newPersistAction()
    {
        $this->denyAccessUnlessGranted($this->getRolesFor('create'));

        $entity = $this->getEntity();

        return $this->persist($entity);
    }

    /**
     * @param $entity
     *
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    protected function persist($entity)
    {
        /** @var EntityInterface $entity */
        $form = $this->getForm($this->getFormType(), $entity);
        $form->handleRequest(parent::getRequest());

        if ($form->isSubmitted() && $form->isValid()) {

            if (method_exists($this->getFormType(), 'hydrateTranslations')) {
                \call_user_func_array([$this->getFormType(), 'hydrateTranslations'],
                    [&$entity, parent::getRequest()]);
            }

            $em = parent::getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('backend.success', 'Wijzigingen opgeslagen!');

            if (array_key_exists('submit_close', parent::getRequest()->get($form->getName()))) {
                return $this->redirect($this->getRedirectUrl());
            }

            if (isset($this->options['form']['buttons'])) {
                foreach ($this->options['form']['buttons'] as $key => $buttonArray) {
                    if (array_key_exists($key, parent::getRequest()->get($form->getName()))) {
                        return $this->redirectToRoute($buttonArray['route'], [
                            $buttonArray['route_parameter'] => $entity->getId(),
                        ]);
                    }
                }
            }

            return $this->redirectToRoute(parent::getRoute('edit'), [
                'id' => $entity->getId(),
            ]);
        }

        if (false === $form->isValid() && true === $this->getRequest()->isXmlHttpRequest()) {
            $errors = ['formError' => []];
            foreach ($form->getErrors(true) as $error) {
                $errors['formError'][] = $error->getMessage();
            }
            return new JsonResponse($errors, 422);
        }

        $this->get('session')->getFlashBag()->add('backend.error', 'Wijzigingen niet opgeslagen!');

        return $this->render(
            'AdminBundle:Crud:form.html.twig', [
            'entity' => $entity,
            'errors' => $form->getErrors(true),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/bewerken", methods={"GET"})
     * @Breadcrumb()
     *
     * @param $id
     *
     * @return Response
     * @throws \Exception
     */
    public function editAction($id)
    {
        $entity = $this->getRepository()->find($id);

        try {
            if (in_array(NonEditableEntity::class, class_uses($entity)) && !$entity->isEditable()) {
                throw new NonEditableException();
            }

            $this->denyAccessUnlessGranted($this->getRolesFor('update'));
        } catch (Exception $e) {
            return $this->redirectToRoute(
                parent::getRoute('view'),
                [
                    'id' => $entity->getId(),
                ]
            );
        }

        $this->screen = self::SCREEN_EDIT;

        return $this->form($entity);
    }

    /**
     * @Route("/{id}/bewerken", defaults={"id"=null}, methods={"POST"})
     *
     * @param $id
     *
     * @return Response
     * @throws \Exception
     */
    public function editPersistAction($id)
    {
        $this->denyAccessUnlessGranted($this->getRolesFor('update'));

        $entity = $this->getRepository()->find($id);

        try {
            return $this->persist($entity);
        } catch (NonEditableException $e) {
            $this->get('session')->getFlashBag()->add('backend.error', 'Dit item mag niet gewijzigd worden.');
        } catch (ImageUploadException $e) {
            $this->get('session')->getFlashBag()->add('backend.error', 'Het bestand kon niet geupload worden.');
        } catch (\Exception $e) {
            if (in_array($this->container->get('kernel')->getEnvironment(), ['dev', 'test'])) {
                throw $e;
            }
            $this->get('session')->getFlashBag()->add('backend.error',
                'Er is een onbekende fout opgetreden tijdens het wijzigen van dit item.');
        }

        return $this->redirectToRoute(
            parent::getRoute('edit'),
            [
                'id' => $entity->getId(),
            ]
        );
    }

    /**
     * @Route("/{id}/verwijderen", methods={"DELETE"})
     *
     * @param $id
     *
     * @return Response
     * @throws \ReflectionException
     */
    public function deleteAction($id)
    {
        $this->denyAccessUnlessGranted($this->getRolesFor('delete'));

        $this->screen = self::SCREEN_DELETE;

        $entity = $this->getRepository()->find($id);

        try {
            $em = parent::getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('backend.success', 'Het item is met success verwijderd!');
        } catch (NonDeletableException $e) {
            $this->get('session')->getFlashBag()->add('backend.error', 'Dit item mag niet verwijderd worden.');
        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add('backend.error',
                'Er is een onbekende fout opgetreden tijdens het verwijderen van dit item.');
        }

        return new Response();
    }

    /**
     * @Route("/verwijderen", methods={"DELETE"})
     *
     * @param Request $request
     *
     * @return CrudActionResponse
     * @throws \ReflectionException
     */
    public function multiDeleteAction(Request $request)
    {
        $this->denyAccessUnlessGranted($this->getRolesFor('delete'));

        $ids = $request->request->get('ids');

        try {
            if (!$ids) {
                throw new MissingMandatoryParametersException('Missing ids in POST request');
            }

            $em = parent::getDoctrine()->getManager();

            $items = $this->getRepository()->findBy([
                'id' => $ids,
            ]);

            foreach ($items as $item) {
                $em->remove($item);
            }

            $em->flush();
        } catch (Exception $e) {
            return new CrudActionResponse([
                'message' => [
                    'type' => 'error',
                    'text' => 'Er is een fout opgetreden tijdens het verwerken van het verzoek',
                ],
            ]);
        }

        return new CrudActionResponse([
            'message' => [
                'title' => 'Actie is succesvol uitgevoerd',
                'type' => 'success',
                'text' => 'De geselecteerde items zijn succesvol verwijderd.',
                'callback' => 'CrudRemoveSelectedItems',
            ],
        ]);
    }

    /**
     * @param FormInterface $form
     *
     * @return array
     */
    private function getFormElements(FormInterface $form)
    {
        $elements = [];

        foreach ($form as $element) {
            if ($element->getConfig()->getOption('inherit_data')) {
                $elements = array_merge($elements, $this->getFormElements($element));
            } else {
                array_push($elements, $element);
            }
        }

        return $elements;
    }

    /**
     * @param int    $id
     * @param string $panelId
     * @param string $action
     * @return string
     */
    private function getPanelActionUrl(int $id, string $panelId, string $action)
    {
        $baseRoute = $this->container->get(BaseRouteService::class)->getBaseRouteForAction($action);

        return $this->container->get('router')->generate($baseRoute, [
            'id' => $id,
            'panelId' => $panelId,
        ]);
    }

}
