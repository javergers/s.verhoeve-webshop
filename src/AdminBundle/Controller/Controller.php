<?php

namespace AdminBundle\Controller;

use AdminBundle\Interfaces\ControllerInterface;
use AppBundle\Entity\Security\Employee\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as SymfonyController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;
use Symfony\Component\Security\Core\Authorization\Voter\RoleVoter;

/**
 * Class Controller
 * @package AdminBundle\Controller
 */
class Controller extends SymfonyController implements ControllerInterface
{
    /**
     * @param null $n
     */
    public function getLabel($n = null)
    {
        // TODO: Implement getLabel() method.
    }

    /**
     * @param null   $attributes
     * @param null   $object
     * @throws \ReflectionException
     */
    public function denyAccessUnlessGranted($attributes = null, $object = null, $message = 'Access Denied.')
    {
        if (null === $object) {
            $object = $this;
        }

        $this->get('cms.role_manager')->denyAccessUnlessGranted($attributes, $object);
    }

    /**
     * @param null $attributes
     * @return bool
     * @throws \ReflectionException
     */
    public function userIsGrantedTo($attributes = null)
    {
        return $this->get('cms.role_manager')->userIsGrantedTo($attributes, $this);
    }

    /**
     * @param null $action
     * @return array
     * @throws \ReflectionException
     */
    public function getRolesFor($action = null)
    {
        return $this->get('cms.role_manager')->getRolesFor($this, $action);
    }

    /**
     *
     * @param FormInterface $form
     *
     * @return FormInterface
     * @throws \ReflectionException
     */
    protected function applySecurity($form)
    {
        $hiddenFields = [];

        if ($form->getRoot()->getConfig()->getDataClass()) {
            $hiddenFields = $this->get('cms.role_manager')->getHiddenFields($form->getRoot()->getConfig()->getDataClass());
        }

        /** @var FormInterface $child */
        foreach ($form->all() as $child) {

            if ($child->getConfig()->getOption('security')) {
                $security = $child->getConfig()->getOption('security');

                if (null !== $child->getParent() && !$this->getBuilderAccessDecisionManager($security['strategy'])->decide($this->get('security.token_storage')->getToken(),
                    $security['roles'])) {
                    $child->getParent()->remove($child->getName());
                }
            }

            if (null !== $child->getParent() && \in_array($child->getName(), $hiddenFields, true)) {
                $child->getParent()->remove($child->getName());
            }

            if ($child->all()) {
                $this->applySecurity($child);
            }
        }

        return $form;
    }

    /**
     *
     * @param $strategy
     *
     * @return AccessDecisionManager
     */
    protected function getBuilderAccessDecisionManager($strategy = AccessDecisionManager::STRATEGY_AFFIRMATIVE)
    {
        $accessDecisionManager = new AccessDecisionManager(
            [
                new RoleVoter(),
            ],
            $strategy,
            false,
            true
        );

        return $accessDecisionManager;
    }

    /**
     * @param $action
     * @return string
     */
    protected function getRoute($action)
    {
        $parts = explode('_', $this->getRequest()->get('_route'));

        array_pop($parts);

        return implode("_", $parts) . "_" . $action;
    }

    /**
     * @param string $action
     * @param array  $params
     * @return string
     */
    public function getUrl(string $action, $params = [])
    {
        return $this->generateUrl($this->getRoute($action), $params);
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->get('request_stack')->getMasterRequest();
    }

    /**
     * @return null|User
     */
    protected function getUser(): ?User
    {
        return parent::getUser();
    }
}
