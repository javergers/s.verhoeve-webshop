<?php

namespace AdminBundle\Controller\Letter;

use AppBundle\Entity\Catalog\Product\Letter;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Form\Cart\LetterType;
use RuntimeException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LetterController
 * @package AdminBundle\Controller\Letter
 * @Route("/brief")
 */
class LetterController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->redirectToRoute('admin_dashboard_index');
    }

    /**
     * @Route("/bewerken/{orderLine}/{uuid}", requirements={
     *     "uuid": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *     "orderLine": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     * }, methods={"GET", "POST"})
     *
     * @ParamConverter("orderLine", options={"mapping": { "orderLine" = "uuid"}})
     *
     * @Template("@App/Letter/edit.html.twig")
     *
     * @param Request   $request
     * @param string    $uuid
     * @param OrderLine $orderLine
     * @return array|Response
     */
    public function editAction(Request $request, $uuid, OrderLine $orderLine)
    {
        $product = $orderLine->getProduct();

        if (!$product instanceof Letter) {
            throw new RuntimeException('Product is not an letter');
        }

        $form = $this->createForm(LetterType::class, null, [
            'action' => $this->generateUrl($request->get('_route'), $request->get('_route_params')),
            'method' => 'POST',
            'attr' => [
                'class' => 'form',
                'novalidate' => 'novalidate',
                'id' => 'letter_form',
            ],
        ]);

        $letter = $this->get('app.letter');

        $letter->setUuid($uuid);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $file */
            if ($form->has('file') && null !== ($file = $form->get('file')->getData())) {
                $allowedExtensions = ['pdf'];
                $allowedMimeTypes = ['application/pdf'];

                if (!\in_array($file->guessClientExtension(), $allowedExtensions,
                        true) || !\in_array($file->getClientMimeType(), $allowedMimeTypes, true)) {
                    $form->addError(new FormError('Deze extensie is niet geldig'));
                }
            } else {
                $form->addError(new FormError('Geen brief geupload'));
            }

            if ($form->getErrors()->count() === 0) {
                try {
                    $letter->uploadFile($file->getPathname());

                    return new Response('');
                } catch (\RuntimeException $e) {
                    $form->addError(new FormError('Bestand kon niet worden geupload'));
                }
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
