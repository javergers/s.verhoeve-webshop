<?php

namespace AdminBundle\Controller\Settings\Geographic;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Settings\Geographic\PostcodeType as PostcodeDatatableType;
use AdminBundle\Form\Settings\Geographic\PostcodeType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Geography\Postcode;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/geografisch/postcodes")
 */
class PostcodeController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Postcodes';
    }

    public function getOptions()
    {
        return [
            'entity_class' => Postcode::class,
            'form_type' => PostcodeType::class,
            'datatable' => [
                'entity_class' => Postcode::class,
                'datatable_type' => PostcodeDatatableType::class,
                'form_type' => PostcodeType::class,
            ],
            'sidebar_menu' => 'AdminBundle:Geographic:menu',
        ];
    }
}
