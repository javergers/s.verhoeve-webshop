<?php

namespace AdminBundle\Controller\Settings\Geographic;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Settings\Geographic\CityType as CityDatatableType;
use AdminBundle\Form\Settings\Geographic\CityType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Geography\City;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/geografisch/plaatsen")
 */
class CityController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Steden';
    }

    public function getOptions()
    {
        return [
            'entity_class' => City::class,
            'form_type' => CityType::class,
            'datatable' => [
                'entity_class' => City::class,
                'datatable_type' => CityDatatableType::class,
                'form_type' => CityType::class,
            ],
            'sidebar_menu' => 'AdminBundle:Geographic:menu',
        ];
    }
}
