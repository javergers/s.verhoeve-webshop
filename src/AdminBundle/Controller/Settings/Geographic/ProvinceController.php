<?php

namespace AdminBundle\Controller\Settings\Geographic;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Settings\Geographic\ProvinceType as ProvinceDatatableType;
use AdminBundle\Form\Settings\Geographic\ProvinceType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Geography\Province;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/geografisch/provincies")
 */
class ProvinceController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Provincies';
    }

    public function getOptions()
    {
        return [
            'entity_class' => Province::class,
            'form_type' => ProvinceType::class,
            'datatable' => [
                'entity_class' => Province::class,
                'datatable_type' => ProvinceDatatableType::class,
                'form_type' => ProvinceType::class,
            ],
            'sidebar_menu' => 'AdminBundle:Geographic:menu',
        ];
    }
}
