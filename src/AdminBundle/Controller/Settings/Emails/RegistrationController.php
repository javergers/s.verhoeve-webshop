<?php

namespace AdminBundle\Controller\Settings\Emails;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Site;
use AppBundle\Services\Mailer;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Services\Mailer\RegistrationMailer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/instellingen/emails/voorbeeld/registratie")
 */
class RegistrationController extends Controller
{
    /**
     * @Route("/");
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function previewAction(Request $request)
    {
        $siteId = (int)$request->get('site', 1);
        $companyId = (int)$request->get('company');
        $customerId = (int)$request->get('customer', 1);

        $em = $this->get('doctrine')->getManager();

        $site = $em->find(Site::class, $siteId);
        $company = $em->find(Company::class, $companyId);
        $customer = $em->find(Customer::class, $customerId);

        if (null === $site) {
            throw new \RuntimeException('Site not found.');
        }

        if (null === $customer) {
            throw new \RuntimeException('Customer not found.');
        }

        $data = null;
        $this->get(RegistrationMailer::class)->send([
            'site' => $site,
            'company' => $company,
            'customer' => $customer
        ], function (\Swift_Message $message) use (&$data) {
            $data = $message->getBody();
            return false;
        });

        return new Response($data);
    }
}
