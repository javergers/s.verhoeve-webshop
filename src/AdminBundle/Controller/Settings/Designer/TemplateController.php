<?php

namespace AdminBundle\Controller\Settings\Designer;

use AdminBundle\Controller\CrudController;
use AdminBundle\Form\Settings\Designer\TemplateType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Designer\DesignerTemplate;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/designer/templates")
 *
 */
class TemplateController extends CrudController implements CrudControllerInterface
{
    public function getLabel($n = null)
    {
        return 'Templates';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => DesignerTemplate::class,
            'form_type' => TemplateType::class,
            'datatable' => [
                'entity_class' => DesignerTemplate::class,
                'datatable_type' => DesignerTemplate::class,
                'form_type' => TemplateType::class,
            ],
        ];
    }

}
