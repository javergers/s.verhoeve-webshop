<?php

namespace AdminBundle\Controller\Settings\Designer;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Settings\Designer\TagType as DatatableTagType;
use AdminBundle\Form\Settings\Designer\TagType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Designer\Tag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/designs/labels")
 */
class TagController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Labels';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => Tag::class,
            'form_type' => TagType::class,
            'datatable' => [
                'entity_class' => Tag::class,
                'form_type' => TagType::class,
                'datatable_type' => DatatableTagType::class,
            ],
        ];
    }

}
