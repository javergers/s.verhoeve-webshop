<?php

namespace AdminBundle\Controller\Settings\Designer;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Settings\Designer\DesignerTemplateDatatableType;
use AppBundle\Exceptions\NonEditableException;
use AdminBundle\Form\Settings\Designer\DesignerTemplateFormType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Designer\DesignerTemplate;
use AppBundle\Traits\NonEditableEntity;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Exception;
use ReflectionException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/designs/templates")
 */
class DesignerTemplateController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Templates';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'label' => 'Templates',
            'title' => function (DesignerTemplate $designerTemplate) {
                return $designerTemplate->getName();
            },
            'form_type' => DesignerTemplateFormType::class,
            'form' => [
                'allow_submit_close' => false,
                'allow_submit' => true,
                'buttons' => [
                    'submit_design' => [
                        'route' => 'admin_settings_designer_designertemplate_saveanddesign',
                        'route_parameter' => 'template',
                        'label' => 'Opslaan en ontwerpen'
                    ],
                ]
            ],
            'entity_class' => DesignerTemplate::class,
            'datatable' => [
                'form_type' => DesignerTemplateFormType::class,
                'entity_class' => DesignerTemplate::class,
                'datatable_type' => DesignerTemplateDatatableType::class,
                'paginator' => [
                    'options' => [
                        'wrap-queries' => true,
                    ],
                ],
            ],
            'assets' => [
                'admin/js/ContentWindow.js',
                'admin/css/ContentWindow.css',
                'admin/js/order/designer.js',
            ],
            'view' => [
                'label_subject' => '%%name%%',
                'panels' => [
                    [
                        'id' => 'information_panel',
                        'type' => 'template',
                        'template' => '@Admin/Designer/Template/information.html.twig',
                    ],
                ],
            ],
        ];
    }

    /**
     * @Route("/{template}/personaliseren", defaults={"template"=null}, methods={"GET"})
     *
     * @param Request          $request
     * @param DesignerTemplate $template
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws ReflectionException
     */
    public function saveAndDesignAction(Request $request, DesignerTemplate $template)
    {
        $this->denyAccessUnlessGranted($this->getRolesFor('update'));

        $callback = 'after_design_saved';

        return $this->render('@Admin/Designer/Template/personalize.html.twig', [
            'design_url' => $this->generateUrl('admin_designer_designer_edit', [
                'uuid' => $template->getUuid(),
                'is-pre-design' => true,
                'callback' => 'window.parent.' . $callback
            ]),
            'after_design_saved' => $callback,
            'callback_url' => $this->generateUrl('admin_settings_designer_designertemplate_index')
        ]);
    }
}
