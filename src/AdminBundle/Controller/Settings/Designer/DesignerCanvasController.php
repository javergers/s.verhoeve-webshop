<?php

namespace AdminBundle\Controller\Settings\Designer;

use AdminBundle\Controller\CrudController;
use AdminBundle\Interfaces\CrudControllerInterface;
use Symfony\Component\Routing\Annotation\Route;
use AdminBundle\Datatable\Settings\Designer\DesignerCanvasDatatableType;
use AdminBundle\Form\Settings\Designer\DesignerCanvasFormType;
use AppBundle\Entity\Designer\DesignerCanvas;

/**
 * @Route("/designs/vormen")
 */
class DesignerCanvasController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Vormen';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'label' => 'Vormen',
            'title' => function (DesignerCanvas $designerCanvas) {
                return $designerCanvas->getName();
            },
            'entity_class' => DesignerCanvas::class,
            'form_type' => DesignerCanvasFormType::class,
            'datatable' => [
                'entity_class' => DesignerCanvas::class,
                'form_type' => DesignerCanvasFormType::class,
                'datatable_type' => DesignerCanvasDatatableType::class,
                'paginator' => [
                    'options' => [
                        'wrap-queries' => true,
                    ],
                ],
            ],
        ];
    }
}

