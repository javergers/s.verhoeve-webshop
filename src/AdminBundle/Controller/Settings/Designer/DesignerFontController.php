<?php

namespace AdminBundle\Controller\Settings\Designer;

use AdminBundle\Controller\CrudController;
use AdminBundle\Interfaces\CrudControllerInterface;
use Symfony\Component\Routing\Annotation\Route;
use AdminBundle\Datatable\Settings\Designer\DesignerFontDatatableType;
use AdminBundle\Form\Settings\Designer\DesignerFontFormType;
use AppBundle\Entity\Designer\DesignerFont;

/**
 * @Route("/designs/lettertypen")
 *
 */
class DesignerFontController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Lettertypen';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'label' => 'Lettertypen',
            'title' => function (DesignerFont $designerFont) {
                return $designerFont->getFamily();
            },
            'entity_class' => DesignerFont::class,
            'form_type' => DesignerFontFormType::class,
            'datatable' => [
                'entity_class' => DesignerFont::class,
                'form_type' => DesignerFontFormType::class,
                'datatable_type' => DesignerFontDatatableType::class,
                'paginator' => [
                    'options' => [
                        'wrap-queries' => true
                    ]
                ],
            ]
        ];
    }
}
