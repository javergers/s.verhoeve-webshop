<?php

namespace AdminBundle\Controller\Settings;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Settings\SiteType as SiteTypeDatatable;
use AdminBundle\Form\Settings\SiteType;
use AdminBundle\Form\Settings\SubSiteFormType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Site\Site;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\RedirectResponse;
use \Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/instellingen/sites")
 */
class SiteController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Websites';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => Site::class,
            'form_type' => SiteType::class,
            'datatable' => [
                'entity_class' => Site::class,
                'form_type' => SiteType::class,
                'datatable_type' => SiteTypeDatatable::class,
                'paginator' => [
                    'options' => [
                        'wrap-queries' => true,
                    ],
                ],
                'actions' => [
                    [
                        'icon' => 'mdi mdi-plus',
                        'route' => 'admin_settings_site_addsubsite',
                        'label' => 'Subsite toevoegen',
                        'className' => 'btn-success',
                    ],
                ],
            ],
            'assets' => [
                'admin/js/settings/site.js',
            ],
        ];
    }

    /**
     * @Route("/subsite/nieuw", methods={"GET", "POST"})
     * @Template("@Admin/Crud/form.html.twig")
     * @param Request $request
     * @return array|RedirectResponse
     */
    public function addSubSite(Request $request)
    {
        $form = $this->createForm(SubSiteFormType::class, new Site());
        if (null !== ($redirect = $this->handleForm($request, $form))) {
            return $redirect;
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{site}/bewerken", methods={"GET", "POST"})
     * @param Request $request
     * @param Site    $site
     * @return Response
     */
    public function redirectEditSite(Request $request, Site $site)
    {
        if ($site->getParent() !== null) {
            return $this->redirectToRoute('admin_settings_site_editsubsite', [
                'site' => $site->getId(),
            ]);
        }
        $thisClass = \get_class($this);
        if ($request->getMethod() === 'POST') {
            foreach ($site->getChildren() as $child) {
                $child->syncWithParent();
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->forward($thisClass . '::editPersistAction', [
                'id' => $site->getId(),
            ]);
        }
        return $this->forward($thisClass . '::editAction', [
            'id' => $site->getId(),
        ]);
    }

    /**
     * @Route("/subsite/{site}/bewerken", methods={"GET", "POST"})
     * @Template("@Admin/Crud/form.html.twig")
     * @param Request $request
     * @param Site    $site
     * @return array
     */
    public function editSubSite(Request $request, Site $site)
    {
        if ($request->getMethod() === 'POST') {
            $site->syncWithParent();
        }
        $form = $this->createForm(SubSiteFormType::class, $site);
        $this->handleForm($request, $form);

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @param Request       $request
     * @param FormInterface $form
     * @return null|RedirectResponse
     */
    private function handleForm(Request $request, FormInterface $form)
    {
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $site = $form->getData();

            $site->syncWithParent();
            $em = $this->getDoctrine()->getManager();
            $em->persist($site);
            $em->flush();

            return $this->redirectToRoute('admin_settings_site_editsubsite', [
                'site' => $site->getId(),
            ]);
        }
        return null;
    }
}
