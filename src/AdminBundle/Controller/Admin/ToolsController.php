<?php

namespace AdminBundle\Controller\Admin;

use AppBundle\Entity\Security\Employee\User;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/hulpmiddelen")
 */
class ToolsController extends Controller
{
    /**
     * @Route()
     * @Template
     * @param Request $request
     * @throws \Exception
     */
    public function indexAction(Request $request)
    {
        if ($request->get('reset') !== null) {
            $this->container->get('reset')->execute();
        }
    }

    /**
     * @Route("/cache/clear")
     * @return RedirectResponse
     */
    public function clearCacheAction()
    {
        $console = \dirname($this->get('kernel')->getRootDir()) . '/bin/console';

        exec(sprintf('php ' . $console . ' cache:clear --env=%s', $this->get('kernel')->getEnvironment()));

        /** @var User $user */
        $user = $this->getUser();

        $this->get('logger')->alert(sprintf("Symfony cached cleared by '%s'", $user->getDisplayName()));

        $this->get('session')->getFlashBag()->add('backend.success', 'Symfony cache is gewist');

        return $this->redirectToRoute('admin_admin_tools_index');
    }
}
