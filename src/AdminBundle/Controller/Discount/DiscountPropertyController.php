<?php

namespace AdminBundle\Controller\Discount;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Discount\DiscountPropertyDatatableType;
use AdminBundle\Form\Discount\DiscountPropertyFormType;
use AppBundle\Entity\Discount\DiscountProperty;
use AppBundle\Interfaces\Sales\OrderCollectionInterface;
use RuleBundle\Entity\Entity;
use RuleBundle\Service\ResolverService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/instellingen/regels/kortingsvelden")
 * Class DiscountPropertyController
 * @package AdminBundle\Controller\Discount
 */
class DiscountPropertyController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => DiscountProperty::class,
            'form_type' => DiscountPropertyFormType::class,
            'datatable' => [
                'entity_class' => DiscountProperty::class,
                'form_type' => DiscountPropertyFormType::class,
                'datatable_type' => DiscountPropertyDatatableType::class,
                'allow_edit' => false,
            ],
        ];
    }
}