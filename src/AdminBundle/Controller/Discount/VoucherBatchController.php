<?php

namespace AdminBundle\Controller\Discount;

use AdminBundle\Components\Crud\CrudActionResponse;
use AdminBundle\Annotation\Breadcrumb;
use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Discount\BatchVoucherDatatable;
use AdminBundle\Datatable\Discount\VoucherBatchDiscountDatatableType;
use AdminBundle\Form\Discount\VoucherBatchFormType;
use AdminBundle\Form\Discount\VoucherFormType;
use AppBundle\Entity\Discount\Voucher;
use AppBundle\Entity\Discount\VoucherBatch;
use AppBundle\Services\JobManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use JMS\JobQueueBundle\Entity\Job;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;

/**
 * @Route("/vouchers/batch")
 * Class VoucherBatchController
 * @package AdminBundle\Controller\Discount
 */
class VoucherBatchController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'form_type' => VoucherBatchFormType::class,
            'entity_class' => VoucherBatch::class,
            'datatable' => [
                'form_type' => VoucherBatchFormType::class,
                'entity_class' => VoucherBatch::class,
                'datatable_type' => VoucherBatchDiscountDatatableType::class,
                'allow_edit' => true,
                'allow_read' => true,
                'query_builder' => function (QueryBuilder $qb) {
                    return $qb->orderBy('voucher_batch.id', 'DESC');
                },
                'bulk_actions' => [
                    [
                        'route' => 'admin_discount_voucherbatch_extend',
                        'label' => 'Actie verlengen',
                        'xhr' => true,
                        'multiple' => true,
                        'pre_confirm' => [
                            'title' => 'Actie verlengen',
                            'text' => 'Hoeveel dagen moeten de geselecteerde voucher batches verlengd worden?',
                            'type' => 'question',
                            'input' => 'range',
                            'confirmButtonColor' => '#5cb85c',
                            'inputAttributes' => [
                                'min' => 1,
                                'max' => 60,
                                'step' => 1,
                            ],
                            'inputValue' => 1,
                        ],
                        'method' => 'POST',
                        'icon' => 'mdi mdi-plus',

                    ],
                ],
            ],
            'view' => [
                'panels' => [
                    [
                        'id' => 'description-panel',
                        'type' => 'template',
                        'template' => '@Admin/Voucher/description-panel.html.twig',
                        'template_parameters' => [
                            'repository' => $this->getDoctrine()->getRepository(VoucherBatch::class),
                        ],
                        'title' => 'Batch bekijken',
                    ],
                    [
                        'id' => 'voucher-panel',
                        'title' => 'Vouchers',
                        'type' => 'datatable',
                        'datatable' => [
                            'datatable_type' => BatchVoucherDatatable::class,
                            'entity_class' => Voucher::class,
                            'form_type' => VoucherFormType::class,
                            'allow_read' => false,
                            'allow_edit' => false,
                            'results_per_page' => 50,
                            'parent_column' => 'batch',
                            'actions_processing' => 'override',
                            'actions' => [],
                            'query_builder' => function (QueryBuilder $qb) {
                                return $qb->orderBy('__result.id', 'DESC');
                            },
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @Route("/extend", methods={"POST"})
     * @param Request $request
     * @return CrudActionResponse
     */
    public function extendAction(Request $request)
    {
        $ids = $request->request->get('ids');
        $days = $request->request->get('data');

        try {
            $days = (int)$days;
            if($days === null || $days <= 0 || $days > 60){
                throw new \OutOfBoundsException('Days must be integer between 1 and 60');
            }

            if (!$ids) {
                throw new MissingMandatoryParametersException('Missing ids in POST request');
            }

            $em = $this->getDoctrine()->getManager();

            $batches = $this->getRepository()->findBy([
                'id' => $ids,
            ]);

            /** @var VoucherBatch $batch */
            foreach ($batches as $batch) {
                $newDate = clone $batch->getValidUntil();
                $newDate->modify(sprintf('+%d day', $days));

                $batch->setValidUntil($newDate);
                foreach($batch->getVouchers() as $voucher){
                    $voucher->setValidUntil($newDate);
                }
            }

            $em->flush();
        } catch (\Exception $e) {
            return new CrudActionResponse([
                'message' => [
                    'type' => 'error',
                    'text' => 'Er is een fout opgetreden tijdens het verwerken van het verzoek',
                ],
            ]);
        }

        return new CrudActionResponse([
            'message' => [
                'title' => 'Actie is succesvol uitgevoerd',
                'type' => 'success',
                'text' => 'De geselecteerde items zijn succesvol verlengd.',
                'callback' => 'CrudRefresh',
            ],
        ]);
    }

    /**
     * @Route("/{batch}/exporteren")
     * @param VoucherBatch $batch
     *
     * @return BinaryFileResponse
     *
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Writer_Exception
     */
    public function exportAction(VoucherBatch $batch)
    {
        $dataSet = $batch->getVouchers();
        $columns = [
            'description' => 'Beschrijving',
            'code' => 'Code',
            'valid_from' => 'Geldig vanaf',
            'valid_until' => 'Geldig tot',
        ];

        $excelService = $this->container->get('app.excel');
        $excelService->build([
            'filename' => $batch->getName(),
            'dataSet' => $dataSet,
            'columns' => $columns,
        ]);

        return $excelService->download();
    }

    /**
     * @Route("/nieuw", methods={"POST"})
     * @throws \Exception
     */
    public function newPersistAction()
    {
        $this->denyAccessUnlessGranted($this->getRolesFor('create'));

        $entity = $this->getEntity();

        $response = $this->persist($entity);

        $job = new Job('app:voucher:generate-batch', [$entity->getId()], true, 'voucher');
        $job->addRelatedEntity($entity);

        $this->container->get('job.manager')->addJob($job);
        $this->getDoctrine()->getManager()->flush();

        $this->get('session')->getFlashBag()->add('backend.success',
            'Batch succesvol aangemaakt. Vouchers worden in de achtergrond gegenereerd. Je ontvangt een email als deze is voltooid.');

        return $response;
    }
}
