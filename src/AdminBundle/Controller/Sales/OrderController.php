<?php

namespace AdminBundle\Controller\Sales;

use AdminBundle\Controller\Controller;
use AppBundle\Entity\Order\FraudDetectionReport;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Order\OrderInternalRemark;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyCustomField;
use AppBundle\Entity\Relation\CompanyCustomFieldOption;
use AppBundle\Entity\Relation\CompanyCustomOrderFieldValue;
use AppBundle\Entity\Site\Site;
use AppBundle\Exceptions\OrderStatusNotFoundException;
use AppBundle\Helper\Order\OrderStatusHelper;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Interfaces\PageView\PageViewControllerInterface;
use AppBundle\Manager\Order\BillingItemGroupManager;
use AppBundle\Manager\Order\OrderCollectionManager;
use AppBundle\Manager\Order\OrderManager;
use AppBundle\Manager\Order\OrderLineManager;
use AppBundle\Model\OrderModel;
use AppBundle\Repository\OrderRepository;
use AppBundle\Services\Discount\VoucherService;
use AppBundle\Services\Fraud\OrderFraudCheckService;
use AppBundle\Services\JobManager;
use AppBundle\Services\OrderSearch;
use AppBundle\Services\Parameter\ParameterService;
use AppBundle\Services\Sales\Order\Invoice;
use AppBundle\Services\Sales\Order\OrderActivityService;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Exception;
use function in_array;
use function is_array;
use JMS\JobQueueBundle\Entity\Job;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/bestellingen")
 * @Security("has_role('ROLE_ADMIN')")
 */
class OrderController extends Controller implements PageViewControllerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var JobManager
     */
    protected $jobManager;

    /**
     * @var OrderCollectionManager
     */
    private $orderCollectionManager;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * OrderController constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param JobManager $jobManager
     * @param OrderCollectionManager $orderCollectionManager
     * @param SessionInterface $session
     * @param ValidatorInterface $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        JobManager $jobManager,
        OrderCollectionManager $orderCollectionManager,
        SessionInterface $session,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->jobManager = $jobManager;
        $this->orderCollectionManager = $orderCollectionManager;
        $this->session = $session;
        $this->validator = $validator;
    }

    /**
     * @return array
     */
    public static function getControllerRoles()
    {
        return [];
    }

    /**
     * @param null $n
     *
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Orders';
    }

    /**
     * @Route("/", methods={"GET"})
     * @Template()
     *
     * @return array
     */
    public function indexAction()
    {
        if (!$this->session->has('sales.order.selectedSites')) {
            $this->session->set('sales.order.selectedSites', null);
        }

        if (!$this->session->has('sales.order.selectedStatuses')) {
            $this->session->set('sales.order.selectedStatuses', ['new']);
        }

        if (!$this->session->has('sales.order.selectedLimit')) {
            $this->session->set('sales.order.selectedLimit', 100);
        }

        return [
            'sites' => $this->getDoctrine()->getRepository(Site::class)->findBy(['parent' => null]),
            'statuses' => array_flip(OrderStatusHelper::getDescriptions()),
            'limits' => ['100', '250', '500'],
            'selectedSites' => $this->session->get('sales.order.selectedSites'),
            'selectedStatuses' => $this->session->get('sales.order.selectedStatuses'),
            'selectedLimit' => $this->session->get('sales.order.selectedLimit'),
        ];
    }

    /**
     * @Route("/factuur/{orderCollection}/verstuur-kopie", methods={"POST"})
     *
     * @param Request $request
     * @param OrderCollection $orderCollection
     *
     * @return JsonResponse
     * @throws BadRequestHttpException
     */
    public function sendInvoiceCopyAction(Request $request, OrderCollection $orderCollection)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new BadRequestHttpException();
        }

        if (!$request->request->get('email')) {
            throw new BadRequestHttpException();
        }

        $emailConstraint = new Email();

        if (\count($this->validator->validate($request->request->get('email'), $emailConstraint)) > 0) {
            throw new ValidatorException(sprintf('The given value "%s" is not a valid email address',
                $request->request->get('email')));
        }

        try {
            $job = new Job('topbloemen:shop:send-invoice', [
                $orderCollection->getId(),
                $request->request->get('email'),
            ], true, 'send_invoice');

            $job->addRelatedEntity($orderCollection);

            $this->jobManager->addJob($job);
            $this->entityManager->flush();

            return new JsonResponse([
                'success' => true,
            ]);
        } catch (Exception $e) {
            return new JsonResponse([
                'error' => true,
            ]);
        }
    }

    /**
     * @Route("/factuur/{orderCollection}/{filename}/{action}", requirements={"action" = "view|download"},
     *                                                          defaults={"action" = "view"})
     *
     * @param OrderCollection $orderCollection
     * @param string $filename
     * @param string $action
     *
     * @return Response
     */
    public function invoiceAction(OrderCollection $orderCollection, $filename, $action)
    {
        void($filename);

        return $this->get(Invoice::class)
            ->setItem($orderCollection)
            ->setAction($action)
            ->getResponse();
    }


    /**
     * @Route("/typeahead")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function typeaheadAction(Request $request): JsonResponse
    {
        $searchString = $request->get('q');

        if (!$searchString) {
            return new JsonResponse([]);
        }

        $searchStringLength = \strlen($searchString);

        if ($searchStringLength < 8 && is_numeric($searchString)) {
            /** @var \PDOStatement $stmt */
            $stmt = $this->getDoctrine()->getConnection()->prepare('
                SELECT `number` AS v 
                FROM `order_collection`
                WHERE `number` LIKE :search
            ');
            $stmt->bindValue('search', $searchString . '%');
            $stmt->execute();

            $orders = $stmt->fetchAll(\PDO::FETCH_COLUMN);

            return new JsonResponse($orders);
        }

        if ($searchStringLength >= 8 && is_numeric($orderCollectionNumber = substr($searchString, 0, 8))) {

            /** @var \PDOStatement $stmt */
            $stmt = $this->getDoctrine()->getConnection()->prepare("
                SELECT CONCAT_WS('-', `order_collection`.`number`, `order`.`number`) AS v
                FROM `order_collection`
                LEFT JOIN `order` ON `order_collection`.`id` = `order`.`order_collection_id`
                WHERE `order_collection`.`number` = :orderCollectionNumber
                AND `order`.`number` LIKE :search
            ");
            $stmt->bindValue('orderCollectionNumber', $orderCollectionNumber);
            $stmt->bindValue('search', substr($searchString, 9) . '%');
            $stmt->execute();

            $orders = $stmt->fetchAll(\PDO::FETCH_COLUMN);

            return new JsonResponse($orders);
        }

        return new JsonResponse([]);
    }

    /**
     * @Route("/zoeken")
     * @Template()
     *
     * @param Request $request
     *
     * @return array
     * @throws Exception
     */
    public function searchAction(Request $request)
    {
        $searchQuery = $request->get('q');
        $searchSites = $request->get('sites');
        $searchStatuses = $request->get('statuses');
        $searchLimit = $request->get('limit', 100);

        if (empty($searchSites)) {
            $searchSites = null;
        }

        if (empty($searchStatuses)) {
            $searchStatuses = null;
        }

        // Save lastest search settings into session.
        $this->session->set('sales.order.selectedSites', $searchSites);
        $this->session->set('sales.order.selectedStatuses', $searchStatuses);
        $this->session->set('sales.order.selectedLimit', $searchLimit);

        $systemParameters = $this->get(ParameterService::class)->setEntity();

        // todo: filter results same as general datatable.
        if (false === empty($searchQuery)) {
            $orders = $this->get(OrderSearch::class)->search($searchQuery, $searchSites, $searchStatuses, $searchLimit);
        } else {
            $em = $this->entityManager;

            /** @var OrderRepository $orderRepository */
            $orderRepository = $em->getRepository(Order::class);

            $qb = $orderRepository->createQueryBuilder('o');
            $qb->addSelect('o');

            $qb->leftJoin('o.orderCollection', 'c');
            $qb->addSelect('c');

            if (false !== is_array($searchSites)) {
                $qb->andWhere('c.site IN (:sites)');
                $qb->setParameter('sites', $searchSites, Connection::PARAM_INT_ARRAY);
            }

            if (false !== is_array($searchStatuses)) {
                $qb->andWhere('o.status IN (:statuses)');
                $qb->setParameter('statuses', $searchStatuses, Connection::PARAM_STR_ARRAY);

                $qb->addOrderBy('o.deliveryDate', in_array('new', $searchStatuses, true) ? 'ASC' : 'DESC');
            }

            $qb->addOrderBy('o.id', 'DESC');
            $qb->addOrderBy('o.priority', 'DESC');

            $qb->setMaxResults($searchLimit);

            $orders = $qb->getQuery()->getResult();
        }

        return [
            'fraudWarningThreshold' => $systemParameters->getValue('fraud_warning_score'),
            'fraudBlockTreshold' => $systemParameters->getValue('fraud_blocked_score'),
            'orders' => $orders,
        ];
    }

    /**
     * @Route("/{orderCollection}")
     * @Template()
     *
     * @param OrderCollection $orderCollection
     *
     * @param Request $request
     *
     * @return array|JsonResponse
     * @throws OrderStatusNotFoundException
     */
    public function orderAction(OrderCollection $orderCollection, Request $request)
    {
        if ($request->isXmlHttpRequest() && $request->query->has('check_drafts')) {
            $finalizeUrl = $this->generateUrl(
                'admin_sales_order_finalizedraft', ['orderCollection' => $orderCollection->getId()]
            );
            $hasDrafts = $this->orderCollectionManager->hasDraftOrders($orderCollection);
            $needsFinalization = $this->get(BillingItemGroupManager::class)->checkByOrderCollection($orderCollection);

            return $this->json([
                'finalizeUrl' => $finalizeUrl,
                'hasDrafts' => $hasDrafts,
                'needsFinalization' => $needsFinalization,
            ]);
        }

        $systemParameters = $this->get(ParameterService::class)->setEntity();

        $fraudDetectionReports = $this->getDoctrine()->getRepository(FraudDetectionReport::class)
            ->findBy(['orderCollection' => $orderCollection->getId()]);

        $fraudDetection = [];
        /** @var FraudDetectionReport $fraudDetectionReport */
        foreach ($fraudDetectionReports as $fraudDetectionReport) {
            $fraudDetection[] = [
                'score' => $fraudDetectionReport->getFraudDetectionRule()->getScore(),
                'description' => $fraudDetectionReport->getFraudDetectionRule()->getRule()->getDescription(),
            ];
        }

        /** @var EntityManagerInterface $em */
        $em = $this->entityManager;

        $rsm = new ResultSetMapping();
        $rsm->addEntityResult(Order::class, 'order');
        $rsm->addFieldResult('order', 'id', 'id');
        $rsm->addFieldResult('order', 'number', 'number');
        $rsm->addFieldResult('order', 'delivery_date', 'deliveryDate');
        $rsm->addFieldResult('order', 'priority', 'priority');
        $rsm->addFieldResult('order', 'status', 'status');
        $rsm->addMetaResult('order', 'force_manual', 'force_manual');

        $query = $em->createNativeQuery(
            'SELECT `id`, `number`, `delivery_date`, `status`, `priority`, `force_manual` FROM `order` WHERE `order_collection_id` = ?',
            $rsm
        );
        $query->setParameter(1, $orderCollection->getId());

        $orders = $query->getScalarResult();

        $array_map = [];

        foreach ($orders as $key => $order) {
            $order['order_number'] = OrderModel::parseNumber($order['order_number']);
            $array_map[$key] = $order;
        }

        $orders = $array_map;

        $orderStatuses = OrderStatusHelper::getDescriptions();
        $orderStatusesArray = [];

        foreach ($orderStatuses as $key => $description) {
            $orderStatusesArray[$key] = [
                'description' => $description,
                'class' => OrderStatusHelper::getLabelClass($key),
            ];
        }

        $companyCustomFieldRepository = $this->getDoctrine()->getRepository(CompanyCustomOrderFieldValue::class);
        $customFields = $companyCustomFieldRepository->findBy([
            'orderCollection' => $orderCollection->getId(),
        ]);

        return [
            'fraudWarningThreshold' => $systemParameters->getValue('fraud_warning_score'),
            'fraudBlockTreshold' => $systemParameters->getValue('fraud_blocked_score'),
            'orderCollection' => $orderCollection,
            'extra_fields' => $orderCollection->getMetadata()['extra_fields'] ?? null,
            'fraudFields' => $this->get(OrderFraudCheckService::class),
            'fraudDetection' => $fraudDetection,
            'orders' => $orders,
            'orderStatuses' => $orderStatusesArray,
            'needsFinalization' => $this->get(BillingItemGroupManager::class)->checkByOrderCollection($orderCollection),
        ];
    }

    /**
     * @Route("/{orderCollection}/maak-concepten-definitief", methods={"POST"})
     *
     * @param OrderCollection $orderCollection
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function finalizeDraft(OrderCollection $orderCollection, Request $request): JsonResponse
    {

        /** Ordercollection $draftOrders */
        $draftOrders = $this->orderCollectionManager->getDraftOrders($orderCollection);

        $finalize_message = $request->get('finalize_message');

        $this->orderCollectionManager->addMetaMessage(
            $orderCollection, 'finalize_messages', $finalize_message
        );

        $hasAuthorizedInvoice = $this->orderCollectionManager->hasAuthorizedInvoice($orderCollection);
        
        $this->orderCollectionManager->processDraftOrders($orderCollection, $draftOrders);

        $payment = $this->orderCollectionManager->createPaymentOrNull($orderCollection);

        $emailSent = $this->orderCollectionManager->sendFinalizeDraftMail($orderCollection, $payment);

        return $this->json([
            'success' => true,
            'invoice' => $hasAuthorizedInvoice,
            'emailSent' => $emailSent,
            'paymentRequestSent' => $payment ? true : false,
        ]);
    }

    /**
     * @Route("/orderOrder/", methods={"POST"})
     * @Template("@Admin/Sales/Order/order_order.html.twig")
     * @param Request $request
     *
     * @return array
     */
    public function orderOrderAction(Request $request): array
    {
        $order = $this->getDoctrine()->getRepository(Order::class)->find($request->get('order'));

        return [
            'order' => $order,
        ];
    }

    /**
     * @Route("/interne-opmerking/{order}", methods={"GET"})
     * @Template()
     *
     * @param Order $order
     *
     * @return array
     */
    public function internalRemarkAction(Order $order): array
    {
        return [
            'order' => $order,
        ];
    }

    /**
     * @Route("/interne-leverancier-opmerking/{company}", methods={"GET", "POST"})
     * @Template()
     *
     * @param Request $request
     * @param Company $company
     *
     * @return array|JsonResponse
     */
    public function internalSupplierRemark(Request $request, Company $company)
    {
        if (null === ($user = $this->getUser())) {
            throw new \RuntimeException('No user found');
        }

        if ($request->isMethod('post')) {
            $prefix = '[' . date('m-d-y H:i') . '] ' . $user->getFirstname() . ': ';
            $remark = trim($request->get('internal_remark'));

            if (\strlen($remark) > 0) {
                $remark = $prefix . $remark;
            }

            $internalRemark = $company->getInternalRemark();

            $separator = '';
            if (null !== $internalRemark && \strlen($internalRemark) > 0) {
                $separator = "\r\n";
            }

            $company->setInternalRemark($internalRemark . $separator . $remark);
            $this->entityManager->flush();

            return new JsonResponse($internalRemark);
        }

        return ['supplier' => $company];
    }

    /**
     * @Route("/interne-opmerking/{order}", methods={"POST"})
     *
     * @param Request $request
     * @param Order $order
     *
     * @return JsonResponse|Response
     * @throws Exception
     */
    public function internalRemarkPersistAction(Request $request, Order $order)
    {
        $remark = trim($request->get('internal-remark'));
        $remark = trim(preg_replace('/[a-z]*\s\d{1,2}\s[a-z]*\s\d{4}\s\d{1,2}:\d{1,2}\s-\s.+(?=:):$/i', '',
            $remark));

        if (empty($remark)) {

            return new JsonResponse([
                'message' => 'Opmerkingen veld is verplicht',
            ], 422);

        }

        $internalRemark = new OrderInternalRemark();
        $internalRemark->setUser($this->getUser());
        $internalRemark->setOrderOrder($order);
        $internalRemark->setRemark($remark);
        $this->entityManager->persist($internalRemark);

        $order->addInternalRemark($internalRemark);
        $this->entityManager->flush();

        $user = $internalRemark->getUser();

        $data = [
            'remark' => $internalRemark->getRemark(),
            'createdAt' => date('d-m-y H:i:s'),
            'user' => $user->getDisplayName(),
        ];

        return new JsonResponse($data);
    }

    /**
     * @Route("/leveranciers-opmerking/{order}", methods={"GET"})
     * @Template()
     *
     * @param Order $order
     *
     * @return array
     */
    public function supplierRemarkAction(Order $order): array
    {
        $order->setSupplierRemark('');
        $this->entityManager->flush();

        return [
            'order' => $order,
        ];
    }

    /**
     * @Route("/leveranciers-opmerking/{order}", methods={"POST"})
     *
     * @param Request $request
     * @param Order $order
     *
     * @return Response
     */
    public function supplierRemarkPersistAction(Request $request, Order $order): Response
    {
        $supplierRemark = trim($request->get('supplier-remark'));

        /**
         * Cleanup internal remark when nothing has been entered
         */
        $supplierRemark = trim(preg_replace('/[a-z]*\s\d{1,2}\s[a-z]*\s\d{4}\s\d{1,2}:\d{1,2}\s-\s.+(?=:):$/i',
            '', $supplierRemark));

        $order->setSupplierRemark($supplierRemark);
        $this->entityManager->flush();

        return new Response($supplierRemark);
    }

    /**
     * @Route("/{order}/beschikbare-statussen", methods={"GET"})
     * @param Order $order
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function availableStatusesAction(Order $order, Request $request): JsonResponse
    {
        $statuses = [];
        $availablePlaces = [
            'processed',
            'complete',
        ];

        if (!$request->isXmlHttpRequest() || !in_array($order->getStatus(), $availablePlaces, true)) {
            return new JsonResponse(false);
        }

        $transitions = $this->get(OrderManager::class)->getWorkflow($order)->getEnabledTransitions($order);

        foreach ($transitions as $transition) {
            try {
                $status = $transition->getTos()[0];

                if (empty($status)) {
                    continue;
                }

                $statuses[$transition->getName()] = OrderStatusHelper::getDescriptionByKey($status);
            } catch (OrderStatusNotFoundException $e) {
                continue;
            }
        }

        return $this->json($statuses);
    }

    /**
     * @Route("/{order}/status", methods={"POST"})
     * @param Order $order
     * @param Request $request
     *
     * @return Response
     * @throws OrderStatusNotFoundException
     * @throws Exception
     */
    public function updateStatusAction(Order $order, Request $request): Response
    {
        $transitionName = $request->get('status');

        $this->get(OrderManager::class)->getWorkflow($order)->apply($order, $transitionName);

        if ($order->getDeliveryStatus() && $order->getStatus() !== 'complete') {
            $order->setDeliveryStatus(null);
        }

        $this->get(OrderActivityService::class)->add('order_status_changed', $order, null, [
            'text' => 'Status gewijzigd naar: ' . OrderStatusHelper::getDescription($order) . ' - ' . $request->get('reason'),
        ]);

        $this->entityManager->flush();

        return $this->forward('AdminBundle:Sales\Order:order', [
            'orderCollection' => $order->getOrderCollection(),
        ]);
    }

    /**
     * @Route("/{orderLine}/download-ontwerp", requirements={
     *     "orderLine": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     * }, methods={"GET"})
     * @ParamConverter("orderLine", options={"mapping": { "orderLine" = "uuid"}})
     *
     * @param OrderLine $orderLine
     *
     * @return null|StreamedResponse
     * @throws Exception
     *
     */
    public function downloadPrintDesign(OrderLine $orderLine): ?StreamedResponse
    {
        $orderLineManager = $this->get(OrderLineManager::class);

        $response = $orderLineManager->downloadPrintDesign($orderLine);

        if (null === $response) {
            throw new NotFoundHttpException('Design not found');
        }

        return $response;
    }

    /**
     * @Route("/{orderLine}/download-brief", requirements={
     *     "orderLine": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     * }, methods={"GET"})
     * @ParamConverter("orderLine", options={"mapping": { "orderLine" = "uuid"}})
     *
     * @param OrderLine $orderLine
     *
     * @return StreamedResponse
     * @throws Exception
     *
     */
    public function downloadLetterAction(OrderLine $orderLine): StreamedResponse
    {
        return $orderLine->downloadLetter();
    }

    /**
     * @Route("/{orderCollection}/activiteit", methods={"GET"})
     * @Template("@Admin/Sales/Order/order_activity.html.twig")
     *
     * @param OrderCollection $orderCollection
     *
     * @return array
     */
    public function activityAction(OrderCollection $orderCollection): array
    {
        return [
            'activities' => $orderCollection->getActivities(),
        ];
    }

    /**
     * @return array
     */
    public function getPageviewSettings(): array
    {
        return [
            'route' => 'admin_sales_order_order',
            'entity' => OrderCollection::class,
            'param' => 'orderCollection',
        ];
    }

    /**
     * @Route("/{order}/analyseren", methods={"GET"})
     * @Template("@Admin/Sales/DryRun/index.html.twig")
     *
     * @param Order $order
     *
     * @return array
     */
    public function dryRunAction(Order $order): array
    {
        $analysis = $this->analyze($order);

        return [
            'order' => $order,
            'analysis' => $analysis,
        ];
    }

    /**
     * @Route("/{orderCollection}/extra-velden", methods={"GET", "POST"})
     * @Template("@Admin/Crud/form_xhr.html.twig")
     *
     * @param OrderCollection $orderCollection
     * @param Request $request
     *
     * @return array
     */
    public function extraFieldsAction(OrderCollection $orderCollection, Request $request): array
    {
        $formBuilder = $this->createFormBuilder();
        $company = $orderCollection->getCompany();

        if (null !== $company && !$company->getCustomFields()->isEmpty()) {
            $companyCustomFieldValueRepository = $this->getDoctrine()->getRepository(CompanyCustomOrderFieldValue::class);
            $extraFields = $companyCustomFieldValueRepository->findBy([
                'orderCollection' => $orderCollection,
            ]);

            $container = $formBuilder->create('container', ContainerType::class, []);

            /** @var CompanyCustomOrderFieldValue $extraField */
            foreach($extraFields as $extraField){
                $customField = $extraField->getCompanyCustomField();
                if($customField === null){
                    continue;
                }
                $options = [
                    'label' => $customField->translate()->getLabel(),
                    'mapped' => false,
                ];

                if (!$customField->getOptions()->isEmpty()) {
                    $choices = [];
                    /** @var CompanyCustomFieldOption $option */
                    foreach ($customField->getOptions() as $option) {
                        $choices[$option->getOptionKey()] = $option->getOptionValue();
                    }

                    $options['choices'] = $choices;
                    $options['multiple'] = $customField->getMultiple();
                    $options['required'] = $customField->getRequired();
                }

                $options['data'] = $extraField->getValue();

                $container->add($customField->getFieldKey(), $customField->getType(), $options);
            }

            $formBuilder->add($container);
        }

        $form = $formBuilder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $metadata = $orderCollection->getMetadata();

            $extraFields = [];
            $formContainer = $form->get('container');

            /** @var CompanyCustomField $customField */
            foreach ($company->getCustomFields() as $customField) {
                $key = $customField->getFieldKey();
                $extraFields[$key] = [
                    'label' => $customField->translate()->getLabel(),
                    'value' => $formContainer->get($key)->getData(),
                ];
            }

            $metadata['extra_fields'] = $extraFields;

            $orderCollection->setMetadata($metadata);
            $this->entityManager->flush();
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{orderCollection}/voucher-toepassen")
     * @param OrderCollection $orderCollection
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function applyVoucherCode(OrderCollection $orderCollection, Request $request): JsonResponse
    {
        $code = $request->get('code');

        try {
            $this->get(VoucherService::class)->applyVoucherForOrderCollection($code, $orderCollection);
            $this->get(OrderCollectionManager::class)->calculateTotals($orderCollection);
            $this->get('doctrine')->getManager()->flush();

            return $this->json([], 200);
        } catch (Exception $e) {
            return $this->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Executes the auto order command in debug and dryrun mode
     *
     * @param Order $order
     *
     * @return mixed
     */
    private function analyze(Order $order)
    {
        $process = new Process(sprintf('cd ./../bin && php console auto:process-order %d --dry-run --debug -v',
            $order->getId()));
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $analysis = [];

        foreach (explode(PHP_EOL, $process->getOutput()) as $item) {
            $analysis[] = str_replace(['[dry-run]', ' '], ['', '&nbsp;'], $item);
        }

        array_shift($analysis);

        return $analysis;
    }
}
