<?php

namespace AdminBundle\Controller\Sales;

use AdminBundle\Form\Sales\SupplierOrderType;
use AppBundle\Connector\AbstractConnector;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Finance\CommissionInvoiceSupplierOrder;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderDeliveryStatus;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Supplier\SupplierOrder;
use AppBundle\Exceptions\CardTextExceedsMaximumLengthException;
use AppBundle\Exceptions\OrderStatusNotFoundException;
use AppBundle\Helper\Order\OrderStatusHelper;
use AppBundle\Interfaces\ConnectorInterface;
use AppBundle\Manager\Order\OrderManager;
use AppBundle\Services\OrderManagerService;
use DateInterval;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Services\SupplierManagerService;
use AppBundle\Services\ConnectorHelper;

/**
 * @Route("/bestelling-order")
 */
class OrderOrderProcessController extends Controller
{
    /**
     * @var ConnectorInterface
     */
    private $connector;

    /**
     * @Route("/{order}/verwerken", methods={"GET"})
     * @Template()
     *
     * @param Order $order
     * @param null  $message
     *
     * @return array
     * @throws \Exception
     */
    public function indexAction(Order $order, $message = null)
    {
        if (!$this->container->get(OrderManager::class)->isProcessable($order)) {
            $message = sprintf('huidige order status: %s', OrderStatusHelper::getDescription($order));
        }

        $supplierData = $this->getSupplierData($order);

        return [
            'order' => $order,
            'supplierData' => $supplierData,
            'message' => lcfirst($message),
        ];
    }

    /**
     * @Route("/{order}/edit-process", methods={"GET"});
     * @Template("@Admin/Sales/OrderOrderProcess/edit.html.twig")
     * @param Order $order
     * @param null  $message
     *
     * @return array
     */
    public function editAction(Order $order, $message = null): array
    {
        $supplierOrder = $order->getSupplierOrder();

        if (null === $supplierOrder) {
            throw new \RuntimeException('SupplierOrder not found.');
        }

        $form = $this->getSupplierOrderForm($order, $supplierOrder, 'edit');

        $supplier = $supplierOrder->getSupplier();

        $isCommissioned = (bool)$this->getDoctrine()->getRepository(CommissionInvoiceSupplierOrder::class)->findOneBy(['supplierOrder' => $supplierOrder->getId()]);

        return [
            'message' => $message,
            'isCommissioned' => $isCommissioned,
            'order' => $order,
            'supplierOrder' => $supplierOrder,
            'supplier' => $supplier,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{order}/edit-process", methods={"POST"});
     * @Template("@Admin/Sales/OrderOrderProcess/edit.html.twig")
     * @param Request $request
     * @param Order   $order
     * @param null    $message
     *
     * @return array|Response
     */
    public function editPersistAction(Request $request, Order $order, $message = null)
    {
        $supplierOrder = $order->getSupplierOrder();

        if (null === $supplierOrder) {
            throw new \RuntimeException('SupplierOrder not found.');
        }

        $form = $this->getSupplierOrderForm($order, $supplierOrder, 'edit');
        $form->handleRequest($request);

        $reason = $form->get('reason')->getData();

        if (empty($reason)) {
            $form->addError(new FormError('Geen reden opgegeven voor de prijs aanpassing.'));
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $orderActivityService = $this->get('order.activity');
            $orderActivityService->add('edit_order_forwarding', $order, null, [
                'text' => $reason,
            ]);

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            // Success empty response.
            return new Response('');
        }

        return $this->forward('AdminBundle:Sales/OrderOrderProcess:edit', [
            'order' => $order,
            'message' => $message,
        ]);
    }

    /**
     * @Route("/{order}/handmatig-verwerken", methods={"GET", "POST"})
     * @Template()
     *
     * @param Request $request
     * @param Order   $order
     *
     * @return mixed
     * @throws OrderStatusNotFoundException
     */
    public function manuallyProcessAction(Request $request, Order $order)
    {
        if ($request->isMethod('POST')) {
            $processed = (bool)$request->get('processed');
            $delivered = (bool)$request->get('delivered');

            if ($processed) {
                $order->setStatus('processed'); // @todo: workflow refactor
            }

            if ($delivered) {
                $order->setStatus('complete'); // @todo: workflow refactor
                $order->setDeliveryStatus($this->container->get('doctrine')->getRepository(OrderDeliveryStatus::class)->find('delivered'));
            }

            //log activity for order
            if ($delivered || $processed) {
                // TODO SF4
                $this->get('order.activity')->add('order_status_changed', $order, null, [
                    'text' => 'Status gewijzigd naar: ' . OrderStatusHelper::getDescription($order) . ' - ' . $request->get('internal_remark'),
                ]);
            }

            $order->setInternalRemark($order->getInternalRemark() . PHP_EOL . $request->get('internal_remark'));

            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(['message' => 'Order verwerkt']);
        }

        return [
            'order' => $order,
        ];
    }

    /**
     * @param Company   $supplier
     * @param OrderLine $orderLine
     *
     * @return float|null
     * @throws \Exception
     */
    private function getOrderLineDefaultForwardPrice(Company $supplier, OrderLine $orderLine)
    {
        $product = $orderLine->getProduct();

        if ($product->isCombination()) {
            //TODO: check supplier manager for combination products
            $forwardPrice = 0;

            foreach ($product->getCombinations() as $combination) {
                // TODO SF4
                $supplierProduct = $this->get(SupplierManagerService::class)->getSupplierProduct($combination->getProduct(),
                    $supplier);

                if ($supplierProduct) {
                    $forwardPrice += $supplierProduct->getForwardPrice();
                }
            }

            return $forwardPrice;
        }

        if ($product instanceof TransportType) {
            // TODO SF4
            $deliveryArea = $this->get(SupplierManagerService::class)->getSupplierDeliveryArea($supplier,
                $orderLine->getOrder());

            if ($deliveryArea) {
                return $deliveryArea->getDeliveryPrice();
            }

            // TODO Indien geen deliveryArea, dan standaard bezorgkosten overnemen
        }

        // TODO SF4
        $supplierProduct = $this->get(SupplierManagerService::class)->getSupplierProduct($product, $supplier);

        if ($supplierProduct && $supplierProduct->getForwardPrice() >= 0.00) {
            return $supplierProduct->getForwardPrice();
        }

        if ($this->getConnector($supplier)->requireForwardingPrices()) {
            throw new \RuntimeException(sprintf("No forwarding price configured for product '%s'",
                $orderLine->getProduct()->translate()->getTitle()));
        }

        return null;
    }

    /**
     * @Route("/{order}/verwerken", methods={"POST"})
     * @Template()
     *
     * @param Request $request
     * @param Order   $order
     *
     * @return array|Response
     * @throws \RuntimeException
     */
    public function indexPersistAction(Request $request, Order $order)
    {
        $supplier = $this->getDoctrine()->getManager()->find(Company::class,
            $request->request->get('supplier_order')['supplier']);

        // TODO SF4
        $supplierOrder = $this->get(OrderManagerService::class)->createSupplierOrder($order, $supplier);

        $supplierOrderForm = $this->getSupplierOrderForm($order, $supplierOrder);

        $supplierOrderForm->handleRequest($request);

        $connector = $this->get(ConnectorHelper::class)->getConnector($supplierOrder);

        if (null === $order->getOrderCollection()->getFraudScore() && $order->getOrderCollection()->isFraudCheckable()) {
            throw new \RuntimeException('Fraude check niet uitgevoerd');
        }

        if ($connector === null) {
            throw new \RuntimeException(sprintf("No connector configured for '%s'", $supplier->getName()));
        }

        if ($connector->requireForwardingPrices()) {
            foreach ($supplierOrderForm->get('lines') as $key => $line) {
                if (null === $line->get('price')->getData()) {
                    $line->addError(new FormError('Doorstuurprijs is een verplicht veld'));
                }
            }
        }

        if ($supplierOrderForm->isSubmitted()) {
            if (!$supplierOrderForm->isValid()) {
                return $this->forward('AdminBundle:Sales/OrderOrderProcess:index', [
                    'order' => $order,
                    'supplier' => $supplier,
                    'message' => $supplierOrderForm->getErrors(true, true)[0]->getMessage(),
                ]);
            }

            try {
                foreach ($supplierOrderForm->get('lines') as $key => $line) {
                    $supplierOrder->getLines()[$key]->setPrice($line->get('price')->getData());
                }
                $this->getDoctrine()->getManager()->flush();

                $this->get(OrderManagerService::class)->forwardToSupplier($supplierOrder);

                $this->getDoctrine()->getManager()->flush();
            } catch (\Exception $e) {
                if ($e instanceof CardTextExceedsMaximumLengthException) {
                    $message = vsprintf('De kaarttekst is te lang (%d tekens), maximaal %d tekens toegestaan.', [
                        $e->getLength(),
                        $e->getMaximumLength(),
                    ]);
                } else {
                    $message = $e->getMessage();
                }

                return $this->forward('AdminBundle:Sales/OrderOrderProcess:index', [
                    'order' => $order,
                    'supplier' => $supplier,
                    'message' => $message,
                ]);
            }
        }

        return [
            'order' => $order,
            'supplier' => $supplier,
        ];
    }

    /**
     * @Route("/{order}/{company}/ophalen-alternatieve-leverancier", methods={"GET"}, requirements={
     *     "uuid": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     * })
     * @ParamConverter("company", options={"mapping": { "company" = "uuid"}})
     *
     * @param Request $request
     * @param Order   $order
     * @param Company $company
     * @return Response
     */
    public function getAlternativeSupplier(Request $request, Order $order, Company $company)
    {
        return $this->render('AdminBundle:Sales/OrderOrderProcess/include:row-supplier.html.twig', [
            'order' => $order,
            'supplier' => $company
        ]);
    }

    /**
     * @param Order $order
     *
     * @return ArrayCollection
     * @throws \Exception
     */
    private function getSupplierData(Order $order): ArrayCollection
    {
        // TODO SF4
        $supplierManager = $this->get(SupplierManagerService::class);
        $availableSuppliers = $supplierManager->getAvailableSuppliersForDelivery($order);
        $supplierData = new ArrayCollection();

        if ($availableSuppliers->isEmpty()) {
            return $supplierData;
        }

        foreach ($availableSuppliers as $availableSupplier) {
            $deliveryArea = $supplierManager->getSupplierDeliveryArea($availableSupplier, $order);
            $deliveryPrice = null;

            //calculate price based on supplier product
            $deliveryLine = null;

            /**
             * @var OrderLine $line
             */
            foreach ($order->getLines() as $line) {
                try {
                    if (null !== $line->getProduct()->getProductgroup() && $line->getProduct()->getProductgroup()->getSkuPrefix() === 'DLV') {
                        $deliveryLine = $line;
                        break;
                    }
                } catch (\Exception $e) {
                    continue;
                }
            }

            if ($deliveryArea) {
                $dateInterval = new DateInterval($deliveryArea->getDeliveryInterval());
                $deliveryArea->dateInterval = $dateInterval->format('%H:%I');

                if ($deliveryLine) {
                    $factor = $deliveryLine->getVatLines()->current()->getVatRate()->getFactor();
                    $deliveryPrice = $deliveryArea->getDeliveryPrice() * (1 + $factor);
                }
            } elseif ($deliveryLine) {
                $factor = $deliveryLine->getVatLines()->current()->getVatRate()->getFactor();
                $deliveryPrice = $this->getOrderLineDefaultForwardPrice($availableSupplier, $line) * (1 + $factor);
            }

            $supplierData->add([
                'supplier' => $availableSupplier,
                'deliveryArea' => $deliveryArea,
                'deliveryCosts' => $deliveryPrice,
            ]);
        }

        return $supplierData;
    }

    /**
     * @param Company|Order $connector
     *
     * @return ConnectorInterface|AbstractConnector
     */
    private function getConnector($connector)
    {
        // TODO SF4
        $this->connector = $this->get(ConnectorHelper::class)->getConnector($connector);

        return $this->connector;
    }

    /**
     * @Route("/{order}/leveranciers-order/{supplier}")
     * @Template()
     *
     * @param Order   $order
     * @param Company $supplier
     * @return Response
     */
    public function supplierOrder(Order $order, Company $supplier)
    {
        $supplierOrder = $this->get(OrderManagerService::class)->createSupplierOrder($order, $supplier);

        return $this->render('@Admin/Crud/form_xhr.html.twig', [
            'form' => $this->getSupplierOrderForm($order, $supplierOrder)->createView(),
        ]);
    }

    /**
     * @param Order         $order
     * @param SupplierOrder $supplierOrder
     * @param string        $action
     * @return FormInterface
     */
    private function getSupplierOrderForm(Order $order, SupplierOrder $supplierOrder, $action = 'add')
    {
        return $this->createForm(SupplierOrderType::class, $supplierOrder, [
            'action' => $this->getFormActionUrl($action, $order),
            'method' => 'POST',
        ]);
    }

    /**
     * @param       $action
     * @param Order $order
     * @return string
     */
    private function getFormActionUrl($action, Order $order): string
    {
        $route = 'admin_sales_orderorderprocess_indexpersist';

        if ($action === 'edit') {
            $route = 'admin_sales_orderorderprocess_editpersist';
        }

        return $this->generateUrl($route, [
            'order' => $order->getId(),
        ]);
    }
}
