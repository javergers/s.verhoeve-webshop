<?php

namespace AdminBundle\Controller\Sales;

use AdminBundle\Form\Sales\CompositionOrderType;
use AdminBundle\Form\Sales\OrderInputFieldFormType;
use AdminBundle\Form\Sales\OrderLineCardType;
use AdminBundle\Form\Sales\OrderType;
use AppBundle\Connector\AbstractConnector;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Assortment\AssortmentProduct;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductCard;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Site\Site;
use AppBundle\Manager\Catalog\Product\ProductPriceManager;
use AppBundle\Manager\Order\OrderCollectionManager;
use AppBundle\Manager\Order\OrderLineManager;
use AppBundle\Manager\Order\OrderManager;
use AppBundle\Services\ConnectorHelper;
use AppBundle\Services\SupplierManagerService;
use AppBundle\Utils\Select2ResultUtil;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\UnitOfWork;
use Exception;
use Generator;
use RuntimeException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/bestelling-order")
 */
class OrderOrderController extends Controller
{
    private const TRANSLATIONS = [
        'error.order.invalid_suppliers' => 'De producten hebben geen overeenkomende leveranciers.',
        'error.orderline.invalidPersonalization' => 'Gekozen personalisatie hoort niet bij het product',
        'error.orderline.invalidCombination' => 'Combinatie van producten is niet correct.',
    ];

    /**
     * @Route("/{order}", requirements={"order" : "\d+"}, methods={"GET"})
     * @Template("@Admin/Sales/OrderOrder/edit.html.twig")
     *
     * @param Order $order
     *
     * @param Request $request
     *
     * @return array|JsonResponse
     */
    public function editAction(Order $order, Request $request)
    {
        if ($request->query->has('get-next-available-number')) {
            $number = $order->getOrderCollection()->getOrders()->last()->getNumber();
            $number = $order->getOrderCollection()->getNumber() . '-' . str_pad((int)++$number, 4, '0', STR_PAD_LEFT);

            return new JsonResponse([
                'number' => $number,
            ]);
        }

        $systemParameters = $this->container->get('app.parameter')->setEntity();

        if (!$order->isCanceled() || !$order->isProcessed()) {
            $form = $this->getForm($order);

            return [
                'order' => $order,
                'form' => $form->createView(),
                'fraudWarningThreshold' => $systemParameters->getValue('fraud_warning_score'),
                'fraudBlockTreshold' => $systemParameters->getValue('fraud_blocked_score'),
                'isEditable' => $this->container->get(OrderManager::class)->isEditable($order),
                'canChangeToDraft' => $this->container->get(OrderManager::class)->canChangeToDraft($order),
            ];
        }

        throw new RuntimeException('Order was either canceled or processed');
    }

    /**
     * @Route("/{order}/nieuwe-samenstelling", methods={"GET"})
     * @Template("@Admin/Sales/OrderOrder/edit_composition.html.twig")
     * @param Order $order
     * @param Request $request
     *
     * @return array
     */
    public function editComposition(Order $order, Request $request)
    {
        $form = $this->getCompostionForm($order);

        $systemParameters = $this->container->get('app.parameter')->setEntity();

        return [
            'form' => $form->createView(),
            'order' => $order,
            'fraudWarningThreshold' => $systemParameters->getValue('fraud_warning_score'),
            'fraudBlockTreshold' => $systemParameters->getValue('fraud_blocked_score'),
            'isEditable' => $this->container->get(OrderManager::class)->isEditable($order),
        ];
    }

    /**
     * @Route("/{currentOrder}/concept-opslaan")
     *
     * @param Order $currentOrder
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function saveDraft(Order $currentOrder, Request $request): JsonResponse
    {
        if (!$currentOrder->getOrderCollection()->isEditable()) {
            throw new RuntimeException('Wijzigen van deze order is op dit moment niet mogelijk omdat de fraudecontrole nog niet is afgerond.');
        }

        $em = $this->getDoctrine()->getManager();

        $orderManager = $this->get(OrderManager::class);

        $form = $this->getCompostionForm($currentOrder);

        $form->handleRequest($request);

        $lines = $currentOrder->getLines();

        $currentOrder->setLines(new ArrayCollection());

        foreach ($lines as $line) {
            if ($line->getParent() !== null) {
                continue;
            }

            $newLine = $orderManager->cloneLine($line, $currentOrder);

            if (false === $line->getChildren()->isEmpty()) {
                foreach ($line->getChildren() as $child) {
                    $newChild = $orderManager->cloneLine($child, $currentOrder);
                    $newChild->setParent($newLine);

                    $newLine->addChild($newChild);
                }
            }

            $currentOrder->addLine($newLine);

            if (false === $newLine->getChildren()->isEmpty()) {
                foreach ($newLine->getChildren() as $child) {
                    $currentOrder->addLine($child);
                }
            }
        }

        $errors = $this->get('validator')->validate($currentOrder);

        $currentOrder->setLines($lines);

        foreach ($currentOrder->getLines() as $orderLine) {
            $this->container->get(OrderLineManager::class)->createVatLines($orderLine);
        }

        if ($errors->count() === 0) {
            $orderManager->calculateTotals($currentOrder);

            $em->persist($currentOrder);

            $em->flush();

        } else {

            return $this->json(['errors' => $this->translateErrorMessages($errors)], 422);
        }

        return $this->json([]);
    }

    /**
     * @Route("/{currentOrder}/samenstelling-opslaan", methods={"POST"})
     *
     * @param Order $currentOrder
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function saveComposition(Order $currentOrder, Request $request): JsonResponse
    {
        if (!$currentOrder->getOrderCollection()->isEditable()) {
            throw new RuntimeException('Wijzigen van deze order is op dit moment niet mogelijk omdat de fraudecontrole nog niet is afgerond.');
        }

        if ($currentOrder->getStatus() === 'draft') {
            return $this->saveDraft($currentOrder, $request);
        }

        /** @var EntityManagerInterface $em */
        $em = $this->getDoctrine()->getManager();

        /** @var UnitOfWork $uow */
        $uow = $em->getUnitOfWork();

        $orderManager = $this->get(OrderManager::class);

        $originalOrder = $currentOrder;
        $originalLines = $originalOrder->getLines();
        foreach ($originalOrder->getLines() as $originalLine) {
            $uow->markReadOnly($originalLine);
        }

        $form = $this->getCompostionForm($currentOrder);

        $currentOrder->setLines($originalLines);
        $form->handleRequest($request);

        $order = $orderManager->clone($form->getData());
        $validate = $this->get('validator')->validate($order);

        $currentOrder = $originalOrder;

        if ($validate->count() === 0) {
            $orderManager->calculateTotals($order);

            $orderMetadata = $currentOrder->getMetadata();
            $orderMetadata['canceled_order_id'] = $currentOrder->getId();

            $order->setMetadata($orderMetadata);

            $em->persist($order);
            $em->flush();

            /** @var Order $orderToCancel */
            $orderToCancel = $em->getRepository(Order::class)->find($currentOrder->getId());

            $currentOrderMetadata = $orderToCancel->getMetadata();
            $currentOrderMetadata['replacement_order_id'] = $order->getId();

            $orderToCancel->setMetadata($currentOrderMetadata);

            /**
             * @var OrderLine $originalLine
             */
            foreach ($originalLines as $originalLine) {
                $originalLine->setDeletedAt(null);
            }

            /** @var OrderLine $formLine */
            foreach ($form->getData()->getLines() as $formLine) {
                if (!$originalLines->contains($formLine)) {
                    $form->getData()->removeLine($formLine);
                }
            }

            $orderToCancel->setLines($originalLines); // Reset lines due to possible removal
            $orderManager->cancel($orderToCancel, true, 'consumer');

            $em->flush();

        } else {
            $errors = $form->getErrors(true);

            return $this->json(['errors' => $this->translateErrorMessages($errors)], 422);
        }

        return $this->json([]);
    }

    /**
     * @Route("/{order}/activiteit", methods={"GET"})
     * @Template("@Admin/Sales/Order/order_activity.html.twig")
     *
     * @param Order $order
     *
     * @return array
     */
    public function activityAction(Order $order)
    {
        return [
            'activities' => $order->getActivities(),
        ];
    }

    /**
     * @Route("/{order}/kaartje/{orderLine}/bewerken", requirements={"order" : "\d+", "orderLine" : "\d+"},
     *     methods={"GET", "POST"})
     * @Template()
     * @param Request $request
     * @param Order $order
     * @param OrderLine $orderLine
     *
     * @return array|JsonResponse
     */
    public function editCardAction(Request $request, Order $order, OrderLine $orderLine)
    {
        $form = $this->getCardForm($order, $orderLine);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            return new JsonResponse('');
        }

        return [
            'order' => $order,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{order}/tekstvelden/{orderLine}/bewerken", requirements={"order" : "\d+", "orderLine" : "\d+"},
     *     methods={"GET", "POST"})
     * @Template()
     * @param Request $request
     * @param Order $order
     * @param OrderLine $orderLine
     *
     * @return array|JsonResponse
     */
    public function editInputFieldsAction(Request $request, Order $order, OrderLine $orderLine)
    {
        $form = $this->createForm(OrderInputFieldFormType::class, $orderLine, [
            'action' => $this->generateUrl('admin_sales_orderorder_editinputfields', [
                'order' => $order->getId(),
                'orderLine' => $orderLine->getId(),
            ]),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $this->getDoctrine()->getManager()->flush();
            return new JsonResponse('');
        }

        return [
            'order' => $order,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/bewerken/{product}/{type}/{order}/beschikbare-personalisaties")
     *
     * @param Product $product
     * @param string $type
     *
     * @param Order $order
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function getAvailablePersonalizations(Product $product, string $type, Order $order): JsonResponse
    {
        // @todo: SF4
        $priceService = $this->get(ProductPriceManager::class);

        $result = [];

        if ($type === 'cards') {
            $result = iterator_to_array($this->formatCards($product->getCards(), $order));
        } else {
            if ($type === 'personalizations' && $personalization = $product->getFirstPersonalization()) {
                $vatGroup = $personalization->getVatGroupForCountry($order->getDeliveryAddressCountry());

                $result = [
                    'id' => $personalization->getId(),
                    'name' => $personalization->getName(),
                    'price' => $personalization->getPrice(),
                    'priceIncl' => $priceService->getPrice($personalization),
                    'vat' => $vatGroup->getRateByDate($order->getDeliveryDate())->getFactor() * 100,
                    'vatGroupId' => $vatGroup->getId(),
                ];
            } else {
                if ($type === 'letters' && $letter = $product->getProductLetter()) {
                    $vatGroup = $letter->getVatGroupForCountry($order->getDeliveryAddressCountry());

                    $result = [
                        'id' => $letter->getId(),
                        'name' => $letter->getName(),
                        'price' => $letter->getPrice(),
                        'priceIncl' => $priceService->getPrice($letter),
                        'vat' => $vatGroup->getRateByDate($order->getDeliveryDate())->getFactor() * 100,
                        'vatGroupId' => $vatGroup->getId(),
                    ];
                }
            }
        }

        return new JsonResponse($result);
    }

    /**
     * @param Order $order
     *
     * @param bool $editable
     *
     * @return FormInterface
     */
    private function getForm(Order $order)
    {
        $form = $this->createForm(OrderType::class, $order, [
            'action' => $this->generateUrl('admin_sales_orderorder_editpersist', [
                'order' => $order->getId(),
            ]),
        ]);

        return $form;
    }

    /**
     * @param Order $order
     *
     * @param bool $editable
     *
     * @return FormInterface
     */
    private function getCompostionForm(Order $order)
    {
        $form = $this->createForm(CompositionOrderType::class, $order, [
            'action' => $this->generateUrl('admin_sales_orderorder_savecomposition', [
                'currentOrder' => $order->getId(),
            ]),
        ]);

        return $form;
    }

    /**
     * @param Order $order
     * @param OrderLine $orderLine
     *
     * @return FormInterface
     */
    private function getCardForm(Order $order, OrderLine $orderLine)
    {
        $form = $this->createForm(OrderLineCardType::class, $orderLine, [
            'action' => $this->generateUrl('admin_sales_orderorder_editcard', [
                'order' => $order->getId(),
                'orderLine' => $orderLine->getId(),
            ]),
        ]);

        return $form;
    }

    /**
     * @Route("/{order}", requirements={"order" : "\d+"}, methods={"POST"})
     *
     * @param Request $request
     * @param Order $order
     *
     * @return JsonResponse
     * @throws RuntimeException
     * @throws Exception
     */
    public function editPersistAction(Request $request, Order $order): JsonResponse
    {
        if (!$order->getOrderCollection()->isEditable()) {
            throw new RuntimeException('Wijzigen van deze order is op dit moment niet mogelijk omdat de fraudecontrole nog niet is afgerond.');
        }

        $form = $this->getForm($order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($order->getLines() as $line) {
                $this->container->get(OrderLineManager::class)->createVatLines($line);
            }

            $this->container->get(OrderCollectionManager::class)->calculateTotals($order->getOrderCollection());

            $em = $this->getDoctrine()->getManager();

            $em->persist($order);
            $em->flush();
        } else {
            $errors = $form->getErrors(true);

            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            return $this->json(['errors' => $this->translateErrorMessages($errors)], 422);
        }

        return $this->json([]);
    }

    /**
     * @Route("/pakbon/{order}/{filename}/{action}", requirements={"action" = "view|download"}, defaults={"action" =
     *                                               "view"})
     *
     * @param Request $request
     * @param Order $order
     * @param string $filename
     * @param string $action
     *
     *
     * @return Response
     */
    public function packingSlipAction(Request $request, Order $order, $filename, $action)
    {
        void($filename);

        $parameters = [];

        if (null !== ($line = $request->get('line'))) {
            $parameters['line'] = (int)$line;
        }

        if (null === $order->getSupplierOrder()) {
            throw new RuntimeException('Invalid order. Order has no SupplierOrder');
        }

        /** @var AbstractConnector $connector */
        $connector = $this->get(ConnectorHelper::class)->getConnector($order->getSupplierOrder());

        $paperSize = $connector->getPackingSlipPaperSize();

        return $this->get('order.pdf.packing_slip')
            ->setPaperSize($paperSize)
            ->setParameters($parameters)
            ->setItem($order)
            ->setAction($action)
            ->getResponse();
    }

    /**
     * @Route("/most-recent", methods={"POST"})
     * @param Request $request
     *
     * @return Response
     */
    public function getRecentOrders(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Order::class);

        $site = $this->getDoctrine()->getRepository(Site::class)->find($request->get('siteId'));
        $recentOrders = $repository->findRecentOrders($site);

        return $this->render('@Admin/Dashboard/blocks/recentOrders.html.twig', [
            'ordersToday' => $recentOrders,
        ]);
    }

    /**
     * @Route("/revenue-today", methods={"POST"})
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getRevenueToday(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Order::class);

        $revenueByCategory = $repository->getRevenueToday($request->get('siteId'), 'site');

        return new JsonResponse($revenueByCategory);
    }

    /**
     * @Route("/revenue-category", methods={"POST"})
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getRevenueByCategory(Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Order::class);

        $revenueByCategory = $repository->getRevenueToday($request->get('siteId'), 'productgroup');

        return new JsonResponse($revenueByCategory);
    }

    /**
     * @param Assortment $cards
     * @param Order $order
     *
     * @return Generator
     * @throws Exception
     */
    private function formatCards(Assortment $cards, Order $order)
    {
        /** @todo: SF4 */
        $imageCacheManager = $this->get('liip_imagine.cache.manager');

        /** @var AssortmentProduct $card */
        foreach ($cards->getAssortmentProducts() as $card) {
            if (null === $card->getProduct()) {
                continue;
            }

            /** @var ProductCard $product */
            $product = $card->getProduct();

            $vatGroup = $product->getVatGroupForCountry($order->getDeliveryAddressCountry());

            yield $product->getId() => [
                'id' => $product->getId(),
                'name' => $product->getDisplayName(),
                'price' => $product->getPrice(),
                'priceIncl' => $this->container->get('app.product_price_manager')->getPrice($product),
                'vat' => $vatGroup->getRateByDate($order->getDeliveryDate())->getFactor() * 100,
                'vatGroupId' => $vatGroup->getId(),
                'image' => $product->getImagePath() ? $imageCacheManager->getBrowserPath($product->getImagePath(),
                    'product_card') : null,
            ];
        }
    }

    /**
     * @Route("/{order}/product/zoeken")
     * @param Order $order
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function searchProduct(Order $order, Request $request)
    {
        $queryTerm = $request->get('query_term');
        $supplierIds = null;

        if (strlen($queryTerm) < 3) {
            return $this->json(["items" => []]);
        }

        if ($request->get('edit_mode') !== "1") {
            $supplierIds = $this->get(SupplierManagerService::class)->getAvailableSuppliers($order)->map(function (
                Company $company
            ) {
                return $company->getId();
            })->toArray();
        }

        $query = $this->getDoctrine()->getRepository(Product::class)->queryVariationsBySiteAndSuppliers(
            $order->getOrderCollection()->getSite()->getId(),
            $supplierIds,
            $queryTerm
        )->getQuery();

        /** @var Product[] $products */
        $products = $query->getResult();

        return $this->json(Select2ResultUtil::generateList($products, [
            'id' => 'id',
            'text' => 'name',
            'parent_id' => 'parent_id',
            'parent_text' => 'parent_name',
        ], true));
    }

    /**
     * @Route("/{order}/product/{product}/informatie")
     * @param Order $order
     * @param Product $product
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function productInformation(Order $order, Product $product, Request $request)
    {
        $vatGroup = $product->getVatGroupForCountry($order->getDeliveryAddressCountry());

        $personalization = $product->getFirstPersonalization();

        $returnData = [
            'id' => $product->getId(),
            'name' => $product->getDisplayName(),
            'price' => $product->getPrice(),
            'priceIncl' => $this->container->get('app.product_price_manager')->getPrice($product),
            'vat' => $vatGroup->getRateByDate($order->getDeliveryDate())->getFactor() * 100,
            'vatGroupId' => $vatGroup->getId(),
        ];

        if ($product instanceof GenericProduct) {
            $returnData['isCombination'] = $product->isCombination();
            $returnData['isCardAvailable'] = $product->isProductCardAvailable();
            $returnData['isLetterAvailable'] = $product->isProductLetterAvailable();
            $returnData['isPersonalizationAvailable'] = $product->isProductPersonalizationAvailable();
            $returnData['isPersonalizationRequired'] = $product->isRequiredPersonalization() || $product->hasRequiredPersonalization();
        }

        if ($personalization) {
            $returnData['personalization'] = [
                'id' => $personalization->getId(),
                'name' => $personalization->getName(),
            ];
        }

        return $this->json($returnData);
    }

    /**
     * @param $errors
     *
     * @return array
     */
    private function translateErrorMessages($errors): array
    {
        $errorMessages = [];
        foreach ($errors as $error) {
            if (!method_exists($error, 'getMessage')) {
                continue;
            }

            $errorMessage = $error->getMessage();
            if (isset(self::TRANSLATIONS[$error->getMessage()])) {
                $errorMessage = self::TRANSLATIONS[$error->getMessage()];
            }

            $errorMessages[] = $errorMessage;
        }

        return $errorMessages;
    }
}
