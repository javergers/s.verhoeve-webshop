<?php

namespace AdminBundle\Controller\Designer;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Interfaces\DesignerControllerInterface;
use AppBundle\Services\Designer;
use AppBundle\Traits\DesignerTrait;
use AppBundle\Utils\Autowire\Profiler;
use Exception;
use RuntimeException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Exception\InvalidParameterException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;


/**
 * @Route("/designer")
 */
class DesignerController extends AbstractController implements DesignerControllerInterface
{
    use DesignerTrait;

    /**
     * @var Designer
     */
    private $designer;

    /**
     * @var Router
     */
    private $router;

    /**
     * DesignerController constructor.
     * @param Designer $designer
     * @param RouterInterface $router
     */
    public function __construct(Designer $designer, RouterInterface $router)
    {
        $this->designer = $designer;
        $this->router = $router;
    }

    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->redirectToRoute('admin_dashboard_index');
    }

    /**
     * @Route("/preview/{uuid}", requirements={
     *     "uuid": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|__UUID__",
     * }, methods={"GET"})
     * @param string $uuid
     *
     * @param Request $request
     *
     * @return string|null
     */
    public function getPreviewUrl(string $uuid, Request $request) {
        $this->designer->setUuid($uuid);
        $this->designer->setIsPreDesign($request->get('predesign', false));

        return new JsonResponse($this->designer->getPreviewUrl(60, Designer::SOURCE_LOCAL));
    }

    /**
     * @Route("/screenshot/{uuid}/{product}", requirements={
     *     "uuid": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *     "product":"\d+"
     * }, defaults={"product" = null}, methods={"GET", "POST"})
     * @Template("@Admin/designer/designer/edit.html.twig")
     *
     * @param Request $request
     * @param KernelInterface $kernel
     * @param Profiler $profiler
     * @param string $uuid
     * @param Product|null $product
     * @return array
     * @throws Exception
     */
    public function screenshot(Request $request, KernelInterface $kernel, Profiler $profiler, $uuid, Product $product = null): array
    {
        $profiler->disable();

        $screenshotModus = true;

        $params = $this->edit($request, $kernel, $uuid, $product, $screenshotModus);

        /** @var Designer $designer */
        $designer = $params['designer'];

        $config = $designer->getConfig();
        $params['screenshotModus'] = $screenshotModus;
        $params['width'] = $designer->convertCmToPx($config->dimensions->width);
        $params['height'] = $designer->convertCmToPx($config->dimensions->height);

        return $params;
    }

    /**
     * @Route("/ontwerpen/{product}/toevoegen", requirements={
     *     "product":"\d+|__PRODUCT__"
     * }, defaults={"product" = null}, methods={"GET", "POST"})
     * @Template("@Admin/designer/designer/edit.html.twig")
     *
     * @param Request $request
     * @param Designer $designer
     * @param KernelInterface $kernel
     * @param Product|null $product
     * @return array|JsonResponse
     * @throws Exception
     */
    public function addAction(Request $request, Designer $designer, KernelInterface $kernel, Product $product = null)
    {
        $this->designer->setIsPreDesign((bool)$request->get('is-pre-design'));
        $this->designer->setCallback($request->get('callback'));

        $uuid = $this->designer->getUuid();

        $params = [
            'uuid' => $uuid,
            'is-pre-design' => $this->designer->isPreDesign(),
        ];

        if (null !== $product) {
            $params['product'] = $product->getId();
            $this->designer->setProduct($product);

            $personalization = $product->getFirstPersonalization();

            if (null !== $personalization) {
                $this->designer->setPersonalization($personalization);
            } else {
                throw new RuntimeException('Product personalization not found.');
            }
        }

        // Set designer in debug modus for environments dev / test.
        if (in_array($kernel->getEnvironment(), ['dev', 'test'])) {
            $this->designer->setDebugMode(true);
        }

        $this->designer->setApiUrl($this->buildRouteUrl('admin_designer_designer_index', $params));

        $processUrl = $this->buildRouteUrl('admin_designer_designer_edit', $params);

        // Todo: Split routes
        $this->designer->setProcessUrl($processUrl);
        $this->designer->setUploadUrl($processUrl);

        if ($request->getMethod() === Request::METHOD_POST) {
            $uploadedFileUrl = $this->designer->processFileUpload();

            // Return uploaded image url directly
            if (null !== $uploadedFileUrl) {
                return new JsonResponse($uploadedFileUrl);
            }

            return new JsonResponse($this->designer->process());
        }

        return [
            'designer' => $this->designer,
        ];
    }

    /**
     * @Route("/ontwerpen/{uuid}/{product}", requirements={
     *     "uuid": "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}|__UUID__",
     *     "product":"\d+|__PRODUCT__"
     * }, methods={"GET", "POST"}, defaults={"product" = null})
     * @Template("@Admin/designer/designer/edit.html.twig")
     *
     * @param Request $request
     * @param KernelInterface $kernel
     * @param string $uuid
     * @param Product|null $product
     * @param bool $screenshotModus
     * @return array|JsonResponse
     * @throws Exception
     */
    public function edit(Request $request, KernelInterface $kernel, $uuid, Product $product = null, bool $screenshotModus = false)
    {
        if ($product === '__PRODUCT__' || $uuid === '__UUID__') {
            throw new InvalidParameterException('PRODUCT and UUID parameters are for templating only');
        }

        $this->designer->setUuid($uuid);

        if($request->get('clone') === '1'
            && $request->getMethod() === Request::METHOD_GET) {
            $uuid = $this->designer->cloneDesign();
            $this->designer->setUuid($uuid);
        }

        $this->designer->setIsPreDesign((bool)$request->get('is-pre-design'));
        $this->designer->setCallback($request->get('callback'));
        $this->designer->setScreenshotModus($screenshotModus);
        $this->designer->setConvertOnProcess(true);

        if ($this->designer->isPreDesign()) {
            $this->designer->setTemplateUuid($uuid);
        }

        $params = [
            'uuid' => $uuid,
            'is-pre-design' => $this->designer->isPreDesign(),
            'clone' => $request->get('clone')
        ];

        if (null !== $product) {
            $params['product'] = $product->getId();
            $this->designer->setProduct($product);

            try {
                $productProperty = $product->findProductProperty('personalization');
            } catch (Exception $e) {
                $productProperty = null;
            }

            if ($product->isCombination() && $product->hasPersonalization()) {
                $this->designer->setPersonalization($product->getPersonalizations()->first());
            } else if (null !== $productProperty && $productProperty->getProductVariation()) {
                $this->designer->setPersonalization($productProperty->getProductVariation());
            } else {
                throw new RuntimeException('Product personalization not found.');
            }
        }

        // Set designer in debug modus for environments dev / test.
        if (in_array($kernel->getEnvironment(), ['dev', 'test'])) {
            $this->designer->setDebugMode(true);
        }

        $this->designer->setApiUrl($this->buildRouteUrl('admin_designer_designer_index', $params));

        $processUrl = $this->buildRouteUrl('admin_designer_designer_edit', $params);

        // Todo: Split routes
        $this->designer->setProcessUrl($processUrl);
        $this->designer->setUploadUrl($processUrl);

        if ($request->getMethod() === Request::METHOD_POST) {
            $convertUrl = $this->router->generate('admin_designer_designer_screenshot', $params, UrlGeneratorInterface::ABSOLUTE_URL);

            $this->designer->setConvertUrl($convertUrl);

            $uploadedFileUrl = $this->designer->processFileUpload();

            // Return uploaded image url directly
            if (null !== $uploadedFileUrl) {
                return new JsonResponse(['imageUrl' => $uploadedFileUrl]);
            }

            return new JsonResponse(
                ['imageUrl' => $this->designer->process()]
            );
        }

        return [
            'designer' => $this->designer,
        ];
    }

    /**
     * @param string $route
     * @param array $params
     *
     * @return string
     *
     * @throws Exception
     */
    private function buildRouteUrl(string $route = null, array $params = []): string
    {
        return $this->getRouterGenerator()->generate($route, $params,
            UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * @return UrlGeneratorInterface
     *
     * @throws Exception
     */
    private function getRouterGenerator(): UrlGeneratorInterface
    {
        return $this->router->getGenerator();
    }

}
