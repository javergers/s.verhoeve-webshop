<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\Security\Employee\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use TheNetworg\OAuth2\Client\Provider\Azure;

/**
 * @Route("/")
 * @link https://github.com/TheNetworg/oauth2-azure#authorization-code-flow
 */
class LoginController extends Controller
{
    /**
     * @Route("/login/azure")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function loginAction(Request $request)
    {
        /** @TODO STORE AZURE AD OBJECT ID */

        if ($this->getUser()) {
            return $this->redirectToRoute("admin_dashboard_index");
        }

        $provider = $this->getProvider();

        if ($request->query->get("error")) {
            throw new \Exception($request->query->get("error_description"));
        } elseif (!$request->query->get("code")) {
            $authorizationUrl = $provider->getAuthorizationUrl([]);

            $this->get("session")->set("oauth2state", $provider->getState());

            return $this->redirect($authorizationUrl);
        } elseif (!$request->query->get("state") || $request->query->get("state") !== $this->get("session")->get("oauth2state")) {
            $this->get("session")->clear();

            throw new \Exception("Invalid state");
        } else {
            $token = $provider->getAccessToken('authorization_code', [
                'code' => $request->query->get("code"),
                'resource' => 'https://graph.windows.net',
            ]);

            $user = $this->getUserFromToken($provider, $token);

            $this->login($user);

            return $this->redirectToRoute("admin_dashboard_index");
        }
    }

    /**
     * @param Azure $provider
     * @param       $token
     *
     * @return User
     */
    private function getUserFromToken(Azure $provider, $token)
    {
        $resourceOwner = $provider->getResourceOwner($token);

        $user = $this->getDoctrine()->getManager()->getRepository(User::class)->findOneBy([
            "username" => $resourceOwner->getUpn(),
        ]);

        if (!$user) {
            $user = new User();
            $user->setFirstname($resourceOwner->getFirstName());
            $user->setLastname($resourceOwner->getLastName());
            $user->setUsername($resourceOwner->getUpn());
            $user->setEmail($resourceOwner->getUpn());
            $user->setEnabled(true);
            $user->setRoles(["ROLE_USER"]);

            $this->getDoctrine()->getManager()->persist($user);
        }

        $user->setLastLogin(new \DateTime());

        $this->getDoctrine()->getManager()->flush();

        return $user;
    }

    /**
     * @return Azure
     */
    public function getProvider()
    {
        return new Azure([
            'clientId' => $this->getParameter("azure_client_id"),
            'clientSecret' => $this->getParameter("azure_client_secret"),
            'redirectUri' => $this->generateUrl("admin_login_login", [], UrlGeneratorInterface::ABSOLUTE_URL),
        ]);
    }

    /**
     * @param User $user
     */
    public function login(User $user)
    {
        $token = new UsernamePasswordToken($user, null, "admin", $user->getRoles());
        $this->get("security.token_storage")->setToken($token);

        $request = $this->get("request_stack")->getCurrentRequest();

        $event = new InteractiveLoginEvent($request, $token);

        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
    }
}
