<?php

namespace AdminBundle\Controller;

use AppBundle\Exceptions\Activity\TableNotSupportedException;
use AdminBundle\Form\Audit\AuditLogType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Security\Employee\User;
use AppBundle\Interfaces\Audit\AuditEntityInterface;
use AdminBundle\Services\Common\UserActivityHistoryService;
use DataDog\AuditBundle\Entity\Association;
use DataDog\AuditBundle\Entity\AuditLog;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

/**
 * Class HistoryController
 * @package AdminBundle\Controller
 * @Route("/history")
 */
class HistoryController extends CrudController implements CrudControllerInterface
{
    /**
     * @param null|int $n
     * @return string
     */
    public function getLabel($n = null): string
    {
        return 'Geschiedenis';
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        $historyInfo = $this->get('admin.audit_log')->getHistoryInfo();

        $user = $this->getUser();

        return [
            'label' => 'Geschiedenis',
            'entity_class' => AuditLog::class,
            'form_type' => AuditLogType::class,
            'assets' => [
                'admin/js/ContentWindow.js',
                'admin/js/history/view.js',
            ],
            'title' => function (AuditLog $log) {
                return $log->getTbl();
            },
            'datatable' => [
                'results_per_page' => 25,
                'datatable_type' => AuditLogType::class,
                'entity_class' => AuditLog::class,
                'form_type' => AuditLogType::class,
                'actions_processing' => 'override',
                'item_actions' => [
                    [
                        'icon' => 'mdi mdi-eye',
                        'route' => 'admin_history_viewdiff',
                        'method' => 'POST',
                        'type' => 'view',
                        'requirement' => function (AuditLog $log) {
                            return $log->getAction() !== 'view';
                        },
                        'javascript_callback' => 'historyPage.viewDiff',
                    ],
                ],
                'query_builder' => function (QueryBuilder $qb) use ($historyInfo, $user) {
                    return $qb
                        ->leftJoin('audit_logs.source', 's')
                        ->leftJoin('audit_logs.blame', 'b')
                        ->andWhere('audit_logs.action IN (:action)')
                        ->andWhere('audit_logs.tbl IN (:tables)')
                        ->andWhere('b.fk = :blame')
                        ->setParameter('action', $historyInfo['actions'])
                        ->setParameter('tables', $historyInfo['tables'])
                        ->setParameter('blame', $user->getId())
                        ->addOrderBy('audit_logs.loggedAt', 'DESC');
                },
                'groups' => ['loggedAt', 'tableName'],
                'add_checkbox' => false,
                'allow_add' => false,
                'allow_read' => false,
                'allow_edit' => false,
                'allow_delete' => false,
                'disable_search' => true,
            ],
        ];
    }

    /**
     * @Route("/{id}/diff", requirements={"id"="\d+"}, methods={"POST"})
     *
     * @param AuditLog $log
     *
     * @return JsonResponse
     */
    public function viewDiff(AuditLog $log): JsonResponse
    {
        $response = [];
        $notAllowedDiffs = ['id'];

        if ($log->getAction() !== 'view' && !empty($log->getDiff())) {
            $em = $this->getDoctrine();

            /** @var Association $source */
            $source = $log->getSource();

            $getData = $em->getRepository($source->getClass())->findOneBy([
                'id' => $source->getFk(),
            ]);

            if (!$getData) {
                return new JsonResponse($response);
            }

            if (!$getData || !($getData instanceof AuditEntityInterface)) {
                return new JsonResponse($response);
            }

            $pageviewSettings = $getData->getAuditSettings();

            if (!isset($pageviewSettings['mapping'])) {
                return new JsonResponse($response);
            }

            $response = [
                'action' => $log->getAction(),
                'title' => null !== $pageviewSettings['label'] ? $pageviewSettings['label']($getData) : '',
                'diff' => [],
            ];

            foreach ($log->getDiff() as $diffField => $diffValue) {
                if (
                    !\is_array($diffValue['new']) &&
                    !\is_array($diffValue['old']) &&
                    $diffValue['old'] !== $diffValue['new'] &&
                    isset($pageviewSettings['mapping'][$diffField]) &&
                    !\in_array($diffField, $notAllowedDiffs, true)
                ) {
                    unset($diffValue['col']);
                    $response['diff'][$pageviewSettings['mapping'][$diffField]] = $diffValue;
                }
            }
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/recente-activiteiten", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @throws TableNotSupportedException
     */
    public function recentActivity(Request $request)
    {
        $tables = $request->get('tables');

        $token = $this->get('security.token_storage')->getToken();

        if (null === $token || !$token instanceof UsernamePasswordToken || !$token->getUser() instanceof User) {
            return new JsonResponse([]);
        }

        /** @var User $user */
        $user = $token->getUser();

        $historyService = $this->get(UserActivityHistoryService::class);
        $historyService->setUser($user);
        $historyService->setTables($tables);

        $recentLogs = $historyService->getMostRecentLogs();

        if(null === $recentLogs) {
            return new JsonResponse([]);
        }

        $results = [];

        $router = $this->get('router');
        $em = $this->getDoctrine()->getManager();

        foreach ($recentLogs as $recentLog) {
            /** @var Association $source */
            $source = $recentLog->getSource();

            $logLabel = $source->getFk();

            $entity = $em->getRepository($source->getClass())->find($source->getFk());

            if(null === $entity) {
                return new JsonResponse([]);
            }

            $pageviewSettings = $entity->getAuditSettings();

            $tableName = $recentLog->getTbl();

            if ($tableName === 'order') {
                $logLabel = $entity->getOrderNumber();
            }

            if ($tableName === 'order_collection') {
                $logLabel = $entity->getNumber();
                $tableName = 'order';
            }

            $menuRoute = $pageviewSettings['route']($entity);

            $entityUrl = $router->generate($menuRoute['route'], $menuRoute['routeParameters']);
            $entityLabel = $pageviewSettings['label']($entity);

            $results[] = [
                'table_name' => $tableName,
                'log_label' => $logLabel,
                'entity_label' => $entityLabel,
                'entity_id' => $entity->getId(),
                'entity_url' => $entityUrl,
            ];
        };

        return new JsonResponse($results);
    }
}
