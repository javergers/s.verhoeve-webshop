<?php

namespace AdminBundle\Controller\Fraud;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Fraud\FraudDetectionRuleDatatableType;
use AdminBundle\Form\Fraud\FraudDetectionRuleFormType;
use AppBundle\Entity\Order\FraudDetectionRule;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Interfaces\Sales\OrderCollectionInterface;
use JMS\JobQueueBundle\Entity\Job;
use RuleBundle\Entity\Entity;
use RuleBundle\Entity\Rule;
use RuleBundle\Service\ResolverService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/fraude-detectie")
 * Class FraudController
 * @package AdminBundle\Controller\Fraud
 */
class FraudController extends CrudController
{

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'entity_class' => FraudDetectionRule::class,
            'form_type' => FraudDetectionRuleFormType::class,
            'datatable' => [
                'datatable_type' => FraudDetectionRuleDatatableType::class,
                'entity_class' => FraudDetectionRule::class,
                'form_type' => FraudDetectionRuleFormType::class,
                'actions' => [
                    [
                        'icon' => 'mdi mdi-plus',
                        'route' => 'admin_fraud_fraud_wizard',
                        'method' => 'GET',
                        'label' => 'Fraude regel toevoegen',
                    ],
                ],
                'item_actions' => [
                    [
                        'icon' => 'mdi mdi-eye',
                        'route' => 'rule_rule_edit',
                        'type' => 'rule_edit',
                        'method' => 'GET',
                        'override_propagation' => true,
                        'identifier' => 'rule.id'
                    ]
                ],
                'actions_processing' => 'override',
            ],
        ];
    }

    /**
     * @Route("/wizard", methods={"GET"})
     * @Template()
     *
     * @return array
     * @throws \Exception
     */
    public function wizardAction()
    {
        $em = $this->container->get('doctrine')->getManager();
        $entities = $em->getRepository(Entity::class)->findBy([
            'ruleStartPoint' => 1,
            'fullyQualifiedName' => OrderCollection::class,
        ]);

        //generate entities when entities do not exist yet
        if (empty($entities)) {
            $jobManager = $this->container->get('job.manager');

            $job = new Job('topgeschenken:entities:generate');
            $jobManager->addJob($job);
            $this->container->get('doctrine')->getManager()->flush();

            return [
                'no_entities' => true,
            ];
        }

        return [];
    }

    /**
     * @Route("/aanmaken", methods={"GET"})
     * @Template()
     *
     * @return array
     * @throws \Exception
     */
    public function createAction(): array
    {
        $ruleTransformer = $this->get('rule.transformer');

        $entity = $this->getDoctrine()->getRepository(Entity::class)->findOneBy([
            'fullyQualifiedName' => OrderCollection::class,
        ]);

        $inputs = [
            ResolverService::QB_ALIAS => $entity,
        ];

        return [
            'filters' => $ruleTransformer->getFilters($inputs),
            'entities' => [$entity],
        ];

    }

    /**
     * @Route("/aanmaken", methods={"POST"})
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function createPersistAction(): RedirectResponse
    {
        $em = $this->container->get('doctrine')->getManager();
        /** @var Rule $rule */
        $rule = $this->container->get('rule')->store($this->getRequest(), true);

        $detectionRule = new FraudDetectionRule();
        $detectionRule->setRule($rule);
        $detectionRule->setScore(0);

        $em->persist($detectionRule);
        $em->flush();

        return $this->redirectToRoute('admin_fraud_fraud_edit', ['id' => $detectionRule->getId()]);
    }

}
