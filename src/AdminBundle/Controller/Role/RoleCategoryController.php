<?php

namespace AdminBundle\Controller\Role;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Role\RoleCategoryDatatableType;
use AdminBundle\Form\Role\RoleCategoryFormType;
use AppBundle\Entity\Security\Employee\Group;
use AppBundle\Entity\Security\Employee\Role;
use AppBundle\Entity\Security\Employee\RoleCategory;
use AppBundle\Entity\Security\Employee\User;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RoleController
 * @package AdminBundle\Controller\Role
 * @Route("/instellingen/rollen")
 */
class RoleCategoryController extends CrudController
{
    /**
     * @param null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Rol categorie';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'label' => 'Rol categorie',
            'entity_class' => RoleCategory::class,
            'form_type' => RoleCategoryFormType::class,
            'datatable' => [
                'entity_class' => RoleCategory::class,
                'datatable_type' => RoleCategoryDatatableType::class,
                'form_type' => RoleCategoryFormType::class,
                'results_per_page' => 50,
                'item_actions' => [
                    [
                        'icon' => 'mdi mdi-eye',
                        'route' => 'admin_role_rolecategory_overlay',
                        'type' => 'view-role',
                        'method' => 'GET',
                    ],
                ],
            ],
            'assets' => [
                'admin/js/role/role.js',
                'admin/js/role/special-access-role.js',
            ],
        ];
    }

    /**
     * @Route("/categorie/{id}", methods={"GET"})
     * @Template("@Admin/Role/overlay.html.twig")
     * @param RoleCategory $category
     *
     * @return array
     */
    public function overlayAction(RoleCategory $category)
    {
        return [
            'category' => $category,
        ];
    }

    /**
     * @Route("/categorie/{id}", methods={"POST"})
     * @Template("@Admin/Role/overlay.html.twig")
     * @param RoleCategory $category
     * @param Request      $request
     *
     * @return array
     */
    public function persistOverlayAction(RoleCategory $category, Request $request)
    {
        $users = $request->get('users');
        $groups = $request->get('groups');

        if (null === $users && null === $groups) {
            throw new \RuntimeException('No roles received');
        }

        $users = json_decode($users, true);
        $groups = json_decode($groups, true);

        $roleCategoryService = $this->container->get('cms.role_category');

        if (!empty($users)) {
            $roleCategoryService->assignUsers($users);
        }

        if (!empty($groups)) {
            $roleCategoryService->assignGroups($groups);
        }

        return [
            'category' => $category,
        ];
    }

    /**
     * @Route("/categorie/{category}/speciale-toegang/groep/{group}", methods={"GET"})
     * @Template("@Admin/Role/specialAccess.html.twig")
     * @param RoleCategory $category
     * @param Group        $group
     *
     * @return array
     */
    public function specialAccessGroupAction(RoleCategory $category, Group $group)
    {
        return [
            'category' => $category,
            'group' => $group,
        ];
    }

    /**
     * @Route("/categorie/{category}/speciale-toegang/groep/{group}", methods={"POST"})
     * @Template("@Admin/Role/specialAccess.html.twig")
     * @param RoleCategory $category
     * @param Group        $group
     * @param Request      $request
     *
     * @return void
     */
    public function persistSpecialAccessGroupAction(RoleCategory $category, Group $group, Request $request)
    {
        $this->container->get('cms.role_category')->processRoleChangesWithFlush($group,
            json_decode($request->get('roles'), true));
    }

    /**
     * @Route("/categorie/{category}/speciale-toegang/gebruiker/{user}", methods={"GET"})
     * @Template("@Admin/Role/specialAccess.html.twig")
     * @param RoleCategory $category
     * @param User         $user
     *
     * @return array
     */
    public function specialAccessUserAction(RoleCategory $category, User $user)
    {
        return [
            'category' => $category,
            'user' => $user,
        ];
    }

    /**
     * @Route("/categorie/{category}/speciale-toegang/gebruiker/{user}", methods={"POST"})
     * @Template("@Admin/Role/specialAccess.html.twig")
     * @param RoleCategory $category
     * @param User         $user
     * @param Request      $request
     *
     * @return void
     */
    public function persistSpecialAccessUserAction(RoleCategory $category, User $user, Request $request)
    {
        $this->container->get('cms.role_category')->processRoleChangesWithFlush($user,
            json_decode($request->get('roles'), true));
    }

    /**
     * @Route("/categorie/{category}/speciale-toegang/kolom-toevoegen", methods={"POST"})
     * @param RoleCategory $category
     * @param Request      $request
     *
     * @return JsonResponse
     */
    public function persistColumnAction(RoleCategory $category, Request $request)
    {
        $column = $this->container->get('cms.role_column')->addNewColumn($category, $request);

        return new JsonResponse([
            'column_id' => $column->getId(),
            'name' => $column->getName(),
            'read' => $column->getReadRole() ? $column->getReadRole()->getId() : false,
            'write' => $column->getWriteRole() ? $column->getWriteRole()->getId() : false,
        ]);
    }

    /**
     * @Route("/categorie/{id}/rol-toevoegen")
     * @param RoleCategory $category
     * @param Request      $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function addRoleAction(RoleCategory $category, Request $request)
    {
        $role = $this->container->get('cms.role_category')->addNewRole($category, $request);

        return new JsonResponse($role);
    }

    /**
     * @Route("/rol/{role}/verwijderen", methods={"GET"})
     * @param Role $role
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteRoleAction(Role $role)
    {
        /** @var EntityManagerInterface $em */
        $em = $this->container->get('doctrine')->getManager();
        $em->remove($role);
        $em->flush();

        return new JsonResponse($role);
    }

    /**
     * @Route("/categorie/{id}/groepen", methods={"GET"})
     * @param RoleCategory $category
     *
     * @return JsonResponse
     */
    public function getGroupListAction(RoleCategory $category)
    {
        $groups = $this->container->get('cms.role_category')->getNonAssignedGroups($category);

        return new JsonResponse(['groups' => $groups]);
    }

    /**
     * @Route("/categorie/{id}/gebruikers", methods={"GET"})
     * @param RoleCategory $category
     *
     * @return JsonResponse
     */
    public function getUserListAction(RoleCategory $category)
    {
        $users = $this->container->get('cms.role_category')->getNonAssignedUsers($category);

        return new JsonResponse(['users' => $users]);
    }

}
