<?php

namespace AdminBundle\Controller\Company;

use AdminBundle\Controller\Controller;
use AppBundle\Entity\Relation\CompanyRule;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Order\Order;
use RuleBundle\Entity\Entity;
use RuleBundle\Service\ResolverService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/bedrijven/regels")
 * Class CompanyRuleController
 * @package AdminBundle\Controller\Company
 */
class CompanyRuleController extends Controller {

    /**
     * @Route("/{company}/aanmaken", methods={"GET", "POST"})
     * @Template("@Admin/Company/company-rule-create.html.twig")
     *
     * @param Request $request
     * @param Company $company
     * @return array | JsonResponse
     * @throws \Exception
     */
    public function createAction(Company $company, Request $request)
    {
        $em = $this->container->get('doctrine')->getManager();
        $ruleTransformer = $this->get('rule.transformer');

        $entities = $this->getDoctrine()->getRepository(Entity::class)->findBy([
            'ruleStartPoint' => 1,
        ]);

        if ($request->isMethod(Request::METHOD_POST)) {
            $company = $em->getRepository(Company::class)->find($request->get('company'));

            /** @var Rule $rule */
            $rule = $this->container->get('rule')->store($request, true);

            $companyRule = new CompanyRule();
            $companyRule->setRule($rule);
            $companyRule->setCompany($company);

            $em->persist($companyRule);
            $em->flush();

            return new JsonResponse(['status' => 'ok']);
        }

        $inputs = [
            ResolverService::QB_ALIAS => $em->getRepository(Entity::class)->findOneBy([
                'fullyQualifiedName' => Order::class,
            ]),
        ];

        return [
            'company' => $company,
            'filters' => $ruleTransformer->getFilters($inputs),
            'entities' => $entities,
        ];
    }

    /**
     * @Route("{companyRule}/verwijderen", methods={"POST"})
     * @Template()
     *
     * @param CompanyRule $companyRule
     *
     * @return JsonResponse
     */
    public function deleteAction(CompanyRule $companyRule)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($companyRule);
        $em->flush();

        return new JsonResponse(['status' => 'ok']);
    }
}
