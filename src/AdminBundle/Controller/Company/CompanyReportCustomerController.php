<?php

namespace AdminBundle\Controller\Company;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Company\CompanyReportCustomerDatatableType;
use AdminBundle\Form\Company\CompanyReportCustomerFormType;
use AppBundle\Entity\Report\CompanyReportCustomer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/bedrijven/rapportages/gebruikers")
 * Class CompanyReportCustomerController
 * @package AdminBundle\Controller\Company
 */
class CompanyReportCustomerController extends CrudController
{

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => CompanyReportCustomer::class,
            'form_type' => CompanyReportCustomerFormType::class,
            'datatable' => [
                'datatable_type' => CompanyReportCustomerDatatableType::class,
                'form_type' => CompanyReportCustomerFormType::class,
                'entity_class' => CompanyReportCustomer::class,
                'base_route' => 'admin_company_companyreportcustomer',
                'paginator' => [
                    'options' => [
                        'wrap-queries' => true,
                    ],
                ],
            ],
        ];
    }
}