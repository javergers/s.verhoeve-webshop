<?php

namespace AdminBundle\Controller\Company;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Company\CompanyReportCustomerDatatableType;
use AdminBundle\Datatable\Company\CompanyReportDatatableType;
use AdminBundle\Form\Company\CompanyReportCustomerFormType;
use AdminBundle\Form\Company\CompanyReportFormType;
use AdminBundle\Form\Report\ReportDownloadEntityFormType;
use AdminBundle\Services\DataTable\DataTableService;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Report\CompanyReport;
use AppBundle\Entity\Report\CompanyReportCustomer;
use AppBundle\Entity\Report\Report;
use AppBundle\Services\Report\ReportService;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Persistence\Mapping\MappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use RuleBundle\Entity\Entity;
use RuleBundle\Entity\Rule;
use RuleBundle\Service\ResolverService;
use RuleBundle\Service\RuleService;
use RuleBundle\Service\RuleTransformerService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * @Route("/bedrijven/rapportages")
 * Class CompanyReportController
 * @package AdminBundle\Controller\Company
 */
class CompanyReportController extends CrudController
{
    /**
     * @var ReportService
     */
    protected $reportService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var DataTableService
     */
    private $dataTableService;
    /**
     * @var RuleTransformerService
     */
    private $ruleTransformerService;
    /**
     * @var RuleService
     */
    private $ruleService;

    /**
     * ReportController constructor.
     * @param ReportService          $reportService
     * @param EntityManagerInterface $entityManager
     * @param RouterInterface        $router
     * @param DataTableService       $dataTableService
     * @param RuleTransformerService $ruleTransformerService
     * @param RuleService            $ruleService
     */
    public function __construct(ReportService $reportService, EntityManagerInterface $entityManager, RouterInterface $router, DataTableService $dataTableService, RuleTransformerService $ruleTransformerService, RuleService $ruleService)
    {
        $this->reportService = $reportService;
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->dataTableService = $dataTableService;
        $this->ruleTransformerService = $ruleTransformerService;
        $this->ruleService = $ruleService;
    }

    /**
     * @return array
     */
    public function getOptions()
    {

        return [
            'entity_class' => CompanyReport::class,
            'form_type' => CompanyReportFormType::class,
            'datatable' => [
                'datatable_type' => CompanyReportDatatableType::class,
                'form_type' => CompanyReportFormType::class,
                'entity_class' => CompanyReport::class,
                'base_route' => 'admin_company_companyreport',
                'paginator' => [
                    'options' => [
                        'wrap-queries' => true,
                    ],
                ],
            ],
        ];

    }

    /**
     * @Route("/{companyReport}/downloaden", methods={"GET", "POST"})
     * @Template("@Admin/Report/Report/download_xhr.html.twig")
     *
     * @param Request       $request
     * @param CompanyReport $companyReport
     * @return array|\Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws MappingException
     * @throws OptimisticLockException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     * @throws \ReflectionException
     */
    public function downloadAction(Request $request, CompanyReport $companyReport)
    {
        $form = $this->createForm(ReportDownloadEntityFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $parameters = [];

            $parameters['company'] = $companyReport->getCompany();
            foreach ($form->getData() as $key => $data) {
                if ($data !== null) {
                    $parameters[$key] = $data;
                }
            }

            return $this->reportService->generate($companyReport->getReport(), $parameters);
        }

        return [
            'report' => $companyReport->getReport(),
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/aanmaken", methods={"GET", "POST"})
     * @Template("@Admin/Company/company-report-create-rule.html.twig")
     *
     * @param Request $request
     * @return array
     * @throws AnnotationException
     * @throws MappingException
     * @throws \ReflectionException
     * @throws OptimisticLockException
     */
    public function createAction(Request $request)
    {
        $entities = $this->entityManager->getRepository(Entity::class)->findBy([
            'ruleStartPoint' => 1,
        ]);

        if ($request->isMethod(Request::METHOD_POST)) {
            /** @var Company $company */
            $company = $this->entityManager->getRepository(Company::class)->find($request->get('company'));

            /** @var Rule $rule */
            $rule = $this->ruleService->store($request, true);

            $report = new Report();
            $report->setRule($rule);
            $report->setTitle($rule->getDescription());

            $companyReport = new CompanyReport();
            $companyReport->setCompany($company);
            $companyReport->setReport($report);

            $this->entityManager->persist($companyReport);
            $this->entityManager->flush();

            $this->redirectToRoute('admin_company_companyreport_edit', ['id' => $companyReport->getId()]);
        }

        $inputs = [
            ResolverService::QB_ALIAS => $this->entityManager->getRepository(Entity::class)->findOneBy([
                'fullyQualifiedName' => Order::class,
            ]),
        ];

        return [
            'filters' => $this->ruleTransformerService->getFilters($inputs),
            'entities' => $entities,
        ];

    }

    /**
     * @Route("/link", methods={"GET", "POST"})
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function linkAction(Request $request)
    {
        if($request->isMethod("POST")) {
            /** @var Company $company */
            $company = $this->entityManager->getRepository(Company::class)->find($request->get('company'));
            /** @var Report $report */
            $report = $this->entityManager->getRepository(Report::class)->find($request->get('report'));

            $this->reportService->cloneForCompany($company, $report);

            return new JsonResponse(['status' => 'ok']);
        } else {
            $reports = $this->reportService->getAvailableReports();

            $returnReports = [];
            /** @var Report $report */
            foreach($reports as $report) {
                $returnReports[] = [
                    'id' => $report->getId(),
                    'name' => $report->getTitle()
                ];
            }

            return new JsonResponse($returnReports);
        }
    }

    /**
     * @Route("/{companyReport}/klanten", methods={"GET"})
     * @Template()
     * @param CompanyReport $companyReport
     * @return array
     * @throws \Twig_Error
     */
    public function customerAction(CompanyReport $companyReport) {
        $datatable = $this->dataTableService->getTable([
            'entity_class' => CompanyReportCustomer::class,
            'datatable_type' => CompanyReportCustomerDatatableType::class,
            'form_type' => CompanyReportCustomerFormType::class,
            'data_url' => $this->router->generate('admin_company_companyreport_companyreportdata',
                ['companyReport' => $companyReport->getId()]),
            'disable_search' => true,
            'base_route' => 'admin_company_companyreportcustomer',
        ]);

        return [
            'companyReport' => $companyReport,
            'datatable' => $datatable
        ];
    }

    /**
     * @Route("/{companyReport}/klanten/data", methods={"GET", "POST"})
     *
     * @param CompanyReport $companyReport
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function companyReportDataAction(CompanyReport $companyReport): JsonResponse
    {
        return $this->dataTableService->getData([
            'parent' => [
                'entity' => 'companyReport',
                'parentId' => $companyReport->getId(),
            ],
            'entity_class' => CompanyReportCustomer::class,
            'datatable_type' => CompanyReportCustomerDatatableType::class,
            'form_type' => CompanyReportCustomerFormType::class,
            'base_route' => 'admin_company_companyreportcustomer',
        ]);
    }


}
