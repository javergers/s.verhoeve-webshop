<?php

namespace AdminBundle\Controller\Company;

use AdminBundle\Controller\Controller;
use AdminBundle\Form\Company\CompanySiteFormType;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanySite;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/bedrijven-sites")
 *
 * Class CompanySiteController
 * @package AdminBundle\Controller\Company
 */
class CompanySiteController extends Controller
{

    /**
     * @Route("/{company}", methods={"GET"})
     * @Template()
     *
     * @param Company $company
     *
     * @return array
     */
    public function indexAction(Company $company)
    {
        $companySite = $company->getSite();

        return [
            'company' => $company,
            'companySite' => $companySite,
        ];
    }

    /**
     * @Route("/{company}/bewerken", methods={"GET", "POST"})
     * @Template("@Admin/Crud/form_xhr.html.twig")
     *
     * @param Company $company
     *
     * @param Request $request
     * @return array
     */
    public function editAction(Company $company, Request $request)
    {

        $errors = null;
        $em = $this->container->get('doctrine')->getManager();

        if ($company->getSite()) {
            $companySite = $company->getSite();
        } else {
            $companySite = new CompanySite();
            $companySite->setRegistrationEnabled(false);

            $company->setSite($companySite);

            $em->persist($companySite);
            $em->flush();
        }

        $form = $this->createForm(CompanySiteFormType::class, $companySite, [
            'action' => $this->generateUrl('admin_company_companysite_edit', ['company' => $company->getId()]),
        ]);

        $form->handleRequest($request);

        if($form->isSubmitted()) {
            if($form->isValid()) {
                $companySite = $form->getData();

                if(!$companySite->getId()) {
                    $em->persist($companySite);
                }

                $em->flush();
            } else {
                $errors = $form->getErrors(true);
            }
        }

        return [
            'errors' => $errors,
            'form' => $form->createView(),
        ];
    }

}
