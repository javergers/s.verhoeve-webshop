<?php

namespace AdminBundle\Controller\Company;

use AdminBundle\Controller\Controller;
use AdminBundle\Form\Company\CompanyAssortmentDefaultSettingsType;
use AdminBundle\Form\Company\CompanyAssortmentFormType;
use AppBundle\DBAL\Types\MenuLocationType;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Assortment\AssortmentType;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Site\Menu;
use AppBundle\Entity\Site\MenuItem;
use AppBundle\Manager\Catalog\Assortment\CompanyAssortmentManager;
use AppBundle\Manager\Catalog\Product\ProductManager;
use AppBundle\Manager\MenuManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Route("/bedrijven/assortimenten")
 * Class CompanyAssortmentController
 * @package AdminBundle\Controller\Company
 */
class CompanyAssortmentController extends Controller
{

    /**
     * @Route("/{company}/toevoegen", methods={"GET"})
     * @Template()
     *
     * @param Company $company
     * @param Request $request
     *
     * @return array
     */
    public function addAction(Company $company, Request $request)
    {
        $assortment = new Assortment();

        if($request->get('isCardAssortment')) {
            $assortmentType = $this->getDoctrine()->getManager()->getRepository(AssortmentType::class)->findOneBy([
                'key' => AssortmentType::TYPE_CARDS
            ]);

            $assortment->setAssortmentType($assortmentType);
        }

        $assortment->setOwner($company);

        return [
            'company' => $company,
            'form' => $this->createForm(CompanyAssortmentFormType::class, $assortment)->createView(),
        ];
    }

    /**
     * @Route("/{company}/assortimenten/{assortment}/bekijken", methods={"GET"})
     * @Template()
     * @param Company    $company
     * @param Assortment $assortment
     *
     * @return array
     */
    public function viewAction(Company $company, Assortment $assortment)
    {
        return [
            'company' => $company,
            'assortment' => $assortment,
            'productManager' => $this->container->get(ProductManager::class) //todo SF4
        ];
    }

    /**
     * @Route("/{company}/assortimenten/{assortment}/bewerken", methods={"GET"})
     * @Template()
     * @param Company    $company
     * @param Assortment $assortment
     *
     * @return array
     * @throws \Exception
     */
    public function editAction(Company $company, Assortment $assortment)
    {
        if (!$assortment->getOwner() || $assortment->getOwner()->getId() != $company->getId()) {
            throw new AccessDeniedException("The assortment is not owned by the company");
        }

        $options = [];
        if ($assortment->getOwner() && $assortment->getOwner()->getId() != $company->getId()) {
            $options['data']['locked'] = true;
        }

        return [
            'company' => $company,
            'form' => $this->createForm(CompanyAssortmentFormType::class, $assortment, $options)->createView(),
        ];
    }

    /**
     * @Route("/{company}/assortimenten/{assortment}/bewerken", methods={"POST"})
     * @Template("@Admin/Company/CompanyAssortment/edit.html.twig")
     * @param Company    $company
     * @param Assortment $assortment
     * @param Request    $request
     *
     * @return array
     */
    public function persistEditAction(Company $company, Assortment $assortment, Request $request)
    {
        $form = $this->createForm(CompanyAssortmentFormType::class, $assortment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
        }

        return [
            'form' => $form->createView(),
            'assortment' => $assortment,
            'company' => $company,
        ];
    }

    /**
     * @Route("/{company}/toevoegen", methods={"POST"})
     * @Template()
     * @param Company $company
     * @param Request $request
     *
     * @return array
     */
    public function persistAddAction(Company $company, Request $request)
    {
        $form = $this->createForm(CompanyAssortmentFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Assortment $assortment */
            $assortment = $form->getData();
            $assortment->addCompany($company);
            $assortment->setOwner($company);

            //TODO SF4
            $em = $this->container->get('doctrine')->getManager();
            $em->persist($assortment);
            $em->flush();

            if(null === $assortment->getAssortmentType() || !\in_array($assortment->getAssortmentType()->getKey(),
                [AssortmentType::TYPE_ADDITIONAL_PRODUCTS, AssortmentType::TYPE_CARDS], true)) {

                $menuManager = $this->get(MenuManager::class);
                $menu = $menuManager->createForCompany($company);
                $menuManager->addAssortment($menu, $assortment);

                $em->flush();
            }

        }

        return [
            'assortment' => $assortment,
            'company' => $company,
        ];
    }

    /**
     * @Route("/{company}/basis-instellingen", methods={"GET"})
     * @Template()
     * @param Company $company
     *
     * @return array
     */
    public function defaultSettingsAction(Company $company)
    {
        //TODO SF4
        $assortments = $this->container->get('admin.company_assortment.service')->getAssortmentsForField();

        $form = $this->createForm(CompanyAssortmentDefaultSettingsType::class, null, [
            'data' => [
                'assortments' => $assortments,
            ],
        ]);

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/{company}/basis-instellingen", methods={"POST"})
     * @Template()
     * @param Company $company
     * @param Request $request
     *
     * @return array
     */
    public function persistDefaultSettingsAction(Company $company, Request $request)
    {
        //TODO SF4
        $this->container->get('admin.company_assortment.service')->copyAssortmentsForCompany($company, $request);

        return [
            'company' => $company,
        ];
    }

    /**
     * @Route("/{company}/dupliceer-assortiment", methods={"POST"})
     * @param Company $company
     * @param Request $request
     * @return JsonResponse
     */
    public function duplicateSingleAssortment(Company $company, Request $request): JsonResponse
    {
        $assortment = $this->getDoctrine()->getRepository(Assortment::class)->find($request->get('assortment'));

        $this->get(CompanyAssortmentManager::class)->duplicate($company, $assortment, true);

        $menuManager = $this->get(MenuManager::class);
        $menu = $company->getMenu();
        if(null === $menu) {
            $menu = $menuManager->createForCompany($company);
        }

        $menuManager->addAssortment($menu, $assortment);

        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
            'ok' => 'Duplicated successfully'
        ]);
    }

    /**
     * @Route("/{company}/assortimenten/{assortment}/verwijderen", methods={"DELETE"})
     * @param Company    $company
     * @param Assortment $assortment
     *
     * @return JsonResponse
     */
    public function deleteAction(Company $company, Assortment $assortment)
    {
        //TODO SF4
        $em = $this->container->get('doctrine')->getManager();

        if ($assortment->getOwner() && ($company->getId() === $assortment->getOwner()->getId())) {
            foreach ($assortment->getAssortmentProducts() as $assortmentProduct) {
                $assortment->removeAssortmentProduct($assortmentProduct);
                $em->remove($assortmentProduct);
            }

            $em->remove($assortment);
        } else {
            $assortment->removeCompany($company);

        }

        $company->removeAssortment($assortment);

        //delete menu item from assortment
        $menu = $company->getMenu();

        /** @var MenuItem $item */
        foreach($menu->getItems() as $item) {
            if($item->getEntityName() === Assortment::class && $item->getEntityId() === $assortment->getId()) {
                $menu->removeItem($item);
                $em->remove($item);
            }
        }

        //delete menu if no assortments are left
        if($company->getAssortments()->isEmpty()) {
            $company->setMenu(null);
            $em->remove($menu);
        }

        $em->flush();

        return new JsonResponse('ok');
    }

}
