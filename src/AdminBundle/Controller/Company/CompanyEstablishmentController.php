<?php

namespace AdminBundle\Controller\Company;

use AdminBundle\Annotation\Breadcrumb;
use AdminBundle\Controller\CrudController;
use AdminBundle\Form\Company\CompanyEstablishmentType;
use AdminBundle\Interfaces\CrudControllerInterface;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyEstablishment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CompanyEstablishmentController
 * @package AdminBundle\Controller\Company
 * @Route("/vestigingen")
 */
class CompanyEstablishmentController extends CrudController implements CrudControllerInterface
{
    /**
     * @param int|null $n
     * @return string
     */
    public function getLabel($n = null)
    {
        return 'Vestigingen';
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'entity_class' => CompanyEstablishment::class,
            'form_type' => CompanyEstablishmentType::class,
            'datatable' => [
                'entity_class' => CompanyEstablishment::class,
                'form_type' => CompanyEstablishmentType::class,
                'datatable_type' => \AdminBundle\Datatable\Company\CompanyEstablishmentType::class,
            ],
            'assets' => [
                'admin/js/form/company-establishment.js',
            ],
        ];
    }

    /**
     * @Route("/{id}/bewerken", methods={"GET"})
     * @ParamConverter("id", options={"mapping": { "companyEstablishment" }})
     * @Breadcrumb()
     *
     * @param CompanyEstablishment $companyEstablishment
     * @return Response
     * @throws \Exception
     */
    public function editCompanyEstablishment(CompanyEstablishment $companyEstablishment){
        $formData = $this->form($companyEstablishment, false);
        return $this->render('@Admin/Crud/div_collection_form.html.twig', $formData);
    }

    /**
     * @Route("/{id}/bewerken", methods={"POST"})
     * @ParamConverter("id", options={"mapping": { "companyEstablishment" }})
     * @Breadcrumb()
     *
     * @param CompanyEstablishment $companyEstablishment
     * @return Response
     * @throws \ReflectionException
     * @throws \Exception
     */
    public function editPersistCompanyEstablishment(CompanyEstablishment $companyEstablishment){
        $this->denyAccessUnlessGranted($this->getRolesFor('update'));

        $form = $this->getForm($this->getFormType(), $companyEstablishment);

        $form->handleRequest($this->getRequest());
        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('doctrine')->getManager()->flush();
        }

        return $this->editCompanyEstablishment($companyEstablishment);
    }

    /**
     * @Route("/details", name="topgeschenken.company.establishment.details", methods={"GET"})
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     * @throws \SoapFault
     * @throws \libphonenumber\NumberParseException
     */
    public function getDetailsByEstablishmentNumber(Request $request)
    {
        $establishmentNumber = $request->get('establishmentNumber');
        $company = $this->container->get('doctrine')->getRepository(Company::class)->find($request->get('company'));

        if (!$company || !$company->getCompanyRegistration()) {
            return new JsonResponse("");
        }

        $details = $this->container->get('webservices_nl')->getCompanyDataByChamberOfCommerceNumber($company->getCompanyRegistration()->getRegistrationNumber(),
            $establishmentNumber);

        return new JsonResponse($details);
    }

    /**
     * @Route("/{id}/verwijderen", methods={"DELETE"})
     * @ParamConverter("CompanyEstablishment", options={"mapping": {"id": "companyEstablishment"}})
     * @param CompanyEstablishment $companyEstablishment
     * @return Response
     * @throws \ReflectionException
     */
    public function deleteCompanyEstablishment(CompanyEstablishment $companyEstablishment)
    {
        if($companyEstablishment->isMain() && $companyEstablishment->isVerified()){
            $this->get('session')->getFlashBag()->add('backend.error', vsprintf('Geverifieerde hoofdvestigingen (%s %d) mogen niet verwijderd worden..', [
                $companyEstablishment->getName(),
                $companyEstablishment->getId()
            ]));
            return new Response();
        }
        return $this->deleteAction($companyEstablishment);
    }
}
