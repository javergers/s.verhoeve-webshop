<?php

namespace AdminBundle\Controller;

use AdminBundle\Datatable\Order\BakeryOrderDatatableType;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Site\Site;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Services\OrderLineService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DashboardController
 * @package AdminBundle\Controller
 */
class DashboardController extends Controller
{
    /**
     * @var string
     */
    protected $guzzleClient;

    public function getLabel() {
        return "Dashboard";
    }

    /**
     * @Route("/", methods={"GET"})
     * @Template()
     * @throws \Exception
     */
    public function indexAction()
    {
        $returnArray = [];

        $returnArray['unprocessedOrders'] = $this->getDatatable('admin_sales_bakeryorder_unprocessedorders');
        $returnArray['processedOrders'] = $this->getDatatable('admin_sales_bakeryorder_processedorders');

        //only show to users in Beheerders group
        if ($this->getUser()->hasGroup('Beheerders') || $this->container->get('security.token_storage')->getToken()->getUser()->hasRole('ROLE_SUPER_ADMIN')) {
            $orderRepository = $this->getDoctrine()->getRepository(Order::class);
            $customerRepository = $this->getDoctrine()->getRepository(Customer::class);

            $sites = $this->getDoctrine()->getRepository(Site::class)->findAll();

            $productgroups = $this->getDoctrine()->getRepository(Productgroup::class)->findAll();

            //Orders vandaag (optionele filter per platform)
            $ordersToday = $orderRepository->findRecentOrders();

            //Nieuwste klanten
            $recentCustomers = $customerRepository->findRecentCustomers();

            //Laatste actieve klanten
            $activeCustomers = $customerRepository->findRecentActiveCustomers();

            //Nieuwste bedrijven
            $recentCompanies = $this->getDoctrine()->getRepository(Company::class)->findRecentCompanies();

            //Orders per productcategorie
            $productGroupLines = $this->get(OrderLineService::class)->getRecentOrderLinesByGroup();

            $returnArray['ordersToday'] = $ordersToday;
            $returnArray['sites'] = $sites;
            $returnArray['recentCustomers'] = $recentCustomers;
            $returnArray['recentCompanies'] = $recentCompanies;
            $returnArray['activeCustomers'] = $activeCustomers;
            $returnArray['productGroupLines'] = $productGroupLines;
            $returnArray['productgroups'] = $productgroups;
        }

        return $returnArray;
    }

    /**
     * @param $route
     * @return string
     * @throws \Exception
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function getDatatable($route) {
        return  $this->container->get('admin.datatable.service')->getTable([
            'entity_class' => Order::class,
            'datatable_type' => BakeryOrderDatatableType::class,
            'form_type' => Order::class,
            'data_url' => $this->generateUrl($route),
            'disable_search' => true,
            'allow_add' => false,
            'allow_read' => false,
            'allow_delete' => false,
            'allow_edit' => false,
            'add_checkbox' => false,
            'item_actions' => [
                [
                    'icon' => 'mdi mdi-eye',
                    'route' => 'admin_sales_order_order',
                    'type' => 'get-order',
                    'method' => 'GET',
                    'identifier' => [
                        'field' => 'orderCollection',
                        'id' => 'orderCollection.id',
                    ],
                ],
                [
                    'icon' => 'fa fa-check',
                    'route' => 'admin_sales_orderorderprocess_manuallyprocess',
                    'type' => 'manually-process-order',
                    'method' => 'GET',
                    'identifier' => [
                        'field' => 'order',
                    ],
                ],
            ]
        ]);
    }
}
