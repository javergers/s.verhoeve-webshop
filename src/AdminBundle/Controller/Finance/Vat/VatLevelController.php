<?php

namespace AdminBundle\Controller\Finance\Vat;

use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Finance\Vat\VatLevelDatatableType;
use AdminBundle\Form\Finance\Vat\VatLevelFormType;
use AppBundle\Entity\Finance\Tax\VatLevel;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/financieel/btw-niveaus")
 * Class VatLevelController
 * @package AdminBundle\Controller\Finance\Vat
 */
class VatLevelController extends CrudController
{
    /**
     * @return array
     */
    public function getOptions(): array
    {
        return [
            'entity_class' => VatLevel::class,
            'form_type' => VatLevelFormType::class,
            'datatable' => [
                'entity_class' => VatLevel::class,
                'form_type' => VatLevelFormType::class,
                'datatable_type' => VatLevelDatatableType::class,
            ]
        ];
    }
}
