<?php

namespace AdminBundle\Controller\Finance;

use AdminBundle\Annotation\Breadcrumb;
use AdminBundle\Components\Crud\CrudActionResponse;
use AdminBundle\Controller\CrudController;
use AdminBundle\Datatable\Finance\ExportCompanySelectType;
use AdminBundle\Datatable\Finance\InvoiceExportType;
use AdminBundle\Services\DataTable\DataTableService;
use AdminBundle\Services\Finance\InvoiceExportGeneratorService;
use AdminBundle\Services\Finance\InvoiceExportService;
use AppBundle\DBAL\Types\ExactInvoiceExportStatusType;
use AppBundle\Entity\Relation\Company;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\DateType;
use AppBundle\Services\JobManager;
use AppBundle\ThirdParty\Exact\Entity\InvoiceExport;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Exception;
use JMS\JobQueueBundle\Entity\Job;
use League\Flysystem\FileNotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;
use Symfony\Component\Routing\RouterInterface;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;

/**
 * @Route("/financieel/facturatie")
 */
class InvoiceExportController extends CrudController
{
    /**
     * @var InvoiceExportService
     */
    private $invoiceExportService;

    /**
     * @var InvoiceExportGeneratorService
     */
    private $invoiceExportGeneratorService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var DataTableService
     */
    private $datatableService;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var JobManager
     */
    private $jobManager;

    /**
     * @var FlashBagInterface
     */
    private $flashBag;

    /**
     * @required
     *
     * @param InvoiceExportService $invoiceExportService
     * @param InvoiceExportGeneratorService $invoiceExportGeneratorService
     * @param EntityManagerInterface $entityManager
     * @param DataTableService $datatableService
     * @param RouterInterface $router
     * @param JobManager $jobManager
     * @param FlashBagInterface $flashBag
     */
    public function setInvoiceExportService(
        InvoiceExportService $invoiceExportService,
        InvoiceExportGeneratorService $invoiceExportGeneratorService,
        EntityManagerInterface $entityManager,
        DataTableService $datatableService,
        RouterInterface $router,
        JobManager $jobManager,
        FlashBagInterface $flashBag
    ): void
    {
        $this->invoiceExportService = $invoiceExportService;
        $this->invoiceExportGeneratorService = $invoiceExportGeneratorService;
        $this->entityManager = $entityManager;
        $this->datatableService = $datatableService;
        $this->router = $router;
        $this->jobManager = $jobManager;
        $this->flashBag = $flashBag;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'label' => 'Exact Globe facturatie',
            'entity_class' => InvoiceExport::class,
            'datatable' => [
                'query_builder' => function(QueryBuilder $queryBuilder) {
                    return $queryBuilder->orderBy('exact_invoice_export.invoiceDate', 'DESC');
                },
                'entity_class' => InvoiceExport::class,
                'form_type' => false,
                'datatable_type' => InvoiceExportType::class,
                'actions_processing' => 'override',
                'actions' => [
                    [
                        'icon' => 'mdi mdi-plus',
                        'route' => 'admin_finance_invoiceexport_export',
                        'label' => 'Export toevoegen',
                        'className' => 'btn-success',
                    ],
                ],
            ],
        ];
    }

    /**
     * @Route("/", methods={"GET"})
     *
     * @return Response
     * @Breadcrumb()
     * @throws Exception
     */
    public function showInvoiceOverview(): Response
    {
        $this->invoiceExportService->checkForFailedExportJobs();

        return $this->datatableAction();
    }

    /**
     * @Route("/export", methods={"GET", "POST"})
     * @Template()
     * @Breadcrumb()
     * @param RequestStack $requestStack
     * @return array|RedirectResponse
     * @throws Exception
     */
    public function export(RequestStack $requestStack)
    {
        if (false === $this->invoiceExportService->canCreateNewExport()) {
            $this->addFlash('backend.error',
                'Facturatie export niet aangemaakt.<br /><i>Er zijn nog niet afgeronde exports</i>'
            );
            return $this->redirectToRoute('admin_finance_invoiceexport_index');
        }

        $form = $this->getInvoiceExportForm();
        $request = $requestStack->getCurrentRequest();
        $form->handleRequest($request);
        $formContainer = $form->get('export_container');

        if ($form->isSubmitted() && $form->isValid()) {
            $invoiceExport = new InvoiceExport();
            $invoiceExport->setDeliveryDate($formContainer->get('deliveryDate')->getData());
            $invoiceExport->setInvoiceDate($formContainer->get('invoiceDate')->getData());

            $selectCompaniesType = $formContainer->get('selectCompanies')->getData();
            if ($selectCompaniesType === 'manual') {

                $this->entityManager->persist($invoiceExport);
                $this->entityManager->flush();

                return $this->redirectToRoute('admin_finance_invoiceexport_selectcompaniesforinvoice', [
                    'invoiceExport' => $invoiceExport->getId(),
                ]);
            }

            if($selectCompaniesType === 'all') {
                /** @var QueryBuilder $qb */
                $qb = $this->invoiceExportService->getCompaniesForExport($invoiceExport->getDeliveryDate(), true);
                $qb->select('company.id');
                $ids = [];
                foreach($qb->getQuery()->getArrayResult() as $idResult){
                    $ids[] = $idResult['id'];
                }
                $invoiceExport->setCompaniesToProcess($ids);
            }

            $this->entityManager->persist($invoiceExport);
            $this->entityManager->flush();

            $job = new Job('app:finance:invoice-export-generator', [$invoiceExport->getId()]);
            $this->jobManager->addJob($job);
            $invoiceExport->setExportStatus(ExactInvoiceExportStatusType::IN_PROGRESS);
            $this->entityManager->flush();

            $this->get('session')->getFlashBag()->add('frontend.success', 'Facturatie export is aangemaakt en wordt uitgevoerd op de achtergrond.');
            return $this->redirectToRoute('admin_finance_invoiceexport_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @param InvoiceExport $invoiceExport
     * @return array
     */
    private function getSelectCompaniesForInvoiceOptions(InvoiceExport $invoiceExport)
    {
        return [
            'entity_class' => Company::class,
            'form_type' => false,
            'datatable_type' => ExportCompanySelectType::class,
            'actions_processing' => 'override',
            'bulk_actions' => [
                [
                    'icon' => 'mdi mdi-export',
                    'route' => 'admin_finance_invoiceexport_saveselectcompaniesforinvoice',
                    'route_params' => [
                        'invoiceExport' => $invoiceExport->getId(),
                    ],
                    'label' => 'Exporteer geselecteerde bedrijven',
                    'method' => 'POST',
                    'multiple' => true,
                    'pre_confirm' => [
                        'text' => 'Facturen exporteren?',
                        'confirmButtonColor' => '#3085d6',
                    ],
                ],
            ],
            'query_builder' => function (QueryBuilder $qb) use ($invoiceExport) {
                $this->invoiceExportService->getCompaniesForExport($invoiceExport->getDeliveryDate(), true, $qb);
            },
        ];
    }

    /**
     * @Route("/{invoiceExport}/select-companies", methods={"POST"})
     * @param InvoiceExport $invoiceExport
     * @param Request $request
     *
     * @return CrudActionResponse
     * @Breadcrumb()
     */
    public function saveSelectCompaniesForInvoice(InvoiceExport $invoiceExport, Request $request): CrudActionResponse
    {

        $ids = $request->request->get('ids');

        try {
            if (!$ids) {
                throw new MissingMandatoryParametersException('Missing ids in POST request');
            }

            $invoiceExport->setCompaniesToProcess($ids);

            $job = new Job('app:finance:invoice-export-generator', [$invoiceExport->getId()]);
            $this->jobManager->addJob($job);
            $invoiceExport->setExportStatus(ExactInvoiceExportStatusType::IN_PROGRESS);
            $this->entityManager->flush();

        } catch (Exception $e) {
            return new CrudActionResponse([
                'message' => [
                    'type' => 'error',
                    'text' => 'Er is een fout opgetreden tijdens het verwerken van het verzoek',
                ],
            ]);
        }

        return new CrudActionResponse([
            'message' => [
                'title' => 'Export is aangemaakt',
                'type' => 'success',
                'text' => 'Facturatie export is aangemaakt en wordt uitgevoerd op de achtergrond.',
                'redirect' => $this->router->generate('admin_finance_invoiceexport_index'),
            ],
        ]);
    }

    /**
     * @Route("/{invoiceExport}/select-companies", methods={"GET"})
     * @param InvoiceExport $invoiceExport
     * @param Request $request
     *
     * @return Response
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Runtime
     * @throws Twig_Error_Syntax
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Recurr\Exception
     * @Breadcrumb()
     */
    public function selectCompaniesForInvoice(InvoiceExport $invoiceExport, Request $request): Response
    {
        $orders = $this->invoiceExportService->ordersAvailableForExport($invoiceExport->getDeliveryDate());

        if (!$orders) {
            $this->addFlash('backend.error',
                'Facturatie export niet aangemaakt.<br /><i>Geen bestellingen om te exporteren.</i>'
            );

            return $this->redirectToRoute('admin_finance_invoiceexport_index');
        }

        $datatable = $this->datatableService->getTable($this->getSelectCompaniesForInvoiceOptions($invoiceExport));

        return $this->render(
            // TODO SF4 snake_case and no caps
            '@Admin/Finance/InvoiceExport/select_companies_for_export.html.twig',
            [
                'datatable' => $datatable,
                'crud' => $this,
                'page_title' => $this->getLabel() ? $this->getLabel() . ' overzicht' : 'Overzicht',
                'breadcrumb_menu' => $this->getBreadcrumbMenu(),
                'assets' => $this->getAssets(),
            ]
        );
    }

    /**
     * @Route("/{invoiceExport}/data", methods={"GET", "POST"})
     * @param InvoiceExport $invoiceExport
     * @return Response
     * @Breadcrumb()
     * @throws Exception
     */
    public function selectCompaniesForInvoiceData(InvoiceExport $invoiceExport): Response
    {
        return $this->datatableService->getData($this->getSelectCompaniesForInvoiceOptions($invoiceExport));
    }

    /**
     * @Route("/{invoiceExport}/download-companies")
     * @param InvoiceExport $invoiceExport
     * @return Response
     */
    public function downloadCompaniesAction(InvoiceExport $invoiceExport): Response
    {
        try {
            $response = new Response($this->invoiceExportGeneratorService->readDebtorsXml($invoiceExport));
            $filename = sprintf('debtors-%d.xml', $invoiceExport->getId());
            $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);
            $response->headers->set('Content-Disposition', $disposition);
            return $response;
        } catch (FileNotFoundException $e) {
            return new Response('File not found', 404);
        }
    }

    /**
     * @Route("/{invoiceExport}/download-orders")
     * @param InvoiceExport $invoiceExport
     * @return Response
     */
    public function downloadOrdersAction(InvoiceExport $invoiceExport): Response
    {
        try {
            $response = new Response($this->invoiceExportGeneratorService->readInvoicesXml($invoiceExport));
            $filename = sprintf('invoices-%d.xml', $invoiceExport->getId());
            $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);
            $response->headers->set('Content-Disposition', $disposition);
            return $response;
        } catch (FileNotFoundException $e) {
            return new Response('File not found', 404);
        }
    }

    /**
     * @return FormInterface
     * @throws Exception
     */
    private function getInvoiceExportForm()
    {
        $builder = $this->createFormBuilder();
        $container = $builder->create('export_container', ContainerType::class, []);

        $data = (new DateTime())->sub(new DateInterval('P1D'));
        $container->add('deliveryDate', DateType::class, [
            'label' => 'Bezorgdatum (tot en met)',
            'data_class' => null,
            'data' => $data,
        ]);
        $container->add('invoiceDate', DateType::class, [
            'label' => 'Factuurdatum',
            'data_class' => null,
            'data' => $data,
        ]);
        $container->add('selectCompanies', ChoiceType::class, [
            'label' => 'Te exporteren bedrijven',
            'choices' => [
                'Handmatige selectie' => 'manual',
                'Alle beschikbare bedrijven' => 'all',
            ]
        ]);

        $builder->add($container);

        $builder->add('submit', SubmitType::class, [
            'label' => 'Export aanmaken',
            'attr' => [
                'class' => 'btn-primary create-invoice-export',
            ],
            'icon' => 'checkmark-circle',
        ]);

        return $builder->getForm();
    }
}
