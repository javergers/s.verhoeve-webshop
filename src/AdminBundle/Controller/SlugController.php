<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\Catalog\Product\Product;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/slug")
 * Class SlugController
 * @package AdminBundle\Controller
 */
class SlugController extends Controller
{
    /**
     * @Route("/is-uniek", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function uniqueCheckAction(Request $request)
    {
        $slug = $request->get('slug');

        /** @var QueryBuilder $qb */
        $qb = $this->container->get('doctrine')->getRepository(Product::class)->createQueryBuilder('p');
        $qb->leftJoin('p.translations', 'pt')
            ->where('pt.slug = :slug')
            ->setParameter('slug', $slug);

        $results = $qb->getQuery()->getResult();

        if (count($results)) {
            return new JsonResponse(['message' => 'error']);
        }

        return new JsonResponse(['message' => 'ok']);
    }
}
