<?php


namespace AdminBundle\EventListener;

use AppBundle\Entity\Designer\DesignerTemplateThumb;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\OptimisticLockException;
use League\Flysystem\Filesystem;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class DesignerTemplateThumbSubscriber
 * @package AdminBundle\EventListener
 */
class DesignerTemplateThumbSubscriber implements EventSubscriber
{
    use ContainerAwareTrait;

    /**
     * @param LifecycleEventArgs $args
     * @throws OptimisticLockException
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        if (is_object($args)) {
            $this->update($args, true);
        }
    }

    /**
     * @param LifecycleEventArgs $args
     * @param bool               $upload
     * @throws OptimisticLockException
     */
    private function update(LifecycleEventArgs $args, $upload = false)
    {
        $designerTemplateThumb = $args->getEntity();

        // only act on some "DesignerTemplateThumb" entity
        if (!$designerTemplateThumb instanceof DesignerTemplateThumb) {
            return;
        }

        $moveToPath = str_replace('tmp/images/designer',
            'images/designer/' . $designerTemplateThumb->getProduct()->getId(),
            $designerTemplateThumb->getPath());

        if ($upload) {
            /**
             * @var Filesystem $filesystem
             */
            $filesystem = $this->container->get('filesystem_public');

            if (!$filesystem->has($moveToPath)) {
                $filesystem->rename($designerTemplateThumb->getPath(), $moveToPath);
            }
        }

        $em = $args->getEntityManager();

        $designerTemplateThumb->setPath(str_replace('tmp/images/designer',
            'images/designer/' . $designerTemplateThumb->getProduct()->getId(), $designerTemplateThumb->getPath()));

        $em->flush();

        $changeset = $em->getUnitOfWork()->getEntityChangeSet($designerTemplateThumb);

        /**
         * Only add a transformation job if the image path changed
         **/
        if (!empty($changeset['path'])) {
            /**
             * @var Filesystem $filesystem
             */
            $filesystem = $this->container->get('filesystem_public');

            if (!$filesystem->has($moveToPath)) {
                $filesystem->rename($designerTemplateThumb->getPath(), $moveToPath);
            }

            $this->addTransformation($args->getEntity());
        }
    }

    /**
     * @param $entity
     * @throws OptimisticLockException
     */
    private function addTransformation($entity)
    {
        $this->container->get('cms.media_image')->process($entity);
    }

    /**
     * @param LifecycleEventArgs $args
     * @throws OptimisticLockException
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        if (is_object($args)) {
            $this->update($args, false);
        }
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist'
        ];
    }
}
