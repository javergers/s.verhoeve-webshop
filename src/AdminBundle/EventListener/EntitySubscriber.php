<?php


namespace AdminBundle\EventListener;

use AppBundle\Exceptions\NonDeletableException;
use AppBundle\Exceptions\NonEditableException;
use AppBundle\Traits\NonDeletableEntity;
use AppBundle\Traits\NonEditableEntity;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;

class EntitySubscriber implements EventSubscriber
{
    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'preUpdate',
            'preRemove',
        ];
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($args->getEntityManager()->getFilters()->isEnabled('noneditable') && in_array(NonEditableEntity::class,
                class_uses($entity))) {
            $columnWhitelist = $entity::getIgnoredColumns();

            $uow = $args->getEntityManager()->getUnitOfWork();
            $changeset = $uow->getEntityChangeSet($entity);

            $columnWhitelist = array_flip($columnWhitelist);
            $blockedColumns = array_diff_key($changeset, $columnWhitelist);

            if (!$entity->isEditable() && count($blockedColumns) >= 1) {
                throw new NonEditableException();
            }
        }
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($args->getEntityManager()->getFilters()->isEnabled('nondeletable') && in_array(NonDeletableEntity::class,
                class_uses($entity)) && !$entity->isDeletable()) {
            throw new NonDeletableException();
        }
    }
}
