<?php


namespace AdminBundle\EventListener;

use Oneup\UploaderBundle\Event\PostUploadEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class DesignerTemplateThumbListener
 * @package AdminBundle\EventListener
 */
class DesignerTemplateThumbListener
{
    /**
     * @var null
     */
    private $kernelRootDir;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var string
     */
    private $tmpFilePath;

    /**
     * DesignerTemplateThumbListener constructor.
     * @param null             $kernelRootDir
     * @param SessionInterface $session
     */
    public function __construct($kernelRootDir = null, SessionInterface $session)
    {
        $this->kernelRootDir = $kernelRootDir;
        $this->session = $session;
    }

    /**
     * @param PostUploadEvent $event
     */
    public function onUpload(PostUploadEvent $event)
    {
        $response = $event->getResponse();

        $webDir = $this->kernelRootDir . '/../web';
        $orphanageDir = $this->kernelRootDir . '/../web/uploader/orphanage/' . $this->session->getId() . '/' . $event->getType() . '/';

        $response['thumbnail_path'] = str_replace($webDir, null, $event->getFile()->getPathname());
        $response['file_path'] = str_replace($orphanageDir, null, $event->getFile()->getPathname());

        $this->tmpFilePath = $response['file_path'];
    }
}
