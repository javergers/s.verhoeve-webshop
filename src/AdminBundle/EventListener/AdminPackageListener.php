<?php

namespace AdminBundle\EventListener;

use Symfony\Component\Asset\PathPackage;
use Symfony\Component\Asset\VersionStrategy\StaticVersionStrategy;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class AdminPackageListener
 * @package AdminBundle\EventListener
 */
class AdminPackageListener
{
    use ContainerAwareTrait;

    public function onKernelRequest()
    {
        $currentRequest = $this->container->get('request_stack')->getCurrentRequest();

        if ($currentRequest
            && strpos($currentRequest->getRequestUri(), '/admin') === 0
            && strpos($currentRequest->getRequestUri(), '/admin/designer/ontwerpen') === false
            && strpos($currentRequest->getRequestUri(), '/admin/designer/screenshot') === false
            && strpos($currentRequest->getRequestUri(), '/emails/voorbeeld/') === false
        ) {
            $package = new PathPackage('',
                new StaticVersionStrategy($this->container->getParameter('application_version')),
                $this->container->get('assets.context'));

            $this->container->get('assets.packages')->setDefaultPackage($package);
        }
    }
}