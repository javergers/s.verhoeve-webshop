<?php

namespace AdminBundle\EventListener;

use AppBundle\Entity\Common\Parameter\ParameterEntityFile;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\UnitOfWork;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\Filesystem;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class ParameterEntityFileSubscriber
 * @package AdminBundle\EventListener
 */
class ParameterEntityFileListener
{
    use ContainerAwareTrait;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * ParameterEntityFileListener constructor.
     * @param EntityManagerInterface $entityManager
     * @param Filesystem    $filesystem
     */
    public function __construct(EntityManagerInterface $entityManager, Filesystem $filesystem)
    {
        $this->entityManager = $entityManager;
        $this->filesystem = $filesystem;
    }

    /**
     * @param ParameterEntityFile $parameterEntityFile
     * @throws FileExistsException
     * @throws FileNotFoundException
     * @throws OptimisticLockException
     */
    public function postPersist(ParameterEntityFile $parameterEntityFile)
    {
        if (\is_object($parameterEntityFile)) {
            $this->update($parameterEntityFile);
        }
    }

    /**
     * @param ParameterEntityFile $parameterEntityFile
     * @throws FileExistsException
     * @throws FileNotFoundException
     * @throws OptimisticLockException
     */
    public function preUpdate(ParameterEntityFile $parameterEntityFile)
    {
        if (\is_object($parameterEntityFile)) {
            $this->update($parameterEntityFile);
        }
    }

    /**
     * @param ParameterEntityFile $parameterEntityFile
     * @throws FileExistsException
     * @throws FileNotFoundException
     * @throws OptimisticLockException
     */
    private function update(ParameterEntityFile $parameterEntityFile)
    {
        $moveToPath = str_replace('tmp/uploads', $parameterEntityFile->getStoragePath(),
            $parameterEntityFile->getPath());

        if (false === $this->filesystem->has($moveToPath) && null !== $parameterEntityFile->getPath()) {
            $this->filesystem->rename($parameterEntityFile->getPath(), $moveToPath);
        }

        $parameterEntityFile->setPath($moveToPath);
    }
}
