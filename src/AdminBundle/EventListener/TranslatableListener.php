<?php

namespace AdminBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\OptimisticLockException;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Knp\DoctrineBehaviors\Model\Translatable\Translation;

/**
 * Class TranslatableListener
 * @package AdminBundle\EventListener
 */
class TranslatableListener implements EventSubscriber
{
    /**
     * @var TimestampableEntity[]
     */
    private $timestampableEntities;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'prePersist',
            'preUpdate',
            'postFlush',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->entityManager = $args->getEntityManager();

        $this->updateTranslatable($args->getEntity());
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->entityManager = $args->getEntityManager();

        $this->updateTranslatable($args->getEntity());
    }

    /**
     * @param $entity
     */
    private function updateTranslatable($entity)
    {
        /**
         * @var $entity       Translation
         * @var $translatable TimestampableEntity
         */

        if (!\in_array(Translation::class, class_uses($entity), true)) {
            return;
        }

        $translatable = $entity->getTranslatable();

        if (!$translatable) {
            return;
        }

        if (!\in_array(TimestampableEntity::class, class_uses($translatable), true)) {
            return;
        }

        $this->timestampableEntities[] = $translatable;
    }

    /**
     * @param PostFlushEventArgs $args
     * @throws OptimisticLockException
     */
    public function postFlush(PostFlushEventArgs $args)
    {
        if ($this->timestampableEntities) {
            $em = $args->getEntityManager();

            foreach ($this->timestampableEntities as $timestampableEntity) {
                $timestampableEntity->setUpdatedAt(new \DateTime());
            }

            $this->timestampableEntities = [];

            $em->flush();
        }
    }
}
