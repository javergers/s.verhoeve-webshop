<?php


namespace AdminBundle\EventListener;

use Oneup\UploaderBundle\Event\PostUploadEvent;
use Oneup\UploaderBundle\OneupUploaderBundle;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class ParameterEntityFileUploadListener
 * @package AdminBundle\EventListener
 */
class ParameterEntityFileUploadListener
{
    /** @var string|null */
    private $kernelRootDir;

    /** @var null|string */
    private $orphanageDir;

    /** @var string */
    private $tmp_file_path;

    /** @var SessionInterface */
    private $session;

    /**
     * ParameterEntityFileUploadListener constructor.
     * @param null|string      $kernelRootDir
     * @param null|string      $orphanageDir
     * @param SessionInterface $session
     */
    public function __construct(string $kernelRootDir, string $orphanageDir, SessionInterface $session)
    {
        $this->kernelRootDir = $kernelRootDir;
        $this->orphanageDir = $orphanageDir;
        $this->session = $session;
    }

    /**
     * @param PostUploadEvent $event
     */
    public function onUpload(PostUploadEvent $event): void
    {
        $response = $event->getResponse();

        $webDir = $this->kernelRootDir . '/../web';
        $orphanageDir = $this->orphanageDir . $this->session->getId() . '/' . $event->getType() . '/';

        $response['thumbnail_path'] = str_replace($webDir, null, $event->getFile()->getPathname());
        $response['file_path'] = str_replace($orphanageDir, null, $event->getFile()->getPathname());

        $this->tmp_file_path = $response['file_path'];
    }
}