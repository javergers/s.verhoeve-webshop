<?php


namespace AdminBundle\EventListener;

use Oneup\UploaderBundle\Event\PostUploadEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class FileUploadListener
{
    private $kernelRootDir;
    private $tmp_file_path;

    public function __construct($kernelRootDir = null, SessionInterface $session)
    {
        $this->kernelRootDir = $kernelRootDir;
        $this->session = $session;
    }

    public function onUpload(PostUploadEvent $event)
    {
        $response = $event->getResponse();

        $webDir = $this->kernelRootDir . "/../web";
        $orphanageDir = $this->kernelRootDir . "/../web/uploader/orphanage/" . $this->session->getId() . "/" . $event->getType() . "/";

        $response['thumbnail_path'] = str_replace($webDir, null, $event->getFile()->getPathname());
        $response['file_path'] = str_replace($orphanageDir, null, $event->getFile()->getPathname());

        $this->tmp_file_path = $response['file_path'];
    }
}