<?php

namespace AdminBundle\EventListener;

use Doctrine\Common\EventArgs;
use Doctrine\Common\EventSubscriber;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class PublishableListener implements EventSubscriber
{
    private $tokenStorage;

    public function getSubscribedEvents(): array
    {
        // @todo: should this be disabled it's going to an empty class
        return [
            'loadClassMetadata',
        ];
    }

    /**
     * PublishableListener constructor.
     *
     * @param TokenStorage $tokenStorage
     */
    public function __construct(TokenStorage $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Return the Token Storage
     *
     * @return TokenStorage
     */
    public function getTokenStorage(): tokenStorage
    {
        return $this->tokenStorage;
    }

    /**
     * Maps additional metadata
     *
     * @param EventArgs $eventArgs
     *
     * @return void
     */
    public function loadClassMetadata(EventArgs $eventArgs)
    {
        void($eventArgs);
    }
}
