<?php

namespace AdminBundle\EventListener;

use AdminBundle\Entity\Settings\OrderPriorityRule;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderActivity;
use AppBundle\Services\Sales\Order\OrderActivityService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use RuleBundle\Service\ResolverService;

/**
 * Class OrderPriorityRuleListener
 * @package AdminBundle\EventListener
 */
class OrderPriorityRuleListener
{
    /** @var EntityManagerInterface $em */
    private $em;

    /** @var ResolverService $resolverService */
    private $resolverService;

    /** @var OrderActivity $orderActivity */
    private $orderActivityService;

    /**
     * OrderPriorityRuleListener constructor.
     * @param EntityManagerInterface        $em
     * @param ResolverService      $resolverService
     * @param OrderActivityService $orderActivityService
     */
    public function __construct(
        EntityManagerInterface $em,
        ResolverService $resolverService,
        OrderActivityService $orderActivityService
    ) {
        $this->em = $em;
        $this->resolverService = $resolverService;
        $this->orderActivityService = $orderActivityService;
    }

    /**
     * @param Order              $order
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */
    public function preUpdate(Order $order, LifecycleEventArgs $args)
    {
        $order->setPriority($this->calculatePriority($order, $args->getEntityManager()));
    }

    /**
     * @param Order              $order
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */
    public function prePersist(Order $order, LifecycleEventArgs $args)
    {
        $order->setPriority($this->calculatePriority($order, $args->getEntityManager()));
    }

    /**
     * @param Order         $order
     * @param EntityManagerInterface $em
     * @return int
     * @throws \Exception
     */
    private function calculatePriority(Order $order, EntityManagerInterface $em): int
    {
        $priority = 0;

        $orderPiorityRules = $em->getRepository(OrderPriorityRule::class)->findAll();

        foreach ($orderPiorityRules as $orderPiorityRule) {
            try {
                if ($this->resolverService->satisfies($order, $orderPiorityRule->getRule())) {
                    $priority += $orderPiorityRule->getPriority();
                }
            } catch (\RuntimeException $e) {
                $this->orderActivityService->add($e->getMessage(), $order);
                $this->em->flush();
            }
        }

        return min(max($priority, 0), 100);
    }
}
