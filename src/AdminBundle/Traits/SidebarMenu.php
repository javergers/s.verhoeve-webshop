<?php

namespace AdminBundle\Traits;

trait SidebarMenu
{
    public function hasSidebarMenu()
    {
        return $this->getOption("sidebar_menu");
    }

    public function getSidebarMenu()
    {
        return $this->getOption("sidebar_menu");
    }
}