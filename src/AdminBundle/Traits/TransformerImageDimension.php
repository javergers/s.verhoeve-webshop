<?php

namespace AdminBundle\Traits;

use AppBundle\Annotation\Type\TransformerParam;

/**
 * Trait TransformerImageDimension
 * @package AdminBundle\Traits
 */
trait TransformerImageDimension
{
    /**
     * @TransformerParam(label="Breedte", type="Symfony\Component\Form\Extension\Core\Type\IntegerType",
     *                                  required=false, sequence="1", attributes={"attr" = { "placeholder" = "Vul
     *                                    hier een getal in"}})
     *
     */
    private $width;

    /**
     * @TransformerParam(label="Hoogte", type="Symfony\Component\Form\Extension\Core\Type\IntegerType", required=false,
     *                                   sequence="2", attributes={"attr" = { "placeholder" = "Vul hier een getal
     *                                   in"}})
     *
     */
    private $height;
}
