<?php

namespace AdminBundle\Interfaces;

interface LocaleDetectorInterface
{
    /**
     * @return string
     */
    public function getLocale();
}