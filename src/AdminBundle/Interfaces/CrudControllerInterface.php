<?php

namespace AdminBundle\Interfaces;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

interface CrudControllerInterface
{
    public function getLabel();

    public function getOptions();
}