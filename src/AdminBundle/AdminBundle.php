<?php

namespace AdminBundle;

use A2lix\AutoFormBundle\A2lixAutoFormBundle;
use A2lix\TranslationFormBundle\A2lixTranslationFormBundle;
use Cnerta\BreadcrumbBundle\CnertaBreadcrumbBundle;
use FM\ElfinderBundle\FMElfinderBundle;
use Knp\Bundle\MenuBundle\KnpMenuBundle;
use Knp\Bundle\PaginatorBundle\KnpPaginatorBundle;
use Knp\Bundle\TimeBundle\KnpTimeBundle;
use Oneup\FlysystemBundle\OneupFlysystemBundle;
use Oneup\UploaderBundle\OneupUploaderBundle;
use Padam87\CronBundle\Padam87CronBundle;
use Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle;
use Symfony\Bundle\AsseticBundle\AsseticBundle;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class AdminBundle
 * @package AdminBundle
 */
class AdminBundle extends Bundle
{
    /**
     * @return array
     */
    public static function getBundles()
    {
        $bundles = [
            new AsseticBundle(),
            new Padam87CronBundle(),
            new FMElfinderBundle(),
            new OneupFlysystemBundle(),
            new OneupUploaderBundle(),
            new A2lixTranslationFormBundle(),
            new A2lixAutoFormBundle(),
            new CnertaBreadcrumbBundle(),
            new KnpMenuBundle(),
            new KnpTimeBundle(),
            new KnpPaginatorBundle(),
            new StofDoctrineExtensionsBundle(),
        ];

        return $bundles;
    }
}
