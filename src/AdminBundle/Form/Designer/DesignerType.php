<?php

namespace AdminBundle\Form\Designer;

use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Services\Designer;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DesignerType
 * @package AdminBundle\Form\Designer
 */
class DesignerType extends AbstractFormType
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var Designer
     */
    private $designer;

    /**
     * DesignerType constructor.
     * @param RequestStack $requestStack
     * @param Designer     $designer
     */
    public function __construct(RequestStack $requestStack, Designer $designer)
    {
        $this->requestStack = $requestStack;
        $this->designer = $designer;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'is_pre_design' => false,
            'save_callback' => null,
        ]);
    }

    /**
     * @param FormView      $view
     * @param FormInterface $form
     * @param array         $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $this->request = $this->requestStack->getMasterRequest();

        $this->designer->setUuid($view->vars['value']);
        $this->designer->setIsPreDesign($options['is_pre_design']);

        if (empty($view->vars['value'])) {
            $uuid = Uuid::uuid4()->toString();

            $this->designer->setUuid($uuid);
            $view->vars['value'] = $uuid;
        }

        $view->vars[''] = $this->designer;
        $view->vars['designerService'] = $this->designer;

        $view->vars['templateId'] = $this->request->get('id');

        $companyId = $this->request->query->get('company');

        $view->vars['company'] = null;
        if($companyId) {
            $view->vars['company'] = $companyId;
        }
    }

    /**
     * @return null|string
     */
    public function getParent()
    {
        return TextType::class;
    }

}
