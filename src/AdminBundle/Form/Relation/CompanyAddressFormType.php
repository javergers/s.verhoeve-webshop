<?php

namespace AdminBundle\Form\Relation;

use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Form\Type\AbstractEntityFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyAddressFormType
 * @package AdminBundle\Form\Relation
 */
class CompanyAddressFormType extends AbstractEntityFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('attn', TextType::class, [
                'required' => false,
                'label' => 'Contactpersoon',
            ])
            ->add('street', TextType::class, [
                'label' => 'Straat',
            ])
            ->add('number', TextType::class, [
                'label' => 'Huisnummer',
            ])
            ->add('numberAddition', TextType::class, [
                'label' => 'Huisnr. toevoeging',
                'required' => false
            ])
            ->add('postcode', TextType::class, [
                'label' => 'Postcode',
            ])
            ->add('city', TextType::class, [
                'label' => 'Stad',
            ])
            ->add('country', EntityType::class, [
                'label' => 'Land',
                'placeholder' => 'Kies een land...',
                'class' => Country::class,
                'select2' => [],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Address::class
        ]);
    }

}