<?php

namespace AdminBundle\Form\Relation;

use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Form\Type\AbstractEntityFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AddressFormType
 * @package AdminBundle\Form\Relation
 */
class AddressFormType extends AbstractEntityFormType
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * AddressFormType constructor.
     *
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('address_container', ContainerType::class, []);

        $column = $builder->create('address_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Adresgegevens',
        ]);

        $column
            ->add('description', TextType::class, [
                'required' => false,
                'label' => 'Omschrijving',
            ])
            ->add('company_name', TextType::class, [
                'label' => 'Bedrijfsnaam',
                'required' => false,
            ])
            ->add('attn', TextType::class, [
                'required' => false,
                'label' => 'Contactpersoon',
            ])
            ->add('street', TextType::class, [
                'label' => 'Straat',
            ])
            ->add('number', TextType::class, [
                'label' => 'Huisnummer',
            ])
            ->add('numberAddition', TextType::class, [
                'label' => 'Huisnummer toevoeging',
                'required' => false,
            ])
            ->add('postcode', TextType::class, [
                'label' => 'Postcode',
            ])
            ->add('city', TextType::class, [
                'label' => 'Stad',
            ])
            ->add('country', EntityType::class, [
                'label' => 'Land',
                'placeholder' => 'Kies een land...',
                'class' => Country::class,
                'select2' => [],
            ])
            ->add('default_delivery', CheckboxType::class, [
                'mapped' => false,
                'label' => 'Is standaard bezorgadres',
                'required' => false,
            ]);

        $container->add($column);
        $builder->add($container);

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            /** @var Address $address */
            $address = $event->getData();
            $entity = $address->getCompany() ?? $address->getCustomer();

            if(null !== $entity && null !== $entity->getDefaultDeliveryAddress() && $address->getId() === $entity->getDefaultDeliveryAddress()->getId()) {
                $event->getForm()->get('address_container')->get('address_column')->get('default_delivery')->setData(true);
            }
        });

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var Address $address */
            $address = $event->getData();
            $request = $this->requestStack->getMasterRequest();
            $entity = null;

            if ($request->get('entityClass') === 'company' || null !== $address->getCompany()) {
                $entity = $address->getCompany() ?? $this->getEntity(Company::class);

                $address->setCompany($entity);
            } elseif ($request->get('entityClass') === 'customer' || null !== $address->getCustomer()) {
                $entity = $address->getCustomer() ?? $this->getEntity(Customer::class);

                $address->setCustomer($entity);
            }

            $defaultDeliveryAddress = $event->getForm()->get('address_container')->get('address_column')->get('default_delivery')->getData();
            if ($defaultDeliveryAddress && null !== $entity) {
                $entity->setDefaultDeliveryAddress($address);
            }

            $address->setDeliveryAddress(true);
        });
    }

    /**
     * @param OptionsResolver $resolver
     * @return array
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }
}