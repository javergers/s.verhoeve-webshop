<?php

namespace AdminBundle\Form;

use AdminBundle\Form\Parameter\ParameterEntityFileType;
use AppBundle\Entity\Common\Parameter\ParameterEntity;
use AppBundle\Entity\Common\Parameter\ParameterEntityFile;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\TabType;
use AppBundle\Form\Type\YesNoType;
use AppBundle\Services\Parameter\ParameterService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;

/**
 * Class AddParameterFieldsSubscriber
 * @package AdminBundle\Form
 */
class AddParameterFieldsSubscriber implements EventSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var ParameterService
     */
    private $parameterService;

    /**
     * @var FormBuilderInterface
     */
    private $builder;

    /**
     * @var string|null
     */
    private $parameterFormType;

    /**
     * AddParameterFieldsSubscriber constructor.
     *
     * @param ContainerInterface   $container
     * @param string               $class
     * @param FormBuilderInterface $builder ;
     *
     * @param string               $parameterFormType
     * @throws \Exception
     */
    public function __construct(
        ContainerInterface $container,
        string $class = null,
        FormBuilderInterface $builder = null,
        string $parameterFormType = null
    ) {
        $this->container = $container;
        $this->parameterService = $this->container->get('app.parameter')->setEntity($class);

        if (null === $parameterFormType) {
            $parameterFormType = ColumnType::class;
        }

        $this->parameterFormType = $parameterFormType;

        $allowedTypes = [
            ContainerType::class,
            TabType::class,
        ];

        if (null !== $builder && !\in_array(\get_class($builder->getType()->getInnerType()), $allowedTypes, true)) {
            throw new \RuntimeException(sprintf('Builder must have an innertype of "%s"', implode(',', $allowedTypes)));
        }

        $this->builder = $builder;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            FormEvents::PRE_SET_DATA => 'onPreSetData',
            FormEvents::POST_SUBMIT => 'onPostSubmit',
        ];
    }

    /**
     * @param FormEvent $event
     *
     * @throws \Exception
     */
    public function onPreSetData(FormEvent $event): void
    {
        $data = $event->getData();
        $form = $event->getForm();

        if ($this->parameterService->getParameters()) {

            $id = null;
            if ($data) {
                $id = $data->getId();
            }

            $formField = null;
            if ($this->builder) {
                $formField = $this->findFormField($form, $this->builder->getName());
            }

            if (null !== $formField) {
                $formField->add('extra_parameters_column', $this->parameterFormType, []);
            }

            $parametersCollection = new ArrayCollection($this->parameterService->getParameters());
            $parameters = $parametersCollection->filter(function (ParameterEntity $parameterEntity) {
                return (!$parameterEntity->getParent());
            })->toArray();

            if ($formField) {
                $formBuilder = $formField->get('extra_parameters_column');
            } else {
                $formBuilder = $form;
            }

            $this->loopParameters($id, $formBuilder, $parameters);
        }
    }

    /**
     * @param               $id
     * @param FormInterface $form
     * @param array         $parameters
     * @throws \Exception
     */
    private function loopParameters($id, FormInterface $form, array $parameters): void
    {
        /** @var ParameterEntity[] $parameters */
        foreach ($parameters as $parameterEntity) {

            $parameter = $parameterEntity->getParameter();

            if(null === $parameter) {
                continue;
            }

            $paramKey = $parameter->getKey();
            $formKey = 'parameter_' . $paramKey;
            $formType = $parameter->getFormType();

            $fieldOptions = [
                'label' => $parameterEntity->getLabel(),
                'mapped' => false,
                'required' => $parameterEntity->getRequired(),
            ];

            $fieldValue = $this->parameterService->getValue($paramKey, $id);

            /** @var EntityManagerInterface $em */
            $em = $this->container->get('doctrine.orm.entity_manager');

            if ($formType === EntityType::class) {

                $relationClass = $parameter->getFormTypeClass();

                $fieldOptions['class'] = $relationClass;
                $fieldOptions['multiple'] = true;
                $fieldOptions['placeholder'] = false;
                $fieldOptions['select2'] = [
                    'placeholder' => 'Selecteer',
                ];

                if (null !== $fieldValue) {
                    $fieldValue = explode(',', $fieldValue);

                    $qb = $em->getRepository($relationClass)->createQueryBuilder('entity');

                    $qb->where('entity.id IN(:values)');
                    $qb->setParameter('values', $fieldValue);

                    $entityChoices = [];
                    $entities = $qb->getQuery()->getResult();

                    foreach ($entities as $entity) {
                        $entityChoices[$entity->getId()] = $entity;
                    }

                    $fieldOptions['data'] = new ArrayCollection($entityChoices);
                }

                $form->add($formKey, $formType, $fieldOptions);

            } elseif ($formType === ParameterEntityFileType::class) {

                if (null !== $fieldValue) {
                    $parameterEntityFile = $em->getRepository(ParameterEntityFile::class)->find((int)$fieldValue);
                    $fieldOptions['data'] = $parameterEntityFile ?: null;
                }

                $form->add($formKey, $formType, $fieldOptions);

            }  else {
                if(in_array($formType, [YesNoType::class, IntegerType::class], true)) {
                    $fieldOptions['data'] = (int) ($fieldValue ?? $parameterEntity->getDefaultValue());
                } else {
                    $defaultValue = ($parameterEntity->getDefaultValue() ?: '');
                    $fieldOptions['data'] = $fieldValue ?? $defaultValue;
                }

                $form->add($formKey, $formType, $fieldOptions);
            }

            $childParameters = $em->getRepository(ParameterEntity::class)->findBy([
                'parent' => $parameterEntity->getId(),
            ]);

            if ($childParameters) {
                $this->loopParameters($id, $form->get($formKey), $childParameters);
            }
        }
    }

    /**
     * @param FormEvent $event
     *
     * @throws \Exception
     */
    public function onPostSubmit(FormEvent $event): void
    {
        $data = $event->getData();
        $form = $event->getForm();

        if ($this->parameterService->getParameters() && $form->isSubmitted() && $form->isValid()) {
            $em = $this->container->get('doctrine')->getManager();

            if (!$data) {
                $data = null;
            } else {
                if (!$data->getId()) {
                    $em->persist($data);
                    $em->flush();
                }
            }

            foreach ($this->parameterService->getParameters() as $parameterEntity) {
                $formBuilder = $form;

                if ($this->builder) {
                    $formField = $this->findFormField($form, $this->builder->getName());
                    if (null !== $formField) {
                        $formBuilder = $formField->get('extra_parameters_column');
                    }
                }

                $key = $parameterEntity->getParameter()->getKey();
                $fieldKey = 'parameter_' . strtolower($key);

                $field = $this->findFormField($formBuilder, $fieldKey);

                if (null === $field) {
                    continue;
                }

                $type = $field->getConfig()->getType()->getInnerType();

                if (!($type instanceof ColumnType) && null !== $field->getData()) {
                    $fieldData = $field->getData();

                    $value = $fieldData;
                    $currentValue = $this->container->get('app.parameter')->setEntity($data)->getValue($key);
                    $defaultValue = ($parameterEntity->getDefaultValue() ?: '');

                    if ($type instanceof IntegerType) {
                        $value = (string)$value;
                    }

                    if ($type instanceof EntityType) {
                        if ($fieldData instanceof ArrayCollection) {
                            $ids = $fieldData->map(function ($entity) {
                                return $entity->getId();
                            })->toArray();
                            if ($ids) {
                                $value = implode(',', $ids);
                            }
                        } else {
                            $value = $fieldData->getId();
                        }
                    }

                    if ($type instanceof ParameterEntityFileType) {
                        if (null === $fieldData->getId()) {
                            $em->persist($fieldData);
                            $em->flush();
                        }

                        $value = $fieldData->getId();

                        if (null !== $fieldData->getId() && null === $fieldData->getPath()) {
                            $em->remove($fieldData);
                            $em->flush();
                            $value = null;
                        }
                    }

                    if ($value === $defaultValue && $currentValue === null) {
                        continue;
                    }

                    if (null !== $value) {
                        if(false === $value) {
                            $value = 0;
                        }
                        $this->container->get('app.parameter')
                            ->setEntity($data)
                            ->setValue($key, $value);
                    }
                }
            }
        }
    }

    /**
     * @param FormInterface $form
     * @param               $key
     * @return null|FormInterface
     */
    private function findFormField(FormInterface $form, $key): ?FormInterface
    {
        if ($form->has($key)) {
            return $form->get($key);
        }

        foreach ($form as $childForm) {
            $field = $this->findFormField($childForm, $key);
            if ($field) {
                return $field;
            }
        }
        return null;
    }
}
