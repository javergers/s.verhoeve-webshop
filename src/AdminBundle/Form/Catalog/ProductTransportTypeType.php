<?php

namespace AdminBundle\Form\Catalog;

use AppBundle\Entity\Catalog\Product\ProductTransportType;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductTransportTypeType
 * @package AdminBundle\Form\Catalog
 */
class ProductTransportTypeType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('transportType', EntityType::class, [
                'class' => TransportType::class,
                'required' => true,
                'label' => 'Transport type',
                'choice_label' => function (TransportType $delivery) {
                    $vatGroup = $delivery->getVatGroups()->filter(function(VatGroup $vatGroup) {
                        return $vatGroup->getCountry()->getId() === 'NL';
                    })->current();

                    if ($vatGroup) {
                        $vatPercentage = $vatGroup->getRateByDate()->getFactor() * 100;

                        if ($vatPercentage) {
                            $btwLabel = 'incl ' . $vatPercentage . '% BTW';
                            $priceInclusive = number_format($delivery->getPrice() / 100 * (100 + $vatPercentage), 2, ',', '.');
                        } else {
                            $btwLabel = 'vrijgesteld van BTW';
                            $priceInclusive = number_format($delivery->getPrice(), 2, ',', '.');
                        }

                        return $delivery->getInternalName() . ' á € ' . $priceInclusive . ' (' . $btwLabel . ')';
                    } else {
                        $priceExclusive = number_format($delivery->getPrice(), 2, ',', '.');
                        return $delivery->getInternalName() . ' á € ' . $priceExclusive . ' (excl. BTW)';
                    }
                },
            ])
            ->add('quantity', IntegerType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Geen maximum…',
                ],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductTransportType::class,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'product_transport_type';
    }
}
