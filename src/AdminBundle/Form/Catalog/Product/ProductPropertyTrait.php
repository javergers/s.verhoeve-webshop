<?php /** @noinspection SlowArrayOperationsInLoopInspection */

namespace AdminBundle\Form\Catalog\Product;

use AppBundle\DBAL\Types\ProductgroupPropertyTypeType;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductgroupProperty;
use AppBundle\Entity\Catalog\Product\ProductgroupPropertySet;
use AppBundle\Entity\Catalog\Product\ProductProperty;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\DateType;
use AppBundle\Form\Type\MoneyType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Trait ProductPropertyTrait
 */
trait ProductPropertyTrait
{
    /**
     * @param Product $formProduct
     * @param FormBuilderInterface $builder
     *
     * @param RouterInterface $router
     * @param EntityManagerInterface $entityManager
     *
     * @return mixed
     */
    protected function addProductProperties(
        ?Product $formProduct,
        FormBuilderInterface $builder,
        RouterInterface $router,
        EntityManagerInterface $entityManager
    ) {
        $columnProperties = $builder->create('column_properties', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
        ]);

        /** @var ProductgroupProperty $properties */
        $properties = $this->getProductVariationProperties($formProduct);

        foreach ($properties as $property) {
            $type = false;

            $attributes = [];
            /**
             * @var ProductgroupProperty $property
             */
            if ($property->getIsUnique()) {
                $attributes['attr'] = [
                    'data-unique' => true,
                    'data-unique-action' => $router->generate('admin_catalog_productgroupproperty_uniquevalue'),
                ];
            }

            switch ($property->getFormType()) {
                case ProductgroupPropertyTypeType::ENTITY:
                    $options = [
                        'label' => $property->translate()->getDescription(),
                        'class' => Product::class,
                        'placeholder' => in_array('read_only', $property->getFormTypeOptions(), true) ? false : '',
                        'choice_label' => function (Product $product) {
                            $value = '';
                            $value .= $product->getName();

                            $price = $product->getPrice();

                            if (!$price && $product->getParent() && $product->getParent()->getPrice()) {
                                $price = $product->getParent()->getPrice();
                            }

                            $value .= ' á € ' . number_format($price, 2, ',', '.');

                            return $value;
                        },
                        'choice_attr' => function ($product, $key, $value) use ($property, $formProduct) {

                            $attr = [];

                            if ($property->getKey() !== 'personalization') {
                                return $attr;
                            }

                            $productProperty = $formProduct->getProductProperty($property);

                            if (null === $productProperty || null === $productProperty->getProductVariation()) {
                                $selectedValue = null;
                            } else {
                                $selectedValue = (string)$productProperty->getProductVariation()->getId();
                            }

                            $disable = false;
                            if ($selectedValue !== $value
                                && \in_array('read_only', $property->getFormTypeOptions(), true)) {
                                $disable = true;
                            }

                            if ($disable) {
                                $attr['disabled'] = 'disabled';
                            }

                            return $attr;
                        },
                        'choices' => $property->getProduct()->getVariations(),
                        'mapped' => false,
                        'required' => in_array('required', $property->getFormTypeOptions(), true),
                    ];

                    if ($attributes) {
                        $options = array_merge($attributes, $options);
                    }

                    $columnProperties->add('property_' . $property->getId(), EntityType::class, $options);

                    break;

                case ProductgroupPropertyTypeType::CHOICE:
                    $choices = [];

                    foreach ($property->getOptions() as $option) {
                        $choices[$option->getId()] = $option->getValue();
                    }

                    $options = [
                        'label' => $property->translate()->getDescription(),
                        'placeholder' => '',
                        'choices' => array_flip($choices),
                        'mapped' => false,
                        'required' => \in_array('required', $property->getFormTypeOptions(), true),
                    ];

                    if ($attributes) {
                        $options = array_merge($attributes, $options);
                    }

                    $columnProperties->add('property_' . $property->getId(), ChoiceType::class, $options);
                    break;

                case ProductgroupPropertyTypeType::TEXTAREA:
                    $type = TextareaType::class;
                    break;
                case ProductgroupPropertyTypeType::DATE:
                    $type = DateType::class;
                    break;
                case ProductgroupPropertyTypeType::PRICE:
                    $type = MoneyType::class;
                    break;
                default:
                    $type = TextType::class;
                    break;
            }

            if ($type) {
                $options = [
                    'label' => $property->translate()->getDescription(),
                    'mapped' => false,
                    'required' => \in_array('required', $property->getFormTypeOptions(), true),
                ];

                if ($attributes) {
                    $options = array_merge($attributes, $options);
                }

                $columnProperties->add('property_' . $property->getId(), $type, $options);
            }
        }

        $builder->addEventListener(FormEvents::POST_SET_DATA,
            function (FormEvent $event) use ($properties) {
                /**
                 * @var Product $product
                 */
                $product = $event->getData();
                if ($product->getProductProperties()) {
                    foreach ($product->getProductProperties() as $productProperty) {
                        /**
                         * @var ProductProperty $productProperty
                         */

                        if ($productProperty->getValue() && array_key_exists($productProperty->getProductgroupProperty()->getId(),
                                $properties)) {
                            $propertyField = $this->getFormElement($event->getForm(), 'property_' . $productProperty->getProductgroupProperty()->getId());
                            $propertyField->setData($productProperty->getValue());
                        }
                    }
                }
            });

        $builder->addEventListener(FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($properties, $entityManager) {
                $variation = $event->getData();
                $propertyColumn = $this->getFormElement($event->getForm(), 'column_properties');

                foreach ($propertyColumn->getIterator() as $child) {
                    /**
                     * @var Form $child
                     */
                    $propertyId = str_replace('property_', null, $child->getName());

                    $property = $entityManager->getRepository(ProductProperty::class)->findOneBy([
                        'product' => $variation->getId(),
                        'productgroupProperty' => $propertyId,
                    ]);

                    if (!$property) {
                        $property = new ProductProperty();
                        $property->setProduct($variation);
                        $property->setProductgroupProperty($properties[$propertyId]);
                        $entityManager->persist($property);
                        $entityManager->flush();

                        $variation->addProductProperty($property);
                    }

                    $property->setValue($child->getData());
                }

                $entityManager->flush();
            });

        return $columnProperties;
    }

    /**
     * Get product properties for current product.
     *
     * @param Product $product
     *
     * @return array
     */
    private function getProductVariationProperties(?Product $product)
    {
        $properties = [];

        if(null === $product) {
            return $properties;
        }

        /**
         * @var ProductgroupPropertySet $propertySet
         */
        $propertySet = $product->getPropertySet();

        // If product has no specific property set use parent
        if (null === $propertySet && $product->getParent()) {
            $propertySet = $product->getParent()->getPropertySet();
        }

        if (null !== $propertySet) {
            foreach ($propertySet->getProductgroupProperties() as $productgroupProperty) {
                $properties[$productgroupProperty->getId()] = $productgroupProperty;
            }
        }

        return $properties;
    }

    /**
     * @param FormInterface $form
     * @param string $name
     * @param int $depth
     *
     * @return FormInterface|null
     */
    public function getFormElement(FormInterface $form, string $name, $depth = 0): ?FormInterface
    {
        foreach ($form as $element) {
            if ($element->getName() === $name) {
                return $element;
            }

            if ($element->getConfig()->getOption('inherit_data')) {
                $childElement = $this->getFormElement($element, $name, $depth + 1);

                if ($childElement) {
                    return $childElement;
                }
            }
        }

        return null;
    }
}