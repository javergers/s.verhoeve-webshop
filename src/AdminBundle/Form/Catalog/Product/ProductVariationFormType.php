<?php

namespace AdminBundle\Form\Catalog\Product;

use AppBundle\Exceptions\ImageUploadException;
use AdminBundle\Form\Catalog\ProductImageType;
use AdminBundle\Form\Catalog\ProductTransportTypeType;
use AdminBundle\Form\Catalog\ProductUspType;
use AdminBundle\Form\Catalog\SupplierGroupProductType;
use AdminBundle\Form\Catalog\SupplierProductType;
use AdminBundle\Form\Catalog\Type\ProductImageCollectionType;
use AdminBundle\Form\Catalog\UpsellCollectionType;
use AdminBundle\Form\Catalog\UpsellType;
use AdminBundle\Form\Product\ProductCombinationType;
use AdminBundle\Form\Product\ProductPackagingFormType;
use AppBundle\Entity\Catalog\Product\GenericProduct;
use AppBundle\Form\Type\DesignSelectType;
use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Form\Common\EntityHiddenTransformer;
use AppBundle\Form\Type\YesNoType as YesNoChoiceType;
use AppBundle\DBAL\Types\YesNoType;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductgroupPropertySet;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\BooleanType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\MoneyType;
use AppBundle\Form\Type\TranslationsType;
use AppBundle\Model\Product\ProductModel;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Oneup\UploaderBundle\Uploader\Storage\FilesystemOrphanageStorage;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductVariationFormType
 * @package AdminBundle\Form\Catalog\Product
 */
class ProductVariationFormType extends AbstractFormType
{
    use ProductPropertyTrait;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ContainerInterface $container
     */
    protected $container;

    /**
     * @var Product|null $product
     */
    private $product;

    /**
     * @var array $translatableProperties
     */
    protected $translatableProperties = [
        'title',
        'description',
        'shortDescription',
        'slug',
        'metaTitle',
        'imagePath',
        'metaDescription',
        'metaRobotsIndex',
        'metaRobotsFollow',
    ];

    /**
     * @var FormBuilderInterface $builder
     */
    private $builder;

    /**
     * ProductVariationFormType constructor.
     * @param EntityManagerInterface      $entityManager
     * @param ContainerInterface $container
     */
    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
        $this->product = $this->getProduct();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if($this->product == null){
            throw new \Exception('Open new varation via product link');
        }
        $this->builder = $builder;

        $this->builder->add('parent', HiddenType::class, [
            'empty_data' => $this->product,
        ]);

        $this->builder->get('parent')->addModelTransformer(new EntityHiddenTransformer(
            $this->entityManager,
            Product::class,
            'id'
        ));

        $this
            ->createProductCombinationContainer()
            ->createPersonalizationDesignContainer()
            ->createProductPriceInformationContainer()
            ->createProductImagesContainer()
            ->createContentContainer()
            ->createVariationUspContainer()
            ->createPublishableContainer()
            ->createWarehousingContainer()
            ->createTransportTypesContainer()
            ->createPackagingUnitsContainer()
            ->createWicsContainer()
            ->createUpsellContainer()
        ;

        $this->builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

            /** @var GenericProduct $product */
            $product = $event->getData();
            $form = $event->getForm();

            if (null === $product->getId()) {
                $column = $form
                    ->get('container_product_variations_properties_usp')
                    ->get('column_product_properties');

                $column->add('propertySet', EntityType::class, [
                    'class' => ProductgroupPropertySet::class,
                    'label' => 'Kenmerkenset',
                    'required' => true,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('ps')
                            ->leftJoin('ps.productgroup', 'pg')
                            ->andWhere('ps.productgroup = :productgroup')
                            ->setParameter('productgroup', ($this->product)? $this->product->getProductgroup(): null);
                    },
                    'data' => ($this->product)? $this->product->getPropertySet(): null,
                ]);
            }
        });

        $this->builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var GenericProduct $product */
            $product = $event->getData();

            if($event->getForm()->has('container_bbe')) {
                $bbeChoice = $event->getForm()->get('container_bbe')->get('column_bbe')->get('bbeChoice');

                if (!$bbeChoice->getData() && $product->getExpirationMeta()) {
                    $product->setExpirationMeta(null);
                }
            }

            if($event->getForm()->has('container_serial')) {
                $serialChoice = $event->getForm()->get('container_serial')->get('column_serial')->get('serialnumberChoice');

                if (!$serialChoice->getData() && $product->getSerialNumberMeta()) {
                    $product->setSerialNumberMeta(null);
                }
            }
        });

        $this->builder->addEventSubscriber(new PublishCompanyProductSubscriber());
    }

    /**
     * @return $this
     */
    public function createProductPriceInformationContainer(): self
    {
        $informationContainer = $this->builder->create('column_information_container', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $informationColumn = $this->builder->create('column_prices', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Algemeen',
        ]);

        $informationColumn
            ->add('name', TextType::class, [
                'label' => 'Interne naam',
                'help_label' => 'Niet zichtbaar voor klanten',
                'required' => true,
            ])
            ->add('ean', NumberType::class, [
                'label' => 'EAN',
                'required' => false,
            ])
            ->add('sku', TextType::class, [
                'label' => 'SKU',
                'required' => false,
                'attr' => [
                    'readonly' => 'readonly',
                ],
            ])
            ->add('customergroups', EntityType::class, [
                'label' => 'Beschikbaar voor',
                'placeholder' => 'Alle klanten',
                'class' => CustomerGroup::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('customer_group')
                        ->orderBy('customer_group.description', 'ASC');
                },
                'required' => true,
                'multiple' => true,
                'multiselect' => [
                    'nonSelectedText' => 'Maak een keuze',
                ],
            ])
            ->add('default_variation', CheckboxType::class, [
                'mapped' => false,
                'label' => 'Is standaard variatie',
                'help_block' => 'De standaard variatie wordt op de product pagina automatisch geselecteerd.',
                'required' => false,
            ]);


        $informationContainer->add($informationColumn);
        $this->builder->add($informationContainer);

        //Add price container
        $priceContainer = $this->builder->create('column_price_container', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $priceColumn = $this->builder->create('column_prices', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Prijzen',
        ]);

        $column1 = $this->builder->create('price_1', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => false,
        ]);

        $column1->add('price', MoneyType::class, [
            'label' => 'Verkoopprijs',
            'required' => true
        ]);

        $column1->add('purchase_price', MoneyType::class, [
            'label' => 'Inkoopprijs',
            'required' => false,
        ]);

        $priceColumn->add($column1);

        $column2 = $this->builder->create('price_2', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => false,
        ]);

        $column2->add('minprice', MoneyType::class, [
            'label' => 'Min. prijs',
            'required' => false,
        ]);

        $column2->add('maxprice', MoneyType::class, [
            'label' => 'Max. prijs',
            'required' => false,
            'attr' => [
                'placeholder' => 'Geen',
            ],
        ]);

        $column2->add('step_min_max_price', MoneyType::class, [
            'label' => 'Stap',
            'required' => false,
            'attr' => [
                'placeholder' => 'Geen',
            ],
        ]);

        $column2->add('vatGroups', EntityType::class, [
            'label' => 'BTW Groepen',
            'class' => VatGroup::class,
            'choice_label' => function(VatGroup $vatGroup) {
                return $vatGroup->getLevel()->getTitle().' ('.$vatGroup->getCountry()->translate()->getName().')';
            },
            'multiselect' => [],
            'multiple' => true,
        ]);

        $priceColumn->add($column2);

        $priceContainer->add($priceColumn);

        $this->builder->add($priceContainer);

        return $this;
    }

    /**
     * @return $this
     */
    public function createProductCombinationContainer()
    {
        /** @var ProductModel $data */
        $product = $this->getFormEntity();

        if (!$product->isCombination() && $product->getParent()) {
            return $this;
        }

        $container = $this->builder->create('product_combination_container', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-12',
            ],
        ]);

        $column = $this->builder->create('product_combination_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Combinatie producten',
        ]);

        $column->add('product_combination', ProductCombinationType::class, [
            'label' => false,
            'mapped' => false,
        ]);

        $container->add($column);
        $this->builder->add($container);

        return $this;
    }

    /**
     * @return $this
     */
    public function createPersonalizationDesignContainer()
    {
        /** @var ProductModel $data */
        $product = $this->getFormEntity();

        if (!$product->hasPersonalization()) {
            return $this;
        }

        $container = $this->builder->create('personalization_design_container', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $column = $this->builder->create('personalization_design_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Ontwerp',
        ]);

        $column->add('metadata', DesignSelectType::class, [
            'required' => false
        ]);

        $container->add($column);
        $this->builder->add($container);

        return $this;
    }

    /**
     * @return $this
     */
    public function createProductImagesContainer(): self
    {
        $imageContainer = $this->builder->create('column_image_container', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $imageColumn = $this->builder->create('column_images', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Afbeeldingen',
        ]);

        $this->builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {

            /** @var FilesystemOrphanageStorage $manager */
            $manager = $this->container->get('oneup_uploader.orphanage_manager')->get('product_image');

            // get files
            $files = $manager->getFiles();
            $totalFiles = \count($files);

            // Check if we got uploaded files
            if (!empty($files) && $totalFiles >= 1) {

                // upload all files to the configured storage
                $uploadedFiles = $manager->uploadFiles();

                if ($totalFiles !== \count($uploadedFiles)) {

                    $event->getForm()->addError(new FormError('Image couldn\'t be uploaded'));
                    throw new ImageUploadException();
                }
            }
        });

        $columns = [
            [
                'label' => 'Positie',
                'width' => '150px',
            ],
            [
                'label' => 'Afbeelding',
                'width' => '100px',
            ],
            [
                'label' => 'Main',
                'width' => '50px',
            ],
            [
                'label' => 'Assortiment',
                'width' => '90px',
            ],
            [
                'label' => 'Omschrijving',
                'width' => '150px',
            ],
        ];

        $imageColumn->add('productImages', ProductImageCollectionType::class, [
            'entry_type' => ProductImageType::class,
            'label' => 'Afbeelding',
            'by_reference' => false,
            'collection' => [
                'columns' => $columns,
                'sortable' => true,
                'hide_add' => true,
            ],
        ]);

        $imageContainer->add($imageColumn);

        $this->builder->add($imageContainer);

        return $this;
    }

    /**
     * @return $this
     */
    public function createContentContainer(): self
    {
        $generalSettingsContainer = $this->builder->create('translations', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $columnGeneralSettings = $this->builder->create('column_general_settings', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Content',
        ]);

        $columnGeneralSettings->add('translations', TranslationsType::class,
            $this->getA2lixTranslatableFieldsOptions([
                'title' => [
                    'label' => 'Titel',
                    'attr' => [
                        'class' => 'slugifyTitle',
                    ],
                ],
            ])
        );

        $generalSettingsContainer->add($columnGeneralSettings);

        $this->builder->add($generalSettingsContainer);

        return $this;
    }

    /**
     * @return $this
     */
    public function createVariationUspContainer(): self
    {
        $container = $this->builder->create('container_product_variations_properties_usp', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $columnProductProperties = $this->builder->create('column_product_properties', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Kenmerken',
        ]);

        $columnProductProperties->add($this->addProductProperties($this->product, $this->builder, $this->container->get('router'), $this->entityManager, $columnProductProperties));

        $container->add($columnProductProperties);

        $uspContainer = $this->builder->create('container_usp', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $columnUsp = $this->builder->create('column_product_usp', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => "Usp's",
        ]);

        $columnUsp->add('usps', CollectionType::class, [
            'entry_type' => ProductUspType::class,
            'label' => 'USP',
            'by_reference' => false,
            'collection' => [
                'sortable' => true,
                'columns' => [
                    [
                        'label' => 'Positie *',
                        'width' => '80px',
                    ],
                    [
                        'label' => 'USP *',
                        'width' => '300px',
                    ],
                ],
            ],
        ]);

        $container->add($columnProductProperties);
        $uspContainer->add($columnUsp);

        $this->builder->add($container);
        $this->builder->add($uspContainer);

        return $this;
    }

    /**
     * @return $this
     */
    public function createWicsContainer(): self
    {
        $bbeContainer = $this->builder->create('container_bbe', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $bbeColumn = $this->builder->create('column_bbe', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Houdbaarheidsdatum instellingen',
        ]);

        $bbeColumn->add('bbeChoice', YesNoChoiceType::class, [
            'mapped' => false,
            'label' => 'Houdbaarheid gebruiken',
            'attr' => [
                'id' => 'bbeChoice',
            ],
        ]);

        $bbeColumn->add('expirationMeta', ExpirationMetaFormType::class, [
            'required' => false,
            'label' => false,
        ]);

        $bbeContainer->add($bbeColumn);

        $serialContainer = $this->builder->create('container_serial', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $serialColumn = $this->builder->create('column_serial', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Serienummer instellingen',
        ]);

        $serialColumn->add('serialnumberChoice', YesNoChoiceType::class, [
            'mapped' => false,
            'label' => 'Serienummer gebruiken',
            'help_label' => 'Alleen voor cadeaubonnen',
            'attr' => [
                'id' => 'serialnumberChoice',
            ],
        ]);

        $serialColumn->add('serialNumberMeta', SerialNumberMetaFormType::class, [
            'required' => false,
            'label' => false,
        ]);

        $serialContainer->add($serialColumn);

        $this->builder->add($bbeContainer);
        $this->builder->add($serialContainer);

        return $this;
    }

    /**
     * @return $this
     */
    public function createWarehousingContainer(): self
    {
        $container = $this->builder->create('container_product_stock', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $column = $this->builder->create('stock', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Voorraad',
        ]);

        $column
            ->add('has_stock', BooleanType::class, [
                'label' => 'Gebruik voorraadbeheer',
                'choices' => YesNoType::getChoices(),
            ])->add('noStockBackorder', BooleanType::class, [
                'label' => 'Bestelbaar bij geen voorraad',
                'choices' => YesNoType::getChoices(),
            ])
            ->add('noStockQuotation', BooleanType::class, [
                'label' => 'Offerte bij geen voorraad',
                'choices' => YesNoType::getChoices(),
            ]);

        $container->add($column);

        $column = $this->builder->create('column2', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => ' ',
            'icon' => ' ',
        ]);

        $column
            ->add('manufacturer', TextType::class, [
                'label' => 'Fabrikant',
                'required' => false,
            ]);

        $container->add($column);


        $quantityContainer = $this->builder->create('container_product_quantity', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $column = $this->builder->create('column_product_quantity', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Besteleenheden',
        ]);

        $column->add('minQuantity', IntegerType::class, [
            'label' => 'Minimale besteleenheid',
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $column->add('maxQuantity', IntegerType::class, [
            'label' => 'Maximale besteleenheid',
            'required' => false,
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $column->add('defaultQuantity', IntegerType::class, [
            'label' => 'Standaard besteleenheid',
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $column->add('quantitySteps', TextType::class, [
            'label' => 'Besteleenheid stapgrootte',
            'widget_horizontal_label_class' => 'col-sm-6 col-md-6 col-lg-6',
            'widget_form_element_class' => 'col-sm-6 col-md-6 col-lg-6',
        ]);

        $quantityContainer->add($column);

        $this->builder->add($container);
        $this->builder->add($quantityContainer);

        return $this;
    }

    /**
     * @return $this
     */
    private function createPublishableContainer(): self
    {
        $container = $this->builder->create('container_publishable', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $column = $this->builder->create('column_publishable', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
        ]);

        $this->builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            /** @var Product $product */
            $product = $event->getData();

            if (null !== $product->getMetadata() && isset($product->getMetadata()['default_variation'])) {
                $event->getForm()->get('column_information_container')->get('column_prices')->get('default_variation')->setData($product->getMetadata()['default_variation']);
            }
        });

        $this->builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $isDefaultVariation = $event->getForm()->get('column_information_container')->get('column_prices')->get('default_variation')->getData();

            if ($isDefaultVariation) {
                /** @var Product $variation */
                $variation = $event->getData();

                $product = $variation->getParent();
                foreach ($product->getVariations() as $var) {
                    $metadata = $var->getMetadata();
                    $metadata['default_variation'] = ($var->getId() == $variation->getId());
                    $var->setMetadata($metadata);
                }
            }
        });

        parent::addPublishableFields($column, $this->builder, []);

        $container->add($column);

        $this->builder->add($container);

        return $this;
    }

    /**
     * @return $this
     */
    public function createTransportTypesContainer(): self
    {
        $container = $this->builder->create('container_transporttypes', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $column = $this->builder->create('column_transporttypes', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Bezorging',
        ]);

        $column->add('productTransportTypes', CollectionType::class, [
            'entry_type' => ProductTransportTypeType::class,
            'label' => 'Bezorgkostenstaffels',
            'by_reference' => false,
            'collection' => [
                'columns' => [
                    [
                        'label' => 'Transport methode *',
                        'width' => '150px',
                    ],
                    [
                        'label' => 'Max. aantal items',
                        'width' => '150px',
                    ],
                    [],
                ],
            ],
        ]);

        $container->add($column);

        $this->builder->add($container);

        return $this;
    }

    /**
     * @return $this
     */
    public function createPackagingUnitsContainer(): self
    {
        $container = $this->builder->create('container_packagingunits', ContainerType::class, [
            'attr' => [
                'class' => 'container-md-6',
            ],
        ]);

        $column = $this->builder->create('column_packagingunits', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Formaat en gewicht',
        ]);

        $column->add('productPackagingUnits', CollectionType::class, [
            'entry_type' => ProductPackagingFormType::class,
            'by_reference' => false,
            'collection' => [
                'columns' => [
                    [
                        'label' => 'Verpakkingseenheid',
                    ],
                    [
                        'label' => 'Hoeveelheid',
                    ],
                    [
                        'label' => 'Lengte (mm)',
                    ],
                    [
                        'label' => 'Breedte (mm)',
                    ],
                    [
                        'label' => 'Hoogte (mm)',
                    ],
                    [
                        'label' => 'Gewicht (gram)',
                    ],
                ],
            ],
        ]);

        $container->add($column);

        $this->builder->add($container);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSuppliers()
    {
        $container = $this->builder->create('container_supplier', ContainerType::class,
            []);

        $column = $this->builder->create('column_product_supplier_group', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
        ]);

        $column->add('supplierGroupProducts', CollectionType::class, [
            'entry_type' => SupplierGroupProductType::class,
            'label' => "Leveranciersgroepen",
            'by_reference' => false,
            'collection' => [
                'columns' => [
                    [
                        'label' => 'Positie *',
                        'width' => '150px',
                    ],
                    [
                        'label' => 'Doorgeef titel *',
                        'width' => '200px',
                    ],
                    [
                        'label' => 'Doorgeef omschrijving *',
                        'width' => '200px',
                    ],
                    [
                        'label' => 'Doorgeefprijs *',
                        'width' => '200px',
                    ],
                    [
                        'label' => 'Leveranciersgroep *',
                        'width' => '150px',
                    ],
                    [
                        'label' => 'Artikelnr. leverancier *',
                        'width' => '150px',
                    ],
                    [
                        'label' => 'Afhalen mogelijk *',
                        'width' => '150px',
                    ],
                ],
                'sortable' => true,
            ],
        ]);

        $container->add($column);

        $column = $this->builder->create('column_product_supplier', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
        ]);

        $column->add('supplierProducts', CollectionType::class, [
            'entry_type' => SupplierProductType::class,
            'label' => 'Leveranciers',
            'by_reference' => false,
            'collection' => [
                'columns' => [
                    [
                        'label' => 'Positie *',
                        'width' => '150px',
                    ],
                    [
                        'label' => 'Doorgeef titel *',
                        'width' => '200px',
                    ],
                    [
                        'label' => 'Doorgeef omschrijving *',
                        'width' => '200px',
                    ],
                    [
                        'label' => 'Doorgeefprijs *',
                        'width' => '200px',
                    ],
                    [
                        'label' => 'Leverancier *',
                        'width' => '150px',
                    ],
                    [
                        'label' => 'Artikelnr. leverancier *',
                        'width' => '150px',
                    ],
                    [
                        'label' => 'Afhalen mogelijk *',
                        'width' => '150px',
                    ],
                ],
                'sortable' => true,
            ],
        ]);

        $container->add($column);

        return $container;
    }

    /**
     * @param FormBuilderInterface $builder
     */
    public function createUpsellContainer()
    {
        $container = $this->builder->create('upsell_container', ContainerType::class, [
            'attr' => [
                'style' => 'margin: 10px; width: 100%;',
            ],
        ]);

        $column = $this->builder->create('upsell_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Upsell producten'
        ]);

        $column->add('upsells', UpsellCollectionType::class, [
            'entry_type' => UpsellType::class,
            'by_reference' => false,
            'prototype' => false,
        ]);

        $container->add($column);
        $this->builder->add($container);
    }

    /**
     * @return Product|null
     */
    private function getProduct(): ?Product
    {
        $masterRequest = $this->container->get('request_stack')->getMasterRequest();

        if (null !== $masterRequest && null !== $masterRequest->get('entity')) {
            $match = $masterRequest->get('entity');
        } else {
            preg_match('/\d+/', $_SERVER['REQUEST_URI'], $match);

            if (!$match) {
                return null;
            }

            $match = $match[0];
        }

        return $this->entityManager->getRepository(Product::class)->find($match);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
            'validation_groups' => [
                Product::class,
                'determineValidationGroups',
            ],
        ]);
    }

    /**
     * @return ProductModel|null
     */
    private function getFormEntity() {
        return $this->builder->getData();
    }
}
