<?php

namespace AdminBundle\Form\Catalog\Product;

use AppBundle\Entity\Carrier\Carrier;
use AppBundle\Entity\Carrier\DeliveryMethod;
use AppBundle\Entity\Catalog\Product\TransportType;
use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Productgroup;
use AppBundle\Form\Extension\TabbedFormTypeExtension;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\MoneyType;
use AppBundle\Form\Type\TabType;
use AppBundle\Form\Type\TranslationType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TransportTypeType
 * @package AdminBundle\Form\Catalog\Product
 */
class TransportTypeType extends AbstractFormType
{
    use ContainerAwareTrait;

    /**
     * @var EntityManagerInterface $em
     */
    protected $em;

    /**
     * @var FormBuilderInterface $builder
     */
    private $builder;

    /**
     * @var array
     */
    private $options = [];

    /**
     * @var array $translatableProperties
     */
    protected $translatableProperties = [
        'title',
    ];

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $this->builder = $builder;
        $this->options = $options;

        $container = $builder->create('translations', ContainerType::class, []);
        $container->add($this->getTabGeneral());

        $builder->add($container);

//        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
//            $event->getData()->setPru('simple');
//        });
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabGeneral(): FormBuilderInterface
    {
        $tab = $this->builder->create('tab_general', TabType::class, [
            'label' => 'Algemeen',
            'alignment' => TabbedFormTypeExtension::ALIGNMENT_VERTICAL,
        ]);

        $tab->add($this->getTabGeneralContainerGeneral());
        $tab->add($this->getTabGeneralContainerFinance());
        $tab->add($this->getTabGeneralContainerContent());
        $tab->add($this->getTabGeneralContainerTransport());

        return $tab;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabGeneralContainerGeneral(): FormBuilderInterface
    {
        $container = $this->builder->create('container_publishable', ContainerType::class, []);

        $column1 = $this->builder->create('container_details_column1', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => 'Algemeen',
        ]);

        $column1
            ->add('productgroup', EntityType::class, [
                'placeholder' => '',
                'label' => 'Productgroep',
                'class' => Productgroup::class,
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('p')
                        ->select('p, COALESCE(pp.title, p.title) as HIDDEN columnOrder')
                        ->leftJoin('p.parent', 'pp', 'p.parent = pp.id')
                        ->addOrderBy('columnOrder', 'ASC')
                        ->addOrderBy('p.parent', 'ASC')
                        ->addOrderBy('p.title', 'ASC');

                    return $qb;
                },
                'choice_label' => function (Productgroup $productgroup) {
                    $label = '';

                    if ($productgroup->getParent()) {
                        $label .= ' - ';
                    }

                    return $label . (string)$productgroup;
                },
            ]);

        $container->add($column1);

        $column2 = $this->builder->create('container_details_column2', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => ' ',
            'icon' => ' ',
        ]);

        $container->add($column2);

        $column3 = $this->builder->create('container_details_column3', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => ' ',
            'icon' => ' ',
        ]);

        $column3->add('customergroups', EntityType::class, [
            'label' => 'Beschikbaar voor',
            'placeholder' => 'Alle klanten',
            'class' => CustomerGroup::class,
            'required' => true,
            'multiple' => true,
            'multiselect' => [
                'nonSelectedText' => 'Maak een keuze',
            ],
        ]);

        $container->add($column3);

        return $container;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabGeneralContainerFinance(): FormBuilderInterface
    {
        $container = $this->builder->create('product_container_finance', ContainerType::class);

        $column1 = $this->builder->create('price_1', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => 'Financieël',
        ]);

        $column1->add('price', MoneyType::class, [
            'label' => 'Verkoopprijs',
        ]);

        $container->add($column1);

        $column2 = $this->builder->create('price_2', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => ' ',
            'icon' => ' ',
        ]);

        $column2->add('purchase_price', MoneyType::class, [
            'label' => 'Inkoopprijs',
            'required' => false,
        ]);

        $container->add($column2);

        $column3 = $this->builder->create('price_3', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => ' ',
            'icon' => ' ',
        ]);

        $column3->add('vatGroups', EntityType::class, [
            'label' => 'BTW Groepen',
            'class' => VatGroup::class,
            'choice_label' => function (VatGroup $vatGroup) {
                return $vatGroup->getLevel()->getTitle() . ' (' . $vatGroup->getCountry()->translate()->getName() . ')';
            },
            'multiselect' => [],
            'multiple' => true,
        ]);

        $container->add($column3);

        return $container;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabGeneralContainerContent(): FormBuilderInterface
    {
        $container = $this->builder->create('product_container_content', ContainerType::class);

        $column1 = $this->builder->create('product_container_content_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Content',
        ]);

        $column1
            ->add('name', TextType::class, [
                'label' => 'Interne naam',
            ])
            ->add('title', TranslationType::class, [
                'label' => 'Titel',
            ])
            ->add('shortDescription', TranslationType::class, [
                'label' => 'Omschrijving',
            ]);

        $container->add($column1);

        return $container;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabGeneralContainerTransport(): FormBuilderInterface
    {
        $container = $this->builder->create('product_container_transport', ContainerType::class);

        $column = $this->builder->create('product_container_transport_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Transport',
        ]);

        $column->add('preferredCarrier', EntityType::class, [
            'label' => 'Voorkeurs transporteur',
            'placeholder' => 'Geen voorkeur',
            'class' => Carrier::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('carr')
                    ->orderBy('carr.shortName', 'ASC');
            },
            'required' => false,
            'multiple' => false,
        ]);

        $column->add('preferredDeliveryMethod', EntityType::class, [
            'label' => 'Voorkeurs leveringswijze',
            'placeholder' => 'Geen voorkeur',
            'class' => DeliveryMethod::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('dlme')
                    ->orderBy('dlme.name', 'ASC');
            },
            'required' => false,
            'multiple' => false,
        ]);

        $container->add($column);

        return $container;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TransportType::class,
            'translatable_class' => Product::class,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'product';
    }
}
