<?php

namespace AdminBundle\Form\Catalog\Product;

use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Catalog\Product\Product;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class PublishCompanyProductSubscriber
 * @package AdminBundle\Form\Product
 */
class PublishCompanyProductSubscriber implements EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FormEvents::POST_SUBMIT => 'onPostSubmit',
        ];
    }

    /**
     * @param FormEvent $event
     *
     * @throws \Exception
     */
    public function onPostSubmit(FormEvent $event): void
    {
        /** @var Product $product */
        $product = $event->getData();
        /** @var CompanyProduct $companyProduct */
        foreach ($product->getCompanyProducts() as $companyProduct) {
            if(false !== $companyProduct->getPublish()) {
                $companyProduct->setPublish($product->getPublish() ? true : null);
            } else {
                $companyProduct->setPublish(false);
            }
        }
    }
}