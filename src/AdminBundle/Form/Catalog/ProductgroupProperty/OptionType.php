<?php

namespace AdminBundle\Form\Catalog\ProductgroupProperty;

use AppBundle\Entity\Catalog\Product\ProductgroupPropertyOption;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OptionType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('position', IntegerType::class, [
                'attr' => [
                    'class' => 'text-right',
                ],
            ])
            ->add('value', TextType::class, []);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductgroupPropertyOption::class,
        ]);
    }
}
