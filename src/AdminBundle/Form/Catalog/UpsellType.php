<?php

namespace AdminBundle\Form\Catalog;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\Upsell;
use AppBundle\Form\Type\AbstractFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UpsellType
 * @package AdminBundle\Form\Catalog
 */
class UpsellType extends AbstractFormType
{
    private $entityManager;

    /**
     * AssortmentProductType constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder->add('position', IntegerType::class, [
            'attr' => [
                'class' => 'text-right',
                'readonly' => true,
            ],
        ]);

        $builder->add('upsellProduct', IntegerType::class, [
            'attr' => [
                'readonly' => true,
            ],
        ]);

        $builder->get('upsellProduct')->addModelTransformer(new CallbackTransformer(
            function ($entity) {
                if (null === $entity) {
                    return null;
                }

                // transform the array to a string
                return $entity->getId();
            },
            function ($id) {
                $product = $this->entityManager
                    ->getRepository(Product::class)
                    // query for the issue with this id
                    ->find($id);

                if (null === $product) {
                    throw new TransformationFailedException(sprintf(
                        'A product with id "%d" does not exist!',
                        $id
                    ));
                }

                return $product;
            }
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Upsell::class,
        ]);
    }
}
