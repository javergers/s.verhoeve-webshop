<?php

namespace AdminBundle\Form\Catalog;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Catalog\Product\ProductBulkPrice;
use AppBundle\Form\Type\AbstractFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductBulkPriceType extends AbstractFormType
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $product = $this->getProduct();

        $builder
            ->add('quantity', IntegerType::class, [
                'label' => 'Aantal',
            ])
            ->add('customergroups', EntityType::class, [
                'label' => 'Klantgroep',
                'placeholder' => "Alle klanten",
                'class' => "AppBundle\Entity\Security\Customer\CustomerGroup",
                'required' => false,
                'multiple' => true,
                'multiselect' => [
                    'nonSelectedText' => 'Alle klanten',
                ],
            ])
            ->add('productVariations', EntityType::class, [
                'label' => 'Variaties',
                'placeholder' => "Alle variaties",
                'class' => 'AppBundle\Entity\Catalog\Product\Product',
                'choice_label' => function ($product) {
                    $value = "";
                    $value .= $product->getName();

                    $price = $product->getPrice();

                    if (!$price && $product->getParent() && $product->getParent() && $product->getParent()->getPrice()) {
                        $price = $product->getParent()->getPrice();
                    }

                    $value .= ' á € ' . number_format($price, 2, ",", ".");

                    return $value;
                },
                'choices' => (($product) ? $product->getVariations() : []),
                'required' => false,
                'multiple' => true,
                'multiselect' => [
                    'nonSelectedText' => 'Alle variaties',
                ],
            ]);

        $builder
            ->add('price', MoneyType::class, [
                'label' => 'Minimum prijs',
                'attr' => [
                    'style' => 'max-width: 200px;',
                ],
            ]);
    }

    private function getProduct()
    {
        preg_match("/[0-9]+/", $_SERVER["REQUEST_URI"], $match);

        if (!$match) {
            return '';
        }

        return $this->entityManager->getRepository(Product::class)->find($match[0]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductBulkPrice::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'product_bulk_price';
    }
}
