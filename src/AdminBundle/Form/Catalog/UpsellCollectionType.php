<?php

namespace AdminBundle\Form\Catalog;

use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UpsellCollectionType
 * @package AdminBundle\Form\Catalog\Type
 */
class UpsellCollectionType extends AbstractFormType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allow_add' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $prototype_name = $this->buildName('name', $this->getNameStructure($form));
        $prototype_id = $this->buildName('id', $this->getNameStructure($form));
        $prototype_base = $this->buildName('base', $this->getNameStructure($form));

        $view->vars['prototype_name'] = $prototype_name;
        $view->vars['prototype_id'] = $prototype_id;
        $view->vars['prototype_base'] = $prototype_base;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'upsell_collection';
    }

    /**
     * {@inherticdoc}
     */
    public function getParent(): string
    {
        return CollectionType::class;
    }

    /**
     * @param       $form
     * @param array $name
     * @return array
     */
    private function getNameStructure(FormInterface $form, $name = []): array
    {
        $name[] = $form->getName();

        if ($form->getParent()) {
            return $this->getNameStructure($form->getParent(), $name);
        }

        return $name;
    }

    /**
     * @param string $type
     * @param        $nameStructure
     * @return bool|string
     */
    private function buildName($type, $nameStructure)
    {
        $structure = array_reverse($nameStructure);

        $base = array_shift($structure);

        if ($type === 'name') {
            return $base . '[' . implode('][', $structure) . '][__index__]';
        }

        if ($type === 'id') {
            return implode('_', array_reverse($nameStructure)) . '___index__';
        }

        if ($type === 'base') {
            return $base . '[' . implode('][', $structure) . ']';
        }

        return false;
    }
}
