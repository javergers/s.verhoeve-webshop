<?php

namespace AdminBundle\Form\Catalog\Type;

use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AssortmentProductCollectionType extends AbstractFormType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'allow_add' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $prototype_name = $this->buildName('name', $this->getNameStructure($form));
        $prototype_id = $this->buildName('id', $this->getNameStructure($form));
        $prototype_base = $this->buildName('base', $this->getNameStructure($form));

        $view->vars['prototype_name'] = $prototype_name;
        $view->vars['prototype_id'] = $prototype_id;
        $view->vars['prototype_base'] = $prototype_base;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'assortment_product_collection';
    }

    /**
     * {@inherticdoc}
     */
    public function getParent()
    {
        return CollectionType::class;
    }

    private function getNameStructure($form, $name = [])
    {
        $name[] = $form->getName();

        if ($form->getParent()) {
            return $this->getNameStructure($form->getParent(), $name);
        } else {
            return $name;
        }
    }

    private function buildName($type = 'name', $nameStructure)
    {
        $structure = array_reverse($nameStructure);

        $base = array_shift($structure);

        if ($type == 'name') {
            return $base . '[' . implode('][', $structure) . '][__index__]';
        } else {
            if ($type == 'id') {
                return implode("_", array_reverse($nameStructure)) . '___index__';
            } else {
                if ($type == 'base') {
                    return $base . '[' . implode('][', $structure) . ']';
                }
            }
        }

        return false;
    }
}
