<?php

namespace AdminBundle\Form\Catalog;

use AppBundle\Entity\Catalog\Product\ProductCountry;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductCountryType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('country', EntityType::class, [
                'label' => 'Land',
                'class' => 'AppBundle\Entity\Geography\Country',
                'choice_label' => function ($country) {
                    return (string)$country->translate()->getName();
                },
            ])
            ->add('price', MoneyType::class, [
                'label' => 'Prijs',
            ])
            ->add('min_price', MoneyType::class, [
                'label' => 'Minimum prijs',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Geen',
                ],
            ])
            ->add('max_price', MoneyType::class, [
                'label' => 'Maximum prijs',
                'required' => false,
                'attr' => [
                    'placeholder' => 'Geen',
                ],
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductCountry::class,
        ]);
    }


}
