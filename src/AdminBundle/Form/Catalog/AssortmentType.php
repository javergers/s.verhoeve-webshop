<?php

namespace AdminBundle\Form\Catalog;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AdminBundle\Form\Catalog\Type\AssortmentProductCollectionType;
use AdminBundle\Form\Type\MediaType;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Site\Banner;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Site\PageTranslation;
use AppBundle\Entity\Site\Site;
use AppBundle\Entity\Site\SiteTranslation;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\SlugType;
use AppBundle\Form\Type\TabType;
use AppBundle\Form\Type\YesNoType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AssortmentType
 * @package AdminBundle\Form\Catalog
 */
class AssortmentType extends AbstractFormType
{

    /**
     * @var FormBuilderInterface $builder
     */
    private $builder;

    /**
     * @var array $options
     */
    private $options;

    /**
     * @var EntityManagerInterface $entityManager
     */
    private $entityManager;

    /**
     * AssortmentType constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->builder = $builder;
        $this->options = $options;

        $container = $builder->create('container', ContainerType::class);

        $container->add('name', TextType::class, [
            'label' => 'Interne naam',
            'help_label' => 'Niet zichtbaar voor klanten',
            'required' => true,
        ]);

        $container->add('sites', EntityType::class, [
            'label' => 'Sites',
            'class' => Site::class,
            'choice_label' => function (Site $site) {
                return $site->translate('nl_NL')->getDescription();
            },
            'query_builder' => function (EntityRepository $er) {
                $qb = $er->createQueryBuilder('s')
                    ->select('s')
                    ->leftJoin(SiteTranslation::class, 'st', 'WITH', 's.id = st.translatable AND st.locale = :locale')
                    ->setParameter('locale', 'nl_NL')
                    ->addOrderBy('st.description', 'ASC');

                return $qb;
            },
            'required' => true,
            'multiple' => true,
            'multiselect' => [
                'nonSelectedText' => 'Maak een keuze',
            ],
        ]);

        $container->add('assortmentType', EntityType::class, [
            'label' => 'Assortiment bestaat uit',
            'class' => 'AppBundle\Entity\Catalog\Assortment\AssortmentType',
            'choice_label' => 'name',
            'required' => false,
            'multiple' => false,
            'placeholder' => 'Standaard producten',
        ]);

        $container->add('banner', EntityType::class, [
            'label' => 'Banner',
            'class' => Banner::class,
            'placeholder' => 'Kies een banner',
            'required' => false,
            'query_builder' => function (EntityRepository $er) {
                $qb = $er->createQueryBuilder('b')
                    ->select('b')
                    ->addOrderBy('b.name', 'ASC');

                return $qb;
            },
        ]);

        $container->add('image', MediaType::class, [
            'label' => 'Afbeelding',
            'instance' => 'form',
            'enable' => true,
            'required' => false,
            'media' => [
                'type' => MediaType::TYPE_IMAGE,
                'thumbnail' => true,
                'upload' => true,
                'dimensions' => [
                    'min' => 'assortment_homepage_block',
                ],
            ],
        ]);

        $container->add('content', EntityType::class, [
            'label' => 'Content',
            'class' => Page::class,
            'placeholder' => 'Kies content',
            'required' => false,
            'query_builder' => function (EntityRepository $er) {
                $qb = $er->createQueryBuilder('p')
                    ->select('p')
                    ->leftJoin(PageTranslation::class, 'pt', 'WITH', 'p.id = pt.translatable AND pt.locale = :locale')
                    ->setParameter('locale', 'nl_NL')
                    ->addOrderBy('pt.title', 'ASC');

                return $qb;
            },
            'choice_label' => function (Page $page) {
                return $page->translate()->getTitle();
            },
            'group_by' => function(Page $page, $key, $index) {
                return $page->getSite()->translate()->getDescription();
            }
        ]);

        $container->add('contentSize', TextType::class, [
            'label' => 'Grootte van content',
            'help_label' => 'bijv. 1x2 (hoogte x breedte)',
        ]);

        $container->add('contentPosition', ChoiceType::class, [
            'label' => 'Content positie',
            'choices' => $this->getChoices(),
            'placeholder' => 'Kies een positie voor de content',
            'required' => false,
        ]);

        $container->add('showFilter', CheckboxType::class, [
            'label' => 'Filter inschakelen',
            'required' => false,
        ]);


        $builder->add($container);

        $builder->add('translations', TranslationsType::class, [
            'label' => false,
            'fields' => [
                'title' => [
                    'label' => 'Titel',
                    'attr' => [
                        'class' => 'slugifyTitle',
                    ],
                ],
                'homepageSlogan' => [
                    'label' => 'Homepage slogan',
                ],
                'slug' => [
                    'field_type' => SlugType::class,
                    'base_on' => '.slugifyTitle',
                ],
            ],
        ]);

        $builder->add($this->tabs($builder));
        $builder->add($this->getTabPublishable());
        $builder->add($this->getTabAccessibility());
    }

    /**
     * @param FormBuilderInterface $builder
     * @return FormBuilderInterface
     */
    private function tabs(FormBuilderInterface $builder)
    {

        $tab_container = $builder->create('tab_container', ContainerType::class);

        $tab2 = $tab_container->create('tab_2', TabType::class, [
            'label' => 'Producten',
            'icon' => 'stack4',
        ]);

        $tab_container->add('assortmentProducts', AssortmentProductCollectionType::class, [
            'entry_type' => AssortmentProductType::class,
            'label' => "Product",
            'by_reference' => false,
            'prototype' => false,
        ]);

        $tab_container->add($tab2);

        return $tab_container;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabPublishable()
    {
        $tab = $this->builder->create('tab_publishable', TabType::class, [
            'label' => 'Publicatie',
        ]);

        $container = $this->builder->create('container_publishable', ContainerType::class, []);

        parent::addPublishableFields($container, $this->builder, $this->options);

        $tab->add($container);

        return $tab;
    }

    /**
     * @return FormBuilderInterface
     */
    private function getTabAccessibility()
    {
        $tab = $this->builder->create('tab_accessible', TabType::class, [
            'label' => 'Toegang',
        ]);


        $container = $this->builder->create('container_accessibility', ContainerType::class);

        $loginRequiredOptions = [
            'label' => 'Zichtbaar voor',
            'choices' => [
                'Iedereen' => 0,
                'Ingelogde gebruikers' => 1,
            ],
            'expanded' => true,
            'multiple' => false,
        ];

        if ($this->builder->getData() && $this->builder->getData()->getLoginRequired() !== true) {
            $loginRequiredOptions['data'] = 0;
        }

        $container->add('loginRequired', ChoiceType::class, $loginRequiredOptions);

        $container->add('accessibleCustomerGroups', EntityType::class, [
            'label' => 'Klantgroep(en)',
            'class' => CustomerGroup::class,
            'multiple' => true,
            'required' => false,
            'multiselect' => [],
            'query_builder' => function (EntityRepository $er) {
                $qb = $er->createQueryBuilder('cg')
                    ->select('cg')
                    ->addOrderBy('cg.description', 'ASC');

                return $qb;
            },
        ]);

        $tab->add($container);

        return $tab;
    }

    /**
     * @return array
     */
    private function getChoices()
    {
        $choices = [];
        for ($i = 0; $i < 30; $i++) {
            $choices[$i + 1] = $i;
        }

        return $choices;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Assortment::class,
        ]);
    }
}
