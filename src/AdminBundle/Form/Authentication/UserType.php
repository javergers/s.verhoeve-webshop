<?php

namespace AdminBundle\Form\Authentication;

use AdminBundle\Services\Role\RoleManagerService;
use AppBundle\Entity\Security\Employee\Group;
use AppBundle\Entity\Security\Employee\User;
use AppBundle\Form\MaskType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class UserType
 *
 * @package AdminBundle\Form\Authentication
 */
class UserType extends AbstractType
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var RoleManagerService */
    private $roleManager;

    /** @var bool */
    private $currentUserRemainsSuperAdmin = false;

    /**
     * UserType constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage, RoleManagerService $roleManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->roleManager = $roleManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $container = $builder->create('container_user', ContainerType::class, []);

        $container
            ->add('email', TextType::class, [
                'label' => 'E-mailadres',
            ])
            ->add('firstname', TextType::class, [
                'label' => 'Voornaam',
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Achternaam',
            ])
            ->add('username', TextType::class, [
                'label' => 'Gebruikersnaam',
            ])
            ->add('voipExtension', NumberType::class, [
                'label' => 'Voip extensie',
                'required' => false,
            ])
            ->add('plain_password', MaskType::class, [
                'label' => 'Wachtwoord (alleen wijzigen)',
                'required' => false,
            ]);

        if ($this->tokenStorage->getToken()->getUser()->isSuperAdmin() && ($builder->getData()->getId() != $this->tokenStorage->getToken()->getUser()->getId())) {
            $container->add('super_admin', CheckboxType::class, [
                'label' => 'Super admin privileges',
                'mapped' => false,
                'required' => false,
                'data' => $builder->getData()->hasRole('ROLE_SUPER_ADMIN'),
            ]);
        }

        $container
            ->add('enabled', CheckboxType::class, [
                'label' => 'Ingeschakeld',
                'required' => false,
            ]);

        $builder->add($container);

        $container = $builder->create('container_user_groups', ContainerType::class, []);

        $container->add('groups', EntityType::class, [
            'label' => 'Groepen',
            'class' => Group::class,
            'choice_label' => function ($site) {
                return $site->getName();
            },
            'required' => false,
            'multiple' => true,
            'expanded' => true,
            'attr' => [
                'id' => 'user_groups',
            ],
            'by_reference' => false,
        ]);

        $builder->add($container);

        $builder->add($container);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            if ($this->tokenStorage->getToken()->getUser()->isSuperAdmin() && $event->getForm()->getData()->getId() == $this->tokenStorage->getToken()->getUser()->getId()) {
                $this->currentUserRemainsSuperAdmin = true;
            }
        });

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $roleSuperAdmin = $this->roleManager->findRoleByName('ROLE_SUPER_ADMIN');
            $roleAdmin = $this->roleManager->findRoleByName('ROLE_ADMIN');

            /** @var User $user */
            $user = $event->getData();
            $user->addRole($roleAdmin);

            if ($this->tokenStorage->getToken()->getUser()->isSuperAdmin() && $user->getId() != $this->tokenStorage->getToken()->getUser()->getId()) {
                if ($event->getForm()->get('container_user')->get('super_admin')->getData()) {
                    $user->addRole($roleSuperAdmin);
                } else {
                    $user->removeRole($roleSuperAdmin);
                }
            } else {
                if ($this->currentUserRemainsSuperAdmin) {
                    $user->addRole($roleSuperAdmin);
                }
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'translation_domain' => false,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'user';
    }
}
