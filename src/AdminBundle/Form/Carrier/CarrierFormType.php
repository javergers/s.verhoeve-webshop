<?php

namespace AdminBundle\Form\Carrier;

use AdminBundle\Form\AddParameterFieldsSubscriber;
use AppBundle\Entity\Carrier\Carrier;
use AppBundle\Entity\Carrier\DeliveryMethod;
use AppBundle\Entity\Relation\Company;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CarrierFormType
 * @package AdminBundle\Form\Carrier
 */
class CarrierFormType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * CustomerType constructor.
     * @param EntityManagerInterface $entityManager
     * @param ContainerInterface     $container
     */
    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $container = $builder->create('carrier_container', ContainerType::class, []);

        $column = $builder->create('carrier_general_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Transporteur',
            'icon' => 'truck',
        ]);

        $column->add('shortName', TextType::class, [
            'label' => 'Naam',
            'required' => true,
        ]);

        $column->add('company', EntityType::class, [
            'label' => 'Bedrijf',
            'placeholder' => 'Selecteer een bedrijf',
            'class' => Company::class,
            'query_builder' => function (EntityRepository $er) use ($builder) {
                $qb = $er->createQueryBuilder('comp');
                $qb->leftJoin('comp.carrier', 'carr');
                $qb->where('carr.id IS NULL');
                if ($builder->getData() !== null) {
                    $qb->orWhere('carr.id = :carrierId');
                    $qb->setParameter('carrierId', $builder->getData()->getId());
                }
                $qb->orderBy('comp.name', 'ASC');
                return $qb;
            },
            'required' => true,
            'multiple' => false,
        ]);

        $column->add('deliveryMethods', EntityType::class, [
            'label' => 'Leveringswijzen',
            'placeholder' => '',
            'class' => DeliveryMethod::class,
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('dlme')
                    ->orderBy('dlme.name', 'ASC');
            },
            'required' => false,
            'multiple' => true,
            'multiselect' => [
                'nonSelectedText' => 'Maak een keuze',
            ],
        ]);

        $container->add($column);
        $builder->add($container);

        //Add parameter container
        $parameterContainer = $builder->create('carrier_parameter_container', ContainerType::class, [
            'label' => 'Geavanceerde instellingen',
        ]);
        $builder->addEventSubscriber(new AddParameterFieldsSubscriber($this->container, Carrier::class,
            $parameterContainer, ColumnType::class));
        $builder->add($parameterContainer);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Carrier::class,
        ]);
    }

}
