<?php

namespace AdminBundle\Form\Fraud;

use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use RuleBundle\Entity\Rule;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class FraudDetectionRuleFormType
 * @package AdminBundle\Form\Fraud
 */
class FraudDetectionRuleFormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('container_fraud_detection_rule', ContainerType::class, []);

        $column = $builder->create('column_fraud_detection_rule', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Fraude detectie',
            'icon' => 'warning',
        ]);

        $column->add('rule', EntityType::class, [
            'class' => Rule::class,
            'label' => 'Regel',
            'placeholder' => 'Selecteer een regel',
            'choice_label' => 'description',
            'required' => true,
            'select2' => [],
        ]);

        $column->add('score', NumberType::class, [
            'label' => 'Score',
            'required' => true
        ]);

        $column->add('publish', CheckboxType::class, [
            'label' => 'Gepubliceerd',
            'required' => false,
        ]);

        $container->add($column);

        $builder->add($container);
    }

}