<?php

namespace AdminBundle\Form\Assortment;

use AppBundle\Exceptions\ImageUploadException;
use AdminBundle\Form\Catalog\Type\ProductImageCollectionType;
use AppBundle\Entity\Catalog\Assortment\AssortmentProduct;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\MoneyType;
use Oneup\UploaderBundle\Uploader\Storage\FilesystemOrphanageStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class EditAssortmentProductFormType
 *
 * @package AdminBundle\Form\Assortment
 */
class EditAssortmentProductFormType extends AbstractFormType
{
    protected $propertyAccessor;

    /**
     * EditAssortmentProductFormType constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->propertyAccessor = new PropertyAccessor(true);
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'label' => 'Titel',
            'mapped' => false,
        ]);


        $builder->add('price', MoneyType::class, [
            'label' => 'Verkoopprijs',
            'mapped' => false,
        ]);

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $product = $this->getProduct();

            $form = $event->getForm()->all();

            foreach ($form as $field) {
                if ($product->hasDiff($field->getName())) {
                    $field->setData($this->propertyAccessor->getValue($product, $field->getName()));
                }

            }
        });

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {

            /** @var FilesystemOrphanageStorage $manager */
            $manager = $this->container->get('oneup_uploader.orphanage_manager')->get('product_image');

            // get files
            $files = $manager->getFiles();
            $totalFiles = count($files);

            // Check if we got uploaded files
            if (!empty($files) && $totalFiles >= 1) {

                // upload all files to the configured storage
                $uploadedFiles = $manager->uploadFiles();

                if ($totalFiles !== count($uploadedFiles)) {

                    $event->getForm()->addError(new FormError('Image couldn\'t be uploaded'));
                    throw new ImageUploadException();
                }
            };
        });

        $columns = [
            [
                'label' => 'Positie',
                'width' => '150px',
            ],
            [
                'label' => 'Afbeelding',
                'width' => '100px',
            ],
            [
                'label' => 'Main',
                'width' => '50px',
            ],
            [
                'label' => 'Assortiment',
                'width' => '90px',
            ],
            [
                'label' => 'Omschrijving',
                'width' => '150px',
            ],
        ];

        $builder->add('assortmentProductImages', ProductImageCollectionType::class, [
            'entry_type' => AssortmentProductImageType::class,
            'label' => 'Afbeelding',
            'by_reference' => false,
            'collection' => [
                'columns' => $columns,
                'sortable' => true,
                'hide_add' => true,
            ],
        ]);
    }

    /**
     * @return null|object
     */
    private function getProduct()
    {
        preg_match("/[0-9]+/", $_SERVER["REQUEST_URI"], $match);

        if (!$match) {
            return null;
        }

        $match = $match[0];
        $product = $this->container->get('doctrine')->getRepository(AssortmentProduct::class)->find($match);

        return $product;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AssortmentProduct::class,
        ]);
    }

}
