<?php

namespace AdminBundle\Form\Site;

use AdminBundle\Form\Type\MediaType;
use AppBundle\Entity\Site\SiteLogo;
use AppBundle\Form\Type\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SiteLogoFormType
 * @package AdminBundle\Form\Site
 */
class SiteLogoFormType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('path', ImageType::class, [
                'label' => false,
                'instance' => 'form',
                'enable' => true,
                'media' => [
                    'type' => MediaType::TYPE_IMAGE,
                    'thumbnail' => true,
                    'upload' => true,
                    'dimensions' => [
                        'min' => 'company-logo',
                    ],
                ],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SiteLogo::class,
        ]);
    }

}
