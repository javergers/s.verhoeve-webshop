<?php

namespace AdminBundle\Form\Extension;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CollectionExtension extends AbstractTypeExtension
{
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $originalValues = new ArrayCollection();

        if (false === $options['preventListeners']) {
            $builder->addEventListener(FormEvents::POST_SET_DATA,
                function (FormEvent $event) use ($builder, $originalValues) {
                    $data = $event->getData();

                    if ($data) {
                        foreach ($data as $value) {
                            $originalValues->add($value);
                        }
                    }
                });

            $builder->addEventListener(FormEvents::POST_SUBMIT,
                function (FormEvent $event) use ($builder, $originalValues) {
                    $data = $event->getData();

                    $values = new ArrayCollection();

                    foreach ($data as $value) {
                        $values->add($value);
                    }

                    foreach ($originalValues as $value) {
                        if (false === $values->contains($value)) {
                            $this->em->remove($value);
                        }
                    }
                });
        }
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {

        if (!isset($options['collection'])) {
            $options['collection'] = [];
        }

        $collectionResolver = new OptionsResolver();

        $collectionResolver->setDefined([
            'columns',
            'view',
            'datatable',
            'sortable',
            'preventListeners',
            'list_headings',
            'list_headings_combined',
            'actions_position'
        ]);

        if (isset($options['collection']['columns'])) {
            $collectionResolver->setAllowedTypes('columns', ['array', 'bool']);
            $collectionResolver->setAllowedTypes('list_headings', ['array', 'bool']);
            $collectionResolver->setAllowedTypes('list_headings_combined', ['bool']);
        }

        $collectionResolver->setDefaults([
            'view' => true,
            'datatable' => true,
            'sortable' => false,
            'columns' => false,
            'hide_add' => false,
            'list_headings' => false,
            'list_headings_combined' => false,
            'actions_position' => ['before'],
        ]);

        $options['collection'] = $collectionResolver->resolve($options['collection']);

        $view->vars['collection'] = $options['collection'];
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined([
            'collection',
            'preventListeners'
        ]);
        $resolver->setAllowedTypes('collection', ['array']);

        $resolver->setDefaults([
            'widget_form_group' => false,
            'delete_empty' => true,
            'allow_add' => true,
            'allow_delete' => true,
            'preventListeners' => false
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return CollectionType::class;
    }
}
