<?php

namespace AdminBundle\Form\Extension;

use AppBundle\Services\SiteService;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;

/**
 * Class FormTypeExtension
 * @package AdminBundle\Form\Extension
 */
class FormTypeExtension extends AbstractTypeExtension
{
    /**
     * @var SiteService
     */
    private $siteService;

    /**
     * FormTypeExtension constructor.
     * @param SiteService $siteService
     */
    public function __construct(SiteService $siteService)
    {
        $this->siteService = $siteService;
    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return FormType::class;
    }

    /**
     * {@inheritdoc}
     *
     */

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setAttribute('static_text', $options['static_text']);


    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined([
            'static_text',
            'security',
        ]);

        $defaultOptions = [
            'static_text' => false,
            'security' => null,
        ];
        if($this->siteService->determineSite() === null){
            $defaultOptions['translation_domain'] = false;
        }

        $resolver->setDefaults($defaultOptions);

        $resolver->setAllowedTypes('static_text', 'boolean');
        $resolver->setAllowedTypes('security', ['null', 'array']);

        $resolver->setNormalizer('security', function (Options $options, $value) {
            void($options);

            if (!empty($value['roles']) && empty($value['strategy'])) {
                $value['strategy'] = AccessDecisionManager::STRATEGY_AFFIRMATIVE;
            }

            return $value;
        });
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['static_text'] = $options['static_text'];
    }
}
