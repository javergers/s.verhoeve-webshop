<?php

namespace AdminBundle\Form\Extension;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class Select2ChoiceExtension extends AbstractSelect2Extension
{
    /**
     * {@inheritdoc}
     */
    public function getExtendedType()
    {
        return ChoiceType::class;
    }
}