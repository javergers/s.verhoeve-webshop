<?php

namespace AdminBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractSelect2Extension extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {

        if (isset($options['select2'])) {
            $select2Resolver = new OptionsResolver();

            $select2Resolver->setDefined([
                'placeholder',
                'tags',
                'allowClear',
                'ajax',
                'cache',
                'multiple',
                'language',
                'minimumInputLength',
                'maximumInputLength',
                'noSearch',
                'closeOnSelect',
            ]);

            $select2Defaults = [
                'language' => 'nl',
            ];

            if (isset($options['select2']['ajax'])) {
                $select2Defaults['minimumInputLength'] = 3;
            }

            $select2Resolver->setDefaults($select2Defaults);

            if (isset($options['select2']['ajax'])) {
                $select2Resolver->setRequired('findBy');
                $select2Resolver->setAllowedTypes('findBy', ['string', 'array']);
            }

            $options['select2'] = $select2Resolver->resolve($options['select2']);

            if (isset($options['select2']['findBy'])) {
                unset($options['select2']['findBy']);
            }

            foreach ($options['select2'] as $key => $values) {
                if (is_array($values)) {
                    foreach ($values as $attr => $value) {
                        $attrDashed = $this->camelCaseToDash($attr);

                        $options['select2'][$key . '--' . $attrDashed] = $value;

                        if ($attrDashed != $attr) {
                            unset($options['select2'][$attr]);
                        }
                    }

                    unset($options['select2'][$key]);
                } else {
                    $keyDashed = $this->camelCaseToDash($key);

                    $options['select2'][$keyDashed] = $values;

                    if ($keyDashed != $key) {
                        unset($options['select2'][$key]);
                    }
                }
            }

            $view->vars['select2'] = $options['select2'];
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined('select2');
        $resolver->setAllowedTypes('select2', ['array']);
    }

    private function camelCaseToDash($string)
    {
        return strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $string));
    }
}
