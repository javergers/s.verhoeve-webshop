<?php

namespace AdminBundle\Form\Extension;

use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractMultiSelectExtension extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {

        if (isset($options['multiselect'])) {
            $multiSelectResolver = new OptionsResolver();

            $multiSelectResolver->setDefined([
                'includeSelectAllOption',
                'enableFiltering',
                'nonSelectedText',
                'selectAllText',
                'allSelectedText',
                'filterPlaceholder',
            ]);

            $multiSelectResolver->setDefaults([
                'nSelectedText' => ' optie(s) geselecteerd',
            ]);

            $options['multiselect'] = $multiSelectResolver->resolve($options['multiselect']);

            foreach ($options['multiselect'] as $key => $value) {
                $keyDashed = $this->camelCaseToDash($key);

                $options['multiselect'][$keyDashed] = $value;

                if ($keyDashed != $key) {
                    unset($options['multiselect'][$key]);
                }
            }

            $view->vars['multiselect'] = $options['multiselect'];
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined('multiselect');
        $resolver->setAllowedTypes('multiselect', ['array']);
    }

    protected function camelCaseToDash($string)
    {
        return strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $string));
    }
}
