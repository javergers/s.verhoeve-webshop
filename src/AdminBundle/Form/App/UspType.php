<?php

namespace AdminBundle\Form\App;

use AppBundle\Entity\Site\Usp;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\TranslationsType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UspType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder->add('translations', TranslationsType::class,
            $this->getA2lixTranslatableFieldsOptions([
                'title' => [
                    'label' => 'Titel',
                ],
            ])
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Usp::class,
        ]);
    }
}
