<?php

namespace AdminBundle\Form\App\Subscription;

use AppBundle\Entity\Order\SubscriptionInterruption;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InterruptionType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('reason', TextType::class, [])
            ->add('startDate', DateType::class, [])
            ->add('endDate', DateType::class, []);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SubscriptionInterruption::class,
        ]);
    }
}
