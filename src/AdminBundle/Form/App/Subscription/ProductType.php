<?php

namespace AdminBundle\Form\App\Subscription;

use AppBundle\Entity\Order\SubscriptionProduct;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\MoneyType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder->add('position', IntegerType::class, [
            'attr' => [
                'class' => 'text-right',
            ],
        ]);

        $builder->add('quantity', IntegerType::class, [
            'required' => true,
            'attr' => [
                'min' => 1,
                'max' => 99,
            ],
        ]);

        $builder->add('product', EntityType::class, [
            'required' => true,
            'placeholder' => '',
            'class' => 'AppBundle\Entity\Catalog\Product\Product',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('p')
                    ->andWhere('p.parent IS NULL');
            },
            'choice_label' => function ($product) {
                return $product->getName();
            },
        ]);

        $builder->add('price', MoneyType::class, [
            'required' => true,
        ]);

        $builder->add('forward_price', MoneyType::class, [
            'required' => false,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SubscriptionProduct::class,
        ]);
    }
}
