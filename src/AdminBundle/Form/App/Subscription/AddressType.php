<?php

namespace AdminBundle\Form\App\Subscription;

use AppBundle\Entity\Relation\Address;
use AppBundle\Entity\Order\SubscriptionAddress;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\MoneyType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AddressType
 * @package AdminBundle\Form\App\Subscription
 */
class AddressType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder->add('position', IntegerType::class, [
            'attr' => [
                'class' => 'text-right',
            ],
        ])
            ->add('address', EntityType::class, [
                'class' => 'AppBundle\Entity\Relation\Address',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p');
                    //->andWhere('p.parent IS NULL');
                },
                'choice_label' => function (Address $address) {
                    return $address->getStreet() . " " . $address->getNumber() . ", " . $address->getPostcode() . " " . $address->getCity() . ", " . $address->getCountry()->getCode() . (($address->getDescription()) ? " (" . $address->getDescription() . ")" : null);
                },
            ])
            ->add('deliveryCosts', MoneyType::class, [
                'required' => false,
            ])
            ->add('deliveryRemark', TextType::class, [
                'required' => false,
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SubscriptionAddress::class,
        ]);
    }
}
