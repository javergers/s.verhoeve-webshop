<?php

namespace AdminBundle\Form\App;

use AppBundle\Entity\Site\Banner;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Site\Site;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\CKEditorType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\SlugType;
use AppBundle\Form\Type\TranslationsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('container', ContainerType::class);

        $container->add('site', EntityType::class, [
            'label' => "Site",
            "class" => Site::class,
            'placeholder' => '',
        ]);

        $container->add('banner', EntityType::class, [
            'label' => 'Banner',
            'class' => Banner::class,
            'placeholder' => 'Kies een banner',
            'required' => false,
        ]);

        $builder->add($container);

        $container2 = $builder->create('translations', ContainerType::class);

        $container2->add('translations', TranslationsType::class,
            $this->getA2lixTranslatableFieldsOptions([
                'title' => [
                    'label' => 'Titel',
                    'attr' => [
                        'class' => 'slugifyTitle',
                    ],
                ],
                'content' => [
                    'label' => 'Content',
                    'field_type' => CKEditorType::class,
                    'ckeditor' => [
                        'custom_toolbar' => "advanced",
                    ],
                ],
                'slug' => [
                    'label' => 'Slug',
                    'required' => false,
                    'field_type' => SlugType::class,
                    'base_on' => '.slugifyTitle',
                ],
            ])
        );

        $builder->add($container2);

        $builder->add(parent::addPublishableFields(null, $builder, $options));


        $container = $builder->create('container_accessibility', ContainerType::class);
        $column = $builder->create('container_accessibility_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Toegang',
            'icon' => 'lock2',
        ]);

        $loginRequiredOptions = [
            'label' => 'Zichtbaar voor',
            'choices' => [
                'Iedereen' => 0,
                'Ingelogde gebruikers' => 1,
            ],
            'expanded' => true,
            'multiple' => false,
        ];

        if ($builder->getData() && $builder->getData()->getLoginRequired() !== true) {
            $loginRequiredOptions['data'] = 0;
        }

        $column->add('loginRequired', ChoiceType::class, $loginRequiredOptions);

        $column->add('accessibleCustomerGroups', EntityType::class, [
            'label' => 'Klantgroep(en)',
            'class' => CustomerGroup::class,
            'multiple' => true,
            'required' => false,
            'multiselect' => [],
        ]);

        $container->add($column);

        $builder->add($container);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Page::class,
        ]);
    }
}
