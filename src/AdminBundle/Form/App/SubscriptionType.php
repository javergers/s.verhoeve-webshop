<?php

namespace AdminBundle\Form\App;

use AdminBundle\Form\App\Subscription\AddressType;
use AdminBundle\Form\App\Subscription\InterruptionType;
use AdminBundle\Form\App\Subscription\ProductType;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Entity\Site\Site;
use AppBundle\Entity\Order\Subscription;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\DateType;
use AppBundle\Form\Type\RowType;
use AppBundle\Form\Type\RruleType;
use AppBundle\Form\Type\TabType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubscriptionType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $row = $builder
            ->create('row_1', RowType::class);

        $column1 = $builder->create('column_1', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => 'Eigenschappen',
            'icon' => 'file-text2',
        ]);

        $column1
            ->add("description", TextType::class, [
                'required' => true,
                'label' => 'Omschrijving',
            ])
            ->add("site", EntityType::class, [
                'required' => true,
                'label' => 'Site',
                'class' => 'AppBundle\Entity\Site\Site',
                'placeholder' => '',
                'choice_label' => function (Site $site) {
                    return $site->translate()->getDescription();
                },
            ])
            ->add('company', EntityType::class, [
                'required' => true,
                'label' => 'Bedrijf',
                'class' => 'AppBundle\Entity\Relation\Company',
                'placeholder' => '',
                'choice_label' => function (Company $company) {
                    return $company->getName() . " (" . $company->getId() . ")";
                },
                'select2' => [
                    'ajax' => true,
                    'findBy' => [
                        'name',
                    ],
                ],
            ])
            ->add('customer', EntityType::class, [
                'required' => true,
                'label' => 'Besteller',
                'class' => 'AppBundle\Entity\Security\Customer\Customer',
                'placeholder' => '',
                'choice_label' => function (Customer $customer) {
                    return $customer->getFullname();
                },
                'select2' => [
                    'ajax' => true,
                    'findBy' => [
                        'fullname',
                    ],
                ],
            ]);

        $row->add($column1);


        $column1a = $builder->create('column_1a', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => 'Interne opmerking',
            'icon' => 'bubble',
        ]);

        $column1a
            ->add("backofficeRemark", TextareaType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'style' => 'height: 159px',
                    'placeholder' => "Alleen zichtbaar voor medewerkers Topgeschenken",
                ],
            ]);

        $row->add($column1a);

        $column2 = $builder->create('column_2', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => 'Opmerking voor leverancier',
            'icon' => 'bubbles4',
        ]);

        $column2
            ->add("supplierRemark", TextareaType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'style' => 'height: 159px',
                    'placeholder' => "Bloemist, taartleverancier, e.d.",
                ],
            ]);

        $row->add($column2);

        $builder->add($row);

        $row = $builder
            ->create('row_2', RowType::class);

        $column3 = $builder->create('column_3', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_8,
            'label' => 'Abonnement',
            'icon' => 'file-css',
        ]);

        $column3
            ->add("rrule", RruleType::class, [
                'label' => "Bezorgpatroon",
            ])
            ->add("startDate", DateType::class, [
                'label' => "Begindatum",
            ])
            ->add("endDate", DateType::class, [
                'label' => "Einddatum",
                'required' => false,
            ]);

        $row->add($column3);

        $column4 = $builder->create('column_4', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
            'label' => 'Overige instellingen',
            'icon' => 'equalizer',
        ]);

        $column4
            ->add("preferred_florist", IntegerType::class, [
                'label' => 'Voorkeursbloemist',
                'required' => false,
                'attr' => [
                    'min' => 1,
                    'max' => 9999,
                    'style' => 'width: 100px; text-align: right;',
                ],
            ])
            ->add("invoice_reference", TextType::class, [
                'label' => 'Factuurreferentie',
                'required' => false,
                'attr' => [
                    'placeholder' => 'PO-nummer, inkoopnummer, e.d.',
                ],
            ]);

        $row->add($column4);

        $builder->add($row);

        $builder->add($this->tabs($builder));
    }

    private function tabs(FormBuilderInterface $builder)
    {
        $tab_container = $builder->create('tab_container', ContainerType::class);

        $tab2 = $tab_container->create('tab_2', TabType::class, [
            'label' => 'Producten',
            'icon' => 'stack4',
        ]);

        $tab2->add('subscriptionProducts', CollectionType::class, [
            'entry_type' => ProductType::class,
            'label' => "Product",
            'by_reference' => false,
            'collection' => [
                'sortable' => true,
                'columns' => [
                    [
                        "label" => "Order *",
                        "width" => "50px",
                    ],
                    [
                        "label" => "Aantal *",
                        "width" => "75px",
                    ],
                    [
                        "label" => "Product *",
                        "width" => "300px",
                    ],
                    [
                        "label" => "Verkoopprijs (excl.) *",
                        "width" => "75px",
                    ],
                    [
                        "label" => "Doorgeefprijs (excl.)",
                        "width" => "75px",
                    ],
                ],
            ],
        ]);

        $tab3 = $tab_container->create('tab_3', TabType::class, [
            'label' => 'Adressen',
            'icon' => 'office',
        ]);

        $tab3->add('subscriptionAddresses', CollectionType::class, [
            'entry_type' => AddressType::class,
            'label' => "Adres",
            'by_reference' => false,
            'collection' => [
                'sortable' => true,
                'columns' => [
                    [
                        "label" => "Order *",
                        "width" => "50px",
                    ],
                    [
                        "label" => "Adres *",
                        "width" => "300px",
                    ],
                    [
                        "label" => "Bezorgkosten (excl.) *",
                        "width" => "75px",
                    ],
                    [
                        "label" => "Opmerking voor bezorger",
                        "width" => "300px",
                    ],
                ],
            ],
        ]);

        $tab4 = $tab_container->create('tab_4', TabType::class, [
            'label' => 'Onderbrekingen',
            'icon' => 'pause',
        ]);

        $tab4->add('subscriptionInterruptions', CollectionType::class, [
            'entry_type' => InterruptionType::class,
            'label' => "Reden",
            'by_reference' => false,
            'collection' => [
                'columns' => [
                    [
                        "label" => "Reden *",
                        "width" => "300px",
                    ],
                    [
                        "label" => "Beginatum *",
                        "width" => "200px",
                    ],
                    [
                        "label" => "Einddatum *",
                        "width" => "200px",
                    ],
                ],
            ],
        ]);

        $tab_container->add($tab2);
        $tab_container->add($tab3);
        $tab_container->add($tab4);

        return $tab_container;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Subscription::class,
        ]);
    }
}
