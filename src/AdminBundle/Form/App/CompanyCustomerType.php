<?php

namespace AdminBundle\Form\App;

use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyCustomerType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('gender', ChoiceType::class, [
                'placeholder' => '',
                'choices' => [
                    'Man' => 'male',
                    'Vrouw' => 'female',
                ],
            ])
            ->add('firstname', TextType::class, [
                'label' => 'Voornaam',
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Achternaam',
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mailadres',
                'disabled' => (strpos($_SERVER['REQUEST_URI'], "bewerken") !== false),
            ])
            ->add('phonenumber', TextType::class, [
                'label' => 'Telefoonnummer',
                'required' => false,
            ])
            ->add('mobilenumber', TextType::class, [
                'label' => 'Mobielnummer',
                'required' => false,
            ])
            ->add('enabled', ChoiceType::class, [
                'label' => 'Accountstatus',
                'choices' => [
                    'Ingeschakeld' => 1,
                    'Uitgeschakeld' => 0,
                ],
            ])
            ->add('customergroup', EntityType::class, [
                'label' => 'Klantgroep',
                'class' => 'AppBundle\Entity\Security\Customer\CustomerGroup',
                'placeholder' => '',
                'choice_label' => function ($customerGroup) {
                    return $customerGroup->getDescription();
                },
                'required' => true,
            ]);;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
        ]);
    }
}
