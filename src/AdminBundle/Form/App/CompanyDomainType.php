<?php

namespace AdminBundle\Form\App;

use AppBundle\DBAL\Types\YesNoType;
use AppBundle\Entity\Relation\CompanyDomain;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyDomainType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('domain', TextType::class, [
                'required' => true,
            ])
            ->add('allowRegistration', ChoiceType::class, [
                'choices' => YesNoType::getChoices(),
            ]);;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CompanyDomain::class,
        ]);
    }
}
