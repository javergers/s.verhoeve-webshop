<?php

namespace AdminBundle\Form\App;

use AppBundle\Entity\Finance\Tax\VatGroup;
use AppBundle\Entity\Supplier\SupplierGroup;
use AppBundle\Entity\Catalog\Product\Vat;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SupplierGroupType extends AbstractFormType implements ContainerAwareInterface
{

    Use ContainerAwareTrait;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $mainContainer = $builder->create('main_container', ContainerType::class, []);

        $column = $builder->create('column1', ColumnType::class, [
            'label' => 'Algemeen',
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
        ]);

        $column->add('name', TextType::class, [
            'label' => 'Naam',
            'widget_horizontal_label_class' => 'col-xs-6',
            'widget_form_element_class' => 'col-xs-6',
        ]);

        $mainContainer->add($column);

        $column2 = $builder->create('column2', ColumnType::class, [
            'label' => 'Standaard bezorg instellingen',
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
        ]);

        $deliveryAreaService = $this->container->get('delivery_area');

        $column2
            ->add('delivery_cost', TextType::class, [
                'label' => 'Kosten',
                'widget_horizontal_label_class' => 'col-xs-6',
                'widget_form_element_class' => 'col-xs-6',
            ])
            ->add('deliveryVatGroups', EntityType::class, [
                'label' => 'BTW Groepen',
                'class' => VatGroup::class,
                'choice_label' => function(VatGroup $vatGroup) {
                    return $vatGroup->getLevel()->getTitle().' ('.$vatGroup->getCountry()->translate()->getName().')';
                },
                'multiselect' => [],
                'multiple' => true,
            ])
            ->add('delivery_interval', ChoiceType::class, [
                'choices' => $deliveryAreaService->getDeliveryIntervalList(),
                'label' => 'Interval',
                'widget_horizontal_label_class' => 'col-xs-6',
                'widget_form_element_class' => 'col-xs-6',
            ]);
        $mainContainer->add($column2);

        $column3 = $builder->create('column3', ColumnType::class, [
            'label' => 'Commission',
            'column_layout' => ColumnType::COLUMN_WIDTH_4,
        ]);

        $column3
            ->add('supplierCommissions', CollectionType::class, [
                'entry_type' => SupplierGroupCommissionType::class,
                'label' => '',
                'by_reference' => false,
                'collection' => [
                    'columns' => [
                        [
                            'label' => 'Productgroep',
                            'width' => '150px',
                        ],
                        [
                            'label' => 'Percentage',
                            'width' => '50px',
                        ],
                    ],
                ],
            ]);

        $mainContainer->add($column3);

        $builder->add($mainContainer);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SupplierGroup::class,
        ]);
    }
}
