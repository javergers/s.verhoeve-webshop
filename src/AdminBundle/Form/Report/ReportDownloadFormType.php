<?php

namespace AdminBundle\Form\Report;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Form\Type\DateTimeType;
use AppBundle\Form\Type\EntityChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ReportDownloadFormType
 * @package AdminBundle\Form\Report
 */
class ReportDownloadFormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fromDate', DateTimeType::class, [
                'label' => 'Rapportage van',
                'required' => false
            ])
            ->add('tillDate', DateTimeType::class, [
                'label' => 'Rapportage tot',
                'required' => false
            ])
            ->add('company', EntityChoiceType::class, [
                'label' => 'Bedrijf',
                'field' => 'name',
                'class' => Company::class,
                'required' => false,
                'select2' => [
                    'ajax' => true,
                    'findBy' => ['name'],
                ],
                'placeholder' => 'Selecteer een bedrijf',
                'choice_label' => function ($id, $value) {
                    return $value . ' (' . $id . ')';
                },
            ])
            ->add('customer', EntityChoiceType::class, [
                'label' => 'Klant',
                'field' => 'fullname',
                'class' => Customer::class,
                'required' => false,
                'select2' => [
                    'ajax' => true,
                    'findBy' => ['fullname'],
                ],
                'placeholder' => 'Selecteer een klant',
                'choice_label' => function ($id, $value) {
                    return $value . ' (' . $id . ')';
                },
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Downloaden'
            ]);
    }

}