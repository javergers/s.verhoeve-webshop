<?php

namespace AdminBundle\Form\Report;

use AppBundle\Entity\Report\Report;
use AppBundle\Entity\Report\ReportColumn;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use RuleBundle\Entity\Rule;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ReportFormType
 * @package AdminBundle\Form\Report
 */
class ReportFormType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $container = $builder->create('report_container', ContainerType::class);
        $column = $builder->create('report_column', ColumnType::class, [
            'label' => 'Rapportage profiel'
        ]);

        $column
            ->add('title', TextType::class, [
                'label' => 'Profiel naam'
            ])
            ->add('columns', CollectionType::class, [
                'label' => 'Kolommen',
                'entry_type' => ReportColumnFormType::class,
                'by_reference' => false,
                'collection' => [
                    'sortable' => true,
                    'columns' => [
                        [
                            "label" => "Positie",
                            "width" => "50px"
                        ],
                        [
                            "label" => "Kolomnaam",
                            "width" => "100px"
                        ],
                        [
                            "label" => "Data kolom",
                            "width" => "300px"
                        ],
                    ]
                ]
            ]);

        $container->add($column);
        $builder->add($container);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Report::class
        ]);
    }

}