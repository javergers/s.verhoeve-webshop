<?php

namespace AdminBundle\Form\Report;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Report\Report;
use AppBundle\Entity\Report\ReportChart;
use AppBundle\Entity\Report\ReportChartData;
use AppBundle\Entity\Report\ReportColumn;
use AppBundle\Form\Type\AbstractEntityFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use RuleBundle\Entity\Rule;
use RuleBundle\Service\ResolverService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class ReportFormType
 * @package AdminBundle\Form\Report
 */
class ReportChartFormType extends AbstractEntityFormType
{

    private $dateFormat = 'd-M';

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $container = $builder->create('report_container', ContainerType::class);
        $column = $builder->create('report_column', ColumnType::class, [
            'label' => 'Rapportage grafiek',
        ]);

        $column
            ->add('name', TextType::class, [
                'label' => 'Naam',
            ])
            ->add('dateFilter', ChoiceType::class, [
                'label' => 'Datum filteren op',
                'choices' => [
                    'Dagelijks' => 'daily',
                    'Wekelijks' => 'weekly',
                    'Maandelijks' => 'monthly',
                    'Jaarlijks' => 'yearly',
                ],
            ])
            ->add('reportChartData', CollectionType::class, [
                'entry_type' => ReportChartDataFormType::class,
                'label' => 'Grafiek data',
                'by_reference' => false,
                'collection' => [
                    'columns' => [
                        [
                            'label' => 'Vanaf',
                        ],
                        [
                            'label' => 'Tot',
                        ],
                        [
                            'label' => 'Sorteren op',
                        ],
                        [
                            'label' => 'Data tonen van',
                        ],
                        [
                            'label' => 'Data label',
                        ],
                    ],
                ],
            ]);

        $container->add($column);
        $builder->add($container);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var ReportChart $reportChart */
            $reportChart = $event->getData();

            if (!$reportChart->getId()) {
                $reportChart->setReport($this->getEntity(Report::class));
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ReportChart::class,
        ]);
    }

}