<?php

namespace AdminBundle\Form\Report;

use AppBundle\Form\Type\DateTimeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ReportDownloadEntityFormType
 * @package AdminBundle\Form\Report
 */
class ReportDownloadEntityFormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fromDate', DateTimeType::class, [
                'label' => 'Rapportage van',
                'required' => false
            ])
            ->add('tillDate', DateTimeType::class, [
                'label' => 'Rapportage tot',
                'required' => false
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Downloaden'
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'attr' => [
               'disable-xhr' => true,
           ],
        ]);
    }

}