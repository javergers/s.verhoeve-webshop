<?php

namespace AdminBundle\Form;

use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class StockFormType
 * @package AdminBundle\Form
 */
class StockFormType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('physical_stock', TextType::class, [
                'label' => 'Actuele voorraad',
                'mapped' => false,
                'disabled' => true,
                'required' => false,
            ])
            ->add('virtual_stock', TextType::class, [
                'label' => 'Virtuele voorraad',
                'mapped' => false,
                'disabled' => true,
                'required' => false,
            ])
            ->add('threshold', TextType::class, [
                'label' => 'Drempel',
                'mapped' => false,
                'attr' => [
                    'pattern' => '^[-]{0}\d+$',
                ],
            ])
            ->add('mutation', TextType::class, [
                'label' => 'Mutatie',
                'mapped' => false,
                'attr' => [
                    'pattern' => '^[-]{0,1}\d+$',
                ],
            ]);

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {

            /** @var Product $product */
            $product = $event->getData();

            $form = $event->getForm();

            $form->get('physical_stock')->setData($product->getPhysicalStock());
            $form->get('virtual_stock')->setData($product->getVirtualStock());
            $form->get('threshold')->setData($product->getStockThreshold());
        });
    }
}
