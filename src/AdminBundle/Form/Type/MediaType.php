<?php

namespace AdminBundle\Form\Type;

use AppBundle\Exceptions\ImageUploadException;
use FM\ElfinderBundle\Form\Type\ElFinderType;
use League\Flysystem\Filesystem;
use Oneup\UploaderBundle\Uploader\Storage\FilesystemOrphanageStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MediaType
 * @package AdminBundle\Form\Type
 */
class MediaType extends ElFinderType
{
    /**
     * @var ContainerInterface
     */
    private $container;

    const TYPE_FILE = "file";
    const TYPE_IMAGE = "image";
    const TYPE_VIDEO = "video";

    /**
     * @var Filesystem $filesystem
     */
    private $filesystem;

    /**
     * MediaType constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {

            if ($this->container) {
                $getFormType = $event->getForm()->getParent()->getConfig()->getType();

                $mappings = $this->container->getParameter('oneup_uploader.config')['mappings'];

                $fileFolder = 'files';
                foreach($mappings as $key => $mapping) {
                    if (method_exists($getFormType, 'getBlockPrefix')) {
                        if($key == $getFormType->getBlockPrefix()) {
                            $fileFolder = $getFormType->getBlockPrefix();
                        }
                    }
                }

                /** @var FilesystemOrphanageStorage $manager */
                $manager = $this->container->get('oneup_uploader.orphanage_manager')->get($fileFolder);

                // get files
                $files = $manager->getFiles();
                $totalFiles = count($files);

                // Check if we got uploaded files
                if (!empty($files) && $totalFiles >= 1) {

                    // upload all files to the configured storage
                    $uploadedFiles = $manager->uploadFiles();

                    if ($totalFiles !== count($uploadedFiles)) {

                        $event->getForm()->addError(new FormError('Image couldn\'t be uploaded'));
                        throw new ImageUploadException();
                    }
                }
            }
        });

        parent::buildForm($builder, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $this->configureOptions($resolver);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $this->filesystem = $this->container->get("filesystem_public");

        $resolver->setDefaults([
            'disabled' => false,
            'media' => [],
            'enable' => true,
            'instance' => null,
            'homeFolder' => null,
            'baseUrl' => $this->filesystem->getPublicUrl(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['valid'] = !$form->isSubmitted() || $form->isValid();

        if (isset($options['media'])) {
            foreach ($options['media'] as $key => $values) {
                if (is_array($values)) {
                    foreach ($values as $attr => $value) {
                        $attrDashed = $this->camelCaseToDash($attr);

                        $options['media'][$key . '--' . $attrDashed] = $value;

                        if ($attrDashed != $attr) {
                            unset($options['media'][$attr]);
                        }
                    }

                    unset($options['media'][$key]);
                } else {
                    $keyDashed = $this->camelCaseToDash($key);

                    $options['media'][$keyDashed] = $values;

                    if ($keyDashed != $key) {
                        unset($options['media'][$key]);
                    }
                }
            }

            $view->vars['media'] = $options['media'];
        }

        $view->vars['enable'] = $form->getConfig()->getAttribute('enable');

        if ($form->getConfig()->getAttribute('enable')) {
            $view->vars['instance'] = $form->getConfig()->getAttribute('instance');
            $view->vars['homeFolder'] = $form->getConfig()->getAttribute('homeFolder');
            $view->vars['baseUrl'] = $this->filesystem->getPublicUrl();
        }
    }

    /**
     * @param $string
     * @return string
     */
    private function camelCaseToDash($string)
    {
        return strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $string));
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return ElFinderType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'media';
    }
}
