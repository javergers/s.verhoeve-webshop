<?php

namespace AdminBundle\Form\Finance\Vat;

use AppBundle\Entity\Finance\Tax\VatRule;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\YesNoType;
use RuleBundle\Entity\Rule;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class VatRuleFormType
 * @package AdminBundle\Form\Finance\Vat
 */
class VatRuleFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $container = $builder->create('vat_rule_container', ContainerType::class, []);
        $column = $builder->create('vat_rule_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'BTW regel',
        ]);

        $column
            ->add('position', NumberType::class, [
                'label' => 'Prioriteit',
            ])
            ->add('rule', EntityType::class, [
                'label' => 'Regel',
                'class' => Rule::class,
            ])
            ->add('allowSplit', YesNoType::class, [
                'label' => 'Mag splitsen',
            ]);

        $container->add($column);
        $builder->add($container);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => VatRule::class,
        ]);
    }
}