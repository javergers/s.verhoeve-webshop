<?php

namespace AdminBundle\Form\Parameter;

use AdminBundle\Form\Type\MediaType;
use AppBundle\Entity\Common\Parameter\ParameterEntityFile;
use AppBundle\Form\Type\AbstractFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductImageType
 * @package AdminBundle\Form\Catalog
 */
class ParameterEntityFileType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    protected $entityManager;

    /**
     * ProductImageType constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('path', MediaType::class, [
                'label' => false,
                'instance' => 'form',
                'enable' => true,
                'media' => [
                    'type' => MediaType::TYPE_FILE,
                    'upload' => true,
                ],
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ParameterEntityFile::class,
        ]);
    }
}
