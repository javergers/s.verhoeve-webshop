<?php

namespace AdminBundle\Form\Parameter;

use AdminBundle\Form\AddParameterFieldsSubscriber;
use AppBundle\DBAL\Types\YesNoType;
use AppBundle\Entity\Common\Parameter\Parameter;
use AppBundle\Entity\Common\Parameter\ParameterEntity;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\TabType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ParameterEntityFormType
 * @package AdminBundle\Form\Parameter
 */
class ParameterEntityFormType extends AbstractType
{
    use ContainerAwareTrait;

    /**
     * ParameterEntityFormType constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $allowedParentTypes = [
            ContainerType::class,
            ColumnType::class,
            TabType::class
        ];

        $entities = $this->container->get('doctrine')->getManager()->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();

        $entityChoices = [];
        foreach ($entities as $entity) {
            $entityChoices[$entity] = $entity;
        }

        ksort($entityChoices);

        $container = $builder->create('parameter_container', ContainerType::class, []);

        $column = $builder->create('parameter_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
        ]);

        $column->add('parameter', EntityType::class, [
            'class' => Parameter::class,
            'required' => true,
            'label' => 'Parameter',
            'select2' => [
                'placeholder' => 'Selecteer parameter',
            ],
        ]);

        $column->add('parent', EntityType::class, [
            'class' => ParameterEntity::class,
            'required' => false,
            'query_builder' => function (EntityRepository $er) use($allowedParentTypes) {
                return $er->createQueryBuilder('pe')
                    ->leftJoin('pe.parameter', 'p')
                    ->where('p.formType IN (:allowedTypes)')
                    ->setParameter('allowedTypes', $allowedParentTypes);
            },
            'choice_label' => function (ParameterEntity $parameterEntity) {
                return $parameterEntity->getParameter();
            },
            'label' => 'Bovenliggende',
            'placeholder' => '',
            'select2' => [
                'placeholder' => 'Selecteer bovenliggende',
            ],
        ]);

        $column->add('label', TextType::class, [
            'label' => 'Label',
            'required' => true,
        ]);

        $column->add('entity', ChoiceType::class, [
            'required' => false,
            'label' => 'Entity',
            'choices' => $entityChoices,
            'select2' => [
                'placeholder' => 'Globale parameter',
            ],
        ]);

        $column->add('required', ChoiceType::class, [
            'label' => 'Verplicht',
            'choices' => YesNoType::getChoices(),
        ]);

        $column->add('defaultValue', TextType::class, [
            'label' => 'Standaard waarde',
            'required' => false,
        ]);

        $container->add($column);
        $builder->add($container);

        $builder->addEventSubscriber(new AddParameterFieldsSubscriber($this->container, ParameterEntity::class,
            $container));


    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'admin_parameter_form';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ParameterEntity::class,
            'translation_domain' => false
        ]);
    }
}
