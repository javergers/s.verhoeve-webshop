<?php

namespace AdminBundle\Form\Parameter;

use AdminBundle\Form\AddParameterFieldsSubscriber;
use AppBundle\Form\Type\ContainerType;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class SystemParameterFormType
 * @package AdminBundle\Form\Parameter
 */
class SystemParameterFormType extends AbstractType
{

    use ContainerAwareTrait;

    /**
     * SystemParameterFormType constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create("parameter_container", ContainerType::class, []);

        $builder->add($container);

        $builder->add('submit', SubmitType::class, [
            'label' => 'Opslaan',
            'attr' => [
                'class' => 'btn btn-primary',
            ],
        ]);

        $builder->addEventSubscriber(new AddParameterFieldsSubscriber($this->container, null, $container));
    }

}
