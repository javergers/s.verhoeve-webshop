<?php

namespace AdminBundle\Form\DataSet\Settings;

use AppBundle\Form\Type\AbstractFormType;
use RuleBundle\Entity\Entity;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DataSetFilterFormType
 * @package AdminBundle\Form\DataSet\Settings
 */
class DataSetFilterFormType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('dataSetFilterFields', DataSetFilterFieldCollectionType::class, [
            'label' => false,
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Entity::class,
        ));
    }
}
