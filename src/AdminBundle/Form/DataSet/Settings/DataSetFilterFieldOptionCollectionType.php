<?php

namespace AdminBundle\Form\DataSet\Settings;

use AdminBundle\Form\CollectionListType;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DataSetFilterFieldOptionCollectionType
 * @package AdminBundle\Form\DataSet\Settings
 */
class DataSetFilterFieldOptionCollectionType extends AbstractFormType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults([
            'label' => false,
            'required' => false,
            'entry_type' => DataSetFilterFieldOptionEntryType::class,
            'by_reference' => false,
            'collection' => [
                'actions_position' => ['after'],
                'list_headings' => ['label'],
                'list_headings_combined' => true,
                'sortable' => true,
            ],
        ]);
    }

    /**
     * @return string
     */
    public function getParent(): string
    {
        return CollectionListType::class;
    }
}
