<?php

namespace AdminBundle\Form\DataSet\Settings;

use AdminBundle\Form\CollectionListEntryType;
use AdminBundle\Form\Fields\PriceRange;
use AppBundle\Entity\Common\DataSet\DataSetFilterField;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\YesNoType;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionException;
use RuleBundle\Entity\Entity;
use RuleBundle\Service\ResolverService;
use RuleBundle\Service\RuleTransformerService;
use RuntimeException;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DataSetFilterFieldEntryType
 * @package AdminBundle\Form\DataSet\Settings
 */
class DataSetFilterFieldEntryType extends AbstractFormType
{
    /**
     * @var array
     */
    private static $choices = [];

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var RuleTransformerService
     */
    private $ruleTransformer;

    /**
     * DataSetFilterFieldEntryType constructor.
     * @param EntityManagerInterface $entityManager
     * @param RequestStack           $requestStack
     * @param RuleTransformerService $ruleTransformer
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        RequestStack $requestStack,
        RuleTransformerService $ruleTransformer
    ) {
        $this->entityManager = $entityManager;
        $this->requestStack = $requestStack;
        $this->ruleTransformer = $ruleTransformer;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws AnnotationException
     * @throws ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('position', TextType::class, [
                'label' => 'Position',
                'attr' => ['class' => 'position'],
            ])
            ->add('path', ChoiceType::class, [
                'label' => 'Eigenschap',
                'placeholder' => 'Kies een eigenschap',
                'choices' => $this->getPathChoices(),
                'required' => true,
                'attr' => [
                    'class' => 'select2'
                ]
            ])
            ->add('formType', ChoiceType::class, [
                'label' => 'Veld type',
                'placeholder' => 'Kies een veld type',
                'choices' => $this->getFieldTypes(),
                'attr' => ['class' => 'form-type'],
                'required' => true,
            ])
            ->add('label', TextType::class, [
                'label' => 'Label',
            ])
            ->add('placeholder', TextType::class, [
                'label' => 'Placeholder',
            ])
//            ->add('pinned', CheckboxType::class, [
//                'label' => 'Vastgezet',
//                'required' => false,
//            ])
            ->add('multiple', CheckboxType::class, [
                'label' => 'Meerdere waardes toestaan',
                'required' => false,
            ])
            ->add('dataSetFilterFieldOptions', DataSetFilterFieldOptionCollectionType::class, [
                'entry_type' => DataSetFilterFieldOptionEntryType::class,
                'prototype_name' => '__name_child__',
                'label' => 'Keuze optie',
                'by_reference' => false,
            ]);
    }

    /**
     * @return array
     * @throws AnnotationException
     * @throws ReflectionException
     */
    public function getPathChoices()
    {
        $entity = $this->getEntity();
        $entityId = $entity->getId();

        // Limit the number of query requests for equal entities
        if (isset(self::$choices[$entityId])) {
            return self::$choices[$entityId];
        }

        //get choices and add path field
        $choices = [];

        $filters = $this->ruleTransformer->getFilters([
            ResolverService::QB_ALIAS => $entity,
        ]);

        foreach ($filters as $filter) {
            $choices[$filter['optgroup']][$filter['label']] = $filter['id'];
        }

        self::$choices[$entityId] = $choices;

        return $choices;
    }

    /**
     * @return array
     */
    private function getFieldTypes()
    {
        return [
            'Text veld' => TextType::class,
            'Meerkeuze veld' => ChoiceType::class,
            'Relatie veld' => EntityType::class,
            'Ja/Nee veld' => YesNoType::class,
            'Prijs veld' => PriceRange::class,
        ];
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DataSetFilterField::class,
        ]);
    }

    /**
     * @return null|Request
     */
    private function getRequest(): ?Request
    {
        return $this->requestStack->getMasterRequest();
    }

    /**
     * @return Entity
     */
    private function getEntity(): Entity
    {
        return $this->entityManager->getRepository(Entity::class)->find($this->getEntityId());
    }

    /**
     * @return int|null
     */
    private function getEntityId()
    {
        $request = $this->getRequest();

        $id = null;

        if (null !== $request && $url = $request->getRequestUri()) {
            preg_match('/\d+/', $request->getRequestUri(), $matches);

            if (!empty($matches[0])) {
                $id = (int)$matches[0];
            }
        }

        if (null === $id) {
            throw new RuntimeException('Entity `id` not found in request.');
        }

        return $id;
    }

    /**
     * @return string
     */
    public function getParent(): string
    {
        return CollectionListEntryType::class;
    }
}
