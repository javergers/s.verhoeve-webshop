<?php

namespace AdminBundle\Form\Audit;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\DatatableBuilder;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderCollection;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use AppBundle\Interfaces\Audit\AuditEntityInterface;
use DataDog\AuditBundle\Entity\Association;
use DataDog\AuditBundle\Entity\AuditLog;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class AuditLogType
 * @package AdminBundle\Form\Audit
 */
class AuditLogType extends AbstractType
{
    use ContainerAwareTrait;

    /**
     * @var FormBuilderInterface $builder
     */
    private $builder;

    /**
     * @var array
     */
    private $options;

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->builder = $builder;
        $this->options = $options;
    }

    /**
     * @param DatatableBuilder $builder
     */
    public function buildDatatable(DatatableBuilder $builder)
    {
        $formatter = new \IntlDateFormatter(\Locale::getDefault(), \IntlDateFormatter::FULL, \IntlDateFormatter::NONE);
        $em = $this->container->get('doctrine');
        $historyInfo = $this->container->get('admin.audit_log')->getHistoryInfo();

        $getEntityData = function (Association $source) use ($em) {

            if (empty($source->getClass()) || empty($source->getFk())) {
                return false;
            }

            return $em->getRepository($source->getClass())->findOneBy([
                'id' => $source->getFk(),
            ]);
        };

        $builder
            ->add('loggedAt', EntityColumn::class, [
                'label' => 'Datum',
                'value' => function (\DateTime $loggedAt) use ($formatter) {
                    return ucfirst($formatter->format($loggedAt));
                },
            ])
            ->add('tableName', CallbackColumn::class, [
                'label' => 'Type',
                'callback' => function ($value, AuditLog $log) {
                    void($value);

                    switch ($log->getTbl()) {
                        case 'order':
                        case 'order_collection':
                            return 'order';
                        case 'company':
                            return 'bedrijf';
                        case 'product':
                            return 'product';
                        case 'customer':
                            return 'klant';
                        default:
                            return $log->getTbl();
                    }
                },
            ])
            ->add('number', CallbackColumn::class, [
                'label' => 'Nummer',
                'callback' => function ($value, AuditLog $log) use ($getEntityData) {
                    void($value);

                    /** @var Association $source */
                    $source = $log->getSource();
                    $entityData = $getEntityData($source);
                    $tableName = $log->getTbl();
                    $number = $source->getFk();

                    if ($tableName === 'order') {

                        /** @var Order $entityData */
                        $number = $entityData->getOrderCollection()->getNumber();
                    }

                    if ($tableName === 'order_collection') {

                        /** @var Order $entityData */
                        $number = $entityData->getNumber();
                    }

                    return $number;
                },
            ])
            ->add('receiver', CallbackColumn::class, [
                'label' => 'Naam',
                'callback' => function ($value, AuditLog $log) use ($historyInfo, $getEntityData) {
                    void($value);

                    /** @var Order|OrderCollection|Company|Product|Customer $entityData */
                    $entityData = $getEntityData($log->getSource());

                    if (!($entityData instanceof AuditEntityInterface)) {
                        return '';
                    }

                    $pageviewSettings = $entityData->getAuditSettings();

                    return (isset($pageviewSettings['label']) ? $pageviewSettings['label']($entityData) : '');
                },
            ])
            ->add('action', EntityColumn::class, [
                'label' => 'Actie',
                'value' => function (string $action) {

                    switch ($action) {
                        case 'insert':
                            return 'toegevoegd';
                        case 'update':
                            return 'geupdate';
                        case 'view':
                            return 'bekeken';
                        default:
                            return '';
                    }
                },
            ])
            ->add('time', CallbackColumn::class, [
                'label' => 'Tijd',
                'callback' => function ($value, AuditLog $log) {
                    void($value);
                    return $log->getLoggedAt()->format('H:i');
                },
            ]);
    }
}
