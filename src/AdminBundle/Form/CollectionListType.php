<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

/**
 * Class CollectionListType
 * @package AdminBundle\Form
 */
class CollectionListType extends AbstractType
{
    /**
     * @return string
     */
    public function getParent(): string
    {
        return CollectionType::class;
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'collection_list';
    }
}