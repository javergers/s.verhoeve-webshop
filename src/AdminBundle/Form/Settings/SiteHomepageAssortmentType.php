<?php

namespace AdminBundle\Form\Settings;

use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Site\SiteHomepageAssortment;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SiteHomepageAssortmentType
 * @package AdminBundle\Form\Settings
 */
class SiteHomepageAssortmentType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        void($options);

        $builder->add('position', IntegerType::class, [
            'attr' => [
                'class' => 'text-right',
            ],
        ]);

        $builder->add('assortment', EntityType::class, [
            'label' => 'Assortiment',
            'class' => Assortment::class,
            'choice_label' => function (Assortment $assortment) {
                return $assortment->getName() . (null !== $assortment->translate()->getHomepageSlogan() ? ' ('. $assortment->translate()->getHomepageSlogan() .')': null );
            },
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SiteHomepageAssortment::class,
        ]);
    }
}
