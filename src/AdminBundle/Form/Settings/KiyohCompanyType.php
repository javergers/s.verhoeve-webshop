<?php

namespace AdminBundle\Form\Settings;

use AppBundle\Annotation\Type\YesNoType;
use AppBundle\Entity\Site\Domain;
use AppBundle\ThirdParty\Kiyoh\Entity\KiyohCompany;
use AppBundle\Entity\Site\Page;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Repository\PageRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class KiyohCompanyType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $container = $builder->create('container_general', ContainerType::class);

        $column_left = $builder->create('column_left', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Algemeen',
            'icon' => 'profile',
        ]);

        $column_left
            ->add('name', TextType::class, [
                'label' => 'Naam',
                'required' => true,
            ])
            ->add('domain', EntityType::class, [
                'placeholder' => "Kies een domeinnaam om dit profiel te koppelen",
                'label' => 'Domeinnaam',
                'class' => 'AppBundle\Entity\Site\Domain',
                'choice_label' => function (Domain $domain) {
                    return $domain->getDomain();
                },
                'required' => true,
                'select2' => [],
            ])
            ->add('syncEnabled', ChoiceType::class, [
                'label' => 'Synchronisatie actief',
                'choices' => array_flip(YesNoType::$choices),
            ]);

        if ($builder->getData() && $builder->getData()->getId()) {
            $column_left->add('page', EntityType::class, [
                'label' => 'Tekstpagina',
                'class' => Page::class,
                'query_builder' => function (PageRepository $er) use ($builder) {
                    $qb = $er->createQueryBuilder('p')
                        ->select('p')
                        ->where('p.site = :site')
                        ->setParameter('site', $builder->getData()->getDomain()->getSite()->getId());

                    return $qb;
                },
                'choice_label' => function (Page $page) {
                    return $page->translate()->getTitle();
                },
                'placeholder' => 'Kies een tekstpagina',
                'required' => false,
            ]);
        }

        $column_right = $builder->create('column_right', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_6,
            'label' => 'Kiyoh Eigenschappen',
            'icon' => 'share3',
        ]);

        $column_right
            ->add('identifier', TextType::class, [
                'label' => 'Profiel ID',
                'required' => true,
            ])
            ->add('url', UrlType::class, [
                'label' => 'Profiel Url',
                'required' => true,
            ])
            ->add('xmlUrl', UrlType::class, [
                'label' => 'XML Url',
                'required' => true,
            ])
            ->add('connectorCode', TextType::class, [
                'label' => 'XML Connector Code',
                'required' => true,
            ]);

        $container->add($column_left);
        $container->add($column_right);

        $builder->add($container);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => KiyohCompany::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'kiyoh_company';
    }
}
