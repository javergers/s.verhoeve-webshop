<?php

namespace AdminBundle\Form\Settings;

use AppBundle\Entity\Payment\Paymentmethod;
use AppBundle\Entity\Site\SitePaymentMethodSettings;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SitePaymentMethodSettingsType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder->add('paymentMethod', EntityType::class, [
            'class' => Paymentmethod::class,
            'choice_label' => function (Paymentmethod $paymentmethod) {
                return $paymentmethod->translate()->getDescription();
            },
            'attr' => [
                "style" => "background-color: inherit; border: none; -webkit-appearance: none; padding: 0; cursor: default;",
            ],
        ]);

        $builder->add('enabled', ChoiceType::class, [
            'choices' => [
                "Ingeschakeld" => 1,
                "Uitgeschakeld" => 0,
            ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SitePaymentMethodSettings::class,
        ]);
    }
}
