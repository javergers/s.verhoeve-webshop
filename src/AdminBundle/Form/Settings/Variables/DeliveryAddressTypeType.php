<?php

namespace AdminBundle\Form\Settings\Variables;

use AdminBundle\Form\Settings\Variables\DeliveryAddressType\DeliveryAddressTypeFieldType;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Geography\DeliveryAddressType;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\TranslationsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DeliveryAddressTypeType
 * @package AdminBundle\Form\Settings\Variables
 */
class DeliveryAddressTypeType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $container = $builder->create('container_delivery_type', ContainerType::class, [
            'label' => false,
        ]);

        $container->add('sites', EntityType::class, [
            'label' => 'Sites',
            'class' => 'AppBundle\Entity\Site\Site',
            'choice_label' => function ($site) {
                return $site->translate()->getDescription();
            },
            'required' => true,
            'multiple' => true,
            'multiselect' => [
                'nonSelectedText' => 'Maak een keuze',
            ],
        ]);

        $container->add('excludedAssortments', EntityType::class, [
            'label' => 'Uitgesloten assortiment(en)',
            'class' => 'AppBundle\Entity\Catalog\Assortment\Assortment',
            'choice_label' => function (Assortment $assortment) {
                return $assortment->getName();
            },
            'required' => false,
            'multiple' => true,
            'multiselect' => [
                'nonSelectedText' => 'Maak een keuze',
            ],
        ]);

        $builder->add($container);

        $builder->add('translations', TranslationsType::class,
            $this->getA2lixTranslatableFieldsOptions([
                'name' => [
                    'label' => 'Naam',
                ],
                'companyLabel' => [
                    'label' => 'Label voor de bedrijfsnaam',
                ],
                'recipientLabel' => [
                    'label' => 'Label voor de ontvanger',
                ],
            ])
        );

        $container = $builder->create('container_delivery_type_2', ContainerType::class, [
            'label' => false,
        ]);

        $container->add('hideCompanyField', CheckboxType::class, [
            'label' => 'Verberg bedrijfsnaam',
            'required' => false,
        ]);

        $builder->add($container);

        $container = $builder->create('container_delivery_type_fields', ContainerType::class, [
            'label' => false,
        ]);

        $container->add('fields', CollectionType::class, [
            'entry_type' => DeliveryAddressTypeFieldType::class,
            'by_reference' => false,
            'collection' => [
                'sortable' => true,
                'columns' => [
                    [
                        "label" => "Positie *",
                        "width" => "80px",
                    ],
                    [
                        "label" => "Label",
                        "width" => "300px",
                    ],
                    [
                        "label" => "Type",
                        "width" => "100px",
                    ],
                    [
                        "label" => "Verplicht",
                        "width" => "100px",
                    ],
                ],
            ],
        ]);

        $builder->add($container);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DeliveryAddressType::class,
        ]);
    }
}
