<?php

namespace AdminBundle\Form\Settings\Geographic;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class CityType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('province', EntityType::class, [
                'label' => 'Provincie',
                'class' => 'AppBundle\Entity\Geography\Province',
                'choice_label' => function ($category) {
                    //return get_class($category);
                    return $category->translate()->getName();
                },
                //'choices_as_values' => true,
                'group_by' => function ($category) {
                    return $category->getCountry()->getCode();
                },
            ])
            ->add('translations', TranslationsType::class, []);
    }
}
