<?php

namespace AdminBundle\Form\Settings\Designer;

use AppBundle\Entity\Designer\DesignerLayout;
use AppBundle\Entity\Designer\DesignerTemplate;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TemplateType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $container = $builder->create('container', ContainerType::class, [
            'label' => false,
        ]);

        $container->add('layout', EntityType::class, [
            'label' => 'Formaat',
            'class' => DesignerLayout::class,
            'choice_label' => function (DesignerLayout $layout) {
                return $layout->getName();
            },
            'placeholder' => '',
        ]);

        $container->add('title', TextType::class, [
            'label' => 'Naam',
            'required' => true,
        ]);

        $builder->add($container);

        $container2 = $builder->create('container_objects', ContainerType::class, [
            'label' => false,
        ]);

        $container2->add("objects", CollectionType::class, [
            'entry_type' => TemplateObjectType::class,
            'label' => false,
            'by_reference' => false,
            'collection' => [
                'columns' => [
                    [
                        'label' => 'Type',
                        'width' => '100px',
                    ],
                    [
                        'label' => 'X-positie',
                        'width' => '100px',
                    ],
                    [
                        'label' => 'Y-positie',
                        'width' => '100px',
                    ],
                    [
                        'label' => 'Breedte',
                        'width' => '100px',
                    ],
                    [
                        'label' => 'Hoogte',
                        'width' => '100px',
                    ],
//                    [
//                        'label' => 'Bewerkbaar',
//                        'width' => '100px'
//                    ],
//                    [
//                        'label' => 'Verplaatsbaar',
//                        'width' => '100px'
//                    ],
                ],
            ],
        ]);

        $builder->add($container2);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DesignerTemplate::class,
        ]);
    }
}
