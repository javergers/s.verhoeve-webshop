<?php

namespace AdminBundle\Form\Settings\Designer;

use AppBundle\Entity\Designer\DesignerFont;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FontType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $container = $builder->create('container', ContainerType::class, [
            'label' => false,
        ]);

        $container->add('family', ChoiceType::class, [
            'label' => 'Lettertype',
            'select2' => [],
            'placeholder' => 'Kies een font',
            'choices' => $this->getFonts(),
        ]);

        $builder->add($container);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            /** @var $font DesignerFont */
            $font = $event->getData();

            if (in_array($font->getFamily(), $this->getGoogleFonts())) {
                $font->setType("google");
            } else {
                $font->setType(null);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DesignerFont::class,
        ]);
    }

    private function getFonts()
    {
        return [
            'WebFonts' => $this->getWebFonts(),
            'Google Fonts' => $this->getGoogleFonts(),
        ];
    }

    private function getGoogleFonts()
    {
        /* @TODO !global $kernel */
        global $kernel;

        $data = json_decode(file_get_contents('https://www.googleapis.com/webfonts/v1/webfonts?key=' . $kernel->getContainer()->getParameter("google_api_key")));

        $fonts = [];

        foreach ($data->items as $font) {
            $fonts[$font->family] = $font->family;
        }

        return $fonts;
    }

    private function getWebFonts()
    {
        return [
            "Arial" => "Arial",
            "Arial Black" => "Arial Black",
            "Comic Sans" => "Comic Sans",
            "Courier" => "Courier",
            "Georgia" => "Georgia",
            "Impact" => "Impact",
            "Lucida Console" => "Lucida Console",
            "Lucida Sans" => "Lucida Sans",
            "Palatino Linotype" => "Palatino Linotype",
            "Tahoma" => "Tahoma",
            "Times New Roman" => "Times New Roman",
            "Trebuchet" => "Trebuchet",
            "Verdana" => "Verdana",
        ];
    }
}
