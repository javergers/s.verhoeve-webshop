<?php

namespace AdminBundle\Form\Settings\Designer;

use AppBundle\Entity\Designer\DesignerCollageObject;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DesignerCollageObjectFormType
 * @package AdminBundle\Form\Settings\Designer
 */
class DesignerCollageObjectFormType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type', ChoiceType::class, array(
            'choices' => [
                'Afbeelding' => 'image'
            ]
        ));

        $builder->add('x', PercentType::class, array(
            'scale' => 2
        ));

        $builder->add('y', PercentType::class, array(
            'scale' => 2
        ));

        $builder->add('width', PercentType::class, array(
            'scale' => 2
        ));

        $builder->add('height', PercentType::class, array(
            'scale' => 2
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DesignerCollageObject::class,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'designer_collage_object';
    }
}
