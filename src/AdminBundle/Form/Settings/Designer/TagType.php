<?php

namespace AdminBundle\Form\Settings\Designer;

use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class TagType
 * @package AdminBundle\Form\Settings\Designer
 */
class TagType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $container = $builder->create('container', ContainerType::class, [
            'label' => false,
        ]);

        $container->add('name', TextType::class, [
            'label' => "Labels",
            'required' => true,
            'trim' => true,
        ]);

        $builder->add($container);

    }
}
