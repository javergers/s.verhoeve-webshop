<?php

namespace AdminBundle\Form\Settings;

use AppBundle\Entity\Site\Domain;
use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SiteDomainType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('domain', TextType::class, [
                'label' => false,
                'required' => true,
            ])
            ->add('main', CheckboxType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'class' => 'domain-main',
                ],
            ])
            ->add('mailFromName', TextType::class, [
                'label' => false,
                'required' => true,
            ])
            ->add('mailFromAddress', EmailType::class, [
                'label' => false,
                'required' => true,
            ])
            ->add('defaultLocale', ChoiceType::class, [
                'label' => false,
                'required' => true,
                'choices' => [
                    'nl_NL' => 'nl_NL',
                    'nl_BE' => 'nl_BE',
                    'fr_BE' => 'fr_BE',
                    'en_US' => 'en_US',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Domain::class,
        ]);
    }

    public function getBlockPrefix()
    {
        return 'site_domain';
    }
}
