<?php

namespace AdminBundle\Form\Settings;

use AppBundle\DBAL\Types\ThemeType;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Geography\Country;
use AppBundle\Entity\Site\Page;
use AppBundle\Entity\Site\Site;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\TranslationType;
use AppBundle\Repository\PageRepository;
use Doctrine\ORM\EntityRepository;
use Misd\PhoneNumberBundle\Form\Type\PhoneNumberType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class SubSiteFormType
 * @package AdminBundle\Form\Settings
 */
class SubSiteFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     * @throws \Exception
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $container = $builder->create('site_container', ContainerType::class);

        $defaultColumn = $builder->create('column_default', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Algemeen',
            'icon' => 'cog2',
        ]);

        $defaultColumn
            ->add('country', EntityType::class, [
                'label' => 'Land',
                'placeholder' => '',
                'class' => Country::class,
            ])
            ->add('description', TranslationType::class, [
                'label' => 'Omschrijving',
            ])
            ->add('theme', ChoiceType::class, [
                'label' => 'Thema',
                'choices' => ThemeType::getChoices(),
                'placeholder' => 'Kies een thema voor deze website',
                'required' => true,
            ])
            ->add('email', EmailType::class, [
                'label' => 'E-mailadres',
            ])
            ->add('phoneNumber', PhoneNumberType::class, [
                'label' => 'Telefoonnummer',
            ])
            ->add('slug', TextType::class, [
                'label' => 'Slug',
                'required' => false,
            ])
            ->add('parent', EntityType::class, [
                'label' => 'Hoofdwebsite',
                'class' => Site::class,
                'required' => false,
            ]);

        $container->add($defaultColumn);

        if ($builder->getData() && $builder->getData()->getId()) {
            $homepageColumn = $builder->create('column_homepage', ColumnType::class, [
                'column_layout' => ColumnType::COLUMN_WIDTH_12,
                'label' => 'Homepage',
                'icon' => 'insert-template',
            ]);

            $homepageColumn->add('homepage', EntityType::class, [
                'label' => 'Tekstpagina',
                'class' => Page::class,
                'query_builder' => function (PageRepository $er) use ($builder) {
                    $qb = $er->createQueryBuilder('p')
                        ->select('p')
                        ->where('p.site = :site')
                        ->setParameter('site', $builder->getData()->getId());

                    return $qb;
                },
                'choice_label' => function (Page $page) {
                    return $page->translate()->getTitle();
                },
                'placeholder' => 'Kies een tekstpagina',
                'required' => false,
            ])->add('homepageAssortment', EntityType::class, [
                'label' => 'Assortiment',
                'class' => Assortment::class,
                'query_builder' => function (EntityRepository $er) use ($builder) {
                    $qb = $er->createQueryBuilder('a')
                        ->select('a')
                        ->join('a.sites', 'site', 'WITH', 'site.id = :site')
                        ->setParameter('site', $builder->getData()->getId());

                    return $qb;
                },
                'choice_label' => 'name',
                'placeholder' => 'Kies een assortiment',
                'required' => false,
            ])->add('homepageAbout', EntityType::class, [
                'label' => 'About',
                'class' => Page::class,
                'query_builder' => function (PageRepository $er) use ($builder) {
                    $qb = $er->createQueryBuilder('p')
                        ->select('p')
                        ->where('p.site = :site')
                        ->setParameter('site', $builder->getData()->getId());

                    return $qb;
                },
                'choice_label' => function (Page $page) {
                    return $page->translate()->getTitle();
                },
                'placeholder' => 'Kies een tekstpagina',
                'required' => false,
            ])->add('customerServicePopupPage', EntityType::class, [
                'label' => 'Klantenservice Popup',
                'class' => Page::class,
                'query_builder' => function (PageRepository $er) use ($builder) {
                    $qb = $er->createQueryBuilder('p')
                        ->select('p')
                        ->where('p.site = :site')
                        ->setParameter('site', $builder->getData()->getId());

                    return $qb;
                },
                'choice_label' => function (Page $page) {
                    return $page->translate()->getTitle();
                },
                'placeholder' => 'Kies een tekstpagina',
                'required' => false,
            ]);

            $container->add($homepageColumn);

            $assortmentColumn = $builder->create('column_assortment', ColumnType::class, [
                'column_layout' => ColumnType::COLUMN_WIDTH_12,
                'label' => 'Kies hier de assortimenten voor op de homepage',
                'icon' => 'check',
            ]);

            $assortmentColumn->add('homepageAssortments', CollectionType::class, [
                'entry_type' => SiteHomepageAssortmentType::class,
                'label' => false,
                'by_reference' => false,
                'collection' => [
                    'sortable' => true,
                    'columns' => [
                        [
                            'label' => 'Positie *',
                            'width' => '80px',
                        ],
                        [
                            'label' => 'Assortment *',
                            'width' => '300px',
                        ],
                    ],
                ],
            ]);

            $container->add($assortmentColumn);
        }

        $builder->add($container);

        $builder->add('submit', SubmitType::class, [
            'label' => 'Opslaan',
            'attr' => [
                'class' => 'btn-primary',
            ]
        ]);
    }

}