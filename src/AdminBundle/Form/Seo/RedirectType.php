<?php

namespace AdminBundle\Form\Seo;

use AppBundle\Entity\Site\Domain;
use AppBundle\Form\Type\ContainerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class RedirectType
 * @package AdminBundle\Form\Seo
 */
class RedirectType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $container = $builder->create('container', ContainerType::class);

        $container
            ->add('status', ChoiceType::class, [
                'label' => 'Status',
                'choices' => [
                    '301 Permanent verhuisd' => 301,
                    '302 Gevonden' => 302,
                    '307 Tijdelijke redirect' => 307,
                    '410 Inhoud verwijderd' => 410,
                    '451 Niet beschikbaar vanwege jurisiche redenen' => 451,
                ],
                'select2' => [],
            ])
            ->add('oldUrl', TextType::class, [
                'label' => 'Oude url',
            ])
            ->add('newUrl', TextType::class, [
                'label' => 'Url *',
                'required' => false,
            ])
            ->add('domains', EntityType::class, [
                'label' => 'Domeinen',
                'class' => Domain::class,
                'required' => false,
                'multiple' => true,
                'group_by' => function (Domain $domain) {
                    return $domain->getSite()->translate()->getDescription();
                },
                'multiselect' => [],
            ]);

        $builder->add($container);
    }
}
