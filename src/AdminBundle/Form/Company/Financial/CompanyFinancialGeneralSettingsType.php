<?php

namespace AdminBundle\Form\Company\Financial;

use AppBundle\DBAL\Types\CompanyPriceDisplayModeType;
use AppBundle\Entity\Payment\Paymentmethod;
use AppBundle\Entity\Relation\Company;
use AppBundle\Form\Type\MoneyType;
use AppBundle\Services\Parameter\ParameterService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyFinancialGeneralSettingsType
 * @package AdminBundle\Form\Company\Financial
 */
class CompanyFinancialGeneralSettingsType extends AbstractType
{

    /**
     * @var ParameterService
     */
    private $parameterService;

    /**
     * CompanyFinancialGeneralSettingsType constructor.
     * @param ParameterService $parameterService
     */
    public function __construct(ParameterService $parameterService)
    {
        $this->parameterService = $parameterService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('creditLimit', MoneyType::class, [
                'required' => false,
                'label' => 'Krediet limiet',
                'attr' => [
                    'placeholder' => $this->parameterService->setEntity()->getValue('company_credit_limit')
                ]
            ])
            ->add('priceDisplayMode', ChoiceType::class, [
                'label' => 'Prijs tonen als',
                'choices' => CompanyPriceDisplayModeType::getChoices()
            ])
            ->add('paymentmethods', EntityType::class, [
                'label' => 'Beschikbare betaalmethodes',
                'class' => Paymentmethod::class,
                'choice_label' => function(Paymentmethod $paymentmethod) {
                    return $paymentmethod->translate()->getDescription();
                },
                'select2' => [
                    'placeholder' => 'Selecteer betaalmethodes',
                ],
                'multiple' => true,
                'required' => false,
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'data_class' => Company::class
            ])
        ;
    }
}
