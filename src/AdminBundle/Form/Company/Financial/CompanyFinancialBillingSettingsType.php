<?php

namespace AdminBundle\Form\Company\Financial;

use AdminBundle\Form\Relation\CompanyAddressFormType;
use AdminBundle\Form\Finance\CompanyInvoiceSettingsFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Relation\Company;

/**
 * Class CompanyFinancialBillingSettingsType
 * @package AdminBundle\Form\Company\Financial
 */
class CompanyFinancialBillingSettingsType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('invoiceAddress', CompanyAddressFormType::class, [
                'label' => false,
            ])
            ->add('iban', TextType::class, [
                'label' => 'IBAN',
                'required' => false,
            ])
            ->add('bic', TextType::class, [
                'label' => 'BIC',
                'required' => false,
            ])
            ->add('bankAccountHolder', TextType::class, [
                'label' => 'Naam rekeninghouder',
                'required' => false,
            ])
            ->add('invoiceEmail', EmailType::class, [
                'label' => 'E-mailadres (facturatie)',
                'required' => false,
            ])->add('companyInvoiceSettings', CompanyInvoiceSettingsFormType::class, [
                'label'=> false,
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'data_class' => Company::class
            ])
        ;
    }
}
