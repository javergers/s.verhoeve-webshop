<?php

namespace AdminBundle\Form\Company;

use AdminBundle\Form\Type\MediaType;
use AppBundle\Entity\Relation\CompanySiteLogo;
use AppBundle\Form\Type\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanySiteLogoFormType
 * @package AdminBundle\Form\Company
 */
class CompanySiteLogoFormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('path', ImageType::class, [
                'label' => false,
                'instance' => 'form',
                'enable' => true,
                'media' => [
                    'type' => MediaType::TYPE_IMAGE,
                    'thumbnail' => true,
                    'upload' => true,
                    'dimensions' => [
                        'min' => 'company-logo',
                    ],
                ],
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CompanySiteLogo::class,
        ]);
    }

}
