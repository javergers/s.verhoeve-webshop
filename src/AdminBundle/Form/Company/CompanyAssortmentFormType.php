<?php

namespace AdminBundle\Form\Company;

use AdminBundle\Form\Catalog\AssortmentProductType;
use AdminBundle\Form\Catalog\AssortmentType;
use AdminBundle\Form\Catalog\Type\AssortmentProductCollectionType;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\SlugType;
use AppBundle\Form\Type\TabType;
use AppBundle\Form\Type\TranslationsType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyAssortmentFormType
 * @package AdminBundle\Form\Company
 */
class CompanyAssortmentFormType extends AssortmentType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->remove('tab_accessible');
        $builder->get('container')->remove('sites');
        $builder->get('container')->remove('content');
        $builder->get('container')->remove('contentSize');
        $builder->get('container')->remove('contentPosition');
    }
}
