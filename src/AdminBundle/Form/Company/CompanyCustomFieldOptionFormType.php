<?php

namespace AdminBundle\Form\Company;

use AppBundle\Entity\Relation\CompanyCustomFieldOption;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CompanyCustomFieldOptionFormType
 * @package AdminBundle\Form\Company
 */
class CompanyCustomFieldOptionFormType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('position', NumberType::class, [
                'label' => 'Positie',
            ])
            ->add('optionKey', TextType::class, [
                'label' => 'Waarde',
            ])
            ->add('optionValue', TextType::class, [
                'label' => 'Weergavenaam',
            ]);

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CompanyCustomFieldOption::class,
        ]);
    }

}
