<?php

namespace AdminBundle\Form\Company;

use AppBundle\Entity\Relation\CompanyEstablishment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyEstablishmentTableType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    //  public function buildList(ListBuilderInterface $builder, array $options)
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('establishmentNumber')
            ->add('name', TextType::class)
            ->add('address', TextType::class)
            ->add('id', HiddenType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CompanyEstablishment::class,
        ]);
    }
}
