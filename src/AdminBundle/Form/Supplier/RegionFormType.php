<?php

namespace AdminBundle\Form\Supplier;

use AppBundle\Entity\Geography\Postcode;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyPreferredSupplier;
use AppBundle\Entity\Supplier\DeliveryArea;
use AppBundle\Entity\Supplier\SupplierGroup;
use AppBundle\Manager\Supplier\DeliveryAreaManager;
use AppBundle\Services\DeliveryArea\DeliveryAreaService;
use ArrayIterator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\PersistentCollection;
use Exception;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class RegionFormType
 * @package AdminBundle\Form\Supplier
 */
class RegionFormType extends AbstractRegionFormType
{
    /** @var ArrayCollection */
    protected $filteredDeliveryAreas;

    /** @var EntityManagerInterface */
    protected $entityManager;

    /**
     * RegionFormType constructor.
     *
     * @param RequestStack $requestStack
     * @param DeliveryAreaService $deliveryAreaService
     * @param DeliveryAreaManager $deliveryAreaManager
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        RequestStack $requestStack,
        DeliveryAreaService $deliveryAreaService,
        DeliveryAreaManager $deliveryAreaManager,
        EntityManagerInterface $entityManager
    ) {
        parent::__construct($requestStack, $deliveryAreaService, $deliveryAreaManager, $entityManager);

        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('deliveryAreas', CollectionType::class, [
                'entry_type' => DeliveryAreaRegionCollectionType::class,
                'entry_options' => [
                    'delivery_intervals' => $this->deliveryAreaService->getDeliveryIntervalList(),
                ],
                'label' => 'Leveranciers',
                'by_reference' => false,
                'collection' => [
                    'columns' => [
                        [
                            'label' => 'Volgorde *',
                        ],
                        [
                            'label' => 'Voorkeur',
                        ],
                        [
                            'label' => 'Leverancier *',
                        ],
                        [
                            'label' => 'Bezorgkosten *',
                        ],
                        [
                            'label' => 'Bezorgkosten Excl. BTW *',
                        ],
                        [
                            'label' => 'Bezorg BTW',
                        ],
                        [
                            'label' => 'Aannametijd *',
                        ],
                    ],
                    'sortable' => true,
                ],
                'allow_add' => true,
                'allow_delete' => true,
            ]);

        $builder->addEventListener(FormEvents::POST_SET_DATA, [$this, 'filterBySupplierGroup']);

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {

            /** @var ArrayIterator $currentDeliveryAreas */
            $currentDeliveryAreas = $event->getForm()->get('deliveryAreas')->getData()->getIterator();
            $currentDeliveryAreas->uasort(function (DeliveryArea $deliveryArea1, DeliveryArea $deliveryArea2) {
                /** @var CompanyPreferredSupplier $companyPreferredSupplier1 */
                $preferredSupplier1 = $this->deliveryAreaService->getPreferredSupplier($deliveryArea1);
                /** @var CompanyPreferredSupplier $companyPreferredSupplier2 */
                $preferredSupplier2 = $this->deliveryAreaService->getPreferredSupplier($deliveryArea2);
                $position1 = $preferredSupplier1 ? $preferredSupplier1->getPosition() : 99999;
                $position2 = $preferredSupplier2 ? $preferredSupplier2->getPosition() : 99999;
                return $position1 <=> $position2;
            });

            $event->getForm()->get('deliveryAreas')->setData($currentDeliveryAreas);
        });

        $builder->addEventListener(FormEvents::POST_SET_DATA, [$this, 'setCollectionMappedFieldValues']);

        $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'setFilteredDeliveryAreas']);

        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'markPreferredSuppliers']);

        $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'appendFilteredDeliveryAreas']);
    }

    /**
     * @param FormEvent $event
     *
     * @throws Exception
     */
    public function setCollectionMappedFieldValues(FormEvent $event)
    {
        foreach ($event->getForm()->get('deliveryAreas') as $collectionField) {

            /** @var DeliveryArea $deliveryArea */
            $deliveryArea = $collectionField->getData();

            /** @var CompanyPreferredSupplier */
            $preferredSupplier = $this->deliveryAreaService->getPreferredSupplier($deliveryArea);

            // Check if supplier is in requested supplier group.
            /** @var SupplierGroup $supplierGroup */
            $supplierGroup = $this->deliveryAreaService->getRequestedSupplierSupplierGroup($deliveryArea->getCompany());

            if ($preferredSupplier) {
                $collectionField->get('position')->setData($preferredSupplier->getPosition());
                $collectionField->get('is_preferred')->setData(true);
            }

            if ($supplierGroup !== null) {
                $postcode = $deliveryArea->getPostcode();

                [$price, $priceIncl, $vat] = $this->deliveryAreaService->calcRegionPrices($supplierGroup, $postcode,
                    $deliveryArea, false);

                $collectionField->get('deliveryPriceInclVat')->setData($priceIncl);
                $collectionField->get('deliveryPrice')->setData($price);
                $collectionField->get('deliveryVat')->setData($vat);
            }
        }
    }

    /**
     * @param FormEvent $event
     *
     * @throws Exception
     */
    public function filterBySupplierGroup(FormEvent $event)
    {
        $data = $event->getForm()->get('deliveryAreas')->getData();

        /** @var PersistentCollection $data */
        $data = $data->filter(function (DeliveryArea $deliveryArea) {

            $supplier = $deliveryArea->getCompany();

            if ($supplier === null) {
                return false;
            }

            foreach ($deliveryArea->getCompany()->getSupplierGroups() as $supplierGroup) {
                if ($supplierGroup->getName() === $this->getRequest()->get('supplierGroup')) {
                    return true;
                }
            }
            return false;
        });

        $event->getForm()->get('deliveryAreas')->setData($data);
    }

    /**
     * @param FormEvent $event
     *
     * @throws Exception
     */
    public function setFilteredDeliveryAreas(FormEvent $event)
    {
        /** @var Postcode $postcode */
        $postcode = $event->getForm()->getData();

        $this->filteredDeliveryAreas = $postcode->getDeliveryAreas()->filter(function (DeliveryArea $deliveryArea) {
            $supplier = $deliveryArea->getCompany();
            if ($supplier === null) {
                return false;
            }
            foreach ($supplier->getSupplierGroups() as $supplierGroup) {
                if ($supplierGroup->getName() !== $this->getRequest()->get('supplierGroup')) {
                    return true;
                }
            }
            return false;
        });
    }

    /**
     * @param FormEvent $event
     */
    public function appendFilteredDeliveryAreas(FormEvent $event)
    {
        /** @var Postcode $postcode */
        $postcode = $event->getData();
        foreach ($this->filteredDeliveryAreas as $deliveryArea) {
            $postcode->addDeliveryArea($deliveryArea);
        }
    }

    /**
     * @param FormEvent $event
     *
     * @return void
     */
    public function markPreferredSuppliers(FormEvent $event): void
    {
        /** @var Postcode $postcode */
        $postcode = $event->getData();

        $regionForm = $this->getRequest()->get('region_form');

        if (null !== $regionForm && false !== isset($regionForm['deliveryAreas'])) {
            $formRequest = $regionForm['deliveryAreas'];

            $preferredSettings = $this->getPreferredSettings($formRequest);

            $this->deliveryAreaManager->markPreferredSuppliers($postcode->getDeliveryAreas(), $preferredSettings);
        }
    }

    /**
     * @param array $formRequest
     *
     * @return ArrayCollection
     */
    private function getPreferredSettings(array $formRequest): ArrayCollection
    {
        $companyRepository = $this->entityManager->getRepository(Company::class);

        $isPreferred = [];
        $preferredSettings = new ArrayCollection();
        $orderIndex = 0;

        foreach ($formRequest as $k => $data) {

            $orderIndex++;

            $isPreferred[$k] = (isset($data['is_preferred']) ? true : false);

            if (!$isPreferred[$k]) {
                continue;
            }

            $company = $companyRepository->find((int)$data['company']);

            $preferredSettings->add([
                'companyPreferredSupplier' => $company,
                'company' => null, // null = Topgeschenken (view).
                'position' => $orderIndex,
            ]);
        }

        return $preferredSettings;
    }
}
