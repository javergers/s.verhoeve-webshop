<?php

namespace AdminBundle\Form\Supplier;

use AppBundle\Entity\Geography\Postcode;
use AppBundle\Entity\Supplier\DeliveryArea;
use AppBundle\Exceptions\LinkedPreferredSuppliersException;
use AppBundle\Manager\Supplier\DeliveryAreaManager;
use AppBundle\Services\DeliveryArea\DeliveryAreaService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class AbstractRegionFormType
 * @package AdminBundle\Form\Supplier
 */
abstract class AbstractRegionFormType extends AbstractType
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var DeliveryAreaManager
     */
    protected $deliveryAreaManager;

    /**
     * @var DeliveryAreaService
     */
    protected $deliveryAreaService;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * AbstractRegionFormType constructor.
     *
     * @param RequestStack $requestStack
     * @param DeliveryAreaService $deliveryAreaService
     * @param DeliveryAreaManager $deliveryAreaManager
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        RequestStack $requestStack,
        DeliveryAreaService $deliveryAreaService,
        DeliveryAreaManager $deliveryAreaManager,
        EntityManagerInterface $entityManager
    ) {
        $this->requestStack = $requestStack;
        $this->deliveryAreaService = $deliveryAreaService;
        $this->deliveryAreaManager = $deliveryAreaManager;
        $this->requestStack = $requestStack;
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SUBMIT, [$this, 'removePreferredSuppliers']);
    }

    /**
     * @param FormEvent $event
     *
     * @return void
     */
    public function removePreferredSuppliers(FormEvent $event): void
    {
        $regionForm = $this->getRequest()->get('region_form');

        $requestDeliveryAreas = [];
        if (null !== $regionForm && isset($regionForm['deliveryAreas'])) {
            $requestDeliveryAreas = $regionForm['deliveryAreas'];
        }

        /** @var Postcode $postcode */
        $postcode = $event->getForm()->getData();

        $deletedDeliveryAreas = $this->listDeletedDeliveryAreas($postcode->getDeliveryAreas(), $requestDeliveryAreas);

        foreach ($deletedDeliveryAreas as $deliveryArea) {
            try {
                $this->deliveryAreaManager->removePreferredSuppliers($deliveryArea);
            } catch (LinkedPreferredSuppliersException $e) {
                $event->getForm()->addError(new FormError($e->getMessage()));
            }
        }
    }

    /**
     * @param Collection $deliveryAreas
     * @param array $request
     *
     * @return ArrayCollection
     */
    private function listDeletedDeliveryAreas(Collection $deliveryAreas, array $request = [])
    {
        $deleted = new ArrayCollection();

        /** @var DeliveryArea[] $deliveryAreas */
        foreach ($deliveryAreas as $k => $deliveryArea) {

            // ignore if delivery area is not deleted.
            if (isset($request[$k])) {
                continue;
            }

            $deleted->add($deliveryArea);
        }

        return $deleted;
    }

    /**
     * @return Request
     */
    protected function getRequest(): Request
    {
        $request = null;

        if (null !== $this->requestStack) {
            $request = $this->requestStack->getCurrentRequest();
        }

        if(null !== $request) {
            return $request;
        }

        throw new RuntimeException('Request not defined');
    }
}
