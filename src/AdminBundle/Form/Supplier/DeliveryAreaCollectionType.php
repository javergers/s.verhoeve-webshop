<?php

namespace AdminBundle\Form\Supplier;

use AppBundle\Entity\Geography\Postcode;
use AppBundle\Entity\Supplier\DeliveryArea;
use AppBundle\Form\Type\AbstractFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DeliveryAreaCollectionType
 * @package AdminBundle\Form\Supplier
 */
class DeliveryAreaCollectionType extends AbstractFormType
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * DeliveryAreaCollectionType constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('postcode', EntityType::class, [
                'class' => Postcode::class,
                'choices' => [],
                'choice_label' => function (Postcode $postcode) {
                    return $postcode->getLabel();
                },
                'attr' => [
                    'readonly' => true,
                ],
                'mapped' => false,
            ])
            ->add('deliveryPriceInclVat', MoneyType::class, [
                'required' => true,
                'scale' => 2,
                'mapped' => false,
                'attr' => [
                    'class' => 'field-price-incl',
                ],
            ])
            ->add('deliveryPrice', MoneyType::class, [
                'required' => true,
                'scale' => 3,
                'attr' => [
                    'class' => 'field-price-excl',
                ],
            ])
            ->add('deliveryVat', TextType::class, [
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'class' => 'field-vat',
                ],
            ])
            ->add('deliveryInterval', ChoiceType::class, [
                'choices' => $options['delivery_intervals'],
                'required' => true,
            ]);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {

            $postcodeId = $event->getForm()->get('postcode')->getData();

            $postcode = $this->entityManager->getRepository(Postcode::class)->find($postcodeId);

            $event->getData()->setPostcode($postcode);
        });

        $builder->get('postcode')->resetViewTransformers();
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefined([
            'delivery_intervals',
        ]);

        $resolver->setAllowedTypes('delivery_intervals', ['array', 'null']);

        $resolver->setDefaults([
            'data_class' => DeliveryArea::class,
            'delivery_intervals' => [],
        ]);
    }
}
