<?php

namespace AdminBundle\Form\Supplier;

use AdminBundle\Form\CollectionListType;
use AppBundle\Entity\Supplier\PickupLocation;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\TimeslotType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PickupLocationType
 * @package AdminBundle\Form\Supplier
 */
class PickupLocationType extends AbstractFormType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var PickupLocation $pickupLocation */
        $pickupLocation = $builder->getData();
        $container = $builder->create('pickup_location_container', ContainerType::class, []);

        $column = $builder->create('pickup_location_general_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Afhaal instellingen voor vestiging ' . $pickupLocation->getCompanyEstablishment()->getName(),
            'icon' => 'key',
        ]);

        $hoursContainer = $builder->create('opening_hours_container', ContainerType::class, []);
        $hoursColumn = $hoursContainer->create('opening_hours_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Afhaal timeslots voor vestiging',
            'icon' => 'key',
        ]);

        $hoursColumn->add('timeslots', CollectionListType::class, [
            'entry_type' => TimeslotType::class,
            'by_reference' => false,
        ]);

        $hoursContainer->add($hoursColumn);
        $builder->add($hoursContainer);

        $column->add('preparationTime', IntegerType::class, [
            'label' => 'Voorbereidings tijd',
            'required' => true,
            'mapped' => true,
        ]);

        $container->add($column);
        $builder->add($container);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PickupLocation::class,
        ]);
    }
}
