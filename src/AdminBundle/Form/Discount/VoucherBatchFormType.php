<?php

namespace AdminBundle\Form\Discount;

use AppBundle\DBAL\Types\VoucherUsageType;
use AppBundle\Entity\Discount\Discount;
use AppBundle\Entity\Discount\VoucherBatch;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\ColumnType;
use AppBundle\Form\Type\ContainerType;
use AppBundle\Form\Type\DateType;
use AppBundle\Services\Discount\VoucherService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class VoucherBatchFormType
 * @package AdminBundle\Form\Discount
 */
class VoucherBatchFormType extends AbstractFormType
{
    /**
     * @var VoucherService
     */
    private $voucherService;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * VoucherBatchFormType constructor.
     *
     * @param VoucherService $voucherService
     * @param EntityManagerInterface $entityManager
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(VoucherService $voucherService, EntityManagerInterface $entityManager, TokenStorageInterface $tokenStorage)
    {
        $this->voucherService = $voucherService;
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $container = $builder->create('container', ContainerType::class);
        $column = $builder->create('category_column', ColumnType::class, [
            'column_layout' => ColumnType::COLUMN_WIDTH_12,
            'label' => 'Batch aanmaken',
        ]);

        $column
            ->add('name', TextType::class, [
                'label' => 'Naam',
                'required' => true,
            ])
            ->add('prefix', TextType::class, [
                'label' => 'Voorvoegsel',
                'required' => false,
                'help_label' => 'Alle vouchers worden voorzien met deze reeks, mag niet meer dan 5 tekens bevatten.',
            ])
            ->add('quantity', IntegerType::class, [
                'label' => 'Aantal',
                'required' => true,
                'mapped' => false,
            ])
            ->add('discount', EntityType::class, [
                'class' => Discount::class,
                'required' => true,
                'label' => 'Korting',
                'select2' => [
                    'placeholder' => 'Selecteer korting',
                ],
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('discount')
                        ->where('discount.rule IS NOT NULL');
                },
                'choice_label' => function ($discount) {
                    return $discount->getRule()->getDescription() . ' (' . $discount->getType() . ': ' . $discount->getValue() . ')';
                },
                'mapped' => false,
            ])
            ->add('usage', ChoiceType::class, [
                'label' => 'Gebruik',
                'choices' => VoucherUsageType::getChoices(),
                'placeholder' => 'Keuze...',
                'required' => true,
                'mapped' => false,
            ])
            ->add('limit', IntegerType::class, [
                'label' => 'Limiet',
                'required' => true,
                'mapped' => false,
            ])
            ->add('validFrom', DateType::class, [
                'label' => 'Vanaf',
                'widget' => 'choice',
                'required' => true,
            ])
            ->add('validUntil', DateType::class, [
                'label' => 'Tot',
                'widget' => 'choice',
                'required' => true,
            ]);

        $container->add($column);
        $builder->add($container);

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $formPrefix = $event->getForm()->get('container')->get('category_column');

            /** @var VoucherBatch $batch */
            $batch = $event->getData();
            $voucherData = $batch->getMetadata();

            $voucherData['user'] = $this->tokenStorage->getToken()->getUser()->getId();
            $voucherData['discount'] = $formPrefix->get('discount')->getData()->getId();
            $voucherData['usage'] = $formPrefix->get('usage')->getData();
            $voucherData['limit'] = $formPrefix->get('limit')->getData();
            $voucherData['validFrom'] = $formPrefix->get('validFrom')->getData()->format('Y-m-d');
            $voucherData['validUntil'] = $formPrefix->get('validUntil')->getData()->format('Y-m-d');

            if($formPrefix->has('quantity')) {
                $voucherData['quantity'] = $formPrefix->get('quantity')->getData();
            }

            $batch->setMetadata($voucherData);

            if ($batch !== null && $batch->getId() !== null) {
                unset($voucherData['quantity'], $voucherData['user']);

                $this->voucherService->editBatch($batch, $voucherData);
            } else {
                $quantity = $formPrefix->get('quantity')->getData();
                $this->voucherService->generateBatch($batch, $quantity, $voucherData);
            }
        });

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm()->get('container')->get('category_column');

            if ($data !== null && $data->getId() !== null) {
                $discount = $this->entityManager->getRepository(Discount::class)->find($data->getMetadata()['discount']);
                $form->get('discount')->setData($discount);
                $form->get('usage')->setData($data->getMetadata()['usage']);
                $form->get('limit')->setData($data->getMetadata()['limit']);

                $form->remove('quantity');
                $form->remove('prefix');
            }
        });
    }
}
