<?php

namespace AdminBundle\Form\Sales;

use AppBundle\Form\Type\AbstractFormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotEqualTo;

class OrderCancelType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        void($options);

        $builder
            ->add('reason', TextareaType::class, [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Reden',
                ],
                'constraints' => [
                    new Length(['min' => 3]),
                    new NotEqualTo('test'),
                ],
            ])
            ->add('company', HiddenType::class, [
                'attr' => [
                    'name' => 'company',
                ],
            ]);;
    }
}
