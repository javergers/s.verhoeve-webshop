<?php

namespace AdminBundle\Form\Sales;

use AppBundle\Entity\Order\Order;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompositionOrderType extends OrderType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('lines', CollectionType::class, [
            'by_reference' => false,
            'entry_type' => OrderLineType::class,
            'delete_empty' => true
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}