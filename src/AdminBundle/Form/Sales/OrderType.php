<?php

namespace AdminBundle\Form\Sales;

use AppBundle\Entity\Order\Order;
use AppBundle\Entity\Order\OrderLine;
use AppBundle\Form\Type\AbstractFormType;
use AppBundle\Form\Type\DateType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class OrderType
 * @package AdminBundle\Form\Sales
 */
class OrderType extends AbstractFormType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('deliveryAddressCompanyName', TextType::class, [
                'widget_horizontal_label_class' => 'col-xs-3',
                'widget_form_element_class' => 'col-xs-9',
                'label' => 'Bedrijfsnaam',
                'required' => false,
            ])
            ->add('deliveryAddressAttn', TextType::class, [
                'widget_horizontal_label_class' => 'col-xs-3',
                'widget_form_element_class' => 'col-xs-9',
                'label' => 'Ontvanger',
            ])
            ->add('deliveryAddressStreet', TextType::class, [
                'widget_horizontal_label_class' => 'col-xs-3',
                'widget_form_element_class' => 'col-xs-9',
            ])
            ->add('deliveryAddressNumber', TextType::class, [
                'widget_horizontal_label_class' => 'col-xs-3',
                'widget_form_element_class' => 'col-xs-9',
            ])
            ->add('deliveryAddressNumberAddition', TextType::class, [
                'widget_horizontal_label_class' => 'col-xs-3',
                'widget_form_element_class' => 'col-xs-9',
                'required' => false,
            ])
            ->add('deliveryAddressPostcode', TextType::class, [
                'widget_horizontal_label_class' => 'col-xs-3',
                'widget_form_element_class' => 'col-xs-9',
            ])
            ->add('deliveryAddressCity', TextType::class, [
                'widget_horizontal_label_class' => 'col-xs-3',
                'widget_form_element_class' => 'col-xs-9',
            ])
            ->add('deliveryAddressCountry', TextType::class, [
                'widget_horizontal_label_class' => 'col-xs-3',
                'widget_form_element_class' => 'col-xs-9',
                'label' => 'Land',
                'static_text' => true,
                'mapped' => false,
            ])
            ->add('deliveryAddressPhoneNumber', TextType::class, [
                'widget_horizontal_label_class' => 'col-xs-3',
                'widget_form_element_class' => 'col-xs-9',
                'label' => 'Telefoonnummer',
                'required' => false,
            ])
            ->add('supplierRemark', TextareaType::class, [
                'label' => 'Opmerking voor leverancier',
                'widget_horizontal_label_class' => '',
                'required' => false,
            ])
            ->add('deliveryRemark', TextareaType::class, [
                'label' => 'Opmerking voor bezorger',
                'widget_horizontal_label_class' => '',
                'required' => false,
            ])
            ->add('invoiceReference', TextType::class, [
                'label' => 'Factuurreferentie',
                'required' => false,
                'widget_horizontal_label_class' => 'col-xs-3',
                'widget_form_element_class' => 'col-xs-9',
            ])
            ->add('deliveryDate', DateType::class, [
                'label' => 'Bezorgdatum',
                'widget_horizontal_label_class' => 'col-xs-3',
                'widget_form_element_class' => 'col-xs-9',
            ])
            ->add('deliveryTimeWindow', ChoiceType::class, [
                'label' => 'Tijdsvak',
                'mapped' => false,
                'widget_horizontal_label_class' => 'col-xs-3',
                'widget_form_element_class' => 'col-xs-9',
                'choices' => [
                    '09:00 - 18:00' => 1,
                ],
                'attr' => [
                    'style' => 'width: 225px;',
                ],
            ])
        ;

        $builder->addEventListener(FormEvents::PRE_SET_DATA, static function (FormEvent $event) use ($options) {
            /** @var Order $order */
            $order = $event->getData();

            $orderLines = new ArrayCollection();

            // Get regular products and their children
            foreach ($order->getLines() as $line) {
                // Skip child lines
                if ($line->getParent() !== null) {
                    continue;
                }

                // Skip delivery costs for now
                if ($line->getProduct()->isTransportType()) {
                    continue;
                }

                $orderLines->add($line);

                foreach ($line->getChildren() as $childLine) {
                    $orderLines->add($childLine);
                }
            }

            // Get other lines (for now only delivery costs)
            foreach ($order->getLines() as $line) {
                // Skip child lines
                if ($line->getParent() !== null) {
                    continue;
                }

                // Skip general products
                if (false === $line->getProduct()->isTransportType()) {
                    continue;
                }

                $orderLines->add($line);
            }

            $order->setLines($orderLines);
        });

        $builder->addEventListener(FormEvents::POST_SET_DATA, static function (FormEvent $event) {
            /** @var Order $order */
            $order = $event->getData();

            /** @var Form $form */
            $form = $event->getForm();

            $site = $order->getOrderCollection()->getSite();

            // Remove fields for delivery if the order is a pickup order.
            if ($order->getPickupAddress() !== null) {
                $form->remove('deliveryAddressCompanyName');
                $form->remove('deliveryAddressAttn');
                $form->remove('deliveryAddressStreet');
                $form->remove('deliveryAddressNumber');
                $form->remove('deliveryAddressNumberAddition');
                $form->remove('deliveryAddressPostcode');
                $form->remove('deliveryAddressCity');
                $form->remove('deliveryAddressCountry');
                $form->remove('deliveryAddressPhoneNumber');
            }

            // If there is no delivery date present in the form remove the field from the form
            if (!$order->getDeliveryDate() && !$site->getDeliveryDateMethod()) {
                $event->getForm()->remove('deliveryDate');
                $event->getForm()->remove('deliveryTimeWindow');
            }
        });

        $builder->addEventListener(FormEvents::SUBMIT, static function (FormEvent $event) {
            $data = $event->getData();

            $previousLine = null;

            /** @var OrderLine $line */
            foreach ($data->getLines() as $line) {
                $metadata = $line->getMetadata();

                $parentIndex = null;
                if(isset($metadata['parentIndex'])) {
                    $parentIndex = $metadata['parentIndex'];
                    unset($metadata['parentIndex']);
                    $line->setMetadata($metadata);
                }

                if (true === $line->getProduct()->isRegularProduct()) {
                    $previousLine = $line;
                }

                if($parentIndex !== null) {
                    $previousLine = $data->getLines()[$parentIndex];
                }

                if (null !== $previousLine && true === $line->getProduct()->isPersonalizationProduct()) {
                    $line->setParent($previousLine);

                    $previousLine->addChild($line);
                }
            }
        });
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Order::class,
        ]);
    }
}
