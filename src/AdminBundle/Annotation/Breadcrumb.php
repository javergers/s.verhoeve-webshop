<?php

namespace AdminBundle\Annotation;

/**
 * Class Breadcrumb
 *
 * @package AdminBundle\Annotation
 *
 * @Annotation
 * @Target({"METHOD"})
 */
class Breadcrumb
{

    /**
     * @var string|null
     */
    private $parentRoute;

    /**
     * @param array $data An array of key/value parameters
     *
     * @throws \BadMethodCallException
     */
    public function __construct(array $data)
    {
        if (isset($data['value'])) {
            $data['path'] = $data['value'];
            unset($data['value']);
        }

        foreach ($data as $key => $value) {
            $method = 'set' . str_replace('_', '', $key);

            if (!method_exists($this, $method)) {
                throw new \BadMethodCallException(sprintf('Unknown property "%s" on annotation "%s".', $key,
                    get_class($this)));
            }

            $this->$method($value);
        }
    }

    /**
     * @param string $parentRoute
     */
    public function setParentRoute($parentRoute = null)
    {
        $this->parentRoute = $parentRoute;
    }

    /**
     * @return string|null
     */
    public function getParentRoute()
    {
        return $this->parentRoute;
    }
}