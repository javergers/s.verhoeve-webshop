<?php

namespace AdminBundle\Factory;

use AdminBundle\Decorator\AuditLogDecorator;
use DataDog\AuditBundle\Entity\AuditLog;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class AuditLogDecoratorFactory
{
    use ContainerAwareTrait;

    /**
     * @param AuditLog $log
     * @return AuditLogDecorator
     */
    public function get(AuditLog $log)
    {
        $decoratedLog = new AuditLogDecorator();
        $decoratedLog->setContainer($this->container);
        $decoratedLog->set($log);

        return $decoratedLog;
    }
}