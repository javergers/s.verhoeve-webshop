<?php

namespace AdminBundle\Entity\Settings;

use Doctrine\ORM\Mapping as ORM;
use RuleBundle\Entity\Rule;

/**
 * Class WmsOrderTypeDatatable
 * @package AppBundle\ThirdParty\Wics\Entity
 *
 * @ORM\Entity()
 */
class WmsOrderType
{
    /**
     * @var int $id
     *
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $code
     *
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    protected $code;

    /**
     * @var int $priority
     *
     * @ORM\Column(type="integer", length=3, nullable=true)
     */
    protected $priority;

    /**
     * @var Rule $rule
     *
     * @ORM\ManyToOne(targetEntity="RuleBundle\Entity\Rule")
     */
    protected $rule;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $code
     * @return mixed
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $code;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $priority
     * @return $this
     */
    public function setPriority(int $priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param Rule $rule
     * @return $this
     */
    public function setRule(Rule $rule)
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * @return Rule
     */
    public function getRule()
    {
        return $this->rule;
    }
}
