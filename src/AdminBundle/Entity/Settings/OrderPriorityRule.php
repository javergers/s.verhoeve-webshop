<?php

namespace AdminBundle\Entity\Settings;

use Doctrine\ORM\Mapping as ORM;
use RuleBundle\Entity\Rule;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class OrderPriorityRule
 * @package AdminBundle\Entity\Settings
 *
 * @ORM\Entity()
 */
class OrderPriorityRule
{
    /**
     * @var int $id
     *
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int $priority
     *
     * @ORM\Column(type="integer", length=3, nullable=true)
     * @Assert\Length(min=1, max=100, minMessage="De waarde moet minimaal {{ limit }} zijn", maxMessage="De waarde mag maximaal {{ limit }} zijn")
     */
    protected $priority;

    /**
     * @var Rule $rule
     *
     * @ORM\ManyToOne(targetEntity="RuleBundle\Entity\Rule")
     */
    protected $rule;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $priority
     * @return $this
     */
    public function setPriority(int $priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param Rule $rule
     * @return $this
     */
    public function setRule(Rule $rule)
    {
        $this->rule = $rule;

        return $this;
    }

    /**
     * @return Rule
     */
    public function getRule()
    {
        return $this->rule;
    }
}
