$(function () {
    initializeDatatables();

    var discountPanel = $('#discount-panel');

    discountPanel
        .on('click', 'a[data-type="add"]', openWindow)
        .on('click', 'a[data-type="edit"]', openWindow);

    function openWindow(e) {
        e.preventDefault();

        var obj = $(e.currentTarget);
        var url = obj.attr('href') + '?discount=' + obj.closest('#discount-panel').attr('data-discount-id');
        obj.attr('href', url);
        obj.attr('data-hide-callback', 'reloadTables');

        openContent(obj);
    }
});

function reloadTables() {
    var discountPanel = $('#discount-panel');
    var discountPanelDatatable = discountPanel.find('.discountPanel').find('table').attr('id');

    $('#' + discountPanelDatatable).dataTable().api().ajax.reload(null, false);
}