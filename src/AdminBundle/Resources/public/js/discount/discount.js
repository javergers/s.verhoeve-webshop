$(function () {
    var discountDescription = $('#discount_description');
    var discountAmount = $('#discount_amount');
    var discountType = $('#discount_type');
    var discountFieldPanel = $('#discount_field_panel');
    var discountInformationPanel = $('#discount_information_panel');
    var discountOperator = $('#discount_operator');
    var ruleValueSelect = $('#ruleValueSelect');
    var ruleValueText = $('#ruleValueText');
    var wizard = $('#wizard');
    var wizardSubmit = $('#wizard_submit');
    var filters = JSON.parse(wizard.attr('data-filters'));
    var selectedFilter = null;
    var selectedValueField = null;

    var operators = {
        'equal': 'Gelijk aan',
        'not_equal': 'Niet gelijk aan',
        'less': 'Minder dan',
        'less_or_equal': 'Minder of gelijk aan',
        'greater': 'Meer dan',
        'greater_or_equal': 'Meer of gelijk aan',
        'between': 'Tussen',
        'not_between': 'Niet tussen',
        'is_null': 'Is leeg',
        'is_not_null': 'Is niet leeg',
        'begins_with': 'Begint met',
        'ends_with': 'Eindigt op'
    }

    $('a[data-action="create-rule"]').on('click', function (e) {
        e.preventDefault();

        var elm = $(this);
        new RuleCreate(elm.attr('href'), function () {
            window.location.replace(elm.attr('data-refresh-url'));
        });
    });

    $('#discount_type').on('change', function () {
        var state = $(this).val();
        discountFieldPanel.toggle(!!state);
    });

    $('#discount_field').on('change', function () {
        var state = $(this).val();
        discountInformationPanel.toggle(!!state);
        wizardSubmit.toggle(!!state);
        discountOperator.html('');

        ruleValueText.hide();
        ruleValueSelect.hide();

        $.each(filters, function (key, filter) {
            if (filter['id'] == '__result.' + state) {
                selectedFilter = filter;

                selectedValueField = ruleValueText;
                ruleValueText.show();

                //fill the select input with compatible operators
                $.each(filter['operators'], function (opKey, operator) {
                    discountOperator.append($('<option>', {
                        value: operator,
                        text: operators[operator]
                    }));
                });

                if (ruleValueSelect.hasClass('select2-hidden-accessible')) {
                    ruleValueSelect.select2('destroy');
                }

                //set up optional ajax autofill
                if (typeof filter['data']['ajax'] !== 'undefined') {
                    ruleValueSelect.show();
                    ruleValueText.hide();

                    ruleValueSelect.select2({
                        ajax: {
                            url: wizard.attr('data-autocomplete-url'),
                            method: 'POST',
                            data: function (params) {
                                var queryParameters = filter.data.ajax;
                                queryParameters.term = params.term;

                                return queryParameters;
                            },
                            delay: 250,
                            processResults: function (data, params) {
                                return {
                                    results: data
                                };
                            },
                        },
                        minimumInputLength: 3
                    }).trigger('change.select2');

                    selectedValueField = ruleValueSelect;
                }
            }
        });
    });

    wizardSubmit.on('click', function (e) {
        e.preventDefault();

        if (!selectedValueField.val()) {
            swal('Foutmelding', 'Vul een waarde in voordat je verder gaat', 'error');
        } else {
            var ruleSet = {
                condition: 'AND',
                rules: [
                    {
                        id: selectedFilter.id,
                        field: selectedFilter.field,
                        type: selectedFilter.type,
                        input: selectedFilter.input,
                        operator: discountOperator.val(),
                        value: selectedValueField.val(),
                        data: selectedFilter.data
                    }
                ],
                valid: true
            };

            $.ajax({
                url: wizard.attr('data-action-url'),
                method: 'POST',
                data: {
                    start: wizard.attr('data-rule-start'),
                    description: discountDescription.val(),
                    amount: discountAmount.val(),
                    type: discountType.val(),
                    rules: ruleSet
                }
            }).then((result) => {
                window.location.replace(wizard.attr('data-success-url'));
            });
        }
    });

});