var CustomerImportModal = function(props) {
    ContentWindow.apply(this, arguments);

    this.url = props.url;

    this.onRequestDone = function(data) {
        this.setHtml(data);
        this.bindEvents();
    };

    this.onRequestFail = function() {
        this.hide();

        swal({
            'title': 'Oeps...',
            'html': 'Fout tijdens het openen<br/>van import scherm.',
            'type': 'error'
        })
    };

    this.onTriggerSubmit = function() {
        this.elm.find('form').trigger('submit');
    };

    this.open = function() {
        this.show();
        this.loading();

        var requestXhr = $.ajax({
            url: this.url,
            method: 'GET',
        });

        requestXhr.done(this.onRequestDone.bind(this));
        requestXhr.fail(this.onRequestFail.bind(this));
    };

    this.bindEvents = function() {
        this.elm.on('click', '#submit_import_form', this.onTriggerSubmit.bind(this))
    };
};
CustomerImportModal.prototype = ContentWindow.prototype;
CustomerImportModal.prototype.constructor = CustomerImportModal;

$(function () {
    $('.view-action').on('click', 'a[data-type="import-users"]', function(e) {
        e.preventDefault();

        var elm = $(this);
        var modal = new CustomerImportModal({
            url: elm.attr('href'),
            width: '500px',
            toolbarButtons: [
                {
                    id: 'submit_import_form',
                    position: 'right',
                    label: 'Opslaan',
                    classNames: 'btn btn-primary',
                }
            ],
            hideCallback: function() {
                // Refresh datatable
                $('div[data-panel-id="tab_panel"] table.dataTable').dataTable().api().ajax.reload();
            }
        });

        modal.open();
    });
});
