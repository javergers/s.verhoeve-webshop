//import moment from 'moment';

function Invoice() {
    const self = this;

    self.bindEvents = function(){
        $('.create-invoice-export').on('click', function(e){
            const form = $(this).closest('form');
            const invoiceDateElm = $('input[type=hidden][name=form\\[export_container\\]\\[invoiceDate\\]]');
            let invoiceDate = moment(invoiceDateElm.val());
            if(invoiceDate > moment()){
                swal({
                    title: 'Factuurdatum',
                    text: `De factuurdatum ${invoiceDate.format('D-M-YYYY')} ligt in de toekomst, weet je zeker dat dit juist is?`,
                    type: 'warning',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Ja, ga veder',
                    cancelButtonText: 'Nee, kies een andere datum',
                    showCancelButton: true,
                }).then(function(result){
                    if(result === true) {
                        form.submit();
                    }
                });
            } else {
                form.submit();
            }
            e.preventDefault();
        });
    }();
}

const invoice = new Invoice();