function ProductModal() {
    this.id = null;
    this.name = null;
    this.price = null;
    this.priceIncl = null;
    this.vat = null;
    this.vatGroupId = null;
    this.metadata = {};

    this.isCombination = false;
    this.isCardAvailable = false;
    this.isPersonalizationAvailable = false;
    this.isPersonalizationRequired = false;
    this.isCardAvailable = false;
    this.isLetterAvailable = false;
}