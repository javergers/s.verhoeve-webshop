function Postcode(parameters) {
    this.elm = $('#delivery-area-map');

    this.dataset = null;
    this.mapVersion = null;
    this.markerClusterer = null;
    this.regions = [];
    this.pointers = [];

    this.forceSupplierFilterUpdate = false;

    // Flag to enable/disable select2
    this.useSelect2 = true;

    // Register active xhr requests.
    this.xhr = null;
    this.datasetXhr = null;
    this.datasetData = null;

    this.pointerXhr;
    this.regionXhr;

    this.historicalRegions = [];

    this.mapCenter = null;
    this.distanceTreshold = null;

    this.initialized = false;

    this.viewModus = 'default';
    this.deliveryAddress = null;
    this.deliveryAddressMarker = null;

    // Register current content window.
    this.currentContentWindow = null;

    this.currentInfoWindow = null;

    if (typeof parameters === 'object') {

        if (typeof parameters.pageUrl !== 'undefined') {
            this.pageUrl = parameters.pageUrl;
        }

        if (typeof parameters.currentDataset !== 'undefined') {
            this.currentDataset = parameters.currentDataset;
        }

        if (typeof parameters.filters !== 'undefined') {

            if (typeof parameters.filters.suppliers !== 'undefined') {
                this.suppliers = parameters.filters.suppliers;
            }

            if (typeof parameters.filters.supplierGroup !== 'undefined') {
                this.supplierGroup = parameters.filters.supplierGroup;
            }

            if (typeof parameters.filters.supplierConnectors !== 'undefined') {
                this.supplierConnectors = parameters.filters.supplierConnectors;
            }

            if (typeof parameters.filters.intervals !== 'undefined') {
                this.intervals = parameters.filters.intervals;
            }
        }

        if (typeof parameters.deliveryAddress !== 'undefined') {
            this.deliveryAddressUuid = parameters.deliveryAddress;
        }

        if (typeof parameters.viewModus !== 'undefined') {
            this.viewModus = parameters.viewModus;
        }

        if (typeof parameters.connectorColors !== 'undefined') {
            this.connectorColors = parameters.connectorColors;
        }

        if (typeof parameters.pointerClusterImage !== 'undefined') {
            this.pointerClusterImage = parameters.pointerClusterImage;
        }
    }
}

Postcode.prototype.init = function () {
    this.fetchData()
        .then(function() {
            this.boot();
        }.bind(this));
};

Postcode.prototype.fetchData = function() {
    const promises = [];

    if(this.deliveryAddressUuid) {
        promises.push(this.getDeliveryAddress()
            .then(function (data) {
                this.deliveryAddress = data;
            }.bind(this)));
    }

    return Promise.all(promises);
};

Postcode.prototype.boot = function() {
    this.initLoader();
    this.initMap();
    this.bindEvents();
    this.initDeliveryAddress();
};

Postcode.prototype.initDeliveryAddress = function() {
    if(null !== this.deliveryAddress) {
        this.deliveryAddressMarker = this.createDeliveryAddressPointer();

        var point = this.deliveryAddress.point;

        this.map.panTo(new google.maps.LatLng(point.lat, point.lng));
    }
};

Postcode.prototype.initLoader = function () {
    this.loader = $('<div id="loader" style="display: none;"><span><i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Loading...</span></div>');
    this.elm.prepend(this.loader);
    this.showLoader();
};

Postcode.prototype.showLoader = function () {
    this.loader.show();
    this.disabled = true;
};

Postcode.prototype.hideLoader = function () {
    this.loader.hide();
    this.disabled = false;
};

Postcode.prototype.initMap = function () {
    var mapsId = document.getElementById('map-canvas');
    this.map = new google.maps.Map(mapsId, {
        zoom: 12,
        minZoom: this.viewModus === 'default' ? 7 : 12,
        maxZoom: this.viewModus === 'default' ? 15 : 12,
        scrollwheel: this.viewModus === 'default',
        mapTypeControl: false,
        streetViewControl: false,
        disableDoubleClickZoom: true,
        disableDefaultUI: this.viewModus !== 'default',
        center: new google.maps.LatLng(52.366667, 4.9),
    });
};

Postcode.prototype.bindEvents = function () {
    var self = this;

    var deliveryAreaMap = $('#delivery-area-map');

    //bounds_changed
    this.map.addListener('dragend', function () {
        self.updateView();
    });

    this.map.addListener('zoom_changed', function () {
        self.updateView();
    });

    this.map.addListener('idle', function () {
        if(self.initialized === false) {
            self.initialized = true;
            $('#dataset', deliveryAreaMap).trigger('change');
        }
    });

    $('#search_term').on('keyup', function (e) {
        if (e.keyCode === 13) {
            $('form').trigger('submit');
        }
    });

    $('#delivery-area-map form').on('submit', function (e) {
        e.preventDefault();
        if ($('#search_term').val()) {
            self.geocode($('#search_term').val());
        }
    });

    $('#map_speed_up_checkbox', deliveryAreaMap).on('change', function (e) {
        e.preventDefault();
        self.updateView();
    });

    $('#dataset', deliveryAreaMap).on('change', function () {

        if (this.disabled) {
            return;
        }

        // var date = ($(this).val().indexOf("Date") !== -1);
        //
        // if (date) {
        // 	$("#date").closest("div").css("visibility", "visible");
        // } else {
        // 	$("#date").closest("div").css("visibility", "hidden");
        // }

        var value = $('option[value=' + $(this).val() + ']', this).val();
        var event = $('option[value=' + $(this).val() + ']', this).data('event');
        var date = null;

        self.dataset = {
            name: value,
            event: event,
            date: date
        };

        if (typeof self.oldDataset !== 'undefined' && self.dataset.name !== self.oldDataset.name) {
            self.intervals = [];
            self.suppliers = [];
        }

        self.showLoader();

        self.supplierGroup = null; // Reset selected value

        self.setDataset(self.dataset);
    });

    deliveryAreaMap.on('change', '#supplierGroups', function (e) {

        if (this.disabled) {
            return;
        }

        var elm = $(this);

        self.supplierGroup = elm.val();
        self.suppliers = [];
        self.intervals = [];

        $('#delivery-area-map').find('#dataset').trigger('change');
    });

    deliveryAreaMap.on("click", "#filter_suppliers .clear", function (e) {
        $("#filter_suppliers select").select2("val", "");
    });

    deliveryAreaMap.on('change', '#suppliers', function (e) {
        if (this.disabled) {
            return;
        }

        var elm = $(this);
        self.suppliers = elm.val();
        $('#delivery-area-map').find('#dataset').trigger('change');
    });

    var intervalTimeout = null;
    deliveryAreaMap.on('change', 'input.legend-option', function (e) {

        if (this.disabled) {
            return;
        }

        var inputs = $('#maps_legend input:checked');

        self.intervals = [];
        inputs.each(function () {
            var input = $(this);
            self.intervals.push(input.val());
        });

        clearTimeout(intervalTimeout);
        intervalTimeout = setTimeout(function () {
            $('#delivery-area-map').find('#dataset').trigger('change');
            clearTimeout(intervalTimeout);
        }, 750);
    });

    deliveryAreaMap.on('click', '#maps_legend .clear', function (e) {

        if (this.disabled) {
            return;
        }

        var elm = $(this);
        var holder = elm.parents('#maps_legend');
        holder.find('input').removeAttr('checked');

        self.intervals = [];

        $('#delivery-area-map').find('#dataset').trigger('change');
    });

    deliveryAreaMap.on('click', '[data-action=reset-regions]', function (e) {

        if (this.disabled) {
            return;
        }

        e.preventDefault();

        swal({
            title: 'Postcode vernieuwen?',
            text: 'Postcodegebieden worden opnieuw ingeladen.',
            confirmButtonText: 'Inladen',
            cancelButtonText: 'Annuleren',
            showCancelButton: true,
            reverseButtons: true
        }).then(function (result) {

            window.location.href = self.pageUrl + '#refresh';
            window.location.reload(true);

        }, function (dismiss) {
            self.hideLoader();
        });
    });

    if (self.useSelect2) {
        var select2Fields = $('#delivery-area-map select');
        select2Fields.select2();
    }
};

Postcode.prototype.geocode = function (address, callback) {
    var self = this;

    new google.maps.Geocoder().geocode({
        address: address,
        bounds: new google.maps.LatLngBounds(
            new google.maps.LatLng(50.7503837, 3.3316000),
            new google.maps.LatLng(53.6756000, 7.2377448)
        )
    }, function (results, status) {
        switch (status) {
            case google.maps.GeocoderStatus.OK:

                var latLng = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());

                self.map.panTo(latLng);

                self.updateView();

                break;
            case google.maps.GeocoderStatus.ZERO_RESULTS:
                alert('Geen locatie gevonden voor ' + address);
                break;
            case google.maps.GeocoderStatus.OVER_QUERY_LIMIT:
            case google.maps.GeocoderStatus.REQUEST_DENIED:
            case google.maps.GeocoderStatus.INVALID_REQUEST:
                alert('Technisch probleem ' + address);
                break;
        }
    });
};

Postcode.prototype.setDataset = function (dataset, flatten, force) {
    var self = this;

    var newDatasetRequestData = {
        dataset: this.dataset.name,
        suppliers: this.suppliers,
        supplierGroup: this.supplierGroup,
        supplierConnectors: this.supplierConnectors,
        intervals: this.intervals,
        date: this.dataset.date
    };

    var isSameRequest = JSON.stringify(newDatasetRequestData) === JSON.stringify(this.datasetRequestData);

    if(isSameRequest && true !== force) {
        return false;
    }

    this.datasetRequestData = newDatasetRequestData;

    if (this.dataset) {
        self.clearRegions(true);
    }

    this.dataset = dataset;

    if(this.datasetXhr) {
        this.datasetXhr.abort();
    }

    this.flatten = !!flatten;

    this.datasetXhr = $.ajax({
        url: this.pageUrl + 'get-dataset',
        data: newDatasetRequestData,
        success: function (data) {
            this.datasetData = data;

            if(data.hasOwnProperty('supplierGroups') && data.supplierGroups.length > 0) {
                 this.supplierGroup = data.supplierGroups[0]; // Default value
            }

            self.updateView();
            self.updateFilters(self.dataset, data);
            self.forceSupplierFilterUpdate = false;
            self.hideLoader();
        }.bind(this),
        dataType: 'json'
    });
};

Postcode.prototype.getDataset = function () {
    return this.dataset;
};

Postcode.prototype.clearRegions = function (resetColor) {
    this.regions.forEach(function(region){
        region.clear(resetColor);
    });
};

Postcode.prototype.updateFilterSupplierGroups = function (supplierGroups) {
    var select = $('select#supplierGroups');
    var sidebarContent = select.parents('.sidebar-content');

    select.html('');
    sidebarContent.hide();

    var options = [];

    for (var key in supplierGroups) {

        var attr = [];
        var supplierGroup = supplierGroups[key];

        if (supplierGroup === this.supplierGroup) {
            attr.push('selected="selected"');
        }

        options.push('<option value="' + supplierGroup + '" ' + attr.join(' ') + '>' + supplierGroup + '</option>');
    }

    if (options) {
        select.html(options.join(''));
        sidebarContent.show();
    }

    return select;
};

Postcode.prototype.updateFilters = function (dataset, results) {
    var self = this;
    var legendItems = $('#delivery-area-map #maps_legend .items');

    var hasOptions = false;
    var hasCheckedOptions = false;

    legendItems.html('').parents('.sidebar-content').hide();
    for (var i in results.legend) {
        var legend = results.legend[i];
        var legendHtml = '<div class="item">';
        if (legend.option_name) {

            hasOptions = true;

            var checked = '';
            var isChecked = (this.intervals.indexOf(legend.value) >= 0);

            if (isChecked) {
                hasCheckedOptions = true;
                checked = ' checked="checked"';
            }
            legendHtml += '<input type="checkbox" name="' + legend.option_name + '" value="' + legend.value + '" class="form-control legend-option"' + checked + ' />';

        }
        legendHtml += '<div>';
        legendHtml += '<div class="icon" style="background-color: ' + legend.color + '"></div>' + legend.label;
        legendHtml += '</div>';
        legendHtml += '</div>';

        legendItems.append(legendHtml);
    }

    if (hasOptions) {
        legendItems.append('<div><span class="clear">Toon alle</span></div>');
    }

    if (!hasCheckedOptions) {
        $('#delivery-area-map #maps_legend input').addClass('checked');
    }

    legendItems.parents('.sidebar-content').show();

    // Update select option of supplierGroups.
    self.updateFilterSupplierGroups(results.supplierGroups);

    var supplierGroupsEquals = false;
    if (this.supplierGroup === this.oldSupplierGroup) {
        supplierGroupsEquals = true;
    }

    var datasetEquals = false;
    if (typeof this.oldDataset !== 'undefined' && this.oldDataset.name === dataset.name) {
        datasetEquals = true;
    }

    this.oldSupplierGroup = this.supplierGroup;

    // Update these field only when dataset changed.
    if (!datasetEquals || !supplierGroupsEquals || self.forceSupplierFilterUpdate
    ) {
        var suppliers = $('select#suppliers');

        suppliers.html('').parents('.sidebar-content').hide();

        for (var key in results.suppliers) {

            var supplier = results.suppliers[key];

            var selected = '';
            if (self.suppliers.indexOf(parseInt(key)) >= 0) {
                selected = 'selected="selected"';
            }
            suppliers.append('<option value="' + key + '" ' + selected + '>' + supplier + '</option>');
        }

        if (results.suppliers) {
            suppliers.parents('.sidebar-content').show();
        }
    }

    // Load pointers only when dataset or supplierGroup changed
    if (!!supplierGroupsEquals === false) {
        self.initPointers();
    }

    if (self.useSelect2) {
        var select2Fields = $('#delivery-area-map select');
        select2Fields.select2('destroy');
        select2Fields.select2();
    }

    this.oldDataset = dataset;
};

Postcode.prototype.getRegion = function (key) {
    if(!!this.regions[key]) {
        return this.regions[key];
    }

    return null;
};

Postcode.prototype.initPointers = function () {
    var self = this;

    self.pointers.forEach(function(pointer) {
        pointer.clear();
    });

    self.pointers = [];

    this.pointerXhr = $.ajax({
        url: this.pageUrl + 'get-pointers',
        data: {
            supplierGroup: self.supplierGroup,
            suppliers: self.suppliers,
            supplierConnectors: self.supplierConnectors
        },
        success: function (pointers) {
            pointers.forEach(function(pointer) {
                self.pointers.push(new PostcodePointer(self, pointer));
            });

            self.drawPointersWithinBounds(true);
        },
        dataType: 'json'
    });
};

Postcode.prototype.drawPointersWithinBounds = function (force) {
    var self = this;
    var markers = [];

    if(!self.pointers) {
        return false;
    }

    self.pointers.forEach(function (pointer) {
        var bounds = self.getBounds();
        if(self.inSpeedUpModus()) {

            // Calculate which pointer are within bounds.
            // Display region only when
            if(
                ((pointer.lat > bounds['SouthWest'].lat
                        && pointer.lat < bounds['NorthEast'].lat)
                    && (pointer.lng > bounds['SouthWest'].lng
                        && pointer.lng < bounds['NorthEast'].lng)
                )) {
                pointer.show();
                markers.push(pointer.marker);
            } else {
                pointer.clear();
            }
        } else {
            pointer.show();
            markers.push(pointer.marker);
        }
    });

    if(self.deliveryAddressMarker) {
        markers.push(self.deliveryAddressMarker);
    }

    if(self.map.getZoom() < 12 || !!force) {
        if(self.markerClusterer) {
            self.markerClusterer.clearMarkers();
        }

        self.markerClusterer = new MarkerClusterer(self.map, markers,
            {imagePath: self.pointerClusterImage}
        );
    }
};

Postcode.prototype.inHistoricalRegions = function() {
    var mapCenter = this.map.getCenter();
    var alreadyRendered = false;

    this.historicalRegions.forEach(function (historicalRegion) {
        var distance = this.getDistance(
            mapCenter.lat(),
            mapCenter.lng(),
            historicalRegion.lat,
            historicalRegion.lng
        );

        if ((distance / 4) < historicalRegion.distance) {
            alreadyRendered = true;
        }

    }.bind(this));

    return alreadyRendered;
};

Postcode.prototype.getNearRegionsFromServer = function(lat, lng, distance) {
    $.ajax({
        url: this.pageUrl + 'get-near-regions/' + lat.toFixed(2) + '/' + lng.toFixed(2) + '/' + distance.toFixed(0)
    }).done(function(regions) {

        this.historicalRegions.push({
            lat: lat.toFixed(2),
            lng: lng.toFixed(2),
            distance: distance / 6,
        });

        regions.forEach(function(region) {
            var regionObj = this.getRegion(region.id);

            if(regionObj === null) {
                regionObj = new PostcodeRegion(this, region);
                this.regions[region.id] = regionObj;
            }
        }.bind(this));

        this.paintRegions();
    }.bind(this));
};

/**
 * @returns {boolean}
 */
Postcode.prototype.drawRegionsWithinBounds = function () {
    if(this.map.getZoom() < 12 && this.inSpeedUpModus()){
        this.clearRegions();
        return false;
    }

    var mapOldCenter = this.mapCenter;
    var mapCenter = this.map.getCenter();
    var southWest = this.map.getBounds().getSouthWest();
    var lat = mapCenter.lat();
    var lng = mapCenter.lng();
    var distance = this.getDistance(
        mapCenter.lat(),
        mapCenter.lng(),
        southWest.lat(),
        southWest.lng()
    );
    var diffCenterDistance = 0;
    var centerDistanceTreshold = distance / 4;

    // Calculate viewport fix between current center and cached center
    if(mapOldCenter !== null) {
        diffCenterDistance = this.getDistance(mapOldCenter.lat(), mapOldCenter.lng(), mapCenter.lat(), mapCenter.lng());
    } else {
        this.mapCenter = mapCenter;
    }

    // Check if current viewport is already rendered.
    var alreadyRendered = false;
    if(diffCenterDistance > centerDistanceTreshold) {
        alreadyRendered = this.inHistoricalRegions();

        // this.clearRegions();
        this.mapCenter = mapCenter;
    }

    // check if needed to execute XHR.
    if((mapOldCenter === null || diffCenterDistance > centerDistanceTreshold)
        && alreadyRendered === false) {
        this.getNearRegionsFromServer(lat, lng, distance);

    }

    this.paintRegions();
};

Postcode.prototype.paintRegions = function() {
    var self = this;
    var regions = this.regions;
    var datasetRegions = this.datasetData ? this.datasetData.regions : null;
    var flatten = this.flatten;

    regions.forEach(function(region) {

        var bounds = self.getBounds();
        var regionBounds = region.getBounds();
        var drawThisRegion = true;
        if(self.inSpeedUpModus()) {
            // Calculate which regions are within bounds.
            // Display region only when
            if(
                ((regionBounds['NorthEast'].lat > bounds['SouthWest'].lat
                        && regionBounds['SouthWest'].lat < bounds['NorthEast'].lat)
                    && (regionBounds['NorthEast'].lng > bounds['SouthWest'].lng
                        && regionBounds['SouthWest'].lng < bounds['NorthEast'].lng)
                )) {
                region.show();
            } else {
                region.clear();
                drawThisRegion = false;
            }
        } else {
            region.show();
        }

        if(typeof datasetRegions !== 'undefined' && datasetRegions !== null) {
            var data = datasetRegions[region.id];

            if(!!data && drawThisRegion) {
                if (flatten === false) {
                    region.setColor(data.color);
                } else {
                    // Used for pointer region selector.
                    region.setColor('#4CAF50');
                }
            }
        }
    });
};

Postcode.prototype.getBounds = function () {
    return {
        'NorthEast': {
            'lat': this.map.getBounds().getNorthEast().lat(),
            'lng': this.map.getBounds().getNorthEast().lng(),
        },
        'SouthWest': {
            'lat': this.map.getBounds().getSouthWest().lat(),
            'lng': this.map.getBounds().getSouthWest().lng(),
        }
    };
};

Postcode.prototype.updateView = function () {
    var self = this;

    self.drawRegionsWithinBounds();
    self.drawPointersWithinBounds();
};

Postcode.prototype.inSpeedUpModus = function() {
    return $('#map_speed_up_checkbox:checked').length > 0;
};

Postcode.prototype.getDistance = function(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1); // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
};

Postcode.prototype.deg2rad = function(deg) {
    return deg * (Math.PI / 180)
};

Postcode.prototype.getDeliveryAddress = function () {
    var url = addressUrl.replace("__ADDRESS__", this.deliveryAddressUuid);

    return new Promise(function(resolve, reject) {
        $.ajax({
            'url': url,
            'method': 'GET',
            'dataType': 'json'
        }).then(resolve, reject);
    });
};

Postcode.prototype.createDeliveryAddressPointer = function() {
    if(
        !this.hasOwnProperty('deliveryAddress')
        || !this.deliveryAddress.hasOwnProperty('point')
        || !this.deliveryAddress.point.hasOwnProperty('lat')
        || !this.deliveryAddress.point.hasOwnProperty('lng')
    ) {
        throw Error('Delivery address not found.');
    }

    var point = this.deliveryAddress.point;
    var map = this.map;

    return new google.maps.Marker({
        position: new google.maps.LatLng(point.lat, point.lng),
        map: map,
        title: 'Bezorgadres',
        icon: {
            url: '/admin/bezorggebieden/get-pointer/delivery-address/%23000000/icon.svg',
            anchor: new google.maps.Point(50, 50),
            scaledSize: new google.maps.Size(50, 50),
        },
        opacity: 1,
        optimized: false,
        zIndex: ((this.zIndex) ? this.zIndex : 50)
    });
};
