function PostcodeSelector(pointer, postcode, parent) {
    this.pointer = pointer;
    this.postcode = postcode;
    this.parent = parent;

    this.selection = [];
};

PostcodeSelector.prototype.init = function () {

    this.hideInfoWindow();
    this.hidePointers();
    this.showToolbar();
    this.bindEvents();

    this.postcode.postcodeSelector = this;
};

PostcodeSelector.prototype.close = function () {
    this.showPointers();
    this.hideToolbar();
    this.clearSelection();

    this.postcode.postcodeSelector = null;
};

PostcodeSelector.prototype.hideInfoWindow = function () {
    this.parent.close(false, false);
};

PostcodeSelector.prototype.showInfoWindow = function () {
    this.parent.reopen();
};

PostcodeSelector.prototype.hidePointers = function () {
    this.postcode.pointers.forEach(function(pointer) {
        if (pointer !== this.pointer
            && null !== pointer.marker
        ) {
            pointer.marker.setVisible(false);
        }
    });
};

PostcodeSelector.prototype.showPointers = function () {
    this.postcode.pointers.forEach(function(pointer) {
        if (pointer !== this.pointer
            && null !== pointer.marker
        ) {
            pointer.marker.setVisible(true);
        }
    });
};

PostcodeSelector.prototype.showToolbar = function () {

    if ($('#postcode_select_toolbar').length === 0) {

        var html = '<div id="postcode_select_toolbar">';

        html += '<button id="postcode_select_btn_apply" class="btn btn-primary btn-apply" data-action="apply">Toepassen</button>';

        html += '<button id="postcode_select_btn_cancel" class="btn btn-secondary btn-cancel" data-action="cancel">Annuleren</button>';

        html += '<div>';

        $('#delivery-area-map #map-canvas').prepend(html);
    }

    $('#postcode_select_toolbar').fadeIn('slow');

};

PostcodeSelector.prototype.hideToolbar = function () {
    $('#postcode_select_toolbar').fadeOut('slow', function () {
        $(this).remove();
    });
};

PostcodeSelector.prototype.toggle = function (region) {
    var self = this;

    if (this.pointer.country !== region.country) {
        return;
    }

    // Remove from selection.
    if (typeof self.selection[region.postcode] !== 'undefined') {
        region.setColor(self.selection[region.postcode].oldColor);
        delete self.selection[region.postcode];
    } else {

        var action = 'add';
        var color = '#FF7B00';

        if (region.color === '#FF0000') {
            action = 'add';
            color = '#4CAF50';
        }

        // #remove item if...
        if (region.color === '#FF7B00') {
            action = 'remove';
            color = '#FFFFFF';
        }

        if (region.color === '#4CAF50') {
            action = 'remove';
            color = '#FF0000';
        }

        $.ajax({
            url: self.postcode.pageUrl + 'get-region-info/' + region.postcode + '/NL/',
            data: {
                supplierId: self.pointer.id,
                supplierGroup: self.postcode.supplierGroup
            },
            success: function (result) {
                self.selection[region.postcode] = {
                    id: result.id,
                    label: result.label,
                    region: region,
                    supplier: self.pointer.id,
                    price: result.defaultPrice,
                    priceIncl: result.defaultPriceIncl,
                    vat: result.defaultVat,
                    interval: result.defaultInterval,
                    action: action,
                    oldColor: region.color
                };


                region.setColor(color);
            },
            dataType: 'json'
        });
    }
};

PostcodeSelector.prototype.applySelection = function () {

    var self = this;
    this.showInfoWindow();

    var window = $(this.parent.contentWindow.elm);

    for (var key in this.selection) {
        var item = this.selection[key];

        switch (item.action) {
            case 'add':

                var exists = false;
                window.find('.collection-item select[name*=\'postcode\']').each(function () {
                    var elm = $(this);
                    if (parseInt(elm.val()) === item.id) {
                        var row = elm.parents('.collection-item');
                        row.show();
                        row.removeClass('removed');
                        exists = true;
                    }
                });

                if (exists) {
                    break;
                }

                window.find('.btn-add').trigger('click');

                var row = window.find('tr.collection-item:last-child');
                var postcodeSelect = row.find('select[name*=\'postcode\']');
                var intervalSelect = row.find('input[name*=\'deliveryInterval\']');

                // Set postcode
                var option = '<option value="' + item.id + '">' + item.label + '</option>';
                postcodeSelect.html(option);
                postcodeSelect.trigger('change.select2');

                // Set Price
                row.find('input[name*=\'deliveryPrice\']').val(item.price);
                row.find('input[name*=\'deliveryPriceInclVat\']').val(item.priceIncl);
                row.find('input[name*=\'deliveryVat\']').val(item.vat);

                if (item.interval) {
                    var intervalOption = '<option value="' + item.interval.interval + '">' + item.interval.description + '</option>';
                    intervalSelect.html(intervalOption);
                    intervalSelect.trigger('change.select2');
                }

                break;

            case 'remove':
                window.find('.collection-item select[name*=\'postcode\']').each(function () {
                    var elm = $(this);
                    if (parseInt(elm.val()) === item.id) {
                        var row = elm.parents('.collection-item');

                        row.hide();
                        row.addClass('removed')
                    }
                });

                break;
            default:
        }
    }

    self.hideToolbar();
};

PostcodeSelector.prototype.clearSelection = function () {

    for (var key in this.selection) {
        var item = this.selection[key];
        if (item.oldColor !== '#26AE00') {
            item.oldColor = '#FFFFFF';
        }
        item.region.setColor(item.oldColor);
    }
    this.selection = [];

};

PostcodeSelector.prototype.bindEvents = function () {

    var self = this;

    $('#delivery-area-map #map-canvas')
    .off('click', '#postcode_select_toolbar button')
    .on('click', '#postcode_select_toolbar button',
        function (e) {

            var btn = $(this);

                switch (btn.data('action')) {
                    case 'apply':
                        self.applySelection();
                        break;
                    case 'cancel':
                    default:
                        self.parent.close();
                        self.close();
                }
            }
        );
};
