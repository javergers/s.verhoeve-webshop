function SupplierInfoWindow(options) {

    var self = this;

    if (typeof options !== 'undefined' && typeof options.parent !== 'undefined') {
        this.parent = options.parent;
    }

    if (typeof options !== 'undefined' && typeof options.pointer !== 'undefined') {
        this.pointer = options.pointer;
    }

    options = $.extend(true, {}, options, {
        width: '600px',
        zIndex: 3000,
        closeButtonLabel: 'Venster sluiten',
        parent: null,
        hideBackground: true,
        hideCallback: function () {

            // Close postcode selector if still active.
            if (typeof self.parent.postcodeSelector !== 'undefined' && self.parent.postcodeSelector) {
                self.parent.postcodeSelector.close();
            }

            // Reset current content window state.
            if(self.parent.currentContentWindow && self.parent.currentContentWindow === self.contentWindow) {
                self.parent.currentContentWindow = null;
            }

            // Update data when modals fully closed...
            self.parent.setDataset(self.parent.dataset, null, true);
        },
        toolbarButtons: [
            {
                id: 'save_edit_supplier_regions',
                position: 'right',
                label: 'Opslaan en sluiten',
                order: 1,
                classNames: 'btn btn-primary',
                action: 'save-edit-supplier-regions'
            }
        ]
    });

    // Don't send this parent to the ContentWindow.
    options.parent = null;

    var currentContentWindow = this.parent.currentContentWindow;

    // Check if there is already a content window.
    if(currentContentWindow) {
        if(
            typeof currentContentWindow.referenceId !== 'undefined'
            && currentContentWindow.referenceId === 'SupplierInfoWindow'
        ) {
            this.contentWindow = currentContentWindow;
        } else {
            currentContentWindow.hide();
        }
    }

    if(!this.contentWindow) {
        this.contentWindow = new ContentWindow(options);
        this.contentWindow.referenceId = 'SupplierInfoWindow';
    }
}

SupplierInfoWindow.prototype.open = function (url, data) {

    var self = this;

    this.url = url;
    this.data = data;

    if (typeof self.contentWindow === 'undefined') {
        return false;
    }

    this.parent.currentContentWindow = self.contentWindow;

    self.contentWindow.empty().show();
    self.contentWindow.loading();

    if(this.parent.xhr) {
        this.parent.xhr.abort();
    }

    this.parent.xhr = $.ajax({
        url: url,
        data: data,
        success: function (res) {
            self.contentWindow.setHtml(res);
            self.bindEvents();
        },
        dataType: 'html'
    });
};

SupplierInfoWindow.prototype.reopen = function () {
    var self = this;
    self.contentWindow.reopen();
};

SupplierInfoWindow.prototype.close = function (executeCallback, removeElm) {
    var self = this;
    self.contentWindow.hide(executeCallback, removeElm);
};

SupplierInfoWindow.prototype.bindSelect2 = function (elm) {

    var self = this;

    elm.select2({
        'ajax': {
            url: self.url,
            processResults: function (data, params) {
                return {
                    results: data.items
                };
            },
            data: function (params) {
                return {
                    name: elm.data('name'),
                    search: params.term
                };
            }
        }
    });
};

SupplierInfoWindow.prototype.bindEvents = function () {

    var self = this;
    var elm = $(self.contentWindow.elm);

    var regionCollectionCalcVat = new RegionCollectionCalcVat(elm.find('form'));
    regionCollectionCalcVat.init(elm.find('form'));

    elm.off("click", "#PostcodeSelector").on("click", "#PostcodeSelector", function (e) {
        e.preventDefault();
        self.postcodeSelector = new PostcodeSelector(self.pointer, self.parent, self);
        self.postcodeSelector.init();
    });

    elm.off("click", "#PostcodeExport").on("click", "#PostcodeExport", function (e) {
        e.preventDefault();
        document.location.href = self.parent.pageUrl + 'get-export-postcodes/' + self.pointer.id;
    });

    $(elm).off('click', '.btn-add').on('click', '.btn-add', function (e) {
        $('select.select2', $(self.contentWindow.elm)).each(function () {
            var selectField = $(this);
            self.bindSelect2(selectField);
        });

        regionCollectionCalcVat.bindEvents();
    });

    elm.find('select.select2').each(function () {
        var selectField = $(this);
        self.bindSelect2(selectField);
    });

    $(elm).off('click', '#save_edit_supplier_regions').on('click', '#save_edit_supplier_regions', function (e) {
        elm.find('form .collection-item.removed').remove();
        elm.find('form').submit();
    });

    $.uniform.update();

    if (typeof(setupFormValidation) !== 'undefined') {
        setupFormValidation({}, true);
    }


};
