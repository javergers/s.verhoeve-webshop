function capitalizeFilter(s) {
    if (typeof s !== 'string') return '';
    return s.charAt(0).toUpperCase() + s.slice(1);
}

/**
 * @param value
 * @param fractionDigits
 * @returns {string}
 */
function priceFilter(value, fractionDigits) {
    return (parseFloat(value))
    .toLocaleString('nl', {
        minimumFractionDigits: fractionDigits,
        maximumFractionDigits: fractionDigits
    });
};

function grabUuidFromString(string) {
    const regex = /[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}/gm;
    let uuid = null, m;

    while ((m = regex.exec(string)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (m.index === regex.lastIndex) {
            regex.lastIndex++;
        }

        // The result can be accessed through the `m`-variable.
        m.forEach((match) => {
            uuid = match;
        });
    }

    return uuid;
}

function getQueryParam(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function generateUuid() {
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
        d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

function dataMerge() {
    // Variables
    var extended = {};
    var deep = false;
    var i = 0;

    // Check if a deep merge
    if (typeof (arguments[0]) === 'boolean') {
        deep = arguments[0];
        i++;
    }

    function merge(obj) {
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                if (deep && Object.prototype.toString.call(obj[prop]) === '[object Object]') {
                    // If we're doing a deep merge and the property is an object
                    extended[prop] = dataMerge(true, extended[prop], obj[prop]);
                } else {
                    // Otherwise, do a regular merge
                    extended[prop] = obj[prop];
                }
            }
        }

        return extended;
    }

    // Loop through each object and conduct a merge
    for (; i < arguments.length; i++) {
        merge(arguments[i]);
    }

    return extended;
};

(function () {
    String.prototype.slugify = function (replace) {
        if (!replace) replace = '-';

        return this.toString().toLowerCase()
        .replace(/\s+/g, replace)                   // Replace spaces with -
        .replace(/&/g, replace + 'and' + replace)     // Replace & with 'and'
        .replace(/[^\w\-]+/g, '')                   // Remove all non-word chars
        .replace(/\-\-+/g, replace)                 // Replace multiple - with single -
        .trim()
            ;
    };
})();

/**
 * JQuery methods
 */
(function ($) {
    $.fn.inView = function () {
        if (!this.length) return false;
        var rect = this.get(0).getBoundingClientRect();

        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );

    };

    $.fn.allInView = function () {
        var all = [];
        this.forEach(function () {
            all.push($(this).inView());
        });
        return all.indexOf(false) === -1;
    };
})(jQuery);

(function ($) {
    $.getScriptOnce = function (url, successhandler) {
        if ($.getScriptOnce.loaded.indexOf(url) === -1) {
            $.getScriptOnce.loaded.push(url);
            if (successhandler === undefined) {
                return $.getScript(url);
            } else {
                return $.getScript(url, function (script, textStatus, jqXHR) {
                    successhandler(script, textStatus, jqXHR);
                });
            }
        } else if ($.getScriptOnce.loaded.indexOf(url) !== -1) {
            return true;
        } else {
            return false;
        }

    };

    $.getScriptOnce.loaded = [];

}(jQuery));
