function historyPageClass() {
    this.viewDiff = function (element, data, button) {

        swal.queue([
            {
                title: 'Laden',
                html: 'Geschiedenis word geladen',
                showConfirmButton: false,
                confirmButtonText: 'Sluiten',
                onOpen: () => {
                    swal.showLoading();

                    $.ajax({
                        method: button.method,
                        url: button.url,
                        type: 'json',
                    }).done(function (response) {
                        swal.disableLoading();

                        if (typeof response.diff === 'undefined' || response.diff.length === 0) {

                            swal({
                                title: 'Oops',
                                html: 'Geen verschil gevonden',
                                confirmButtonText: 'Sluiten'
                            });
                            return;
                        }

                        var html = $('<table class="table table-striped table-hover history-table"></table>');
                        var thead = $('<thead><tr><th>Veld</th></tr></thead>');
                        var tbody = $('<tbody></tbody>');

                        if (response.action === 'update') {
                            $('<th>Van</th><th colspan="2">Naar</th>').appendTo(thead.find('tr'));
                        } else {
                            $('<th>Inhoud</th>').appendTo(thead.find('tr'));
                        }

                        for (var key in response.diff) {
                            var newTr = $('<tr><td>' + key + '</td></tr>');

                            if (response.action === 'update') {
                                $('<td>' + response.diff[key]['old'] + '</td>').appendTo(newTr);
                            }

                            $('<td>' + response.diff[key]['new'] + '</td>').appendTo(newTr);

                            newTr.appendTo(tbody);

                        }

                        thead.appendTo(html);
                        tbody.appendTo(html);

                        swal({
                            title: response.title,
                            html: html,
                            confirmButtonText: 'Sluiten'
                        });
                    }).fail(function () {

                        swal({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Pagina kon niet geladen worden',
                            confirmButtonText: 'Sluiten'
                        })
                    });
                }
            }
        ]);
    }
}

var historyPage = new historyPageClass();

