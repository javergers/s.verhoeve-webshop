var orderSearch; // Add to public scope

function OrderSearch() {
    this.typeaheadDelay = 1000;
    this.typeaheadXhr = null;
    this.typeaheadTimer = null;

    this.searchTimeout = null;
    this.searchDelay = 1000;
    this.searchKeyLength = 0;

    this.searchForm = $('form#search');
    this.searchInput = $('.field-search input[type=search]');
    this.searchLimitInput = $('select[name=limit]');
    this.searchStatusInput = $('select[name=status]');
    this.searchSiteInput = $('select[name=site]');
    this.orderList = $('#order_list').find('tbody');

    this.bindEvents = function() {
        var self = this;

        this.searchSiteInput.on('change', function() {
            self.search();
        });

        this.searchStatusInput.on('change', function() {
            self.search();
        });

        this.searchLimitInput.on('change', function() {
            self.search();
        });

        this.searchInput
            .typeahead({}, {
                source: function (query, syncResults, asyncResults) {

                    if(self.typeaheadTimer) {
                        clearTimeout(self.typeaheadTimer);
                    }

                    self.typeaheadTimer = setTimeout(function () {
                        clearTimeout(self.typeaheadTimer);
                        self.typeahead(query, syncResults, asyncResults);
                    }, self.typeaheadDelay);
                },
                minLength: 6
            })
            .on('typeahead:selected', function (e) {
                self.searchInput.val(e.target.value);
                self.search();
            })
            .on('keyup', function (e) {
                const prevSearchKeyLength = this.searchKeyLength;

                this.searchKeyLength = e.currentTarget.value.length;

                if(prevSearchKeyLength === 0) {
                    // Clear site filter while typing new query.
                    this.searchSiteInput.val(null).trigger('change');
                    this.searchStatusInput.find('option').prop('selected', true);
                }

                if (e.keyCode === 13) {
                    self.search(10);
                }
            }.bind(this))
        ;

        this.searchForm
            .on('submit', function (e) {
                e.preventDefault();
                self.search();

                return false;
            })
            .trigger('submit')
        ;
    };

    this.typeahead = function(query, syncResults, asyncResults) {
        var regex = /(^[0-9-]*$)/;

        if(self.typeaheadXhr) {
            self.typeaheadXhr.abort();
            self.typeaheadXhr = null;
        }

        if(!regex.test(query)) {
            return;
        }

        self.typeaheadXhr = $.get('typeahead?q=' + query, function (data) {
            asyncResults(data);
        });
    };

    this.search = function(searchDelay) {
        this.clearSearchTimeout();

        var delay = searchDelay || this.searchDelay;

        this.searchTimeout = setTimeout(this.doSearch.bind(this), delay);
    };

    this.clearSearchTimeout = function() {
        if(this.searchTimeout !== null) {
            clearTimeout(this.searchTimeout);
            this.searchTimeout = null;
        }
    };

    this.doSearch = function() {
        this.clearSearchTimeout();

        var self = this;
        var url = this.searchForm.attr('action');
        var search = this.searchInput.val();
        var limit = this.searchLimitInput.find(':selected').val(); // Fix to selected highest value
        var sites = this.searchSiteInput.val();
        var statuses = this.searchStatusInput.val();

        this.searchSiteInput.prop('disabled', true);
        this.searchStatusInput.prop('disabled', true);
        this.searchLimitInput.prop('disabled', true);
        this.orderList.css('opacity', 0.1);

        if(this.searchXhr) {
            this.searchXhr.abort();
            this.searchXhr = null;
        }

        var data = {
            q: search,
            limit: limit,
        };

        if(typeof statuses === 'object') {
            data['statuses'] = statuses;
        }

        if(typeof sites === 'object') {
            data['sites'] = sites;
        }

        this.searchXhr = $.ajax({
            url: url,
            data: data
        })
        .done(function(html) {
            self.updateOrderList(html);
        })
        .always(function() {
            self.searchSiteInput.removeAttr('disabled');
            self.searchStatusInput.removeAttr('disabled');
            self.searchLimitInput.removeAttr('disabled');
            self.orderList.css('opacity', 1);
        })
        ;
    };

    this.updateOrderList = function(data) {
        this.orderList
            .css('opacity', 1)
            .empty()
            .html(data)
        ;

        $('[data-popup="tooltip"]', this.orderList).tooltip();
    };

    this.init = function () {
        // Only available on order overview page.
        if(this.orderList.length > 0) {
            this.bindEvents();
        }
    };

    this.init();
}

$(function () {
    // Add order search to the global scope.
    orderSearch = new OrderSearch();
});
