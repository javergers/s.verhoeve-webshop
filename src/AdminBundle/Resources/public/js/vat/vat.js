$('a[data-action="create-rule"]').on('click', function (e) {
    e.preventDefault();

    var elm = $(this);
    new RuleCreate(elm.attr('href'), function () {
        if(elm.attr('data-refresh-url')) {
            window.location.replace(elm.attr('data-refresh-url'));
        }
    });
});