$(function() {
    var panel = $('.panel');

    panel.on('click', 'a[data-type="create-fraud-detection-rule"]', function(e) {
        e.preventDefault();

        new RuleCreate($(this).attr('href'), function() {
            window.location.reload();
        });
    });
})