$(function () {
    var view = $('.view-action');
    var pendingXhr = false;
    view.on('click', 'button[data-action="open-assortment-default-settings"]', function () {
        new CompanyAssortmentDefaultSettings($(this).attr('data-href'));
    });

    view.on('click', 'button[data-action="new-company-assortment"]', function () {
        new CompanyAssortment($(this));
    });

    view.on('click', 'button[data-action="copy-assortment"]', function () {
        var self = $(this);
        $.ajax({
            url: self.attr('data-assortment-url'),
            method: 'GET',
        }).done((result) => {
            swal({
                title: 'Assortimenten',
                text: 'Selecteer een assortiment om te dupliceren',
                input: 'select',
                inputOptions: result,
                inputValidator: (value) => {
                    return new Promise((resolve) => {
                        if (value) {
                            resolve()
                        } else {
                            reject('Selecteer een assortiment')
                        }
                    })

                }
            }).then((result) => {
                $.ajax({
                    url: self.attr('data-href'),
                    method: 'POST',
                    data: {
                        assortment: result
                    }
                }).done(() => {
                    loadPanel($('.tab_panel'));

                    swal('Successvol', 'Het assortiment is succesvol gedupliceerd', 'success');
                }).fail(() => {
                    swal('Fout', 'Er is een fout opgetreden met het dupliceren van het assortiment', 'error');
                });
            });
        });
    });

    view.on('click', 'a[data-action="edit-company-assortment"]', function () {
        new CompanyAssortment($(this));
    });

    view.on('click', 'a[data-action="view-company-product"]', function(e) {
        e.preventDefault();

        openContent($(this));
    });

    view.on('click', 'a[data-action="view-company-assortment"]', function () {
        new CompanyAssortment($(this));
    });

    view.on('click', 'a[data-action="select-existing-product"]', function () {
        if (pendingXhr) {
            pendingXhr.abort();
        }

        var self = $(this);
        pendingXhr = $.ajax({
            url: self.attr('data-product-list-url'),
            method: 'GET',
        }).then((result) => {
            showModal(result.products, self);

            pendingXhr = false;
        });
    });

    view.on('click', 'button[data-action="create-company-card"]', function () {
        var self = $(this);

        var availableCards = JSON.parse(self.attr('data-available-cards'));

        swal({
            title: 'Kaartje toevoegen',
            html: 'Wil je een van een kaartje afstammen of een standaard kaartje tonen?<br/><br/>' +
                '<button class="create-card btn btn-primary">Afstammen</button>' +
                '<button class="copy-card btn btn-primary" style="margin-left: 10px;">Standaard kaartje toevoegen</button>',
            type: 'question',
            onOpen: function() {
                $('.create-card').data('available-cards', availableCards);
            }
        });
    });

    $(document).on('click', '.create-card', function(e) {
        var self = $('button[data-action="create-company-card"]');
        var availableCards = JSON.parse(self.attr('data-available-cards'));

        var selectElm = $('<select>');
        selectElm.addClass('select2');

        if (availableCards.length) {
            selectElm.append('<option value="">Maak een selectie</option>');
            $.each(availableCards, function (key, value) {
                selectElm.append(`<option value="${value.id}">${value.title}</option>`);
            });

            swal({
                title: 'Selecteer een kaartje om van af te stammen',
                type: 'question',
                html: selectElm[0].outerHTML,
                showCancelButton: true,
                cancelButtonText: 'Annuleren',
                onOpen: function () {
                    $('.select2').select2();
                },
                preConfirm: () => {
                    return new Promise((resolve, reject) => {
                        var card = $('.select2').val();
                        if (card) {
                            resolve();
                        } else {
                            reject('Selecteer een kaartje voordat je verdergaat.');
                        }
                    });
                }
            }).then(() => {
                var card = $('.select2').val();
                self.attr('data-url', self.attr('data-url') + '&parent=' + card)
                openContent(self);
            });
        } else {
            swal('Fout', 'Er zijn nog geen basiskaartjes ingesteld.', 'error');
        }
    });

    $(document).on('click', '.copy-card', function(e) {
        var self = $('button[data-action="create-company-card"]');
        var availableCards = JSON.parse(self.attr('data-default-cards'));

        var selectElm = $('<select>');
        selectElm.addClass('select2');

        if (availableCards.length) {
            selectElm.append('<option value="">Maak een selectie</option>');
            $.each(availableCards, function (key, value) {
                selectElm.append(`<option value="${value.id}">${value.title}</option>`);
            });

            swal({
                title: 'Selecteer een kaartje om standaard te tonen',
                type: 'question',
                html: selectElm[0].outerHTML,
                showCancelButton: true,
                cancelButtonText: 'Annuleren',
                onOpen: function () {
                    $('.select2').select2();
                },
                preConfirm: () => {
                    return new Promise((resolve, reject) => {
                        var card = $('.select2').val();
                        if (card) {
                            resolve();
                        } else {
                            reject('Selecteer een kaartje voordat je verdergaat.');
                        }
                    });
                }
            }).then(() => {
                var card = $('.select2').val();
                $.ajax({
                    url: self.attr('data-copy-url'),
                    method: 'POST',
                    data: {
                        card: card
                    }
                }).then(() => {
                    swal('Succes', 'Kaartje succesvol toegevoegd.', 'success');
                    loadPanel($('.panel[data-panel-id="' + self.attr('data-refresh-panel') + '"]'));
                });
            });
        } else {
            swal('Fout', 'Er zijn geen kaartjes beschikbaar.', 'error');
        }
    });

    function showModal(data, button) {
        var selectElm = $('<select>');
        selectElm.addClass('select2');

        swal({
            title: 'Selecteer een product om over te nemen',
            type: 'question',
            html: selectElm[0].outerHTML,
            onOpen: function () {
                $('.select2').select2({
                    data: data,
                    escapeMarkup: function(markup) {
                        return markup;
                    },
                    templateResult: function(d) {
                        const publishClass = d.published ? 'success' : 'danger';
                        const combinationHTML = d.combination ? '<i class="mdi mdi-copyright"></i>' : '';

                        return `
                            <div>
                               ${d.text} <span class="icon-primitive-dot text-${publishClass}"></span> ${combinationHTML}        
                            </div>
                        `;
                    },
                    templateSelection: function(d) {
                        const publishClass = d.published ? 'success' : 'danger';
                        const combinationHTML = d.combination ? '<i class="mdi mdi-copyright"></i>' : '';

                        return `
                            <div>
                               ${d.text} <span class="icon-primitive-dot text-${publishClass}"></span> ${combinationHTML}                
                            </div>
                        `;
                    }
                });
            }
        }).then((result) => {
            var product = $('.select2').val();

            $.ajax({
                url: button.attr('data-copy-product-url'),
                method: 'POST',
                data: {
                    product: product,
                }
            }).then((result) => {
                loadPanel($('[data-panel-id="tab_panel"]'));
            });
        })
    }
});

function CompanyAssortmentDefaultSettings(url) {
    this.contentWindow = new ContentWindow({
        width: window.innerWidth * 0.8 + 'px',
        zIndex: 2010,
        closeButtonLabel: 'Annuleren',
        toolbarButtons: [
            {
                position: 'right',
                label: 'Opslaan',
                classNames: 'btn btn-primary',
                action: 'initialize_assortments',
            }
        ]
    });

    this.initialize(url);
}

CompanyAssortmentDefaultSettings.prototype.initialize = function (url) {
    var self = this;

    this.url = url;
    this.contentWindow.empty().show();

    $.get(url).done(function (data) {
        self.contentWindow.setHtml(data);

        $('form[name="company_assortment_default_settings"]').attr('action', url);
    }).fail(function () {

    });

    this.bindEvents();
};

CompanyAssortmentDefaultSettings.prototype.bindEvents = function () {
    $('a[data-action="initialize_assortments"]').on('click', function () {
        $('form[name="company_assortment_default_settings"]').submit();
        $('a[data-action="initialize_assortments"]').hide();
    });
};

function CompanyAssortment(triggeredButton) {
    $('.content-window').remove();

    var deletionUrl = triggeredButton.attr('data-delete-href');
    var url = triggeredButton.attr('data-href');
    var action = triggeredButton.attr('data-action');

    var toolbarButtons = [];

    if (action !== 'view-company-assortment') {
        toolbarButtons.push({
            position: 'right',
            label: 'Opslaan',
            classNames: 'btn btn-primary',
            action: 'save-assortment-product-group',
        });
    }

    if (typeof deletionUrl !== typeof undefined) {
        var label = '';

        if (action === 'edit-company-assortment') {
            label = 'Assortiment verwijderen';
        } else {
            label = 'Assortiment ontkoppelen';
        }

        toolbarButtons.unshift({
            position: 'right',
            label: label,
            classNames: 'btn btn-danger',
            action: 'delete_assortment',
        });
    }

    this.contentWindow = new ContentWindow({
        width: window.innerWidth * 0.8 + 'px',
        zIndex: 2010,
        closeButtonLabel: 'Annuleren',
        toolbarButtons: toolbarButtons,
        hideCallback: function () {
            loadPanel($('.panel[data-panel-id="tab_panel"]'))
        }
    });

    this.initialize(url, deletionUrl);
}

CompanyAssortment.prototype.initialize = function (url, deletionUrl) {
    var self = this;

    $('a[data-action="save-assortment-product-group"]').unbind();
    $('a[data-action="delete_assortment"]').unbind();

    this.url = url;
    this.contentWindow.empty().show();

    $.get(url).done(function (data) {
        self.contentWindow.setHtml(data);

        $('form[name="company_assortment_form"]').attr('action', url);
        new AssortmentProductTool();
    }).fail(function () {

    });

    this.bindEvents(deletionUrl);
}

CompanyAssortment.prototype.bindEvents = function (deletionUrl) {
    var deleteAssortmentButton = $('a[data-action="delete_assortment"]');
    $('a[data-action="save-assortment-product-group"]').on('click', function () {
        $('form[name="company_assortment_form"]').submit();
    });

    if (typeof deletionUrl !== typeof undefined) {
        deleteAssortmentButton.show();
        deleteAssortmentButton.on('click', function () {
            var self = $(this);
            swal({
                type: 'warning',
                title: 'Let op',
                text: 'Weet je zeker dat je dit assortiment wilt verwijderen? Dit kan niet ongedaan gemaakt worden!',
                showCancelButton: true,
                focusCancel: true,
                onOpen: function () {
                    $('.swal2-container').css({
                        'z-index': 6000
                    })
                }
            }).then(function () {
                $.ajax({
                    url: deletionUrl,
                    method: 'DELETE'
                }).then(function (data) {
                    swal('Assortiment verwijderd', 'Het assortiment is succesvol verwijderd', 'success');

                    location.reload();
                });
            })
        });
    } else {
        deleteAssortmentButton.hide();
    }
}

