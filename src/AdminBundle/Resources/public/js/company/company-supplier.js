function CompanySupplier() {
    this.init();
}

CompanySupplier.prototype.init = function () {
    var body = $('body');

    body.on('change', 'select[data-customer-flag]', function () {
        var state = $(this).val() === '1';

        $('select[data-customer-group-list]').closest('.form-group').toggle(state);
    }).trigger('change');

    body.on('change', 'select[data-supplier-flag]', function () {
        var state = $(this).val() === '1';

        $('select[data-supplier-group-list]').closest('.form-group').toggle(state);
    }).trigger('change');

    body.on('change', 'select[name*=supplierConnector]', function () {
        var containers = $('input[name*=company_suppliers_connector]').closest('fieldset').parent();
        containers.hide();

        if (!$(this).val()) {
            return;
        }

        var selectedContainer = $('input[name*=company_suppliers_connector_' + $(this).val().toLowerCase() + '_]').closest('fieldset').parent();
        selectedContainer.show();
    });

    $('select[name*=supplierConnector]').trigger('change')
};

function initCompanySupplier() {
    new CompanySupplier();
}
