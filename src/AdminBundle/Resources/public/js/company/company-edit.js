var companyEdit;

$(function () {
    function CompanyEdit() {
        this.init();
    }

    CompanyEdit.prototype.init = function () {
        this.bindEvents();
    };

    CompanyEdit.prototype.bindEvents = function () {
        var body = $('body');

        body.on('click', '#chamber_of_commerce_update', function () {
            var $this = $(this);
            $.ajax({
                method: 'POST',
                url: '/admin/klanten/bedrijven/migreer',
                data: {
                    company: $(this).closest('div').find('input').data('id')
                }
            }).done(function (data) {
                $('.companyRegistrationWarning').remove();
                $('.chamber_of_commerce_input').val(data['chamber_of_commerce_number']);
                $('.chamber_of_commerce_establishment_number').val(data['chamber_of_commerce_establishment_number']);
                $('.vat_number').val(data['vat_number']);

                $this.closest('.tab-pane').prepend('<div class="alert alert-success companyRegistrationWarning">De KvK nummers en BTW nummers zijn geupdate.</div>');
            }).fail(function () {
                $('.companyRegistrationWarning').remove();
                $this.closest('.tab-pane').prepend('<div class="alert alert-warning companyRegistrationWarning">De KvK nummers en BTW nummers kunnen momenteel niet worden geupdate.</div>');
            });
        });

        body.on('change', '#_site', function () {
            if ($(this).val() === '1') {
                $('#company_custom_shop_column').removeClass('hidden');

                $.ajax({
                    method: 'GET',
                    url: $('.information-panel').attr('data-company-site-url')
                }).done(function (data) {
                    $('#company_custom_shop_column_content').html(data);
                }).error(function (error) {
                    swal('Foutmelding', 'Gegevens van de bedrijfssite kunnen momenteel niet worden opgehaald', 'error');
                });

            } else {
                $('#company_custom_shop_column').addClass('hidden');
            }
        }).trigger('change');
    };

    CompanyEdit.prototype.alwaysTrigger = function () {
        $('#_customShop').trigger('change');
    };

    companyEdit = new CompanyEdit();
});

var companyEditTrigger = function () {
    companyEdit.alwaysTrigger();
};
