var designSelectModal = null;

function DesignSelectModal(url, options) {

    if (typeof (ContentWindow) === 'undefined') {
        throw 'Missing ContentWindow dependency';
    }

    this.options = $.extend({
        id: 'content_window_select_design',
        width: '600px',
        zIndex: 999999,
        toolbarButtons: [
            {
                position: 'right',
                label: 'Selecteren',
                order: 1,
                classNames: 'btn btn-primary',
                disabled: true,
                id: 'btn_select_design',
                action: 'select-design'
            }
        ]
    }, options);

    this.contentWindow = new ContentWindow(this.options);

    this.open(url);
}

DesignSelectModal.prototype.open = function (url) {
    var self = this;
    this.url = url;
    self.contentWindow.empty().show();

    this.doSearch();
    this.bindEvents()
};

DesignSelectModal.prototype.bindEvents = function () {
    var self = this;
    var contentWindow = self.contentWindow.elm;
    var submitBtn = contentWindow.find('#btn_select_design');
    var searchDelayTime = 250;
    var searchTimeout = null;

    $(contentWindow).on('keyup change', '#searchQuery', function (e) {
        if(searchTimeout) {
            clearTimeout(searchTimeout);
            searchTimeout = null;
        }

        searchTimeout = setTimeout(function () {
            clearTimeout(searchTimeout);
            self.doSearch(e.target.value);
        }, searchDelayTime);
    });

    $(contentWindow).on('change', 'input[name=design]', function (e) {
        var elm = $(this);
        var form = elm.parents('form');
        var value = elm.val();
        var imgSrc = elm.find('+ .tile-content img').attr('src');
        var imgField = form.find('#image_url');

        if (value) {
            submitBtn.removeAttr('disabled');
            imgField.val(imgSrc);
        } else {
            submitBtn.attr('disabled', 'disabled');
            imgField.val('');
        }
    });

    $(contentWindow).on('click', '[data-action=select-design]', function (e) {
        e.preventDefault();

        var elm = $(this);
        var form = elm.parents('.content-window').find('form');
        var data = {};
        var fields = form.serializeArray();

        self.contentWindow.formChange = false;

        for(var key in fields) {
            var field = fields[key];
            data[field.name] = field.value
        }

        if (typeof self.contentWindow.options.successCallback === 'function') {
            self.contentWindow.options.successCallback(data, self.contentWindow);
        }
    });
};

DesignSelectModal.prototype.doSearch = function(searchKey) {
    var self = this;

    if (this.xhr) {
        this.xhr.abort();
    }

    var data = {};
    if(searchKey) {
        data = {
            searchKey: searchKey
        };
    }

    this.xhr = $.ajax({
        method: 'GET',
        url: self.url,
        data: data,
        dataType: 'html'
    })
    .done(function (html) {
        self.contentWindow.setHtml(html);
    })
    .fail(function(){
        swal({
            type: 'error',
            title: 'Oops...',
            text: 'Er is iets fout gegaan.',
        })
    });

    return this;
};

$(function () {
    var bodyElm = $('body');

    bodyElm
        .on('click', '[data-action="design-select"]', function () {
            var elm = $(this);
            var url = elm.data('url');
            designSelectModal = new DesignSelectModal(url, {
                successCallback: function (data, modal) {
                    modal.formChange = false;

                    if (typeof data['design'] !== 'undefined') {
                        elm.parents('.tile-wrapper').find('input').val(data['design']);
                    }

                    if (typeof data['image-url'] !== 'undefined') {
                        var img = $('<img src="' + data['image-url'] + '" />');
                        elm.find('.tile-content').html('').prepend(img);
                        modal.empty().hide();
                    }

                    elm.parents('.tile-wrapper').find('.action').show();
                }
            });
        })
        .on('click', '[data-action="design-unlink"]', function () {
            var elm = $(this);
            var wrapper = elm.parents('.tile-wrapper');
            var input = wrapper.find('input');
            var tileContent = wrapper.find('.tile-content');

            input.val('');
            tileContent.html('<i class="mdi mdi-image-area"></i>');
            elm.hide();
        })
    ;
});
