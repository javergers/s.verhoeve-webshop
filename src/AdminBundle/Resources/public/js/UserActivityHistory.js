var UserActivityHistory = function () {
    this.id = 'user_activity_history_menu';
    this.elm = $('#' + this.id);

    this.xhrRequests = [];
    this.timeout = null;

    this.init = function () {
        this.bindEvents();
    };

    this.onClick = function () {
        var url = this.elm.attr('data-xhr');

        this.clearHistoryRows();
        this.showLoader();

        $.ajax({
            url: url,
            method: 'GET'
        }).done(this.handleResults.bind(this));
    };

    this.handleResults = function(results) {
        this.hideLoader();

        results.forEach(function(result) {
            if(result.hasOwnProperty('log_label')) {
                this.renderLog(result);
            }
        }.bind(this));
    };

    this.showLoader = function() {
        var li = this.elm.find('ul > li:last-child');
        $('<li class="loader"><a href="#">Loading...</a></li>').insertBefore(li);
    };

    this.hideLoader = function() {
        var li = this.elm.find('ul > li:last-child');
        li.closest('ul').find('li.loader').remove();
    };

    this.clearHistoryRows = function() {
        var ul = this.elm.find('ul');
        ul.find('li.loader').remove();
        ul.find('li.history-link').remove();
    };

    this.renderLog = function(data) {
        var li = this.elm.find('ul > li:last-child');
        var elm = $(this.renderHtml(data)).insertBefore(li);

        $('[data-popup="tooltip"]', elm).tooltip();
    };

    this.renderHtml = function (data)
    {
        var entity = data.table_name;
        var entityId = data.entity_id;
        var logLabel = data.log_label;
        var entityLabel = data.entity_label;
        var entityUrl = data.entity_url;
        var icon = this.renderIcon(entity);
        var attr = this.renderAttr(entity, entityId);

        return `<li class="history-link history-${entity}"><a href="${entityUrl}" ${attr}>${icon}<span><strong>${logLabel}</strong> ${entityLabel}</span></a></li>`;
    };

    this.renderAttr = function(entity, entityId) {
        var attributes = [];
        attributes.push('data-popup="tooltip"');
        attributes.push('data-placement="left"');
        attributes.push('data-original-title="Openen"');

        if(entity === 'order') {
            attributes.push('data-action="get-order"');
            attributes.push('data-entity-id="' + entityId + '"');
        }

        return attributes.join(' ');
    };

    this.renderIcon = function(entity) {
        var icon;

        switch (entity) {
            case 'order':
                icon = 'mdi-store';
                break;
            case 'company':
                icon = 'mdi-domain';
                break;
            case 'product':
                icon = 'mdi-flower';
                break;
            case 'customer':
                icon = 'mdi-account-key';
                break;
            default:
                icon = '';
        }

        return `<i class="mdi ${icon}"></i>`;
    };

    this.bindEvents = function () {
        this.elm.on('click', this.onClick.bind(this));

        this.elm.on('click', 'a[data-action="get-order"]', function (e) {
            e.preventDefault();

            var href = $(this).attr('href');
            var orderId = $(this).data('entity-id');

            new Order(href, orderId);

            return false;
        });
    };

    this.init();
};

$(function () {
    new UserActivityHistory();
});
