var DataSetFilterSettingsModal = function(props) {
    ContentWindow.apply(this, arguments);

    this.url = props.url;

    this.onRequestDone = function(data) {
        this.setHtml(data);
        this.bindEvents();
    };

    var optionsParams = {
        toolbarButtons: [
            {
                id: 'save_filters',
                position: 'right',
                label: 'Opslaan en sluiten',
                order: 1,
                classNames: 'btn btn-primary',
                action: 'save-filters'
            }
        ],
        successCallback: function(data, self) {
            if(!data) {
                this.formChange = false;
                this.hide(true, true);
                window.location.reload(true);
            }

            var newForm = $(data).find('form');
            var alerts = newForm.find('.alert-danger');
            var errors = $('<div id="errors"></div>').append(alerts);

            this.elm.find('#errors').remove();
            this.elm.find('form').prepend(errors);

            alerts.each(function(i) {
                // Open all collection-list-item (layers)
                $(this).parents('.collection-list-item').addClass('show');
            });

            this.hideloader();
        }.bind(this)
    };

    this.options = $.extend(true, {}, this.options, optionsParams);

    this.onRequestFail = function() {
        this.hide();

        swal({
            'title': 'Oeps...',
            'html': 'Fout tijdens het openen<br/>van het bewerkscherm.',
            'type': 'error'
        })
    };

    this.onTriggerSubmitForm = function() {
        this.elm.find('form').trigger('submit');
    };

    this.open = function() {
        this.show();
        this.loading();

        var requestXhr = $.ajax({
            url: this.url,
            method: 'GET',
        });

        requestXhr.done(this.onRequestDone.bind(this));
        requestXhr.fail(this.onRequestFail.bind(this));
    };

    this.bindEvents = function() {
        this.elm.on('click', '[data-action=save-filters]', this.onTriggerSubmitForm.bind(this));

        this.elm.find('input.position').closest('.field-container').hide();

        this.elm.on('change', 'select.form-type', function(e) {
            var formTypeSelectElm = $(e.currentTarget);

            this.toggleOptionsCollection(formTypeSelectElm);
        }.bind(this)).trigger('change');

        var formTypeSelectElms = $('select.form-type');
        formTypeSelectElms.each(function(i) {
            var elm = formTypeSelectElms.eq(i - 1);

            this.toggleOptionsCollection(elm);
        }.bind(this));

        this.elm.find('.collection-list').on('add_list_collection_item', function(e) {
            var item = e.originalEvent.detail.item;
            var formTypeSelectElm = item.find('select.form-type');

            item.find('input.position').closest('.field-container').hide();

            this.toggleOptionsCollection(formTypeSelectElm);
        }.bind(this));
    };

    this.toggleOptionsCollection = function(formTypeSelectElm) {
        var isChoiceType = false;
        var formType = null;

        if(formTypeSelectElm) {
            formType = formTypeSelectElm.val();

            if(formType) {
                isChoiceType = formType.indexOf('ChoiceType') !== -1 || formType.indexOf('PriceRange') !== -1;
            }
        }

        var optionsCollectionContainer = formTypeSelectElm.closest('.list-body').find('.field-collection-container');

        if(isChoiceType === false) {
            // Remove existing rows.
            optionsCollectionContainer.hide();
            optionsCollectionContainer.find('.collection-list-item').remove();
        }

        optionsCollectionContainer.toggle(isChoiceType);
    }
};

DataSetFilterSettingsModal.prototype = ContentWindow.prototype;
DataSetFilterSettingsModal.prototype.constructor = DataSetFilterSettingsModal;