function CompanyWizard() {
    this.country = null;
    this.identification = null;
    this.identification_exists = null;
    this.establishment_manually = null;
    this.postcode_check = null;
    this.address_manually = null;
    this.verified = null;
    this.is_customer = null;
    this.is_supplier = null;
    this.customer_groups = null;
    this.supplier_groups = null;
    this.username_exists = false;

    this.init();
};

CompanyWizard.prototype.delay = (function () {
    var timer = 0;

    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();

CompanyWizard.prototype.init = function () {
    this.triggeredSearch = false;

    $('#other_country').select2();

    $('#customer_group').multiselect({
        nonSelectedText: 'Maak een keuze',
        allSelectedText: 'Alles geselecteerd'
    });

    $('#supplier_group').multiselect({
        nonSelectedText: 'Maak een keuze',
        allSelectedText: 'Alles geselecteerd'
    });

    this.bindEvents();
};

CompanyWizard.prototype.bindEvents = function () {
    var self = this;

    $('input[name=country]').on('change', function (e) {
        var value = $('input[name=country]:checked').val();

        self.country = value;
        self.showHideIdentification(value);

        if (!e.isTrigger) {
            self.reset();
        }

        self.check();
    }).trigger('change');

    $('input[name=unregistered_establishment]').on('change', function (e) {
        var checked = $('input[name=unregistered_establishment]').is(':checked');

        if (!self.identification_exists) {
            $(this).removeProp('checked').removeAttr('checked');

            return false;
        }

        $('input[name=kvk]').attr('readonly', true);
        $('input[name=kvk_establishment]').attr('disabled', true);

        if (checked) {
            swal({
                title: 'Weet je zeker',
                text: 'Dat er een (neven)vestiging aangemaakt wordt zonder KvK-registratie?',
                type: 'question',
                showCancelButton: true,
                confirmButtonColor: '#4CAF50',
                confirmButtonText: 'Ja',
                cancelButtonText: 'Nee',
                confirmButtonClass: 'btn btn-success',
                allowEscapeKey: false,
                allowEnterKey: false,
                allowOutsideClick: false
            }).then(
                function () {
                    self.identification = true;
                    self.establishment_manually = true;

                    $('#details', '#identification').show();
                    $('#customer, #organization_settings').show();

                    $('button[data-type="kvk"]').hide();

                    $('.form-group[class*="has-error"], input[class*="error"]').toggleClass('has-error', false).toggleClass('error', false);

                    self.check();

                    var main_address = $('#address_form').clone();

                    self.prepareAddressForm(main_address, 'main');

                    $(main_address).appendTo($('address.main', '#details'));

                    var invoice_address = $('#address_form').clone();

                    self.prepareAddressForm(invoice_address, 'invoice');

                    $(invoice_address).appendTo('address.invoice', '#details');
                }, function (dismiss) {
                    if (dismiss == 'cancel') {
                        self.establishment_manually = false;

                        $('input[name=kvk_establishment]').removeAttr('disabled').removeProp('disabled');
                        $('input[name=unregistered_establishment]').removeProp('checked').removeAttr('checked');
                        $('input[name=kvk]').attr('readonly', false);
                    }
                }
            );
        } else {
            $('input[name=kvk]').removeAttr('readonly', false);
            $('#details', '#identification').find('address').empty();
            $('#details', '#identification').hide();
            $('#customer').hide();
            $('#organization_settings').hide();
            $('button[data-type="kvk"]').show();

            $('input[name=kvk_establishment]').removeAttr('disabled').removeProp('disabled');
        }
    });

    $('address.main').on('keyup change', 'input', function (e) {
        if ($('#invoice_equals_main').is(':checked')) {
            $('input[name="invoice[' + $(this).attr('data-element') + ']"]', 'address.invoice').val($(this).val());
        }

        self.check();
    });

    $('address.invoice').on('keyup change', 'input', function (e) {
        self.check();
    });

    $('address').on('keyup change', 'input[name*=postcode], input[name*=housenumber]', function (e) {
        var parent = $(this).closest('address');

        var postcode = $('input[data-element="postcode"]', parent);
        var housenumber = $('input[data-element="housenumber"]', parent);
        var housenumberAddition = $('input[data-element="housenumber_addition"]', parent);
        var street = $('input[data-element="street"]', parent);
        var city = $('input[data-element="city"]', parent);

        postcode.val(postcode.val().replace(' ', ''));

        if (postcode.val().length < 6 || !housenumber.val()) {
            return;
        }

        CompanyWizard.prototype.delay(function () {
            $('.address-loading', parent).show();

            street.val('').trigger('change');
            city.val('').trigger('change');

            postcode.attr('readonly', true);
            housenumber.attr('readonly', true);
            housenumberAddition.attr('readonly', true);

            $.ajax({
                url: window.location.href + '/get-address',
                data: {
                    postcode: postcode.val(),
                    housenumber: housenumber.val(),
                    housenumberAddition: housenumberAddition.val(),
                    country: 'nl'
                }
            }).done(function (response) {
                if (!response.street) {
                    self.postcode_check = false;
                    self.address_manually = true;

                    street.removeAttr('readonly');
                    city.removeAttr('readonly');

                    return;
                }

                street.attr('readonly', true);
                city.attr('readonly', true);

                self.postcode_check = true;
                self.address_manually = null;

                if (housenumberAddition.val() && !response.houseNumberAddition) {
                    housenumberAddition.val('');
                    housenumberAddition.val('');

                    housenumberAddition.toggleClass('error', true).css({
                        'border-color': '#D84315'
                    });

                    if (housenumberAddition.attr('name').indexOf('invoice') == -1) {
                        $('input[name="invoice[housenumber_addition]"]').val('');
                    }

                    // add tooltip that the given addition does not exist
                } else if (housenumberAddition.val() && response.houseNumberAddition) {
                    housenumberAddition.toggleClass('error', false).css({
                        'border-color': '#ddd'
                    });
                }

                street.val(response.street).trigger('change');
                city.val(response.city).trigger('change');
            }).error(function () {
                self.postcode_check = false;
                self.address_manually = true;

            }).always(function () {
                $('.address-loading', parent).hide();
                postcode.removeAttr('readonly', true);
                housenumber.removeAttr('readonly', true);
                housenumberAddition.removeAttr('readonly', true);
            });
        }, 500);
    });

    $('input[name=is_customer]').on('change', function (e) {
        var value = $('input[name=is_customer]:checked').val();

        self.is_customer = (value == '1');
        self.check();

        $('#customer_groups').toggle((value && value == '1'));

        if (value != '1') {
            $('#verified').hide();
        }
    }).trigger('change');

    $('#customer_group').on('change', function (e) {
        var values = $(this).find('option:selected').length;

        self.customer_groups = (values > 0);
        self.check();

        $('#verified').toggle(values > 0);
    }).trigger('change');

    $('input[name=verified]').on('change', function (e) {
        var value = $('input[name=verified]:checked').val();

        self.verified = value;
        self.check();
    }).trigger('change');

    $('input[name=is_supplier]').on('change', function (e) {
        var value = $('input[name=is_supplier]:checked').val();

        self.is_supplier = (value == '1');
        self.check();

        $('#supplier_groups').toggle((value && value == '1'));
    }).trigger('change');

    $('#supplier_group').on('change', function (e) {
        var values = $(this).find('option:selected').length;

        self.supplier_groups = (values > 0);
        self.check();
    }).trigger('change');

    $('input[type=text], input[type=number]', '#identification').on('keyup', function (e) {
        $(this).closest('.form-group').toggleClass('has-error', !$(this).val());

        $(this).val($(this).val().replace(' ', '').replace('.', ''));

        if (!$(this).val()) {
            self.identification = false;
        }

        self.check();
    });

    $('input[name="customer[email]"]', '#customer').on('keyup', function (e) {
        self.username_exists = false;

        self.check();
    });

    $(':input[name^=customer], #supplier_group').on('change keyup', function (e) {
        $(this).closest('.form-group').toggleClass('has-error', !$(this).val());

        self.check();
    });

    $('button', '#identification').on('click', function (e) {
        e.preventDefault();

        var button = $(this);
        var input = $('.' + button.attr('data-type') + '-input');
        var inputEstablishment = $('.' + button.attr('data-type') + '-establishment-input');
        var btw_error = false;

        if (input.val() && self.country == 'be' && input.val().substr(0, 2).toLowerCase() != 'be') {
            btw_error = true;
        } else if (input.val() && self.country == 'other' && (!$('#other_country').val() || input.val().substr(0, 2).toLowerCase() != $('#other_country').val().toLowerCase())) {
            btw_error = true;
        }

        if (!input.val() || btw_error) {
            input.closest('.form-group').toggleClass('has-error', true);

            return;
        }

        self.identification_exists = null;

        button.toggleClass('disabled', true);
        button.find('i').toggleClass('icon-reload-alt', false).toggleClass('icon-spinner10', true).toggleClass('spinner', true);

        $.ajax({
            'url': window.location.href + '/identification',
            'dataType': 'json',
            'data': {
                type: button.attr('data-type'),
                value: input.val(),
                establishment: inputEstablishment.val()
            }
        }).done(function (response) {
            if (response.error) {
                input.closest('.form-group').toggleClass('has-error', true);

                if (!response.exists && !response.parentExists) {
                    swal({
                        title: 'Hoofdvestiging bestaat nog niet',
                        text: 'De relatie die u probeert aan te maken staat bij de Kamer van Koophandel geregistreerd als nevenvestiging, u dient eerst de hoofdvestiging aan te maken.',
                        type: 'error',
                        showConfirmButton: false,
                        showCancelButton: true,
                        cancelButtonText: 'Annuleren',
                        allowEscapeKey: false,
                        allowEnterKey: false,
                        allowOutsideClick: false
                    }).then(function () {
                        window.location.href = response.company.url;
                    });

                    self.identification_exists = true;
                } else if (response.exists) {
                    swal({
                        title: 'Identificatie nummer bestaat al',
                        text: 'Er is al een klant met de naam \'' + response.company.name + '\' bekend onder dit identificatie nummer.',
                        type: 'error',
                        showCancelButton: true,
                        confirmButtonColor: '#4CAF50',
                        confirmButtonText: 'Open klantprofiel',
                        cancelButtonText: 'Toch doorgaan',
                        confirmButtonClass: 'btn btn-success',
                        allowEscapeKey: false,
                        allowEnterKey: false,
                        allowOutsideClick: false
                    }).then(function () {
                        var dataTableJson = JSON.parse(localStorage.getItem('DataTables_DataTables_Table_0_/admin/klanten/bedrijven/'));
                        dataTableJson['search']['search'] = response.company.name;
                        localStorage.setItem('DataTables_DataTables_Table_0_/admin/klanten/bedrijven/', JSON.stringify(dataTableJson));
                        window.location.href = response.company.url;
                    }, function (dismiss) {
                        if (dismiss == 'cancel') {
                            //$('.unregistered_establishment', '#identification').show();
                            fillIdentifiedData(input, response.identification)
                        }
                    });

                    self.identification_exists = true;
                }
            } else {
                fillIdentifiedData(input, response);
            }
        }).error(function (response) {
            input.closest('.form-group').toggleClass('has-error', true);
            input.removeAttr('readonly')
            .removeProp('readonly');

            $('#details', '#identification').find('address').empty();
            $('#details', '#identification').hide();
            $('#customer').hide();
            $('#organization_settings').hide();

            // @todo: meer hiden

            self.identification = false;
        }).always(function () {
            button.toggleClass('disabled', false);
            button.find('i').toggleClass('icon-reload-alt', true).toggleClass('icon-spinner10', false).toggleClass('spinner', false);

            self.check();
        });
    });

    function fillIdentifiedData(input, response) {
        input.closest('.form-group').toggleClass('has-error', false);
        input.attr('readonly', true)
        .prop('readonly', true);

        if (response && response.address) {
            $('#details', '#identification').find('address.main').html([
                response.name, response.address.street + ' ' + [response.address.houseNumber, response.address.houseNumberAddition].join(' '), response.address.postcode + ' ' + response.address.city
            ].join('<br />'));
        }

        if (response && response.postaddress) {
            $('#details', '#identification').find('address.invoice').html([
                response.name, response.postaddress.street + ' ' + [response.postaddress.houseNumber, response.postaddress.houseNumberAddition].join(' '), response.postaddress.postcode + ' ' + response.postaddress.city
            ].join('<br />'));
        } else {
            $('#details', '#identification').find('address.invoice').html('<em>Er is geen apart factuuradres bekend.</em>');
        }

        $('#details', '#identification').show();
        $('#organization_settings').show();
        $('#customer').show();

        self.identification = true;
    }

    $('address.invoice').on('change', '#invoice_equals_main', function (e) {
        var address = $('address.invoice');

        if ($(this).is(':checked')) {
            $('input[type="text"]', address).each(function () {
                $(this).val($('input[name="main[' + $(this).attr('data-element') + ']"]', 'address.main').val());
            }).attr('readonly', true);
        } else {
            $('input[type="text"]', address)
            .not($('input[name="invoice[street]"]', address))
            .not($('input[name="invoice[city]"]', address))
            .removeAttr('readonly')
            .removeProp('readonly');
        }
    });

    $('#wizard_reset').on('click', function (e) {
        e.preventDefault();

        swal({
            title: 'Weet je het zeker?',
            text: 'Alle gegevens worden gewist.',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#4CAF50',
            cancelButtonColor: '#F44336',
            confirmButtonText: 'Ik weet het zeker',
            cancelButtonText: 'Annuleren',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger'
        }).then(function () {
            self.reset();

            $('input[name=country]').trigger('change');
        });
    });

    $('#wizard_submit').on('click', function (e) {
        if (!e.isTrigger) {
            e.preventDefault();

            $.ajax({
                url: window.location.href + '/check-username',
                dataType: 'json',
                data: {
                    email: $('input[name="customer[email]"]').val()
                }
            }).done(function (response) {
                if (response.error) {
                    $('input[name="customer[email]"]').toggleClass('error', true).toggleClass('valid', false);
                    $('input[name="customer[email]"]').closest('.form-group').toggleClass('has-error', true).toggleClass('has-success', false);

                    self.username_exists = true;

                    self.check();
                } else {
                    $('input[name="customer[email]"]').toggleClass('error', false).toggleClass('valid', true);
                    $('input[name="customer[email]"]').closest('.form-group').toggleClass('has-error', false).toggleClass('has-success', true);

                    self.username_exists = false;

                    $('#wizard_submit').trigger('click');
                }
            }).error(function (response) {

            }).always(function () {

            });
        }
    })
};

CompanyWizard.prototype.prepareAddressForm = function (address, type) {
    $('input', address).each(function (i, elm) {
        var name = type + '[' + $(elm).attr('data-element') + ']'

        $(elm).attr('name', name);
    });

    if (type == 'invoice') {
        $('input[type="text"]', address).attr('readonly', true);
        $('input[name="invoice[name]"]', address).remove();
    } else {
        $('input[name="' + type + '[street]"]', address).attr('readonly', true);
        $('input[name="' + type + '[city]"]', address).attr('readonly', true);
    }

    $('.invoice_equals_main_container', 'address.main').remove();
};

CompanyWizard.prototype.showHideIdentification = function (country) {
    var self = this;

    $('#kvk, #btw, #other_countries').hide();
    $('#details', '#identification').find('address').empty();
    $('#details', '#identification').hide();
    $('#customer').hide();

    this.identification = null;

    switch (country) {
        case 'nl':
            $('#kvk').show();

            break;
        case 'be':
            $('#btw').show();

            break;
        case 'other':
            $('#btw, #other_countries').show();

            break;
    }

    $('input[type=text], input[type=number]', '#identification')
    .removeAttr('readonly')
    .removeProp('readonly')
};

CompanyWizard.prototype.reset = function () {
    this.country = null;
    this.identification = null;
    this.verified = null;

    $('input[type=text], input[type=number]', '#identification').val('');
    $('input[type=text], input[type=number]', '#identification').removeAttr('readonly').removeProp('readonly');
    $('input[type=text], input[type=number]', '#identification').closest('.form-group').removeClass('has-success');
};

CompanyWizard.prototype.check = function () {
    var self = this;
    var check = true;

    if (!self.country) {
        check = false;
    }

    if (!self.identification) {
        check = false;
    }

    if (!self.is_customer && !self.is_supplier) {
        check = false;
    } else {
        if (self.is_customer && !self.customer_groups) {
            check = false;
        }
    }

    if (self.is_customer && (self.verified === null || typeof(self.verified) == 'undefined')) {
        check = false;
    }

    if (!$('select[name="customer[gender]"]').val()
        || !$('input[name="customer[firstname]"]').val()
        || !$('input[name="customer[lastname]"]').val()
        || !$('input[name="customer[email]"]').val()
        || !$('select[name="customer[locale]"]').val()
        || !$('select[name="customer[site]"]').val()
        || !$('select[name="customer[customerGroup]"]').val()
    ) {
        check = false;
    }

    /**
     * Check address fields in case of manually establishment process
     */
    if (self.establishment_manually) {
        // Check main address
        if (!$('input[name="main[name]"]').val()
            || !$('input[name="main[postcode]"]').val()
            || !$('input[name="main[housenumber]').val()
            || !$('input[name="main[street]"]').val()
            || !$('input[name="main[city]"]').val()
        ) {
            check = false;
        }

        // Check invoice address
        if (!$('input[name="invoice[postcode]"]').val()
            || !$('input[name="invoice[housenumber]').val()
            || !$('input[name="invoice[street]"]').val()
            || !$('input[name="invoice[city]"]').val()
        ) {
            check = false;
        }
    }

    if (self.username_exists) {
        check = false;
    }

    if (!check) {
        $('#wizard_submit, #wizard_reset')
        .attr('disabled', true)
        .prop('disabled', true);
    } else {
        $('#wizard_submit, #wizard_reset')
        .removeAttr('disabled')
        .removeProp('disabled');
    }
};

new CompanyWizard();
