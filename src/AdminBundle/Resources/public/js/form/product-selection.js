function ProductSelection(url) {
    this.contentWindow = new ContentWindow({
        width: window.innerWidth * 0.4 + 'px',
        zIndex: 2000
    });

    this.searchTimer = null;
    this.searchXhr = null;

    this.init(url);
}

ProductSelection.prototype.init = function (url) {
    var self = this;

    this.url = url;
    this.contentWindow.empty().show();
    this.contentWindow.setOption('refreshUrl', this.url);
    this.contentWindow.setOption('refreshCallback', function () {
        self.initialize();
    });

    $('#searchQuery').unbind();
    $(document).off('click', '.col-product-item');

    $.get(url).done(function (data) {
        self.contentWindow.setHtml(data);
        self.bindEvents(url);
    });
};

ProductSelection.prototype.renderProduct = function(data) {
    $('.products').empty();

    $.each(data, function (k, product) {
        var productImage = '';
        var price = '' + product.price.toFixed(2);

        if(product.image) {
            productImage = '<img src="' + product.image + '" />';
        } else {
            productImage = '<div class="product-item-image-placeholder">Geen afbeelding ingesteld</div>';
        }

        var productElm = $('<div class="col-product-item" data-toggle="tooltip" data-placement="right" title="' + product.title + '" data-product-id="' + product.id + '" style="width: 24%;">' +
            '<div class="product-item selectable">' +
            '<div class="product-item-image">' + productImage + '</div>' +
            '<div class="product-item-description">' + product.title + '</div>' +
            '<div class="product-item-price">&euro; ' + price.replace('.', ',') + '</div>' +
            '</div>' +
            '</div>').tooltip();

        $('.products').append(productElm);
    });
};

ProductSelection.prototype.searchProducts = function(url, val) {
    var self = this;

    if(this.searchXhr) {
        this.searchXhr.abort();
        this.searchXhr = null;
    }

    this.searchXhr = $.ajax({
        url: url,
        method: 'POST',
        data: {
            query: val
        }
    })
        .done(self.renderProduct.bind(this))
        .always(function() {
            self.searchXhr = null;
        })
    ;
};

ProductSelection.prototype.bindEvents = function (url) {
    var self =this;
    var cWindow = this.contentWindow;

    $('#searchQuery').on('change keyup', function () {
        var val = $(this).val();

        if(self.searchTimer) {
            clearTimeout(self.searchTimer);
        }

        this.searchTimer = setTimeout(function() {
            clearTimeout(self.searchTimer);
            self.searchTimer = null;

            self.searchProducts(url, val);
        }, 300);
    });

    $(document).on('click', '.col-product-item', function () {
        var selectedProducts = JSON.parse($('#selectedProducts').val());
        var productIds = [];
        var productId = $(this).attr('data-product-id');
        var $this = $(this);

        $.each(selectedProducts, function (key, product) {
            productIds.push(product.id);
        });

        $.ajax({
            url: $('.select-product-panel').attr('data-combination-url'),
            method: 'POST',
            data: {
                product: productId,
                products: productIds
            }
        }).then(function () {
            self.addProductToInput({
                id: $this.attr('data-product-id'),
                title: $this.find('.product-item-description').html(),
                image: $this.find('.product-item-image img').attr('src'),
                price: $this.find('.product-item-price').html()
            });
        }, function (data) {
            var msg = 'Het geselecteerde product kan niet worden gecombineerd met de huidige producten.';
            if (data.responseJSON) {
                msg = data.responseJSON;
            }

            swal({
                type: 'error',
                title: 'Foutmelding',
                text: msg
            });
        });
    });

    $(document).on('click', '.product-delete', function () {
        var productElm = $(this).closest('.product');
        var productId = productElm.attr('data-product-id');

        var selectedProductsInput = $('#selectedProducts');
        var selectedProducts = JSON.parse(selectedProductsInput.val());

        $.each(selectedProducts, function (key, product) {
            if (productId === product.id) {
                selectedProducts.splice(key, 1);
            }
        });

        selectedProductsInput.val(JSON.stringify(selectedProducts));
        selectedProductsInput.trigger('change');
        productElm.remove();
    });

};

ProductSelection.prototype.addProductToInput = function (product) {
    var selectedProductsInput = $('#selectedProducts');
    var selectedProducts = JSON.parse(selectedProductsInput.val());

    selectedProducts.push({id: product.id, quantity: 1});
    selectedProductsInput.val(JSON.stringify(selectedProducts));

    var productImage;
    if(!!product.image) {
        productImage = `<img src="${product.image}">`;
    } else {
        productImage = '<div class="product-item-image-placeholder">Geen afbeelding ingesteld</div>';
    }


    $('.product-container').append(`<div class="col-md-3 product" data-product-id="${product.id}" data-quantity="${product.quantity}" style="width: 24%;">
        <div class="product-delete"><i class="fa fa-remove"></i></div>
        <div class="product-item">
        <div class="product-item-image">
            ${productImage}
        </div>
        <div class="product-item-description">${product.title}</div>
        <div class="product-item-price">${product.price}</div>
        </div></div>`);
    selectedProductsInput.trigger('change');

    this.contentWindow.hide();
};
