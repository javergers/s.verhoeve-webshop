$(document).ready(function () {
    var discountAmount = $('#catalog_promotion_container_discount_discount_amount');
    var discountPercentage = $('#catalog_promotion_container_discount_discount_percentage');

    $(discountAmount).on('change', function () {
        if (parseFloat($(this).val()) > 0 && ($(discountPercentage).val() != '0' && $(discountPercentage).val() != '')) {
            bootbox.alert('Kortingsbedrag en kortingspercentage kunnen niet gelijktijdig gebruikt worden.<br />Kortingspercentage is op 0 gezet.');

            $(discountPercentage).val('0');
        }
    });

    $(discountPercentage).on('change', function () {
        if (parseFloat($(this).val()) > 0 && ($(discountAmount).val() != '0.00' && $(discountAmount).val() != '')) {
            bootbox.alert('Kortingspercentage en kortingsbedrag kunnen niet gelijktijdig gebruikt worden.<br />Kortingsbedrag is op 0 gezet.');

            $(discountAmount).val('0.00');
        }
    });
});
