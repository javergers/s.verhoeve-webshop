function AssortmentProductTool() {
    this.init();
};

AssortmentProductTool.prototype.init = function () {
    this.triggeredSearch = false;
    this.product_search_timer = false;

    this.bindSortables();
    this.bindEvents();
    this.process();
};

AssortmentProductTool.prototype.bindEvents = function () {
    var self = this;

    var contentSize = $('#assortment_container_contentSize');
    var contentPosition = $('#assortment_container_contentPosition');
    var content = $('#assortment_container_content');

    content.on('change', function () {
        contentSize.closest('.form-group').hide();
        contentPosition.closest('.form-group').hide();

        if ($(this).val()) {
            contentSize.closest('.form-group').show();
            contentPosition.closest('.form-group').show();
        }
    });
    content.trigger('change');

    $('#product_selector_search').bind('keypress keydown keyup', function (e) {
        clearTimeout(self.product_search_timer);
        self.product_search_timer = setTimeout(self.searchProducts.bind(self), 800);

        if (e.keyCode === 13) {
            e.preventDefault();
            clearTimeout(self.product_search_timer);
            self.searchProducts();
        }
    });
};

AssortmentProductTool.prototype.bindSortables = function () {
    var self = this;

    var productPanel = document.getElementById('assortment_products_sortable');
    if(productPanel) {
        var sort = Sortable.create(productPanel, {
            group: {
                name: 'product-container',
                pull: true,
                put: true
            },
            animation: 150,
            draggable: '.col-product-item',
            handle: '.col-product-item',
            ghostClass: 'ghost',
            onAdd: function (evt) {
                $('[data-product-id="' + $(evt.item).attr('data-product-id') + '"]').toggleClass('col-product-item-present', true);
                self.reevaluateSearchResponse();

                self.addInputs($(evt.item));

                self.process();
            },
            onUpdate: function (evt) {
                self.process();
            },
            onStart: function () {
                // toon trash balk
                $('.product-trash').fadeIn('fast');
            },
            onEnd: function () {
                $('.product-trash').fadeOut('fast');
            }
        });
    }

    var productTrash = document.getElementById('product_trash');

    if(productTrash) {
        Sortable.create(productTrash, {
            group: {
                name: 'product-container',
                pull: false,
                put: true
            },
            animation: false,
            ghostClass: false,
            sort: false,
            onAdd: function (evt) {
                var item = $(evt.item);

                // Remove it from the grid
                $(evt.item).remove();

                // Make it visible in the search
                $('[data-product-id="' + $(item).attr('data-product-id') + '"]', '#product_selector_result_container').toggleClass('col-product-item-present', false);

                self.reevaluateSearchResponse();
                self.process();
            },
        });
    }
};

AssortmentProductTool.prototype.addInputs = function (elm) {
    var prototype = $('#assortment_products_container').attr('data-prototype');
    var totalItems = $('.product-item', '#assortment_products_sortable').length;

    $('.product-item-form-elements', elm).html(prototype.replace(/__index__/g, ++totalItems));
    $('.product-item-form-elements input[type=number][name*="[product]"]', elm).val($(elm).attr('data-product-id'));
    $('.product-item-form-elements input[type=number][name*="[upsellProduct]"]', elm).val($(elm).attr('data-product-id'));
};

AssortmentProductTool.prototype.process = function () {
    $('input[type=number][name*="[position]"]', '#assortment_products_sortable').each(function (i, elm) {
        $(elm).val(i);
    });
};

AssortmentProductTool.prototype.resetSearch = function () {
    $('.product-selector-container').html('').html('<p class="search-placeholder">Typ in bovenstaand invoerveld de zoekopdracht.</p>');
};

AssortmentProductTool.prototype.reevaluateSearchResponse = function (element) {
    if (typeof element === 'undefined') {
        element = $('.product-selector-container');
    }

    $('.product-selector-result-all-used', $(element)).remove();

    // Hide categories where all items are present in the assortment
    $('.product-selector-result-category', $(element)).each(function (i, elm) {
        var totalItems = $('.col-product-item', $('[data-category-id="' + $(elm).attr('id') + '"]', $(element))).length;
        var totalHiddenItems = $('.col-product-item-present', $('[data-category-id="' + $(elm).attr('id') + '"]', $(element))).length;

        if (totalItems === totalHiddenItems) {
            $('<div class="product-selector-result-all-used">Alle producten in deze groep zijn al gekoppeld.</div>').prependTo($('.product-selector-result-products[data-category-id="' + $(elm).attr('id') + '"]', $(element)));
        } else {
            $('.product-selector-result-all-used', $('.product-selector-result-products[data-category-id="' + $(elm).attr('id') + '"]', $(element))).remove();
        }
    });
};
AssortmentProductTool.prototype.searchProducts = function () {
    var self = this;
    var search_value = $('#product_selector_search').val();

    if (!search_value) {
        self.resetSearch();

        return false;
    }

    if (self.triggeredSearch) {
        return;
    }

    self.triggeredSearch = true;

    var products_container = $('#assortment_products_container');
    var assortmentType = $('#assortment_container_assortmentType');

    if(!assortmentType.length) {
        assortmentType = $('#company_assortment_form_container_assortmentType');
    }

    var ajax_url = products_container.attr('data-search-url');
    var assortment_id = products_container.attr('data-assortment-id');
    var product_selector_container = $('.product-selector-container');

    product_selector_container.empty();
    var loading = $('<br /><center><i class="icon-spinner2 spinner"></i></center>').appendTo(product_selector_container);

    $.ajax({
        'url': ajax_url,
        'data': {
            'assortment': assortment_id,
            'query': search_value,
            'isCardAssortment': parseInt(assortmentType.val()) === 2 ? 1 : 0,
            'isCompanyAssortment': parseInt(products_container.attr('data-company-assortment')),
            'company': parseInt(products_container.attr('data-company'))
        }
    }).done(function (data) {
        var response = $(data);

        self.reevaluateSearchResponse(response);

        product_selector_container.html(response);

        $('.col-product-item-present', $('#product_selector_result_container', product_selector_container)).each(function (i, elm) {
            if ($('[data-product-id="' + $(elm).attr('data-product-id') + '"]', '#assortment_products_sortable').length === 0) {
                $(elm).toggleClass('col-product-item-present', false);
            }
        });

        self.reevaluateSearchResponse();

        $('.product-selector-result-products').each(function (i, elm) {
            var element = $(elm).get(0);

            Sortable.create(element, {
                group: {
                    name: 'product-container',
                    pull: 'clone',
                    put: false
                },
                animation: 150,
                draggable: '.col-product-item',
                handle: '.col-product-item',
                ghostClass: 'ghost',
                sort: false
            });
        });

    }).fail(function (response) {
    }).always(function () {
        self.triggeredSearch = false;
        loading.remove();
    });
};

new AssortmentProductTool();
