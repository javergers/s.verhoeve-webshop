function NavigationBuilder() {
    this.triggeredSearch = false;
    this.markedForRemoval = [];
    this.source = [];
    this.defaultLocale = null;
    this.contentCKEditors = [];
    this.max_levels = 0;
    this.max_root_items = 0;
    this.max_child_items = 0;
}

NavigationBuilder.prototype.init = function () {
    var self = this;

    var menuSortableListElm = $('#menu_sortable_list');
    var maxLevels = menuSortableListElm.attr('data-max-levels');
    var maxRootItems = menuSortableListElm.attr('data-max-root-items');
    var maxChildItems = menuSortableListElm.attr('data-max-child-items');

    self.menu_list = $('#menus');
    self.menu_sortable_list = menuSortableListElm;
    self.defaultLocale = menuSortableListElm.attr('data-default-locale');

    if (typeof maxLevels !== "undefined") {
        self.max_levels = parseInt(maxLevels);
    }

    if (typeof maxRootItems !== "undefined") {
        self.max_root_items = parseInt(maxRootItems);
    }

    if (typeof maxChildItems !== "undefined") {
        self.max_child_items = parseInt(maxChildItems);
    }

    self.bindEvents();
    self.initMultiSelect();
    self.initUniform();
    self.initNestedSortable();
};

NavigationBuilder.prototype.bindEvents = function () {
    var self = this;

    $('#menu_select_button').on('click', function (e) {
        e.preventDefault();

        if ($(self.menu_list).val()) {
            window.location.href = $(self.menu_list).find('option:selected').attr('data-url');
        }
    });

    $('#search_item').bind('keypress keydown keyup', function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();

            if (!$(this).val()) {
                self.resetSearch();

                return false;
            }

            if (self.triggeredSearch) {
                return;
            }

            self.triggeredSearch = true;

            var ajax_url = $('#search_result').attr('data-search-url');

            $.ajax({
                'url': ajax_url,
                'data': {
                    'keyword': $(this).val()
                }
            }).done(function (response) {
                response = $(response);

                $('#search_result').html(response).show();
                $('#search_result_button').show();

                $.each($('.init-styled', '#search_result'), function (i, elm) {
                    $(elm).uniform({
                        radioClass: 'choice',
                        wrapperClass: 'bg-white'
                    });
                });
            }).fail(function () {
                $('#search_result').html('').hide();
                $('#search_result_button').hide();
            }).always(function () {
                self.triggeredSearch = false;
            });
        }
    });

    $('button', '#panel_custom_link').on('click', function (e) {
        e.preventDefault();

        var $linkUrl = $('input[name="custom_link[url]"]');
        var $linkText = $('input[name="custom_link[text]"]');
        var $linkBlank = $('input[name="custom_link[blank]"]');

        if (!$linkText.val() || ($linkUrl.val().trim().substr(0, 1) != '#' && !self.validateUrl($linkUrl.val()))) {
            return false;
        }

        self.addMenuItem({
            label: $linkText.val(),
            url: $linkUrl.val(),
            blank: $linkBlank.prop('checked'),
            type: 'custom'
        });

        $linkUrl.val('');
        $linkText.val('');
        $linkBlank.removeAttr('checked').removeProp('checked');

        $.uniform.update();
    });

    $('#search_result_button').on('click', function (e) {
        e.preventDefault();

        var search_result = $('#search_result');

        if ($(':checked', search_result).length == 0) {
            return false;
        }

        $.each($(':checked', search_result), function (i, elm) {
            self.addMenuItem({
                label: $(elm).attr('data-label'),
                type: $(elm).attr('data-type'),
                entity_id: $(elm).attr('data-id'),
                entity_name: $(elm).attr('data-entity')
            });

            $(this).removeAttr('checked').removeProp('checked');
        });

        $.uniform.update();
    });

    $('.button_save').on('click', function (e) {
        e.preventDefault();

        self.save();
    });

    $(self.menu_sortable_list).on('keyup', '.menu_item_url input', function (e) {
        e.preventDefault();

        var container = $(this).closest('li.sortable');

        if (!$(this).val()) {
            container.removeAttr('data-url');
        } else {
            container.attr('data-url', $(this).val());
        }
    });

    $(self.menu_sortable_list).on('keyup', '.menu_item_title input[name]', function (e) {
        e.preventDefault();

        var container = $(this).closest('li.sortable');
        var panel_container = $(this).closest('.panel');

        if (!$(this).val()) {
            container.removeAttr('data-label');
        } else {
            container.attr('data-label', $(this).val());
        }

        if ($(this).attr('data-locale') == self.defaultLocale) {
            container.find('.panel-title:first').html($(this).val());
        }

        if (!panel_container.hasClass('custom-item') && $(this).val()) {
            $('.menu_entity_original', panel_container).show();
        } else {
            $('.menu_entity_original', panel_container).hide();

            container.find('.panel-title:first').html($('.menu_entity_original', panel_container).find('span').html());
        }
    });

    $(self.menu_sortable_list).on('change', '.menu_item_blank input', function (e) {
        e.preventDefault();

        var container = $(this).closest('li.sortable');

        container.attr('data-blank', $(this).is(':checked'));

        $.uniform.update();
    });

    $(self.menu_sortable_list).on('click', '[data-action="collapse"]', function (e) {
        e.preventDefault();

        if ($('.panel-configuration', $(this).closest('.panel')).is(':visible')) {
            $('.panel-configuration', $(this).closest('.panel')).hide();
        }
    });

    $(self.menu_sortable_list).on('click', '.action_delete a', function (e) {
        e.preventDefault();

        if (!confirm('Weet je zeker dat dit menu item verwijdert mag worden?')) {
            return false;
        }

        self.removeMenuItem($(this).closest('li.sortable'));
    });

    $(self.menu_sortable_list).on('click', '.action_cancel a', function (e) {
        e.preventDefault();

        $(this).closest('.panel').find('[data-action="collapse"]').trigger('click');
    });

    $(self.menu_sortable_list).on('click', '.action_options a', function (e) {
        e.preventDefault();

        var panel_configuration = $(this).closest('.panel').find('.panel-configuration');

        panel_configuration.removeClass('ui-sortable-handle');
        panel_configuration.find('div').removeClass('ui-sortable-handle');

        panel_configuration.fadeToggle();
    });

    $(self.menu_sortable_list).on('mousedown', '.panel-configuration', function (e) {
        var type = $(e.target).get(0).type;

        if (!type || (type && type != 'textarea')) {
            e.preventDefault();
            e.stopPropagation();

            return false;
        }
    });

    $(self.menu_sortable_list).on('show.bs.tab', '.panel-configuration .nav-tabs-bottom > li > a[data-toggle="tab"]', function (e) {
        var tab_type = $(e.target).attr('data-tab-type');
        var tab_id = $(e.target).attr('data-tab-id');
        var tab = $('#' + tab_id);
        var container = $(tab).closest('li.sortable');

        switch (tab_type) {
            case 'html':
                if ($('li.active', tab).length == 0) {
                    $('li[data-default-locale] a', tab).trigger('click');
                }

                break;
            case 'accessibility':
                $('.tab-accessibility-menu-entity, .tab-accessibility-menu-manual', container).hide();

                if ((container.attr('data-entity-id') && container.attr('data-entity-name')) || (container.attr('data-entity_id') && container.attr('data-entity_name'))) {
                    $('.tab-pane-accessibility', container).toggleClass('loading', true);

                    $.ajax({
                        data: {
                            action: 'get-entity-accessibility',
                            entityId: container.attr('data-entity_id') ? container.attr('data-entity_id') : container.attr('data-entity-id'),
                            entityName: container.attr('data-entity_name') ? container.attr('data-entity_name') : container.attr('data-entity-name')
                        }
                    }).done(function (response) {
                        if (!response || !response.hasAccessibility) {
                            $('.tab-accessibility-menu-manual', container).show();

                            self.initMultiSelect($('.init-multiselect', container));

                            return;
                        }

                        $('.tab-accessibility-menu-entity', container).show();

                        $('.entity-display-title', $('.tab-accessibility-menu-entity', container)).html(response.displayTitle);

                        var visibleFor = 'Iedereen';

                        if (response.loginRequired == true) {
                            visibleFor = 'Ingelogde gebruikers';
                        }

                        $('.entity-login-required', $('.tab-accessibility-menu-entity', container)).html(visibleFor);

                        var customerGroups = 'Geen selectie gemaakt.';

                        if (response.customerGroups && Object.keys(response.customerGroups).length > 0) {
                            customerGroups = [];

                            $.each(response.customerGroups, function (i, customerGroup) {
                                customerGroups.push(customerGroup);
                            });

                            customerGroups = customerGroups.join(', ');
                        }

                        $('.entity-customer-groups', $('.tab-accessibility-menu-entity', container)).html(customerGroups);
                    }).error(function (response) {

                    }).always(function () {
                        $('.tab-pane-accessibility', container).toggleClass('loading', false);
                    });
                } else {
                    $('.tab-accessibility-menu-manual', container).show();
                }

                break;
        }
    });

    $(self.menu_sortable_list).on('click', '.menu-item-content-tabs li > a', function () {
        var panel = $(this).closest('.panel');
        var panel_container = panel.closest('li.sortable');

        var tab_id = $(this).attr('data-tab-id');
        var tab = $('.tab-pane[id=' + tab_id + ']');

        var element = $('textarea', tab);
        var element_id = element.attr('id');

        self.destroyCKEditors(panel_container);

        var toolbar = [
            {
                name: 'basicstyles',
                items: ['Bold', 'Italic', 'Underline', 'Strike', '-', 'RemoveFormat']
            }
        ];

        var options = {
            height: ((panel.hasClass('custom-item')) ? 143 : 103) + 'px',
            toolbar: toolbar,
            skin: 'minimalist',
            removePlugins: 'elementspath'
        };

        var editor = CKEDITOR.replace(element_id, options);

        if (typeof(self.contentCKEditors[panel_container.attr('id')]) === 'undefined') {
            self.contentCKEditors[panel_container.attr('id')] = [];
        }

        editor.on('instanceReady', function (event) {
            self.contentCKEditors[panel_container.attr('id')].push(element_id);

            var item_index = $('li.sortable').index(panel_container);
            var z_index = Math.abs(item_index - 100);

            $('.panel-configuration', panel_container).css({
                'z-index': z_index
            });

            event.editor.on('change', function () {
                event.editor.updateElement();
            });

            event.editor.on('resize', function (resizeEvent) {

                var container = $(event.editor.element.$).closest('.sortable');

                var calculatedHeight = 105 + resizeEvent.data.contentsHeight;

                $('.panel-configuration', container).css({
                    'height': calculatedHeight
                });
            });
        });
    });

    $('#button_delete_menu').on('click', function (e) {
        if (!confirm('Weet u zeker dat dit menu verwijdert moet worden?')) {
            e.preventDefault();
        }
    });
};

NavigationBuilder.prototype.initUniform = function (elements) {
    var self = this;

    if (!elements) {
        var elements = $('.init-styled', $(self.menu_sortable_list));
    }

    var settings = {
        radioClass: 'choice',
        wrapperClass: 'bg-white'
    };

    $(elements).each(function (i, elm) {
        $(elm).uniform(settings);
    });
};

NavigationBuilder.prototype.initMultiSelect = function (elements) {
    var self = this;

    if (!elements) {
        var elements = $('select.init-multiselect', $(self.menu_sortable_list));
    }

    var settings = {
        enableClickableOptGroups: true,
        nonSelectedText: 'Geen selectie',
        selectAllText: 'Selecteer alles',
        allSelectedText: 'Alles geselecteerd',
        numberDisplayed: 9,
        buttonClass: 'btn btn-xs btn-default',
        templates: {
            divider: '<div class="divider" data-role="divider"></div>',
            filter: '<li class="multiselect-item multiselect-filter"><i class="icon-search4"></i> <input class="form-control" type="text"></li>'
        },
        includeSelectAllOption: true,
        selectAllJustVisible: true,
        onChange: function (option, checked, select) {
            $('input[value=\'multiselect-all\']', $(select).parents('.multi-select-full'))
            .attr('checked', false)
            .prop('checked', false);

            $.uniform.update();
        },
        onSelectAll: function () {
            $.uniform.update();
        }
    };

    $(elements).each(function (i, elm) {
        $(elm).multiselect(settings);
    });

    // Styled checkboxes and radios
    $.each(elements, function (i, elm) {
        $('.multiselect-container :checkbox', $(elm).closest('.tab-pane')).uniform({radioClass: 'choice'});
    });
};

NavigationBuilder.prototype.addError = function (id, message) {
    var error_container = $('#menu_sortable_errors ul');

    error_container.empty();
    error_container.append('<li data-error-id="' + id + '">' + message + '</li>');
    error_container.closest('#menu_sortable_errors').show();
};

NavigationBuilder.prototype.removeError = function (id) {
    var error_container = $('#menu_sortable_errors ul');

    error_container.find('[data-error-id="' + id + '"]').remove();

    if (error_container.find('li').length == 0) {
        error_container.closest('#menu_sortable_errors').hide();
    }
};

NavigationBuilder.prototype.resetErrors = function () {
    $('#menu_sortable_errors ul').empty();
    $('#menu_sortable_errors').hide();
};

NavigationBuilder.prototype.validateSubMenuItems = function (items) {
    var errors_found = 0;

    var isLimited = !!this.max_child_items;

    if (!isLimited) {
        return errors_found;
    }

    this.removeError('error.max_child_items');

    for (var k in items) {
        var item = items[k];

        if (!item.hasOwnProperty('children')) {
            continue;
        }

        if (item.children.length > this.max_child_items) {
            this.addError('error.max_child_items', 'Er kunnen maximaal ' + this.max_child_items + ' menu-items in het submenu aanwezig zijn, verplaats/verwijder een of meerdere items.');
            errors_found++;
        }
    }

    return errors_found;
};

NavigationBuilder.prototype.save = function () {
    var self = this;

    var errors_found = 0;
    var errors_submenu_found = 0;

    $.each($('input', '#panel-menu-structure .menu_item_url '), function (i, elm) {
        if ($(elm).val() && $(elm).val().trim().substr(0, 1) != '#' && !self.validateUrl($(elm).val())) {
            $(elm).removeClass('valid').addClass('error');
            $(elm).parent('.form-group').addClass('has-error');

            errors_found++;
        } else {
            $(elm).addClass('valid').removeClass('error');
            $(elm).parent('.form-group').removeClass('has-error');
        }
    });

    if (self.max_levels) {
        var items = $(this.menu_sortable_list).nestedSortable('toArray', {startDepthCount: 0});
        var error_max_levels = false;

        for (var i = 0; i < items.length; i++) {
            if (parseInt(items[i].depth) > (self.max_levels - 1)) {
                errors_found++;
                error_max_levels = true;

                self.addError('error.max_levels', 'Er ' + ((self.max_levels > 1) ? 'kunnen maximaal ' + self.max_levels + ' niveau\'s' : 'kan maximaal 1 niveau') + ' aangemaakt worden, verplaats/verwijder een of meerdere items.');

                break;
            }
        }
        if (!error_max_levels) {
            self.removeError('error.max_levels');
        }
    }

    var items = $(this.menu_sortable_list).nestedSortable('toHierarchy', {startDepthCount: 0});

    if (self.max_root_items) {
        if (items && (parseInt(items.length) > self.max_root_items)) {
            self.addError('error.max_root_items', 'Er kunnen maximaal ' + self.max_root_items + ' menu-items op het hoofdniveau aanwezig zijn, verplaats/verwijder een of meerdere items.');

            errors_found++;
        } else {
            self.removeError('error.max_root_items');
        }

        errors_submenu_found = this.validateSubMenuItems(items);

        if(errors_submenu_found > 0) {
            errors_found = errors_found + errors_submenu_found;
        }
    }

    if (errors_found) {
        return false;
    } else {
        self.resetErrors();
    }

    var treeSource = $(this.menu_sortable_list).nestedSortable('toHierarchy', {startDepthCount: 0});
    var postObject = JSON.stringify(this.buildPostObject(treeSource, []));
    var markedForRemoval = JSON.stringify(this.markedForRemoval);

    $('#tree_source').val(postObject);
    $('#marked_for_removal').val(markedForRemoval);

    $('#form_menu').submit();
};

NavigationBuilder.prototype.destroyCKEditors = function (panel_container) {
    var panel_id = panel_container.attr('id');

    if (typeof(this.contentCKEditors[panel_container.attr('id')]) === 'undefined') {
        return;
    }

    for (var i = 0; i < this.contentCKEditors[panel_id].length; i++) {
        CKEDITOR.instances[this.contentCKEditors[panel_id][i]].destroy();

        this.contentCKEditors[panel_id].splice(i, 1);
    }
};

NavigationBuilder.prototype.initNestedSortable = function () {
    var self = this;

    var menu_sortable_options = {
        handle: 'div',
        items: 'li.sortable',
        toleranceElement: '> div',
        isTree: true
    };

    if (self.max_levels > 0) {
        menu_sortable_options.maxLevels = self.max_levels;
    }

    $(self.menu_sortable_list).nestedSortable(menu_sortable_options);

    $(self.menu_sortable_list).on('sort', function (event, item) {
        var width = $(item.helper).find('.ui-sortable-handle').css('width');
        var height = $(item.helper).find('.ui-sortable-handle').height() - 10;

        $(item.placeholder).css({
            'width': width,
            'height': height + 'px'
        });
    });
};

NavigationBuilder.prototype.addMenuItem = function (item) {
    var self = this;

    var prototype = $('#menu_item_prototype').clone();

    $(prototype).find('.panel-title').html(item.label);
    $(prototype).find('.menu-title-type small').html(((item.type == 'custom') ? 'Handmatige link' : item.type));

    var random_id = 'menu_' + self.generateRandomString();

    var $linkUrl = $(prototype).find('.menu_item_url input[name*=' + self.defaultLocale + ']');
    var $linkLabel = $(prototype).find('.menu_item_title input[name*=' + self.defaultLocale + ']');

    var $linkBlank = $(prototype).find('.menu_item_blank input');

    if (item.type == 'custom') {
        $(prototype).find('.panel').toggleClass('custom-item', true);
        $(prototype).find('li.sortable').attr({
            'id': random_id,
            'data-url': item.url,
            'data-label': item.label,
            'data-blank': item.blank,
            'data-new': true
        });

        $linkUrl
        .attr('value', item.url);

        $linkLabel
        .attr('value', item.label);

        $linkBlank
        .attr('checked', item.blank);
    } else {
        $(prototype).find('.panel').toggleClass('custom-item', false);

        $(prototype).find('li.sortable').attr({
            'id': random_id,
            'data-entity_id': item.entity_id,
            'data-entity_name': item.entity_name,
            'data-blank': false,
            'data-new': true
        });

        $linkLabel.attr('placeholder', item.label);
    }

    $(prototype).html($(prototype).html().replace(new RegExp('__INDEX__', 'g'), random_id));

    var addedItem = $($(prototype).html()).appendTo($(self.menu_sortable_list));

    // Collapse on click
    $('.panel:last [data-action=collapse]', $(self.menu_sortable_list)).on('click', function (e) {
        e.preventDefault();

        var $panelCollapse = $(this).parent().parent().parent().parent().nextAll();

        $(this).closest('.panel').toggleClass('panel-collapsed');
        $(this).toggleClass('rotate-180');

        $panelCollapse.slideToggle(150);
    });


    self.initMultiSelect($('.init-multiselect', $(addedItem)));
    self.initUniform($('.init-styled', $(addedItem)));
};

NavigationBuilder.prototype.validateUrl = function (value) {
    return value.match(/(?:http[s]?\/\/)?(?:[\w\-]+(?::[\w\-]+)?@)?(?:[\w\-]+\.)+(?:[a-z]{2,4})(?::[0-9]+)?(?:\/[\w\-\.%]+)*(?:\?(?:[\w\-\.%]+=[\w\-\.%!]+&?)+)?(#\w+\-\.%!)?/);

};

NavigationBuilder.prototype.removeMenuItem = function (item) {
    var self = this;

    if (!$(item).attr('data-new')) {
        self.markedForRemoval.push($(item).attr('data-item-id'));
    }

    $(item).remove();
};

NavigationBuilder.prototype.generateRandomString = function (length) {
    if (!length) {
        length = 10;
    }

    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

NavigationBuilder.prototype.buildPostObject = function (items, results) {
    var self = this;

    for (var i = 0; i < items.length; i++) {
        var result = {
            'id': items[i].id,
            'new': items[i].new,
            'itemId': items[i].itemId,
            'children': [],
            'title': [],
            'content': [],
            'url': [],
            'loginRequired': false,
            'customerGroups': []
        };

        if (items[i].entity_id && items[i].entity_name) {
            result.entity_id = items[i].entity_id;
            result.entity_name = items[i].entity_name;
        }

        var menu = $('#menu_' + result.id);
        var panel = $('.panel:first', menu);

        $.each(panel.find('.menu_item_title input'), function (i, elm) {
            var data = {};

            data[$(elm).attr('data-locale')] = $(elm).val();

            result.title.push(data);
        });

        $.each(panel.find('.menu_item_url input'), function (i, elm) {
            var data = {};

            data[$(elm).attr('data-locale')] = $(elm).val();

            result.url.push(data);
        });

        result.blank = menu.find('.menu_item_blank input').is(':checked');
        result.publish = menu.find('.menu_item_publish input:checked').val();
        result.publish_start = menu.find('.menu_item_publish_start input').val();
        result.publish_end = menu.find('.menu_item_publish_end input').val();
        result.login_required = (menu.find('.menu_item_login_required input:checked').val() == '1');
        result.customer_groups = menu.find('.menu_item_customer_groups select').val();

        $.each(panel.find('.menu-item-content-tabs textarea'), function (i, elm) {
            var data = {};

            data[$(elm).attr('data-locale')] = $(elm).val();

            result.content.push(data);
        });

        if (items[i].children) {
            result.children = self.buildPostObject(items[i].children, []);
        }

        results.push(result);
    }

    return results;
};

NavigationBuilder.prototype.getMaxZIndex = function () {
    var zIndex,
        z = 0,
        all = document.getElementsByTagName('*');
    for (var i = 0, n = all.length; i < n; i++) {
        zIndex = document.defaultView.getComputedStyle(all[i], null).getPropertyValue('z-index');
        zIndex = parseInt(zIndex, 10);
        z = (zIndex) ? Math.max(z, zIndex) : z;
    }
    return z;
};

$(document).ready(function () {
    var nb = new NavigationBuilder();

    nb.init();
});

