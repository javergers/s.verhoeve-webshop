$('input[name*=email]').on('change', function () {
    $('input[name*=username]').val($('input[name*=email]').val()).trigger('keyup');
});

$('body').on('click', '.login-as-customer', function (event) {
    event.preventDefault();

    var elm = $(this);

    var customerId = elm.data('customer-id');
    var name = elm.data('customer-name');
    var siteRegistered = elm.data('customer-registered-site-id');

    if (!customerId) {
        console.error('Missing customer id');

        return false;
    }

    $.ajax({
        method: 'GET',
        url: '/admin/klanten/login-as-customer-sites/' + customerId,
        dataType: 'json'
    }).done(function (sites) {
        swal({
            type: 'question',
            title: 'Inloggen als: ' + name,
            input: 'select',
            inputPlaceholder: 'Kies een webshop',
            inputValue: siteRegistered.toString(),
            inputOptions: sites,
            showCancelButton: true,
            cancelButtonText: 'Sluiten',
            confirmButtonText: 'Inloggen',
            showLoaderOnConfirm: true,
            preConfirm: function (siteId) {
                return new Promise(function (resolve, reject) {
                    if (!siteId) {
                        reject('Er is geen site geselecteerd');
                    } else {
                        resolve();
                    }
                });
            }
        }).then(function (siteId) {
            window.open('/admin/klanten/login-as-customer/' + customerId + '/' + siteId, '_blank');
        }).catch(swal.noop);

    }).fail(function (jqXHR, textStatus) {
        console.log(textStatus);
    });

});

$(function () {
    var searchParams = new URLSearchParams(window.location.search);

    if (searchParams.has('id')) {
        var id = searchParams.get('id');
        $('#customer_customer_container_customer_container_tab_account_container_product_account_office_container_product_account_office_column_company').val(id).trigger('change');
    }
})

