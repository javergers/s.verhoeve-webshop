function RegionCollectionCalcVat(collectionElm) {}

RegionCollectionCalcVat.prototype = new CollectionCalcVat();
RegionCollectionCalcVat.prototype.constructor = RegionCollectionCalcVat;

RegionCollectionCalcVat.prototype.init = function(collectionElm) {
    this.priceVatClass = '.field-vat';
    this.priceInclClass = '.field-price-incl';
    this.priceExclClass = '.field-price-excl';

    this.collectionElm = collectionElm;
    this.bindEvents();
};

RegionCollectionCalcVat.prototype.bindSupplierFieldEvent = function() {
    this.collectionElm
        .off('select2:select', 'select.select2')
        .on('select2:select', 'select.select2', function (e) {
            var supplierFieldElm = $(e.currentTarget);
            var data = e.params.data;
            var collectionItemElm = supplierFieldElm.closest('.collection-item');

            collectionItemElm.find(this.priceVatClass).val(data.vat);

            collectionItemElm.find(this.priceInclClass).val(data.priceInclFormatted);
            collectionItemElm.find(this.priceExclClass).val(data.priceFormatted);

            collectionItemElm.find('select[name*=deliveryInterval] option[value=' + data.interval + ']').attr('selected', 'selected');

            collectionItemElm.find(this.priceInclClass).trigger('keyup');
        }.bind(this))
    ;
};

RegionCollectionCalcVat.prototype.bindEvents = function() {
    this.hideColumn(this.priceExclClass);
    this.hideColumn(this.priceVatClass);

    this.bindPriceFieldEvents();
    this.bindSupplierFieldEvent();
};
