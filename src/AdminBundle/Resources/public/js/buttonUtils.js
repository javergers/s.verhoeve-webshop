;(function($) {
    $.fn.extend({
        confirm: function (title, type, cancelText, confirmText, callback) {

            title = typeof title !== 'string' ? $(this).attr('data-title') : title;
            type = typeof type !== 'string' ? $(this).attr('data-type') : type;
            cancelText = typeof cancelText !== 'string' ? $(this).attr('data-cancelText') : cancelText;
            confirmText = typeof confirmText !== 'string' ? $(this).attr('data-confirmText') : confirmText;
            callback = typeof callback !== 'string' ? $(this).attr('data-callback') : callback;

            if (typeof title !== 'string' || typeof type !== 'string' || typeof cancelText !== 'string' || typeof confirmText !== 'string' || typeof callback === 'undefined') {
                return false;
            }

            swal({
                title: title,
                type: type,
                showCancelButton: true,
                cancelButtonText: cancelText,
                confirmButtonText: confirmText,
                reverseButtons: true
            }).then(function (result) {
                console.info(result, 'result');
                new Function('return ' + callback.toString())()(result);
            }).catch(swal.noop);
        },
    });

    $(document).ready(function () {
        $('[swal="confirm"]').on('click', function (e) {
            e.preventDefault();
            $(this).confirm();
        });
    });
})(jQuery);
