$(document).ready(function () {

    $('ul.breadcrumb-elements').on('click', 'li.history-menu > ul> li > a[data-href]', function (e) {
        e.preventDefault();

        new Order($(this).attr('data-href'));
    });
});
