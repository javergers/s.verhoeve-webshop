function StockManagerModel(props) {
    this.loadModalData = function() {
        if(this.modalDataXhr) {
            this.modalDataXhr.abort();
            this.modalDataXhr = null;
        }

        this.modalDataXhr = $.ajax({
            url: this.url,
            method: 'GET',
        })
        .done(function(html) {
            this.contentWindow.setHtml(html);
            this.bindEvents();
        }.bind(this))
        .fail(function() {
            swal({
                title: 'Fout bij het openen van het bewerkscherm.',
                type: 'error'
            })
        })
        ;
    };

    this.initModal = function () {

        var toolbarButtons = [];

        if(this.showSaveButton) {
            toolbarButtons.push({
                id: 'stock_submit',
                position: 'right',
                label: 'Opslaan',
                order: 1,
                classNames: 'btn btn-primary',
                action: 'stock-save'
            });
        }

        this.contentWindow = new ContentWindow({
            width: '600px',
            toolbarButtons: toolbarButtons,
            successCallback: function(data, contentWindow) {
                var rowElm = this.rowElm;

                $(rowElm).attr('data-stock-status', data.stockStatus);

                $('.physical-stock', rowElm).text(data.physicalStock);
                $('.virtual-stock', rowElm).text(data.virtualStock);
                $('.threshold', rowElm).text(data.threshold);

                contentWindow.formChange = false;
                contentWindow.empty().hide();

                cms.showNotification({
                    type: 'success',
                    text: 'Wijzigingen opgeslagen!'
                });
            }.bind(this)
        });

        this.contentWindow.show().loading();

        this.loadModalData();
    };

    this.bindEvents = function () {
        var contentWindowElm = $(this.contentWindow.elm);

        $('#stock_submit', contentWindowElm).on('click', function(e) {
            contentWindowElm.find('form').submit();
        });
    };

    this.init = function (props) {
        this.url = props.url;
        this.rowElm = props.rowElm;
        this.showSaveButton = props.showSaveButton;

        this.modalDataXhr = null;

        this.initModal();
    };

    this.init(props);
}


$(document).ready(function () {
    $('#stock_manager')
        .on('click', '.row-actions a[data-action=edit]', function (e) {
            e.preventDefault();
            new StockManagerModel({
                url: e.currentTarget.href,
                showSaveButton: true,
                rowElm: $(this).closest('tr')
            });
        })
        .on('click', '.row-actions a[data-action=log]', function (e) {
            e.preventDefault();
            new StockManagerModel({
                url: e.currentTarget.href,
                showSaveButton: false
            });
        })
    ;
});

