$(function () {
    var supplierPanel = $('#supplier-panel');

    supplierPanel.on('click', 'a[data-type="edit"]', function (e) {
        openWindow($(this), e);
    });

    supplierPanel.find('a[data-type="add"]').on('click', function (e) {
        openWindow($(this), e);
    });

    function openWindow(obj, e) {
        e.preventDefault();

        var url = obj.attr('href') + '?product=' + obj.closest('#supplier-panel').attr('data-product-id');
        obj.attr('href', url);
        obj.attr('data-hide-callback', 'reloadTables');

        openContent(obj);
    }
});

function reloadTables() {
    var supplierPanel = $('#supplier-panel');
    var supplierProductDatatable = supplierPanel.find('.supplierProductPanel').find('table').attr('id');
    var supplierGroupProductDatatable = supplierPanel.find('.supplierGroupProductPanel').find('table').attr('id');

    $('#' + supplierProductDatatable).dataTable().api().ajax.reload();
    $('#' + supplierGroupProductDatatable).dataTable().api().ajax.reload();
}