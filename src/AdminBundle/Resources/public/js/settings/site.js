$(function () {
    $('.checkbox').on('click', function () {
        $.each($('.checkbox').not(this), function (key, checkBox) {
            checkBox.removeAttr('checked').removeProp('checked');
        });

        this.attr('checked', true).prop('checked', true);
        
        $.uniform().update();
    });
});