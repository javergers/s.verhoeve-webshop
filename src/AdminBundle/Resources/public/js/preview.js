$(document).ready(function () {
    $('.datatable').on('click', '[data-type="preview"]', function (e) {
        e.preventDefault();

        var elm = $(this);
        var sites = {};

        swal({
            type: 'info',
            title: 'Een moment geduld',
            html: 'Informatie voor de preview wordt opgehaald'
        }).catch(swal.noop);

        swal.showLoading();

        $.ajax({
            url: elm.attr('href') + '/sites',
            dataType: 'json',
            method: 'GET'
        }).done(function (response) {
            if (response && response.hasOwnProperty('sites') && response.sites) {
                sites = response.sites;

                var siteIds = Object.keys(response.sites);

                if (siteIds.length === 0) {
                    showErrorModal(null, 'Er zijn geen sites gekoppeld aan dit item, hierdoor kan er geen preview getoond worden.');

                    return;
                }

                if (siteIds.length === 1) {
                    showSuccessModal(siteIds[0]);

                    return;
                }

                swal({
                    type: 'question',
                    title: 'Maak een keuze',
                    html: 'Kies een van de onderstaande websites',
                    input: 'select',
                    inputOptions: response.sites
                }).then(function (siteId) {
                    showSuccessModal(siteId);
                }, function () {

                });
            } else {
                showErrorModal(null, 'Dit item is niet gekoppeld aan een website.');
            }
        }).fail(showErrorModal);

        function showSuccessModal(siteId) {
            var url = elm.attr('href') + '/' + siteId;

            swal({
                type: 'info',
                title: 'Een moment geduld',
                html: 'U wordt nu doorgestuurd naar de preview op<br /><br />' + sites[siteId]
            }).catch(swal.noop);

            swal.showLoading();

            var redirectToPreviewTimer = setTimeout(function () {
                url = url + '?return_uri=' + decodeURIComponent(window.location.href);

                window.open(url);

                swal.close();

                clearTimeout(redirectToPreviewTimer);
            }, 1000);
        }

        function showErrorModal(title, html) {
            if (!title) {
                title = 'Er is een fout opgetreden';
            }

            if (!html) {
                html = 'Probeer het later nogmaals';
            }

            swal({
                type: 'error',
                title: title,
                html: html
            }).catch(swal.noop);
        }
    });
});
