function ViewPDF(url, parameters) {
    if (typeof (ContentWindow) == 'undefined') {
        throw 'Missing ContentWindow dependency';
    }

    var options = {
        width: '980px',
        zIndex: 2010,
        parent: Order.contentWindow
    };

    options = $.extend(options, parameters);

    this.contentWindow = new ContentWindow(options);

    this.open(url, options);
}

ViewPDF.prototype.open = function (url, options) {
    this.url = url;
    this.contentWindow.setOption('toolbarButtons', options.toolbarButtons);
    this.contentWindow.empty().show();
    this.contentWindow.setHtml('<iframe id="pdf_viewer" src="' + url + '"></iframe>');

    this.bindEvents();

    $('iframe').load(this.loaded());
};

ViewPDF.prototype.loaded = function () {
    var url = this.url;
    var contentWindowElm = $(this.contentWindow.elm);
    var contentWindowBodyElm = $('.content-window-body', contentWindowElm);

    contentWindowBodyElm.toggleClass('no-scrolling', true);

    contentWindowBodyElm.css({
        'padding-top': 0,
        'padding-left': 0,
        'padding-right': 0
    });

    var height = contentWindowBodyElm.height();

    $('#pdf_viewer', contentWindowElm).css({
        height: height
    }).show(function () {
        var downloadBtnElm = $('[data-action="pdf-download"]', contentWindowElm);

        if (downloadBtnElm.length === 1) {
            downloadBtnElm.attr('href', url);
        }

        $.each($('.btn[data-action]', contentWindowElm), function (i, elm) {
            $(elm).removeAttr('disabled');
        });
    });
};

ViewPDF.prototype.bindEvents = function () {
    $(this.contentWindow.elm).on('click', '[data-action="pdf-print"]', this.print);
    $(this.contentWindow.elm).on('click', '[data-action="pdf-download"]', this.download);
};

ViewPDF.prototype.print = function () {
    var pdfFrame = document.getElementById('pdf_viewer').contentWindow;

    pdfFrame.focus();
    pdfFrame.print();
};

ViewPDF.prototype.download = function (e) {
    e.preventDefault();

    var url = $(this).attr('href');

    window.open(url + '/download', '_self');
};
