$(function () {
    $('.product-orderunit').on('click', function () {
        var self = $(this);
        var combinationId = $(this).attr('data-combination-id');
        var productName = $(this).find('.product-item-description').html();

        swal.setDefaults({
            input: 'text',
            type: 'question',
            progressSteps: ['1', '2']
        });

        var steps = [
            {
                title: 'Minimum besteleenheid',
                text: 'Wat is de minimum besteleenheid voor een ' + productName + ' in de huidige combinatie?'
            },
            {
                title: 'Maximum besteleenheid',
                text: 'Wat is de maximum besteleenheid voor een ' + productName + ' in de huidige combinatie?'
            }
        ];

        swal.queue(steps).then((result) => {
            var minQuantity = result[0];
            var maxQuantity = result[1];

            if (typeof minQuantity !== 'string' || minQuantity === "" || minQuantity <= 0) {
                swal.resetDefaults();
                swal('Fout', 'Geen geldig minimum besteleenheid', 'error');
                return;
            }

            if (typeof maxQuantity !== 'string' || maxQuantity === "" || maxQuantity <= 0) {
                swal.resetDefaults();
                swal('Fout', 'Geen geldig maximum besteleenheid', 'error');
                return;
            }

            if (maxQuantity <= minQuantity) {
                swal.resetDefaults();
                swal('Fout', 'Maximale kan niet het zelfde of lager zijn dan minimale besteleenheid', 'error');
                return;
            }

            $.ajax({
                url: $('.combination-product-holder').attr('data-combination-quantity-url'),
                method: 'POST',
                data: {
                    'combination': combinationId,
                    'minQuantity': minQuantity,
                    'maxQuantity': maxQuantity
                }
            }).then(() => {
                swal.resetDefaults();
                swal('Success!', 'De besteleenheden zijn aangepast', 'success');
                self.attr('data-min-quantity', minQuantity);
                self.attr('data-max-quantity', maxQuantity)
            });
        });
    });

    var configSelect = $('.configuration-select');
    var selectedConfiguration = null;
    var combinationQuantityInput = $('.combination-quantity');

    configSelect.on('change', function () {
        //filter select fields on previous selected values
        var selectedProducts = [];
        if ($(this).attr('data-main')) {
            var self = $(this);
            configSelect.each(function () {
                if (!self.is($(this))) {
                    $(this)[0].selectedIndex = 0;
                }

                if ($(this).val()) {
                    selectedProducts.push($(this).val());
                }
            });
        }
    });

    var currentSelect = $(this);
    //hide all options except main options and current select options
    $('[data-combination-option]').each(function () {
        if (!$(this).parent().is(currentSelect) && !$(this).parent().attr('data-main')) {
            $(this).hide();
        }
    });

    $.each(configurations, function (key, configuration) {
        var productsInConfiguration = true;
        $.each(selectedProducts, function (i) {
            productsInConfiguration = configuration.indexOf(parseInt(selectedProducts[i])) < 0;
        });

        //show available configuration options
        if (productsInConfiguration) {
            $.each(configuration, function (i) {
                $('[data-combination-option="' + configuration[i] + '"]').show();
            });

            if ($('option[data-combination-option]:selected').length === $('.configuration-select').length) {
                $('#form_variation').val(key);
                $('#form_submit').removeAttr('disabled');
                selectedConfiguration = configuration;

                //retrieve product information from matched configuration
                $.ajax({
                    url: $('#content-product').attr('data-product-information-url'),
                    method: 'GET',
                    data: {
                        product: key
                    }
                }).then((data) => {
                    $('#product_price').html(data.product.displayPrice);
                });
            }
        }
    });

    combinationQuantityInput.on('change', function () {
        var metadataInput = $('#form_metadata');
        var metadata = [];
        var totalPrice = 0;

        $('.combination-quantity').each(function () {
            var productId = $(this).closest('.line-double').attr('data-product-id');
            var quantity = $(this).val();
            var price = $(this).closest('.line-double').attr('data-price');

            metadata.push({
                productId: productId,
                quantity: quantity,
                price: price
            });

            totalPrice += (parseFloat(price.replace(',', '.')) * parseInt(quantity));

        });

        $('#product_price').html(parseFloat(totalPrice).toFixed(2).replace('.', ','));
        metadataInput.val(JSON.stringify(metadata));
    });

    combinationQuantityInput.trigger('change');
    configSelect.trigger('change');
});
