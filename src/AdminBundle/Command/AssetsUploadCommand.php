<?php

namespace AdminBundle\Command;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Response;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

class AssetsUploadCommand extends ContainerAwareCommand
{
    /**
     * @var \Symfony\Component\Console\Output\OutputInterface
     */
    private $output;

    private $uploads = [];

    protected function configure()
    {
        $this->setName('assets:upload');
        $this->setDescription("Uploads assets to ObjectStore");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        void($input);

        $this->output = $output;

        $this->client = new Client([
            'base_uri' => $this->getContainer()->getParameter("openstack_object_store") . '/public/',
        ]);

        $finder = new Finder();
        $finder->files();
        $finder->followLinks();
        $finder->files()->in("./web");
        $finder->files()->name('/\.(css|js)$/');
        $finder->files()->name('/\.(jpg|jpeg|png|gif)$/');
        $finder->files()->name('/\.(otf|eot|svg|ttf|woff|woff2)$/');

        $this->output->writeln("Comparing assets");

        $progress = new ProgressBar($this->output, count($finder));
        $progress->setFormat('verbose');
        $progress->start();

        $requests = function ($finder) use ($progress) {
            foreach ($finder as $file) {
                yield function () use ($file, $progress) {
                    return $this->client->headAsync($file->getRelativePathname(), [
                        'auth' => [
                            $this->getContainer()->getParameter("openstack_username"),
                            $this->getContainer()->getParameter("openstack_password"),
                        ],
                    ])->then(function (Response $response) use ($file, $progress) {
                        if ($response->getHeaderLine("Etag") != md5_file($file->getRealpath())) {
                            array_push($this->uploads, $file);
                        }

                        $progress->advance();
                    }, function (ClientException $clientException) use ($file, $progress) {
                        if ($clientException->getCode() == 404) {
                            array_push($this->uploads, $file);
                        }

                        $progress->advance();
                    });
                };
            }
        };

        $pool = new Pool($this->client, $requests($finder), [
            'concurrency' => 250,
        ]);

        $promise = $pool->promise()->then(function () use ($progress) {
            $progress->finish();

            print PHP_EOL;

            $this->upload();
        });

        $promise->wait();
    }

    private function upload()
    {
        if (!$this->uploads) {
            return;
        }
        $this->output->writeln("Uploading assets");

        $progress = new ProgressBar($this->output, count($this->uploads));
        $progress->setFormat('verbose');
        $progress->start();

        $requests = function ($uploads) use ($progress) {
            foreach ($uploads as $file) {
                yield function () use ($file, $progress) {
                    return $this->client->putAsync($file->getRelativePathname(), [
                        'body' => fopen($file->getRealpath(), 'r'),
                        'auth' => [
                            $this->getContainer()->getParameter("openstack_username"),
                            $this->getContainer()->getParameter("openstack_password"),
                        ],
                    ])->then(function (Response $response) use ($file, $progress) {
                        void($response);

                        $progress->advance();
                    }, function (ClientException $clientException) use ($file, $progress) {
                        $this->output->writeln(sprintf('<error>%s</error>', $clientException->getMessage()));
                        exit();
                    });
                };
            }
        };

        $pool = new Pool($this->client, $requests($this->uploads), [
            'concurrency' => 100,
        ]);

        $promise = $pool->promise()->then(function () use ($progress) {
            $progress->finish();

            print PHP_EOL;
        });

        $promise->wait();
    }
}
