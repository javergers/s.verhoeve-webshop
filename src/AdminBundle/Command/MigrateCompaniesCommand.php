<?php

namespace AdminBundle\Command;

use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Relation\CompanyEstablishment;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MigrateCompaniesCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('topgeschenken:migrate:companies')
            ->setDescription('Migrates all companies with a parent to an establishment');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /** @var QueryBuilder $qb */
        $qb = $this->getContainer()->get('doctrine')->getRepository(Company::class)->createQueryBuilder('c');
        $companies = $qb->where('c.supplierConnector IN(:connectors)')
            ->andWhere('c.parent IS NOT NULL')
            ->setParameter('connectors', ['Bakker', 'BakkerMail'])
            ->getQuery()->getResult();

        $em = $this->getContainer()->get('doctrine')->getManager();

        /** @var Company[] $companies */
        foreach ($companies as $company) {
            if ($company->getParent()->getEstablishments()->isEmpty()) {
                $company->setIsSupplier(false);

                $establishment = new CompanyEstablishment();
                $establishment->setCompany($company->getParent());
                $establishment->setAddress($company->getMainAddress());
                $establishment->setName($company->getName());

                $em->persist($establishment);
            }
        }

        $em->flush();
    }
}
