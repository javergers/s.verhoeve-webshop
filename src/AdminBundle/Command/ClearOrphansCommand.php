<?php

namespace AdminBundle\Command;

use Padam87\CronBundle\Annotation\Job;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @Job(hour="5", minute="0")
 */
class ClearOrphansCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('adiuvo:uploader:clear-orphans')
            ->setDescription('Clear orphaned uploads according to the max-age you defined in your configuration.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        void($input, $output);

        $manager = $this->getContainer()->get('oneup_uploader.orphanage_manager');
        $manager->clear();
    }
}
