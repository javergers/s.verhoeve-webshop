<?php

namespace AdminBundle\Filesystem;

use Oneup\UploaderBundle\Uploader\File\FileInterface;
use Oneup\UploaderBundle\Uploader\Naming\NamerInterface;

class FileUploadNamer implements NamerInterface
{
    public function name(FileInterface $file)
    {
        return sprintf('tmp/uploads/%s.%s', uniqid(), $file->getExtension());
    }
}