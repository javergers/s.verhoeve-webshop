<?php

namespace AdminBundle\Services\Role;

use AppBundle\Entity\Security\Employee\Group;
use AppBundle\Entity\Security\Employee\Role;
use AppBundle\Entity\Security\Employee\RoleCategory;
use AppBundle\Entity\Security\Employee\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RoleCategoryService
 * @package AdminBundle\Services\Role
 */
class RoleCategoryService
{
    use ContainerAwareTrait;

    /**
     * @param RoleCategory $category
     * @return array
     */
    public function getNonAssignedGroups(RoleCategory $category)
    {
        $groups = $category->getGroups();

        $existingGroups = [];
        /** @var Group $group */
        foreach ($groups as $group) {
            $existingGroups[] = $group->getId();
        }

        /** @var EntityRepository $groupRepository */
        $groupRepository = $this->container->get('doctrine')->getRepository(Group::class);
        $qb = $groupRepository->createQueryBuilder('g');
        $qb->select('g.id, g.name');

        if (!empty($existingGroups)) {
            $qb->andWhere('g.id NOT IN(:ids)');
            $qb->setParameter('ids', $existingGroups);
        }

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param RoleCategory $category
     * @return array
     */
    public function getNonAssignedUsers(RoleCategory $category)
    {
        $users = $category->getUsers();

        $existingUsers = [];
        /** @var User $user */
        foreach ($users as $user) {
            $existingUsers[] = $user->getId();
        }

        /** @var EntityRepository $userRepository */
        $userRepository = $this->container->get('doctrine')->getRepository(User::class);
        $qb = $userRepository->createQueryBuilder('u');
        $qb->select('u.id, u.firstname, u.lastname, u.username');

        if (!empty($existingUsers)) {
            $qb->andWhere('u.id NOT IN(:ids)');
            $qb->setParameter('ids', $existingUsers);
        }

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param $users
     *
     * @return RoleCategoryService
     */
    public function assignUsers($users)
    {
        if (!empty($users)) {
            $this->assign($users, User::class);
        }

        return $this;
    }

    /**
     * @param $groups
     *
     * @return RoleCategoryService
     */
    public function assignGroups($groups)
    {
        if (!empty($groups)) {
            $this->assign($groups, Group::class);
        }

        return $this;
    }

    /**
     * @param array $array
     * @param string $class
     *
     * @return RoleCategoryService
     */
    public function assign($array, string $class)
    {
        $doctrine = $this->container->get('doctrine');
        /** @var EntityRepository|User|Group $group */
        $repository = $doctrine->getRepository($class);
        $entityManager = $doctrine->getManager();

        foreach ($array as $id => $roles) {
            /** @var user|Group $entity */
            $entity = $repository->find($id);

            if (null === $entity) {
                throw new \RuntimeException('Item not found');
            }

            $object = $this->processRoleChanges($entity, $roles);
            $entityManager->persist($object);
        }

        $entityManager->flush();

        return $this;
    }

    /**
     * @param RoleCategory $category
     * @param Request      $request
     * @return int
     * @throws \RuntimeException
     */
    public function addNewRole(RoleCategory $category, Request $request)
    {
        if (empty($request->get('roleName'))) {
            throw new \RuntimeException('The role name cannot be empty.');
        }

        $roleExists = $this->container->get('doctrine')->getRepository(Role::class)->findOneBy(['name' => $request->get('roleName')]);
        if ($roleExists) {
            throw new \RuntimeException(sprintf('A role with the name %s already exists.', $request->get('roleName')));
        }

        $role = new Role();
        $role->setName($request->get('roleName'));
        $role->setDescription($request->get('roleDescription'));
        $role->setCategory($category);

        $doctrine = $this->container->get('doctrine')->getManager();
        $doctrine->persist($role);
        $doctrine->flush();

        return $role->getId();
    }

    /**
     * @param Group|User $object
     * @param array $roles
     * @return Group|User
     */
    private function processRoleChanges($object, array $roles)
    {
        /** @var EntityRepository|Role $roleRepository */
        $roleRepository = $this->container->get('doctrine')->getRepository(Role::class);

        foreach ($roles as $roleId => $checked) {
            /** @var Role $role */
            $role = $roleRepository->find($roleId);
            if ($object->hasrole($role, false)) {
                if (!$checked) {
                    $object->removeRole($role);
                }
            } elseif ($checked) {
                $object->addRole($role);
            }
        }

        return $object;
    }

    /**
     * @param Group|User $object
     * @param array $roles
     * @return RoleCategoryService
     */
    public function processRoleChangesWithFlush($object, $roles)
    {
        $doctrine = $this->container->get('doctrine')->getManager();
        $object = $this->processRoleChanges($object, $roles);

        $doctrine->persist($object);
        $doctrine->flush();

        return $this;
    }

}
