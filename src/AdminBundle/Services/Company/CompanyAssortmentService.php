<?php

namespace AdminBundle\Services\Company;

use AppBundle\DBAL\Types\MenuLocationType;
use AppBundle\Entity\Catalog\Assortment\Assortment;
use AppBundle\Entity\Catalog\Assortment\AssortmentProduct;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Site\Menu;
use AppBundle\Entity\Site\MenuItem;
use AppBundle\Entity\Site\Site;
use AppBundle\Manager\Catalog\Assortment\CompanyAssortmentManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CompanyAssortmentService
 * @package AdminBundle\Services\Company
 */
class CompanyAssortmentService
{
    use ContainerAwareTrait;

    /**
     * @var CompanyAssortmentManager
     */
    protected $companyAssortmentManager;

    /**
     * CompanyAssortmentService constructor.
     * @param CompanyAssortmentManager $companyAssortmentManager
     */
    public function __construct(CompanyAssortmentManager $companyAssortmentManager)
    {
        $this->companyAssortmentManager = $companyAssortmentManager;
    }

    /**
     * @return array
     */
    public function getAssortments()
    {
        $companies = [];

        $results = $this->container->get('doctrine')->getRepository(Assortment::class)->createQueryBuilder('a')
            ->join('a.sites', 's')
            ->select('DISTINCT s.id')
            ->getQuery()
            ->getResult();

        $siteIds = array_map(function ($item) {
            return $item['id'];
        }, $results);

        $sites = $this->container->get('doctrine')->getRepository(Site::class)->findBy(['id' => $siteIds]);

        return [
            'sites' => $sites,
            'companies' => $companies,
        ];
    }

    /**
     * @return array
     */
    public function getAssortmentsForField()
    {
        $assortments = $this->getAssortments();
        $returnArray = [];

        foreach ($assortments as $key => $assortmentType) {

            foreach ($assortmentType as $id => $entity) {
                if ($key === 'sites') {
                    $returnArray[$key][$entity->translate()->getDescription()] = 's_' . $entity->getId();
                } else {
                    $returnArray[$key][$entity->getName()] = 'c_' . $entity->getId();
                }
            }
        }

        return $returnArray;
    }

    /**
     * @param Company $company
     * @param Request $request
     */
    public function copyAssortmentsForCompany(Company $company, Request $request)
    {
        $newAssortments = [];

        $em = $this->container->get('doctrine')->getManager();

        $assortmentParentId = $request->get('company_assortment_default_settings')['assortment'];
        $setNewOwner = $request->get('company_assortment_default_settings')['copy'];
        if (strpos($assortmentParentId, 's_') !== false) {
            $siteId = explode('_', $assortmentParentId);
            $assortments = $em->getRepository(Assortment::class)->findBySite(array_pop($siteId));
        } else {
            $companyId = explode('_', $assortmentParentId);
            $assortments = $em->getRepository(Assortment::class)->findByCompany($companyId);
        }

        foreach ($assortments as $assortment) {
            $this->companyAssortmentManager->duplicate($company, $assortment, $setNewOwner);
        }

        $menu = $company->getMenu();
        //create new menu if menu doesnt exists
        if (null === $menu) {
            $menu = new Menu();
            $menu->setLocation(MenuLocationType::MENU_LOCATION_MAIN);
            $menu->setName($company->getName() . ' Hoofdmenu');

            $company->setMenu($menu);

            $em->persist($menu);
        }

        foreach ($newAssortments as $assortment) {
            if (null !== $assortment->getAssortmentType() && \in_array($assortment->getAssortmentType()->getKey(),
                    ['cards', 'additional_products'])) {
                continue;
            }

            $menuItem = new MenuItem();
            $menuItem->setEntityName(Assortment::class);
            $menuItem->setEntityId($assortment->getId());
            $menuItem->setPublish(true);
            $menuItem->setPosition(1);
            $menuItem->setBlank(false);

            $menu->addItem($menuItem);
        }

        $em->flush();
    }
}
