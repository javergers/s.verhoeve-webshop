<?php

namespace AdminBundle\Services\Company;

use AppBundle\Entity\Catalog\Product\CompanyProduct;
use AppBundle\Entity\Relation\Company;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CompanySkuService
 * @package AdminBundle\Services\Company
 */
class CompanySkuService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * CompanySkuService constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param Company $company
     * @return string
     * @throws \Exception
     */
    public function getCompanyProductSkuPrefix(Company $company): string
    {
        if (null !== $company->getCompanyProductSkuPrefix()) {
            return $company->getCompanyProductSkuPrefix();
        }
        $companyName = $company->getName();
        //Get first sku letters
        $companyName = preg_replace('/\s(?:[BN].?\s?V.?|V.?\s?O.?\s?F.?|GmbH|Ltd.?|SarL)/', '', $companyName);
        $companyName = preg_replace('/[^a-zA-Z0-9]/', ' ', $companyName);
        preg_match_all('/(?<=\s|^)[a-z]/i', $companyName, $matches);
        $sku = implode('', $matches[0]);
        $sku = strtoupper($sku);

        //Fill or cut to 5 chars
        $sku = substr($sku, 0, 5);
        while (strlen($sku) < 5) {
            $sku .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[random_int(0, 25)];
        }

        //Check availablilty
        $codeCheckTimeoutLimit = 676; //26^2
        $qb = $this->entityManager->getConnection()->createQueryBuilder();
        while ($codeCheckTimeoutLimit-- > 0) {

            $qb->
            select('comp.id')
                ->from('company', 'comp')
                ->leftJoin('comp', 'product', 'prod', 'comp.id = prod.company_id')
                ->where('comp.company_product_sku_prefix = :skucomp')
                ->orWhere('LEFT(prod.sku, 5) = :skuprod')
                ->setParameter('skucomp', $sku)
                ->setParameter('skuprod', $sku);
            if ($qb->execute()->rowCount() === 0) {
                $company->setCompanyProductSkuPrefix($sku);
                $this->entityManager->flush();
                return $sku;
            }

            //Cut and fill to 5 chars
            $sku = substr($sku, 0, 3);
            while (strlen($sku) < 5) {
                $sku .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'[random_int(0, 25)];
            }
        }
        throw new \OutOfBoundsException(sprintf('Can not generate company sku prefix for %s, all variations are taken',
            $companyName));
    }

    /**
     * @param Company $company
     * @return string
     * @throws \Exception
     */
    public function getNextAvailableProductSkuForCompany(Company $company): string
    {
        $prefix = $this->getCompanyProductSkuPrefix($company);
        $qb = $this->entityManager->getConnection()->createQueryBuilder();
        $qb->select('MAX(RIGHT(prod.sku, 3)) AS current_max')
            ->from('product', 'prod')
            ->where('LEFT(prod.sku, 5) = :prefix')
            ->setParameter('prefix', $prefix);
        $currentMax = $qb->execute()->fetch()['current_max'];
        $currentMax = (int)$currentMax;

        foreach($this->entityManager->getUnitOfWork()->getScheduledEntityInsertions() as $entityInsertion){
            if(($entityInsertion instanceof CompanyProduct) && $entityInsertion->getSku() !== null) {
                $entitySkuMax = (int)substr($entityInsertion->getSku(), 5);
                if($entitySkuMax > $currentMax){
                    $currentMax = $entitySkuMax;
                }
            }
        }

        if ($currentMax === 999) {
            $company->setCompanyProductSkuPrefix(null);
            return $this->getNextAvailableProductSkuForCompany($company);
        }
        return $prefix . str_pad((string)($currentMax + 1), 3, '0', STR_PAD_LEFT);
    }

}
