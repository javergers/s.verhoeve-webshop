<?php

namespace AdminBundle\Services\DataSet;

use AdminBundle\Form\Fields\PriceRange;
use AppBundle\Entity\Common\DataSet\DataSetFilterField;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;
use RuleBundle\Entity\Entity;
use RuleBundle\Service\ResolverService;
use RuleBundle\Service\RuleTransformerService;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * Class DataSetFilterService
 */
class DataSetFilterService
{
    /**
     * @var RuleTransformerService
     */
    private $ruleTransformer;

    /**
     * @var ResolverService
     */
    private $ruleResolver;

    /**
     * DataSetFilterService constructor.
     * @param RuleTransformerService $ruleTransformer
     * @param ResolverService        $ruleResolver
     */
    public function __construct(
        RuleTransformerService $ruleTransformer,
        ResolverService $ruleResolver
    ) {
        $this->ruleTransformer = $ruleTransformer;
        $this->ruleResolver = $ruleResolver;
    }

    /**
     * @param QueryBuilder $qb
     * @param Entity|null  $entity
     * @param array|null   $parameters
     * @param array|null   $filters
     *
     * @return mixed
     */
    public function filterQuery(QueryBuilder $qb, ?Entity $entity, ?array $parameters, ?array $filters)
    {
        $ruleText = $this->getRuleTextForFilters($entity, $filters);

        return $this->ruleResolver->applyFilter($qb, $ruleText, $parameters);
    }

    /**
     * @param Entity|null $entity
     * @param array|null  $filters
     * @return string
     */
    public function getRuleTextForFilters(?Entity $entity, ?array $filters): string
    {
        $ruleText = '1 = 1';

        if (null === $filters || null === $entity) {
            return $ruleText;
        }

        /** @var ArrayCollection|DataSetFilterField[] $filters */
        $dataSetFilters = $entity->getDataSetFilterFields();

        if ($dataSetFilters->isEmpty()) {
            return $ruleText;
        }

        /** @var DataSetFilterField[] $dataSetFilters */
        foreach ($dataSetFilters as $dataSetFilter) {
            $expectedKey = 'filter_' . $dataSetFilter->getId();

            foreach ($filters as $key => $value) {

                if ($expectedKey !== $key) {
                    continue;
                }

                if (null === $value || $value === '') {
                    continue;
                }

                $property = str_replace('__result.', '', $dataSetFilter->getPath());
                $options = $dataSetFilter->getDataSetFilterFieldOptions();
                $isMultiple = $dataSetFilter->isMultiple();

                if ($dataSetFilter->getFormType() === ChoiceType::class) {
                    if (true === $options->isEmpty()) {
                        continue;
                    }

                    $ruleText .= $this->getRuleTextForChoiceType($property, $value, $options, $isMultiple);
                } elseif ($dataSetFilter->getFormType() === PriceRange::class) {
                    $ruleText .= $this->getRuleTextForPriceRange($property, $value);
                } elseif (\is_array($value)) {
                    $optionParts = [];
                    foreach ($value as $k => $v) {
                        $optionParts[] = trim($property . ' = ' . $this->getQuotedValue($v));
                    }

                    $ruleText .= sprintf(' AND (%s)', implode(' OR ', $optionParts));
                } else {
                    $ruleText .= sprintf(' AND %s = %s', $property, $this->getQuotedValue($value));
                }
            }
        }

        return $ruleText;
    }

    /**
     * @param string     $property
     * @param            $value
     * @param Collection $options
     * @param bool       $isMultiple
     * @return string
     */
    private function getRuleTextForChoiceType(string $property, $value, Collection $options, bool $isMultiple)
    {
        $optionParts = [];
        foreach ($options as $option) {
            if (\is_array($value) && false === \in_array($option->getId(), $value, false)) {
                continue;
            }

            if (false === \is_array($value) && $option->getId() !== (int)$value) {
                continue;
            }

            $optionValue = $option->getValue();

            if (!empty($optionValue)) {
                $optionValue = $this->getQuotedValue($optionValue);
            }

            $optionParts[] = trim(sprintf('%s %s %s', $property, $option->getOperator(), $optionValue));
        }

        if (\count($optionParts) > 0) {
            $operant = $isMultiple ? ' OR ' : ' AND ';

            return sprintf(' AND (%s)', implode($operant, $optionParts));
        }

        return '';
    }

    /**
     * @param string $property
     * @param        $value
     * @return string
     */
    private function getRuleTextForPriceRange(string $property, $value): string
    {
        $values = explode(',', $value);

        if (\count($values) !== 2) {
            return '';
        }

        [$min, $max] = $values;

        return sprintf(' AND (%s >= %s AND %s <= %s)', $property, $this->getQuotedValue($min), $property,
            $this->getQuotedValue($max));
    }

    /**
     * @param $value
     * @return string
     */
    private function getQuotedValue($value): string
    {
        return $value !== '' || $value !== null ? "'" . $value . "'" : '';
    }

    /**
     * @return null|array
     */
    public function getOperatorChoices(): ?array
    {
        return array_flip($this->ruleTransformer->getOperators());
    }
}