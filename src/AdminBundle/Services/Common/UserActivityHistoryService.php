<?php

namespace AdminBundle\Services\Common;

use AppBundle\Exceptions\Activity\TableNotSupportedException;
use AppBundle\Entity\Security\Employee\User;
use DataDog\AuditBundle\Entity\AuditLog;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class UserActivityHistoryService
 * @package AppBundle\Services
 */
class UserActivityHistoryService
{
    /**
     * @var array
     */
    private const ALLOWED_TABLES = [
        'company',
        'order_collection',
        'order',
        'customer',
        'product',
    ];

    /**
     * @var array
     */
    private const ALLOWED_ACTIONS = [
        'insert',
        'update',
        'view'
    ];

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var User
     */
    private $user;

    /**
     * @var array
     */
    private $tables = [];

    /**
     * HistoryService constructor.
     * @param $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;

        $this->tables = self::ALLOWED_TABLES;
    }

    /**
     * @param User $user
     * @return UserActivityHistoryService
     */
    public function setUser(User $user): UserActivityHistoryService
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @param array|null $tables
     * @return UserActivityHistoryService
     * @throws TableNotSupportedException
     */
    public function setTables(?array $tables = null): UserActivityHistoryService
    {
        if (null !== $tables) {
            if($this->validTables($tables)) {
                $this->tables = $tables;
            } else {
                throw new TableNotSupportedException(sprintf("You can't request logs for: %s", implode(', ', $tables)));
            }
        }

        return $this;
    }

    /**
     * @param int $results
     * @return array
     */
    public function getRecentLogs(int $results = 10): array
    {
        $auditLogRepository = $this->em->getRepository(AuditLog::class);

        $qb = $auditLogRepository->createQueryBuilder('log');

        try {
            $now = new \DateTime();
            $yesterday = $now->sub(new \DateInterval('PT24H'));
        } catch(\Exception $e) {
            // should never happen
            $yesterday = null;
        }

        $qb
            ->join('log.blame', 'blame')
            ->join('log.source', 'source')
            ->andWhere('blame.class = :class')
            ->andWhere('blame.fk = :fk')
            ->andWhere('log.action IN (:actions)')
            ->andWhere('log.loggedAt > :yesterday')
            ->andWhere('log.tbl in (:tables)')

            ->orderBy('log.loggedAt', 'DESC')
            ->setMaxResults($results)
            ->setParameter('class', User::class)
            ->setParameter('fk', $this->user->getId())
            ->setParameter('actions', self::ALLOWED_ACTIONS)
            ->setParameter('yesterday', $yesterday)
            ->setParameter('tables', $this->tables)
        ;

        return $qb->getQuery()->getResult();
    }

    /**
     * @return ArrayCollection|null
     */
    public function getMostRecentLogs(): ?ArrayCollection
    {
        $logs = $this->getRecentLogs(10);

        if(\count($logs) > 0) {
            return new ArrayCollection($logs);
        }

        return null;
    }

    /**
     * @param array|null $tables
     * @return bool
     */
    private function validTables(array $tables)
    {
        return \count(array_diff($tables, self::ALLOWED_TABLES)) === 0;
    }
}