<?php

namespace AdminBundle\Services\DataTable;

use AdminBundle\Components\Crud\QueryBuilder;
use AdminBundle\Components\Datatable\Column\ActionColumn;
use AdminBundle\Components\Datatable\Column\IdColumn;
use AdminBundle\Components\Datatable\Column\PublishColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AdminBundle\Components\Datatable\Datatable;
use AdminBundle\Components\Datatable\DatatableBuilder;
use AdminBundle\Form\DataSet\DataSetFilterFormType;
use AdminBundle\Interfaces\DatatableInterface;
use AdminBundle\Services\AutoDetect\AutoDetectTypeService;
use AdminBundle\Services\DataSet\DataSetFilterService;
use AdminBundle\Services\Role\RoleManagerService;
use AdminBundle\Services\Router\BaseRouteService;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query;
use Exception;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use RuleBundle\Entity\Entity;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouteCollection;
use Traversable;

/**
 * Class DataTableService
 * @package AdminBundle\Services\DataTable
 */
class DataTableService
{
    use ContainerAwareTrait;

    /**
     * @var array
     */
    private $options;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var AutoDetectTypeService
     */
    private $autoDetectType;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var BaseRouteService
     */
    private $baseRoute;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var RoleManagerService
     */
    private $roleManager;

    /**
     * @var Paginator
     */
    private $knpPaginator;

    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var QueryBuilder
     */
    private $crudQueryBuilder;

    /**
     * @var Query
     */
    private $query;

    /**
     * @var Datatable
     */
    private $datatable;

    /**
     * @var SlidingPagination
     */
    private $paginator;

    /**
     * @var FormFactory
     */
    private $formFactory;

    /**
     * @var \Doctrine\ORM\QueryBuilder
     */
    private $qb;

    /**
     * @var DataSetFilterService
     */
    private $dataSetFilterService;

    /**
     * DataTableService constructor.
     *
     * @param \Twig_Environment     $twig
     * @param AutoDetectTypeService $autoDetectType
     * @param Router                $router
     * @param BaseRouteService      $baseRoute
     * @param RequestStack          $requestStack
     * @param RoleManagerService    $roleManager
     * @param Paginator             $knpPaginator
     * @param Registry              $doctrine
     * @param QueryBuilder          $crudQueryBuilder
     */
    public function __construct(
        \Twig_Environment $twig,
        AutoDetectTypeService $autoDetectType,
        Router $router,
        BaseRouteService $baseRoute,
        RequestStack $requestStack,
        RoleManagerService $roleManager,
        Paginator $knpPaginator,
        Registry $doctrine,
        QueryBuilder $crudQueryBuilder,
        FormFactory $formFactory,
        DataSetFilterService $dataSetFilterService
    ) {
        $this->twig = $twig;
        $this->autoDetectType = $autoDetectType;
        $this->router = $router;
        $this->requestStack = $requestStack;
        $this->baseRoute = $baseRoute;
        $this->roleManager = $roleManager;
        $this->knpPaginator = $knpPaginator;
        $this->doctrine = $doctrine;
        $this->crudQueryBuilder = $crudQueryBuilder;
        $this->formFactory = $formFactory;
        $this->dataSetFilterService = $dataSetFilterService;
    }

    /**
     * @param array   $options
     *
     * @param \object $parentEntity
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     * @throws Exception
     */
    public function getTable(array $options, object $parentEntity = null)
    {
        $this->options = $options;
        $this->resolveOptions();

        $entity = $this->doctrine->getManager()->getRepository(Entity::class)->findOneBy([
            'fullyQualifiedName' => $this->getOption('entity_class'),
        ]);

        $options = [
            'entity' => $entity,
            'dataTable' => $this->build(),
            'dataTableButtons' => $this->getTableButtons($parentEntity),
            'dataTableBulkActions' => $this->getBulkActions(),
            'datatableOrder' => $this->getOption('order'),
            'datatable_id' => $this->getOption('datatable_id'),
            'data_url' => $this->getDataUrl(),
            'allow_reorder' => $this->getOption('allow_reorder'),
            'reorder_url' => $this->getReorderUrl(),
            'dataTableGroups' => $this->getOption('groups'),
            'disable_search' => $this->getOption('disable_search'),
        ];

        if (null !== $entity && false === $entity->getDataSetFilterFields()->isEmpty()) {
            $options['filters'] = $this->getFilterForm($entity)->createView();
        }

        return $this->twig->render('@Admin/Partials/datatable.html.twig', $options);
    }

    /**
     * @param array $options
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function getData(array $options)
    {
        $this->options = $options;
        $this->resolveOptions();

        $dataTable = $this->build();

        $paginator = $this->getPaginator();

        $columns = [];
        $groups = [];
        foreach ($dataTable as $k => $column) {
            $isGroup = \in_array($column->getName(), $this->options['groups'], true);

            $columns[$k] = [
                'name' => $column->getName(),
                'group' => $isGroup,
            ];

            if ($isGroup) {
                $groups[$column->getName()] = $k;
            }
        }

        $totalItemCount = $paginator->getTotalItemCount();
        $returnData = [
            'recordsTotal' => $totalItemCount,
            'recordsFiltered' => $totalItemCount,
            'data' => [],
            'groups' => $groups,
            'columns' => $columns,
        ];

        $getIdMethodExists = method_exists($this->options['entity_class'], 'getId');
        $getPositionMethodExists = method_exists($this->options['entity_class'], 'getPosition');

        $keyCheckPos = true === $this->options['allow_reorder'] ? 2 : 1;

        foreach ($paginator as $i => $result) {
            $row = [];

            foreach ($dataTable as $k => $column) {

                if ($this->options['disable_parent'] === false && $k === $keyCheckPos && method_exists($result, 'getParent') && $result->getParent()) {

                    $row[] = '&nbsp;<span class="icon-arrow-right22"></span> ' . $column->value($result);

                } else {
                    if ($column instanceof ActionColumn && !$getIdMethodExists && !$getPositionMethodExists) {
                        continue;
                    }

                    $columnValue = $column->value($result);

                    if ($column->getName() === 'buttons') {
                        foreach ($column->getOption('buttons') as $buttonKey => $button) {
                            if (!property_exists($button, 'requirement')) {
                                continue;
                            }

                            $requirementOption = $button->requirement;

                            if ($requirementOption && \is_callable($requirementOption) && !$requirementOption($result)) {
                                unset($columnValue[$buttonKey]);
                            }
                        }
                    }

                    $row[] = $columnValue;
                }
            }

            $returnData['data'][] = $row;
        }

        return new JsonResponse($returnData);
    }

    /**
     * @param array $options
     * @param array $reorder
     * @return JsonResponse
     */
    public function reorder(array $options, array $reorder)
    {
        $em = $this->container->get('doctrine')->getManager();

        foreach($reorder as $id => $position) {
            $classObj = $em->getRepository($options['entity_class'])->find($id);

            if(null !== $classObj && method_exists($classObj, 'setPosition')) {
                $classObj->setPosition((int)$position);
            }
        }

        $em->flush();

        return new JsonResponse();
    }

    /**
     * @return Datatable
     * @throws \RuntimeException
     * @throws \ReflectionException
     * @throws Exception
     */
    private function build()
    {
        if (null !== $this->datatable) {
            return $this->datatable;
        }

        $datatableBuilder = new DatatableBuilder();

        if (!$this->hasOption('datatable_type')) {
            $this->options['datatable_type'] = $this->autoDetectType->autoDetectClassType($this->getOption('entity_class'),
                'datatable');
        }

        if ($this->options['add_checkbox'] && !method_exists($this,
                'getCollection') && method_exists($this->options['entity_class'], 'getId')
            && \count($this->getBulkActions()) !== 0
        ) {
            $datatableBuilder->add('id', IdColumn::class, [
                'label' => '<input type="checkbox" id="select_all" name="select_all" value="1" class="form-styled" />',
            ]);
        }

        if (true === $this->options['allow_reorder']) {
            $datatableBuilder->add('position', TextColumn::class, [
                'width' => 15,
                'label' => 'Positie',
                'orderable' => false,
            ]);
        }

        if (!class_exists($this->options['datatable_type'])) {
            throw new \RuntimeException("Datatable '" . $this->options['datatable_type'] . "' doesn't exists");
        }

        $datatable = null;
        if($this->container->has($this->options['datatable_type'])){
            $datatable = $this->container->get($this->options['datatable_type']);
        } else {

            /** @var DatatableInterface $datatable */
            $datatable = new $this->options['datatable_type']();
        }

        if($datatable === null){
            throw new \LogicException(sprintf('Class "%s" is not accessible (public) via the container or instantiatable without constructor args', $this->options['datatable_type']));
        }

        if (\in_array(ContainerAwareTrait::class, class_uses($this->options['datatable_type'], true), true)) {
            $datatable->setContainer($this->container);
        }

        $datatable->buildDatatable($datatableBuilder, []);

        // Check if entity is a publishable entity and add publish column after checkbox
        if (\in_array('AppBundle\Traits\PublishableEntity', class_uses($this->getOption('entity_class')), true)) {
            $datatableBuilder->add('publish', PublishColumn::class, [
                'width' => 20 + 40,
            ]);
        }

        if (!method_exists($this, 'getCollection') && method_exists($this->options['entity_class'], 'getId')) {
            $datatableBuilder->add('buttons', ActionColumn::class, [
                'label' => '',
                'buttons' => $this->getButtons(),
                'width' => \count($this->getButtons()) * 20 + 40,
            ]);
        }

        $datatableBuilder = $this->applySecurity($datatableBuilder);

        return ($this->datatable = $datatableBuilder->getDatatable());
    }

    /**
     * @return string
     */
    public function getDataUrl()
    {
        return $this->getOption('data_url');
    }

    /**
     * @return string
     */
    public function getReorderUrl()
    {
        return $this->getOption('reorder_url');
    }

    /**
     * Returns the buttons associated to the table itself
     *
     * @param null $entity
     * @return array
     */
    private function getTableButtons($entity): array
    {
        $buttons = [];

        if ($this->options['form_type'] && $this->getOption('actions_processing') === 'append') {
            $buttons[] = $this->getAddButton();
        }

        if(null !== ($reorderButton = $this->getReorderButton())) {
            $buttons[] = $reorderButton;
        }

        if ($this->getOption('actions')) {
            foreach ($this->getOption('actions') as $action) {
                if (isset($action['route']) && null !== $action['route']) {
                    $route = $action['route'];
                    $routeGenerated = $this->router->generate($action['route'], $action['route_parameters'] ?? []);
                } elseif (isset($action['url']) && null !== $action['url']) {
                    $routeGenerated = $route = $action['url']($entity);
                } else {
                    throw new \RuntimeException('No route received');
                }

                $icon = null;
                $label = $action['label'];
                $className = null;
                $type = $action['type'] ?? null;

                if (array_key_exists('icon', $action)) {
                    $icon = $action['icon'];
                }

                if (array_key_exists('className', $action)) {
                    $className = $action['className'];
                }

                $buttons[] = (object)[
                    'icon' => $icon,
                    'label' => $label,
                    'type' => $type,
                    'route' => $route,
                    'route_generated' => $routeGenerated,
                    'className' => $className,
                    'action' => $action['action'] ?? null,
                    'refresh_panel' => $this->getOption('refresh_panel'),
                    'xhr' => $action['xhr'],
                    'method' => $action['method'],
                    'pre_confirm' => $action['pre_confirm'] ?: null,
                ];
            }
        }

        if ($this->options['form_type'] && $this->getOption('actions_processing') !== 'override') {
            $button = $this->getAddButton();
            if (null !== $button) {
                $buttons[] = $button;
            }
        }

        return $buttons;
    }

    /**
     * @return array
     */
    private function getAddButton()
    {
        if (!$this->getOption('allow_add')) {
            return null;
        }

        $addRouteName = $this->hasOption('base_route') ? $this->getOption('base_route') . '_add' : $this->baseRoute->getBaseRouteForAction('add');
        $addRoute = $this->router->generate($addRouteName);

        return [
            'icon' => 'icon-plus2',
            'label' => $this->getOption('button_label') . ' Toevoegen',
            'route' => $addRouteName,
            'route_generated' => $addRoute,
            'className' => 'btn-sm btn-success btn-add',
            'type' => 'add',
            'refresh_panel' => $this->getOption('refresh_panel'),
        ];
    }

    /**
     * @return array|null
     */
    private function getReorderButton()
    {
        if (false === $this->getOption('allow_reorder')
            || null === $this->getOption('reorder_url')) {
            return null;
        }

        return [
            'icon' => 'icon-plus2',
            'label' => 'Volgorde wijzigen',
            'className' => 'btn-sm btn-success btn-reorder',
            'data' => [
                'save-text' => 'Volgorde opslaan',
                'default-text' => 'Volgorde wijzigen',
            ],
            'route' => '',
            'route_generated' => '#',
            'type' => 'reorder',
            'action' => 'reorder',
            'refresh_panel' => $this->getOption('refresh_panel'),
        ];
    }

    /**
     * @return array
     */
    private function getBulkActions()
    {
        $bulk_actions = [];

        if (!$this->getOption('bulk_actions')) {
            return $bulk_actions;
        }

        foreach ((array)$this->getOption('bulk_actions') as $action) {
            $bulk_actions[] = $action;
        }

        return $bulk_actions;
    }

    /**
     * Returns the buttons which are associated to a single row in the table
     *
     * @return array
     * @throws \ReflectionException
     * @throws Exception
     */
    private function getButtons()
    {
        $buttons = [];

        $router = $this->router;
        $entity = $this->getOption('entity_class');
        $this->roleManager->setObject($entity);

        if ($this->getOption('item_actions')) {
            foreach ($this->getOption('item_actions') as $action) {
                $action['router'] = $router;
                $action['refresh_panel'] = $this->getOption('refresh_panel');
                $buttons[] = (object)$action;
            }
        }

        if ($this->getOption('form_type') === false) {
            return $buttons;
        }

        if (!$this->hasOption('form_type')) {
            $this->options['form_type'] = $this->autoDetectType->autoDetectClassType($this->getOption('entity_class'),
                'form');
        }

        $baseRouteService = $this->baseRoute;
        if ($this->getOption('form_type')) {
            if ($this->getOption('allow_edit') && $this->roleManager->userIsGrantedTo($this->roleManager->getRolesFor($entity,
                    'update'))) {
                $buttons[] = (object)[
                    'icon' => 'mdi mdi-pencil',
                    'router' => $router,
                    'route' => $this->getOption('base_route') ? $this->getOption('base_route') . '_edit' : $baseRouteService->getBaseRouteForAction('edit'),
                    'method' => 'GET',
                    'type' => 'edit',
                    'label' => 'Bewerken',
                    'refresh_panel' => $this->getOption('refresh_panel'),
                ];
            }

            if ($this->getOption('allow_read') && $this->roleManager->userIsGrantedTo($this->roleManager->getRolesFor($entity,
                    'read'))) {
                $buttons[] = (object)[
                    'icon' => 'mdi mdi-eye',
                    'router' => $router,
                    'route' => $this->getOption('base_route') ? $this->getOption('base_route') . '_view' : $baseRouteService->getBaseRouteForAction('view'),
                    'method' => 'GET',
                    'xhr' => true,
                    'type' => 'view',
                    'label' => 'Bekijken',
                    'refresh_panel' => $this->getOption('refresh_panel'),
                ];
            }
        }

        if ($this->getOption('allow_delete') && $this->roleManager->userIsGrantedTo($this->roleManager->getRolesFor($entity,
                'delete'))) {
            $buttons[] = (object)[
                'icon' => 'mdi mdi-delete',
                'router' => $router,
                'route' => $this->getOption('base_route') ? $this->getOption('base_route') . '_delete' : $baseRouteService->getBaseRouteForAction('delete'),
                'method' => 'DELETE',
                'type' => 'delete',
                'label' => 'Verwijderen',
                'refresh_panel' => $this->getOption('refresh_panel'),
            ];
        }


        return $buttons;
    }

    /**
     * @return SlidingPagination|PaginationInterface
     * @throws Exception
     */
    private function getPaginator()
    {
        if ($this->paginator) {
            return $this->paginator;
        }

        /** @var Request $request */
        $request = $this->getOption('request');

        $maxResults = $request->get('length', $this->getOption('results_per_page', 20));

        $paginator = $this->knpPaginator->paginate($this->getPaginatorTarget(), $this->getCurrentPage(), $maxResults,
            $this->getPaginatorOptions());

        return ($this->paginator = $paginator);
    }

    /**
     * @return array
     */
    private function getPaginatorOptions()
    {
        if ($this->getOption('paginator') && array_key_exists('options', $this->getOption('paginator'))) {
            $options = $this->getOption('paginator')['options'];
        } else {
            /** @var ClassMetadata $metadata */
            $metadata = $this->doctrine->getManager()->getMetadataFactory()->getMetadataFor($this->getOption('entity_class'));

            /**
             * Wrapping queries and COALESCE don't play well together
             */
            $options = [
                'wrap-queries' => $metadata->hasAssociation('parent') ? false : true,
            ];
        }

        return $options;
    }

    /**
     * @return QueryBuilder|ArrayCollection|Query
     * @throws Exception
     */
    private function getPaginatorTarget()
    {
        if (method_exists($this, 'getCollection')) {
            return $this->getCollection();
        }

        if($this->getOption('disable_filter')) {
            return $this->getQuery();
        }

        return $this->processQueryWithRuleText();
    }

    /**
     * @return QueryBuilder
     *
     * @throws Exception
     */
    private function processQueryWithRuleText()
    {
        /** @var Request $request */
        $request = $this->getOption('request');

        $requestedFilters = $request->get('filters');

        $indexedFilters = null;
        if(!empty($requestedFilters)) {
            parse_str($requestedFilters, $indexedFilters);
            if (isset($indexedFilters['datatable_filters'])) {
                $indexedFilters = $indexedFilters['datatable_filters'];
            }
        }

        $filters = $indexedFilters;

        $crudQbParamaters = $this->getQuery()->getParameters();

        $this->getQuery();

        if($this->qb instanceof QueryBuilder){
            $this->qb = $this->qb->getQueryBuilder();
        }
        $this->qb->setParameters($crudQbParamaters);

        $entity = $this->doctrine->getManager()->getRepository(Entity::class)->findOneBy([
            'fullyQualifiedName' => $this->getOption('entity_class'),
        ]);

        $parameters = $crudQbParamaters->toArray();

        return $this->dataSetFilterService->filterQuery($this->qb, $entity, $parameters, $filters);
    }

    /**
     * @return float|int
     */
    private function getCurrentPage()
    {
        /** @var Request $request */
        $request = $this->getOption('request');

        $firstResult = $request->get('start');
        $resultsPerPage = $request->get('length', $this->getOption('results_per_page', 20));

        return ($firstResult / $resultsPerPage) + 1;
    }

    /**
     * @return \Doctrine\ORM\Query
     * @throws Exception
     */
    private function getQuery()
    {
        if (!$this->query) {
            $alias = $this->getOption('alias') ?? '__result';

            if ($this->getOption('parent')) {
                if (!isset($this->options['parent_column']) && $this->hasOption('query_builder')) {
                    $qb = $this->container->get('cms.crud.query_builder');
                    $qb->initialize($this->getOption('request'), $this->getOption('entity_class'), $this->build(), $alias);

                    /**
                     * @var \Closure $queryBuilderWrapper
                     */
                    $queryBuilderWrapper = $this->getOption('query_builder');

                    if ($queryBuilderWrapper) {
                        $qb = $queryBuilderWrapper($qb->getQueryBuilder(),
                            $this->requestStack->getMasterRequest());
                    }
                } elseif (isset($this->options['parent_column']) && strpos($this->options['parent_column'],
                        '.') !== false) {
                    $qb = $this->crudQueryBuilder;
                    $qb->initialize($this->getOption('request'), $this->getOption('entity_class'), $this->build(),
                        $alias);

                    $parts = \explode('.', $this->options['parent_column']);
                    $field = \array_pop($parts);

                    $customAlias = $alias;

                    foreach ($parts as $key => $part) {
                        $newAlias = '_' . $part;
                        $qb->getQueryBuilder()->leftJoin($customAlias . '.' . $part, $newAlias);

                        $customAlias = $newAlias;
                    }

                        $qb->getQueryBuilder()->andWhere($customAlias . '.' . $field . ' = :value')->setParameter('value',
                            $this->options['parent']['parentId']);

                        if ($this->hasOption('query_builder')) {
                            /**
                             * @var \Closure $queryBuilderWrapper
                             */
                            $queryBuilderWrapper = $this->getOption('query_builder');

                            if ($queryBuilderWrapper) {
                                $qb = $queryBuilderWrapper($qb->getQueryBuilder(), $this->container->get('request_stack')->getMasterRequest());
                            }
                        }
                    } else {
                        $qb = $this->container->get('cms.crud.query_builder');
                        $qb->initialize($this->getOption('request'), $this->getOption('entity_class'), $this->build(), $alias);

                    $rootAliases = $qb->getQueryBuilder()->getRootAliases()[0];

                    $qb->getQueryBuilder()->andWhere(($rootAliases ? $rootAliases . '.' : null) . lcfirst(isset($this->options['parent_column']) ? $this->options['parent_column'] : $this->options['parent']['entity']) . ' = :value')->setParameter('value',
                        $this->options['parent']['parentId']);
                }

                $this->qb = $qb;
                $this->query = $qb->getQuery();
            } else {
                $qb = $this->crudQueryBuilder;
                $qb->initialize($this->getOption('request'), $this->getOption('entity_class'), $this->build(),
                    $this->getOption('alias'));

                if ($this->hasOption('query_builder')) {
                    /**
                     * @var \Closure $queryBuilderWrapper
                     */
                    $queryBuilderWrapper = $this->getOption('query_builder');

                    if ($queryBuilderWrapper) {
                        $queryBuilderWrapper($qb->getQueryBuilder());
                    }
                }

                $this->query = $qb->getQuery();
                $this->qb = $qb->getQueryBuilder();
            }

        }

        return $this->query;
    }

    /**
     * @param DatatableBuilder $datatableBuilder
     *
     * @return DatatableBuilder
     * @throws \ReflectionException
     */
    private function applySecurity(DatatableBuilder $datatableBuilder)
    {
        $hiddenFields = $this->roleManager->getHiddenFields($this->getOption('entity_class'),
            'READ');

        foreach ($datatableBuilder->all() as $field => $child) {
            if (\in_array($field, $hiddenFields, true)) {
                $datatableBuilder->remove($field);
            }
        }

        return $datatableBuilder;
    }

    /**
     * @param $key
     *
     * @return bool
     */
    private function hasOption($key)
    {
        return array_key_exists($key, $this->options);
    }

    /**
     * @param      $key
     * @param null $default
     *
     * @return null
     */
    private function getOption($key, $default = null)
    {
        if (!$this->hasOption($key)) {
            return $default;
        }

        return $this->options[$key];
    }

    /**
     * @throws Exception
     */
    private function resolveOptions()
    {
        $resolver = new OptionsResolver();

        $resolver->setDefined([
            'actions_processing',
            'assets',
            'actions',
            'datatable_type',
            'form_type',
            'item_actions',
            'results_per_page',
            'button_label',
            'bulk_actions',
            'query_builder',
            'paginator',
            'data_url',
            'reorder_url',
            'parent',
            'base_route',
            'parent_column',
            'allow_read',
            'allow_add',
            'allow_edit',
            'allow_delete',
            'allow_reorder',
            'groups',
            'disable_search',
            'disable_filter',
            'add_checkbox',
            'refresh_panel',
            'order',
            'alias',
            'disable_parent',
            'datatable_id'
        ]);

        $resolver->setRequired([
            'entity_class',
        ]);

        $this->options = $resolver->resolve($this->options);

        $resolver->setDefaults([
            'form_type' => $this->autoDetectType->autoDetectClassType($this->getOption('entity_class'), 'form'),
            'datatable_type' => $this->autoDetectType->autoDetectClassType($this->getOption('entity_class'),
                'datatable'),
            'request' => $this->requestStack->getMasterRequest(),
            'item_actions' => null,
            'bulk_actions' => null,
            'actions' => null,
            'data_url' => 'data',
            'reorder_url' => null,
            'add_checkbox' => true,
            'allow_add' => true,
            'allow_edit' => true,
            'allow_delete' => true,
            'allow_read' => false,
            'allow_reorder' => false,
            'groups' => [],
            'disable_search' => false,
            'disable_filter' => false,
            'refresh_panel' => null,
            'alias' => null,
            'order' => [
                [1, 'asc'],
            ],
            'disable_parent' => false,
            'datatable_id' => null,
        ]);

        $resolver->setAllowedTypes('order', ['array']);

        $this->options = $resolver->resolve($this->options);

        if (isset($this->options['parent'])) {
            $parentResolver = new OptionsResolver();
            $parentResolver->setDefined([
                'entity',
                'parentId',
            ]);

            $parentResolver->setAllowedTypes('entity', ['string']);
            $parentResolver->setAllowedTypes('parentId', ['integer']);

            $parentResolver->setRequired([
                'entity',
                'parentId',
            ]);

            $this->options['parent'] = $parentResolver->resolve($this->options['parent']);
        }

        if ($this->options['bulk_actions'] !== false && $this->options['form_type']) {
            if ($this->getOption('allow_delete')) {
                $bulkOptions = [
                    [
                        'route' => $this->baseRoute->getBaseRouteForAction('multidelete'),
                        'route_generated' => $this->router->generate($this->baseRoute->getBaseRouteForAction('multidelete')),
                        'method' => 'DELETE',
                        'label' => 'Verwijder geselecteerde items',
                        'icon' => 'mdi mdi-delete',
                        'pre_confirm' => [
                            'text' => 'Weet je zeker dat de geselecteerde items verwijderd moeten worden?',
                        ],
                    ],
                ];
            } else {
                $bulkOptions = [];
            }

            $this->options['bulk_actions'] = array_merge($bulkOptions, (array)$this->options['bulk_actions']);
        }

        if ($this->options['actions']) {
            $this->resolveActions();
        }

        if ($this->options['bulk_actions']) {
            $this->resolveBulkActions();
        }

        if ($this->options['item_actions']) {
            $this->resolveItemActions();
        }
    }

    private function resolveActions()
    {
        $resolver = new OptionsResolver();

        $resolver->setDefaults([
            'icon' => null,
            'class' => null,
            'method' => 'POST',
            'xhr' => true,
            'action' => null,
            'pre_confirm' => null,
        ]);

        $resolver->setRequired([
            'icon',
            'label',
        ]);

        $resolver->setDefined([
            'type',
            'className',
            'route',
            'url',
            'method',
            'xhr',
            'controller',
            'route_parameters',
        ]);

        $resolver->setAllowedTypes('icon', ['null', 'string']);
        $resolver->setAllowedTypes('route', ['null', 'string']);
        $resolver->setAllowedTypes('label', 'string');
        $resolver->setAllowedTypes('class', ['null', 'string']);
        $resolver->setAllowedTypes('url', ['string', 'closure']);
        $resolver->setAllowedTypes('type', 'string');
        $resolver->setAllowedTypes('xhr', 'boolean');
        $resolver->setAllowedTypes('className', ['null', 'string']);
        $resolver->setAllowedValues('method', ['GET', 'POST', 'DELETE']);
        $resolver->setAllowedTypes('controller', ['null', 'object']);
        $resolver->setAllowedTypes('pre_confirm', ['null', 'array']);

        $resolver->setNormalizer('url', function (Options $options, $value) {
            if (empty($value)) {
                if (empty($options['route'])) {
                    throw new MissingOptionsException(
                        'When "url" is null, "route" needs to be given.'
                    );
                }

                if (empty($options['controller'])) {
                    throw new MissingOptionsException(
                        'When "controller" is null, "action" needs to be given.'
                    );
                }
            }

            return $value;
        });

        $resolver->setNormalizer('method', function (Options $options, $value) {
            $value = strtoupper($value);

            if ($value !== 'GET' && !$options['xhr']) {
                throw new InvalidOptionsException(sprintf(
                    'When "xhr" is false, "method" can only be GET, but %s was given.', $value
                ));
            }

            return $value;
        });

        $resolver->setNormalizer('pre_confirm', function (Options $options, $value) {
            void($options);

            if (\is_array($value) && empty($value['text'])) {
                throw new MissingOptionsException(
                    'When "pre_confirm" is set, "text" can cannot be empty.'
                );
            }

            return $value;
        });

        foreach ($this->getOption('actions') as $key => $action) {
            $this->options['actions'][$key] = $resolver->resolve($action);
        }
    }

    private function resolveBulkActions()
    {
        $resolver = new OptionsResolver();

        $resolver->setDefaults([
            'icon' => null,
            'multiple' => true,
            'xhr' => true,
            'method' => 'POST',
            'route_generated' => null,
            'route_params' => [],
            'pre_confirm' => null,
        ]);

        $resolver->setRequired([
            'route',
            'label',
            'icon',
            'method',
            'xhr',
            'multiple',
        ]);

        $resolver->setAllowedTypes('route', 'string');
        $resolver->setAllowedTypes('label', 'string');
        $resolver->setAllowedTypes('icon', ['null', 'string']);
        $resolver->setAllowedTypes('multiple', 'boolean');
        $resolver->setAllowedTypes('method', 'string');
        $resolver->setAllowedTypes('xhr', 'boolean');
        $resolver->setAllowedTypes('multiple', 'boolean');
        $resolver->setAllowedTypes('pre_confirm', ['null', 'array']);

        $resolver->setAllowedValues('method', ['GET', 'POST', 'DELETE']);

        $resolver->setNormalizer('method', function (Options $options, $value) {
            $value = strtoupper($value);

            if ($value !== 'GET' && !$options['xhr']) {
                throw new InvalidOptionsException(sprintf(
                    'When "xhr" is false, "method" can only be GET, but %s was given.', $value
                ));
            }

            return $value;
        });

        /** @var RouteCollection $routeCollection */
        $routeCollection = $this->router->getRouteCollection();

        $resolver->setNormalizer('route_generated', function (Options $options) use ($routeCollection) {
            $route = $routeCollection->get($options['route']);

            if (null === $route) {
                throw new \RuntimeException('No route found');
            }

            $pathVariables = $route->compile()->getPathVariables();
            $dummyParameters = [];

            foreach ($pathVariables as $key) {
                $dummyParameters[$key] = '%' . $key . '%';
            }

            return $this->router->generate($options['route'], $options['route_params'] !== []? $options['route_params']: $dummyParameters);
        });

        $resolver->setNormalizer('pre_confirm', function (Options $options, $value) {
            void($options);

            if (\is_array($value) && empty($value['text'])) {
                throw new MissingOptionsException(
                    'When "pre_confirm" is set, "text" can cannot be empty.'
                );
            }

            return $value;
        });

        foreach ($this->getOption('bulk_actions') as $key => $action) {
            $this->options['bulk_actions'][$key] = $resolver->resolve($action);
        }
    }

    private function resolveItemActions()
    {
        $resolver = new OptionsResolver();

        $resolver->setDefaults([
            'icon' => null,
        ]);

        $resolver->setRequired([
            'icon',
            'type',
        ]);

        $resolver->setDefined([
            'pre_confirm',
            'route',
            'route_parameters',
            'controller',
            'method',
            'url',
            'label',
            'attr',
            'allow_delete',
            'allow_edit',
            'allow_add',
            'allow_reorder',
            'requirement',
            'javascript_callback',
            'override_propagation',
            'identifier',
        ]);

        $resolver->setAllowedTypes('icon', 'string');
        $resolver->setAllowedTypes('route', ['null', 'string']);
        $resolver->setAllowedTypes('method', 'string');
        $resolver->setAllowedTypes('type', 'string');
        $resolver->setAllowedTypes('url', ['string', 'closure']);
        $resolver->setAllowedTypes('label', 'string');
        $resolver->setAllowedTypes('controller', ['null', 'object']);
        $resolver->setAllowedTypes('pre_confirm', ['null', 'array']);
        $resolver->setAllowedTypes('attr', ['null', 'array']);
        $resolver->setAllowedTypes('requirement', ['null', 'object']);
        $resolver->setAllowedTypes('javascript_callback', ['null', 'string']);
        $resolver->setAllowedTypes('override_propagation', ['boolean']);
        $resolver->setAllowedTypes('identifier', ['string', 'array']);
        $resolver->setAllowedTypes('allow_reorder', ['boolean', 'null']);

        $resolver->setAllowedValues('method', ['GET', 'POST', 'DELETE']);

        $resolver->setNormalizer('url', function (Options $options, $value) {
            if (empty($value)) {
                if (empty($options['route'])) {
                    throw new MissingOptionsException(
                        'When "url" is null, "route" needs to be given.'
                    );
                }

                if (empty($options['controller'])) {
                    throw new MissingOptionsException(
                        'When "controller" is null, "action" needs to be given.'
                    );
                }
            }

            return $value;
        });

        $resolver->setNormalizer('pre_confirm', function (Options $options, $value) {
            void($options);

            if (\is_array($value) && empty($value['text'])) {
                throw new MissingOptionsException(
                    'When "pre_confirm" is set, "text" can cannot be empty.'
                );
            }

            return $value;
        });

        foreach ($this->getOption('item_actions') as $key => $action) {
            $this->options['item_actions'][$key] = $resolver->resolve($action);
        }
    }

    /**
     * @param array $groups
     * @param array $newArray
     * @return array
     */
    private function buildGroupsArray(array $groups, array $newArray = [])
    {
        $shifted = array_shift($groups);
        if ($shifted !== null) {
            return $this->buildGroupsArray($groups, [$shifted => $newArray]);
        }

        return $newArray;
    }

    /**
     * @param Entity $entity
     * @return FormInterface
     */
    private function getFilterForm(Entity $entity): FormInterface
    {
        $builder = $this->formFactory->createBuilder(DataSetFilterFormType::class, $entity, []);

        return $builder->getForm();
    }
}
