<?php

namespace AdminBundle\Services\AutoDetect;

use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class AutoDetectTypeService
 * @package AdminBundle\Services\AutoDetect
 */
class AutoDetectTypeService
{
    /** @var KernelInterface */
    private $kernel;

    /**
     * AutoDetectTypeService constructor.
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @param        $entityClass
     * @param string $type
     * @return null|string
     * @throws \Exception
     */
    public function autoDetectClassType($entityClass, string $type)
    {
        $bundleName = substr($this->getBundleName($entityClass), 0, -6);
        $entityName = $this->getEntityName($entityClass);

        $className = $this->getBundleNamespace() . '\\' . ucfirst(strtolower($type)) . '\\' . $bundleName . '\\' . $entityName . 'Type';

        if (!class_exists($className)) {
            return null;
        }

        return $className;
    }

    /**
     * @return bool|string
     */
    private function getBundleNamespace()
    {
        $adminBundle = \get_class($this->kernel->getBundle('AdminBundle'));

        return substr($adminBundle, 0, strrpos($adminBundle, "\\"));
    }

    /**
     * @param $entityClass
     * @return mixed
     * @throws \RuntimeException
     */
    private function getBundleName($entityClass)
    {
        $match = null;

        preg_match('/([a-z]*Bundle)/i', $entityClass, $match);

        if (!$match) {
            throw new \RuntimeException(sprintf("Can't determine BundleName from '%s'", $entityClass));
        }

        return $match[0];
    }

    /**
     * @param $entityClass
     * @return array|mixed
     */
    private function getEntityName($entityClass)
    {
        $entityName = explode('\\', $entityClass);
        $entityName = end($entityName);

        return $entityName;
    }
}
