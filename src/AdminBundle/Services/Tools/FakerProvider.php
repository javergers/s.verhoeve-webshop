<?php

namespace AdminBundle\Services\Tools;

use libphonenumber\PhoneNumber;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class FakerProvider
{
    use ContainerAwareTrait;

    /**
     * @param string $fakePhoneNumber
     *
     * @return PhoneNumber
     * @throws \libphonenumber\NumberParseException
     */
    public function formatPhoneNumber($fakePhoneNumber)
    {
        return $this->container->get('libphonenumber.phone_number_util')->parse($fakePhoneNumber, "NL");
    }

    /**
     * @param array $strings
     * @return string
     */
    public function concat(...$strings)
    {
        return implode("", $strings);
    }

    /**
     * @param $number
     * @return string
     */
    public function formatOrderNumber($number): string
    {
        return 'order_' . $this->orderifyNumber($number) . '_0001';
    }

    /**
     * @param $number
     * @return string
     */
    public function formatOrderCollectionNumber($number): string
    {
        return 'order_collection_' . $this->orderifyNumber($number);
    }

    /**
     * @param $number
     * @return string
     */
    public function formatOrderLineNumber($number): string
    {
        return 'order_line_' . $this->orderifyNumber($number) . '_0001_1';
    }

    /**
     * @param $number
     * @return int
     */
    protected function orderifyNumber($number): int
    {
        return 10000000 + $number;
    }

    /**
     * @return string
     */
    protected function faqList(): string
    {
        $lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

        $list = "<dl class=\"faq - list\"><dt>Eerste vraag</dt><dd>$lorem</dd><dt>Tweede vraag</dt><dd>$lorem</dd><dt>Derde vraag</dt><dd>$lorem</dd></dl>";

        $content = [];
        $content[] = "<h3>Kop boven eerste lijst</h3>";
        $content[] = $list;
        $content[] = "<h3>Kop boven tweede lijst</h3>";
        $content[] = $list;

        return implode('', $content);
    }
}