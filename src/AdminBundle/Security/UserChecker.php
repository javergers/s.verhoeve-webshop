<?php

namespace AdminBundle\Security;

use AdminBundle\Exceptions\AccountDisabledException;
use AppBundle\Entity\Security\Employee\User;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\CredentialsExpiredException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserChecker
 */
class UserChecker implements UserCheckerInterface
{
    /**
     * Checks the user account before authentication.
     *
     * @param UserInterface|User $user
     *
     * @throws CredentialsExpiredException
     */
    public function checkPreAuth(UserInterface $user): void
    {
        if (false === $this->isEmployee($user)) {
            return;
        }

        if (true === $user->isCredentialsExpired()) {
            throw new CredentialsExpiredException('De inloggegevens voor dit account zijn verlopen.');
        }
    }

    /**
     * Checks the user account after authentication.
     *
     * @param UserInterface|User $user
     *
     * @throws AccountDisabledException
     */
    public function checkPostAuth(UserInterface $user): void
    {
        if (false === $this->isEmployee($user)) {
            return;
        }

        if (false === $user->isEnabled()) {
            throw new AccountDisabledException('Dit account is uitgeschakeld');
        }

        if (true === $user->isExpired()) {
            throw new AccountExpiredException('Dit account is verlopen');
        }
    }

    /**
     * @param UserInterface|User $user
     *
     * @return bool
     */
    private function isEmployee(UserInterface $user): bool
    {
        return $user instanceof User;
    }
}