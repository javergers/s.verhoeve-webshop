<?php


namespace AdminBundle\Security;

use AdminBundle\Services\AuthenticationLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;

class AuthenticationLoginSuccessHandler extends DefaultAuthenticationSuccessHandler
{
    /**
     * @var AuthenticationLogger
     */
    private $authenticationLogHandler;

    /**
     * AuthenticationSuccessHandler constructor.
     *
     * @param AuthenticationLogger $authenticationLogHandler
     */
    public function setAuthenticationLogger(AuthenticationLogger $authenticationLogHandler)
    {
        $this->authenticationLogHandler = $authenticationLogHandler;
    }

    /**
     *
     * @param Request        $request
     * @param TokenInterface $token
     *
     * @return Response never null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $this->authenticationLogHandler->success('login', $token->getUsername(), $token->getUser());

        return parent::onAuthenticationSuccess($request, $token);
    }
}
