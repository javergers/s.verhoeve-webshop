<?php

namespace AdminBundle\Datatable\Finance;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\DateColumn;
use AdminBundle\Components\Datatable\Column\DateTimeColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Finance\CommissionBatch;
use AppBundle\Entity\Finance\CommissionInvoice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CommissionInvoiceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add("commissionBatch", EntityColumn::class, [
                "label" => "Batch",
                "value" => function (CommissionBatch $commissionBatch) {
                    return $commissionBatch->getNumber();
                },
            ])
            ->add("number", TextColumn::class, [
                "label" => "Factuurnummer",
            ])
            ->add("company", EntityColumn::class, [
                "label" => "Bedrijfsnaam",
            ])
            ->add("date", DateColumn::class, [
                "label" => "Factuurdatum",
            ])
            ->add("createdAt", DateTimeColumn::class, [
                "label" => "Aangemaakt op",
            ])
            ->add("updatedAt", CallbackColumn::class, [
                "label" => "",
                "callback" => function ($value, CommissionInvoice $commissionInvoice) {
                    global $kernel;
                    void($value);

                    return '<a href="' . $kernel->getContainer()->get("router")->generate("admin_finance_commissioninvoice_render",
                            [
                                "commissionInvoice" => $commissionInvoice->getId(),
                            ]) . '" target="_blank"><button type="button" class="btn btn-xs btn-primary btn-icon"><i class="icon-file-pdf"></i></button>&nbsp;&nbsp;&nbsp;Download PDF</a>';
                },
            ]);
    }
}
