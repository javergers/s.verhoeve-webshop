<?php

namespace AdminBundle\Datatable\Finance;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\DateTimeColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Finance\CommissionBatch;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CommissionBatchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     */
    public function buildDatatable($builder)
    {
        $builder
            ->add("number", TextColumn::class, [
                "label" => "Batchnummer",
            ])
            ->add("createdAt", DateTimeColumn::class, [
                "label" => "Aangemaakt op",
            ])
            ->add("companiesCount", CallbackColumn::class, [
                "label" => "Debiteuren",
                "callback" => function ($value, CommissionBatch $commissionBatch) {
                    global $kernel;

                    return '<a href="' . $kernel->getContainer()->get("router")->generate("admin_finance_commissionbatch_twinfielddebtorsexport",
                            [
                                "commissionBatch" => $commissionBatch->getId(),
                            ]) . '"><button type="button" class="btn btn-xs btn-primary btn-icon"><i class="icon-square-down"></i></button></a>&nbsp;&nbsp;&nbsp;' . $value . ' debiteuren';
                },
            ])
            ->add("commissionInvoicesCount", CallbackColumn::class, [
                "label" => "Facturen",
                "callback" => function ($value, CommissionBatch $commissionBatch) {
                    global $kernel;

                    return '<a href="' . $kernel->getContainer()->get("router")->generate("admin_finance_commissionbatch_twinfieldexport",
                            [
                                "commissionBatch" => $commissionBatch->getId(),
                            ]) . '" target="_blank"><button type="button" class="btn btn-xs btn-primary btn-icon"><i class="icon-square-down"></i></button></a>&nbsp;&nbsp;&nbsp;' . $value . ' facturen';
                },
            ])
            // need to use a 'dummy' name for this button to show
            ->add("updatedAt", CallbackColumn::class, [
                "label" => "",
                "callback" => function ($value, CommissionBatch $commissionBatch) {
                    global $kernel;
                    void($value);

                    return '<a href="' . $kernel->getContainer()->get("router")->generate("admin_finance_commissionbatch_send",
                            [
                                "commissionBatch" => $commissionBatch->getId(),
                            ]) . '"><button type="button" class="btn btn-xs btn-primary btn-icon"><i class="icon-envelop3"></i></button></a>&nbsp;&nbsp;&nbsp;Verstuur facturen per e-mail naar leveranciers';
                },
            ]);
    }
}
