<?php

namespace AdminBundle\Datatable\Finance\Vat;

use AdminBundle\Components\Datatable\Column\DateColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AdminBundle\Components\Datatable\DatatableBuilder;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class VatRateDatatableType
 * @package AdminBundle\Datatable\Finance\Vat
 */
class VatRateDatatableType extends AbstractType
{
    /**
     * @param DatatableBuilder $builder
     * @param array            $options
     */
    public function buildDatatable(DatatableBuilder $builder, array $options)
    {
        $builder->add('code', TextColumn::class, [
            'label' => 'Code',
        ])->add('title', TextColumn::class, [
            'label' => 'Titel',
        ])->add('validFrom', DateColumn::class, [
            'label' => 'Geldig vanaf',
        ])->add('validUntil', DateColumn::class, [
            'label' => 'Geldig tot en met',
        ])->add('factor', TextColumn::class, [
            'label' => 'Factor',
        ]);
    }
}