<?php

namespace AdminBundle\Datatable\Finance\Vat;

use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\DatatableBuilder;
use Symfony\Component\Form\AbstractType;

/**
 * Class VatGroupDatatableType
 * @package AdminBundle\Datatable\Finance\Vat
 */
class VatGroupDatatableType extends AbstractType
{
    /**
     * @param DatatableBuilder $builder
     * @param array            $options
     */
    public function buildDatatable(DatatableBuilder $builder, array $options)
    {
        $builder
            ->add('country', EntityColumn::class, [
                'label' => 'Land',
            ])
            ->add('level', EntityColumn::class, [
                'label' => 'BTW Niveau',
            ]);
    }
}