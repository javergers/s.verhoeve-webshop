<?php

namespace AdminBundle\Datatable\Finance;

use AdminBundle\Components\Datatable\Column\AddressColumn;
use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\DateColumn;
use AdminBundle\Components\Datatable\Column\DateTimeColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AdminBundle\Components\Datatable\Column\UserColumn;
use AppBundle\DBAL\Types\CompanyInvoiceFrequencyType;
use AppBundle\DBAL\Types\CompanyInvoiceTypeType;
use AppBundle\DBAL\Types\ExactInvoiceExportStatusType;
use AppBundle\Entity\Relation\Company;
use AppBundle\ThirdParty\Exact\Entity\Invoice;
use AppBundle\ThirdParty\Exact\Entity\InvoiceExport;
use function array_flip;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class ExportCompanySelectType
 * @package AdminBundle\Datatable\Finance
 */
class ExportCompanySelectType extends AbstractType
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * ExportType constructor.
     * @param RouterInterface        $router
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(RouterInterface $router, EntityManagerInterface $entityManager)
    {
        $this->router = $router;
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder->add('name', TextColumn::class, [
            'label' => 'Bedrijfsnaam',
        ]);
        $builder->add('exactRelationNumber', TextColumn::class, [
            'label' => 'Debiteurcode',
        ]);
        $builder->add('mainAddress', CallbackColumn::class, [
            'label' => 'Adres',
            'callback' => static function($entity, Company $company){
                /** @var Company $company */
                if(!($address = $company->getInvoiceAddress())){
                    return '';
                }
                $addressString = $address->getStreet().' '.$address->getNumber().''.$address->getNumberAddition();
                $addressString .= ', ' . $address->getPostcode() . ' ' . $address->getCity();
                $addressString .= ', ' . $address->getCountry();
                return $addressString;
            },
        ]);
        $builder->add('invoiceType', CallbackColumn::class, [
            'label' => 'Factuur type',
            'callback' => static function($value, Company $company){
                /** @var Company $company */
                if($company === null || $company->getCompanyInvoiceSettings() === null){
                    return '';
                }

                $choices = array_flip(CompanyInvoiceTypeType::getChoices());
                return $choices[$company->getCompanyInvoiceSettings()->getInvoiceType()];
            },
        ]);
        $builder->add('invoiceCollection', CallbackColumn::class, [
            'label' => 'Factuur frequentie',
            'callback' => static function($value, Company $company){
                /** @var Company $company */
                if($company === null || $company->getCompanyInvoiceSettings() === null){
                    return '';
                }

                $choices = array_flip(CompanyInvoiceFrequencyType::getChoices());
                return $choices[$company->getCompanyInvoiceSettings()->getInvoiceFrequency()];
            },
        ]);
    }
}
