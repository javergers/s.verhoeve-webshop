<?php

namespace AdminBundle\Datatable\Discount;

use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class DiscountDatatableType
 * @package AdminBundle\Datatable\Discount
 */
class DiscountDatatableType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('description', TextColumn::class, [
                'label' => 'Omschrijving'
            ])
            ->add('rule', EntityColumn::class, [
                'label' => 'Regel'
            ])
            ->add('type', TextColumn::class, [
                'label' => 'Kortingstype'
            ])
            ->add('value', TextColumn::class, [
                'label' => 'Kortingswaarde'
            ]);
    }
}