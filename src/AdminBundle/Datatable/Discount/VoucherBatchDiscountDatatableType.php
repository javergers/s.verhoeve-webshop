<?php

namespace AdminBundle\Datatable\Discount;

use AdminBundle\Components\Datatable\Column\DateColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Security\Employee\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class VoucherBatchDiscountDatatableType
 * @package AdminBundle\Datatable\Discount
 */
class VoucherBatchDiscountDatatableType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('name', TextColumn::class, [
                'label' => 'Beschrijving'
            ])
            ->add('validFrom', DateColumn::class, [
                'label' => 'Geldig vanaf'
            ])
            ->add('validUntil', DateColumn::class, [
                'label' => 'Geldig tot'
            ]);
    }
}