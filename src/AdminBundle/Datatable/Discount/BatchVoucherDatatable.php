<?php

namespace AdminBundle\Datatable\Discount;

use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class VoucherDatatableType
 * @package AdminBundle\Datatable\Discount
 */
class BatchVoucherDatatable extends VoucherDatatableType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildDatatable($builder, array $options)
    {
        parent::buildDatatable($builder, $options);

        $builder->remove('description');
    }
}