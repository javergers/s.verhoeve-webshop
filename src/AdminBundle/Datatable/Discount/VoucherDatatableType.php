<?php

namespace AdminBundle\Datatable\Discount;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\DateColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Discount\Voucher;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class VoucherDatatableType
 * @package AdminBundle\Datatable\Discount
 */
class VoucherDatatableType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('description', TextColumn::class, [
                'label' => 'Beschrijving'
            ])
            ->add('code', TextColumn::class, [
                'label' => 'Code'
            ])
            ->add('discount', EntityColumn::class, [
                'label' => 'Kortingsregel'
            ])
            ->add('limit', TextColumn::class, [
                'label' => 'Gebruikslimiet'
            ])
            ->add('validFrom', DateColumn::class, [
                'label' => 'Geldig vanaf'
            ])
            ->add('validUntil', DateColumn::class, [
                'label' => 'Geldig tot'
            ])
            ->add('usage', CallbackColumn::class, [
                'label' => 'Gebruik',
                'callback' => function($value, Voucher $voucher) {
                    if($voucher->getOrderCollections()->count() > 0) {
                        $string = $voucher->getOrderCollections()->count();

                        switch($voucher->getUsage()) {
                            case 'limited':
                                $string .= ' / '.$voucher->getLimit();
                                break;
                            case 'unlimited':
                                $string .= ' / ongelimiteerd';
                                break;
                            default:
                                $string = 'Gebruikt';
                                break;
                        }
                        return '<span class="label label-flat border-danger text-danger-600">' . $string . '</span>';
                    }

                    return '<span class="label label-flat border-success text-success-600">Ongebruikt</span>';
                }
            ])
        ;
    }
}