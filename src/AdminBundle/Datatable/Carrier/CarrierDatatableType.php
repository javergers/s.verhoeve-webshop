<?php

namespace AdminBundle\Datatable\Carrier;

use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CarrierDatatableType
 * @package AdminBundle\Datatable\Carrier
 */
class CarrierDatatableType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options): void
    {
        $builder
            ->add('shortName', TextColumn::class, [
                'label' => 'Naam',
            ])
            ->add('company', EntityColumn::class, [
                'label' => 'Bedrijf',
            ]);
    }
}
