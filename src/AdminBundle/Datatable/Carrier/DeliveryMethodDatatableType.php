<?php

namespace AdminBundle\Datatable\Carrier;

use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class DeliveryMethodDatatableType
 * @package AdminBundle\Datatable\Carrier
 */
class DeliveryMethodDatatableType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options): void
    {
        $builder->add('name', TextColumn::class, [
            'label' => 'Naam',
        ]);
    }
}
