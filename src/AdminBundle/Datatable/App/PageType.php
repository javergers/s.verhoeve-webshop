<?php

namespace AdminBundle\Datatable\App;

use AdminBundle\Components\Datatable\Column\AccessibilityColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Site\Page;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('title', TextColumn::class, [
                'label' => 'Titel',
            ])
            ->add('site', EntityColumn::class, [
                'label' => 'Site',
            ])
            ->add('loginRequired', AccessibilityColumn::class, [
                'width' => 60,
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Page::class,
        ]);
    }
}
