<?php

namespace AdminBundle\Datatable\App;

use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Site\Usp;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UspType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        void($options);
        $builder
            ->add('title', TextColumn::class, [
                'label' => 'Titel',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Usp::class,
        ]);
    }
}
