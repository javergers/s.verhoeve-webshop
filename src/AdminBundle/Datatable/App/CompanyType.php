<?php

namespace AdminBundle\Datatable\App;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\EmailColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\PhoneNumberColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\DBAL\Types\CompanyTrustStatusType;
use AppBundle\Entity\Relation\Company;
use AppBundle\Entity\Security\Customer\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CompanyType
 * @package AdminBundle\Datatable\App
 */
class CompanyType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options): void
    {
        void($options);

        $builder
            ->add('name', CallbackColumn::class, [
                'label' => 'Bedrijfsnaam',
                'callback' => function ($value, Company $company) {
                    if ($company->getTrustStatus() === CompanyTrustStatusType::TRUST_STATUS_WHITELIST) {
                        $value .= '&nbsp;&nbsp;<i style="color: #388E3C;" class="icon-shield-check"></i>';
                    }

                    return $value;
                },
                'searchable' => true,
            ])
            // Hacky, but necessary: using another column
            ->add('deletedAt', CallbackColumn::class, [
                'label' => 'Plaats',
                'orderable' => false,
                'searchable' => false,
                'callback' => function ($value, Company $company) {
                    void($value);

                    if ($company->getMainAddress() === null) {
                        return null;
                    }

                    return $company->getMainAddress()->getCity() .' <em>('. $company->getMainAddress()->getPostcode() .')</em>';
                },
                'width' => 250
            ])
            ->add('phoneNumber', PhoneNumberColumn::class, [
                'label' => 'Telefoonnummer',
                'orderable' => false,
                'width' => 150
            ])
            ->add('email', EmailColumn::class, [
                'label' => 'E-mailadres',
                'searchable' => true,
                'width' => 180
            ])
            ->add('customerGroups', CallbackColumn::class, [
                'label' => 'Klantgroepen',
                'callback' => function ($value, Company $company) {
                    void($value);

                    if (!$company->getIsCustomer()) {
                        return '';
                    }

                    $customerGroups = [];

                    foreach ($company->getCustomerGroups() as $customerGroup) {
                        $customerGroups[] = $customerGroup->getDescription();
                    }

                    return implode(', ', $customerGroups);
                },
                'orderable' => false,
            ])
            ->add('supplierGroups', CallbackColumn::class, [
                'label' => 'Leveranciersgroepen',
                'callback' => function ($value, Company $company) {
                    void($value);

                    global $kernel;

                    if (!$company->getIsSupplier()) {
                        return '';
                    }

                    $supplierGroups = [];

                    foreach ($company->getSupplierGroups() as $supplierGroup) {
                        $supplierGroups[] = $supplierGroup->getName();
                    }

                    $href = $kernel->getContainer()->get('router')->generate('admin_customer_company_login', [
                        'id' => $company->getId(),
                    ]);

                    return ((strtolower($company->getSupplierConnector()) === 'bakker') ? '<a href="' . $href . '" target="_blank" style="color: #333;"><i class="icon-lock2"></i></a> ' : null) . implode(', ',
                            $supplierGroups) . ($company->getSupplierConnector() ? '<br /> <em style="color: #BBB;"><em class="icon-power-cord"></em> ' . $company->getSupplierConnector() . '</em>' : null);
                },
                'orderable' => false,
            ])
            ->add('customers', EntityColumn::class, [
                'label' => 'Gebruikers',
                'path' => 'admin_customer_customer_view',
                'value' => function (Customer $customer) {
                    return $customer->getFullname();
                },
                'orderable' => false,
            ]);
    }
}
