<?php

namespace AdminBundle\Datatable\App;

use AdminBundle\Components\Datatable\Column\DateColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\RruleColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SubscriptionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    //  public function buildList(ListBuilderInterface $builder, array $options)
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add("description", EntityColumn::class, [
                'label' => "Omschrijving",
            ])
            ->add("company", EntityColumn::class, [
                'label' => "Bedrijfsnaam",
            ])
            ->add("customer", EntityColumn::class, [
                'label' => "Contactpersoon",
            ])
            ->add("startDate", DateColumn::class, [
                'label' => "Begindatum",
                'datetype' => \IntlDateFormatter::LONG,
            ])
            ->add("endDate", DateColumn::class, [
                'label' => "Einddatum",
                'datetype' => \IntlDateFormatter::LONG,
            ])
            ->add("nextDate", DateColumn::class, [
                'label' => "Eerstvolgende bezorgdatum",
                'datetype' => \IntlDateFormatter::LONG,
            ])
            ->add("rrule", RruleColumn::class, [
                'label' => "Bezorgpatroon",
            ]);
    }
}
