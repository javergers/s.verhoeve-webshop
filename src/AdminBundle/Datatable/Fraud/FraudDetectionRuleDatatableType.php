<?php

namespace AdminBundle\Datatable\Fraud;

use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;

/**
 * Class FraudDetectionRuleDatatableType
 * @package AdminBundle\Datatable\Fraud
 */
class FraudDetectionRuleDatatableType extends AbstractType
{

    /**
     * @param $builder
     * @param array $options
     */
    public function buildDatatable($builder, array $options)
    {

        $builder
            ->add("rule", EntityColumn::class, [
                'label' => "Regel",
            ])
            ->add('score', TextColumn::class, [
                'label' => 'Score',
            ]);
    }

}