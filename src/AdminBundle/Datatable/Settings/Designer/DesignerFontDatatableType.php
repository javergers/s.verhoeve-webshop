<?php

namespace AdminBundle\Datatable\Settings\Designer;

use AdminBundle\Components\Datatable\Column\FontColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class DesignerFontDatatableType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('family', FontColumn::class, [
                'label' => 'Lettertype',
            ]);
    }
}
