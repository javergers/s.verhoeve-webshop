<?php

namespace AdminBundle\Datatable\Settings\Designer;

use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class DesignerCollageDatatableType
 * @package AdminBundle\Datatable\Settings\Designer
 */
class DesignerCollageDatatableType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     */
    public function buildDatatable($builder)
    {
        $builder
            ->add('name', TextColumn::class, array(
                'label' => 'Collage'
            ))
        ;
    }
}
