<?php

namespace AdminBundle\Datatable\Settings\Geographic;

use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CountryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    //  public function buildList(ListBuilderInterface $builder, array $options)
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('name', TextColumn::class, [
                'label' => 'Land',
            ])
            ->add('slug', TextColumn::class, [
                'label' => 'Slug',

            ]);
    }
}
