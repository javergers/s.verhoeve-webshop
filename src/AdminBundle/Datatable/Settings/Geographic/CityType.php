<?php

namespace AdminBundle\Datatable\Settings\Geographic;

use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CityType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    //  public function buildList(ListBuilderInterface $builder, array $options)
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
//            ->add('country', EntityColumn::class, array(
//                'label' => 'Land',
//                'value' => function($entity) {
//                    return $entity->getCountry()->translate("nl")->getName();
//                },
//                'proxy' => 'province'
//            ))
//            ->add('province', EntityColumn::class, array(
//                'label' => 'Provincie',
//                'value' => function($entity) {
//                    return $entity->getProvince()->translate("nl")->getName();
//                }
//            ))
            ->add('name', TextColumn::class, [
                'label' => 'Plaatsnaam',
            ])
            ->add('slug', TextColumn::class, [
                'label' => 'Slug',
            ])
//            ->add('postcodes', CollectionColumn::class, array(
//                'label' => 'Postcodes',
//                'value' => function($entity) {
//                    return $entity->getPostcode();
//                }
//            ))
        ;
    }
}
