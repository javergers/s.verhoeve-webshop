<?php

namespace AdminBundle\Datatable\Settings\Geographic;

use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class PostcodeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    //  public function buildList(ListBuilderInterface $builder, array $options)
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
//            ->add('country', EntityColumn::class, array(
//                'proxy' => 'city.province',
//                'label' => 'Land'
//            ))
//            ->add('city', EntityColumn::class, array(
//                'label' => 'Plaats'
//            ))
            ->add('postcode', TextColumn::class, [
                'label' => 'Postcode',
            ]);
    }
}
