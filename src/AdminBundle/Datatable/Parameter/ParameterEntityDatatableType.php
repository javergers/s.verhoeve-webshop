<?php

namespace AdminBundle\Datatable\Parameter;

use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;

/**
 * Class ParameterDatatableType
 * @package AdminBundle\Datatable\Parameter
 */
class ParameterEntityDatatableType extends AbstractType
{

    /**
     * @param       $builder
     * @param array $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('parameter', EntityColumn::class, [
                'label' => 'Sleutel',
            ])
            ->add('label', TextColumn::class, [
                'label' => 'Label',
            ])
            ->add('entity', TextColumn::class, [
                'label' => 'Entity',
            ])
            ->add('required', TextColumn::class, [
                'label' => 'Verplicht',
            ]);
    }

}
