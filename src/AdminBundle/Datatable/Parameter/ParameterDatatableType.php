<?php

namespace AdminBundle\Datatable\Parameter;

use AdminBundle\Components\Datatable\Column\BooleanColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;

/**
 * Class ParameterDatatableType
 * @package AdminBundle\Datatable\Parameter
 */
class ParameterDatatableType extends AbstractType
{

    /**
     * @param       $builder
     * @param array $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('key', TextColumn::class, [
                'label' => 'Sleutel',
            ])
            ->add('formType', TextColumn::class, [
                'label' => 'Form type',
            ])
            ->add('multiple', BooleanColumn::class, [
                'label' => 'Meerdere waarde toestaan',
            ]);
    }

}
