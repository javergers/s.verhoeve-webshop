<?php

namespace AdminBundle\Datatable\Audit;

use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AuditType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    //  public function buildList(ListBuilderInterface $builder, array $options)
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('action', TextColumn::class, [
                'label' => 'Actie',
            ])
            ->add('blame.label', TextColumn::class, [
                'label' => 'Gebruiker',
                'empty_data' => 'Systeem',
            ])
            ->add('source.fk', TextColumn::class, [
                'label' => 'Entity ID',
                'searchable' => true,
            ])
            ->add('source.class', TextColumn::class, [
                'label' => 'Entity',
            ])
            ->add('tbl', TextColumn::class, [
                'label' => 'Tabel',
            ]);
    }
}
