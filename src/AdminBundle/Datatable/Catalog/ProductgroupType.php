<?php

namespace AdminBundle\Datatable\Catalog;

use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductgroupType extends AbstractType
{
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('title', TextColumn::class, [
                'label' => 'Naam',
            ]);
    }
}
