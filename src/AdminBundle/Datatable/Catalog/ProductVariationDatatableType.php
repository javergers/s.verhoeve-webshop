<?php

namespace AdminBundle\Datatable\Catalog;

use AdminBundle\Components\Datatable\Column\AccessibilityColumn;
use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\MediaColumn;
use AdminBundle\Components\Datatable\Column\PriceColumn;
use AdminBundle\Components\Datatable\Column\TextareaColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AdminBundle\Components\Datatable\DatatableBuilder;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Security\Customer\CustomerGroup;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Form\AbstractType;

/**
 * Class ProductVariationDatatableType
 * @package AdminBundle\Datatable\Catalog
 */
class ProductVariationDatatableType extends AbstractType
{
    use ContainerAwareTrait;

    /**
     * @param DatatableBuilder $builder
     */
    public function buildDatatable(DatatableBuilder $builder)
    {
        /** @var CacheManager */
        $imagineCacheManager = $this->container->get('liip_imagine.cache.manager');

        $builder
            ->add('image', MediaColumn::class, [
                'label' => 'Afbeelding',
                'value' => function (Product $product) use ($imagineCacheManager) {
                    $decoratedProduct = $this->container->get('app.product.factory')->get($product);

                    if (!$decoratedProduct->getMainImage()) {
                        return null;
                    }

                    $image = $imagineCacheManager->getBrowserPath($decoratedProduct->getMainImage()->getPath(),
                        'product_assortment');

                    if (!$image) {
                        return null;
                    }

                    return $image;
                },
            ])
            ->add('combination', CallbackColumn::class, [
               'label' => false,
                'callback' => function($value, Product $product) {
                    if($product->isCombination()) {
                        return '<i class="mdi mdi-copyright"></i>';
                    }

                    return '';
                }
            ])
            ->add('name', TextColumn::class, [
                'label' => 'Naam',
            ])
            ->add('sku', TextColumn::class, [
                'label' => 'Sku',
            ])
            ->add('title', TextColumn::class, [
                'label' => 'Titel',
            ])
            ->add('shortDescription', TextareaColumn::class, [
                'label' => 'Omschrijving',
            ])
            ->add('customergroups', EntityColumn::class, [
                'label' => 'Beschikbaar voor',
                'value' => function (CustomerGroup $customerGroup) {
                    return $customerGroup->getDescription();
                },
            ])
            ->add('price', PriceColumn::class, [
                'label' => 'Prijs (incl. BTW)',
            ])
            ->add('default_variation', CallbackColumn::class, [
                'label' => false,
                'callback' => function($value, Product $product) {
                    $icon = '<i class="mdi mdi-star-outline"></i>';

                    if(!empty($product->getMetadata()['default_variation'])) {
                        $icon = '<i class="mdi mdi-star"></i>';
                    }

                    return sprintf('<div class="default-variation">%s</div>', $icon);
                }
            ])
            ->add('loginRequired', AccessibilityColumn::class, [
                'width' => 60,
            ]);
    }

}
