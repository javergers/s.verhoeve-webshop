<?php

namespace AdminBundle\Datatable\Catalog;

use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Catalog\Product\Productgroup;
use Symfony\Component\Form\AbstractType;

class ProductGroupPropertySetDatatableType extends AbstractType
{

    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add("name", TextColumn::class, [
                'label' => 'Set',
            ])
            ->add("description", TextColumn::class, [
                'label' => 'Omschrijving',
            ])->add('productgroup', EntityColumn::class, [
                'value' => function (Productgroup $group) {
                    return $group->getTitle();
                },
                'label' => 'Productgroep',
            ]);
    }
}
