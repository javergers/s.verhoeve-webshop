<?php

namespace AdminBundle\Datatable\Catalog;

use AdminBundle\Components\Datatable\DatatableBuilder;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class CompanyProductDatatableType
 * @package AdminBundle\Datatable\Catalog
 */
class CompanyProductDatatableType extends ProductVariationDatatableType
{
    use ContainerAwareTrait;

    /**
     * @param DatatableBuilder $builder
     */
    public function buildDatatable(DatatableBuilder $builder)
    {
        parent::buildDatatable($builder);

        $builder->remove('vat');
    }
}
