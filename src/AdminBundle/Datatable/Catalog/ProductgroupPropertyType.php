<?php

namespace AdminBundle\Datatable\Catalog;

use AdminBundle\Components\Datatable\Column\ChoiceColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\DBAL\Types\ProductgroupPropertyTypeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ProductgroupPropertyType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    //  public function buildList(ListBuilderInterface $builder, array $options)
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add("key", TextColumn::class, [
                'label' => 'Sleutelwaarde',
            ])
            ->add("description", TextColumn::class, [
                'label' => 'Omschrijving',
            ])
            ->add('form_type', ChoiceColumn::class, [
                'label' => 'Type',
                'choices' => ProductgroupPropertyTypeType::getChoices(),
            ]);
    }
}
