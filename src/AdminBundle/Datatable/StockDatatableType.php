<?php

namespace AdminBundle\Datatable;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\LabelColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Catalog\Product\Product;
use AppBundle\Entity\Relation\Company;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class StockDatatableType
 * @package AdminBundle\Datatable\Order
 */
class StockDatatableType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     */
    public function buildDatatable($builder)
    {
        $builder
            ->add('statusIndicatorLabel', LabelColumn::class, [
                'label' => '',
                'class' => 'status-label',
                'attr' => [
                    'data-stock-status' => 'stockStatus',
                    'data-product-id' => 'id',
                ],
            ])
            ->add('name', CallbackColumn::class, [
                'label' => 'Naam',
                'searchable' => true,
                'callback' => function ($value, Product $product) {
                    if ($product->getParent()) {
                        $value = '<i class="fa fa-flip-vertical fa-share"></i> &nbsp;&nbsp;'.$value;
                    }

                    return $value;
                },
            ])
            ->add('sku', TextColumn::class, [
                'label' => 'SKU',
            ])
            ->add('physicalStock', TextColumn::class, [
                'label' => 'Actuele voorraad',
            ])
            ->add('virtualStock', TextColumn::class, [
                'label' => 'Virtuele voorraad',
            ])
            ->add('stockThreshold', TextColumn::class, [
                'label' => 'Drempel',
            ]);

    }
}
