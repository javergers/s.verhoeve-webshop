<?php

namespace AdminBundle\Datatable\Site\Navigation;

use AdminBundle\Components\Datatable\Column\ChoiceColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\DBAL\Types\MenuLocationType;
use AppBundle\Entity\Site\Menu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MenuType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('name', TextColumn::class, [
                'label' => 'Naam',
            ])
            ->add('site', EntityColumn::class, [
                'label' => 'Site',
            ])
            ->add('location', ChoiceColumn::class, [
                'label' => 'Locatie',
                'choices' => array_flip(MenuLocationType::getChoices()),
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Menu::class,
        ]);
    }
}
