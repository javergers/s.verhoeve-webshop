<?php

namespace AdminBundle\Datatable\Company;

use AdminBundle\Datatable\App\CustomerType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CompanyCustomerType
 * @package AdminBundle\Datatable\App
 */
class CompanyCustomerType extends CustomerType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options): void
    {
        parent::buildDatatable($builder, $options);

        $builder->remove('company');
    }
}
