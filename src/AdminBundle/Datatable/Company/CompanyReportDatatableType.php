<?php

namespace AdminBundle\Datatable\Company;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\EntityColumn;
use AppBundle\Entity\Report\CompanyReport;
use AppBundle\Entity\Report\CompanyReportCustomer;
use AppBundle\Entity\Security\Customer\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CompanyReportDatatableType
 * @package AdminBundle\Datatable\Company
 */
class CompanyReportDatatableType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('report', EntityColumn::class, [
                'label' => 'Rapportage profiel',
            ])
            ->add('customers', EntityColumn::class, [
                'label' => 'Gekoppelde gebruikers',
                'value' => function (CompanyReportCustomer $customerReport) {
                    return $customerReport->getCustomer()->getFullname();
                }
            ]);
    }
}
