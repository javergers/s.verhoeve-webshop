<?php

namespace AdminBundle\Datatable\Company;

use AdminBundle\Components\Datatable\Column\BooleanColumn;
use AdminBundle\Components\Datatable\Column\ClassNameColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class CompanyCustomFieldDatatableType
 * @package AdminBundle\Datatable\Company
 */
class CompanyCustomFieldDatatableType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('label', TextColumn::class, [
                'label' => 'Label'
            ])
            ->add('type', ClassNameColumn::class, [
                'label' => 'Type veld',
            ])
            ->add('required', BooleanColumn::class, [
                'label' => 'Verplicht',
            ]);
    }
}