<?php

namespace AdminBundle\Datatable\Company;

use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CompanyEstablishmentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    //  public function buildList(ListBuilderInterface $builder, array $options)
    public function buildDatatable($builder, array $options)
    {
        $builder
            ->add('name', TextColumn::class, [
                'label' => 'Naam',
            ])
            ->add('company', EntityColumn::class, [
                'label' => 'Bedrijf',
            ])
            ->add('address', EntityColumn::class, [
                'label' => 'Adres',
            ]);
    }
}
