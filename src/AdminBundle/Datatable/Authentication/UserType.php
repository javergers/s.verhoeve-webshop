<?php

namespace AdminBundle\Datatable\Authentication;

use AdminBundle\Components\Datatable\Column\EntityColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AppBundle\Entity\Security\Employee\Group;
use Symfony\Component\Form\FormBuilderInterface;

class UserType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    //public function buildList(ListBuilderInterface $builder, array $options)
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('username', TextColumn::class, [
                'label' => 'Gebruikersnaam',
            ])
            ->add('firstname', TextColumn::class, [
                'label' => 'Voornaam',
            ])
            ->add('lastname', TextColumn::class, [
                'label' => 'Achternaam',
            ])
            ->add('groups', EntityColumn::class, [
                'label' => 'Lid van',
                'value' => function (Group $group) {
                    return $group->getName();
                },
                'orderable' => false
            ]);
    }
}
