<?php

namespace AdminBundle\Datatable\Authentication;

use AdminBundle\Components\Datatable\Column\CallbackColumn;
use AdminBundle\Components\Datatable\Column\DateTimeColumn;
use AdminBundle\Components\Datatable\Column\IpColumn;
use AdminBundle\Components\Datatable\Column\TextColumn;
use AdminBundle\Entity\AuthenticationLog;
use AdminBundle\Services\AuthenticationLogger;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LogType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildDatatable($builder, array $options)
    {
        void($options);

        $builder
            ->add('datetime', DateTimeColumn::class, [
                'label' => 'Datum',
                'datetype' => \IntlDateFormatter::MEDIUM,
                'timetype' => \IntlDateFormatter::SHORT,
            ])
            ->add('type', TextColumn::class, [
                'label' => 'Type',
            ])
            ->add('event', TextColumn::class, [
                'label' => 'Event',
            ])
            ->add('reason', CallbackColumn::class, [
                'label' => 'Reden',
                'callback' => function ($value, $entity) {
                    if ($entity->getEvent() != 'failure') {
                        return $value;
                    }

                    return AuthenticationLogger::getReasonDescription($value);
                },
            ])
            ->add('username', TextColumn::class, [
                'label' => 'Gebruikersnaam',
            ])
            ->add('ip', IpColumn::class, [
                'label' => 'IP-adres',
            ])
            ->add('userAgent', TextColumn::class, [
                'label' => 'User Agent',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AuthenticationLog::class,
        ]);
    }
}
