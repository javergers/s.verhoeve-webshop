<?php

namespace AdminBundle\Datatable\Report;

use AdminBundle\Components\Datatable\Column\TextColumn;
use Symfony\Component\Form\AbstractType;

/**
 * Class ReportDatatableType
 * @package AdminBundle\Datatable\Report
 */
class ReportDatatableType extends AbstractType {

    /**
     * @param $builder
     * @param array $options
     */
    public function buildDatatable($builder, array $options)
    {
        $builder->add('title', TextColumn::class, [
            'label' => 'Titel'
        ]);
    }

}