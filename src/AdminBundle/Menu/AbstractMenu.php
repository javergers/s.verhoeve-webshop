<?php

namespace AdminBundle\Menu;

use AdminBundle\Annotation\Breadcrumb;
use AppBundle\Entity\Security\Employee\User;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Knp\Menu\Iterator\RecursiveItemIterator;
use Knp\Menu\MenuItem;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Traversable;

/**
 * Class AbstractMenu
 * @package AdminBundle\Menu
 */
abstract class AbstractMenu implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ItemInterface|Traversable $menu
     * @throws \ReflectionException
     */
    protected function applyUserRoles(ItemInterface $menu): void
    {
        $token = $this->container->get('security.token_storage')->getToken();

        if (null === $token) {
            return;
        }

        $user = $token->getUser();

        if ($user->hasRole('ROLE_SUPER_ADMIN')) {
            return;
        }

        $iterator = new \RecursiveIteratorIterator(new RecursiveItemIterator($menu),
            \RecursiveIteratorIterator::CHILD_FIRST);

        $routeCollection = $this->container->get('router')->getRouteCollection();
        $normalizer = new CamelCaseToSnakeCaseNameConverter();

        foreach ($iterator as $item) {
            /**
             * @var $item MenuItem
             */
            $route = $item->getExtra('routes')[0]['route'];

            if ($route && ($routeSettings = $routeCollection->get($route))) {
                $controller = $routeSettings->getDefault('_controller');
                $controller = substr($controller, 0, strpos($controller, ':'));

                $reflectionClass = new \ReflectionClass($controller);

                $className = strtoupper(ltrim($normalizer->normalize(str_replace('Controller', null,
                    $reflectionClass->getShortName())), '_'));

                $role = 'ROLE_' . $className . '_READ';

                /** @var $user User */
                if (!$user->hasRole($role) && null !== $item->getParent()) {
                    $item->getParent()->removeChild($item);

                    continue;
                }
            }

            if (!$item->hasChildren() && !$item->getUri() && null !== $item->getParent()) {
                $item->getParent()->removeChild($item);

                continue;
            }
        }
    }

    /**
     * @param MenuItem $menu
     * @throws \ReflectionException
     */
    protected function applyExtraRoutes(MenuItem $menu): void
    {
        $request = $this->container->get('request_stack')->getMasterRequest();
        if (null === $request) {
            return;
        }

        $annotationReader = $this->container->get('annotation_reader');
        $iterator = new \RecursiveIteratorIterator(new RecursiveItemIterator($menu),
            \RecursiveIteratorIterator::CHILD_FIRST);

        $currentController = $request->attributes->get('_controller');
        $currentRoute = $request->attributes->get('_route');
        $currentRouteIndex = null;
        $reflectionClass = new \ReflectionClass(substr($currentController, 0, strpos($currentController, ':')));
        $method = $reflectionClass->getMethod(substr($currentController, strrpos($currentController, ':') + 1));

        $breadcrumbMethod = array_filter($annotationReader->getMethodAnnotations($method), function ($annotationClass) {
            return ($annotationClass instanceof Breadcrumb);
        });

        $breadcrumbMethod = array_shift($breadcrumbMethod);

        if ($breadcrumbMethod) {
            $parentRoute = $breadcrumbMethod->getParentRoute();

            if (null === $parentRoute) {
                $parentRoute = substr($currentRoute, 0, strrpos($currentRoute, '_') + 1) . 'index';
            }

            foreach ($iterator as $item) {
                /**
                 * @var $item MenuItem
                 */
                $route = $item->getExtra('routes')[0]['route'];

                if ($route === $parentRoute) {
                    $extraRoutes = $item->getExtra('routes');
                    $extraRoutes[] = $currentRoute;

                    $item->setExtra('routes', $extraRoutes);
                }
            }
        }
    }
}
