<?php

namespace AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Knp\Menu\MenuFactory;
use Knp\Menu\MenuItem;
use Symfony\Component\Routing\Router;

/**
 * Class BreadcrumbElements
 * @package AdminBundle\Menu
 */
class BreadcrumbElements extends AbstractMenu
{
    /**
     * @var ItemInterface
     */
    private $menu;

    /**
     * @var FactoryInterface
     */
    private $factory;

    /**
     * @var Router
     */
    private $router;

    /**
     * @param ItemInterface $factory
     * @param array            $options
     * @return ItemInterface
     * @throws \ReflectionException
     */
    public function menu(FactoryInterface $factory, array $options): ItemInterface
    {
        void($options);

        $this->factory = $factory;

        $this->router = $this->container->get('router');

        /** @var ItemInterface $menu */
        $this->menu = $this->factory->createItem('root', [
            'childrenAttributes' => [
                'class' => 'breadcrumb-elements',
            ],
        ]);

        $this->createHistoryMenu();
        $this->createSystemMenu();

        $this->applyUserRoles($this->menu);

        return $this->menu;
    }

    /**
     * @return void
     */
    private function createHistoryMenu(): void
    {
        $this->menu->addChild('Geschiedenis')->setAttributes([
            'id' => 'user_activity_history_menu',
            'icon' => 'hour-glass',
            'class' => 'user-activity-history-menu',
            'data-xhr' => $this->router->generate('admin_history_recentactivity')
        ]);

        $this->menu['Geschiedenis']->addChild('Meer', ['route' => 'admin_history_index']);
    }

    /**
     * @return void
     */
    private function createSystemMenu(): void
    {
        $systemMenu = $this->factory->createItem('Systeem');

        $systemMenu->setAttributes([
            'icon' => 'cog3',
        ]);

        $systemMenu->addChild('Logboeken')->setAttributes([
            'navigation_header' => true,
        ]);

        $systemMenu->addChild('Audit', ['route' => 'admin_audit_audit_index']);
        $systemMenu->addChild('Authenticatie', ['route' => 'admin_authentication_log_index']);

        $systemMenu->addChild('Overig')->setAttributes([
            'navigation_header' => true,
        ]);

        $systemMenu->addChild('Hulpmiddelen', ['route' => 'admin_admin_tools_index']);

        $routeCollection = $this->router->getRouteCollection();

        if ($routeCollection && $routeCollection->get('translation_index')) {
            $systemMenu->addChild('Vertalingen', [
                'route' => 'translation_index',
                'linkAttributes' => ['target' => '_blank'],
            ]);
        }

        if ($routeCollection && $routeCollection->get('jms_jobs_overview')) {
            $systemMenu->addChild('Achtergrond taken', [
                'route' => 'jms_jobs_overview',
                'linkAttributes' => ['target' => '_blank'],
            ]);
        }

        $this->menu->addChild($systemMenu);
    }
}
