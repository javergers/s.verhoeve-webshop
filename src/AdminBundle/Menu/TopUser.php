<?php

namespace AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class TopUser implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function menu(FactoryInterface $factory, array $options)
    {
        void($options);

        $menu = $factory->createItem('root', [
            'childrenAttributes' => [
                'class' => 'navigation',
            ],
        ]);

        $menu->addChild('Uitloggen', ['route' => 'admin_security_logout'])->setAttributes([
            "icon" => "switch2",
        ]);

        return $menu;
    }
}
