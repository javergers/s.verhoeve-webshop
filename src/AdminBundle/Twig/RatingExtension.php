<?php

namespace AdminBundle\Twig;

class RatingExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        $options = [
            'is_safe' => ['html'],
        ];

        return [
            new \Twig_SimpleFunction('rating', [$this, 'rating'], $options),
        ];
    }

    public function rating($rating, $base = 5)
    {
        // convert base
        $rating = $rating * 5 / $base;

        // round
        $rating = round($rating * 2) / 2;

        $html = str_repeat('<i class="fa text-orange-300 fa-star"></i>', floor($rating));

        if (round($rating) != $rating) {
            $html .= str_repeat('<i class="fa text-orange-300 fa-star-half-o"></i>', 1);
        }

        $html .= str_repeat('<i class="fa text-orange-300 fa-star-o"></i>', 5 - ceil($rating));

        return $html;
    }

    public function getName()
    {
        return 'stars_extension';
    }
}
