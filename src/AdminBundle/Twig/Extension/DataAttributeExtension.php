<?php

namespace AdminBundle\Twig\Extension;

class DataAttributeExtension extends \Twig_Extension
{

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('data_attribute', [$this, 'dataAttribute']),
        ];
    }

    /**
     * Parses the attribute to a string format.
     *
     * @param string $attribute
     *
     * @return string
     */
    public function dataAttribute($attribute)
    {
        if (is_array($attribute)) {
            $attribute = json_encode($attribute);
        } else {
            if (is_bool($attribute)) {
                if ($attribute) {
                    $attribute = "true";
                } else {
                    $attribute = "false";
                }
            }
        }

        return $attribute;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'admin_data_attribute';
    }
}
