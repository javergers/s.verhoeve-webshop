<?php

namespace AdminBundle\Twig\Extension;

use AdminBundle\Templating\Helper\IntlLocaleHelper;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * LocaleExtension extends Twig with local capabilities.
 *
 */
class LocaleExtension extends AbstractExtension
{
    /**
     * @var IntlLocaleHelper
     */
    protected $helper;

    /**
     * LocaleExtension constructor.
     * @param IntlLocaleHelper $helper
     */
    public function __construct(IntlLocaleHelper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new TwigFilter('country', [$this, 'country'], ['is_safe' => ['html']]),
            new TwigFilter('locale', [$this, 'locale'], ['is_safe' => ['html']]),
            new TwigFilter('language', [$this, 'language'], ['is_safe' => ['html']]),
        ];
    }

    /**
     * Returns the localized country name from the provided code.
     *
     * @param string      $code
     * @param string|null $locale
     *
     * @return string
     */
    public function country($code, $locale = null)
    {
        return $this->helper->country($code, $locale);
    }

    /**
     * Returns the localized locale name from the provided code.
     *
     * @param string      $code
     * @param string|null $locale
     *
     * @return string
     */
    public function locale($code, $locale = null)
    {
        return $this->helper->locale($code, $locale);
    }

    /**
     * Returns the localized language name from the provided code.
     *
     * @param string      $code
     * @param string|null $locale
     *
     * @return string
     */
    public function language($code, $locale = null)
    {
        return $this->helper->language($code, $locale);
    }
}
