#!/usr/bin/env bash

if [ -e /provisioning/config.sh ]; then
	. /provisioning/config.sh
fi

## Check if playbooks are present
if ! [ -d /provisioning/playbooks/main ]; then
	>&2 echo "Playbook directory is missing. Did you forget to 'git submodule init' and 'git submodule update'?"
	exit 1
fi

## Set timezone
echo "Europe/Amsterdam" > /etc/timezone
dpkg-reconfigure -f noninteractive tzdata

## Make sure apt-add-repository is available
apt-get install -y software-properties-common tofrodos

## Install ansible from PPA
apt-add-repository ppa:ansible/ansible
apt-get update
apt-get install -y ansible

## Add ansible user and allow it to sudo
useradd -m ansible
chpasswd <<< "ansible:$(date +%s | sha256sum | base64 | head -c 32)"
printf "ansible ALL=NOPASSWD:ALL\nansible ALL=(solr) NOPASSWD:ALL" | \
	(sudo su -c 'EDITOR="tee" visudo -f /etc/sudoers.d/ansible')

## Generate host variables
if ! [ -d /provisioning/playbooks/main/host_vars ]; then
	mkdir /provisioning/playbooks/main/host_vars
fi

## Generate password
sleep 2s
PHPMYADMIN_PASSWORD=$(date +%s | sha256sum | base64 | head -c 32)

cat > /provisioning/playbooks/main/host_vars/localhost.yml <<-EOF
	---
	tg_environment: dev
	topgeschenken_path: /vagrant
	topgeschenken_user: topgeschenken
	topgeschenken_group: topgeschenken

	db_hostname: localhost

	mysql_root_username: root
	mysql_root_password: dev_root
	phpmyadmin_mysql_admin_password: dev_root
	phpmyadmin_mysql_password: $PHPMYADMIN_PASSWORD

	web_sites:
	  - name: default
	    webroot: /vagrant/web
	    nginx_vars:
	      fastcgi_backend: phpfcgi_tg
	    snippets:
	      - tg-webshop
	      - secure-php

	  - name: phpmyadmin
	    template: roles/phpmyadmin/templates/nginx_site.conf.j2

	web_fcgi_pools:
	  tg: tg_app_servers
	  phpmyadmin: ctl_server

	php_version: 7.2
	php_repositories:
	  - ppa:ondrej/php

	php_fpm_pools:
	  www:
	    disable: yes

	  tg:
	    unix_socket: php7.2-fpm-tg.sock
	    appname: Topgeschenken (dev)

	  phpmyadmin:
	    unix_socket: php7.2-fpm-phpmyadmin.sock

	db:
	  tg_password: Top12345
	  users:
	    - name: vagrant
	      password: ''
	      priv: '*.*:ALL PRIVILEGES'
EOF

## Ask for NewRelic license key
if [ -n $NEWRELIC_KEY ] && ! grep '^common_newrelic_key: ' /provisioning/playbooks/main/host_vars/localhost.yml; then
	echo "common_newrelic_key: $NEWRELIC_KEY" >> /provisioning/playbooks/main/host_vars/localhost.yml
fi

## Install unison
## Force downgraded unison on Ubuntu 16.04
if [ $(lsb_release -rs) == '16.04' ]; then
	dpkg -i /provisioning/unison_2.40.102-2ubuntu1_amd64.deb
	apt-mark hold unison
	apt-get install -f -y
else
	apt-get install -y unison
fi

## enable swap
SWAP_FILE=/var/swap.1
/bin/dd if=/dev/zero of="$SWAP_FILE" bs=1M count=2048
chmod 0600 "$SWAP_FILE"
/sbin/mkswap "$SWAP_FILE"
/sbin/swapon "$SWAP_FILE"

## Correct line endings in script if necessary
fromdos /provisioning/run-playbook.sh

if [ -d "/vagrant" ] && [ -f "/vagrant/composer.lock" ]; then
	echo "Found stuff at /vagrant..."

	## Run ansible
	/provisioning/run-playbook.sh

	## update search index for locate
	updatedb &
else
	echo "Sorry, but there seems to be nothing usable at /vagrant ..."
	echo "Please sync the /vagrant folder and run 'sudo /provisioning/run-playbook.sh'"
fi
