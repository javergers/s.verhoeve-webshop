# Upgrade from 1.4 to 1.5

Site description moet nu vertaald worden [LET OP!!]
-----
De omschrijving bij de site entity is nu vertaalbaar geworden, de description wordt gebruikt in zowel de website
als de e-mailbevestigingen en moet dus na livegang gelijk gedaan worden.

Helaas (omdat er nog geen translations zijn in de koppeltabel) worden in de overzichtspagina van het site beheer geen items getoond,
de items zullen dus handmatig bewerkt moeten worden.

Door middel van het openen van de bewerk url:
https://start.topgeschenken.nl:8080/admin/instellingen/sites/[VERVANGENDOORUNIEKEID]/bewerken

Vervolgens kunnen de omschrijving velden voorzien worden van de nieuwe omschrijving.

De oude description column wordt verwijderd zodra de migraties worden gedraaid, het is dus raadzaam om van te voren
de huidige omschrijvingen te noteren.

Postcode bestand dient lokaal geplaatst te worden
----
./var/resources/postcodes_nl.xslx (let op de kolom indeling)

Leveranciers zijn nu relaties met leveranciers flag
-----
Alle huidige leveranciers moeten hiervoor aangemaakt worden als relatie met de zelfde eigenschappen als dat de leverancier reeds had.

Alle companies voorzien van isCustomer en/of isSupplier
----
Vanaf heden zijn er twee boolean kolommen aanwezig voor de kenmerken:

isCustomer en isSupplier

In de live omgeving moet er een update uitgevoerd worden om de juiste relaties daar op 1 te zetten.

Google API key
----
De aparte Google API keys zijn samengevoegd, in de parameters.yml dient vanaf heden slechts één keer de Google API key ingevuld te worden. (#703)

Leverancier paremeters
----
Parameters voor leverancier moeten vanuit de admin worden ingevuld, de hard-coded parameters zijn verwijderd. (#720)

Leverancier product doorstuurprijs, titel en omschrijving
-----
De doorstuurprijs is verplaats van het product (of productvariatie) naar leveranciers product. Tevens titel en omschrijving toegevoegd (#615)

Voicedata notificaties
-----
In deze release zijn de voicedata notificaties gefixt. Notificaties dient hun data te posten naar https://start.topgeschenken.nl/voicedata/notification
