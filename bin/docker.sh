#!/bin/bash


## TODO Check file exist parameters.yaml and .env


cd "$(dirname "$0")"
cd ..

bootscriptRoot="$(dirname '$0')"

export $(egrep -v '^#' .env | xargs)
export DOCKER_SYNC_STRATEGY="unison"

if [[ "$OSTYPE" == "linux-gnu" ]]; then
    os='ux'
    export DOCKER_SYNC_STRATEGY="native_linux"
elif [[ "$OSTYPE" == "darwin"* ]]; then
   os='ux'
   export DOCKER_SYNC_STRATEGY="native_osx"
elif [[ "$OSTYPE" == "cygwin" ]]; then
   os='ms'
elif [[ "$OSTYPE" == "msys" ]]; then
   os='ms'
elif [[ "$OSTYPE" == "win32" ]]; then
   echo "Use git bash!"
   exit
elif [[ "$OSTYPE" == "freebsd"* ]]; then
   os='ux'
else
   echo "Unknown OS"
   exit
fi

if [[ "$1" == "shell" ]]; then
  docker exec -it tg_php bash
  exit
fi

if [[ "$1" == "help" ]]; then
  echo "'bash bin/docker-wsl.sh'            start docker with 'prod' environment"
  echo "'bash bin/docker-wsl.sh %env%'      start docker with '%env%' environment"
  echo "'bash bin/docker-wsl.sh dev shell'  start docker with 'dev' environment and open a shell"
  echo "'bash bin/docker-wsl.sh shell'      open a shell"
  echo "'bash bin/docker-wsl.sh stop'       stop docker"
  exit
fi

env=$1
env=${env:=prod}

if [[ "$env" != "dev" && "$env" != "prod" && "$env" != "stop" ]]; then
	echo "Use argument 'dev' or 'prod' for the environment"
	exit
fi

## run this file to test docker setup locally

#docker volume create --name=app-sync

echo "#####################"
echo "## stopping docker ##"
echo "#####################"

docker-sync stop
docker-compose stop

if [[ "$1" == "stop" ]]; then
  exit
fi

echo "##########################"
echo "## starting docker-sync ##"
echo "##########################"

echo "#############################"
echo "## starting docker-compose ##"
echo "#############################"

docker-compose -f docker-compose.yml -f docker-compose-${env}.yml up -d

bash ci/file_permissions.sh
bash ci/key_permissions.sh

if [[ "$2" == "reset" ]]; then
  bash ci/composer-install.sh
  bash ci/yarn-install.sh
  bash ci/reset.sh
  bash ci/solr-install.sh
fi

#bash ci/doctrine-schema-update.sh
#bash ci/phpunit.sh

if [[ "$2" == "shell" ]]; then
	docker exec -it tg_php bash
fi
