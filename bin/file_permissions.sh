#!/usr/bin/env bash

HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)

setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX /app/var
setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX /app/var

mkdir -p /app/web/media

setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX /app/web/media
setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX /app/web/media
