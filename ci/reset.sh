#!/usr/bin/env bash

set -eu

echo "######################"
echo "## running reset.sh ##"
echo "######################"

## reset
docker-compose exec -T php sh -c 'cd /app && php bin/console reset'