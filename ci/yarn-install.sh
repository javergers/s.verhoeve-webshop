#!/usr/bin/env bash

set -eu

echo "#############################"
echo "## running yarn-install.sh ##"
echo "#############################"

## composer
docker-compose exec -T php sh -c 'cd /app && yarn install'

# copy composer files
echo "COPY node files to local"
rm -dfR ./node_modules
docker cp tg_php:/app/node_modules ./node_modules