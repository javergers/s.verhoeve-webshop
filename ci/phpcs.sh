#!/usr/bin/env bash

set -eu

echo "######################"
echo "## running phpc.sh ##"
echo "######################"

docker-compose exec -T php sh -c 'cd /app && php bin/phpcs.phar src public'