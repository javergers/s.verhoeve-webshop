export TG_SSH_KEY=${HOME}/.ssh/${SSH_KEY_NAME}

docker-compose exec -T php sh -c 'rm -rf ~/.ssh && mkdir ~/.ssh'
echo ${TG_SSH_KEY}
docker cp ${TG_SSH_KEY} tg_php:/root/.ssh
docker-compose exec -T php sh -c 'chmod 700 ~/.ssh && chmod 644 ~/.ssh/*.pub && chmod 600 ~/.ssh/id_rsa'

docker-compose exec -T php sh -c 'eval $(ssh-agent -s) && ssh-add ~/.ssh/id_rsa'

docker-compose exec -T php sh -c 'ssh-keyscan -H bitbucket.org >> ~/.ssh/known_hosts';
docker-compose exec -T php sh -c 'ssh-keyscan -H github.com >> ~/.ssh/known_hosts';

#docker-compose exec -T php sh -c 'ssh git@bitbucket.org'
#docker-compose exec -T php sh -c 'ssh git@github.com'

