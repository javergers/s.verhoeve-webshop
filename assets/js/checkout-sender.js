require('./../scss/checkout-sender.scss');

const CustomerAddress = require('./includes/customer-address');

let checkoutSender = new function () {

};

checkoutSender.init = function () {
    checkoutSender._bindEvents();

    $('#cart_site_checkout_payment_ideal_issuer').css('width', '100%').select2({
        placeholder: trans('payment.choose_your_bank'),
        minimumResultsForSearch: -1,
        templateResult: function (state) {
            if (state.loading) {
                return state.loading;
            }
            return state.text;

            //Disable icons
            //Missing files
            // 0804.png (Handelsbanken)
            // 0803.png (Moneyou)

            // if (state.text.indexOf('Test') !== -1) {
            //     return state.text;
            // }
            //
            // if (state.loading) {
            //     return state.loading;
            // }
            //
            // /**
            //  * Logo's
            //  * @link https://shop.aeneas.nl/veilig-betalen
            //  */
            // return $(
            //     '<span><img src="/bundles/app/img/issuers/icons/' + state.id + '.png"/> ' + state.text + '</span>'
            // );
        }
    });

     var dropdown = $('.extra-field-dropdown');
     dropdown.select2();
};

checkoutSender._bindEvents = function () {
    CustomerAddress.init();

    $('[data-toggle="tooltip"]').each(function (i, elm) {
        new Tooltip(elm);
    });

    $('#cart_submit, #site_checkout_payment_cart_submit').on('click', function(e) {
        $('.payment-tab').each(function() {
            if(!$(this).hasClass('active')) {
                $(this).find('input').val('');
                $(this).find('option:selected').removeAttr('selected');
            }
        });

        var customerType = $('[name="cart[customerType]"]:checked').val();

        if(customerType === 'customer') {
            $('#cart_customer_default_invoice_address_companyName').val('');
        }

        $(this).submit();
    });

    $('#content-shoppingcart').on('change', '.payment-tab input[type=radio][name="cart[paymentMethod]"]', function () {
        var paymentTab = $(this).closest('.payment-tab');

        if (paymentTab.hasClass('active')) {
            return;
        }

        // Reset tabs and all dropdowns
        $('.payment-tab').removeClass('active');
        $('.drop-active', '.payment-tab').slideUp(function () {
            $(this).removeClass('drop-active');
        });

        // Activate current tab
        paymentTab.addClass('active');
        paymentTab.find('.payment-dropdown').slideDown(function () {
            $(this).addClass('drop-active');
        });

        $(this).attr('checked', true).prop('checked', true);

        var subtotal_element = $('.subtotal', '.order-dropdown.active');
        var subtotal = parseFloat(subtotal_element.attr('data-original-subtotal'));

        if (subtotal) {
            subtotal = subtotal.replace(',', '.');
        }

        var product_line_html = '\
                <div class="product-item payment-costs">\
                    <div class="left">\
                        <div class="product-foto"></div>\
                    </div>\
                    <div class="right">\
                        <div class="product-row">\
                            <p class="name"></p>\
                            <p class="price"></p>\
                        </div>\
                        <div class="product-extras"></div>\
                    </div>\
                </div>';


        if (paymentTab.attr('data-payment-costs-amount')) {
            var payment_costs = parseFloat(paymentTab.attr('data-payment-costs-amount').replace(',', '.'));
            var subtotal_new = (subtotal + payment_costs).toFixed(2).replace('.', ',');

            // subtotal_element.find('.right span').html(subtotal_new);

            var payment_costs_line = $(product_line_html);
            payment_costs_line.find('.right .name').html(paymentTab.attr('data-payment-costs-name'));
            payment_costs_line.find('.right .price').html('&euro; ' + paymentTab.attr('data-payment-costs-amount'));

            payment_costs_line.insertBefore(subtotal_element);
        } else {
            // subtotal_element.find('.right span').html(subtotal.toFixed(2).replace('.', ','));

            $('.product-item.payment-costs', '.order-dropdown.active').remove();
        }
    })
    .on('click', '.payment-row', function () {
        var input = $(this).find('input[type=radio][name="cart[paymentMethod]"]');
        input.attr('checked', true);
        input.trigger('change');
    })
    .on('click', '.order-row', function () {
        if ($(this).closest('.order-tab').hasClass('active')) {
            return;
        }

        $('.order-tab').removeClass('active');

        $('.active', '.order-tab').slideUp(function () {
            $(this).removeClass('active');
        });

        // Activate current tab
        $(this).closest('.order-tab').addClass('active');
        $(this).closest('.order-tab').find('.order-dropdown').slideDown(function () {
            $(this).addClass('active');
        });
    });

    $('input[type=radio]', '#cart_customerStatus').on('change', function () {
        var value = $('input[type=radio]:checked', '#cart_customerStatus').val();
        
        var termsElm = $('#cart_terms');

        if (value === 'new') {
            termsElm.attr('required', 'required');
            $('#form-client').hide();
            $('#form_new_client').show();
            $('#cart_customer_email').closest('.form-group').show();


            $('#customer_country_container').show();
            $('#customer_type').show();
        } else if (value === 'existing') {
            termsElm.removeAttr('required');
            $('#form_new_client').hide();
            $('#form-client').show();

            $('#cart_customer_email').closest('.form-group').hide();

            $('#cart_login_login_form_active').val('');

            $('#customer_country_container').hide();
            $('#customer_type').hide();
        }
    }).trigger('change');

    $('#cart_login_submit').on('click', function () {
        $('#cart_login_login_form_active').val(1);

        $('#cart').submit();
    });

    $('.country-choice input', '#customer_country_container').on('change', function () {
        var company_registration_description = $('.company-account-information');

        if ($(this).val().toLowerCase() === 'other') {
            company_registration_description.addClass('hide-form');
        } else {
            company_registration_description.removeClass('hide-form');
        }

        $('#cart_companyAccountRegistration input').trigger('change');
    });

    $('#cart_companyAccountRegistration input').on('change', function () {
        var customerType = $('#cart_customerType input:checked').val();
        var country_list = $('.country-list', '#customer_country_container');
        var form_client_data = $('.form-client-data');
        var company_account_registration_identification = $('.row-company-account-registration-identification');
        var company_account_registration_coc = $('.row-company-account-registration-coc');
        var company_account_registration_vat = $('.row-company-account-registration-vat');

        $('#cart_customerType_1').trigger('click')

        if ($('#cart_companyAccountRegistration input:checked').length === 0 && (customerType !== 'customer' && country_list.val() && ['nl', 'be'].indexOf(country_list.val().toLowerCase()) !== -1)) {
            form_client_data.addClass('hide-form');
            $('.company-account-information').removeClass('hide-form');
            company_account_registration_identification.hide();
            company_account_registration_vat.hide();
            company_account_registration_coc.hide();
        } else if (customerType === 'customer') {
            $('.company-account-information').addClass('hide-form')
        } else {
            var cartTermInput = $('#cart_terms');

            if ($('#cart_companyAccountRegistration input:checked').val() === '0' || (!country_list.val() || ['nl', 'be'].indexOf(country_list.val().toLowerCase()) === -1)) {
                cartTermInput.attr('required', 'required');
                form_client_data.removeClass('hide-form');
                company_account_registration_identification.hide();
                company_account_registration_vat.hide();
                company_account_registration_coc.hide();
            } else {
                cartTermInput.removeAttr('required');

                form_client_data.addClass('hide-form');
                $('.company-account-information').removeClass('hide-form')
                company_account_registration_identification.show();

                if (country_list.val().toLowerCase() === 'nl') {
                    company_account_registration_vat.hide();
                    company_account_registration_coc.show();
                } else if (country_list.val().toLowerCase() === 'be') {
                    company_account_registration_coc.hide();
                    company_account_registration_vat.show();
                } else {
                    company_account_registration_identification.hide();
                }
            }
        }
    });

    $('#cart_customerType input').on('change', function () {
        var form_client_data = $('.form-client-data');
        var country_choice = $('.country-choice input:checked', '#customer_country_container');
        var company_registration_description = $('.company-account-information');
        var company_account_registration_information = $('.company-account-description');
        var company_name_input = $('.row-company');
        var customer_registration_information = $('.row-customer-registration');

        if ($('#cart_customerType input:checked').length === 0) {
            form_client_data.addClass('hide-form');
            customer_registration_information.toggleClass('hide-form', true);
        } else {
            var country_label = $('#customer_country_container .row-country-label');

            if ($('#cart_customerType input:checked').val() !== 'company') {
                $('#cart_companyAccountRegistration input').removeAttr('checked').removeProp('checked');
                company_registration_description.addClass('hide-form');
                form_client_data.removeClass('hide-form');

                country_label.html(country_label.attr('data-label-customer'));

                $('#cart_companyAccountRegistration input').removeAttr('checked').removeProp('checked');
                company_account_registration_information.removeClass('hide-form');
                company_name_input.hide();
                company_name_input.find('label').attr('placeholder', company_name_input.find('label').attr('alt'));
                customer_registration_information.toggleClass('hide-form', false);
            } else {
                if (!$('#form_new_client').attr('data-company')) {
                    form_client_data.addClass('hide-form');
                }

                company_registration_description.removeClass('hide-form');

                country_label.html(country_label.attr('data-label-company'));

                company_name_input.find('label').attr('placeholder', company_name_input.find('label').attr('alt') + '*');
                company_name_input.show();

                $('#cart_companyAccountRegistration input').trigger('change');
                customer_registration_information.toggleClass('hide-form', true);
            }
        }

        if (country_choice.val().toLowerCase() === 'other') {
            company_registration_description.addClass('hide-form');
        }
    }).trigger('change');

    $("input[name='cart[customerType]'], input[name='cart[customer][country_choice]']").on('change', function (e) {
        // if ($(this).val() && !$("#payments").hasClass('loading') && $(this).val() != $(this).prop('defaultValue')) {
        var cart = {
            'site_checkout_payment': {}
        };

        cart.paymentMethod = $('input[name="cart[paymentMethod]"]:checked').val();
        cart.customerType = $('input[name="cart[customerType]"]:checked').val();
        cart.customerCountry = $('input[name="cart[customer][country_choice]"]:checked').val();

        $.ajax({
            url: $('#payments').data('payment-method-url'),
            method: 'POST',
            dataType: 'html',
            data: {
                cart: cart
            }
        }).done(function (response) {
            var values = {};

            $.each($('#payment_methods').find('input[type=text], select'), function () {
                values[$(this).attr('id')] = $(this).val();
                cart_companyAccountRegistration });

            $('#payment_methods').html(response).show();

            $.each($('#payment_methods').find('input[type=text], select'), function () {
                $(this).val(values[$(this).attr('id')]);
                $(this).toggleClass('filled', $(this).val());
            });

            $('[data-toggle="popover"]', '#payment_methods').each(function (i, elm) {
                new Popover(elm, {
                    trigger: 'focus',
                    placement: 'bottom'
                });
            });
        }).fail(function (response) {
            //$("#payments #payment_methods").remove();
        }).always(function () {
            //$("#payments").removeClass('loading');
        });
        // }
    });

    $('#cart_site_checkout_payment_creditcard_number').on('keydown', function (e) {
        if (e.keyCode === 32) {
            return false;
        }

        return true;
    })
    .on('change', function () {
        $(this).val($(this).val().replace(/[^0-9.]/g, ''));
    });

    $('[data-toggle="popover"]').each(function (i, elm) {
        new Popover(elm, {
            trigger: 'focus',
            placement: 'bottom'
        });
    });

    $('body')
    .on('click', '.set-create-account', function (e) {
        e.preventDefault();
        $('#cart_customerStatus_0').trigger('click');
        $('#cart_customerType_1').trigger('click');
        $('#cart_companyAccountRegistration_0').trigger('click');
    }).on('click', '.set-already-account', function (e) {
        e.preventDefault();
        $('#cart_customerStatus_1').trigger('click');
    });
};

checkoutSender.init();
