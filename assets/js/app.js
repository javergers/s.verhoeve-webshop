import './includes/images';

import './../../assets/scss/app.scss';

import Navigation from './components/Navigation';
import MobileMenu from './components/MobileMenu';
import ReturnToTop from './components/ReturnToTop';
import SearchBar from './components/SearchBar';

const flashbag = require('./includes/flashbag');
const banner = require('./includes/banner');
const menus = require('./includes/menus');
const timer = require('./includes/timer');

import formValidation from './includes/form-validation';

let APP = new function () {
    this.flashbag = flashbag;
    this.banner = banner;
    this.timer = timer;
    this.formValidation = formValidation;
    this.navigation = new Navigation;
};

if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.msMatchesSelector ||
        Element.prototype.webkitMatchesSelector;
}

if (!Element.prototype.closest) {
    Element.prototype.closest = function(s) {
        var el = this;

        do {
            if (el.matches(s)) return el;
            el = el.parentElement || el.parentNode;
        } while (el !== null && el.nodeType === 1);
        return null;
    };
}

APP.init = function () {
    if ($('#header').length === 1) {
        this._initHeader();
    }

    this._bindEvents();

    // Load IE Dependencies conditionally
    var ua = window.navigator.userAgent;
    if (ua.indexOf('Trident/') > 0 || ua.indexOf('Edge/') > 0) {
        this._loadIEDependencies();
    }

    this.mobileMenu = new MobileMenu();
    this.returnToTop = new ReturnToTop();
    this.searchBar = new SearchBar();

    menus.topmenu.init();

    this.formValidation.init();

    $.fn.select2.defaults.set('language', 'nl');

    this.getCartCount();

};
APP._loadIEDependencies = function () {
    $.getScript('https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js');
};

APP._bindEvents = function () {
    // Setup lazyload
    new LazyLoad({
        elements_selector: ".lazy"
    });

    var textBlockInner = $('#text-blocks .text-block-inner');
    var textBlockReadMore = $('#text-blocks .text-block-read-more');

    if (textBlockInner && textBlockInner.length === 1) {
        var innerHeight = textBlockInner.get(0).scrollHeight;
        var height = textBlockInner.height();
        var textBlocksHeight= $('#text-blocks').innerHeight();

        if (innerHeight > height) {
            textBlockReadMore
                .css('visibility', 'visible')
                .css('display', 'block')
            ;
        }

        if (innerHeight <= textBlocksHeight) {
            textBlockReadMore.hide();
        }
    }

    textBlockReadMore.on('click', function () {
        var parent = $(this).parents('.text-block');

        if (parent.length > 1) {
            parent = parent[1];
        }

        if (!$(parent).hasClass('open')) {
            var clone = $(parent).clone(true);

            $(clone).css({
                'position': 'absolute',
                'z-index': 2,
                'top': 0,
                'left': 0,
                'background-color': '#fff'
            }).addClass('child');

            $(clone).find('.text-block-inner').css({
                'height': 'auto',
                'overflow': 'visible'
            });

            $(clone).appendTo($(parent));

            $(clone).find('.text-block-read-more').text($(clone).find('.text-block-read-more').attr('data-text-close'));

            $(parent).toggleClass('open', true);
        } else {
            $(parent).find('.child').fadeOut('fast', function () {
                $(this).css({
                    'box-shadow': 'none',
                    '-webkit-box-shadow': 'none',
                    '-moz-box-shadow': 'none'
                }).remove();
            });

            $(parent).toggleClass('open', false);
        }
    });

    $('.footer-dropdown h3').on('click', function (e) {
        if ($(window).width() < 769) {
            $(this).parent().find('ul').toggle();
            $(this).find('.down').toggle();
            $(this).find('.up').toggle();
        }
    });

    $('#toggle-menu').on('click', function (e) {
        $('html').toggleClass('show-mobile-menu');
    });

    $('body').on('blur', 'textarea, input[type="text"], input[type="password"], input[type="email"], input[type="number"], input[type="tel"]', function () {
        $(this).removeClass('filled');

        if ($(this).val()) {
            $(this).addClass('filled');
        }
    });
};

APP._initHeader = function () {
    $('.header-blok', '#header').delay(1000).fadeIn();
};

APP.getCartCount = function () {
    const productCountElm = $('.product-count');
    const isVisible = productCountElm.is(':visible');

    if(!isVisible) {
        return false;
    }

    const productCountUrl = productCountElm.data('url');

    $('.shopping-hover .count-items').remove();

    $.ajax({
        url: productCountUrl,
    }).done(function (count) {
        if (count > 0) {
            productCountElm.attr('data-count', count).html('<span>' + count + '</span>');
        }
    });
};

$(function() {
    APP.init();
});

export default APP;
