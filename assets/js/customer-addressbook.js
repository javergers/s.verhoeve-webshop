require('./customer-register');
require('./../scss/customer-account.scss');
require('./../scss/customer-addressbook.scss');
require('jquery.fancytable');

$(document).ready(function() {
    let fancyTableOptions = {
        sortColumn:0,
        inputPlaceholder: trans('cart.order_address.search'),
        paginationClass: 'btn btn-theme-secondary',
        paginationClassActive: 'theme-background-color',
        sortable: true,
        pagination: true,
        searchable: true,
        globalSearch: true,
        perPage: 5,
    };

    let addressBookTable = $("#account-address-book-table");
    addressBookTable.fancyTable(fancyTableOptions);
    addressBookTable.find('input').addClass('form-control');
});

