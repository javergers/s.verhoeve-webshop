/**
 * @Description: Jquery Plugin to display long textareas truncated
 *
 * @param options
 * @returns {*}
 */
$.fn.truncate = function (options) {

    var self = this;
    var settings = $.extend({
        minLines: 2,
        openText: 'Lees meer...',
        closeText: 'Sluiten'
    }, options);

    this.init = function () {

        this.setLineHeight();
        if (this.height() > (settings.minLines * this.lineHeight)) {
            this.display('truncated');
            this.bindEvents();
        }
    };

    this.display = function (type) {
        if (type === 'truncated') {
            this.toggleClass('truncated');
            var html = this.html();
            var newHtml = '' +
                '<div class="text-content" style="height: ' + (this.lineHeight * settings.minLines) + 'px; overflow:hidden;">' +
                html +
                (settings.minLines > 1 ? '<div class="transparent-layer" style="height:' + (this.lineHeight) + 'px"></div>' : '') +
                '</div>' +
                '<div class="text-actions">' +
                '<div class="toggle"' +
                ' data-action="toggle"' +
                ' data-text-open="' + settings.openText + '"' +
                ' data-text-close="' + settings.closeText + '">' +
                settings.openText +
                '</div>' +
                '</div>';
            this.html(newHtml);
        }
    };

    this.bindEvents = function () {
        this.on('click', '[data-action=toggle]', function () {
            var elm = $(this);
            var productDescription = elm.closest('.product-description');
            var openText = elm.attr('data-text-open');
            var closeText = elm.attr('data-text-close');

            productDescription.toggleClass('expand');
            if (productDescription.hasClass('expand')) {
                elm.text(closeText);
                productDescription.find('.text-content').height('auto');
            } else {
                elm.text(openText);

                var height = (self.lineHeight * settings.minLines);
                productDescription.find('.text-content').height(height);
            }
        });
    };

    this.setLineHeight = function () {
        var elm = this;
        if (this.find('p').length > 0) {
            elm = this.find('p');
        }
        this.lineHeight = elm.css('line-height').replace('px', '');
    };

    return this.init();
};
