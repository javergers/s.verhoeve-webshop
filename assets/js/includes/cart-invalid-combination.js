require('./../../scss/_components/_cart-invalid-combination.scss');

function renderCartInvalidCombinationModal(modal) {
    var url = $('#content-product').attr('data-invalid-cart-combi-url');

    if (!url) {
        swal.close();
    }

    swal.showLoading();

    $.get(url, {}, function (html) {
        $('#swal2-content', modal).html(html).show();

        swal.hideLoading();

        var quantity = $('#form_quantity').val();

        if (!quantity) {
            quantity = 1;
        }

        $('#cart_invalid_combination #quantity', modal).html(quantity);

        $('.swal2-confirm', modal).attr('disabled', true).show();

        $('.item-container', modal).on('click', function (e) {
            e.preventDefault();

            $('.item-container').removeClass('selected');
            $(this).addClass('selected');
            $('.swal2-confirm, .swal2-cancel', modal).html($('#cart_invalid_combination', modal).attr('data-text-confirm-label'));

            if ($(this).closest('[data-current-product]').length === 1) {
                $('.swal2-cancel', modal).removeAttr('disabled').removeProp('disabled').show();
                $('.swal2-confirm', modal).attr('disabled', true).prop('disabled', true).hide();
            } else {
                $('.swal2-confirm', modal).removeAttr('disabled').removeProp('disabled').show();
                $('.swal2-cancel', modal).attr('disabled', true).prop('disabled', true).hide();
            }
        });
    });
}

window.renderCartInvalidCombinationModal = renderCartInvalidCombinationModal;
