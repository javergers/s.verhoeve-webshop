import _includes from 'lodash/includes';
import _indexOf from 'lodash/indexOf';
import _first from 'lodash/first';
import _last from 'lodash/last';
import _nth from 'lodash/nth';

$.fn.qty = function () {
    let qtyClickTimeout;
    let qtyIsBeingUpdated = false;

    const disableClickTimer = () => {
        clearTimeout(qtyClickTimeout);

        qtyIsBeingUpdated = false;
    };

    const enableClickTimer = (callback, subject, onSuccess) => {
        qtyClickTimeout = setTimeout(() => {
            callback(subject, onSuccess);
        }, 500);
    };

    const updateQuantity = (subject, onSuccess) => {
        const url = subject.attr('data-url');
        const value = parseInt(subject.val());

        qtyIsBeingUpdated = true;

        if (!value || value < 0) {
            disableClickTimer();

            return;
        }

        subject.trigger('qty-update-started');

        $.ajax({
            url: url,
            method: 'POST',
            data: {
                quantity: value
            }
        }).done(() => { // when callback is successfull inform user
            subject.prop('defaultValue', value);
            subject.removeData('paste');

            if (typeof(onSuccess) !== 'undefined') {
                onSuccess();
            }
        }).fail(() => { // when callback fails inform user

        }).always(disableClickTimer); // always clear the timeout
    };

    const showUpdateStatusIcon = (target) => {
        target.find('.fa').hide();
        target.append('<span class="checkmark"></span>');

        setTimeout(() => {
            target.find('.fa').fadeIn();
            target.find('.checkmark').remove();
        }, 1000);
    };

    const qtyButtonProcessor = (e) => {
        const target = $(e.currentTarget);
        const qtyInput = e.data.qtyInput;
        const action = target.attr('data-action');
        const ranges = qtyInput.attr('data-ranges') ? JSON.parse(qtyInput.attr('data-ranges')) : null;
        const min = qtyInput.attr('min') ? parseInt(qtyInput.attr('min')) : 1;
        const max = qtyInput.attr('max') ? parseInt(qtyInput.attr('max')) : 99;
        const previousValue = qtyInput.prop('defaultValue');

        let currentQty = parseInt(qtyInput.val());

        if (qtyIsBeingUpdated) {
            return;
        }

        e.preventDefault();

        disableClickTimer();

        // Check if there is a predefined range with steps and if the current value exists in the collection
        if (ranges && !_includes(ranges, currentQty)) {
            return;
        }

        currentQty = determineQuantityStep(ranges, currentQty, min, max, action);

        qtyInput.val(currentQty);

        enableClickTimer(updateQuantity, qtyInput, () => {
            target.find('.fa').hide();
            target.append('<span class="checkmark"></span>');

            qtyInput.trigger('qty-updated');

            if (currentQty > previousValue) {
                qtyInput.trigger('qty-increased');
            } else {
                qtyInput.trigger('qty-decreased');
            }

            setTimeout(() => {
                target.find('.fa').fadeIn();
                target.find('.checkmark').remove();
            }, 500);
        });
    };

    const qtyInputProcessor = (e) => {
        const qtyInput = $(e.currentTarget);
        let currentQty = parseInt(qtyInput.val());

        const qtyIncrease = qtyInput.parent('.qty-selector').find('a.plus');
        const qtyDecrease = qtyInput.parent('.qty-selector').find('a.minus');
        const previousValue = qtyInput.prop('defaultValue');
        const target = currentQty > previousValue ? qtyIncrease : qtyDecrease;
        const action = currentQty > previousValue ? 'increase' : 'decrease';
        const ranges = qtyInput.attr('data-ranges') ? JSON.parse(qtyInput.attr('data-ranges')) : null;
        const min = qtyInput.attr('min') ? parseInt(qtyInput.attr('min')) : 1;
        const max = qtyInput.attr('max') ? parseInt(qtyInput.attr('max')) : 99;

        currentQty = determineQuantityStep(ranges, currentQty, min, max, action, true);

        qtyInput.val(currentQty);

        enableClickTimer(updateQuantity, qtyInput, () => {
            $(qtyInput).prop('defaultValue', currentQty);

            showUpdateStatusIcon(target);
        });
    };

    const determineQuantityStep = (ranges, currentQty, min, max, action, manual) => {
        const mod = manual === true ? 0 : action === 'increase' ? 1 : -1;
        const currentIndex = _indexOf(ranges, currentQty);
        const newIndex = (currentIndex + mod);

        if (!ranges) {
            if (action === 'increase' && (currentQty + mod) > max) {
                return max;
            } else if (action === 'decrease' && (currentQty + mod) < min) {
                return min;
            } else {
                return (currentQty + mod);
            }
        }

        let newQuantity = _nth(ranges, newIndex);

        if (typeof (newQuantity) === 'undefined' || (newIndex < min || newIndex > max)) {
            newQuantity = action === 'increase' ? _last(ranges) : _first(ranges);
        }

        return newQuantity;
    };

    return this.each(function () {
        const qtyInput = $(this).find('input');
        const qtySelect = $(this).find('select');
        const qtyIncrease = $(this).find('a.plus');
        const qtyDecrease = $(this).find('a.minus');

        if (qtyInput.length === 1) {
            if (!qtyInput.attr('data-qty-initialized')) {
                if (!qtyDecrease.attr('data-action')) {
                    throw 'Missing data action for decrease button';
                }

                if (!qtyIncrease.attr('data-action')) {
                    throw 'Missing data action for increase button';
                }

                qtyInput.on('paste keyup blur', (e) => {
                    if (qtyIsBeingUpdated) {
                        return;
                    }

                    disableClickTimer();

                    const element = $(e.currentTarget);
                    const value = $(element).val().replace(/[^0-9]/g, '');
                    const previousValue = $(qtyInput).prop('defaultValue');

                    switch (e.type) {
                        case 'keyup':
                            if ((e.isTrigger || typeof (e.isTrigger) === 'undefined') && element.data('paste')) {
                                qtyInputProcessor(e);
                            } else {
                                if (e.type === 'keyup' && !((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105))) {
                                    return false;
                                }

                                qtyInputProcessor(e);
                            }

                            break;
                        case 'paste':
                            $(element).data('paste', true).trigger('keyup');

                            break;

                        case 'blur':
                            if ((!value || parseInt(value) <= 0) && e.type === 'blur') {
                                $(element).val(previousValue);

                                return;
                            }

                            break;
                    }
                }).attr('data-qty-initialized', true);

                qtyDecrease.on('click', {qtyInput: qtyInput}, qtyButtonProcessor);
                qtyIncrease.on('click', {qtyInput: qtyInput}, qtyButtonProcessor);
            }
        }

        if (qtySelect.length === 1) {
            if (!qtySelect.attr('data-qty-initialized')) {
                if (!qtySelect.attr('data-url')) {
                    throw 'Missing data url for select';
                }

                qtySelect.on('change', (e) => {
                    if (qtyIsBeingUpdated) {
                        return;
                    }

                    disableClickTimer();

                    updateQuantity(qtySelect, () => {
                        qtySelect.trigger('qty-updated');
                    })
                }).attr('data-qty-initialized', true);
            }
        }
    });
};
