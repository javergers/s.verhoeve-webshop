function GoogleAnalytics() {
}

GoogleAnalytics.productClick = function (elm, productData, assortiment) {
    if (typeof ga === 'undefined' || !ga.hasOwnProperty('loaded') || !ga.loaded) {
        document.location = elm.attr('href');

        return;
    }

    dataLayer.push({
        'event': 'productClick',
        'ecommerce': {
            'click': {
                'actionField': {'list': assortiment},      // Optional list property.
                'products': [
                    productData
                ]
            }
        },
        'eventCallback': function () {
            document.location = elm.attr('href');
        }
    });
}

GoogleAnalytics.addToCart = function (elm, productData, quantity) {
    productData.quantity = quantity;

    dataLayer.push({
        'event': 'addToCart',
        'ecommerce': {
            'add': {
                'products': [
                    productData
                ]
            }
        }
    });
}

module.exports = GoogleAnalytics;
