var flashbag = new function () {
    this.parent = '.container-messages > .container > .row';
};

flashbag.checkMessageContainer = function () {
    if ($('.container-messages', $('.page-wrapper')).length === 0) {
        $('<div class="container-messages"><div class="container"><div class="row"></div></div></div>').prependTo($('.page-wrapper'));
    }
}

flashbag.add = function (type, message) {
    var allowedTypes = ['success', 'notice', 'warning', 'danger'];

    if ((!type || !message) || allowedTypes.indexOf(type) === -1) {
        return;
    }

    this.checkMessageContainer();

    var message_html = '\
        <div>\
            <div class="alert alert-' + type + '" role="alert">\
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">\
                    <span aria-hidden="true">&times;</span>\
                </button>\
                <ul>\
                    <li>' + message + '</li>\
                </ul>\
            </div>\
        </div>';

    $(this.parent).html('');
    $(message_html).appendTo(this.parent);

    setTimeout(function () {
        $('.alert', flashbag.parent).fadeOut(function () {
            $('.alert', flashbag.parent).remove();
        });
    }, 2500);
};

flashbag.setContainer = function (element) {
    flashbag.parent = element;
};

module.exports = flashbag;
