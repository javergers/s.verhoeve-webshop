require('./../../img/logos/ideal.png');
require('./../../img/logos/afterpay.png');
require('./../../img/logos/invoice.png');
require('./../../img/logos/freeofcharge.png');
require('./../../img/logos/creditcard.png');
require('./../../img/logos/paypal.png');
require('./../../img/logos/bancontact.png');
require('./../../img/logos/visa.jpg');
require('./../../img/logos/mastercard.jpg');
require('./../../img/logos/american.jpg');
require('./../../img/logos/ideal.jpg');
require('./../../img/logos/paypal.jpg');
require('./../../img/logos/rekening.jpg');
require('./../../img/logos/afterpay.jpg');
require('./../../img/logos/levering.jpg');
require('./../../img/logos/gift-card.jpg');
require('./../../img/logos/kiyoh.png');
require('./../../img/logos/thuiswinkel-zw.jpg');
require('./../../img/logos/thuiswinkel-grijs.jpg');
require('./../../img/logos/thuiswinkel.jpg');
require('./../../img/klantenservice.jpg');

// about block
require('./../../img/fruitmand.jpg');

// video bar
require('./../../img/fruit-zakelijk.jpg');       // topfruit
require('./../../img/campagne-people.jpg');      // toptaarten
require('./../../img/medewerkers-magazijn.jpg'); // topgeschenken

// about bar
require('./../../img/bezorger-tg.jpg');          // topgeschenken

// topfruit
require('./../../img/logos/topfruit-kleur.png');
require('./../../img/theme/topfruit/topfruit-card-preview.png');
require('./../../img/theme/topfruit/topfruit-logo.svg');
require('./../../img/theme/topfruit/topfruit-favicon.ico');
require('./../../img/theme/topfruit/topfruit-van.png');
require('./../../img/theme/topfruit/topfruit-placeholder.png');
require('./../../img/theme/topfruit/topfruit-apple-touch-icon-57x57.png');
require('./../../img/theme/topfruit/topfruit-apple-touch-icon-60x60.png');
require('./../../img/theme/topfruit/topfruit-apple-touch-icon-72x72.png');
require('./../../img/theme/topfruit/topfruit-apple-touch-icon-76x76.png');
require('./../../img/theme/topfruit/topfruit-apple-touch-icon-114x114.png');
require('./../../img/theme/topfruit/topfruit-apple-touch-icon-120x120.png');
require('./../../img/theme/topfruit/topfruit-apple-touch-icon-144x144.png');
require('./../../img/theme/topfruit/topfruit-apple-touch-icon-152x152.png');
require('./../../img/theme/topfruit/topfruit-apple-touch-icon-180x180.png');
require('./../../img/theme/topfruit/topfruit-android-chrome-192x192.png');
require('./../../img/theme/topfruit/topfruit-smalltile.png');
require('./../../img/theme/topfruit/topfruit-mediumtile.png');
require('./../../img/theme/topfruit/topfruit-widetile.png');
require('./../../img/theme/topfruit/topfruit-largetile.png');

require('./../../img/categories/fruitmanden.png');
require('./../../img/categories/fruitboxen.png');
require('./../../img/categories/fruitkisten.png');
require('./../../img/categories/met-geschenk.png');
require('./../../img/categories/biologisch-fruit.png');
require('./../../img/categories/werkfruit.png');

// Topbloemen
require('./../../img/logos/topbloemen-kleur.png');

// Toptaarten
require('./../../img/logos/toptaarten-kleur.png');
require('./../../img/theme/toptaarten/toptaarten-card-preview.png');
require('./../../img/theme/toptaarten/toptaarten-logo.svg');
require('./../../img/theme/toptaarten/toptaarten-favicon.ico');
require('./../../img/theme/toptaarten/toptaarten-van.png');
require('./../../img/theme/toptaarten/toptaarten-placeholder.png');
require('./../../img/theme/toptaarten/toptaarten-apple-touch-icon-57x57.png');
require('./../../img/theme/toptaarten/toptaarten-apple-touch-icon-60x60.png');
require('./../../img/theme/toptaarten/toptaarten-apple-touch-icon-72x72.png');
require('./../../img/theme/toptaarten/toptaarten-apple-touch-icon-76x76.png');
require('./../../img/theme/toptaarten/toptaarten-apple-touch-icon-114x114.png');
require('./../../img/theme/toptaarten/toptaarten-apple-touch-icon-120x120.png');
require('./../../img/theme/toptaarten/toptaarten-apple-touch-icon-144x144.png');
require('./../../img/theme/toptaarten/toptaarten-apple-touch-icon-152x152.png');
require('./../../img/theme/toptaarten/toptaarten-apple-touch-icon-180x180.png');
require('./../../img/theme/toptaarten/toptaarten-android-chrome-192x192.png');
require('./../../img/theme/toptaarten/toptaarten-smalltile.png');
require('./../../img/theme/toptaarten/toptaarten-mediumtile.png');
require('./../../img/theme/toptaarten/toptaarten-widetile.png');
require('./../../img/theme/toptaarten/toptaarten-largetile.png');

require('./../../img/categories/slagroomtaarten.png');
require('./../../img/categories/marsepeintaarten.png');
require('./../../img/categories/luxe-taarten.png');
require('./../../img/categories/fototaarten.png');
require('./../../img/categories/themataarten.png');
require('./../../img/categories/traktaties.png');

// Topgeschenken
require('./../../img/logos/topgeschenken-kleur.png');
require('./../../img/theme/topgeschenken/topgeschenken-card-preview.png');
require('./../../img/theme/topgeschenken/topgeschenken-favicon.ico');
require('./../../img/theme/topgeschenken/topgeschenken-logo.svg');
require('./../../img/theme/topgeschenken/topgeschenken-van.png');
require('./../../img/theme/topgeschenken/topgeschenken-placeholder.png');
require('./../../img/theme/topgeschenken/topgeschenken-apple-touch-icon-57x57.png');
require('./../../img/theme/topgeschenken/topgeschenken-apple-touch-icon-60x60.png');
require('./../../img/theme/topgeschenken/topgeschenken-apple-touch-icon-72x72.png');
require('./../../img/theme/topgeschenken/topgeschenken-apple-touch-icon-76x76.png');
require('./../../img/theme/topgeschenken/topgeschenken-apple-touch-icon-114x114.png');
require('./../../img/theme/topgeschenken/topgeschenken-apple-touch-icon-120x120.png');
require('./../../img/theme/topgeschenken/topgeschenken-apple-touch-icon-144x144.png');
require('./../../img/theme/topgeschenken/topgeschenken-apple-touch-icon-152x152.png');
require('./../../img/theme/topgeschenken/topgeschenken-apple-touch-icon-180x180.png');
require('./../../img/theme/topgeschenken/topgeschenken-android-chrome-192x192.png');
require('./../../img/theme/topgeschenken/topgeschenken-smalltile.png');
require('./../../img/theme/topgeschenken/topgeschenken-mediumtile.png');
require('./../../img/theme/topgeschenken/topgeschenken-widetile.png');
require('./../../img/theme/topgeschenken/topgeschenken-largetile.png');

// Suppliers
require('./../../img/theme/suppliers/suppliers-card-preview.png');
require('./../../img/theme/suppliers/suppliers-favicon.ico');
require('./../../img/theme/suppliers/suppliers-logo.svg');
require('./../../img/theme/suppliers/suppliers-van.png');
require('./../../img/theme/suppliers/suppliers-placeholder.png');
require('./../../img/theme/suppliers/suppliers-apple-touch-icon-57x57.png');
require('./../../img/theme/suppliers/suppliers-apple-touch-icon-60x60.png');
require('./../../img/theme/suppliers/suppliers-apple-touch-icon-72x72.png');
require('./../../img/theme/suppliers/suppliers-apple-touch-icon-76x76.png');
require('./../../img/theme/suppliers/suppliers-apple-touch-icon-114x114.png');
require('./../../img/theme/suppliers/suppliers-apple-touch-icon-120x120.png');
require('./../../img/theme/suppliers/suppliers-apple-touch-icon-144x144.png');
require('./../../img/theme/suppliers/suppliers-apple-touch-icon-152x152.png');
require('./../../img/theme/suppliers/suppliers-apple-touch-icon-180x180.png');
require('./../../img/theme/suppliers/suppliers-android-chrome-192x192.png');
require('./../../img/theme/suppliers/suppliers-smalltile.png');
require('./../../img/theme/suppliers/suppliers-mediumtile.png');
require('./../../img/theme/suppliers/suppliers-widetile.png');
require('./../../img/theme/suppliers/suppliers-largetile.png');

require('./../../img/categories/jodeco-glass.png');
require('./../../img/categories/chrysal.png');
require('./../../img/categories/rouwlinten-printer.png');
require('./../../img/categories/oasis.png');
require('./../../img/categories/uitvaart-artikelen.png');
require('./../../img/categories/promotie-artikelen.png');
require('./../../img/categories/cadeauartikelen.png');
require('./../../img/categories/topbloemen-artikelen.png');
require('./../../img/categories/bloemisterij-artikelen.png');