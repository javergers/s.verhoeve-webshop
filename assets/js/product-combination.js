$(function () {
    $('.col-product-item').on('click', function () {
        var self = $(this);
        var combinationId = $(this).attr('data-combination-id');
        var productName = $(this).find('.product-item-description').html();

        swal.setDefaults({
            input: 'text',
            type: 'question',
            progressSteps: ['1', '2']
        });

        var steps = [
            {
                title: trans('cart.order.minimum_unit'),
                text: trans('cart.order.question_for_unit').replace('%unit%', trans('cart.order.minimum_unit').toLower()).replace('%product%', productName)
            },
            {
                title: trans('cart.order.maximum_unit'),
                text: trans('cart.order.question_for_unit').replace('%unit%', trans('cart.order.maximum_unit').toLower()).replace('%product%', productName)
            }
        ];

        swal.queue(steps).then((result) => {
            var minQuantity = result[0];
            var maxQuantity = result[1];

            $.ajax({
                url: $('.combination-product-holder').attr('data-combination-quantity-url'),
                method: 'POST',
                data: {
                    'combination': combinationId,
                    'minQuantity': minQuantity,
                    'maxQuantity': maxQuantity
                }
            }).then(() => {
                swal.resetDefaults();
                swal(trans('modal.success'), trans('cart.order.unit_changed'), 'success');
                self.attr('data-min-quantity', minQuantity);
                self.attr('data-max-quantity', maxQuantity)
            });
        });
    });

    var configSelect = $('.configuration-select');
    var selectedConfiguration = null;
    var combinationQuantityInput = $('.combination-quantity');

    if(configSelect.length) {
        $('#form_submit').attr('disabled', 'disabled');
    }

    configSelect.on('change', function () {
        //filter select fields on previous selected values
        var selectedProducts = [];
        var self = $(this);
        configSelect.each(function () {
            if (self.attr('data-main') && !self.is($(this))) {
                $(this)[0].selectedIndex = 0;
            }

            if ($(this).val()) {
                selectedProducts.push($(this).val());
            }
        });

        var currentSelect = $(this);
        //hide all options except main options and current select options
        $('[data-combination-option]').each(function () {
            if (!$(this).parent().is(currentSelect) && !$(this).parent().attr('data-main')) {
                $(this).hide();
            }
        });

        $.each(configurations, function (key, configuration) {
            var productsInConfiguration = true;
            $.each(selectedProducts, function (i) {
                productsInConfiguration = configuration.indexOf(parseInt(selectedProducts[i])) < 0;
            });

            //show available configuration options
            if (productsInConfiguration) {
                $.each(configuration, function (i) {
                    $('[data-combination-option="' + configuration[i] + '"]').show();
                });

                if ($('option[data-combination-option]:selected').length === $('.configuration-select').length) {
                    $('#form_variation').val(key);
                    $('#form_submit').removeAttr('disabled');
                    selectedConfiguration = configuration;

                    //retrieve product information from matched configuration
                    $.ajax({
                        url: $('#content-product').attr('data-product-information-url'),
                        method: 'GET',
                        data: {
                            product: key
                        }
                    }).then((data) => {
                        $('#product_price').html(data.product.displayPrice);
                    });

                    return false;
                }
            }
        });
    });

    combinationQuantityInput.on('change', function () {
        var metadataInput = $('#form_metadata');
        var metadata = [];
        var totalPrice = 0;

        $('.combination-quantity').each(function () {
            var productId = $(this).closest('.line-double').attr('data-product-id');
            var quantity = $(this).val();
            var price = $(this).closest('.line-double').attr('data-price');

            metadata.push({
                productId: productId,
                quantity: quantity,
                price: price
            });

            totalPrice += (parseFloat(price.replace(',', '.')) * parseInt(quantity));

        });

        $('#product_price').html(parseFloat(totalPrice).toFixed(2).replace('.', ','));
        metadataInput.val(JSON.stringify(metadata));
    });

    combinationQuantityInput.trigger('change');
    configSelect.trigger('change');
});