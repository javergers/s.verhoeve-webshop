import DeliveryAddress from './forms/DeliveryAddress';

const flatpickr = require('flatpickr');
const dutch = require('flatpickr/dist/l10n/nl').default.nl;

require('./../scss/checkout-delivery.scss');

const AddressBook = require('./address-book');

const Cart = require('./includes/cart');
const CustomerAddress = require('./includes/customer-address');

import PickupLocation from './checkout-pickup-location';
global.PickupLocation = PickupLocation;
PickupLocation.init();

let checkoutDelivery = new function () {
    new AddressBook();
};
let addressFixed;
let addressSuggestionXhrActive;
let deliveryDateXhr;
let deliveryDateTimeout;

checkoutDelivery.init = function () {
    this.bindEvents();

    this.deliveryAddressForm = new DeliveryAddress('.delivery-details-container');

    this.getDeliveryMethods();
};

checkoutDelivery.updateOtherbox = function(dateStr) {
    const tabContent = $('.tab-content', deliveryDateContainerElm);
    const deliveryDateContainerElm = $('.delivery-date-container');
    const deliveryDateOtherElm = $('li.delivery-date-other', deliveryDateContainerElm);
    const selectedDateInput = $(`input[name=deliveryDateReadable][value="${dateStr}"]`);
    const selectedDateOtherInput = $('input[name="deliveryDateReadable"][value="other"]');
    const otherDatePrice = $('span.price', '.delivery-date-other');

    $('input[name=deliveryDateReadable]').removeAttr('checked').removeProp('checked');

    if (selectedDateInput.length === 0) {
        const deliveryDateInfo = deliveryDatesJson[dateStr];

        if (deliveryDatesJson && deliveryDateInfo && deliveryDateInfo.price) {
            let {price} = deliveryDateInfo;

            if(price > 0) {
                price = "" + deliveryDateInfo.price.toFixed(2);
                price = price.replace(".", ",");
            } else {
                price = "0,00";
            }

            otherDatePrice.html(price);
        }

        selectedDateOtherInput.attr('checked', true).trigger('change');
    } else {
        selectedDateInput.attr('checked', true).trigger('change');
        otherDatePrice.empty();
    }

    if(dateStr) {
        deliveryDateOtherElm.attr('data-choosen-date', dateStr);
    }

    // Hide datepicker.
    tabContent.removeClass('show');
};

checkoutDelivery.updateTabContent = function() {
    const deliveryDateContainerElm = $('.delivery-date-container');
    const tabContent = $('.tab-content', deliveryDateContainerElm);
    const dateStr = this.deliveryDateElement.input.value;
    const deliveryDateStr = this.deliveryDateElement.altInput.value;
    const deliveryDateArray = deliveryDateStr.split(' ');
    const deliveryDateInfo = deliveryDatesJson[dateStr];

    let shortDescription = '';
    if(deliveryDateInfo.hasOwnProperty('transport_type')) {
        shortDescription = deliveryDateInfo.transport_type.short_description;
    }

    if(deliveryDateInfo) {
        tabContent.find('.chosen-date').text(deliveryDateArray.join(' '));
        tabContent.find('.delivery-product-description').html(shortDescription);
    }
};

checkoutDelivery.bindEvents = function () {
    Cart.init();
    CustomerAddress.init();

    const self = this;
    this.postcode = $('.postcode input', '.address-container');
    this.number = $('.number input', '.address-container');
    this.numberAddition = $('.numberAddition :input', '.address-container');
    this.country = $('.country select', '.address-container');
    this.deliveryDateElements = $('.postcode input, .number input, .numberAddition :input, .country select', '.address-container');

    this.deliveryDateContainer = $('.delivery-date-container');
    this.deliveryDateWrapper = $('.delivery-date-container-wrapper');
    this.deliveryDatesContainer = $('.delivery-dates', this.deliveryDateContainer);
    this.deliveryMessage = $('.bezorg-bericht');
    this.deliveryDateRow = $('.delivery-date-row');
    this.columnPersonalization = $('.column-personalization');
    this.columnAdditionalProducts = $('.column-additional-products');

    const deliveryDateElements = this.deliveryDateElements;

    this.deliveryDateElement = null;

    this.columnPersonalization
    .on('click', '[data-action="card-edit"], [data-action="card-add"]', function (e) {
        e.preventDefault();

        const href = $(this).attr('data-url');

        Cart.showEditCardModal(href, function () {
            checkoutDelivery.refreshPersonalizationProducts();
            self.getDeliveryMethods();

            swal({
                title: trans('cart.order_edit_card.title_success'),
                text: trans('cart.order_edit_card.text.html'),
                timer: 2000
            });
        });
    })
    .on('click', '[data-action="designer-edit"]', function (e) {
        e.preventDefault();

        const href = $(this).attr('data-url');

        Cart.showEditDesignerModal(href, function(){
            self.getDeliveryMethods();
        });
    })
    .on('click', '[data-action="letter-edit"]', function (e) {
        e.preventDefault();

        const href = $(this).attr('data-url');

        Cart.showEditLetterModal(href, function(){
            self.getDeliveryMethods();
        });
    })
    .on('click', '[data-action="remove"]', function (e) {
        e.preventDefault();

        const url = $(this).attr('data-url');

        Cart.showRemovePersonalizationProductModal(url).then(function () {
            checkoutDelivery.refreshPersonalizationProducts();
            self.getDeliveryMethods();
        }, function () {
        });
    })
    ;

    this.columnAdditionalProducts
    .on('click', '[data-action="remove"]', function (e) {
        e.preventDefault();

        const url = $(this).attr('data-url');

        checkoutDelivery.showRemoveAdditionalProductModal(url).then(function () {
            checkoutDelivery.refreshAdditionalProducts();
            self.getDeliveryMethods();
        }, function () {
        });
    })
    .on('click', '#additional_product_show_all', function (e) {
        e.preventDefault();

        const href = $('.column-additional-products .product-lines').closest('.column-additional-products').attr('data-additional-products-url');

        swal({
            title: trans('checkout.delivery.modal_additional_products.title')
        });

        swal.showLoading();

        $.get(href).then(function (html) {
            checkoutDelivery.showAdditionalProductsModal(html);
        });
    });

    $('#cart_order_deliveryAddressType').on('change', function () {
        const option = $(this).find('option:selected');
        const field_container = $('#' + $(option).attr('data-container'));

        let company_label = ((typeof $(option).attr('data-company-label') !== 'undefined') ? $(option).attr('data-company-label') : null);
        let recipient_label = ((typeof $(option).attr('data-recipient-label') !== 'undefined') ? $(option).attr('data-recipient-label') : null);

        const companyContainer = $('.delivery-details-container .company');

        if (company_label) {
            const companyName = $('#cart_order_deliveryAddressCompanyName');
            const required = companyName.attr('required');

            if (required) {
                company_label += '*';
            }

            companyName
            .next('label')
            .attr('alt', company_label)
            .attr('placeholder', company_label);
        }

        if (recipient_label) {
            const attn = $('#cart_order_deliveryAddressAttn');
            const required = attn.attr('required');

            if (required) {
                recipient_label += '*';
            }

            attn
            .next('label')
            .attr('alt', recipient_label)
            .attr('placeholder', recipient_label);
        }

        $('.delivery-address-type-fields').hide();

        let showInputElement = $('.delivery-details-container .show-input');

        if (!$(this).val()) {
            showInputElement.hide();
        } else {
            showInputElement.show();
        }

        showInputElement.toggleClass('show-company', !$(option).attr('data-hide-company'));
        companyContainer.toggle(!$(option).attr('data-hide-company'));

        if ($(option).attr('data-hide-company')) {
            $('.delivery-details-container .attn').toggleClass('col-md-12', true).toggleClass('col-md-6', false);

            $('input', companyContainer).val('');
        } else {
            $('.delivery-details-container .attn').toggleClass('col-md-12', false).toggleClass('col-md-6', true);
        }

        if ($(field_container).length === 1) {
            $(field_container).show();
        }
    }).trigger('change');

    this.deliveryDateElements.on('change', function () {
        self.getDeliveryDate();
    });

    this.deliveryDateContainer.on('change', 'input[name=deliveryDateReadable]', function (e) {
        const elm = $(e.currentTarget);
        const li = elm.closest('li');
        const value = $('input[name=deliveryDateReadable]:checked', '.delivery-dates').val();
        const tabContentElm = $('.datepicker', this.deliveryDateContainer).closest('.tab-content');
        const chosenDate = li.attr('data-choosen-date');
        const hasChosenDate = !!chosenDate;
        const hasValue = !!value;
        const showDatepicker = li.hasClass('delivery-date-other') || (!hasChosenDate && !hasValue);

        if(value === 'other' && !!chosenDate) {
            self.deliveryDateElement.setDate(chosenDate, true);
        } else if (!e.isTrigger && value !== 'other') {
            self.deliveryDateElement.setDate(value, true);
        }

        $('li', self.deliveryDateContainer).removeClass('active');
        li.addClass('active');

        tabContentElm.toggleClass('show', showDatepicker);
    }.bind(this));

    this.deliveryDateContainer.on('click', '.btn-change-date', function(e) {
        const elm = $(e.currentTarget);
        const li = elm.closest('li');
        const tabContentElm = $('.datepicker', this.deliveryDateContainer).closest('.tab-content');
        const chosenDate = li.attr('data-choosen-date');

        if(!!chosenDate) {
            self.deliveryDateElement.setDate(chosenDate, true);
        }

        // Used for mobile purposes
        tabContentElm.toggleClass('show', true);
    }.bind(this));

    if (this.postcode.val() && this.number.val()) {
        this.postcode.trigger('change');
    }

    $('#cart_order_deliveryRemark').on('keyup', function (e) {
        if ($(this).get(0).scrollHeight > 36 && e.keyCode === 13) {
            $(this).height($(this).get(0).scrollHeight - 10);
        }
    });

    $('#cart_order_deliveryAddressStreet, #cart_order_deliveryAddressCity').on('change', function (e) {
        if (!e.isTrigger) {
            getAddressSuggestions();
        }
    });

    $('.address-container').on('getDeliveryDate', function (e) {
        self.getDeliveryDate();
    });

    function getAddressSuggestions() {
        let addressContainer = $('.address-container');
        let deliveryAddressPostcode = $('#cart_order_deliveryAddressPostcode').val();
        let deliveryAddressHouseNumber = $('#cart_order_deliveryAddressNumber').val();
        let deliveryAddressStreet = $('#cart_order_deliveryAddressStreet').val();
        let deliveryAddressCity = $('#cart_order_deliveryAddressCity').val();
        let addressSearching = parseInt(addressContainer.attr('data-address-searching')) === 1;
        let addressFoundAttr = addressContainer.attr('data-address-found');
        let addressFound = typeof addressFoundAttr === 'string' && parseInt(addressFoundAttr) === 1 &&
            deliveryAddressPostcode === addressContainer.attr('data-address-searched-postcode') &&
            deliveryAddressHouseNumber === addressContainer.attr('data-address-searched-houseNumber') &&
            deliveryAddressStreet === addressContainer.attr('data-address-searched-street') &&
            deliveryAddressCity === addressContainer.attr('data-address-searched-city')
        ;

        if (addressFoundAttr === 1 && addressFoundAttr !== addressFound) {
            addressContainer.attr('data-address-found', 0);
        }

        if (!addressFound && !addressSearching && deliveryAddressStreet && deliveryAddressCity && !addressSuggestionXhrActive) {
            addressSuggestionXhrActive = true;

            $.ajax({
                url: $('.address-fix').attr('data-address-fix-url'),
                method: 'POST',
                data: {
                    street: deliveryAddressStreet,
                    city: deliveryAddressCity,
                    postcode: $('#cart_order_deliveryAddressPostcode').val(),
                    housenumber: $('#cart_order_deliveryAddressNumber').val()
                }
            }).then(function (html) {
                addressFixed = null;

                if (html) {
                    swal({
                        title: trans('checkout.delivery.modal_address_suggestions.title'),
                        html: html,
                        showConfirmButton: false,
                        showCancelButton: false,
                        customClass: 'address-suggestions',
                        useRejections: true // swal7fix
                    }).then(function () {
                        addressFixed = false;
                    }, function () {
                        addressFixed = false;
                    });

                    $('tr', '.address-suggestions').on('click', function () {
                        const line = $(this);
                        const address = JSON.parse(line.attr('data-address'));

                        $('#cart_order_deliveryAddressStreet').val(address.street);
                        $('#cart_order_deliveryAddressCity').val(address.city);
                        $('#cart_order_deliveryAddressPostcode').val(address.postcode);
                        $('#cart_order_deliveryAddressNumber').val(address.housenumber);

                        addressFixed = true;

                        swal.close();
                    });

                    $('a', '.address-suggestions').on('click', function (e) {
                        e.preventDefault();

                        swal.close();
                        addressContainer.attr('data-address-found', 1);
                    });
                }
            }).always(function () {
                addressSuggestionXhrActive = false;
            });
        }
    }

    $('#checkout_continue_with_address').on('click', function (e) {
        e.preventDefault();

        $('form[name="cart_order"]')
        .append('<input type="hidden" name="continue_with_address" data-clear-when-invalid="1" value="1" />')
        .submit();
    });

    $('.qty-selector input').on('qty-updated',function () {
        const quantity = $(this).val();
        if(typeof $(this).attr('data-upsell') === 'undefined') {
            $('.qty-selector input[data-upsell]').each(function() {
                if($(this).attr('data-quantity-mode') == '' || $(this).attr('data-quantity-mode') == 'strictly_following') {
                    const url = $(this).attr('data-url')

                    $.ajax({
                        url: url,
                        method: 'POST',
                        data: {
                            quantity:  parseInt(quantity)
                        }
                    }).then(function() {
                        $(this).val(quantity);
                    });
                }
            });
        }

        if (typeof this.deliveryDateElements !== 'undefined') {
            $(this.deliveryDateElements).trigger('change');
        }
    });
};

checkoutDelivery.getDeliveryDate = function () {

    const deliveryDateXhrUrl = this.deliveryDateContainer.attr('data-delivery-date-url');
    const tabContentElm = $('.tab-content', this.deliveryDateContainer);

    let self = this;

    if(this.country.val() === '' || this.postcode.val() === '' || this.number.val() === ''){
        return false;
    }

    if (deliveryDateXhrUrl) {

        if(deliveryDateXhr) {
            deliveryDateXhr.abort();
        }

        if(deliveryDateTimeout) {
            clearTimeout(deliveryDateTimeout);
            deliveryDateTimeout = null;
        }

        deliveryDateTimeout = setTimeout(() => {
            clearTimeout(deliveryDateTimeout);

            this.deliveryDateWrapper.addClass('loading');

            $('.delivery-dates').remove();
            this.deliveryMessage.hide();

            deliveryDateXhr = $.ajax({
                url: deliveryDateXhrUrl,
                method: 'GET',
                dataType: 'html',
                data: {
                    country: this.country.val(),
                    postcode: this.postcode.val(),
                    number: this.number.val(),
                    numberAddition: this.numberAddition.val(),
                    deliveryDate: $('#cart_order_deliveryDate').val()
                }
            }).done(function (response) {
                self.deliveryMessage.hide();

                const html = $(response);

                html.find('.tab-content').addClass('show');

                html.insertAfter(self.deliveryMessage).show();

                self.deliveryDateRow.appendTo('.delivery-date-container-wrapper .tab-content .datepicker');

                let availableDates = Object.keys(deliveryDatesJson);

                if (!availableDates || !$.isArray(availableDates)) {
                    return false;
                }

                self.deliveryDateElement = $('#cart_order_deliveryDate').flatpickr({
                    locale: dutch,
                    inline: true,
                    animate: false,
                    dateFormat: 'Y-m-d',
                    altInput: true,
                    altFormat: 'l d F Y',
                    altInputClass: 'form-control',
                    onChange: function (dateObj, dateStr) {
                        self.updateOtherbox(dateStr);
                        self.updateTabContent(dateStr);
                    },
                    disableMobile: true
                });

                self.deliveryDateElement.set('enable', availableDates);

                $('.form-row.phone').show();
                $('.form-row.comment').show();
            }).fail(function () {
                self.deliveryMessage.show();
                self.deliveryDatesContainer.remove();

                $('.form-row.phone').hide();
                $('.form-row.comment').hide();
            }).always(function () {
                self.deliveryDateWrapper.removeClass('loading');
            });

        }, 250);
    }
};

checkoutDelivery.refreshPersonalizationProducts = function () {
    const personalizationLinesContainer = $('.column-personalization .product-lines');
    const href = personalizationLinesContainer.closest('.column-personalization').attr('data-personalization-url');

    personalizationLinesContainer.css('height', personalizationLinesContainer.height()).empty().addClass('loading');

    $.get(href).then(function (html) {
        var productLines = $(html).find('.product-lines');

        personalizationLinesContainer.css('height', 'auto').removeClass('loading').replaceWith(productLines);

        Cart.applyQuantityPlugin($('.column-personalization'));
    });
};

checkoutDelivery.refreshAdditionalProducts = function () {
    let additionalProductLinesContainer = $('.column-additional-products .product-lines');
    const href = $('.column-additional-products').attr('data-additional-products-url');

    additionalProductLinesContainer.css('height', additionalProductLinesContainer.height()).empty().addClass('loading');

    $.get(href, {
        max: 3
    }).then(function (html) {
        additionalProductLinesContainer.css('height', 'auto').removeClass('loading').replaceWith(html);

        Cart.applyQuantityPlugin($('.column-additional-products'));

        const additionalProductCount = additionalProductLinesContainer.find('product-line[data-id]').length;

        if (parseInt(additionalProductCount) === 0) {
            $('.row-additional-products').show();
            $('.buttons:last').show();
        }
    });
};

checkoutDelivery.showAdditionalProductsModal = function (html) {
    swal({
        title: trans('checkout.delivery.modal_additional_products.title'),
        html: html,
        showConfirmButton: false,
        showCancelButton: true,
        cancelButtonText: trans('modal.close'),
        customClass: 'checkout-delivery-modal-additional-products',
    });

    $('.qty-selector', '.checkout-delivery-modal-additional-products').qty();

    $('[data-action="remove"]', '.checkout-delivery-modal-additional-products').on('click', function (e) {
        e.preventDefault();

        const lineIdentifier = $(this).closest('.product-line').attr('data-id');

        const url = $(this).attr('data-url');

        checkoutDelivery.showRemoveAdditionalProductModal(url).then(function () {
            const line = $('.product-line[data-id="' + lineIdentifier + '"]', '.checkout-delivery-modal-additional-products');
            const href = $('.column-additional-products .product-lines').closest('.column-additional-products').attr('data-additional-products-url');

            line.addClass('marked-for-removal');

            swal({
                title: trans('checkout.delivery.modal_additional_products.title')
            });

            swal.showLoading();

            $.get(href).then(function (html) {
                checkoutDelivery.showAdditionalProductsModal(html);

                checkoutDelivery.refreshAdditionalProducts();
            });
        }, function () {
            checkoutDelivery.showAdditionalProductsModal(html);
        });
    });
};

checkoutDelivery.showRemoveAdditionalProductModal = function (url) {
    return swal({
        type: 'question',
        text: trans('checkout.delivery.modal_removal_confirm.text'),
        showCancelButton: true,
        confirmButtonText: trans('modal.confirm_removal'),
        cancelButtonText: trans('modal.cancel'),
        showLoaderOnConfirm: true,
        useRejections: true, // swal7fix
        preConfirm: function () {
            return new Promise((resolve, reject) => {
                Cart.removeOrderLine(url).done(function () {
                    resolve();
                }).fail(function () {
                    // reject(trans('modal.error'));
                    swal.showValidationError(trans('modal.error')); // swal7fix
                });
            });
        }
    });
};

checkoutDelivery.getDeliveryMethods = function () {
    let deliveryMethodContainer = $('.delivery-methods');
    let deliveryMethodList = $('.delivery-method-list', deliveryMethodContainer);
    let deliveryDateWrapper = $('.delivery-date-container-wrapper');
    let deliveryAddressElement = $('.delivery-details-container .adres');
    let pickupLocationelement = $('.delivery-details-container .pickup');

    if (deliveryMethodContainer.data('delivery-method-url')) {
        if (!deliveryMethodContainer.hasClass('loading')) {
            deliveryMethodContainer.addClass('loading');
            deliveryMethodList.empty();

            const country = $('.country select', '.address-container').val();
            let checkedValue = $('input:checked', '.delivery-methods').val();

            if (typeof checkedValue === 'undefined' && deliveryMethodList.attr('data-method')) {
                checkedValue = deliveryMethodList.attr('data-method');
            }

            $.ajax({
                url: deliveryMethodContainer.data('delivery-method-url'),
                method: 'GET',
                dataType: 'html',
                data: {
                    deliveryAddressCountry: country,
                    deliveryMethod: checkedValue
                }
            }).done(function (response) {
                deliveryMethodList.html(response);

                $('input:radio', deliveryMethodList).on('change', function (e) {
                    if (e.isTrigger && $('input:radio:checked', deliveryMethodList).length === 0) {
                        return false;
                    }
                    const nrOfDeliveryOptions = $('.delivery-method-list').find('li').length;
                    $('.delivery-methods').toggle(nrOfDeliveryOptions > 1);

                    const type = $('input:radio:checked', deliveryMethodList).val();
                    $('#delivery_method_none_description, .delivery-pickup-method-description').hide();

                    switch (type) {
                        case 'delivery':
                            $('#delivery_method_delivery_description').show();

                            deliveryDateWrapper.show();
                            deliveryAddressElement.show();
                            pickupLocationelement.hide();
                            $('.delivery-dates').show();

                            break;
                        case 'pickup':
                            $('#delivery_method_pickup_description').show();
                            $('.delivery-dates').hide();

                            deliveryDateWrapper.hide();
                            deliveryAddressElement.hide();
                            pickupLocationelement.show();

                            break;
                    }
                }).trigger('change');
            }).always(function () {
                deliveryMethodContainer.removeClass('loading');
            });
        }
    }
};

$(function() {
    checkoutDelivery.init();
});
