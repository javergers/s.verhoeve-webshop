require('jquery.fancytable');

module.exports = class AddressBook{
    constructor(){
        const self = this;
        self.bindEvents();
        $(document).ready(function(){
            if(typeof sessionAddress !== 'undefined'){
                self.saveSessionAddress(sessionAddress);
            }
            if(typeof deliveryAddress !== 'undefined'){
                AddressBook.setDeliveryAddress(deliveryAddress);
            }
        });
    }

    bindEvents(){
        const self = this;
        $('.container').on('click', '[data-action="addressBook"]', function (e) {
            e.preventDefault();

            const href = $(this).attr('data-url');

            swal({
                title: trans('cart.order_address.title')
            });

            swal.showLoading();

            $.get(href).then(function (html) {
                swal({
                    title: trans('cart.order_address.title'),
                    html: html,
                    showConfirmButton: false,
                    showCancelButton: true,
                    cancelButtonText: trans('modal.cancel'),
                    customClass: 'cart-modal-edit-order addressbook-suggestions',
                });

                let fancyTableOptions = {
                    sortColumn:0,
                    inputPlaceholder: trans('cart.order_address.search'),
                    paginationClass: 'btn btn-theme-secondary',
                    paginationClassActive: 'theme-background-color',
                    sortable: true,
                    pagination: true,
                    searchable: true,
                    globalSearch: true,
                    perPage: 5,
                };

                let addressBookTable = $("#address-book-table");
                addressBookTable.fancyTable(fancyTableOptions);
                addressBookTable.find('input').addClass('form-control');

                self.modalAddressBindEvents();
            });
        });
    }

    /**
     * @param addressString
     */
    static setDeliveryAddress(addressString){
        const address = JSON.parse(addressString);

        $('#cart_order_deliveryAddressType').val(address.type).trigger('change');

        $('#cart_order_deliveryAddressCountry').val(address.country);
        $('#cart_order_deliveryAddressCompanyName').val(address.companyName);
        $('#cart_order_deliveryAddressAttn').val(address.attn);
        $('#cart_order_deliveryAddressPostcode').val(address.postcode);
        $('#cart_order_deliveryAddressNumberAddition').val(address.numberAddition);
        $('#cart_order_deliveryAddressStreet').val(address.street);
        $('#cart_order_deliveryAddressCity').val(address.city);
        $('#cart_order_deliveryAddressPhoneNumber').val(address.phone);
        $('#cart_order_deliveryAddressNumber').val(address.number).trigger('change');

        if(typeof address.meta === 'object') {
            for (const key in address.meta) {
                const meta = address.meta[key];
                $('#cart_order_deliveryAddressMeta_' + meta.id).val(meta.value);
            }
        }
    }

    /**
     * @param href
     */
    saveSessionAddress(href) {
        swal({
            title: trans('cart.order_address_default.title')
        });

        swal.showLoading();

        $.get(href).then(function (html) {
            swal({
                title: trans('cart.order_address_default.title'),
                html: html,
                showConfirmButton: false,
                showCancelButton: true,
                cancelButtonText: trans('modal.cancel'),
                customClass: 'cart-modal-edit-order',
            });

            $('.address-list').on('click', '[data-address]', function (e) {
                e.preventDefault();

                let address = $(this).attr('data-address');
                address = JSON.parse(address);

                $.post(href, address).then(function (data) {
                    if (data === 'true') {
                        swal({
                            title: trans('cart.order_address.addres_saved'),
                            html: trans('cart.order_address.forward_home'),
                            showConfirmButton: true,
                        }).then(function (confirm) {
                            if (confirm === true) {
                                window.location.href = '/';
                            }
                        });
                    }
                });

                $('.swal2-cancel').click();
            });
        });
    }

    modalAddressBindEvents() {
        $('.address-list').on('click', '[data-address]', function (e) {
            e.preventDefault();

            const address = $(this).attr('data-address');
            AddressBook.setDeliveryAddress(address);

            $('.swal2-cancel').click();
        });
    };
};
