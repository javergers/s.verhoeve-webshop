// require jQuery normally
const $ = require('jquery');
const swal = require('sweetalert2');
const GoogleAnalytics = require('./includes/google-analytics');

// create global $ and jQuery variables
global.$ = $;
global.swal = swal;
global.GoogleAnalytics = GoogleAnalytics;

// LazyLoad
global.LazyLoad = require('vanilla-lazyload/dist/lazyload');

/* Bootstrap */
global.Util = require('exports-loader?Util!bootstrap/js/dist/util');
global.Tooltip = require('exports-loader?Tooltip!bootstrap/js/dist/tooltip');
global.Popover = require('exports-loader?Popover!bootstrap/js/dist/popover');