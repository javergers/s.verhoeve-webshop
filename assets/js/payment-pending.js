require('./../scss/_components/_cart.scss');
require('./../scss/payment-pending.scss');

$(function () {
    var i = 12;

    function check() {
        if (i == 0) {
            console.log('betaling onduidelijk');
            return;
        }

        i--;

        $.post(document.location.href)
        .done(function (res) {
            if (res.redirect) {
                document.location.href = res.redirect;
            } else {
                setTimeout(function () {
                    check();
                }, 5000);
            }
        })
        .fail(function () {
            setTimeout(function () {
                check();
            }, 5000);
        });
    };

    check();
});
