import 'slick-carousel';

require('./../scss/product.scss');
require('./includes/product-variables');

let forceSubmit = false;
let displayPrice = null;
let doAnimate = true;
let isHome = true;

let product = new function () {};

const combinationProductsContainer = $('#combination_products_product_container');

product.init = function () {
    product.bindEvents();

    product.hashRedirection();
    product.togglePersonalizationRequired();
    product.togglePersonalizationPriceDisplay();
    product.toggleSubmitBtn();

    product.determineImageHeight();
    product.changeCombinationDetails(combinationProductsContainer.attr('data-init-id'));
};

product.hashRedirection = () => {
    let variationHash = parseInt(location.hash.slice(1));

    if (isNaN(variationHash)) {
        return;
    }

    let productVariations = $('#product_variation option').map(function () {
        let val = $(this).val();
        if (val !== '') {
            return parseInt(val);
        }
    }).get();

    let hasVariationHash = productVariations.indexOf(variationHash) >= 0 ;

    if (hasVariationHash) {
        history.replaceState(null, null, location.origin + location.pathname);

        const selector = $(`[data-variation-id='${variationHash}'] .btn-product-selection`);
        if (selector.length) {
            // Three column start
            product.selectProductVariation(selector, false);
        } else {
            // Normal product
            $(`#product_variation option[value="${variationHash}"]`).prop('selected', true);
        }
    }
};

product.determineImageHeight = function () {
    const fotoGroot = $('.foto-groot');
    fotoGroot.find('img').attr('height', fotoGroot.find('img').width());
};

product.changeCombinationDetails = function (productId) {
    if (combinationProductsContainer.length > 0 && productId) {
        $.ajax({
            url: combinationProductsContainer.attr('data-combination-info-path'),
            method: 'POST',
            data: {
                product: productId
            }
        }).then((result) => {
            combinationProductsContainer.html(result);
        });
    }

    return false;
};

product.checkPersonalizationRequired = function () {
    const productField = $('#product_form_product');
    const variationField = $('#product_variation');
    const selectedOption = variationField.find('option:selected');

    // Check on selected variation
    if (variationField.length > 0 && selectedOption.length > 0) {
        return selectedOption.attr('data-personalization-required') === 'true';
    }

    if (
        variationField.length === 0
        && (typeof productField.attr('data-personalization-required') !== 'undefined')
    ) {
        return !!productField.attr('data-personalization-required');
    }

    return false;
};

product.togglePersonalizationRequired = function () {
    const isRequired = product.checkPersonalizationRequired();
    const addPersonalizationField = $('#add_personalization');
    const personalizationField = $('#product_form_personalization_product_id');
    const personalizationOption = $('#personalization_option');

    personalizationOption.removeClass('hidden');

    if (parseInt(personalizationField.val()) > 0) {
        if (isRequired) {
            addPersonalizationField.prop('checked', isRequired).prop('required', isRequired);
        } else {
            addPersonalizationField.removeAttr('required');
        }
    } else {
        // If there is no personalization product hide option.
        addPersonalizationField.prop('checked', false).removeAttr('required');
        personalizationOption.addClass('hidden');
    }
};

product.toggleSubmitBtn = function () {
    const addPersonalizationField = $('#add_personalization');
    const submitBtn = $('button#product_form_submit');
    const personalizationElm = $('#product_form_personalization_product_id');

    if (addPersonalizationField.prop('checked') && parseInt(personalizationElm.val()) > 0) {
        submitBtn.text(submitBtn.data('label-create-design'));
    } else {
        submitBtn.text(submitBtn.data('label-default'));
    }
};

product.toggleVariationPersonalizationPrice = function (selectedOption) {
    const personalizationElm = $('#product_form_personalization_product_id');
    const personalizationId = selectedOption.attr('data-personalization-product-id');
    const personalizationPrice = selectedOption.attr('data-personalization-product-price');
    const personalizationPriceElm = $('#personalization_price');

    if (personalizationElm.length === 1) {
        personalizationElm.val(personalizationId);

        if (personalizationPrice) {
            personalizationPriceElm.html(personalizationPrice);
        } else {
            personalizationPriceElm.html('');
        }
    }
};

product.togglePersonalizationPriceDisplay = function () {
    const addPersonalizationField = $('#add_personalization');
    const personalizationPriceField = $('#personalization_price');
    const personalizationRequired = product.checkPersonalizationRequired();
    const personalizationOption = $('#personalization_option');

    personalizationOption.show();
    if (personalizationRequired === true) {
        personalizationOption.hide();
    }

    if (addPersonalizationField.is(':checked')) {
        personalizationPriceField.css({
            'font-weight': 'bold'
        });
    } else {
        personalizationPriceField.css({
            'font-weight': 'normal'
        });
    }
};

product.toggleSku = function (variationId) {
    const productCarousel = $('.product-carousel');

    let productCarouselItem = null;
    $('.product-sku .variation-sku').hide();
    if (variationId) {
        productCarouselItem = productCarousel.find('.carousel-item[data-variation-id=' + variationId + ']');
        $('#variation-sku-' + variationId).show();
    }

    if (productCarouselItem !== null && productCarouselItem.length > 0) {
        productCarouselItem.find('form').focus();
        productCarouselItem.find('button').focus();

        const label = productCarouselItem.find('.line-checkbox label').first();

        $('#use_combination + label').text(label.data('title'));
        $('#use_combination_price').text(label.data('price'));
    }
};

product.toggleStockIndicator = function (variationId) {
    $('.stock-variation').removeClass('visible');
    if (variationId) {
        $('#stock-variation-' + variationId).addClass('visible');
    }
};

product.toggleVariationPrice = function (selectedOption) {
    let price = selectedOption.attr('data-price');
    const productPrice = $('.product-price');

    productPrice.closest('.price').find('> p:not(.amount)').hide();

    // Save min price
    if (!displayPrice) {
        displayPrice = productPrice.html();
    }

    if (typeof price === 'undefined') {
        price = displayPrice;
        productPrice.closest('.price').find('> p.from-price').show();

        $(this).closest('form').find('#product_form_submit').prop('disabled', true);
    } else {
        productPrice.closest('.price').find('> p.piece-price').show();
        $(this).closest('form').find('#product_form_submit').prop('disabled', false);
    }

    productPrice.text(price);
};

product.toggleVariationOrderableQuantities = function (selectedOption) {
    let html = '';
    let formQuantity = $('#product_form_quantity');
    let orderableQuantities = selectedOption.attr('data-orderable-quantities');

    if (orderableQuantities) {
        orderableQuantities = orderableQuantities.split(',');
        orderableQuantities.forEach(function (value) {
            html += '<option value="' + value + '"' + (formQuantity.val() === value ? ' selected="selected"' : null) + '>' + value + '</option>';
        });

        formQuantity.html(html);
    }
};

product.toggleVariationImage = function (selectedOption) {
    const variation_image = selectedOption.attr('data-image');
    if (variation_image) {
        $('img', '#product-foto .foto-groot').attr('src', variation_image);
        $('.foto-thumbs .fa').removeClass('active');
    }
};

product.changeCombinationDetails = function (productId) {
    if (combinationProductsContainer.length > 0 && productId) {
        $.ajax({
            url: combinationProductsContainer.attr('data-combination-info-path'),
            method: 'POST',
            data: {
                product: productId
            }
        }).then((result) => {
            combinationProductsContainer.html(result);
        });
    }
};

product.initProductCarousel = function (size) {
    const productCarousel = $('.product-carousel');

    if (productCarousel.hasClass('slick-slider')) {
        productCarousel.slick('unslick');
    }

    if (size > 768) {
        return false;
    }

    function setSlickNavLabels(sliderElm, currentSlideElm) {
        const prevSlideElm = currentSlideElm.prev();
        const nextSlideElm = currentSlideElm.next();
        const prevLabel = prevSlideElm.find('.carousel-item').data('label');
        const nextLabel = nextSlideElm.find('.carousel-item').data('label');

        sliderElm.find('> .slider-nav-prev').text(prevLabel);
        sliderElm.find('> .slider-nav-next').text(nextLabel);
    }

    productCarousel
    .on('init', function (slick) {
        setSlickNavLabels($(slick.delegateTarget), $(slick.delegateTarget).find('.slick-current'));
    })
    .on('afterChange', function (slick) {
        setSlickNavLabels($(slick.delegateTarget), $(slick.delegateTarget).find('.slick-current'));
    })
    ;

    productCarousel.slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 1,
        prevArrow: '<span class="slider-nav-prev"></span>',
        nextArrow: '<span class="slider-nav-next"></span>',
    });
};

product.bindEvents = function () {
    const productForm = $('#product_form');
    const productVariation = $('#product_variation');
    let previousQuantity = 0;

    window.addEventListener('popstate', function (e) {

        if (e.state === null && location.hash === '') {
            isHome = true;
            product.home();
            return;
        }

        isHome = false;

        let variationId = null;
        if (e.state !== null && e.state.variationId !== undefined) {
            variationId = e.state.variationId;
        }

        if (!variationId && location.hash.length > 0) {
            variationId = parseInt(location.hash.replace('#', ''));
        }

        if (variationId) {
            const selector = `[data-variation-id='${variationId}'] .btn-product-selection`;
            product.selectProductVariation($(selector), false)
        }
    });

    // Disable button after submit.
    productForm.on('submit', function () {
        $('#product_form_submit').attr('disabled', 'disabled');
    });

    $(window).on('resize', function () {
        product.determineImageHeight();
    });

    $('.foto-thumbs').on('mouseenter', '.thumb', function () {
        product.setThumbnail($(this));
    });

    productVariation.on('change', function (e) {
        const variationId = $(this).val();
        const selectedOption = $(e.currentTarget).find('option:selected');

        product.toggleVariationPersonalizationPrice(selectedOption);
        product.toggleVariationImage(selectedOption);
        product.toggleSku(variationId);
        product.toggleStockIndicator(variationId);
        product.toggleVariationPrice(selectedOption);
        product.toggleVariationOrderableQuantities(selectedOption);

        product.changeCombinationDetails(selectedOption.val());

        product.togglePersonalizationRequired();
        product.togglePersonalizationPriceDisplay();
        product.toggleSubmitBtn();

        product.toggleUpsell(variationId);
    });

    $('#add_personalization').on('change', function () {
        product.togglePersonalizationRequired();
        product.togglePersonalizationPriceDisplay();
        product.toggleSubmitBtn();
    });

    $('#add_card').on('change', function () {
        const cardAlreadyChecked = $('input[name=radio-card]:checked').length;

        if ($(this).is(':checked')) {
            $(this).closest('.line-checkbox').find('.aantal').show();
            $('.wishcard', '#content-product #product-info').show();

            if (!cardAlreadyChecked) {
                $('input[name=radio-card]:first').attr('checked', true).prop('checked', true).trigger('change');
            }

            $('#card_price', '#content-product #product-info').css({
                'font-weight': 'bold'
            });
        } else {
            $(this).closest('.line-checkbox').find('.aantal').hide();
            $('.wishcard', '#content-product #product-info').hide();
            $('#card_price', '#content-product #product-info').css({
                'font-weight': 'normal'
            });
        }
    }).trigger('change');

    $('#letter').on('change', function () {
        $('.letter-dropdown').toggle($(this).is(':checked'));
    }).trigger('change');

    $('#product_form_letter_file').on('change', function () {
        $('.upload-status').show();
        $('#letter_upload_icon').addClass('fa-pencil').removeClass('fa-cloud-upload');
        $('.upload-btn-text').html(this.files && this.files.length ? this.files[0].name : '');
    });

    $('.wishcard textarea').on('keydown keyup', function (e) {
        switch (e.type) {
            case 'keydown':
                if (parseInt($(this).get(0).scrollHeight) <= parseInt($(this).outerHeight())) {
                    $.data(this, 'value', $(this).val());
                }

                break;
            case 'keyup':
                if (parseInt($(this).get(0).scrollHeight) > parseInt($(this).outerHeight())) {
                    if ($.data(this, 'value').length > 0) {
                        $(this).val($.data(this, 'value'));
                    } else {
                        $(this).val($(this).val().substr(0, -1)).trigger('keyup');
                    }
                }

                $(this).toggleClass('empty', e.currentTarget.value === '');

                break;
        }
    }).trigger('keyup');

    $('input.radio-card').on('change', function () {
        const checkedState = $('input.radio-card:checked');
        const cardSelect = $('select[name="card"]');

        if ($(this).is(':checked')) {
            cardSelect.val($(this).val());
        }

        const cardImage = $('#card_fillin_image');
        const cardImageUrl = cardSelect.find('option:selected').attr('data-image');

        if (!checkedState) {
            cardSelect.val('');
        }

        if (cardImageUrl) {
            cardImage.css({
                'background-image': 'url(\'' + cardImageUrl + '\')'
            });
        } else {
            cardImage.css({
                'background-image': ''
            });
        }
    }).trigger('change');

    productForm.on('submit', function (e) {
        const personalizationOptionsAvailable = $('.product-options .line-checkbox:visible').length;
        const personalizationOptionsChecked = $('.product-options .line-checkbox:visible input[type=checkbox]:checked').length;

        const productQuantity = $('#product_form_quantity').val();
        const cardOptionChecked = $('.product-options .line-checkbox:visible input[name="add_card"]:checked').length;

        const cardQuantity = $('#form_card_quantity');
        const productName = $('#content-product').hasClass('product-type-simple') ? $('.form-static-select-wrapper', '.soort').text() : $('option:selected', '#product_variation').text();

        const personalizationRequired = product.checkPersonalizationRequired();

        const cardOption = document.querySelector('#add_card');
        let cardText = document.getElementsByName('card_text')[0];
        let cardIsValid = true;
        if (cardText !== undefined) {
            cardIsValid = !cardOptionChecked || (cardOptionChecked && cardText.value !== '')
        }

        const letterOption = document.querySelector('#letter');
        let letterOptionChecked = $('.product-options .line-checkbox:visible input[name="letter"]:checked').length;
        const letterFile = document.querySelector('#product_form_letter_file');
        let letterIsValid = true;
        if (letterFile !== null) {
            letterIsValid = !letterOptionChecked || (letterOptionChecked && letterFile.value !== '');
        }

        const submitButton = document.querySelector('#product_form_submit');

        const personalizationOptionCount = cardOptionChecked || letterOptionChecked || personalizationOptionsChecked;

        if (!cardIsValid) {
            e.preventDefault();

            swal({
                title: trans('product.card_text.empty'),
                html: trans('product.card.type_your_text'),
                type: 'info',
                confirmButtonText: trans('product.card_text.button'),
                showCancelButton: true,
                cancelButtonText: trans('cart.order.card.continue_without'),
                reverseButtons: true,
                useRejections: true // swal7fix
            }).then(() => {
                cardText.focus();
            }, () => {
                cardOption.checked = false;
                cardOption.removeAttribute('checked');
                document.querySelector('.wishcard').style.display = 'none';
                productForm.submit();
            });

            submitButton.removeAttribute('disabled');
        } else if (!letterIsValid) {
            e.preventDefault();

            swal({
                title: trans('checkout.delivery.no_letter_cta_text.html'),
                html: trans('letter.upload'),
                type: 'info',
                confirmButtonText: trans('letter.choose_file'),
                showCancelButton: true,
                cancelButtonText: trans('letter.decline'),
                reverseButtons: true,
                useRejections: true // swal7fix
            }).then(() => {
                letterFile.click();
            }, () => {
                letterOption.checked = false;
                letterOption.removeAttribute('checked');
                document.querySelector('.letter-dropdown').style.display = 'none';
                productForm.submit();
            });

            submitButton.removeAttribute('disabled');
        }

        if (!forceSubmit && personalizationOptionsAvailable >= 1) {
            if (
                personalizationOptionCount === 0
                && !personalizationRequired
                && letterIsValid
            ) {
                e.preventDefault();

                swal({
                    title: trans('modal.title.are_you_sure'),
                    html: trans('cart.product.message.no_personalization'),
                    type: 'question',
                    showCancelButton: true,
                    cancelButtonText: trans('cart.order.card.choose'),
                    confirmButtonText: trans('cart.order.card.continue_without'),
                    reverseButtons: true,
                    useRejections: true // swal7fix
                }).then(() => {
                    forceSubmit = true;
                    productForm.submit();
                    submitButton.setAttribute('disabled', 'disabled');
                }, (dismiss) => {
                    if (dismiss === 'cancel') {
                        $('#add_card').prop('checked', true).attr('checked', true).trigger('change');
                    }
                    submitButton.removeAttribute('disabled');
                });
            } else if (
                cardQuantity.length === 0
                && productQuantity > 1
                && personalizationOptionsChecked > 0
                && letterIsValid
            ) {
                e.preventDefault();

                swal({
                    customClass: 'modal-personalization',
                    title: trans('cart.personalization.message.same').replace('%quantity%', productQuantity),
                    html: trans('cart.product.message.same_personalization_message').replace('%product%', productName),
                    type: 'info',
                    showCancelButton: true,
                    cancelButtonText: trans('modal.cancel'),
                    confirmButtonText: trans('modal.add_to_cart'),
                    reverseButtons: true,
                    useRejections: true // swal7fix
                }).then(() => {
                    forceSubmit = true;
                    submitButton.setAttribute('disabled', 'disabled');
                    productForm.submit();
                }, () => {
                    submitButton.removeAttribute('disabled');
                });
            }
        }
    });

    $('.product-column-view')
    .on('submit', '.product-selection-form', function (e) {
        e.preventDefault();

        const form = $(this);
        const values = form.serializeArray();
        const productVariationid = form.find('input[name=product_variation_id]').val();
        const productForm = $('#product_form');
        const useCombinationField = $('#form_use_combination', productForm);
        const variationField = $('#form_variation', productForm);

        variationField.val(productVariationid).trigger('change');

        useCombinationField.val(0);
        if (values.length > 1) {
            useCombinationField.val(1);
        }

        productForm.submit();
    })
    .on('click', '.btn-product-selection', function (e) {
        e.preventDefault();

        const variationId = $(this).closest('.carousel-item').data('variation-id');

        history.pushState({variationId: variationId}, null, location.href);

        product.selectProductVariation($(this), true);
    })
    .on('change', '.product-carousel .line-checkbox input[type=checkbox]', function () {
        const elm = $(this);
        const label = elm.next();
        const checked = elm.prop('checked');

        $('.line-checkbox input[type=checkbox]').prop('checked', false);
        elm.prop('checked', checked);

        $('#use_combination').prop('checked', checked);
        $('#use_combination + label').text(label.data('title'));
        $('#use_combination_price').text(label.data('price'));
    })
    ;

    $(window).resize(function () {
        product.initProductCarousel($(window).width());
    }).trigger('resize');

    $('#product_form_quantity').on('change', function () {
        if (previousQuantity === 0) {
            previousQuantity = $(this).val();
        }

        const upsellQuantity = $('.upsell-quantity');
        if (upsellQuantity.length
            && upsellQuantity.attr('data-quantity-mode') === 'loosely_following'
            && previousQuantity === upsellQuantity.val()) {
            upsellQuantity.val($(this).val());
        }

        previousQuantity = $(this).val();
    }).trigger('change');

    $('.upsell-checkbox').on('change', function() {
        if($(this).hasClass('variation-holder')) {
            const variationContainer = $(this).closest('.upsell-container').find('.variations');

            variationContainer.toggle();

            if(variationContainer.is(':hidden')) {
                variationContainer.find('input').prop('checked', false);
            }
        }
    });

    $('.upsell-variation-radio').on('click', function() {
        $('.upsell-property').hide();
        const variationId = $(this).attr('data-variation-id');
        $(`.upsell-property[data-variation-id="${variationId}"]`).show();

        $(this).closest('.upsell-container').find('.upsell_price').html($(this).attr('data-price'));
    })

    productVariation.trigger('change');
};

product.selectProductVariation = (elm, animate) => {
    doAnimate = !!animate;

    const carouselElm = elm.closest('.product-carousel');
    const carouselItemElm = elm.closest('.carousel-item');
    const items = carouselElm.find('.carousel-item');
    const productSelectionForm = $('#product-selection-form');
    const variationId = carouselItemElm.data('variation-id');

    items.each(() => {
        const item = $(this);
        const productItem = item.find('> div');

        item.attr('style', 'margin: 0;');
        productItem.width(item.width());
    });

    items.removeClass('active');
    carouselItemElm.addClass('active');
    carouselElm.addClass('animate');

    let animationTimeout = null;
    const animationReadyCallback = () => {
        if (animationTimeout) {
            clearTimeout(animationTimeout);
        }
        carouselElm.addClass('ready');

        productSelectionForm.fadeOut(0).fadeIn(350);
    };

    if (doAnimate) {
        animationTimeout = setTimeout(animationReadyCallback, 350);
    } else {
        animationReadyCallback();
    }

    $('#product_variation').val(variationId).trigger('change');
};

product.home = function () {
    $('.carousel-item').removeClass('active');
    $('.product-carousel').removeClass('animate ready').removeAttr('style');
    $('#product-selection-form').hide();
};

product.setThumbnail = function (element) {
    $('.foto-thumbs span').removeClass('active');
    $('.foto-groot img:first-child').attr('src', $(element).find('img').attr('data-large-src'));

    $(element).find('span').addClass('active');
};

product.toggleUpsell = function (variationId) {
    $('.upsell-checkbox').removeAttr('checked');

    $('.upsell-container').hide();
    $('.upsell-container[data-variation-id="' + variationId + '"]').show();
};

product.init();
