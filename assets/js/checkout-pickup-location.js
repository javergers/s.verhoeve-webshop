import 'slick-carousel';
import moment from 'moment';

const PickupLocation = new function () {
    const self = this;

    let queryDistance = 35;
    let geocodeDistance = 300;

    let map = null;
    let geocoder = null;
    let pickupRootElement = $('.pickup');
    let markers = [];
    let timeslots = [];
    let current = {
        location: null,
        day: null,
        timeslot: null,
        valid: false,
    };

    const daySliderOptions = {
        infinite: false,
        prevArrow: '<span class="fa fa-chevron-left"></span>',
        nextArrow: '<span class="fa fa-chevron-right"></span>',
        slidesToShow: 7,
        slidesToScroll: 7,
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 5
                }
            },
        ],
    };

    self.pickupContainer = null;

    self.getMap = function () {
        if (map === null) {
            map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 52.05218768385301, lng: 5.4791296146915025},
                zoom: 6,
                disableDefaultUI: true,
            });
        }

        return map;
    };

    self.getGeocoder = function () {
        if (geocoder === null) {
            geocoder = new google.maps.Geocoder();
        }

        return geocoder;
    };

    self.init = function () {
        if (typeof (currentLocale) !== 'undefined') {
            moment.locale(currentLocale);
        }
        //Do not load script when maps in not available
        if (typeof (google) === 'undefined') {
            return;
        }
        self.getMap();
        self.bindEvents();

        $.ajax({
            url: pickupRootElement.data('prefill-locations-url'),
            method: 'GET',
            dataType: 'json',
            data: {}
        }).done(function (response) {
            self.insertMarkers(response);
        });
    };

    self.getNearbyLocations = function (q) {
        $('.delivery-pickup-method-error').html('');
        let geocodeDistanceCircle = new google.maps.Circle({
            radius: geocodeDistance * 1000,
            center: self.getMap().getCenter(),
        });
        self.getGeocoder().geocode({
            address: q.toString(),
            bounds: geocodeDistanceCircle.getBounds(),
        }, function (geocode, geocodeStatus) {
            if (geocodeStatus === 'OK') {
                //Load markers
                $.ajax({
                    url: pickupRootElement.data('nearby-pickup-locations-url'),
                    method: 'GET',
                    dataType: 'json',
                    data: {
                        lat: geocode[0].geometry.location.lat,
                        lng: geocode[0].geometry.location.lng,
                        distance: queryDistance,
                    }
                }).done(function (response) {
                    self.insertMarkers(response);
                });

                //Zoom to location
                let zoomCircle = new google.maps.Circle({
                    radius: queryDistance * 1000,
                    center: geocode[0].geometry.location,
                });
                self.getMap().fitBounds(zoomCircle.getBounds());
            } else {
                $('.delivery-pickup-method-error').html(`<div class="alert alert-danger">${trans('label.pickup_timeslot.not_found')}</div>`);
                console.error('Geocode was not successful for the following reason: ' + geocodeStatus);
            }
        });
    };

    self.insertMarkers = function (response) {
        if (response.length > 0) {
            markers = [];
            for (let mId in response) {
                let location = response[mId];
                let marker = {
                    infoWindow: new google.maps.InfoWindow({
                        content: `<div>
                                                <div style="font-weight: bold;">${location.supplierName}</div>
                                                <div>${location.street}</div>
                                                <div>${location.postcode} ${location.city}</div>
                                               </div>`,
                    }),
                    marker: new google.maps.Marker({
                        position: {
                            lat: parseFloat(location.lat),
                            lng: parseFloat(location.lng),
                        },
                        map: self.getMap(),
                        title: location.supplierName,
                    }),
                    location: location,
                };
                marker.marker.addListener('click', function () {
                    marker.infoWindow.open(self.getMap(), marker.marker);
                    $(`label[for=pickup-radio-${mId}]`).trigger('click');
                });
                markers.push(marker);
            }
            self.renderSearchResultList();
        } else {
            $('.delivery-pickup-method-error').html(`<div class="alert alert-danger">${trans('label.pickup_timeslot.not_found')}</div>`);
        }
    };

    self.calculateColWidth = function (nrOfCols, maxNrOfCols) {
        if (nrOfCols >= maxNrOfCols) {
            return 12 / maxNrOfCols;
        }
        return 12 / nrOfCols;
    };

    self.renderSearchResultList = function () {
        const list = $('#pickup-location-list');
        list.html('');

        for (let id in markers) {
            let marker = markers[id];
            let element = `
                <li>
                    <label for="pickup-radio-${id}">
                        <input type="radio" value="${marker.location.pickupLocationId}" id="pickup-radio-${id}" name="pickupLocation">
                        <span class="title truncate">${marker.location.supplierName}</span>
                        <span class="description">${marker.location.street}</span>
                        <span class="description">${marker.location.postcode} ${marker.location.city}</span>
                    </label>
                </li>
            `;
            list.append(element);
        }

        if (markers.length === 1) {
            $('input[name=pickupLocation]:first-child').prop('checked', true).trigger('change');
        }
    };

    self.loadTimeslots = function (pickupLocationId) {
        if (typeof (timeslots[pickupLocationId]) !== 'undefined') {
            return self.renderDaysList(pickupLocationId);
        }

        $.ajax({
            url: pickupRootElement.data('pickup-location-opening-hours-url'),
            method: 'GET',
            dataType: 'json',
            data: {
                pickupLocationId: pickupLocationId,
            }
        }).done(function (response) {
            if (response.length > 0) {
                for (let id in response[0].dates) {
                    response[0].dates[id].date = moment(response[0].dates[id].date);
                }
                timeslots[pickupLocationId] = response[0].dates;
                self.renderDaysList(pickupLocationId);
            }
        });
    };

    self.renderTimeslots = function (pickupLocationId, dateId) {
        let slotItems = timeslots[pickupLocationId][dateId].timeslots;
        let colWith = self.calculateColWidth(slotItems.length, 3);
        let pickupTimeslotsElement = $('#pickup-timeslots');
        pickupTimeslotsElement.html('');
        pickupTimeslotsElement.parent().find('.not-available').remove();
        pickupTimeslotsElement.parent().find('.bezorg-bericht').remove();
        pickupTimeslotsElement.parent().find('.label-timeslots').show();

        current.valid = false;
        current.timeslot = null;

        if (slotItems.length === 0) {
            pickupTimeslotsElement.before(`<div class="not-available">${trans('label.timeslot.not_available')}</div>`);
        } else {
            for (let id in slotItems) {
                let timeslot = slotItems[id];
                let element = `
                    <div class="col-md-${colWith}">
                        <div class="timeslot-item" data-timeslot-id="${id}">
                            <label for="pickup-timeslot-radio-${id}">
                                <input type="radio" value="${timeslot.from}-${timeslot.till}" id="pickup-timeslot-radio-${id}" name="pickupLocationTimeslot">
                                <span class="title">${timeslot.from} - ${timeslot.till} <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="${trans('label.delivery_pickup_timeslot_info')}"></i></span>
                            </label>
                         </div>
                    </div>
                `;

                const elm = $(element);
                const iconElm = elm.find('i.fa');

                pickupTimeslotsElement.append(elm);

                if (iconElm.length > 0) {
                    new Tooltip(iconElm[0]);
                }
            }
        }
    };

    self.renderDaysList = function (pickupLocationId) {
        let dates = timeslots[pickupLocationId];
        let pickupDaysElement = $('#pickup-days');
        pickupDaysElement.html('');

        current.valid = false;
        current.day = null;
        current.timeslot = null;

        let firstSelected = false;

        for (let id in dates) {
            let date = dates[id];
            let element = $(`
                <div class="pickup-day">
                    <div class="timeslot-day-item" data-timeslot-day-id="${id}">
                        <label for="pickup-timeslot-day-radio-${id}">
                            <input type="radio" value="${date.date.toISOString()}" id="pickup-timeslot-day-radio-${id}" name="pickupLocationTimeslotDay">
                            <span class="title">${date.date.format('dd')}</span>
                            <span class="description">${date.date.format('D MMM')}</span>
                        </label>
                     </div>
                </div>
            `);

            pickupDaysElement.append(element);

            if (date.timeslots.length > 0 && firstSelected === false) {
                firstSelected = true;
                element.find('input').prop('checked', true).trigger('change');
            }
        }

        if (pickupDaysElement.hasClass('slick-initialized')) {
            pickupDaysElement.slick('unslick');
        }

        pickupDaysElement.slick(daySliderOptions);
    };

    self.bindEvents = function () {
        $('.scrollbar-inner', '.pickup-container').scrollbar();

        self.pickupContainer = $('.pickup-container');
        self.pickupDescription = $('.delivery-pickup-method-description');

        let deliveryMethodContainer = $('.delivery-methods');
        let deliveryMethodList = $('.delivery-method-list', deliveryMethodContainer);
        deliveryMethodList.on('change', function () {
            $('#pickup-days').resize();
        });

        $('#cart_order_submit_search').on('click', function (e) {
            e.preventDefault();
            self.getNearbyLocations($('#cart_order_pickup_search').val());
        });

        $('#cart_order_pickup_search').on('keydown', function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                $('#cart_order_submit_search').trigger('click');
            }
        });

        $('form[name=cart_order]').on('submit', function (e) {
            if ($('#delivery_method_pickup').prop('checked') && current.valid === false) {
                $('.delivery-pickup-method-error').html(`<div class="alert alert-danger">${trans('label.timeslot.not_selected')}</div>`);
                e.preventDefault();
            }
        });

        self.pickupContainer.on('change', 'input[name=pickupLocation]', function (e) {
            const elm = $(e.currentTarget);
            const li = elm.closest('li');
            let pickupLocationId = elm.val();
            current.location = pickupLocationId;
            self.loadTimeslots(pickupLocationId);

            $('li', self.pickupContainer).removeClass('active');
            li.addClass('active');
        });

        self.pickupDescription.on('change', 'input[name=pickupLocationTimeslot]', function (e) {
            const elm = $(e.currentTarget);
            const timeslotItem = elm.closest('.timeslot-item');
            let timeslotId = timeslotItem.data('timeslot-id');
            current.timeslot = timeslotId;
            current.valid = true;
            $('.delivery-pickup-method-error').html('');

            $('.timeslot-item', self.pickupDescription).removeClass('active');
            timeslotItem.addClass('active');
        });

        self.pickupDescription.on('change', 'input[name=pickupLocationTimeslotDay]', function (e) {
            const elm = $(e.currentTarget);
            const timeslotDayItem = elm.closest('.timeslot-day-item');
            let timeslotDayId = timeslotDayItem.data('timeslot-day-id');
            current.day = timeslotDayId;
            self.renderTimeslots(current.location, timeslotDayId);

            $('.timeslot-day-item', self.pickupDescription).removeClass('active');
            timeslotDayItem.addClass('active');
        });
    };
};

export default PickupLocation;
