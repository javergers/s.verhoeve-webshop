require('jquery-ui/themes/base/slider.css');
require('./../scss/assortment.scss');

require('jquery-ui/ui/widgets/slider');

import ContentBlock from './components/ContentBlock';

const APP = require('./app').default;

let assortment = new function () {
    this.contentBlock = new ContentBlock();
};

assortment.init = function () {
    APP.timer.setTimer();
    APP.banner.leftTop();

    assortment.bindEvents();
};

assortment.bindEvents = function () {
    var self = this;

    $('.filter-toggle').on('click', function () {
        $('.filter').toggle();
    });

    $('.footer-dropdown h3').click(function () {
        $(this).parent().find('ul').toggle();
        $(this).find('.down').toggle();
        $(this).find('.up').toggle();
    });

    $('.producthover').on('mouseenter mouseleave', function (e) {
        if ($(window).width() < 992) {
            return false;
        }

        switch (e.type) {
            case 'mouseenter':
                var productItemClone = $(this).find('.product-item').clone(true);

                $(productItemClone).addClass('product-item-clone');
                $(productItemClone).insertAfter($(this).find('.product-item'));

                break;
            case 'mouseleave':
                $(this).find('.product-item-clone').remove();

                break;
        }
    });

    var minAmount = $('#min-amount');
    var maxAmount = $('#max-amount');
    var sliderRange = $('#slider-range');

    sliderRange.slider({
        range: true,
        min: parseInt(sliderRange.attr('data-min-price')),
        max: parseInt(sliderRange.attr('data-max-price')) + 1,
        values: [0, 100],
        slide: function (event, ui) {
            var firstVal = parseInt(ui.values[0]);
            var secondVal = parseInt(ui.values[1]);
            minAmount.val(firstVal);
            maxAmount.val(secondVal);

            minAmount.trigger('change');
            maxAmount.trigger('change');
        }
    });

    var firstVal = sliderRange.slider('values', 0);
    var secondVal = sliderRange.slider('values', 1);
    minAmount.val(firstVal);
    maxAmount.val(secondVal);

    minAmount.on('change', function (e) {
        var firstVal = $(this).val();
        sliderRange.slider('values', 0, firstVal);

        filterProducts();
    });

    maxAmount.on('change', function (e) {
        var secondVal = $(this).val();
        sliderRange.slider('values', 1, secondVal);

        filterProducts();
    });

    $('.search').on('click', function () {
        $('.filter').toggle();
    });

    $('.filter-check').on('click', function () {
        filterProducts();
    });

    function filterProducts() {
        var product = $('.producthover');

        product.hide().removeClass('filtered');
        var filtered = false;

        var filters = {};
        var i = 0;
        $('.filter-check').each(function () {
            if ($(this).is(':checked')) {
                filtered = true;
                var filterArray = $(this).val().split('-');
                if (typeof filters[filterArray[0]] === typeof undefined) {
                    filters[filterArray[0]] = [];
                }

                filters[filterArray[0]].push($(this).val());
                i++;
            }
        });

        if (!filtered) {
            product.show();
        } else {
            var filterCombinations = cartesian(filters);
            $.each(filterCombinations, function (index, combination) {
                var filteredProduct = '';
                $.each(combination, function (key, filter) {
                    filteredProduct += '.filter-' + filter;
                });

                $(filteredProduct).addClass('filtered').show();
            });
        }

        product.filter(function () {
            return parseFloat($(this).attr('data-product-price')) < parseInt(minAmount.val()) || parseFloat($(this).attr('data-product-price')) > parseInt(maxAmount.val());
        }).hide();

        product.filter(function () {
            if (filtered) {
                if (!$(this).hasClass('filtered')) {
                    return false;
                }
            }
            return parseFloat($(this).attr('data-product-price')) > parseInt(minAmount.val()) && parseFloat($(this).attr('data-product-price')) < parseInt(maxAmount.val());
        }).show();

        $('.filter-check').each(function () {
            var filteredProduct = $('.producthover.filter-' + $(this).attr('id').replace('check-', '') + ':visible');

            var count = filteredProduct.length;
            $(this).closest('.check-row').find('.filter-count').html(count);
        });
    }
};


function cartesian(args) {
    var arg = [];
    $.each(args, function (index, array) {
        arg.push(array);
    });

    var r = [], max = arg.length - 1;

    function helper(arr, i) {
        for (var j = 0, l = arg[i].length; j < l; j++) {
            var a = arr.slice(0); // clone arr
            a.push(arg[i][j]);
            if (i == max)
                r.push(a);
            else
                helper(a, i + 1);
        }
    }

    helper([], 0);
    return r;
}

assortment.init();
