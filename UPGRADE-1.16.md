# Upgrade from 1.15 to 1.16

##CSV
Bedrijven kunnen middels CSV een bestelling plaatsen.
Elke regel in een CSV word gezien als èèn (1) `Winkelwagen`.

Er word en mail verzonden naar de helpdesk bij een foutieve CSV (denk hierbij aan een missend sku)

In `app\config\parameters.yml` dient de `csv_location` variabele gezet te zijn. 

Elke csv bestelling word gelogged en opgeslagen in de `csv_order` tabel
  
**Voorbeeld**  
`var\csv\1\order-1.csv` Bestands naam is naar keuze
```csv
    ReferentieNummer,ProductSKU,ProductAmount,KaartjeSKU,KaartjeAmount,KaartTekst,PersonalisatieSKU,PersonalisatieAmount,Bezorgdatum,NaamOntvanger,StraatOntvanger,HuisnummerOntvanger,PostcodeOntvanger,PlaatsOntvanger,Opmerking
    A12345BD,FRT10001,1,CRD10000,1,Deze bijzondere dag Maar 1 keer in het jaar Een dag waarop alles mag Om te vieren met elkaar!,PER10002,2,23-04-18,Lars Aalberts,Noorderdreef,60,2153LL,Nieuw-Vennep,Een test bestelling van TG CSV import order
```
    
Dit betreft een basis fruitmand, en zal een `Cart`, `CartOrder` en meerdere `CartOrderLine`s bevatten.

**Foutief voorbeeld**
`var\csv\2\order-1.csv`
```csv
    ReferentieNummer,ProductSKU,ProductAmount,KaartjeSKU,KaartjeAmount,KaartTekst,PersonalisatieSKU,PersonalisatieAmount,Bezorgdatum,NaamOntvanger,StraatOntvanger,HuisnummerOntvanger,PostcodeOntvanger,PlaatsOntvanger,Opmerking
    A12345BD,,1,CRD10000,1,Deze bijzondere dag Maar 1 keer in het jaar Een dag waarop alles mag Om te vieren met elkaar!,PER10002,2,23-04-18,Lars Aalberts,Noorderdreef,60,2153LL,Nieuw-Vennep,Een test bestelling van TG CSV import order
```

Dit betreft een missend `productSKU`
Hierdoor wordt een mail verstuurd naar `helpdesk@topgeschenken.nl` met het CSV bestand als bijlage.
Meer email adressen kunnen worden toegevoegd aan de variabele `csv_error_emails` 


###Einde dag (trigger command `csv:report`)
Aan het einde van de dag word een csv rapportage opgemaakt en verstuurd naar `klantenservice@topgeschenken.nl`.
Meer email adressen kunnen worden toegevoegd aan de variabele `csv_report_emails`

