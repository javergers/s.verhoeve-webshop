<?php

/** @noinspection PhpUndefinedClassInspection */
/** @noinspection InArrayMissUseInspection */

use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

/**
 * Class AppKernel
 */
class AppKernel extends Kernel
{
    /**
     * @return array|\Symfony\Component\HttpKernel\Bundle\BundleInterface[]
     */
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new DataDog\AuditBundle\DataDogAuditBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test', 'staging'])) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
        }

        if (in_array($this->getEnvironment(), ['dev'])) {
            $bundles[] = new \Symfony\Bundle\WebServerBundle\WebServerBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Hautelook\AliceBundle\HautelookAliceBundle();
            $bundles[] = new Fidry\AliceDataFixtures\Bridge\Symfony\FidryAliceDataFixturesBundle();
            $bundles[] = new Nelmio\Alice\Bridge\Symfony\NelmioAliceBundle();
        }

        $bundles[] = new FOS\UserBundle\FOSUserBundle();
        $bundles[] = new Fresh\DoctrineEnumBundle\FreshDoctrineEnumBundle();
        $bundles[] = new Scheb\TwoFactorBundle\SchebTwoFactorBundle();
        $bundles[] = new Snc\RedisBundle\SncRedisBundle();
        $bundles[] = new Liuggio\ExcelBundle\LiuggioExcelBundle();
        $bundles[] = new EightPoints\Bundle\GuzzleBundle\EightPointsGuzzleBundle();
        $bundles[] = new Knp\DoctrineBehaviors\Bundle\DoctrineBehaviorsBundle();
        $bundles[] = new Misd\PhoneNumberBundle\MisdPhoneNumberBundle();
        $bundles[] = new JMS\JobQueueBundle\JMSJobQueueBundle();
        $bundles[] = new JMS\DiExtraBundle\JMSDiExtraBundle();
        $bundles[] = new JMS\AopBundle\JMSAopBundle();
        $bundles[] = new Craue\FormFlowBundle\CraueFormFlowBundle();
        $bundles[] = new SunCat\MobileDetectBundle\MobileDetectBundle();
        $bundles[] = new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle();
        $bundles[] = new FS\SolrBundle\FSSolrBundle();
        $bundles = array_merge($bundles, \AdminBundle\AdminBundle::getBundles());
        $bundles[] = new Liip\ThemeBundle\LiipThemeBundle();
        $bundles[] = new Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle();
        $bundles[] = new \Siphoc\PdfBundle\SiphocPdfBundle();
        $bundles[] = new \Bazinga\GeocoderBundle\BazingaGeocoderBundle();

        $bundles[] = new Translation\Bundle\TranslationBundle();
        $bundles[] = new Translation\PlatformAdapter\Loco\Bridge\Symfony\TranslationAdapterLocoBundle();

        // Main Bundles
        $bundles[] = new AppBundle\AppBundle();
        $bundles[] = new AdminBundle\AdminBundle();

        // Rule Bundle
        $bundles[] = new RuleBundle\RuleBundle();
        $bundles[] = new Ambta\DoctrineEncryptBundle\AmbtaDoctrineEncryptBundle();

        $bundles[] = new Liip\ImagineBundle\LiipImagineBundle();
        $bundles[] = new Ekino\NewRelicBundle\EkinoNewRelicBundle();

        return $bundles;
    }

    /**
     * @return string
     */
    public function getCacheDir()
    {
        return dirname(__DIR__) . '/var/cache/' . $this->getEnvironment();
    }

    /**
     * @return string
     */
    public function getLogDir()
    {
        return dirname(__DIR__) . '/var/logs';
    }

    /**
     * @param LoaderInterface $loader
     * @throws Exception
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir() . '/config/config_' . $this->getEnvironment() . '.yml');
    }

    /**
     * @return string
     */
    public function getRootDir()
    {
        return __DIR__;
    }
}
