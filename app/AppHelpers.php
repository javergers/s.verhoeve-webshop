<?php

// Place common but un-categorized functionality here

/**
 * @Description litteraly does nothing but prevents an unused variable message from the code inspector ...
 * @param array $_
 */
function void(...$_)
{
// Poof
}

if (!\function_exists('timecop_freeze')) {
    /**
     * @param int|\DateTimeInterface $timeStamp New concept of now
     * @return bool Returns <code>true</code> if <code>now</code> is frozen and <code>false</code> if argument is not valid
     */
    function timecop_freeze($timeStamp): bool
    {
        // Dummy function for code completion
        void($timeStamp);

        return true;
    }
}

if (!\function_exists('timecop_return')) {
    /**
     * @return bool Returns <code>true</code>
     */
    function timecop_return(): bool
    {
        // Dummy function for code completion

        return true;
    }
}